{
    "id": "db860f0e-1cfc-494c-b153-11163206bdbb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework",
    "eventList": [
        {
            "id": "0ce1b8c5-48e3-451a-bed4-3284d6cdb12d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "db860f0e-1cfc-494c-b153-11163206bdbb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b2c0c58c-6f53-4bd2-bbc0-603400ff285f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bd3fdac9-2296-4ce2-aa15-d354cea2783f",
    "visible": true
}