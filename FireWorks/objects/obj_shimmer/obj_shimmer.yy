{
    "id": "f28c138b-de24-4c68-90b0-6c12bc3a95c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shimmer",
    "eventList": [
        {
            "id": "d6f8f622-8864-432f-9bb4-82cf077710ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "f28c138b-de24-4c68-90b0-6c12bc3a95c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "321d3820-bf0b-4153-86ae-7371df4fd062",
    "visible": true
}