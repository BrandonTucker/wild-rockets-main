



scale = random_range(.7,.9);
motion_set(random_range(0,360), random_range(1,4));
image_xscale = image_xscale*scale;
image_yscale = image_xscale*scale;
image_speed = random_range(.3,.6)
image_angle=direction+90;
gravity = 0.05;
