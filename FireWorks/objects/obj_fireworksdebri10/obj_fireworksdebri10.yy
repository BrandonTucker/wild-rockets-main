{
    "id": "270bae12-e093-4a6d-bf92-485ae7315d3f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri10",
    "eventList": [
        {
            "id": "1757faba-367a-47b2-b068-54b4dc7213a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "270bae12-e093-4a6d-bf92-485ae7315d3f"
        },
        {
            "id": "dcfdf800-a9ea-4c47-8ac6-b465c01faf6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "270bae12-e093-4a6d-bf92-485ae7315d3f"
        },
        {
            "id": "1223ca6c-cfc2-4db0-905e-f1657afd38e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "270bae12-e093-4a6d-bf92-485ae7315d3f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
    "visible": true
}