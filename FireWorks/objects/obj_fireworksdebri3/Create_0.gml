

scale = random_range(.9,1.1);
motion_set(random_range(0,360), random_range(2,7));
image_xscale = image_xscale*scale;
image_yscale = image_xscale*scale;
image_speed = random_range(.35,.7)
image_angle=direction+90;
gravity = 0.03;
