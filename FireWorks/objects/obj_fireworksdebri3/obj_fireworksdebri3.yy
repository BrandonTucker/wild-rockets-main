{
    "id": "11cfecfe-7798-401a-95a5-f36f41b26e3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri3",
    "eventList": [
        {
            "id": "5e5abd9b-7189-423a-9e90-e19089aabf7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11cfecfe-7798-401a-95a5-f36f41b26e3b"
        },
        {
            "id": "bb0bbaaf-88ed-4e59-b0b4-f0c616e09fc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "11cfecfe-7798-401a-95a5-f36f41b26e3b"
        },
        {
            "id": "336d5a6c-afdb-4c28-8d40-54b62e81fc56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "11cfecfe-7798-401a-95a5-f36f41b26e3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
    "visible": true
}