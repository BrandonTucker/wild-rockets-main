{
    "id": "704b6664-fc57-48d3-998b-4cc0c8d108d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri11",
    "eventList": [
        {
            "id": "edf9b78c-0a6a-4e4f-8d3c-b6ebb73acbcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "704b6664-fc57-48d3-998b-4cc0c8d108d0"
        },
        {
            "id": "321d14c7-08b7-4b32-9ccc-dfef48b2c2e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "704b6664-fc57-48d3-998b-4cc0c8d108d0"
        },
        {
            "id": "6b212ff1-c8e6-44f4-91f4-c9ea0adc5e53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "704b6664-fc57-48d3-998b-4cc0c8d108d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
    "visible": true
}