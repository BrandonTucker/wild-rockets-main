{
    "id": "ffee330d-92b6-422e-b360-6820b4537c33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mortor",
    "eventList": [
        {
            "id": "d0add8f7-f5b0-4801-a8c5-d0f5691badee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ffee330d-92b6-422e-b360-6820b4537c33"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3fbb2e5a-28e8-4a75-bda2-5d78664470bb",
    "visible": true
}