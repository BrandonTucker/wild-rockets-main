{
    "id": "405955a4-d8f2-47a3-8b47-c446c5df5ef8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri6",
    "eventList": [
        {
            "id": "f1672cdd-a74b-4292-a0ec-dcd8421d4c03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "405955a4-d8f2-47a3-8b47-c446c5df5ef8"
        },
        {
            "id": "1dc3afa6-0557-46d3-b517-6e633ae0ae18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "405955a4-d8f2-47a3-8b47-c446c5df5ef8"
        },
        {
            "id": "50eeae5a-fcf4-474e-9811-4f3fd41d7b06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "405955a4-d8f2-47a3-8b47-c446c5df5ef8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
    "visible": true
}