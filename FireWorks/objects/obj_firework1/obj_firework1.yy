{
    "id": "100c24d3-fc93-4dc7-bab5-f9b9b8acd9cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework1",
    "eventList": [
        {
            "id": "0851f4ea-d342-4555-9965-efcf89501405",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "100c24d3-fc93-4dc7-bab5-f9b9b8acd9cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b2c0c58c-6f53-4bd2-bbc0-603400ff285f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "67b47d8c-b23f-481d-9e1f-c96d24547142",
    "visible": true
}