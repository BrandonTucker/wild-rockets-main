{
    "id": "1a90b3bb-f36e-4a9d-8114-9e89ff94b3e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri",
    "eventList": [
        {
            "id": "4e7f2031-bb70-456e-ac3c-601af7948bc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1a90b3bb-f36e-4a9d-8114-9e89ff94b3e5"
        },
        {
            "id": "6d2c7d57-d4b6-478a-bf2e-1bf3d206f2ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "1a90b3bb-f36e-4a9d-8114-9e89ff94b3e5"
        },
        {
            "id": "fbdc925c-29e7-4c5f-acc0-1e471bf6307a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1a90b3bb-f36e-4a9d-8114-9e89ff94b3e5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
    "visible": true
}