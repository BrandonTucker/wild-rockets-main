{
    "id": "7b1f1a9c-f152-45df-bc07-d39c69cfd983",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri7",
    "eventList": [
        {
            "id": "72845744-185d-4196-910c-dcde862b299b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7b1f1a9c-f152-45df-bc07-d39c69cfd983"
        },
        {
            "id": "29e4ed8a-a024-4ac1-9326-1434476cbeca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "7b1f1a9c-f152-45df-bc07-d39c69cfd983"
        },
        {
            "id": "86cc2f96-6359-4d95-a019-e7cf9c56c11f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7b1f1a9c-f152-45df-bc07-d39c69cfd983"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
    "visible": true
}