{
    "id": "2cd28dd2-2cb5-4918-abe6-e9ca9aa8d831",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri1",
    "eventList": [
        {
            "id": "b2ef379c-405f-4702-ad0c-8262282d2cb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2cd28dd2-2cb5-4918-abe6-e9ca9aa8d831"
        },
        {
            "id": "7d5735d7-3b10-410a-9070-b561c2c88aba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "2cd28dd2-2cb5-4918-abe6-e9ca9aa8d831"
        },
        {
            "id": "e0de9c13-9a00-4059-8dff-2dac600abde1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2cd28dd2-2cb5-4918-abe6-e9ca9aa8d831"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
    "visible": true
}