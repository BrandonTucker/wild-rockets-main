{
    "id": "e98ffbe8-03c6-4d3c-be38-20a0dd4f4734",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_smallsparkle",
    "eventList": [
        {
            "id": "f8e6dbfc-9a14-4f5f-8207-e82b79280653",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "e98ffbe8-03c6-4d3c-be38-20a0dd4f4734"
        },
        {
            "id": "21909e26-1855-4d0b-a113-2ce27a4fbe35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e98ffbe8-03c6-4d3c-be38-20a0dd4f4734"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
    "visible": true
}