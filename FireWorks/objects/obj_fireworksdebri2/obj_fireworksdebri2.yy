{
    "id": "8bf277f2-5708-4400-ae4e-a91ea5267d86",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri2",
    "eventList": [
        {
            "id": "44aaccb0-5a81-48fc-9ab1-bbb4bb4f780b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8bf277f2-5708-4400-ae4e-a91ea5267d86"
        },
        {
            "id": "eff80954-ab32-4a3c-8ae6-5852264fa680",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "8bf277f2-5708-4400-ae4e-a91ea5267d86"
        },
        {
            "id": "9a41aa1d-b00c-4366-8f55-de2c2a664511",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8bf277f2-5708-4400-ae4e-a91ea5267d86"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
    "visible": true
}