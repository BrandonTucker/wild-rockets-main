
scale = random_range(.9,1.1);
motion_set(random_range(0,360), random_range(2,5));
image_xscale = image_xscale*scale;
image_yscale = image_xscale*scale;
image_speed = random_range(.25,.45)
image_angle=direction+90;
gravity = 0.07;
