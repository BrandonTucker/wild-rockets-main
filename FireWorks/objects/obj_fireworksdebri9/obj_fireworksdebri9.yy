{
    "id": "82dad78f-436b-41a8-a5fe-96908030ca1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri9",
    "eventList": [
        {
            "id": "0be29d62-2492-48d0-adf1-bd7e4b4a03d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82dad78f-436b-41a8-a5fe-96908030ca1e"
        },
        {
            "id": "a5ef7f62-d8fb-45ec-97bd-4eeea0e815ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "82dad78f-436b-41a8-a5fe-96908030ca1e"
        },
        {
            "id": "992f3d5f-fceb-4759-a871-781103146885",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "82dad78f-436b-41a8-a5fe-96908030ca1e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
    "visible": true
}