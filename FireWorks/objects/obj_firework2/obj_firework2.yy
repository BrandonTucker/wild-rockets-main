{
    "id": "5b518740-ced2-421a-ba82-707581bcde20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_firework2",
    "eventList": [
        {
            "id": "6be75793-d1ec-42a2-9649-d8edf87eec88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5b518740-ced2-421a-ba82-707581bcde20"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b2c0c58c-6f53-4bd2-bbc0-603400ff285f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
    "visible": true
}