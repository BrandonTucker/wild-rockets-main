{
    "id": "e9a575af-2b8d-4998-84a3-57a7dce45007",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri4",
    "eventList": [
        {
            "id": "1dfad951-de8b-453a-a2c9-ee31709d4e38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e9a575af-2b8d-4998-84a3-57a7dce45007"
        },
        {
            "id": "56ce5ac4-3f1f-44b6-9fdb-97d40abc0587",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "e9a575af-2b8d-4998-84a3-57a7dce45007"
        },
        {
            "id": "54844a62-59b9-46a1-ab14-c636c4fa232c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e9a575af-2b8d-4998-84a3-57a7dce45007"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
    "visible": true
}