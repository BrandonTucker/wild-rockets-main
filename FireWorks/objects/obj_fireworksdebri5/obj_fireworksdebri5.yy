{
    "id": "7a82487c-f383-457b-893f-d441fd069562",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri5",
    "eventList": [
        {
            "id": "5e4796db-22d3-449c-9f77-2ca1c832a0a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7a82487c-f383-457b-893f-d441fd069562"
        },
        {
            "id": "8ecbec93-fadb-4ba0-8174-23af28323076",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "7a82487c-f383-457b-893f-d441fd069562"
        },
        {
            "id": "6dcf58d5-59f1-482f-836f-12f82d777526",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7a82487c-f383-457b-893f-d441fd069562"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
    "visible": true
}