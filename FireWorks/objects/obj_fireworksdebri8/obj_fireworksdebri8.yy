{
    "id": "48967e72-d9b5-45e6-8c30-7e50570114cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireworksdebri8",
    "eventList": [
        {
            "id": "3754ba12-70a5-42d4-9fb7-181e99d2ece3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "48967e72-d9b5-45e6-8c30-7e50570114cb"
        },
        {
            "id": "cb4aa4e4-a80f-4f89-a4bc-323c3917305d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "48967e72-d9b5-45e6-8c30-7e50570114cb"
        },
        {
            "id": "1db274a6-b4ed-4d53-b166-318a6f39cfc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "48967e72-d9b5-45e6-8c30-7e50570114cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
    "visible": true
}