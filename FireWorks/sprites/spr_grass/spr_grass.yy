{
    "id": "47b15c49-be2a-4c53-8ceb-b9a15607ce23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1168,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9867908-0730-41b2-8dda-c6d1e8494229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47b15c49-be2a-4c53-8ceb-b9a15607ce23",
            "compositeImage": {
                "id": "02a74547-2f0b-465b-86bc-bfc7715eb75c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9867908-0730-41b2-8dda-c6d1e8494229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e082c64a-628b-4f4d-877a-80cac0bc85f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9867908-0730-41b2-8dda-c6d1e8494229",
                    "LayerId": "cdb98a48-5e78-4127-949d-82fcce99afc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1170,
    "layers": [
        {
            "id": "cdb98a48-5e78-4127-949d-82fcce99afc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47b15c49-be2a-4c53-8ceb-b9a15607ce23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}