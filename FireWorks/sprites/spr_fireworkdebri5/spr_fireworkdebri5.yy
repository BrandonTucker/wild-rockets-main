{
    "id": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 44,
    "bbox_right": 55,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd448d4b-4d62-4de6-9b37-1f29992068cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "65f2b24b-4c73-4a7c-bff1-c6a23ad80295",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd448d4b-4d62-4de6-9b37-1f29992068cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67d9b8fa-156f-43cf-a8a2-4ddf91fc29f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd448d4b-4d62-4de6-9b37-1f29992068cf",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "77620574-e49d-486c-a9d6-825c1e66944c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "538173c7-e22e-4634-ac2b-067509ed1063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77620574-e49d-486c-a9d6-825c1e66944c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5b44194-39be-4345-9502-190ba490d68a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77620574-e49d-486c-a9d6-825c1e66944c",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "e84546d1-29d6-49c8-bffd-cf60c05b89b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "d3a5611a-4e7f-47b9-86da-74e75a9fa781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84546d1-29d6-49c8-bffd-cf60c05b89b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef1bba73-f138-46c5-965c-e3bc457cd55d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84546d1-29d6-49c8-bffd-cf60c05b89b2",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "61c833df-2cc1-4e67-9825-fb9cc433af77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "5e2117a0-a907-49e9-8b92-f886f2e1011b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61c833df-2cc1-4e67-9825-fb9cc433af77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c52529c-49f9-4df4-b661-e9ecc792e70d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61c833df-2cc1-4e67-9825-fb9cc433af77",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "28449e6b-4672-4d28-bd12-1621a3be8d25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "22eae0ee-6f99-4aca-a14b-6e75b443faa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28449e6b-4672-4d28-bd12-1621a3be8d25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce5581df-2d16-4653-afba-e287823d856a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28449e6b-4672-4d28-bd12-1621a3be8d25",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "b5ab10b0-4f21-43d5-8fb1-0e8923bfa6c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "2117fdf8-0bd1-47af-ad5a-4662240d2be2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5ab10b0-4f21-43d5-8fb1-0e8923bfa6c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e7c71a-6aa2-4629-885d-15f71ca40df0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5ab10b0-4f21-43d5-8fb1-0e8923bfa6c7",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "4c1c0540-e1d2-4bb4-b8b5-973a44f13b64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "2f5ecb17-631a-447a-afd0-7fd27e2c8a3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c1c0540-e1d2-4bb4-b8b5-973a44f13b64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2664e6d-1470-425b-802b-eebe85488c20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c1c0540-e1d2-4bb4-b8b5-973a44f13b64",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "64c53a99-fd4c-4ef2-82b7-85747c8e1ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "ad7405ed-8571-47c7-b427-d281b385d046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c53a99-fd4c-4ef2-82b7-85747c8e1ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a49c90-27fe-47cd-8522-d9531054b12d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c53a99-fd4c-4ef2-82b7-85747c8e1ff7",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "ddb70c2e-81d9-42a5-90f5-1e56d6997118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "314de964-cb27-45b0-8716-19947d2eade1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddb70c2e-81d9-42a5-90f5-1e56d6997118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf7f86b9-26ec-436f-94f2-c6dd36c231ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddb70c2e-81d9-42a5-90f5-1e56d6997118",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "f61b18dc-b818-4876-b1ce-c4d7128ac093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "1d79030c-aa24-43ae-b696-ae03d1712218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f61b18dc-b818-4876-b1ce-c4d7128ac093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4436136-41fa-43bd-8a39-73a38d0b46f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f61b18dc-b818-4876-b1ce-c4d7128ac093",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "e55badd1-deab-4396-8944-78164e63403b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "1a4df8ba-43ea-4218-af2d-146954dbe250",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e55badd1-deab-4396-8944-78164e63403b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f198113-9809-4d6f-b54c-e4f6b2e518d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e55badd1-deab-4396-8944-78164e63403b",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "47bb0e31-4867-464d-b962-cedcdacfe8f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "3ee31519-890c-495e-b3bb-a5d31fcdc916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47bb0e31-4867-464d-b962-cedcdacfe8f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06a1e7fc-793a-41f6-83dd-5129450f1678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47bb0e31-4867-464d-b962-cedcdacfe8f8",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "83a7b96f-cf56-45fb-bbae-ec99c966b58f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "a21a5739-b77e-4886-a5a3-119f70856684",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83a7b96f-cf56-45fb-bbae-ec99c966b58f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30aae975-1f50-4430-9e3b-01a16a5def0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83a7b96f-cf56-45fb-bbae-ec99c966b58f",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "2a3a42dd-c3c3-4f77-a062-01285893de8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "915f9eb2-abea-4190-be31-8274f70d98b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a3a42dd-c3c3-4f77-a062-01285893de8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08c0eeb9-9143-4f95-a179-57854fb6743f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a3a42dd-c3c3-4f77-a062-01285893de8f",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "6efe2ed6-1d46-4309-8f86-ed7489106db3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "a76399cd-e452-4a1e-9df6-850c99fccfb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6efe2ed6-1d46-4309-8f86-ed7489106db3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "333b460d-b90e-42dc-ae18-2d4e1818b00e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6efe2ed6-1d46-4309-8f86-ed7489106db3",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "4264dbb6-a9c9-4916-97b8-bf4179a1c154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "9d3b9495-e093-45c3-afc6-035455b9e18d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4264dbb6-a9c9-4916-97b8-bf4179a1c154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81eb499a-5700-4a3c-b83f-701dac853b39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4264dbb6-a9c9-4916-97b8-bf4179a1c154",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "54cf531e-c90d-443d-b7fc-46b5fb221124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "f5fc9c38-53cd-4a20-acaa-66a26693a8d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54cf531e-c90d-443d-b7fc-46b5fb221124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0fba5f7-df5a-4e9a-ad0e-c1a5d1f322c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54cf531e-c90d-443d-b7fc-46b5fb221124",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "6b5bab6e-fbec-4047-b753-1f2ca8e8c2bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "1440cbb8-2176-4f26-8589-6305e5d085ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b5bab6e-fbec-4047-b753-1f2ca8e8c2bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a27d71c5-7ccc-4bca-bc3f-6f68f96471db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b5bab6e-fbec-4047-b753-1f2ca8e8c2bb",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "a07e089f-cdde-4f76-a387-695e033bf3d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "67330806-9d0d-4f35-84c6-7365cf575a3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a07e089f-cdde-4f76-a387-695e033bf3d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ecdef68-f2a3-4182-8d3f-4d62c1c9c34e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a07e089f-cdde-4f76-a387-695e033bf3d8",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "aa90e861-c3ec-4517-ac88-ed02760dfd16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "3ccd20ee-4f65-4acf-80da-f50be963a0d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa90e861-c3ec-4517-ac88-ed02760dfd16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68b6c474-7d06-4b8f-afb1-31d3f811a8ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa90e861-c3ec-4517-ac88-ed02760dfd16",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "5e1a0871-b289-4da2-8399-6c1f7b85163d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "c9826196-af44-4976-a782-691db159deda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e1a0871-b289-4da2-8399-6c1f7b85163d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "820ebca1-1096-40d9-a44c-17796f5a1922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e1a0871-b289-4da2-8399-6c1f7b85163d",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        },
        {
            "id": "59c0221c-8c62-4283-b0b3-ed72214ff934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "compositeImage": {
                "id": "f2767fde-ba47-420b-877b-05cfde0405ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59c0221c-8c62-4283-b0b3-ed72214ff934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95f3257c-5907-4e85-8336-e6694fa6fd3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59c0221c-8c62-4283-b0b3-ed72214ff934",
                    "LayerId": "87dc8b9b-3db8-4498-85b1-01d194c40aa9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "87dc8b9b-3db8-4498-85b1-01d194c40aa9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe2db90d-20ea-4743-8e62-700865a6c5d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 56
}