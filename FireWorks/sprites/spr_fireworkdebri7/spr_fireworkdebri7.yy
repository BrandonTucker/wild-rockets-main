{
    "id": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 35,
    "bbox_right": 65,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62b01732-4769-4379-8dac-b8161cf058d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "2068a654-5353-4e46-b383-026281883625",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b01732-4769-4379-8dac-b8161cf058d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a521ff-c32e-44c4-885c-ad4086059149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b01732-4769-4379-8dac-b8161cf058d1",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "fefcbfa7-ece6-4e8d-955f-aabaa0ffc2c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "3e3baaf0-18b0-48c8-8fce-e6ca5234f82e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fefcbfa7-ece6-4e8d-955f-aabaa0ffc2c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7c6fdca-5986-4395-95a3-f7c206f012ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fefcbfa7-ece6-4e8d-955f-aabaa0ffc2c0",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "43ef8393-13c0-4734-a166-138565c363f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "b36f1af9-b7d4-4850-a29f-c56efa5cccc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ef8393-13c0-4734-a166-138565c363f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "762dbe78-4f49-4d79-b0e8-c52d8b0a58f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ef8393-13c0-4734-a166-138565c363f0",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "74b3109b-ba48-403d-9273-d8289ccdef5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "a515c1de-e989-4fbd-8245-0c594f791065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74b3109b-ba48-403d-9273-d8289ccdef5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3709e1b2-b96e-40b9-a479-60ea570016bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74b3109b-ba48-403d-9273-d8289ccdef5e",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "2d18c971-bbc4-4475-a79c-87b61a9f54a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "eb9115ef-6a4c-4aa9-b943-79df17634d3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d18c971-bbc4-4475-a79c-87b61a9f54a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d27560cc-c5f0-45fb-843d-29b82aa9d6d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d18c971-bbc4-4475-a79c-87b61a9f54a7",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "ab975558-b9ae-4fe1-9a26-92c2e30ad5c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "f887261f-c1aa-407d-9d7e-24dc46dbbb0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab975558-b9ae-4fe1-9a26-92c2e30ad5c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3853a52-d3a4-4d69-a0ce-fd59505713b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab975558-b9ae-4fe1-9a26-92c2e30ad5c0",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "d67246a8-a4c2-4fe3-a38c-4c8faeddbef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "ad08ffee-f768-42ee-bafd-72ea69fc10bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d67246a8-a4c2-4fe3-a38c-4c8faeddbef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0e42624-bcd9-4a87-912e-186450550675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d67246a8-a4c2-4fe3-a38c-4c8faeddbef9",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "6823e694-9510-47ab-96b4-cda64bfda54b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "7ccec00a-1503-48f6-918b-75210759ceff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6823e694-9510-47ab-96b4-cda64bfda54b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba86162c-ca62-44e9-833e-12b1dd9b8152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6823e694-9510-47ab-96b4-cda64bfda54b",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "42afce1a-ff14-4563-ab14-5e8cd4b156e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "292cf2df-7a3d-4edb-9380-9fa33a14b5c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42afce1a-ff14-4563-ab14-5e8cd4b156e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14408572-6e00-4413-8fa1-380bf0264e81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42afce1a-ff14-4563-ab14-5e8cd4b156e1",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "750ef255-0192-488d-bc37-caaa4c715d75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "89233ad3-547f-4ffa-b48e-4a45e15e6808",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750ef255-0192-488d-bc37-caaa4c715d75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2a2613a-3e1e-4db0-8b1b-72fc4a6922fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750ef255-0192-488d-bc37-caaa4c715d75",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "ce374c14-8070-45b8-a1b5-8bd14cdd4701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "2a32c09c-670e-4178-ab0a-94da0098cb3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce374c14-8070-45b8-a1b5-8bd14cdd4701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e823c0c-1f63-4ad6-bc43-2719991b8d97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce374c14-8070-45b8-a1b5-8bd14cdd4701",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "6ae0f6fb-4510-4f0d-9c01-8f93c121af67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "ee3ccff9-e5c4-4ddf-a306-08a755671fbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ae0f6fb-4510-4f0d-9c01-8f93c121af67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7970fef-5ce6-44e5-aed4-fd52e276ce98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ae0f6fb-4510-4f0d-9c01-8f93c121af67",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "0940f612-6898-440b-b788-b70b0778fc98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "4f923082-127b-4ca1-afbd-f812673a0e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0940f612-6898-440b-b788-b70b0778fc98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee5fac00-3bc5-46a3-87bf-3a32668d2aa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0940f612-6898-440b-b788-b70b0778fc98",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "af9f07a7-40f6-4a18-ac3b-fe65464c2fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "a0ecec44-029f-4f19-8591-954c53c89a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af9f07a7-40f6-4a18-ac3b-fe65464c2fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51fccc30-7b07-4936-9356-f4d79010a6c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af9f07a7-40f6-4a18-ac3b-fe65464c2fb6",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "c144d754-b6b1-49d2-b2b1-58d0b6c27643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "5093643b-feca-4abe-a6c5-19ea0ebebac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c144d754-b6b1-49d2-b2b1-58d0b6c27643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df2f385f-982b-40ae-ada5-355c61c8b7f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c144d754-b6b1-49d2-b2b1-58d0b6c27643",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        },
        {
            "id": "c6d91650-3d72-4864-897b-61e4ea74556e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "compositeImage": {
                "id": "8e092b2a-31a1-4120-8164-4c538a2fcd4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d91650-3d72-4864-897b-61e4ea74556e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fcfab6d-d874-47b6-b3b1-0d5694918de4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d91650-3d72-4864-897b-61e4ea74556e",
                    "LayerId": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "39a5e0b1-bdc9-4bd7-927e-98f7f454fa9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab9f717c-f470-4942-9179-f8bd3bb9440a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 71
}