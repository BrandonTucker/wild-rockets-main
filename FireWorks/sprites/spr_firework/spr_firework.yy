{
    "id": "bd3fdac9-2296-4ce2-aa15-d354cea2783f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firework",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 18,
    "bbox_right": 25,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77a9aa5b-be4b-47be-85ff-b5c4dc082994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd3fdac9-2296-4ce2-aa15-d354cea2783f",
            "compositeImage": {
                "id": "7b605e0b-7bb4-4b1c-8b4c-0b56ffef1f71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77a9aa5b-be4b-47be-85ff-b5c4dc082994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b6048fe-946f-40d5-9669-b1c2ddad23b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77a9aa5b-be4b-47be-85ff-b5c4dc082994",
                    "LayerId": "1bf65908-91d9-4e1a-b4af-6c6044a9e93e"
                }
            ]
        },
        {
            "id": "2b5fc263-af58-42e1-b6c9-a094ecfff210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd3fdac9-2296-4ce2-aa15-d354cea2783f",
            "compositeImage": {
                "id": "2d9652bb-f8bf-430e-9241-516de58a2b4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5fc263-af58-42e1-b6c9-a094ecfff210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c3cf46-635e-4df0-8cc7-20d6f92f0d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5fc263-af58-42e1-b6c9-a094ecfff210",
                    "LayerId": "1bf65908-91d9-4e1a-b4af-6c6044a9e93e"
                }
            ]
        },
        {
            "id": "08797f43-1f1e-4d8f-b87f-1d307dd81971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd3fdac9-2296-4ce2-aa15-d354cea2783f",
            "compositeImage": {
                "id": "8bf81ba3-2314-4c21-ab5d-d3e7a6a22b99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08797f43-1f1e-4d8f-b87f-1d307dd81971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5faa37a2-8b28-40c1-8ebb-f917f35d2d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08797f43-1f1e-4d8f-b87f-1d307dd81971",
                    "LayerId": "1bf65908-91d9-4e1a-b4af-6c6044a9e93e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "1bf65908-91d9-4e1a-b4af-6c6044a9e93e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd3fdac9-2296-4ce2-aa15-d354cea2783f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 62
}