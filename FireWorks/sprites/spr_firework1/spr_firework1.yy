{
    "id": "67b47d8c-b23f-481d-9e1f-c96d24547142",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firework1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 14,
    "bbox_right": 25,
    "bbox_top": 59,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30407a1e-9a61-4dec-8455-e57a1693e991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67b47d8c-b23f-481d-9e1f-c96d24547142",
            "compositeImage": {
                "id": "e7d53f1a-8943-4ce5-8c3a-7fa5c5b56978",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30407a1e-9a61-4dec-8455-e57a1693e991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e004c134-8890-4f7a-8d06-e4238cdcae96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30407a1e-9a61-4dec-8455-e57a1693e991",
                    "LayerId": "8c84b7cd-60af-40d7-87c2-0c09dbc126a6"
                }
            ]
        },
        {
            "id": "e83d47cc-4ec6-4a14-9edf-858651d441da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67b47d8c-b23f-481d-9e1f-c96d24547142",
            "compositeImage": {
                "id": "c37d561d-d445-4941-9307-81ab7ae3a29e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e83d47cc-4ec6-4a14-9edf-858651d441da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45dd7a1f-e7eb-461b-8821-2363cd54596d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e83d47cc-4ec6-4a14-9edf-858651d441da",
                    "LayerId": "8c84b7cd-60af-40d7-87c2-0c09dbc126a6"
                }
            ]
        },
        {
            "id": "623a5c0a-b3c5-4273-8498-d5a8639dd59d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67b47d8c-b23f-481d-9e1f-c96d24547142",
            "compositeImage": {
                "id": "a7dd1a8a-fbdc-4ed3-aa33-24af4e4d416b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "623a5c0a-b3c5-4273-8498-d5a8639dd59d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d78cfa-68aa-458a-ab06-fba7e2bc4610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "623a5c0a-b3c5-4273-8498-d5a8639dd59d",
                    "LayerId": "8c84b7cd-60af-40d7-87c2-0c09dbc126a6"
                }
            ]
        },
        {
            "id": "179f64db-e0d7-4381-ace7-6ebf4c27ae57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67b47d8c-b23f-481d-9e1f-c96d24547142",
            "compositeImage": {
                "id": "88bc0a15-fd10-4ba2-ae92-563030511adc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "179f64db-e0d7-4381-ace7-6ebf4c27ae57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b3adeaa-ac7b-44fa-89ff-bf596c16bd10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "179f64db-e0d7-4381-ace7-6ebf4c27ae57",
                    "LayerId": "8c84b7cd-60af-40d7-87c2-0c09dbc126a6"
                }
            ]
        },
        {
            "id": "998d5baa-5b30-4be7-954f-7a9a45d0cc68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67b47d8c-b23f-481d-9e1f-c96d24547142",
            "compositeImage": {
                "id": "6f0ea54e-5399-42b7-b553-f2bb36f6212c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "998d5baa-5b30-4be7-954f-7a9a45d0cc68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c87e9569-46a7-4b37-8f1d-25046e7e056d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "998d5baa-5b30-4be7-954f-7a9a45d0cc68",
                    "LayerId": "8c84b7cd-60af-40d7-87c2-0c09dbc126a6"
                }
            ]
        },
        {
            "id": "c7415c72-5b57-477d-8725-60a321d2c534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67b47d8c-b23f-481d-9e1f-c96d24547142",
            "compositeImage": {
                "id": "be12e6b1-3086-4668-b1ab-f002cad4177c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7415c72-5b57-477d-8725-60a321d2c534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8635cb79-3e41-476b-b7fb-5a7a1001b41e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7415c72-5b57-477d-8725-60a321d2c534",
                    "LayerId": "8c84b7cd-60af-40d7-87c2-0c09dbc126a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "8c84b7cd-60af-40d7-87c2-0c09dbc126a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67b47d8c-b23f-481d-9e1f-c96d24547142",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 19,
    "yorig": 62
}