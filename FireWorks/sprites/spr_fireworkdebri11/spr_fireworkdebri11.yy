{
    "id": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 36,
    "bbox_right": 69,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "260735a8-f448-433c-898a-824de7aa205d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "86f650c4-beea-4f8f-84e3-6b251dd4557f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "260735a8-f448-433c-898a-824de7aa205d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a15c9ea-07ad-4425-af89-19cf07a31078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "260735a8-f448-433c-898a-824de7aa205d",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "9caaed82-6eaf-40ce-be7c-6f0faaae2566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "f6ea4b8c-84c7-44e7-8f2c-ed5ef42f686c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9caaed82-6eaf-40ce-be7c-6f0faaae2566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35e52358-0f3b-436e-b6cc-32ea72dcba91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9caaed82-6eaf-40ce-be7c-6f0faaae2566",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "e46d1d2a-4ca2-4f7b-872d-22a6aeaa15c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "9b5bc31e-25cf-4268-b49a-639bfc026b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e46d1d2a-4ca2-4f7b-872d-22a6aeaa15c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb6bae49-1cc9-4675-a88a-3c7d6231f339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e46d1d2a-4ca2-4f7b-872d-22a6aeaa15c0",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "467e905e-63db-457f-a483-ab0f6d5a04ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "1924dd7e-e906-4210-b6ca-b61e47c8335d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "467e905e-63db-457f-a483-ab0f6d5a04ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3dff711-ce01-43fa-949a-c37f816eacc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "467e905e-63db-457f-a483-ab0f6d5a04ca",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "4cedaace-eed7-42e6-8ed3-8811f38a8190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "54ff745a-9cb1-4f2c-a101-25d8605a916f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cedaace-eed7-42e6-8ed3-8811f38a8190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "947bea84-5dfe-4562-adaf-6f1434088e5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cedaace-eed7-42e6-8ed3-8811f38a8190",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "9ea1c5b0-3a37-449f-9e57-e7582e510dfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "98ae0c56-805d-4b34-b224-3dc32ff00f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ea1c5b0-3a37-449f-9e57-e7582e510dfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20423904-4b04-49a8-8489-702cb6e89ce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ea1c5b0-3a37-449f-9e57-e7582e510dfb",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "c1330e96-37c2-49e7-9fa8-db7052caec3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "633e0bf0-55c5-4f40-8e1e-daef49a5fbb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1330e96-37c2-49e7-9fa8-db7052caec3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9cea178-75c5-453d-8c24-fe74451170c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1330e96-37c2-49e7-9fa8-db7052caec3f",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "abe25b58-6c8d-45a3-81f0-abd6c0f96579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "d8f67cfb-5d35-4c05-a33d-4ff71c17de63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe25b58-6c8d-45a3-81f0-abd6c0f96579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c187bb98-1508-4bef-8430-3d9b56ffd764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe25b58-6c8d-45a3-81f0-abd6c0f96579",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "95501c09-1b57-4d07-aaf9-1fc61786dddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "dd022db0-3c4b-4ebd-b024-cdb8c3e48d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95501c09-1b57-4d07-aaf9-1fc61786dddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b166961-3e4e-445d-8b0e-8ff3ce66335b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95501c09-1b57-4d07-aaf9-1fc61786dddc",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "1fbf17f8-3011-4a02-a8cb-5c0100b9b1c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "32e2851a-53bb-4e97-840f-f7f98a00aec6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fbf17f8-3011-4a02-a8cb-5c0100b9b1c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd68902-7a77-4e56-9a9f-1ffded0d9181",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fbf17f8-3011-4a02-a8cb-5c0100b9b1c8",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "69ed1525-60a0-44a7-975c-a8d75e36d1c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "5cdea310-1b57-4ddc-9095-a2e915da1c16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69ed1525-60a0-44a7-975c-a8d75e36d1c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0efa03b1-708f-4e40-847f-6effd7eea720",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69ed1525-60a0-44a7-975c-a8d75e36d1c4",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "51e28703-0bf0-4754-bd14-a111717c5345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "e43e3390-5d88-4897-9999-8d76f0642573",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51e28703-0bf0-4754-bd14-a111717c5345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38621689-28e2-4c07-92f2-acddf017e4d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51e28703-0bf0-4754-bd14-a111717c5345",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "f97e1d52-fd21-40b9-974d-d56e5843a2ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "eaff3040-35f7-4365-975a-1040cb48561a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f97e1d52-fd21-40b9-974d-d56e5843a2ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e30d39e1-5d6b-4a8e-9fe7-5fc9ce29bc3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f97e1d52-fd21-40b9-974d-d56e5843a2ad",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "a650514f-7fdf-417a-9bf7-9348c92ba12b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "5a4fd3ef-84b1-4872-845d-15003518c3a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a650514f-7fdf-417a-9bf7-9348c92ba12b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e14c952d-b368-45ca-9580-30f0a6ba9e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a650514f-7fdf-417a-9bf7-9348c92ba12b",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "e135c4ca-40f9-44a6-83ed-05c8410ebc3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "0ba94fed-d255-4b0f-b564-d0901b0a028f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e135c4ca-40f9-44a6-83ed-05c8410ebc3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb9fd5b-5d39-42b1-b667-6773bbd07328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e135c4ca-40f9-44a6-83ed-05c8410ebc3a",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "6f7a6eb6-c634-4d9a-85ee-ff5b7dd8a351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "f15ee19e-cc93-4dd6-ab29-80cd9090d524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f7a6eb6-c634-4d9a-85ee-ff5b7dd8a351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8de22c51-7d2b-4308-9540-7345c0475dbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f7a6eb6-c634-4d9a-85ee-ff5b7dd8a351",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "1dcf34ea-4101-4df4-91ed-cc453370602b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "71dc4226-c494-4d73-9c59-65f2c9e3334c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dcf34ea-4101-4df4-91ed-cc453370602b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0067c2-724a-4380-b0e0-c7c384731688",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dcf34ea-4101-4df4-91ed-cc453370602b",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "2bb76aca-a43a-4036-a47c-fad01fd7cb45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "65255ed6-137e-4f88-818d-72aeaa34b84d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bb76aca-a43a-4036-a47c-fad01fd7cb45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf3840a1-1a4d-4eb8-950b-674cae35b817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bb76aca-a43a-4036-a47c-fad01fd7cb45",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "8950fd2b-1dba-4895-815b-5e38041ff31e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "af256ba0-1118-4b87-88e7-5e0c6f03944d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8950fd2b-1dba-4895-815b-5e38041ff31e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d65e1922-c1d6-45c7-a888-5bcf378b0a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8950fd2b-1dba-4895-815b-5e38041ff31e",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "10b85ccb-9e00-47e6-836b-dfa4f51f0654",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "31be706d-fc49-4565-bf74-33c95d4a8c7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10b85ccb-9e00-47e6-836b-dfa4f51f0654",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1204b6a8-1cec-4f7a-a7bd-19cd439e85ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10b85ccb-9e00-47e6-836b-dfa4f51f0654",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "64aa54f4-212f-4e8a-9f1f-9063ba27b482",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "3b0fe784-1b58-439c-a7f6-1579891c8816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64aa54f4-212f-4e8a-9f1f-9063ba27b482",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17dddd24-1159-4907-9f4c-4dcf5522e55c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64aa54f4-212f-4e8a-9f1f-9063ba27b482",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "48024e5f-f524-41c3-a79d-96a30003404f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "54cef370-4b2c-4ea3-af36-843d9f00e23a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48024e5f-f524-41c3-a79d-96a30003404f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b847471-446a-4c8b-b1f3-e566541fd948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48024e5f-f524-41c3-a79d-96a30003404f",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "596c19f9-e809-4882-ba3b-762fb05aad8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "2e699c10-6c0a-4101-b9eb-fb55515fdbe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "596c19f9-e809-4882-ba3b-762fb05aad8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6584b6b1-d2ff-4f3b-8c2f-1d48129a9428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "596c19f9-e809-4882-ba3b-762fb05aad8d",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "6c3db231-5b90-4a2e-a48d-a23cb3eada47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "b70d2c68-21cb-40f3-9961-411d3d08eaf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c3db231-5b90-4a2e-a48d-a23cb3eada47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1382456-a8de-4cb6-9094-bb3d703f851b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c3db231-5b90-4a2e-a48d-a23cb3eada47",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "61f2acd9-71aa-43e2-9f9e-69499094ef0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "8b579d28-4c85-4044-944f-66e6c746a56e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61f2acd9-71aa-43e2-9f9e-69499094ef0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76ea87b0-cf39-4613-b9cd-2252dab46dda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61f2acd9-71aa-43e2-9f9e-69499094ef0b",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "595334a3-9a63-48d5-903b-967b0fa51cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "6b2f6fb5-ab1a-4f64-8077-e6920ffd1faf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "595334a3-9a63-48d5-903b-967b0fa51cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20e27f6c-eb87-4ee1-b661-2e22d52eba66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "595334a3-9a63-48d5-903b-967b0fa51cda",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "1be5baf2-74fb-4c1b-99c7-95162ae58f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "78f2fa71-b2fb-48fd-8b0a-1279b3c51861",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1be5baf2-74fb-4c1b-99c7-95162ae58f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "676ae13f-a914-45a0-9bf3-b1c567438601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1be5baf2-74fb-4c1b-99c7-95162ae58f2f",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "34bb675e-ce5c-4b5a-8549-f28538c39dae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "7f944ae1-7ecd-41b3-97c4-6064c4522199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34bb675e-ce5c-4b5a-8549-f28538c39dae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec9fc913-d553-4e4f-9fd0-4d92b6cc60ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34bb675e-ce5c-4b5a-8549-f28538c39dae",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "df47ce96-d941-4028-acb4-bf7914593028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "82718808-23fd-4d30-b52f-694b32b73b4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df47ce96-d941-4028-acb4-bf7914593028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55fd12be-13d5-44fb-a24b-3120679967c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df47ce96-d941-4028-acb4-bf7914593028",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "6bfc52fc-4269-4613-bf84-980ece228390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "de5603ec-3075-40a0-957c-4285b581c7a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bfc52fc-4269-4613-bf84-980ece228390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cf176a3-b54f-435d-bd5e-45c3fcbdfeb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bfc52fc-4269-4613-bf84-980ece228390",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "92e603b2-a853-49b3-9d0c-2e4b270771fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "a3290331-c434-4ec6-8acf-6f91b6cc6041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92e603b2-a853-49b3-9d0c-2e4b270771fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07e411bc-e596-4a0d-93b2-bd939c89a92e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e603b2-a853-49b3-9d0c-2e4b270771fb",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "92b8a227-9646-40eb-84cc-f8ba865e1bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "f3de3e72-7bbe-4d4d-8ce1-1b183d9d2d4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92b8a227-9646-40eb-84cc-f8ba865e1bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e40bcd9f-9b9b-488c-bdd4-ad619abcdbf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92b8a227-9646-40eb-84cc-f8ba865e1bbc",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "7df24703-b193-40d5-8c16-2bd58927fbf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "529e610e-01f4-41f8-ae33-4ef3ef164966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7df24703-b193-40d5-8c16-2bd58927fbf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "172494c9-2f4f-4439-8715-ab35d8a8fb1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7df24703-b193-40d5-8c16-2bd58927fbf5",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "92b8dde7-50f0-4a27-8acc-5ef09da80de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "bdfefaf1-ce72-4da4-bef6-d78cc7577cdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92b8dde7-50f0-4a27-8acc-5ef09da80de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d4c95e-87b5-40b8-aadd-f560b48efa67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92b8dde7-50f0-4a27-8acc-5ef09da80de9",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "7e030058-09c2-454d-a79e-96d82b767ec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "ff35da91-7174-454e-a91c-662dde92e942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e030058-09c2-454d-a79e-96d82b767ec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9deb1544-2979-4531-ab59-6934165983fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e030058-09c2-454d-a79e-96d82b767ec3",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        },
        {
            "id": "8538cf2d-5db3-4544-b7c7-33719f7bbb25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "compositeImage": {
                "id": "e2b2c3be-a159-4b8a-8f39-233423bd8aed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8538cf2d-5db3-4544-b7c7-33719f7bbb25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21305261-1a75-47b4-99e7-fccbc67a3b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8538cf2d-5db3-4544-b7c7-33719f7bbb25",
                    "LayerId": "1068bcf2-df98-4b54-967d-8ff9caa15e3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "1068bcf2-df98-4b54-967d-8ff9caa15e3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "646cca12-b281-4f08-815e-9eb5c4f5e1ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 52,
    "yorig": 55
}