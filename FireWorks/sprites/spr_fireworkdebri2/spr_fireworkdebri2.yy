{
    "id": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 40,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fa62862-d0dd-4a6c-99f5-1bb061c66d32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "bf02c249-62c1-4a3a-9620-d7366c02dda9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fa62862-d0dd-4a6c-99f5-1bb061c66d32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbc07c56-b249-4e77-8cee-208365c77a2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fa62862-d0dd-4a6c-99f5-1bb061c66d32",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "148f3c64-0f43-4f4c-8bfd-e69c7505c48d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "a30821fb-12ff-41eb-9c56-ea9af2423613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "148f3c64-0f43-4f4c-8bfd-e69c7505c48d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc62c895-c25f-43c1-9fc5-c9909e16df0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "148f3c64-0f43-4f4c-8bfd-e69c7505c48d",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "528a8f3a-49d5-4885-9bbd-9364edb1e8f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "39682f18-e411-4162-8301-1e026a10b252",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "528a8f3a-49d5-4885-9bbd-9364edb1e8f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90c304bd-b7f1-4769-987a-d6fb1567d103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "528a8f3a-49d5-4885-9bbd-9364edb1e8f0",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "cbb62ff4-9a2b-4f63-9e8b-44a14d77b364",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "6e0b9e65-f7d1-4f5f-8ebe-b6a36ec34d9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbb62ff4-9a2b-4f63-9e8b-44a14d77b364",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42600c46-4fd3-4758-994e-c01e12608bca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbb62ff4-9a2b-4f63-9e8b-44a14d77b364",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "aeb0e16e-293a-41bd-946b-836f73c5b970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "f9973634-d08c-42a2-8553-eaaa57b0488b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeb0e16e-293a-41bd-946b-836f73c5b970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8b47cae-6e3d-4c69-b2c2-8098225be05a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeb0e16e-293a-41bd-946b-836f73c5b970",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "0562a425-0e4e-4d89-831a-522a10c4159f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "6248912a-8401-4d69-8aff-fee01da1f91c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0562a425-0e4e-4d89-831a-522a10c4159f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "970e8fad-5fc8-4cb5-bcf3-581097913212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0562a425-0e4e-4d89-831a-522a10c4159f",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "2f202dcd-32de-4301-b6a0-580fe6a87db3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "2a844fc3-8764-4ecb-8648-41e0dae84244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f202dcd-32de-4301-b6a0-580fe6a87db3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c667ee3-9b8e-4509-a7fd-50531f837a40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f202dcd-32de-4301-b6a0-580fe6a87db3",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "5f4c990c-ab52-4e34-81ae-3786f4d9c8f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "84cdd5ed-bfe4-4835-941f-48327dd5d858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f4c990c-ab52-4e34-81ae-3786f4d9c8f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10b07a6f-91ed-45c9-94b1-d79002b0578f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f4c990c-ab52-4e34-81ae-3786f4d9c8f6",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "36231b28-4fbe-43b8-acdd-3ce43e36c13f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "a9087ba0-1fee-4056-93b2-ccece670cd65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36231b28-4fbe-43b8-acdd-3ce43e36c13f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adec0c3d-de17-4350-a550-73aa4ad3d824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36231b28-4fbe-43b8-acdd-3ce43e36c13f",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "d05e912a-cdbf-4eb7-b626-94b58bf6d983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "94dc82c3-ab60-43c3-8945-a9128e20f965",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d05e912a-cdbf-4eb7-b626-94b58bf6d983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edd0c99b-68cc-436c-8316-de25c0489024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d05e912a-cdbf-4eb7-b626-94b58bf6d983",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "fd3cfd67-cf0b-4fb7-bad0-44cedbfbc20b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "6a3ad28b-7e2d-4a69-9177-7c626de7ebed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd3cfd67-cf0b-4fb7-bad0-44cedbfbc20b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "832c259c-3b9c-4cfc-a5ff-74cf7e372f6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd3cfd67-cf0b-4fb7-bad0-44cedbfbc20b",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "39f35c4c-dffb-4e4e-a511-725d5310b30c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "fe5b941e-18b9-42b8-8211-0f0e85f8821b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39f35c4c-dffb-4e4e-a511-725d5310b30c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8d68afe-a1a0-434d-a0ad-9eb65dce2c58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39f35c4c-dffb-4e4e-a511-725d5310b30c",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "ef6050ec-26d3-4efe-9a38-ae880e9285b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "e2320bc4-9259-4f0e-a823-bf39e585af70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef6050ec-26d3-4efe-9a38-ae880e9285b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c120b55-ca06-4a14-8c51-b7440c68210f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef6050ec-26d3-4efe-9a38-ae880e9285b6",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "2c4a130b-edb5-4a27-a0cb-57625dfd33ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "3b0e3e14-a0f0-4fb0-ac1e-91a6a897b455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c4a130b-edb5-4a27-a0cb-57625dfd33ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b09e24b-3a86-4247-a117-e51e510df932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c4a130b-edb5-4a27-a0cb-57625dfd33ab",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "bd4437ed-12b1-4540-b7fc-b0ea93a05396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "0ff29b15-61bb-46a9-9a64-02fbcdb9d210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd4437ed-12b1-4540-b7fc-b0ea93a05396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff17a03-0bc7-427a-bfeb-7884233f2d1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd4437ed-12b1-4540-b7fc-b0ea93a05396",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        },
        {
            "id": "f6d8938d-cc5d-476f-bc00-d3321c50c735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "compositeImage": {
                "id": "73a4ff82-4740-47ae-96c0-3d76c4d513f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d8938d-cc5d-476f-bc00-d3321c50c735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58019178-e090-4881-9714-109d9c2bb6fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d8938d-cc5d-476f-bc00-d3321c50c735",
                    "LayerId": "14778abd-0ed6-4f03-898d-49839b935990"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "14778abd-0ed6-4f03-898d-49839b935990",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68ccd1d6-3c01-4e4c-b714-9aad5b351a32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 56
}