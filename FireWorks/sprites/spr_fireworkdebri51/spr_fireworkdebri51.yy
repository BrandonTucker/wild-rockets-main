{
    "id": "034c9c75-9312-4eee-b2b2-390f434275c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri51",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 40,
    "bbox_right": 64,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e2b0cd4-f24e-43d7-ad9d-b72c58ae15d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "f14f2e67-f914-4e77-bebf-9be4352fa78f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e2b0cd4-f24e-43d7-ad9d-b72c58ae15d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49b9cec3-a1fa-493b-b311-ffbc25bff37f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e2b0cd4-f24e-43d7-ad9d-b72c58ae15d7",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "fc64cbb3-907e-4dc2-a01f-bb95b016cd14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "70aca4a0-6a0d-435e-a193-1fdc6cc8369c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc64cbb3-907e-4dc2-a01f-bb95b016cd14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c55e045c-ab69-42c0-b015-fcf4ebbfd8af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc64cbb3-907e-4dc2-a01f-bb95b016cd14",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "edf381a3-8658-4426-8207-41da0083103e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "6c0428dc-c73c-4ce9-891d-f2dffe4b6c94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edf381a3-8658-4426-8207-41da0083103e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd19a644-75dd-46b4-8f54-34ec88ad6def",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf381a3-8658-4426-8207-41da0083103e",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "5f3715c3-0261-45cd-8b3b-e6a0b34ce08a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "8e068bd7-bd41-4a2f-b201-73c0b0240560",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f3715c3-0261-45cd-8b3b-e6a0b34ce08a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "061ce6fb-c0d3-4213-b81e-07cd3526f33c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f3715c3-0261-45cd-8b3b-e6a0b34ce08a",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "7d7e97e0-2e75-4b45-82c6-6d211f8b29a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "0a87b640-bbee-4eba-8758-948d31d5d0e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d7e97e0-2e75-4b45-82c6-6d211f8b29a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a9f5d83-41ed-4f86-9aa0-fdb09901ba7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d7e97e0-2e75-4b45-82c6-6d211f8b29a2",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "c5a71a9d-f69a-4dee-ade6-3bfd81170aa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "28c093b4-d324-4560-9ef3-f5e163e51606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a71a9d-f69a-4dee-ade6-3bfd81170aa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3de294-d40c-436b-803e-ae2db463712e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a71a9d-f69a-4dee-ade6-3bfd81170aa3",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "a79be419-bf10-4732-bb75-4e724e3a1773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "cd8c30e5-9cec-41a7-b233-21e0d478fb1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a79be419-bf10-4732-bb75-4e724e3a1773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05df2b72-4c73-42c7-93b0-bb0bf51ed1a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79be419-bf10-4732-bb75-4e724e3a1773",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "2ce4dea1-90e0-4f5c-abd0-432c8bf03d47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "052498e5-8fd2-4aff-94ff-0ba8a7a55a1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce4dea1-90e0-4f5c-abd0-432c8bf03d47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42ee4db-06b4-46cd-bb0e-503ae7d0c31b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce4dea1-90e0-4f5c-abd0-432c8bf03d47",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "0ebe4f08-0be9-418b-9d91-3f46f7cecdcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "32943bc4-fa20-44e8-92a4-7b3e4abeec6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ebe4f08-0be9-418b-9d91-3f46f7cecdcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a4e4bdd-f861-48f4-ad70-1bf5d4253171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ebe4f08-0be9-418b-9d91-3f46f7cecdcf",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "a9c39e22-e605-4361-b944-e9f896a2056f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "9bd42738-fb26-45b8-80f9-ca37040697ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9c39e22-e605-4361-b944-e9f896a2056f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e262ee82-1cbc-4316-b1c4-79735ae62a08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9c39e22-e605-4361-b944-e9f896a2056f",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "1125425c-6e22-45f3-a58a-09127446b8dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "f1d96241-2db4-41ab-a5de-1f51ee22a0c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1125425c-6e22-45f3-a58a-09127446b8dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffe07029-42d6-4ba3-af52-ba3a2969e81c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1125425c-6e22-45f3-a58a-09127446b8dc",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "2fad3ff2-862a-4795-9d94-7471d570b619",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "1cbde9aa-0825-4118-9895-e291888af82d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fad3ff2-862a-4795-9d94-7471d570b619",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87c825f1-92f0-47b7-93a4-600530551cca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fad3ff2-862a-4795-9d94-7471d570b619",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "32de8c3c-010a-4f5c-9b87-544ea678d758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "bc925485-3d2e-481d-a745-23c12f8c9d82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32de8c3c-010a-4f5c-9b87-544ea678d758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "549e4d8e-0e6f-45af-a722-c41d5f9adae9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32de8c3c-010a-4f5c-9b87-544ea678d758",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "21776807-a37e-49fc-a028-105a76168055",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "2b1e6500-f8bd-4f68-82c8-09e596a2d5cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21776807-a37e-49fc-a028-105a76168055",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7f65327-07d4-4c53-90ea-86beb70e0322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21776807-a37e-49fc-a028-105a76168055",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "f4986127-5b53-4239-83f6-adec54690fa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "0841c1ce-7916-4d46-9b97-c61fff37ce41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4986127-5b53-4239-83f6-adec54690fa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "108f5bc4-aeff-4896-a001-ca7be7bc834d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4986127-5b53-4239-83f6-adec54690fa8",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "df69bb92-052a-4775-8534-5143438f2c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "cdc17fe0-abb2-4a40-8189-13c3f7931c1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df69bb92-052a-4775-8534-5143438f2c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c71c473-d814-44f6-874b-838248116419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df69bb92-052a-4775-8534-5143438f2c04",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "87d1b464-a1c8-410d-8bb1-861433e9df28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "21da75f5-e591-40a9-9372-c6c7598b7437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87d1b464-a1c8-410d-8bb1-861433e9df28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7674571e-a204-4424-946d-cdda44f25253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87d1b464-a1c8-410d-8bb1-861433e9df28",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "04b547cc-551c-4445-b70b-97b34842e0e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "4b76d804-2857-44aa-9c46-6d3f1c38beb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04b547cc-551c-4445-b70b-97b34842e0e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "410c3f11-d9df-4c27-a804-e24a8d7f36a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04b547cc-551c-4445-b70b-97b34842e0e0",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "e73dece9-7828-48cf-bb3a-f0fdda22bb95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "32d9269d-7e6e-4ab7-a8cf-3692d47ea0ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e73dece9-7828-48cf-bb3a-f0fdda22bb95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c8c6447-262c-4973-aeed-d92e5ff14870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e73dece9-7828-48cf-bb3a-f0fdda22bb95",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "79414c03-c3fc-4e20-8593-7c7c9b2a05b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "0b3ba741-1746-4f49-9ba4-2d2697cd9160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79414c03-c3fc-4e20-8593-7c7c9b2a05b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9bf1df8-ff99-4308-9808-a9b1786bf7b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79414c03-c3fc-4e20-8593-7c7c9b2a05b5",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "b6e20795-e0ee-4f56-9e91-efcf166aec92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "0ec01c0c-748a-49af-aef2-1535c6f608a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e20795-e0ee-4f56-9e91-efcf166aec92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "418f0c2d-2097-405b-a4f5-72eecb646363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e20795-e0ee-4f56-9e91-efcf166aec92",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "b629a9cd-566d-4590-985a-f05d7422f94f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "bdb685e8-e347-4915-9e76-7cb59cc692fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b629a9cd-566d-4590-985a-f05d7422f94f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fcc3aa0-c5d7-4188-a75c-69e51204281f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b629a9cd-566d-4590-985a-f05d7422f94f",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "bde3e58b-fa74-467a-9ad2-fc297086a8df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "b871e8dd-38a6-4e0e-b450-a19a4edc4fb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde3e58b-fa74-467a-9ad2-fc297086a8df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdad39e6-d3a5-466e-bf4e-4fd89349813a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde3e58b-fa74-467a-9ad2-fc297086a8df",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "eeb6f61a-ad1e-4cb8-9133-cf0ade9263bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "16fc28ed-fdab-4044-bdd9-7e9e91d20b5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeb6f61a-ad1e-4cb8-9133-cf0ade9263bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0c5272a-0c71-44e8-9df1-76cb023dcc93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeb6f61a-ad1e-4cb8-9133-cf0ade9263bf",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "b4578ec7-14a3-4641-89a2-d714f5c32206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "f20d5272-d72d-4409-8ae7-be3bbf876719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4578ec7-14a3-4641-89a2-d714f5c32206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0053e33f-9322-48cb-b219-5d22b2633406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4578ec7-14a3-4641-89a2-d714f5c32206",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "88c5d89f-42a7-45ca-8b5c-2675e09c762f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "08276017-14db-49e0-8645-bc422d9dd368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88c5d89f-42a7-45ca-8b5c-2675e09c762f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a38cc175-1d19-4127-bc7c-ae9ea1e4c0ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88c5d89f-42a7-45ca-8b5c-2675e09c762f",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "9fa86c49-28df-49ea-a973-aa234271a629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "f8bda0bb-995c-4019-9df5-cb7df23a1491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fa86c49-28df-49ea-a973-aa234271a629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fd1594e-cb6c-454f-a649-4eb04697b8af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fa86c49-28df-49ea-a973-aa234271a629",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "e9276553-ceca-4129-b5b0-d3b302330143",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "8a5b7ad7-b14b-4588-8a6d-c1382c875c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9276553-ceca-4129-b5b0-d3b302330143",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c4dc137-c79d-4d36-a8ff-c4705c25908b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9276553-ceca-4129-b5b0-d3b302330143",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "8dd7b50f-5b43-434d-b97c-33e0d2e6f3f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "a6c0bf5e-3cf2-4c48-a086-7a625244f834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dd7b50f-5b43-434d-b97c-33e0d2e6f3f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d3878f-a4ea-4af3-a973-98577830f14b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dd7b50f-5b43-434d-b97c-33e0d2e6f3f1",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "10d0fd4a-149e-426d-b7d4-fad091afb82c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "dcebc055-7527-4e75-b2f6-2962dd9f6468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d0fd4a-149e-426d-b7d4-fad091afb82c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30c27042-4d52-46c0-831f-9a0a51f5eb1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d0fd4a-149e-426d-b7d4-fad091afb82c",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "6bad9e29-bf58-4414-bd55-c37c0ec0bd36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "65a87267-1c87-4826-b7ce-8401b6edfd97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bad9e29-bf58-4414-bd55-c37c0ec0bd36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb6a4901-5775-4ecd-b4cd-dccdd67930ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bad9e29-bf58-4414-bd55-c37c0ec0bd36",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "71bcd4f0-9c48-481e-8627-2f46d771a3d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "2713c7de-9c38-437a-a179-a41bb81e451f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71bcd4f0-9c48-481e-8627-2f46d771a3d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbaf2ff8-49c9-4a17-b514-1dad44a01467",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71bcd4f0-9c48-481e-8627-2f46d771a3d5",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "f7be83a9-fe13-418f-9c51-e0d87c277edc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "c1190dcc-37a9-4630-ba06-d19c68fdb16b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7be83a9-fe13-418f-9c51-e0d87c277edc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9176c647-b62f-4722-beb8-135e9d0363f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7be83a9-fe13-418f-9c51-e0d87c277edc",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "0e8f30b7-3a2d-47ef-a585-51fe020c5e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "02da2b5f-b67f-4e78-8d2a-bc5e2665a041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e8f30b7-3a2d-47ef-a585-51fe020c5e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12084f3d-684e-4f0b-a955-0789d383bd55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e8f30b7-3a2d-47ef-a585-51fe020c5e65",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "7802a0f5-87ee-46b4-804f-1d73413ff777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "39d6481b-8ee1-453a-8bb0-05b36c14e31a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7802a0f5-87ee-46b4-804f-1d73413ff777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9a154f-28a3-4000-b845-21117451c320",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7802a0f5-87ee-46b4-804f-1d73413ff777",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        },
        {
            "id": "a867cb69-052a-4236-a7d7-45eb03f6449a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "compositeImage": {
                "id": "c45346e1-55db-4534-9e26-d692b40d70b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a867cb69-052a-4236-a7d7-45eb03f6449a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9185c9f-216f-4022-acf3-1fd988d308a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a867cb69-052a-4236-a7d7-45eb03f6449a",
                    "LayerId": "dfb724f7-40dc-4aec-b7d8-d82d9148d394"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "dfb724f7-40dc-4aec-b7d8-d82d9148d394",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "034c9c75-9312-4eee-b2b2-390f434275c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 52,
    "yorig": 48
}