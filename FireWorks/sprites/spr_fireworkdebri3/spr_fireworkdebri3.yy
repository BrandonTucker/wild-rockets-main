{
    "id": "38b39598-3840-45b5-bc5d-c55c195849a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 44,
    "bbox_right": 55,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1d4fb87-2bce-4d06-a7da-1195d2574a95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "a4e08919-5b75-42f5-803f-3eba42ce48a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1d4fb87-2bce-4d06-a7da-1195d2574a95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04962836-0829-4edf-9c30-10b786df45ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1d4fb87-2bce-4d06-a7da-1195d2574a95",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "1588d3df-885e-4794-a518-f038a19103cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "d8dc8038-ee0e-4a5a-a181-cd02785532d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1588d3df-885e-4794-a518-f038a19103cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ff3c138-75f9-4da4-80da-c5619388a6f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1588d3df-885e-4794-a518-f038a19103cb",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "ce533b0b-9c26-4089-9ed3-510e93950e63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "13c366d8-d672-4c52-b642-1b838e143327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce533b0b-9c26-4089-9ed3-510e93950e63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f91aa20-c43c-4500-806e-5d94fdf393fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce533b0b-9c26-4089-9ed3-510e93950e63",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "92ad5d35-a705-41fa-82c8-6d007e1d3c9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "32f55568-6a61-49ce-a304-df9edc11fa89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92ad5d35-a705-41fa-82c8-6d007e1d3c9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bb0c35b-16af-4ec9-afec-ed8d63394ca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92ad5d35-a705-41fa-82c8-6d007e1d3c9d",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "0da7b39b-0b23-4d95-875d-c18f4d149c16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "f25d5957-04e2-4dbd-8b47-da1c676be132",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0da7b39b-0b23-4d95-875d-c18f4d149c16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5969f766-72cd-4a91-9aa8-e782aab4ad8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0da7b39b-0b23-4d95-875d-c18f4d149c16",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "b9b2d00e-6748-4cee-8a25-d0938a2cb802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "6802ff10-15b7-49a5-982d-302d7c8404be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9b2d00e-6748-4cee-8a25-d0938a2cb802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e84baf32-e8ce-4acf-8b45-e64325a5762f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b2d00e-6748-4cee-8a25-d0938a2cb802",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "b5860cf5-67af-4ad6-b76d-4eed8cd9ce94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "d255211c-c85b-4fbc-ac1e-2e9a7fca22a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5860cf5-67af-4ad6-b76d-4eed8cd9ce94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "799bc3f2-06e6-42bd-98a8-13912fd22b9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5860cf5-67af-4ad6-b76d-4eed8cd9ce94",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "b54550b1-cf0a-4fd6-a304-abd8ea349c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "96f38493-7f03-466f-817b-457c4c931a1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b54550b1-cf0a-4fd6-a304-abd8ea349c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c01bcb7b-2bc8-43b2-91e7-bccdc36bd274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b54550b1-cf0a-4fd6-a304-abd8ea349c4e",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "a0037634-aabc-42e1-be7e-5b96e946009a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "f282170e-4bad-4bea-b43f-8700bd31e6c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0037634-aabc-42e1-be7e-5b96e946009a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ca5fa62-03eb-49e2-83e0-28649d5de5a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0037634-aabc-42e1-be7e-5b96e946009a",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "5993b598-7c3a-42fa-b1e4-a52433092536",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "06cecab1-9157-406d-bae7-52b679285a1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5993b598-7c3a-42fa-b1e4-a52433092536",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ea44d2b-39b6-4892-84b2-54faae540412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5993b598-7c3a-42fa-b1e4-a52433092536",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "dce5c9d8-e6b0-437c-99d3-6283dbd646f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "1cb604b1-2897-45a8-8c43-14732eb8b8ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce5c9d8-e6b0-437c-99d3-6283dbd646f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06369ffe-7ec2-43ae-8c0c-95703c235801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce5c9d8-e6b0-437c-99d3-6283dbd646f1",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "d744a662-2ce9-4199-a116-2dfd2de5b070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "3bf7f453-307c-4a36-b8af-cbe51d2ea6f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d744a662-2ce9-4199-a116-2dfd2de5b070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac8066b4-39e7-40bd-baf4-a96384120816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d744a662-2ce9-4199-a116-2dfd2de5b070",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "874d5d76-6761-4293-8ad5-117d704d42e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "3326d403-4cf9-4244-9e36-86a7799244f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "874d5d76-6761-4293-8ad5-117d704d42e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d360c5e-e592-4e7c-b7bd-ebea3639cb10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "874d5d76-6761-4293-8ad5-117d704d42e7",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "1b89f789-f9ca-4075-a5d8-b2d718b86474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "f52eac5d-32d9-4b3f-a9b1-8523a58a2f67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b89f789-f9ca-4075-a5d8-b2d718b86474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9d3f3b-33b7-4cab-9430-4c39e4839acf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b89f789-f9ca-4075-a5d8-b2d718b86474",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "2a51e3cb-e94a-46b7-8be6-5ca80b9d3098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "bc29ede8-1d78-4feb-bb60-db5fa7ade209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a51e3cb-e94a-46b7-8be6-5ca80b9d3098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6fe7b22-084b-4483-a03a-cc72da5df6d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a51e3cb-e94a-46b7-8be6-5ca80b9d3098",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "18d6d20c-e21e-440f-871b-22ba7aed53ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "5b7714f3-e375-49af-8d85-3e6053bb0731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18d6d20c-e21e-440f-871b-22ba7aed53ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5d61c25-aded-4f58-bd4c-6fda279fd1f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18d6d20c-e21e-440f-871b-22ba7aed53ee",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "c244827e-e475-4095-9e88-a2957eba5b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "d257f565-cddd-4516-8d80-f3e30f7e12bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c244827e-e475-4095-9e88-a2957eba5b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6330f135-3f5b-4845-90a5-1fcbc77c3248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c244827e-e475-4095-9e88-a2957eba5b1c",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "f76e5064-05d7-4478-9218-518fa6e80ac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "3ad949e2-5c21-45b5-827a-d0f921323f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76e5064-05d7-4478-9218-518fa6e80ac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d87cbd3-a63d-4e3f-83bf-c74c53e88f41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76e5064-05d7-4478-9218-518fa6e80ac1",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "520bda5c-ad9b-4258-918e-08f099bd8aef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "7ab64ad3-84d1-46a3-83fc-6e076ffeb650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "520bda5c-ad9b-4258-918e-08f099bd8aef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e5e95fa-88e4-49a9-b4f3-f3bca9297c7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "520bda5c-ad9b-4258-918e-08f099bd8aef",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "913eb6e7-baa0-44ed-b4d0-083904b2e6c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "ed8b5615-bdd2-4701-86e3-cd8d7e504879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "913eb6e7-baa0-44ed-b4d0-083904b2e6c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a66a14a8-e4e3-49aa-b887-9ce1a52bbeee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "913eb6e7-baa0-44ed-b4d0-083904b2e6c7",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "1237fddb-4c83-4fa1-bc28-2dc8876ff13d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "1191d4e1-dcbb-4bc0-96ad-5d9fa9cfa89f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1237fddb-4c83-4fa1-bc28-2dc8876ff13d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "811ad8a6-a702-4755-98a2-df2d1dbc0a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1237fddb-4c83-4fa1-bc28-2dc8876ff13d",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        },
        {
            "id": "7440ffef-55ba-413e-abbb-137b86c21f6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "compositeImage": {
                "id": "4b117110-427d-4631-94b8-f5e1dd64bb32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7440ffef-55ba-413e-abbb-137b86c21f6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f4421ee-0317-4d86-b7fc-5967d21fa5f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7440ffef-55ba-413e-abbb-137b86c21f6b",
                    "LayerId": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "7e683a31-38aa-4fa6-b3fc-8249c12a83fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38b39598-3840-45b5-bc5d-c55c195849a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 56
}