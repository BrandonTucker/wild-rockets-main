{
    "id": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 36,
    "bbox_right": 69,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "754d939f-d25e-4834-97f1-2d664e4b797e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "c350074e-a45d-48bf-9661-513921948515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "754d939f-d25e-4834-97f1-2d664e4b797e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49bb2bed-eff7-49fa-a1a9-a8bab9f40a83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "754d939f-d25e-4834-97f1-2d664e4b797e",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "449451ca-b636-4ea2-b64b-80a4ca7b43f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "7e44908f-ec7a-4013-8c69-8e00bb3f6c6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "449451ca-b636-4ea2-b64b-80a4ca7b43f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a8a50e6-281d-41ff-b7c3-40b00f5f1bdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "449451ca-b636-4ea2-b64b-80a4ca7b43f9",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "29a1a30f-fab5-4956-a3aa-c83dbf6ac704",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "87a584ad-710a-46d3-9583-341e1974856e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29a1a30f-fab5-4956-a3aa-c83dbf6ac704",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35580d50-74f4-4708-9b08-b31332baa1da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29a1a30f-fab5-4956-a3aa-c83dbf6ac704",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "b05289fb-e945-4c7a-8d63-e99543b05cf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "ae0f9008-a6aa-4333-9a4f-a41e2e5a963f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b05289fb-e945-4c7a-8d63-e99543b05cf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8623bc39-3442-46ef-8a43-23fedbf479fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b05289fb-e945-4c7a-8d63-e99543b05cf7",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "3b6ece87-b343-4cfe-a888-c5628dcc4a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "dff09a2a-6478-41b4-aa46-146c0056d11e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b6ece87-b343-4cfe-a888-c5628dcc4a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a7e6cf1-df44-40d6-b444-157be4ec3293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b6ece87-b343-4cfe-a888-c5628dcc4a2a",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "36353612-90e4-4b8d-be73-c24c190c6efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "39042ef9-2f85-4f78-bc7c-fd408075a4ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36353612-90e4-4b8d-be73-c24c190c6efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfd38c7a-24a8-468b-a533-413d7a759208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36353612-90e4-4b8d-be73-c24c190c6efd",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "48b9bc67-4c0c-4e08-8616-cf7f7bd5d026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "f8b556e1-4dba-4583-b419-9a5a0679a265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48b9bc67-4c0c-4e08-8616-cf7f7bd5d026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d08d845-4a83-460d-bcb3-353a8da2a53a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b9bc67-4c0c-4e08-8616-cf7f7bd5d026",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "08d1e56e-b344-421f-901e-9c1761125ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "942de69c-89b4-4103-83c3-63a8fef367db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08d1e56e-b344-421f-901e-9c1761125ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbe95cb8-e4f9-4dc6-b9be-83d4f291bed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08d1e56e-b344-421f-901e-9c1761125ee3",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "4169f665-04ee-4ef0-b0a3-409d7e0df4fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "c1cba1af-a43f-4ffd-8ea0-09b3767f5b04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4169f665-04ee-4ef0-b0a3-409d7e0df4fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d583321-0e32-44d7-a749-4a8b21534417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4169f665-04ee-4ef0-b0a3-409d7e0df4fd",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "228236e6-4639-468d-b0ac-c56586319209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "7f2ab934-d1d0-4d4d-b667-8067574995f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228236e6-4639-468d-b0ac-c56586319209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbc51713-c096-4335-bc71-3987155b1a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228236e6-4639-468d-b0ac-c56586319209",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "7016c166-b883-4a4b-ae2b-9e1f8c4cf3f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "fab7b505-bdb9-4e29-ae85-805198a2052f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7016c166-b883-4a4b-ae2b-9e1f8c4cf3f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00af0ab8-345d-4dfd-acac-aebae6ec0657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7016c166-b883-4a4b-ae2b-9e1f8c4cf3f5",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "0c826c30-d3e9-4ca2-905e-3cf17498f642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "0a7d5ce9-7201-4cfc-a6fc-9d79cda31e74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c826c30-d3e9-4ca2-905e-3cf17498f642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26a134c0-3298-4f94-8880-579055d14e9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c826c30-d3e9-4ca2-905e-3cf17498f642",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "cf8ab3cf-0747-47bd-9ff2-0b188e59df7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "804c7966-2cf9-42f7-b17d-065d9d7372c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf8ab3cf-0747-47bd-9ff2-0b188e59df7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23acb09b-4cec-45fa-a243-0ee09639e1fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf8ab3cf-0747-47bd-9ff2-0b188e59df7d",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "38dd3687-1b51-46a7-8ab7-4852bc62997c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "795150e1-4d30-4529-bcd8-1f714adcbd6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38dd3687-1b51-46a7-8ab7-4852bc62997c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f8f858-8bbe-415a-8e2c-28bf99925a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38dd3687-1b51-46a7-8ab7-4852bc62997c",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "3856b4f6-ed30-40db-ad66-3f24355d875e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "39792618-f5b2-4c08-be41-846b59bd5969",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3856b4f6-ed30-40db-ad66-3f24355d875e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b89f33db-133c-4968-821b-7f6b47f02255",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3856b4f6-ed30-40db-ad66-3f24355d875e",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "895f961b-31c7-458b-a6c1-04acedc71195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "b2899702-f3ff-485f-b838-33523083aee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "895f961b-31c7-458b-a6c1-04acedc71195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "662a265b-b292-46d9-8a76-0808698b4e8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895f961b-31c7-458b-a6c1-04acedc71195",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "34b79172-7100-4aff-8c7d-6744bd24f558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "787f8b86-b467-4506-920f-4db23eedf729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34b79172-7100-4aff-8c7d-6744bd24f558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b574fbe-ea6d-486c-9a2a-ae4b9c727620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34b79172-7100-4aff-8c7d-6744bd24f558",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "ff3cbc78-9dfc-421b-91e4-1907a75127f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "838faf57-42c1-46d8-b851-1bddbfa34a5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff3cbc78-9dfc-421b-91e4-1907a75127f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a622065f-0699-43b4-817c-08a6418712e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff3cbc78-9dfc-421b-91e4-1907a75127f6",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "32021900-d025-452e-b9a5-329fd36941a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "923c5101-20bb-45bf-b737-9dc30f705395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32021900-d025-452e-b9a5-329fd36941a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6b69ab7-6a92-46ca-9a10-5885b66a7e2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32021900-d025-452e-b9a5-329fd36941a3",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "e9f2656b-8702-46b6-ac93-c17d5234dfe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "ccaf3db4-fc98-461c-88bb-93bbdbd9eebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f2656b-8702-46b6-ac93-c17d5234dfe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5320eaca-2c8c-41f2-8cf8-3aa5599c9d2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f2656b-8702-46b6-ac93-c17d5234dfe1",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "b3510959-996e-4746-80b5-2ccddf28e284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "081dc6a8-2885-4e94-babe-6f08453326b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3510959-996e-4746-80b5-2ccddf28e284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe7e9a37-0f7b-40c0-a57e-b63cc04f1771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3510959-996e-4746-80b5-2ccddf28e284",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "4a7c6a58-c97f-4fec-8e2b-1fa3bb69894d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "a064ccb4-0684-45dc-8209-e2a6fa1798ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a7c6a58-c97f-4fec-8e2b-1fa3bb69894d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc602e3-5448-47c6-8a05-050be4594f65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a7c6a58-c97f-4fec-8e2b-1fa3bb69894d",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "6cc8cf1f-0cc7-4f1f-8890-e4beb3e26510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "82aa99c3-0538-4285-bf51-1c92054bce09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cc8cf1f-0cc7-4f1f-8890-e4beb3e26510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec7b43c5-6742-456c-97d5-c8ca9a276ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cc8cf1f-0cc7-4f1f-8890-e4beb3e26510",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "bce6fc7f-b078-4446-882a-92d51a43b93f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "088e3f0c-8291-4b93-a351-e8d442d57c41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bce6fc7f-b078-4446-882a-92d51a43b93f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19db98f2-eb75-4e5d-bc8f-d4f92179b76f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bce6fc7f-b078-4446-882a-92d51a43b93f",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "3416cb87-4660-42cf-bf92-e7d7d7555a90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "bcd46f61-e2fe-48fd-bafd-b62e510146a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3416cb87-4660-42cf-bf92-e7d7d7555a90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e2dbdd0-4e07-48b3-94c8-40eab68d2197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3416cb87-4660-42cf-bf92-e7d7d7555a90",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "92c578fe-2cdc-4007-9d19-a1b56606b878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "b27c1765-d9b8-4419-acc6-3cd3458de125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92c578fe-2cdc-4007-9d19-a1b56606b878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be97bc0-5b87-45f7-aea7-02dcf26a0353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92c578fe-2cdc-4007-9d19-a1b56606b878",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        },
        {
            "id": "321f2c3f-07fe-4b85-aab6-d3c7d2e31ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "compositeImage": {
                "id": "e16eb5f5-6387-4ade-9b13-eb5322b45aee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "321f2c3f-07fe-4b85-aab6-d3c7d2e31ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aaefcef-40e7-49db-b5d5-9ab0fc8fc421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "321f2c3f-07fe-4b85-aab6-d3c7d2e31ee8",
                    "LayerId": "a16be56d-cb20-4a60-a701-a36a6151de11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "a16be56d-cb20-4a60-a701-a36a6151de11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "386a0ff3-3e7f-48f9-99f5-38a8ed9dfd4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 70
}