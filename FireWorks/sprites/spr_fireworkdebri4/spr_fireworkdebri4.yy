{
    "id": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 44,
    "bbox_right": 55,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae0c256d-6888-4a3e-9c17-ad3474a12895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "2ff209cd-1f26-4298-a986-616df2bec011",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae0c256d-6888-4a3e-9c17-ad3474a12895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed63a32b-d85f-4a5d-b5e5-443721de4325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae0c256d-6888-4a3e-9c17-ad3474a12895",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "5c65d2c5-dfe1-4a50-8c03-e418f898630e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "a8b2baa8-e51c-4d74-883e-6229609daa64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c65d2c5-dfe1-4a50-8c03-e418f898630e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20e90eba-4463-443a-af0f-2099058383ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c65d2c5-dfe1-4a50-8c03-e418f898630e",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "674d8cb0-4005-42f5-adbc-8f4ab04a948f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "327728ab-2e81-4803-b29b-2fd5a00b1203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "674d8cb0-4005-42f5-adbc-8f4ab04a948f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9bb20ab-b373-42dd-88fb-cff8e78c40ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "674d8cb0-4005-42f5-adbc-8f4ab04a948f",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "bfbc3e7a-d4e5-4a28-ab56-b2fce4236336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "96b30d6f-2f55-4d92-89ba-b31d01c3310c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfbc3e7a-d4e5-4a28-ab56-b2fce4236336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf63f315-db38-406f-ab2d-f96dec7f8bc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfbc3e7a-d4e5-4a28-ab56-b2fce4236336",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "ec010576-10d8-4408-8d7e-5884bf6e128a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "b9432c05-347f-464f-b9c9-9804ff0e1327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec010576-10d8-4408-8d7e-5884bf6e128a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1972fe9-c04a-4069-8c47-3c739cfc0f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec010576-10d8-4408-8d7e-5884bf6e128a",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "34b8f950-8881-426c-8d40-d60092a9b7c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "7546447f-a6fb-4314-abc4-7059c8f1915b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34b8f950-8881-426c-8d40-d60092a9b7c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52324cb8-1614-4bf7-a2c2-cb05a0aa4299",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34b8f950-8881-426c-8d40-d60092a9b7c9",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "b42a312c-db4d-4ae5-a06b-24d18d726904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "c7d60744-e63a-4242-8558-f3c438f91d2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b42a312c-db4d-4ae5-a06b-24d18d726904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecdff6f7-8924-4394-92b6-3943f2eef2e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b42a312c-db4d-4ae5-a06b-24d18d726904",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "037a53e3-8e99-4e8f-88ec-ec42dc8b3f6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "91a4f037-1f67-4681-9747-4f7d1c8be03c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "037a53e3-8e99-4e8f-88ec-ec42dc8b3f6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dd84f96-c451-4460-9101-7b9de7987c4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "037a53e3-8e99-4e8f-88ec-ec42dc8b3f6d",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "3c2ed2e2-aff1-4638-a470-80f118ae8086",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "87b556ea-8a5c-4655-9e7b-cbfc12441908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c2ed2e2-aff1-4638-a470-80f118ae8086",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3df320d-6193-424c-95ff-ef0fe0164799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c2ed2e2-aff1-4638-a470-80f118ae8086",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "c3df6d54-e8d5-4074-8bd9-2d0ecd8136e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "43701f58-0a76-4eb7-9a04-0b1646ebbf7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3df6d54-e8d5-4074-8bd9-2d0ecd8136e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "043a5c1d-5ead-4601-aab9-845befed47be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3df6d54-e8d5-4074-8bd9-2d0ecd8136e0",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "9ba17848-2bd7-4997-8e99-5a69220c113d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "a03d2933-737c-4207-8285-ce2e948e9d6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ba17848-2bd7-4997-8e99-5a69220c113d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b9b1777-d76d-4f18-a0bf-f1651dbd7aaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ba17848-2bd7-4997-8e99-5a69220c113d",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "7a305f05-0ace-47ce-a55e-35172576cec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "1d05fce1-cce9-4f93-a74c-e69dd4e7df10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a305f05-0ace-47ce-a55e-35172576cec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "934f0616-5986-4158-b389-88544e655130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a305f05-0ace-47ce-a55e-35172576cec7",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "7ce1a9ab-6a02-4c9b-9083-03cd6702cd10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "e510adb8-1d2e-423a-b419-05ee0829c2cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ce1a9ab-6a02-4c9b-9083-03cd6702cd10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a825451-50af-44a0-babb-fe28109887f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce1a9ab-6a02-4c9b-9083-03cd6702cd10",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "1b5b31db-0368-43e5-9045-9cc4f4c65e73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "14196d72-420e-4104-862c-104e51796324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b5b31db-0368-43e5-9045-9cc4f4c65e73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "404c9abe-806a-4d9b-ac70-e94287016f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b5b31db-0368-43e5-9045-9cc4f4c65e73",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "00749cd4-81bd-41b5-ac7b-1ac06063b593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "b751fe34-717c-4e2e-8b36-7df122924b3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00749cd4-81bd-41b5-ac7b-1ac06063b593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29d6f842-a019-4d19-9c92-f1c5551e7322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00749cd4-81bd-41b5-ac7b-1ac06063b593",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "60689c6f-429b-49c1-be8f-0b9ae01493f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "a9f94a03-d129-4156-946c-12c61ed83280",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60689c6f-429b-49c1-be8f-0b9ae01493f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76ab355d-0ece-4ce0-8747-036f98d7fdee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60689c6f-429b-49c1-be8f-0b9ae01493f0",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "507b8700-cc36-4a83-a78c-91f0c8b43ebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "b0f7c1bc-8303-4049-a475-078dd287cb4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "507b8700-cc36-4a83-a78c-91f0c8b43ebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c17a280-723e-4ce9-b54d-9704569681ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "507b8700-cc36-4a83-a78c-91f0c8b43ebb",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "0a0c93c1-6ed6-4088-bea4-5824e4c9ee87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "2eea7ad5-5e34-4bc4-b1c6-4ffbd134484f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a0c93c1-6ed6-4088-bea4-5824e4c9ee87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a35b2182-19c3-4722-9c76-8764eedb2443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a0c93c1-6ed6-4088-bea4-5824e4c9ee87",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "2f23783e-c06e-41ab-a779-cf886d0c6206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "8faab567-f1d7-46fe-88c6-0e55d2dba727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f23783e-c06e-41ab-a779-cf886d0c6206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5879319-5f7e-4119-b40d-d2356ae9f272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f23783e-c06e-41ab-a779-cf886d0c6206",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "cd021c25-b09f-4d5e-bb8d-b2022e694156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "574ae35a-29d3-4a05-8e2f-0a23e35db717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd021c25-b09f-4d5e-bb8d-b2022e694156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "296896a4-dbcb-4652-ba11-acd92cebc8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd021c25-b09f-4d5e-bb8d-b2022e694156",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "750c9bf9-ef34-414a-b2f9-e4a200a92953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "8f6e2af6-eedc-4ba3-a287-5283eeaa49fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750c9bf9-ef34-414a-b2f9-e4a200a92953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15463dc2-7b0b-4fa9-ad6c-624bc1915cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750c9bf9-ef34-414a-b2f9-e4a200a92953",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        },
        {
            "id": "651a6a10-0be1-4874-962d-43e2a2e28f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "compositeImage": {
                "id": "3b5b103f-fd94-4f99-aa04-92fdcce0dbd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "651a6a10-0be1-4874-962d-43e2a2e28f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fdd51bd-d9ed-40ad-ae6f-20ce3261a953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "651a6a10-0be1-4874-962d-43e2a2e28f5a",
                    "LayerId": "c3764bc8-7960-4cbe-8ce5-879826189183"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "c3764bc8-7960-4cbe-8ce5-879826189183",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a29c62f5-601e-42b7-8a94-5415dcb4d464",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 56
}