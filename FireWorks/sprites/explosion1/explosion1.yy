{
    "id": "d00bac86-debc-4a80-ac83-c947c59d5243",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "explosion1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0b70ce2-b72c-4bd6-84fb-2ee9bb1fb75f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d00bac86-debc-4a80-ac83-c947c59d5243",
            "compositeImage": {
                "id": "b33ddef1-ec42-411f-a132-365c55b8c57f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0b70ce2-b72c-4bd6-84fb-2ee9bb1fb75f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d50c8b4d-433a-4813-a22b-120dcefa5bd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0b70ce2-b72c-4bd6-84fb-2ee9bb1fb75f",
                    "LayerId": "bc768c53-c51b-40a2-b237-c1d8e90b4082"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bc768c53-c51b-40a2-b237-c1d8e90b4082",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d00bac86-debc-4a80-ac83-c947c59d5243",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}