{
    "id": "78b908ad-4f4a-41f5-9126-dd604502eace",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 35,
    "bbox_right": 65,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19fdbe6f-1413-4fc1-ab17-9d6e7e9ff963",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "098351b6-8cf9-4738-9a9d-d53208375c35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19fdbe6f-1413-4fc1-ab17-9d6e7e9ff963",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54ad3b0a-497b-455c-b88e-ffc8e175f96f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19fdbe6f-1413-4fc1-ab17-9d6e7e9ff963",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "b805c195-4b2a-4639-9a2f-829f19e01bb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "b1177420-ef91-4712-ac13-19ebf74035e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b805c195-4b2a-4639-9a2f-829f19e01bb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2398e52f-e00e-4d79-901a-0f053c429d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b805c195-4b2a-4639-9a2f-829f19e01bb9",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "b427cdcd-6950-4c81-8fd4-34f6006a2d9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "ba93ceda-a0a9-40b6-a661-d778063367a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b427cdcd-6950-4c81-8fd4-34f6006a2d9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dab90cb8-8cde-49b0-8835-273b285054d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b427cdcd-6950-4c81-8fd4-34f6006a2d9a",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "5640da3a-e4d4-4a07-8263-2c2d435899f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "e7e37669-bcff-4ad8-b5a5-f4be2ed084c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5640da3a-e4d4-4a07-8263-2c2d435899f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bef72256-0d36-4f2d-8c05-47d6d780a16a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5640da3a-e4d4-4a07-8263-2c2d435899f1",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "6d70788f-faee-431f-b84a-71c01d250223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "815223db-2f68-45c4-8632-33441d95685f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d70788f-faee-431f-b84a-71c01d250223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "413d33aa-19b6-40df-b008-2b1ed1e829ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d70788f-faee-431f-b84a-71c01d250223",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "6e9e5c3e-6e1f-4363-8ae6-de9516ef6134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "3aa9f436-ed68-4513-8fe3-7ddf4cbc7640",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e9e5c3e-6e1f-4363-8ae6-de9516ef6134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29fcaed3-6d87-41b2-9275-a3dc0ce6c72e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e9e5c3e-6e1f-4363-8ae6-de9516ef6134",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "d82bc7d1-73b0-4050-a2de-0eb0a6528881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "c369e2d7-d81f-4e65-b3c4-b860a54e0202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d82bc7d1-73b0-4050-a2de-0eb0a6528881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d779c083-4dc2-48b8-81ff-e6eecae541dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d82bc7d1-73b0-4050-a2de-0eb0a6528881",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "d2fe1fee-127b-4522-be07-7a8fa9a5a5fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "f998e5bb-88e8-4326-b7c5-b5f1472fb051",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2fe1fee-127b-4522-be07-7a8fa9a5a5fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae0bda9d-7487-4e10-bc23-bfe8b6fd636d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2fe1fee-127b-4522-be07-7a8fa9a5a5fa",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "492215b1-ea68-4f2b-975e-768eed017a6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "8dfc4349-f1fe-4ed0-9e78-859ad31cecea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "492215b1-ea68-4f2b-975e-768eed017a6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8195774-f765-467f-9151-23c25b6fa25f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "492215b1-ea68-4f2b-975e-768eed017a6f",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "694b2033-d5e7-4ae3-bcb6-d5f08c11b9c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "75b3b6d7-5a5f-4f1c-81fb-3415ad96c5b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "694b2033-d5e7-4ae3-bcb6-d5f08c11b9c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c0108ba-d9cb-4b0d-a9ff-4a8bb62fd913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "694b2033-d5e7-4ae3-bcb6-d5f08c11b9c3",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "fdc697f1-f5db-44e3-8c38-ed67c0c2ca71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "78bd18eb-7596-4bb7-a3c2-42da00a886b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdc697f1-f5db-44e3-8c38-ed67c0c2ca71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843e9814-b017-4935-8801-402b2da6c0ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdc697f1-f5db-44e3-8c38-ed67c0c2ca71",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "cf196988-5fee-4c85-ae17-af2745b48ef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "c8112fec-5e8b-4a1c-9bf9-b6dd25113c6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf196988-5fee-4c85-ae17-af2745b48ef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de15fbe6-66b1-4fee-b67b-2f9accfe0156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf196988-5fee-4c85-ae17-af2745b48ef0",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "462e5b3e-e83d-4859-b6be-c485360198e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "43b91bd7-e8a1-4ad3-8e54-2e76fcf2fabf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "462e5b3e-e83d-4859-b6be-c485360198e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65934c4b-d0ce-43f0-abe4-6bb34eaa78cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462e5b3e-e83d-4859-b6be-c485360198e4",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "6256c377-8e6c-41e8-9840-60f62feb6ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "7b478695-116c-406b-9ebc-5b77966a5e0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6256c377-8e6c-41e8-9840-60f62feb6ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "537a47d5-c3b9-41aa-87fd-dcddde0ddb1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6256c377-8e6c-41e8-9840-60f62feb6ab5",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "d8ab72da-f921-4067-b3af-e5377ec737e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "7efa1142-9938-43e4-bb23-8122c1751db6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8ab72da-f921-4067-b3af-e5377ec737e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "071972c9-de9a-4de2-94dc-03edae90711c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8ab72da-f921-4067-b3af-e5377ec737e4",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        },
        {
            "id": "ee53e84e-a7b3-4b5e-ae65-ae12c1c9bc09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "compositeImage": {
                "id": "328115ff-98f6-46a3-a26f-951e3409429b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee53e84e-a7b3-4b5e-ae65-ae12c1c9bc09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c91bcd7-4911-4383-8d74-1afcd25d7f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee53e84e-a7b3-4b5e-ae65-ae12c1c9bc09",
                    "LayerId": "7c942bac-0a6b-464e-9889-7952ba0188bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "7c942bac-0a6b-464e-9889-7952ba0188bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78b908ad-4f4a-41f5-9126-dd604502eace",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 70
}