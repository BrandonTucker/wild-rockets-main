{
    "id": "0f25f1db-dad6-4def-9026-47896dfe9c71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 32,
    "bbox_right": 70,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e046bee6-09d8-46ce-8177-45dd28476451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "29bff8ba-3654-4041-beb4-88da57803985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e046bee6-09d8-46ce-8177-45dd28476451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5df2e126-5c38-4196-b1f2-7e06371c72e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e046bee6-09d8-46ce-8177-45dd28476451",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "28056aff-1246-441c-9c3e-ad74a203e8ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "c3cf1b35-1db1-461f-8a6d-978c9aa1bc22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28056aff-1246-441c-9c3e-ad74a203e8ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98179f3c-9769-46cd-9743-ec1e0d8b7783",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28056aff-1246-441c-9c3e-ad74a203e8ca",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "4d7e6014-a892-4a1a-baba-06fac888b805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "fec247c5-dc63-46da-89de-11325b469585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7e6014-a892-4a1a-baba-06fac888b805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee92e006-748d-42e4-bf48-7dfbd5914224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7e6014-a892-4a1a-baba-06fac888b805",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "af4b1e71-d58f-4c20-b5fb-9f18eadcf91f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "3d3bb631-8afc-47a0-b5a4-e96bc3445fa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af4b1e71-d58f-4c20-b5fb-9f18eadcf91f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b99f853c-3968-4fd7-a60b-c6854c21490a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af4b1e71-d58f-4c20-b5fb-9f18eadcf91f",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "5123cb34-a7e3-4b7c-aa33-50ec9736a53d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "c81fab31-aba1-4906-bf0b-05a63e74c3c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5123cb34-a7e3-4b7c-aa33-50ec9736a53d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2abb92dd-795b-414c-a2a0-6b9c9ba8332d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5123cb34-a7e3-4b7c-aa33-50ec9736a53d",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "1612f97d-9eea-48b1-be55-36dde14924e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "fafa917e-3f2a-4f0a-9af2-774f434228a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1612f97d-9eea-48b1-be55-36dde14924e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81f82c8f-707f-46ff-9134-9cf5a697f9a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1612f97d-9eea-48b1-be55-36dde14924e9",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "216ff398-5a17-4f94-8e00-4793dcab149a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "0a3244a0-b48e-463f-a981-df967c0bd463",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "216ff398-5a17-4f94-8e00-4793dcab149a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d88c8a0-562e-4fe5-8557-db9e2fdfdeae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "216ff398-5a17-4f94-8e00-4793dcab149a",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "5b5e8195-aaf2-4e22-a99a-15199504e786",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "96c9bb78-e478-4e9f-8d7a-0d4996c4f5aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b5e8195-aaf2-4e22-a99a-15199504e786",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "511c494a-ac13-475d-8547-be34898fc082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b5e8195-aaf2-4e22-a99a-15199504e786",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "6cdd0481-0ce6-41f2-a254-37c6b298f3f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "e4d4acf0-45d2-4230-8787-9060913f7c5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cdd0481-0ce6-41f2-a254-37c6b298f3f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5cbfe4f-e511-47d1-91a4-239dd468437f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cdd0481-0ce6-41f2-a254-37c6b298f3f5",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "885e7597-d8e2-42ee-a0b2-f72bcaaa6ab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "7f74a8c8-338f-4573-9703-ffa936311147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "885e7597-d8e2-42ee-a0b2-f72bcaaa6ab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8479834f-4144-455c-a1fc-2eba942d613a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "885e7597-d8e2-42ee-a0b2-f72bcaaa6ab7",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "b3f37c9b-464e-4acd-9312-8c9df93a0782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "df0c3b96-0f32-49b5-867a-2d7c73ca9064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3f37c9b-464e-4acd-9312-8c9df93a0782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91d0c881-9316-4ca4-9d4f-2d3eb9ff38a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3f37c9b-464e-4acd-9312-8c9df93a0782",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "37b36d0c-f19f-402b-bf30-071e665b7a8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "6e08f752-650e-403c-96f0-9643fd7d95e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b36d0c-f19f-402b-bf30-071e665b7a8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da2a1a41-dd26-467a-8e85-5c749e938ed6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b36d0c-f19f-402b-bf30-071e665b7a8a",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "4b98f686-af1d-4857-b987-67bfe7c56ce2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "a629ebb6-6172-45e8-bc46-0a1ac20c13db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b98f686-af1d-4857-b987-67bfe7c56ce2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45db64a9-117f-4c0a-9ac0-a402e27771f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b98f686-af1d-4857-b987-67bfe7c56ce2",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "6d6164f2-8eb1-4c8a-9d24-f5bd6afa19fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "51b64c34-b1d0-4be6-9ebf-9eb591621405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d6164f2-8eb1-4c8a-9d24-f5bd6afa19fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6405156-670b-49ce-9ed3-cb493650c579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6164f2-8eb1-4c8a-9d24-f5bd6afa19fa",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "84626d4f-266d-4474-9e54-a9da3b104bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "df5984eb-9868-4958-9680-e24c7ec906e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84626d4f-266d-4474-9e54-a9da3b104bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3be2c083-0529-449a-a0e3-a9b8a3ca809e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84626d4f-266d-4474-9e54-a9da3b104bc8",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "922a2ccb-7e8f-448f-b763-5c88d57379ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "6f8e6e95-d717-4b9b-8d2b-4990b733e491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "922a2ccb-7e8f-448f-b763-5c88d57379ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b3e94a5-b67a-41a8-b359-d52886f1b158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "922a2ccb-7e8f-448f-b763-5c88d57379ff",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "eb384dd0-81f4-4b1e-8de1-6834c2ccb48d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "2aeb6c36-34bc-4336-802f-671fc1ca0f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb384dd0-81f4-4b1e-8de1-6834c2ccb48d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdf1ce77-b04b-4c2b-9387-ba430605ce6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb384dd0-81f4-4b1e-8de1-6834c2ccb48d",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "37c60c74-e4ff-45fb-be69-7f98d52d2a19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "f5fef656-ad31-4f97-801a-33df3f76a94e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37c60c74-e4ff-45fb-be69-7f98d52d2a19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94e91704-3449-4e18-ac61-ce15e125d71e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37c60c74-e4ff-45fb-be69-7f98d52d2a19",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        },
        {
            "id": "14767912-b63c-4d04-82c4-4a2b3ea992d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "compositeImage": {
                "id": "a4f5c882-5e26-4112-b953-ca5de35699f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14767912-b63c-4d04-82c4-4a2b3ea992d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7d95712-1698-4784-b300-709b881ecc81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14767912-b63c-4d04-82c4-4a2b3ea992d9",
                    "LayerId": "f8955973-7afc-4a54-91a8-9c60d129ecf5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "f8955973-7afc-4a54-91a8-9c60d129ecf5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f25f1db-dad6-4def-9026-47896dfe9c71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 52,
    "yorig": 65
}