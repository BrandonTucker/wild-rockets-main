{
    "id": "321d3820-bf0b-4153-86ae-7371df4fd062",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sparkle1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 14,
    "bbox_right": 17,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6eba5b4-d9ac-4512-91cf-9362514ffec4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "321d3820-bf0b-4153-86ae-7371df4fd062",
            "compositeImage": {
                "id": "ab1950d6-a4ec-49c4-8886-299f6d2bf9f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6eba5b4-d9ac-4512-91cf-9362514ffec4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b5f38cc-3496-408c-9aa2-ff0f2aec9352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6eba5b4-d9ac-4512-91cf-9362514ffec4",
                    "LayerId": "eed903f6-879b-4d3c-9b5e-766282c2b43f"
                }
            ]
        },
        {
            "id": "f6bcf070-9ea8-4e43-8b75-bb220febfff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "321d3820-bf0b-4153-86ae-7371df4fd062",
            "compositeImage": {
                "id": "c7c3071c-6229-46cd-84c3-8f2e3edc21df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6bcf070-9ea8-4e43-8b75-bb220febfff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "375d9195-c7a8-435d-822e-f801a9fce76c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6bcf070-9ea8-4e43-8b75-bb220febfff0",
                    "LayerId": "eed903f6-879b-4d3c-9b5e-766282c2b43f"
                }
            ]
        },
        {
            "id": "f330db75-2a50-4079-aa4e-68b055e1f339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "321d3820-bf0b-4153-86ae-7371df4fd062",
            "compositeImage": {
                "id": "ca432a61-5d13-4bcd-939d-01ca929c57e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f330db75-2a50-4079-aa4e-68b055e1f339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aedc7e6-fc23-4f8a-937c-8415439b220f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f330db75-2a50-4079-aa4e-68b055e1f339",
                    "LayerId": "eed903f6-879b-4d3c-9b5e-766282c2b43f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eed903f6-879b-4d3c-9b5e-766282c2b43f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "321d3820-bf0b-4153-86ae-7371df4fd062",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}