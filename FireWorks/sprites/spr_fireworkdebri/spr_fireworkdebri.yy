{
    "id": "2fb95093-84ca-4f62-a980-f289d9cf9808",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 44,
    "bbox_right": 55,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df450bff-be7f-4ce8-a89a-322ec9bfcb93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "dcf80b72-0564-4e60-a5c4-da3b29d24a9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df450bff-be7f-4ce8-a89a-322ec9bfcb93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8de5493-ea76-4ce3-873f-d8a940254969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df450bff-be7f-4ce8-a89a-322ec9bfcb93",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "5128915b-fa4e-4548-b119-85aa4499ed1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "f69950bc-5a50-439c-8418-3c03c442e938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5128915b-fa4e-4548-b119-85aa4499ed1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5e1541e-3306-4367-a5ee-2f1289db13c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5128915b-fa4e-4548-b119-85aa4499ed1a",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "6183a125-c78c-425a-b582-4c8e72fa7c58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "228786af-f073-42ec-a700-09aae55fef80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6183a125-c78c-425a-b582-4c8e72fa7c58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e320a1c2-0da6-42e7-a49d-d1580f9586e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6183a125-c78c-425a-b582-4c8e72fa7c58",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "d83f9fce-a78b-433f-a3ee-3eb5fd9bc5b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "9f50976c-6d61-4855-84d7-e3453bce0d16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d83f9fce-a78b-433f-a3ee-3eb5fd9bc5b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94e3b49f-1743-400e-9b9b-594a252860e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d83f9fce-a78b-433f-a3ee-3eb5fd9bc5b2",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "59cfed37-d698-4408-8bf7-e78963b40779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "d3ace9fc-cfb4-4300-b01a-60ed0c9efea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59cfed37-d698-4408-8bf7-e78963b40779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd8f55b-3537-4614-b5c4-2a8d4d140d38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59cfed37-d698-4408-8bf7-e78963b40779",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "bc61da88-d701-4327-a59a-823fd65ce1f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "0458991c-f6e4-4404-80a0-299ab998bec6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc61da88-d701-4327-a59a-823fd65ce1f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e50674ad-45ea-440b-a568-20f419bf762f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc61da88-d701-4327-a59a-823fd65ce1f8",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "fcf00f12-6dba-40c3-9325-00c52c8a99c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "e192e9f5-126b-48fc-998f-448c9426c6de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf00f12-6dba-40c3-9325-00c52c8a99c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5f3637b-202c-44e3-a1de-75b04f0ff4e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf00f12-6dba-40c3-9325-00c52c8a99c5",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "cec3b817-6fe0-43c4-9e65-9812b09c4810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "b281de09-b5ca-46bd-a890-1ebb51ff7f01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cec3b817-6fe0-43c4-9e65-9812b09c4810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c43fe778-692a-4930-8395-1d0d38db0e81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cec3b817-6fe0-43c4-9e65-9812b09c4810",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "c6847964-5175-40bd-b010-1d593fd3cd59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "26080f2e-b470-456b-a109-64c7c23ad75e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6847964-5175-40bd-b010-1d593fd3cd59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b59dde5e-6ab7-469a-9a66-644e20774e25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6847964-5175-40bd-b010-1d593fd3cd59",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "55c8098a-027b-4d3c-b7ae-e61972eca950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "5d90aec1-f247-47a9-8c12-9168df218ab5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55c8098a-027b-4d3c-b7ae-e61972eca950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "230ef8e6-ef56-430d-8b29-3abeaa2559c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55c8098a-027b-4d3c-b7ae-e61972eca950",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "7fb2abb3-a4c4-4e42-9589-5cedc67aeccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "349e55db-2080-4f42-a9d7-4839e2a916cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fb2abb3-a4c4-4e42-9589-5cedc67aeccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7937de29-f319-45e6-bee9-894385c03bbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fb2abb3-a4c4-4e42-9589-5cedc67aeccb",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "2a0a2486-218c-45c2-8f06-4f11688f0310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "20a2ff9d-336e-4923-a0bf-4b2abe4f20e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a0a2486-218c-45c2-8f06-4f11688f0310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f38bca1a-2d86-4a9a-8c31-71d578a0fcaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a0a2486-218c-45c2-8f06-4f11688f0310",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "79550c82-786e-4433-b950-2209ec9376fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "4223fa6c-0556-4585-9f1c-bb888adff6b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79550c82-786e-4433-b950-2209ec9376fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "894e7057-40d4-4732-b07e-15f156341282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79550c82-786e-4433-b950-2209ec9376fa",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "681e50ae-e742-412f-9c85-476018c5adc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "70defc31-e2e2-4b55-9024-73cbc98e8dba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "681e50ae-e742-412f-9c85-476018c5adc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "606e1069-b294-4e8b-a352-9d302d116277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "681e50ae-e742-412f-9c85-476018c5adc0",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "9c791f6c-a7c3-46ff-8cd5-ecb4058f1655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "1c11e4ab-f04b-4eb2-b2e5-3534d3549c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c791f6c-a7c3-46ff-8cd5-ecb4058f1655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bdfd877-af22-4208-971b-d6709323816f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c791f6c-a7c3-46ff-8cd5-ecb4058f1655",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "063650ae-feb9-419c-88d3-2adfaaea8ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "3b727358-2d83-4ffd-80d2-18d7b88b9177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "063650ae-feb9-419c-88d3-2adfaaea8ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc98d8c9-239f-4b4a-aaf0-248b52d85b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "063650ae-feb9-419c-88d3-2adfaaea8ce4",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "5bba04e6-1093-4631-89c6-7fe37698a4d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "413d3365-6b4d-41c8-aa0f-ba03a27a38ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bba04e6-1093-4631-89c6-7fe37698a4d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e49ef483-87f2-4bde-9d03-0c6cddc35a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bba04e6-1093-4631-89c6-7fe37698a4d1",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "f5ce62d5-0a75-4b24-bfca-ab3de20cdc75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "9cdc2a05-b4a1-48bf-9fc3-23e79b3b728a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5ce62d5-0a75-4b24-bfca-ab3de20cdc75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fff3d83-30d4-4509-bb1d-18f65de0b217",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5ce62d5-0a75-4b24-bfca-ab3de20cdc75",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "e2f5c82b-004d-4186-a4a3-ec793c35848a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "43d5f5f5-4645-4504-b114-26a0ee4ce9ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2f5c82b-004d-4186-a4a3-ec793c35848a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adc22330-b180-4209-af05-a61c7a51bfe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2f5c82b-004d-4186-a4a3-ec793c35848a",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "7d2b1e08-288e-4836-8c06-3f5821a4d1b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "3420f883-cb14-49bd-a313-828a3d956f71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2b1e08-288e-4836-8c06-3f5821a4d1b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca3b7d1f-d974-4706-a09c-ff67e4f335ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2b1e08-288e-4836-8c06-3f5821a4d1b4",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "d599dec7-e958-4d8f-8a4a-7970eed0f562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "52ee86af-5062-480a-98a6-a5a68256a263",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d599dec7-e958-4d8f-8a4a-7970eed0f562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c420516-73fa-44ec-8456-3d460e4a5a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d599dec7-e958-4d8f-8a4a-7970eed0f562",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        },
        {
            "id": "1b3558ff-d35c-4e89-afa0-0ddafc9bd4fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "compositeImage": {
                "id": "8885ecf5-b5c6-46b6-bb3e-c63e9d60c534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b3558ff-d35c-4e89-afa0-0ddafc9bd4fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bcea411-f1d6-4f52-a449-828720496e08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b3558ff-d35c-4e89-afa0-0ddafc9bd4fa",
                    "LayerId": "2dd095fe-7d90-4c9b-b82e-f345833a8aec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "2dd095fe-7d90-4c9b-b82e-f345833a8aec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fb95093-84ca-4f62-a980-f289d9cf9808",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 56
}