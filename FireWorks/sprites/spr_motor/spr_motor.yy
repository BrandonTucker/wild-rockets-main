{
    "id": "3fbb2e5a-28e8-4a75-bda2-5d78664470bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_motor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 0,
    "bbox_right": 58,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "116c708e-3f7d-4635-9de6-491422f48dd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fbb2e5a-28e8-4a75-bda2-5d78664470bb",
            "compositeImage": {
                "id": "3f32401f-2411-4f5c-94b5-fb031c3a5e0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "116c708e-3f7d-4635-9de6-491422f48dd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc0c20b9-2387-4ea8-9ef5-d9bbea677850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "116c708e-3f7d-4635-9de6-491422f48dd2",
                    "LayerId": "bf6d3caa-5602-4dbb-ad85-0d59e1471919"
                }
            ]
        },
        {
            "id": "c76e988a-6e0b-48bc-b7b2-c45acaead0f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fbb2e5a-28e8-4a75-bda2-5d78664470bb",
            "compositeImage": {
                "id": "4baf911f-0432-4367-8125-1fe76bc1e178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c76e988a-6e0b-48bc-b7b2-c45acaead0f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3afd9fb8-833c-405b-be9c-5cc472782f79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c76e988a-6e0b-48bc-b7b2-c45acaead0f2",
                    "LayerId": "bf6d3caa-5602-4dbb-ad85-0d59e1471919"
                }
            ]
        },
        {
            "id": "e80ff291-6351-4227-b1b9-a55a27b58c20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fbb2e5a-28e8-4a75-bda2-5d78664470bb",
            "compositeImage": {
                "id": "566be946-b71f-41e7-baef-17ef6f0fceee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e80ff291-6351-4227-b1b9-a55a27b58c20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1514492a-0ed9-4400-b3b3-a7674e492fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e80ff291-6351-4227-b1b9-a55a27b58c20",
                    "LayerId": "bf6d3caa-5602-4dbb-ad85-0d59e1471919"
                }
            ]
        },
        {
            "id": "76e20097-abef-4f36-9a46-758944725bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fbb2e5a-28e8-4a75-bda2-5d78664470bb",
            "compositeImage": {
                "id": "9591317b-5d06-4b09-8d42-9889205b46d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e20097-abef-4f36-9a46-758944725bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de7d6cb2-4157-4750-8a09-c76006ab56ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e20097-abef-4f36-9a46-758944725bfd",
                    "LayerId": "bf6d3caa-5602-4dbb-ad85-0d59e1471919"
                }
            ]
        },
        {
            "id": "5e401693-a9de-4480-b661-bf50ea08df45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fbb2e5a-28e8-4a75-bda2-5d78664470bb",
            "compositeImage": {
                "id": "ed6f0bdf-2bb8-488c-b36c-91b0e6a1effa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e401693-a9de-4480-b661-bf50ea08df45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be7efc6f-4fdc-430b-9a91-a9d964dd2ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e401693-a9de-4480-b661-bf50ea08df45",
                    "LayerId": "bf6d3caa-5602-4dbb-ad85-0d59e1471919"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "bf6d3caa-5602-4dbb-ad85-0d59e1471919",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fbb2e5a-28e8-4a75-bda2-5d78664470bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 59,
    "xorig": 30,
    "yorig": 63
}