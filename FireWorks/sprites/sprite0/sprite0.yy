{
    "id": "6f0c51ce-5b31-496a-96e6-344951091bb5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f40c75e3-d75b-4ad3-a5c1-e96c75ff1d60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f0c51ce-5b31-496a-96e6-344951091bb5",
            "compositeImage": {
                "id": "021c0e0a-0b89-430d-bc24-c3992a20dad8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f40c75e3-d75b-4ad3-a5c1-e96c75ff1d60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b280fbf6-bc96-4d59-b94a-66c348f5844b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f40c75e3-d75b-4ad3-a5c1-e96c75ff1d60",
                    "LayerId": "57aa94c9-070f-4049-9371-505c56fe24c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "57aa94c9-070f-4049-9371-505c56fe24c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f0c51ce-5b31-496a-96e6-344951091bb5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}