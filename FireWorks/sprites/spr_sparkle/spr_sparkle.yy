{
    "id": "12516c4e-1506-4a9b-8fcb-628a98593d72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sparkle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 40,
    "bbox_right": 54,
    "bbox_top": 51,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26ce6da4-5838-4c1f-83df-ae5403236170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "717e29aa-463e-4a30-aa3c-ef2f3c9c1a28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ce6da4-5838-4c1f-83df-ae5403236170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "922c885c-2266-4a1d-aae2-05a85d3e63f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ce6da4-5838-4c1f-83df-ae5403236170",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "4eefbfa2-8c7e-46b7-b0c8-83534ceea610",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "c7726b1c-f117-4b87-a0c3-c5353433f6e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eefbfa2-8c7e-46b7-b0c8-83534ceea610",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad0fd57-7595-4ed1-8387-200e9fdd44c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eefbfa2-8c7e-46b7-b0c8-83534ceea610",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "14f2e4ab-c3f4-41da-aa1e-9124c8589067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "5224a7fa-96d4-4ebd-92a2-f4b3e794ebdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14f2e4ab-c3f4-41da-aa1e-9124c8589067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58935d75-2b56-492e-8a5b-3eec940b508c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14f2e4ab-c3f4-41da-aa1e-9124c8589067",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "6db8bb9c-4d1b-4d6c-85cb-8667e26fce82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "1eba4689-8289-4329-8fad-66273ee8a248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6db8bb9c-4d1b-4d6c-85cb-8667e26fce82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a3ef9a8-968b-4e3c-b2a4-001f562eff64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db8bb9c-4d1b-4d6c-85cb-8667e26fce82",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "da434b2c-bc69-4b23-959f-80d4832dcbc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "a63c5f94-d12f-4a40-a618-6d7112751e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da434b2c-bc69-4b23-959f-80d4832dcbc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0acb074b-abdc-4ba3-895b-b3f8d6ea442d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da434b2c-bc69-4b23-959f-80d4832dcbc4",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "67945fdd-ca21-43f1-baf0-01ca3e7da042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "3f13d1e9-2649-4d17-9f6a-9bbae267940e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67945fdd-ca21-43f1-baf0-01ca3e7da042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b82dcc1-7398-43ec-9e32-682eb96de8fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67945fdd-ca21-43f1-baf0-01ca3e7da042",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "a6fa28c8-e815-4d01-b8a8-d1ecb42e9c90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "d45ce3f2-8d5b-48f3-b347-b0ec0a1cceef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6fa28c8-e815-4d01-b8a8-d1ecb42e9c90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbc47d35-3a4c-43b2-b3d7-e83391be73de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6fa28c8-e815-4d01-b8a8-d1ecb42e9c90",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "2ef045e5-45be-448f-a8fb-d22982e5e2d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "1314e5a4-5e66-4d67-9e9f-57d05bad819c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ef045e5-45be-448f-a8fb-d22982e5e2d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cde9d53-6f51-47bb-bcde-ba6221d91d29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ef045e5-45be-448f-a8fb-d22982e5e2d4",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "56d759fa-5d13-422c-850b-188c719356e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "0a05fdd0-8f14-4d09-a43e-6f316b90b627",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56d759fa-5d13-422c-850b-188c719356e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab1550f9-85a9-4f65-a827-49e8a1abe95f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56d759fa-5d13-422c-850b-188c719356e4",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "fe5edfca-17c4-460b-b3a2-664885d1b413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "9da80e12-c6bd-45a6-b003-559a658ae086",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe5edfca-17c4-460b-b3a2-664885d1b413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11e04d9c-f02a-4e71-a6d3-382b0fd22baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe5edfca-17c4-460b-b3a2-664885d1b413",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "fe3fe93a-f02e-470e-8bf6-8c8f374cf9f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "358790f8-c5e4-436c-bf1e-3d6c453cbad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe3fe93a-f02e-470e-8bf6-8c8f374cf9f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "443ae72b-870a-448b-bb30-403b28d47383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe3fe93a-f02e-470e-8bf6-8c8f374cf9f3",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "d1fe7fe3-b876-4672-8ea0-75fccb2cfe45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "087a8419-8ac0-49f5-91dd-dedf03c73234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1fe7fe3-b876-4672-8ea0-75fccb2cfe45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5243fd0-5a52-4f41-a318-98b23434c870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1fe7fe3-b876-4672-8ea0-75fccb2cfe45",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "463d5eb2-b866-4834-9676-6cb8d2739eb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "12f828fa-673b-4c46-8954-a8dda5f2e400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "463d5eb2-b866-4834-9676-6cb8d2739eb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87d4e62-78b2-4b59-8812-530919df53bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "463d5eb2-b866-4834-9676-6cb8d2739eb2",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "2b73cf74-4e15-4ef9-b7c3-e9860cd53015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "b4f176dc-e32b-4fe0-aba3-03ddccc3d90f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b73cf74-4e15-4ef9-b7c3-e9860cd53015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6cf4c27-2c50-4c03-b143-37b83d24b0bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b73cf74-4e15-4ef9-b7c3-e9860cd53015",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "bee0c8c5-4234-4c83-8864-373637253b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "b4f42868-d725-4c93-b316-af0083dad133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bee0c8c5-4234-4c83-8864-373637253b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a6f5d4-49ff-4d37-ba7e-b16f35754661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bee0c8c5-4234-4c83-8864-373637253b48",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "2b32d691-043a-479e-ae56-4a5ad00d7a3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "bde0475f-af49-4b1e-9c1c-50e2b7f9a6ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b32d691-043a-479e-ae56-4a5ad00d7a3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "315ab3d3-d553-495a-b110-3f0a15edf112",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b32d691-043a-479e-ae56-4a5ad00d7a3e",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "5d250300-6e55-4870-9358-fc57e13ff8f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "dd1e90f6-dbf5-4a41-8fec-cd5d3db057d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d250300-6e55-4870-9358-fc57e13ff8f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "687a9491-7240-4c77-9a62-63574270ee33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d250300-6e55-4870-9358-fc57e13ff8f6",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "59511fce-4d4b-4a21-b47b-0a907d4ee34f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "04e7be33-9969-4d87-a2b5-5ab8d792e61e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59511fce-4d4b-4a21-b47b-0a907d4ee34f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "094675cf-011b-4eca-89d8-df73d0a0c475",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59511fce-4d4b-4a21-b47b-0a907d4ee34f",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "1e59f48a-8e66-4d37-ba8b-b8fd7f01c5d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "6247fb08-926f-4c97-a650-7631c352b16c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e59f48a-8e66-4d37-ba8b-b8fd7f01c5d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3141a64c-47fa-49ba-9604-6270e72566a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e59f48a-8e66-4d37-ba8b-b8fd7f01c5d1",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "9b5fb7ab-f7ed-4e93-8a57-f5ce081ecfb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "d45b08c2-b01c-4af0-86b5-aad7999e1398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b5fb7ab-f7ed-4e93-8a57-f5ce081ecfb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6535107-0e67-4b18-9b6f-f89a77419089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b5fb7ab-f7ed-4e93-8a57-f5ce081ecfb5",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "0ce2a960-1b1c-40da-90fe-73e72c147743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "469f007c-8c0d-4e19-85ea-1d619328bc92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ce2a960-1b1c-40da-90fe-73e72c147743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23fbf5a9-56fd-40ea-9b8b-651acc7d37ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ce2a960-1b1c-40da-90fe-73e72c147743",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "98fe8061-c37e-48fc-8de1-84bc86c468fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "a85fb967-2531-4e72-b93f-16305c648b6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98fe8061-c37e-48fc-8de1-84bc86c468fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2e02b61-b385-4c9d-b287-21b0aa116d59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98fe8061-c37e-48fc-8de1-84bc86c468fc",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "ddcc8ae7-9cff-4442-a5e9-9663f9c4d1e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "6bc9c278-c1f2-4f94-96e4-4ab0ea632ce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddcc8ae7-9cff-4442-a5e9-9663f9c4d1e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba220b7-8ee3-4eb0-a39a-43b81599bb3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddcc8ae7-9cff-4442-a5e9-9663f9c4d1e0",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "7590a0b2-ef73-48c5-a68d-8882096347cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "5201cda3-1310-4712-bb00-e1eee4435cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7590a0b2-ef73-48c5-a68d-8882096347cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e35689c-5bfa-4136-bf4a-4b855aaac005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7590a0b2-ef73-48c5-a68d-8882096347cc",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "beb04d56-50ec-4641-a1bb-99316e03a101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "f3c31836-bc4c-4cc7-beea-82860df3692c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beb04d56-50ec-4641-a1bb-99316e03a101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "212dbd85-3c03-489d-ae02-ac541af1acab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beb04d56-50ec-4641-a1bb-99316e03a101",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "4ee1a818-bcc6-4de9-a2b7-f0afa1bf78ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "56a4a8e0-fc8d-4426-ab0a-69b348dd178d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee1a818-bcc6-4de9-a2b7-f0afa1bf78ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e0ed618-6b21-43b4-ab00-eb0ec5c9a26d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee1a818-bcc6-4de9-a2b7-f0afa1bf78ca",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "73a30145-094f-4a93-a390-f425791fa302",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "6b33445c-7403-4b0d-851b-9b1485be1719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73a30145-094f-4a93-a390-f425791fa302",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fc76f3c-abe2-47ec-8916-2f6df50a2ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73a30145-094f-4a93-a390-f425791fa302",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "307e2fc7-b972-48e1-ba28-94ddec7334f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "8b034e94-dfd4-4807-bb2b-b5ff589393d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "307e2fc7-b972-48e1-ba28-94ddec7334f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0601b0b-fdea-4456-8d0e-17b9cdbafa85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "307e2fc7-b972-48e1-ba28-94ddec7334f4",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "17632ccc-91d1-4676-a6ec-0800097fb1f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "dcd8025d-8c96-4212-9ff5-03925cc4302c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17632ccc-91d1-4676-a6ec-0800097fb1f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50d0b905-e02b-417d-9a34-65cb26817964",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17632ccc-91d1-4676-a6ec-0800097fb1f2",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "464bebcd-046b-4baa-870b-8bc925719ed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "7fb9512a-87f3-486b-9785-e9fd68e64b91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "464bebcd-046b-4baa-870b-8bc925719ed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e96918b5-069d-4b38-8b7a-1e3eefc8a50b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "464bebcd-046b-4baa-870b-8bc925719ed9",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "26143677-eb11-4b72-85b8-24b5581b8e5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "40d83137-d6bf-4047-bcc3-224d87a4d218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26143677-eb11-4b72-85b8-24b5581b8e5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea324a2-5705-4f59-b492-172964a42920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26143677-eb11-4b72-85b8-24b5581b8e5a",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        },
        {
            "id": "7e29b791-7c95-4e22-9bba-038cdf0664ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "compositeImage": {
                "id": "9142fea3-c2b0-41f7-9004-e366f9695ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e29b791-7c95-4e22-9bba-038cdf0664ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f239381-f568-437b-abe4-62ebd7b37035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e29b791-7c95-4e22-9bba-038cdf0664ad",
                    "LayerId": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "2a649cf3-6a41-49e3-a77f-cd2f70b45d82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12516c4e-1506-4a9b-8fcb-628a98593d72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 47,
    "yorig": 58
}