{
    "id": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireworkdebri1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 41,
    "bbox_right": 64,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5a7fe27-8d7b-4322-95b8-9d09f4122063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "1869647e-6c09-400d-ab9c-61270c4933c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5a7fe27-8d7b-4322-95b8-9d09f4122063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71cb6ad8-c396-4f8f-b76a-26de569b41e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5a7fe27-8d7b-4322-95b8-9d09f4122063",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "66bdfef6-b8d6-4e17-9baa-a39f371cf5b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "a3146181-6a22-407b-8b2e-705f36dfb4a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66bdfef6-b8d6-4e17-9baa-a39f371cf5b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcecf243-043e-4764-bc41-f2dd43fe508b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66bdfef6-b8d6-4e17-9baa-a39f371cf5b7",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "90d745df-8385-40d4-a635-80d125f958a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "acbf469a-2aa5-4021-81a6-0dfacdc6ce46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90d745df-8385-40d4-a635-80d125f958a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93637361-ba43-458b-b3f6-360a08687eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90d745df-8385-40d4-a635-80d125f958a1",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "b1094779-4b57-44fe-b3b7-d83dd88137ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "c85fd1da-ee87-4a52-8c7b-0bba5f1b304f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1094779-4b57-44fe-b3b7-d83dd88137ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80d50fc0-09ff-4c89-a15d-b91c3c68cb22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1094779-4b57-44fe-b3b7-d83dd88137ad",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "28b2c2c9-027a-417d-b6be-0bfab5627809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "1d1cec92-977b-478c-a64d-6c25d0c674ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28b2c2c9-027a-417d-b6be-0bfab5627809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae948625-2338-4a4c-ae21-c2dc7500d5da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28b2c2c9-027a-417d-b6be-0bfab5627809",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "4a4efc86-9285-430f-b751-a8452b9aaf58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "5adbcaba-00df-463c-a639-e8fbd6563f23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a4efc86-9285-430f-b751-a8452b9aaf58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cee011ef-172d-4648-9b0b-1867c5efefde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a4efc86-9285-430f-b751-a8452b9aaf58",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "c3125420-fea3-4678-9504-af8a7c2f581c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "4d2d9274-9baa-44ff-a27d-91f5516ddca6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3125420-fea3-4678-9504-af8a7c2f581c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c663b9c-293c-47b9-91bc-dedb7f2607c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3125420-fea3-4678-9504-af8a7c2f581c",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "a0943ca5-d623-4541-a5d0-c0b3e7749920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "0fdefd30-0e00-47c2-96f5-84006dea62f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0943ca5-d623-4541-a5d0-c0b3e7749920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aea16158-68da-4824-a4c7-a9227ca3dcb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0943ca5-d623-4541-a5d0-c0b3e7749920",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "15c893c1-b5c0-417f-a0f2-725354fd66ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "7097ee3e-a1b1-41f9-9f8e-a6fc49bfd55d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15c893c1-b5c0-417f-a0f2-725354fd66ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7661806c-a108-4a01-945d-079b6991e201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15c893c1-b5c0-417f-a0f2-725354fd66ff",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "7fe198fa-e201-45f7-8c0e-0a093ba4b7d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "682afec7-f671-4368-adec-8b5a81d6ce19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fe198fa-e201-45f7-8c0e-0a093ba4b7d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11af0a7d-165a-408b-bbfc-f6cf1b60cc02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fe198fa-e201-45f7-8c0e-0a093ba4b7d0",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "49ab62da-81e4-4d2a-b04c-fc19a5975981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "ce035ea6-4b96-4604-9bc4-0245925375f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ab62da-81e4-4d2a-b04c-fc19a5975981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed26780a-dca7-4234-b7ee-ad71a1af6c75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ab62da-81e4-4d2a-b04c-fc19a5975981",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "3574cc18-a6fb-4f71-8542-ed04f149dcba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "fe90d28a-d434-43f0-bd5a-802ee267f8d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3574cc18-a6fb-4f71-8542-ed04f149dcba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c670b883-5caf-4ebf-adc3-83aa27c3c7c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3574cc18-a6fb-4f71-8542-ed04f149dcba",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "c607a5f0-ebe8-4dc4-b7f6-728880cd9563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "9f8fd789-4c30-4693-b40c-415c7e50b7ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c607a5f0-ebe8-4dc4-b7f6-728880cd9563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b0bc51a-6864-4210-9b3b-7f4d759fe211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c607a5f0-ebe8-4dc4-b7f6-728880cd9563",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "7b35a88c-1850-4ce2-a30c-c31012a9b85e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "a3666125-793a-4d45-a352-1f0e24249bde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b35a88c-1850-4ce2-a30c-c31012a9b85e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116f471e-2a43-4b41-9c92-8310122e24d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b35a88c-1850-4ce2-a30c-c31012a9b85e",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "c2b48c00-c1f7-4578-8f46-679d3bb6c48a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "9f3c34c4-cba4-45b4-bc65-69ec9468438e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2b48c00-c1f7-4578-8f46-679d3bb6c48a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0698794d-a311-4085-b0c1-d2a180222f6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2b48c00-c1f7-4578-8f46-679d3bb6c48a",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "d9b40d90-25e8-4526-a037-3e5f3c8e5227",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "ee4a4dc4-4ac2-4162-909e-9e329f593e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9b40d90-25e8-4526-a037-3e5f3c8e5227",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2db9f39-9663-44e2-a59f-89ff18237814",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9b40d90-25e8-4526-a037-3e5f3c8e5227",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "d3a6d9f3-ae34-468a-b1a5-85906b4a7f27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "9d3ce4e5-d366-42b8-983e-6c5d52de250a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a6d9f3-ae34-468a-b1a5-85906b4a7f27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0245373e-5475-4a4e-8b9d-28ce0f12b7ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a6d9f3-ae34-468a-b1a5-85906b4a7f27",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "8e923fd8-8f60-4ca4-b092-71ffe66b047a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "715b16c4-c617-4f51-8748-83a18ca6bfd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e923fd8-8f60-4ca4-b092-71ffe66b047a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "703303ae-abdb-47a3-8dc3-f655113a711a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e923fd8-8f60-4ca4-b092-71ffe66b047a",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "7e581273-e99d-4325-8580-c56413a5ea40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "1c80d609-3681-4260-aca1-8c2588ba958c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e581273-e99d-4325-8580-c56413a5ea40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6acf97a5-816d-4357-9bab-02c60ccfcd96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e581273-e99d-4325-8580-c56413a5ea40",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "e0cf17b2-d4d0-4d1d-b91b-8990ba8f82e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "d8146e45-6b93-4033-9676-156fa0497eef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0cf17b2-d4d0-4d1d-b91b-8990ba8f82e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8c38795-aaba-42e5-927e-a250a14ff833",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0cf17b2-d4d0-4d1d-b91b-8990ba8f82e1",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "b13aaef9-feac-48ac-9280-80fc532f382e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "a63c61c7-aed6-470c-ba24-2f603fc1112b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b13aaef9-feac-48ac-9280-80fc532f382e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c313b2-8647-4537-b243-9b87e7b02f2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b13aaef9-feac-48ac-9280-80fc532f382e",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        },
        {
            "id": "39921131-436d-4290-b9f5-751c6f4e6a3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "compositeImage": {
                "id": "1ee3f646-3f14-4f12-9b01-7501e4161b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39921131-436d-4290-b9f5-751c6f4e6a3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "762936c7-b4b6-4768-88fb-92fcf8784624",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39921131-436d-4290-b9f5-751c6f4e6a3f",
                    "LayerId": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "40cbf91e-47f9-43ce-9b8e-53aed6f20ccd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e805977-9318-4dcc-80b8-38a0769b7ef7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 56
}