{
    "id": "9144a84b-144c-4ece-bd0b-28739f1450cd",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "GooglePlayLicensingAsExt",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        "com.android.vending.BILLING"
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "GooglePlayLicensingAsExt",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 834777342120174,
    "date": "2018-02-07 11:09:07",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "d386e8f5-7d43-4a6f-9a56-014749e8a027",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 2097160,
            "filename": "GooglePlayLicensingAsExt.ext",
            "final": "",
            "functions": [
                
            ],
            "init": "",
            "kind": 4,
            "order": [
                
            ],
            "origname": "extensions\\GooglePlayLicensingAsExt.ext",
            "uncompress": false
        }
    ],
    "gradleinject": "\\u000d\\u000acompile 'org.jbundle.util.osgi.wrapped:org.jbundle.util.osgi.wrapped.org.apache.http.client:4.1.2'",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.yoyogames.googleplaylicensingextension",
    "productID": "6C049D182ECD002FCA87540902AF4A74",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": null,
    "tvosmaccompilerflags": null,
    "tvosmaclinkerflags": null,
    "tvosplistinject": null,
    "version": "2.3.0"
}