{
    "id": "1c28b403-97cf-4907-afa5-9a2049a13123",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path1",
    "closed": false,
    "hsnap": 32,
    "kind": 0,
    "points": [
        {
            "id": "d1c24f15-4468-484d-8e37-d98413d6831e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 256,
            "speed": 100
        },
        {
            "id": "00147663-a6b5-4323-88f4-5aa1cb922a34",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 224,
            "speed": 100
        },
        {
            "id": "2d4816d8-48b4-4730-a84f-0e2cb55b6bcd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 64,
            "speed": 100
        },
        {
            "id": "535771b0-7df4-45a0-83d9-17a71a4ecc5c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 32,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 32
}