{
    "id": "f1643967-48d4-4a68-8df6-4d15625cf6b7",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path2",
    "closed": true,
    "hsnap": 16,
    "kind": 1,
    "points": [
        {
            "id": "c2fd341e-dbbe-4940-9295-6f74003bced9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 192,
            "speed": 100
        },
        {
            "id": "b8af8c41-d622-4f37-9a49-8351f30ebe3a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 336,
            "y": 208,
            "speed": 100
        },
        {
            "id": "15c7537b-ea9f-4ed5-925c-ec3aa0d33837",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 336,
            "y": 192,
            "speed": 100
        },
        {
            "id": "dd00236b-4d73-4a3e-982b-959ad5a995ef",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 208,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 16
}