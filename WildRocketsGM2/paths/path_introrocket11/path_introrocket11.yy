{
    "id": "afc6f78b-8b2c-45ed-aa57-d77c2127311c",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_introrocket11",
    "closed": false,
    "hsnap": 16,
    "kind": 1,
    "points": [
        {
            "id": "aca079f5-065c-4b16-af5c-28e844683206",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 304,
            "y": 1120,
            "speed": 100
        },
        {
            "id": "20af9d13-0473-4fe8-ad95-a4dcdfae9d66",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 240,
            "y": 656,
            "speed": 100
        },
        {
            "id": "72681dfe-e0cb-41a7-a1ef-54ac29b431d7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 176,
            "y": 368,
            "speed": 100
        },
        {
            "id": "c215d7cd-2e79-49aa-a426-9c068aafbc93",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 80,
            "speed": 100
        },
        {
            "id": "709c2552-82a6-41ce-a0ab-22dab57efd5a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": -112,
            "y": -112,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 16
}