{
    "id": "037379aa-3285-476f-87e2-4907abe05c9f",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_challangbutton",
    "closed": true,
    "hsnap": 16,
    "kind": 1,
    "points": [
        {
            "id": "c334aa7e-d25c-40ac-b917-d8db34798caa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 176,
            "speed": 100
        },
        {
            "id": "6f2e3f21-37f9-4c68-8a61-72bf57829b67",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 304,
            "y": 176,
            "speed": 100
        },
        {
            "id": "1fea8c7f-feb6-4bc1-b630-b5b3ca01cabf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 304,
            "y": 192,
            "speed": 100
        },
        {
            "id": "f1feb738-a740-4b5f-b062-4d18dcb5f506",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288,
            "y": 192,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 16
}