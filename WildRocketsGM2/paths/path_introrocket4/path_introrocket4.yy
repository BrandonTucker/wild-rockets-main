{
    "id": "632def39-851c-4954-8be2-58076937e906",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_introrocket4",
    "closed": false,
    "hsnap": 16,
    "kind": 1,
    "points": [
        {
            "id": "1e3a59f6-3d66-4902-adfe-13d05386a6ba",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 1072,
            "speed": 100
        },
        {
            "id": "c752551f-43ef-4eb5-aee9-97f67dda5e1a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 304,
            "y": 656,
            "speed": 100
        },
        {
            "id": "a3f3b2c9-6181-4b19-80ca-880565f98e43",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 384,
            "speed": 100
        },
        {
            "id": "b138622f-523d-478b-92af-334f9b92c289",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 96,
            "speed": 100
        },
        {
            "id": "46c7cd2e-50b8-4556-a108-070d313496d3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 672,
            "y": -144,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 16
}