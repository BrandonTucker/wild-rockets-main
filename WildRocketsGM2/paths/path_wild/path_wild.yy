{
    "id": "2c2ad85e-eee1-452f-8225-2c7d1f679de7",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_wild",
    "closed": false,
    "hsnap": 16,
    "kind": 1,
    "points": [
        {
            "id": "34ab4170-a91b-491d-897f-c1eb6d181c7b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 212,
            "y": 960,
            "speed": 100
        },
        {
            "id": "d2fb412f-f689-4a45-bc9d-94674f73e0e9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 192,
            "y": 816,
            "speed": 100
        },
        {
            "id": "35b6e8a9-786f-4d90-b5b2-c43f08406f59",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 168,
            "y": 740,
            "speed": 100
        },
        {
            "id": "6c2acaff-9d8e-4478-b105-e288e962a14b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 132,
            "y": 608,
            "speed": 100
        },
        {
            "id": "2f56be45-0e8d-4677-80e4-2d7df1041bd1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 104,
            "y": 524,
            "speed": 100
        },
        {
            "id": "c0550880-0133-47bb-b8dc-d6edda1e10ca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 72,
            "y": 404,
            "speed": 100
        },
        {
            "id": "e32c3dc8-6883-4354-9af5-1a4e0159d3bd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 56,
            "y": 312,
            "speed": 100
        },
        {
            "id": "e72d40ec-ab3c-409d-81a6-b2708499ec6e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 200,
            "speed": 100
        },
        {
            "id": "82058ea9-ece6-49ca-8544-9f8aecfef2cb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 52,
            "y": 116,
            "speed": 100
        },
        {
            "id": "e52cf941-43cd-4c8f-a7d0-94d0c50b1f1c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 68,
            "y": 100,
            "speed": 100
        },
        {
            "id": "5698ffb4-241e-4e19-82c4-7017fd4f9c8a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 84,
            "y": 96,
            "speed": 100
        },
        {
            "id": "e871985f-5309-485a-a850-7319be634039",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 100,
            "y": 100,
            "speed": 100
        },
        {
            "id": "bf14ec87-da06-44b0-960e-289eee780c62",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 108,
            "y": 116,
            "speed": 100
        },
        {
            "id": "4816f5b4-fd3f-4d50-bb7f-cdd1cb3473f0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 112,
            "y": 144,
            "speed": 100
        },
        {
            "id": "c8a8337e-8631-410d-acd6-4425aeaf08db",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 112,
            "y": 164,
            "speed": 100
        },
        {
            "id": "f6b2abeb-0c18-4c1e-9ee1-7b13daafe9e6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 108,
            "y": 212,
            "speed": 100
        },
        {
            "id": "dd45123c-371a-443a-97e7-f8c3558eef57",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 148,
            "y": 212,
            "speed": 100
        },
        {
            "id": "a02da40d-6845-4933-afce-814667735e18",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 140,
            "y": 128,
            "speed": 100
        },
        {
            "id": "498094df-4f42-4b1e-9bb6-ad693f9ba254",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 176,
            "y": 132,
            "speed": 100
        },
        {
            "id": "7782dc54-4051-40a0-933a-e32a25cb6861",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 164,
            "y": 212,
            "speed": 100
        },
        {
            "id": "b0be9268-7f1e-4ac2-a2ff-df9f167a4fe2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 200,
            "y": 212,
            "speed": 100
        },
        {
            "id": "36a8cc36-4fa1-477b-b87a-e08d63786a17",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 204,
            "y": 168,
            "speed": 100
        },
        {
            "id": "ec22338d-de22-4971-b8b2-689b2503f9f1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 212,
            "y": 108,
            "speed": 100
        },
        {
            "id": "a7f77ed4-d5ac-4a2d-b401-f6afc2b04cad",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 252,
            "y": 124,
            "speed": 100
        },
        {
            "id": "2d8f1bb9-b6e9-4c95-b0da-40ab0ec5f5b0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 244,
            "y": 168,
            "speed": 100
        },
        {
            "id": "03a1327a-bf08-4e41-bd6f-905a83ba817d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 248,
            "y": 204,
            "speed": 100
        },
        {
            "id": "80dda0dc-c273-43bc-8e14-7db904b91565",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 260,
            "y": 212,
            "speed": 100
        },
        {
            "id": "dd24bd62-d65e-45a3-a8eb-375581f03e2f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 280,
            "y": 204,
            "speed": 100
        },
        {
            "id": "f30d8fd8-0420-4d8f-99d1-fc2d576a003b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 284,
            "y": 164,
            "speed": 100
        },
        {
            "id": "25b8d7d9-8f46-4d67-92ef-affcc84809e0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 300,
            "y": 140,
            "speed": 100
        },
        {
            "id": "e1dda7a3-f127-4603-a341-a97a8e0471df",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 348,
            "y": 144,
            "speed": 100
        },
        {
            "id": "2959e9e7-14bb-447e-aab0-4f997e75ef37",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 356,
            "y": 184,
            "speed": 100
        },
        {
            "id": "2566e70f-7106-4541-b2e9-667167fb9321",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 352,
            "y": 200,
            "speed": 100
        },
        {
            "id": "047e72a8-d1b0-4658-94d5-912313671ff1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 336,
            "y": 216,
            "speed": 100
        },
        {
            "id": "f7b90f0a-68d2-45c6-9e01-b498bd05f1e2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 280,
            "y": 228,
            "speed": 100
        },
        {
            "id": "8eca3ea4-0d58-4fd1-b660-8c5ea16690dd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 212,
            "y": 240,
            "speed": 100
        },
        {
            "id": "f3eba2ca-8afd-47b1-bc5d-92015200d39f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 252,
            "speed": 100
        },
        {
            "id": "cc1c0b98-e2e2-42a1-9225-8904cc95517b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 100,
            "y": 272,
            "speed": 100
        },
        {
            "id": "a766f460-af94-4e2b-83ad-ddfb296e1504",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 88,
            "y": 300,
            "speed": 100
        },
        {
            "id": "8fa13ff1-bff4-4fed-a44e-8eca75d7c0ed",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 92,
            "y": 324,
            "speed": 100
        },
        {
            "id": "46d96fc3-2464-4bcc-befa-56e232a8a808",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 340,
            "speed": 100
        },
        {
            "id": "6c4ea631-c302-458d-9b06-9cfa37fb0b5c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 120,
            "y": 344,
            "speed": 100
        },
        {
            "id": "df68cb57-9f1e-4bd6-b1a2-d0f916b156c6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 132,
            "y": 320,
            "speed": 100
        },
        {
            "id": "e86d7e1a-228d-443c-9be3-e6f75f8de56e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 136,
            "y": 292,
            "speed": 100
        },
        {
            "id": "2ed8dba8-c776-4797-9c6c-4e93dddac405",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 152,
            "y": 276,
            "speed": 100
        },
        {
            "id": "adb0e62c-8816-408a-9b7c-9ef99fdb086f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 172,
            "y": 276,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 4
}