{
    "id": "3eea049e-e1eb-4e81-b0e4-8261066ad1bf",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_rockets",
    "closed": false,
    "hsnap": 4,
    "kind": 1,
    "points": [
        {
            "id": "b45a2200-b66f-4e5a-89c6-6347fcb88a07",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 376,
            "y": 964,
            "speed": 100
        },
        {
            "id": "79ac1a2d-790e-428a-a113-d62605d03748",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 400,
            "y": 920,
            "speed": 100
        },
        {
            "id": "7f3fcae3-fe67-4490-90ed-21b831a6b4fd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 504,
            "y": 444,
            "speed": 100
        },
        {
            "id": "6fd6be7e-e19e-46c6-836a-d22e103898fa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 540,
            "y": 344,
            "speed": 100
        },
        {
            "id": "b42dc326-3b01-44d4-ac7c-9b1dfc772834",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 540,
            "y": 264,
            "speed": 100
        },
        {
            "id": "b645a54e-a38c-46e5-8d3d-1d3712e095de",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 508,
            "y": 248,
            "speed": 100
        },
        {
            "id": "259313d9-1ec2-4889-97a3-d02b86f0fda6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 468,
            "y": 248,
            "speed": 100
        },
        {
            "id": "39d04c73-57ff-4134-81cf-94a69cef8106",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 452,
            "y": 260,
            "speed": 100
        },
        {
            "id": "cc51f262-36ea-4cbe-9260-ab2b58a1f8b0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 452,
            "y": 276,
            "speed": 100
        },
        {
            "id": "2002fb8e-3892-4666-a55d-9f3b246b784a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 460,
            "y": 288,
            "speed": 100
        },
        {
            "id": "0fd60908-9f72-4b8c-b929-3a99148145d0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 476,
            "y": 296,
            "speed": 100
        },
        {
            "id": "f54d2096-7953-4e08-a6bc-a76bdc1f1b98",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 488,
            "y": 308,
            "speed": 100
        },
        {
            "id": "a9b56496-4d7e-4eb7-a20c-8b30543d7f93",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 488,
            "y": 320,
            "speed": 100
        },
        {
            "id": "3f0f4e3e-33c1-4b95-a67f-2a6e976a315e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 476,
            "y": 328,
            "speed": 100
        },
        {
            "id": "11e7a643-1e42-4d0d-ad03-29ec261227d9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 456,
            "y": 328,
            "speed": 100
        },
        {
            "id": "31499c3b-15d6-456f-bb0a-5edd833b42ec",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 440,
            "y": 320,
            "speed": 100
        },
        {
            "id": "58637f97-3608-46c2-ba73-49a049517975",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 432,
            "y": 304,
            "speed": 100
        },
        {
            "id": "5284d5fa-2aab-4b5d-ad29-2848ae3170d1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 424,
            "y": 284,
            "speed": 100
        },
        {
            "id": "a1131954-4477-4f08-ae83-aefe63d5092e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 416,
            "y": 272,
            "speed": 100
        },
        {
            "id": "c85a475d-2ace-482b-84d3-989a86a37d00",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 396,
            "y": 268,
            "speed": 100
        },
        {
            "id": "dda24ec9-c19c-4999-b0c2-f0ff75b898ba",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 388,
            "y": 280,
            "speed": 100
        },
        {
            "id": "e60ba547-92d0-4bf7-b309-bd66f5be4b12",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 376,
            "y": 312,
            "speed": 100
        },
        {
            "id": "19b299e5-aecb-4118-b738-4f6f0e6d9e50",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 368,
            "y": 336,
            "speed": 100
        },
        {
            "id": "1e2698d2-851c-4c3e-9801-14089e9929ea",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 348,
            "y": 336,
            "speed": 100
        },
        {
            "id": "91a3d164-2733-4f9a-91d5-c83412371ac7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 332,
            "y": 320,
            "speed": 100
        },
        {
            "id": "c9f85643-d1ae-4e81-97c1-5a71c24e6817",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 296,
            "speed": 100
        },
        {
            "id": "bc5ce84a-2b49-40b7-a482-d3a8e83bae83",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 312,
            "y": 268,
            "speed": 100
        },
        {
            "id": "0e783ab5-d6ee-4817-8ff4-5d90ee8ce94a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 296,
            "y": 264,
            "speed": 100
        },
        {
            "id": "71eb9f0e-f35c-45ba-92b7-47937238f3c7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 280,
            "y": 276,
            "speed": 100
        },
        {
            "id": "55246bbd-e8d4-4377-9491-8aaa83e0c1e2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 264,
            "y": 296,
            "speed": 100
        },
        {
            "id": "54b2c257-0774-4346-9ea5-0aa6c09b5d49",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 320,
            "speed": 100
        },
        {
            "id": "2379cff8-2da8-477e-90a0-5588bbddacf9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 244,
            "y": 336,
            "speed": 100
        },
        {
            "id": "a11ee2f5-2905-4cec-90b9-05c60210d2a4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 340,
            "speed": 100
        },
        {
            "id": "d6739bc4-70fd-44d4-9f60-a2029b857329",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 208,
            "y": 328,
            "speed": 100
        },
        {
            "id": "ef8170df-529c-446c-a586-f1b3f27a381c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 204,
            "y": 300,
            "speed": 100
        },
        {
            "id": "acbe6682-34a9-4859-9e28-6a77b7123177",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 196,
            "y": 292,
            "speed": 100
        },
        {
            "id": "70c5c576-2e16-4640-94d3-9fc9cece802f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 184,
            "y": 288,
            "speed": 100
        }
    ],
    "precision": 8,
    "vsnap": 4
}