{
    "id": "0242f5b0-fa05-411e-b884-b902d3740cae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship24debri3",
    "eventList": [
        {
            "id": "184ba587-adf4-478a-ab57-c7ea171c49a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0242f5b0-fa05-411e-b884-b902d3740cae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
    "visible": true
}