{
    "id": "44f88356-9a5a-47d2-babb-921217897144",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked29",
    "eventList": [
        {
            "id": "1930d040-e280-413b-adae-1e59230ab636",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "44f88356-9a5a-47d2-babb-921217897144"
        },
        {
            "id": "7f70b55e-7675-4ca5-b291-9ff888248209",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "44f88356-9a5a-47d2-babb-921217897144"
        },
        {
            "id": "16e64633-dd1b-48ba-8241-6bedf81e3008",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "44f88356-9a5a-47d2-babb-921217897144"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c1f4b280-3cd5-4296-92e2-2f9ad6186b31",
    "visible": true
}