{
    "id": "ddfb14ef-a6bf-4a2b-b474-e2006756e147",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange2button",
    "eventList": [
        {
            "id": "a42a9b89-1cfa-413b-8029-da0c95c901c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ddfb14ef-a6bf-4a2b-b474-e2006756e147"
        },
        {
            "id": "246bf94f-9935-4fb5-935b-e68117160272",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ddfb14ef-a6bf-4a2b-b474-e2006756e147"
        },
        {
            "id": "272b5b11-fa7f-4719-8490-0e6731396623",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ddfb14ef-a6bf-4a2b-b474-e2006756e147"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec95b60b-45c2-449f-80e2-1020a594fdab",
    "visible": true
}