{
    "id": "51967c6d-7380-46ec-b62f-2022c3b572fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship35debri2",
    "eventList": [
        {
            "id": "46e5dd7e-1f9e-4170-a522-f1fb6396629f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51967c6d-7380-46ec-b62f-2022c3b572fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
    "visible": true
}