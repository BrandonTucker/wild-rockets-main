{
    "id": "da3b6ead-d746-448c-8c5f-bb2753a2f33d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship18button",
    "eventList": [
        {
            "id": "b24e5ff3-2d29-4645-8921-9e08fab6bf86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "da3b6ead-d746-448c-8c5f-bb2753a2f33d"
        },
        {
            "id": "a9d5fa0f-e0d5-44c3-8c64-9afdd48bf465",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "da3b6ead-d746-448c-8c5f-bb2753a2f33d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "40c7553a-055c-49bc-8116-04183668eedf",
    "visible": true
}