{
    "id": "e07e795b-1ce9-4f6e-bf98-c1b0554c8822",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lightbluefireball",
    "eventList": [
        {
            "id": "b70409b2-3911-4d7e-bf2b-348b966a88fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e07e795b-1ce9-4f6e-bf98-c1b0554c8822"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1470b819-1795-4256-a239-9cae84d06668",
    "visible": true
}