{
    "id": "a8dacd43-c779-4ac9-87d6-c69add8d14f9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship14button",
    "eventList": [
        {
            "id": "42814e2f-cac9-4f9b-b0b5-b2d2ce699557",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a8dacd43-c779-4ac9-87d6-c69add8d14f9"
        },
        {
            "id": "b9a62f4a-dae8-409a-8700-1211347062f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "a8dacd43-c779-4ac9-87d6-c69add8d14f9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4dbdea5-fa9a-4d96-ab93-f354f450b4d5",
    "visible": true
}