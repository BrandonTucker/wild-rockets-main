{
    "id": "d2f80642-f85e-47e1-83bc-a440ba2de566",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challangecomplete",
    "eventList": [
        {
            "id": "0f77f1d8-fe23-4af1-9fe0-deb51d89c550",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d2f80642-f85e-47e1-83bc-a440ba2de566"
        },
        {
            "id": "ab676798-8de3-498a-80bc-370a7e4821aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "d2f80642-f85e-47e1-83bc-a440ba2de566"
        },
        {
            "id": "dac8c262-ad9d-4f6c-a956-7775dffb76dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d2f80642-f85e-47e1-83bc-a440ba2de566"
        },
        {
            "id": "6eca6852-c067-4cd0-824f-06388479c4e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d2f80642-f85e-47e1-83bc-a440ba2de566"
        },
        {
            "id": "8fd7f3a0-e7f4-4355-901a-0a2b64a740d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d2f80642-f85e-47e1-83bc-a440ba2de566"
        },
        {
            "id": "6ef5495b-4f51-4589-b3b8-0632eb371344",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "d2f80642-f85e-47e1-83bc-a440ba2de566"
        },
        {
            "id": "c6cc59b9-874e-49ba-84b6-a746499991f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d2f80642-f85e-47e1-83bc-a440ba2de566"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
    "visible": true
}