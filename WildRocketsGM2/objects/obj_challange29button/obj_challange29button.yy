{
    "id": "4486cabf-ea7d-4033-b975-6b04d52f01a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange29button",
    "eventList": [
        {
            "id": "99b14b2d-d47b-4a97-a37f-8b1ff19567e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4486cabf-ea7d-4033-b975-6b04d52f01a5"
        },
        {
            "id": "984565aa-8a17-4072-8ed4-f4f713718b8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4486cabf-ea7d-4033-b975-6b04d52f01a5"
        },
        {
            "id": "700073ce-5f84-4f6d-9bdb-9bfa4024017c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4486cabf-ea7d-4033-b975-6b04d52f01a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "92388c1d-bb6d-456b-9906-e9556f5c24be",
    "visible": true
}