{
    "id": "9e1588c0-20db-49b8-a2c5-a1edecd1407d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_yellowfireball2",
    "eventList": [
        {
            "id": "cfb3749a-a42a-4dee-aa55-0a5dc0c73a0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e1588c0-20db-49b8-a2c5-a1edecd1407d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0989f38-e81c-4b6a-9efc-7ff51fabeb8c",
    "visible": true
}