{
    "id": "0672b1fc-a2a7-4787-a4ae-658bf7be3505",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shipdisplay",
    "eventList": [
        {
            "id": "8a48e1a1-ca3a-41fd-af5e-dd131b57ca35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0672b1fc-a2a7-4787-a4ae-658bf7be3505"
        },
        {
            "id": "80103dd4-913e-4dbe-a73f-511796ef3a95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0672b1fc-a2a7-4787-a4ae-658bf7be3505"
        },
        {
            "id": "f7c2c6a5-97e6-465c-abee-56782d39fff1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0672b1fc-a2a7-4787-a4ae-658bf7be3505"
        },
        {
            "id": "7dcd58b5-052e-4b1b-a83a-75cb097d3f19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0672b1fc-a2a7-4787-a4ae-658bf7be3505"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}