instance_create(x + 10, y + 180, obj_blueexplosion1);

for( i = 0; i < 24; i++){
	action_create_object(obj_spark, x+random_range(-30,30), y+random_range(140,180));
}

audio_play_sound(snd_explosion3, 10, false);

if(global.newshipunlocked == 1){
	instance_create(x, y, obj_unlocksparklecreate);
	global.newshipunlocked = 0;
}
