{
    "id": "4dff55ad-7f7a-43a8-9a95-dbc37388c21e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shipintro",
    "eventList": [
        {
            "id": "0f3fc172-5ca1-4a73-a32f-1e36f4f98051",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4dff55ad-7f7a-43a8-9a95-dbc37388c21e"
        },
        {
            "id": "d28f6e10-d42f-4aaf-9a25-fb1fb57e9c53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4dff55ad-7f7a-43a8-9a95-dbc37388c21e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "a8b9793d-3aa7-4cce-b355-45c56ca8a2b4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "7b63104d-1a2b-4415-9bca-3d6b735da6b5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 28
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
    "visible": true
}