{
    "id": "ecc0b0c8-8467-4c89-bd42-d7d3803ff91b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_creditsbutton",
    "eventList": [
        {
            "id": "4d47fe2a-3248-4cb5-86c4-b288b15a84f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ecc0b0c8-8467-4c89-bd42-d7d3803ff91b"
        },
        {
            "id": "b8d479c5-34e5-4eab-9f52-24591d44d160",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ecc0b0c8-8467-4c89-bd42-d7d3803ff91b"
        },
        {
            "id": "8db40c14-7981-4262-b84a-32c1e69e74f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ecc0b0c8-8467-4c89-bd42-d7d3803ff91b"
        },
        {
            "id": "3b423314-ea4c-44d1-88fb-2cd8ea21da41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ecc0b0c8-8467-4c89-bd42-d7d3803ff91b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "visible": true
}