{
    "id": "9eb542f5-a8a7-4143-a0b2-c14b077f5566",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship37button",
    "eventList": [
        {
            "id": "df69318d-31d9-4a9c-823f-bd669156c93b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9eb542f5-a8a7-4143-a0b2-c14b077f5566"
        },
        {
            "id": "d2cce3cf-d1cc-43e2-b011-a35369820e1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "9eb542f5-a8a7-4143-a0b2-c14b077f5566"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bf5a0591-b61b-4be2-ae56-f02adb1ea4f5",
    "visible": true
}