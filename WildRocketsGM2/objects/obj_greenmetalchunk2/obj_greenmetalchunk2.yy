{
    "id": "a87759a9-c1c5-4b54-8d9c-f02ab0b10716",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greenmetalchunk2",
    "eventList": [
        {
            "id": "ae90a5b3-6438-4634-8d6c-4a63d74307b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a87759a9-c1c5-4b54-8d9c-f02ab0b10716"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
    "visible": true
}