{
    "id": "465f42dd-1cc4-4132-86ea-3432fcd0bfc7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked12",
    "eventList": [
        {
            "id": "5dc10bc8-16cd-4de9-930a-673ae252d9df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "465f42dd-1cc4-4132-86ea-3432fcd0bfc7"
        },
        {
            "id": "6e17e59a-c689-4792-88a7-5ef637f7ebe3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "465f42dd-1cc4-4132-86ea-3432fcd0bfc7"
        },
        {
            "id": "a108450d-6964-4039-8694-1b5d0d530691",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "465f42dd-1cc4-4132-86ea-3432fcd0bfc7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc9c153a-fe88-47cc-8544-00b4e5b680c0",
    "visible": true
}