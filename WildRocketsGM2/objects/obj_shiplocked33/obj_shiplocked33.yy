{
    "id": "add341d8-2e44-485c-90f2-e837fd3aadce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked33",
    "eventList": [
        {
            "id": "21e61d56-8b09-474e-a940-e59534bbde64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "add341d8-2e44-485c-90f2-e837fd3aadce"
        },
        {
            "id": "27a22d8a-f251-48ce-bed9-6d8f949db7ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "add341d8-2e44-485c-90f2-e837fd3aadce"
        },
        {
            "id": "a318c3c9-f839-4ea4-8e43-1edc9b5a58a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "add341d8-2e44-485c-90f2-e837fd3aadce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
    "visible": true
}