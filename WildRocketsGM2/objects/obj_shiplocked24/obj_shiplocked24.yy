{
    "id": "ee0cf504-0534-4843-a887-16951c0140fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked24",
    "eventList": [
        {
            "id": "f7c8f4f1-ea4d-4a4b-ad6d-9f0a7ac07bfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ee0cf504-0534-4843-a887-16951c0140fc"
        },
        {
            "id": "4a51704d-c37e-4b87-9d9d-90fc2ca35e0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ee0cf504-0534-4843-a887-16951c0140fc"
        },
        {
            "id": "aaa5e447-566c-4b05-8612-ddeeb3973191",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ee0cf504-0534-4843-a887-16951c0140fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "67e6f697-50d3-4ea9-9bc8-fad547f0c206",
    "visible": true
}