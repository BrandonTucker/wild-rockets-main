{
    "id": "7dfe14c6-4aab-4704-9dba-2dfd38461d98",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship20button",
    "eventList": [
        {
            "id": "d0e0ee4e-791b-4ff5-8941-96bfe45e2609",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7dfe14c6-4aab-4704-9dba-2dfd38461d98"
        },
        {
            "id": "dbd2c1f8-46bb-4fa5-9075-6ccfd86b4f3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7dfe14c6-4aab-4704-9dba-2dfd38461d98"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "72d575c9-e41f-4e81-b4fc-8541811d0071",
    "visible": true
}