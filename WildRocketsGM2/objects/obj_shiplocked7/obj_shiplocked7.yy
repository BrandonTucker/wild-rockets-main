{
    "id": "bdc7ac68-26e3-42d2-9ead-7e08b7929920",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked7",
    "eventList": [
        {
            "id": "f5a80338-2956-415b-a8ff-ef77c7a44382",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bdc7ac68-26e3-42d2-9ead-7e08b7929920"
        },
        {
            "id": "9a0cb74a-4e67-4b26-849c-95b996224565",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bdc7ac68-26e3-42d2-9ead-7e08b7929920"
        },
        {
            "id": "9403a7d9-6809-42b8-ac9c-8e1f328d90c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "bdc7ac68-26e3-42d2-9ead-7e08b7929920"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a038f436-c573-4969-9eb1-dd2b1af9c4ec",
    "visible": true
}