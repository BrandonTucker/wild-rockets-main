{
    "id": "90b79005-00bc-4eaf-8c9b-8032644dbe43",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_block2",
    "eventList": [
        {
            "id": "ce45b102-d98f-41e0-ac0d-84758c4070f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90b79005-00bc-4eaf-8c9b-8032644dbe43"
        },
        {
            "id": "f9d5e0ae-cba2-46c4-8966-cd3d6322bf9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "90b79005-00bc-4eaf-8c9b-8032644dbe43"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8ed2210-b224-4a3c-869f-2cff7d7b0c23",
    "visible": true
}