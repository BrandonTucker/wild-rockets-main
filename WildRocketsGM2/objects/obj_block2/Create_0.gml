action_set_relative(1);
var __b__;
__b__ = action_if_dice(4);
if __b__
{
{
__b__ = action_if_variable(x, 320+32, 3);
if __b__
{
{
action_create_object(obj_blockgap, 0, 0);
var purplblock = false;


if(irandom(5)==5){
    var xpos = 32;
    var blockx = (x+7*32)-((4*32)/2);
    var purplblock = true;
    for(i = 1; i < 4; i++){
        instance_create(blockx+xpos,y,obj_blockdestructable2);
        xpos+=32;
    }
}


var chance = irandom(10000);

if(purplblock == false){
    if(chance > 9990) instance_create(x+7*32,y,obj_diamond);
    if(chance > 9900 && chance < 9990) instance_create(x+7*32,y,obj_ruby);
    if(chance > 9750 && chance < 9900) instance_create(x+7*32,y,obj_emerald);
    if(chance > 9500 && chance < 9750) instance_create(x+7*32,y,obj_bananas);
    if(chance > 9250 && chance < 9500) instance_create(x+7*32,y,obj_pineapple);
    if(chance > 9000 && chance < 9250) instance_create(x+7*32,y,obj_grapes);
    if(chance > 6000 && chance < 9000) instance_create(x+7*32,y,obj_coinstring);
    if(chance > 5500 && chance < 6000) {
        instance_create(x+7*32+10,y,obj_coinstring);
        instance_create(x+7*32-10,y,obj_coinstring);
    }
    
}

action_create_object(obj_blockgap, (3*32)+(6*32), 0);
action_kill_object();
}
}
}
}
action_set_relative(0);
