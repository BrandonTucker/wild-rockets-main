{
    "id": "9b4acd81-ee28-472d-9d19-1dbf7f644bfc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship40debri2",
    "eventList": [
        {
            "id": "c649e3e7-daa7-49ca-8ae8-2def25958798",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9b4acd81-ee28-472d-9d19-1dbf7f644bfc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
    "visible": true
}