{
    "id": "05c8fdd3-14c6-4a8a-a387-f531dc0c5c8d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship28button",
    "eventList": [
        {
            "id": "7950a377-1952-43c5-8b2e-22b21714fd78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "05c8fdd3-14c6-4a8a-a387-f531dc0c5c8d"
        },
        {
            "id": "d4073780-5d1b-4e2c-bc96-19fb6082849a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "05c8fdd3-14c6-4a8a-a387-f531dc0c5c8d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eb5046a0-1ba5-4939-872a-a4452528107d",
    "visible": true
}