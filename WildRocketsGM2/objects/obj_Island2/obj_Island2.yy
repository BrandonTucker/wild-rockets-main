{
    "id": "2ad1b47d-6a7a-4aea-92fd-810c6f542431",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Island2",
    "eventList": [
        {
            "id": "786e7517-6e1f-460e-ac78-cc67cda0f60c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ad1b47d-6a7a-4aea-92fd-810c6f542431"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "d721724f-a7f3-4dc8-8efb-e26b1604da21",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "0c26359e-c44a-4440-a043-4aaafb1dab81",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 28
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db93d53c-74f7-44b0-bce7-6a689e850cb4",
    "visible": true
}