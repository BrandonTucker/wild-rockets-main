{
    "id": "3eb9a520-1bc0-4ffe-ad64-ffff30f0bede",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ghostblock",
    "eventList": [
        {
            "id": "7b9366e1-9d2d-4521-a2c9-be3bb267f148",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3eb9a520-1bc0-4ffe-ad64-ffff30f0bede"
        },
        {
            "id": "1712a8cb-8e83-441a-8a65-b9abfde41ac5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3eb9a520-1bc0-4ffe-ad64-ffff30f0bede"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b24c17f-9c8a-4714-a775-095171399af9",
    "visible": true
}