{
    "id": "d9f548ca-c66e-4d6b-8a9d-1c1a6d23299a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked39",
    "eventList": [
        {
            "id": "d5554301-731d-47f5-be63-dcb296306c39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9f548ca-c66e-4d6b-8a9d-1c1a6d23299a"
        },
        {
            "id": "e077615c-6a0f-417e-a8e5-56ac39523b0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d9f548ca-c66e-4d6b-8a9d-1c1a6d23299a"
        },
        {
            "id": "0cb0bedc-6fc8-4cac-bcb1-fed17c946243",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d9f548ca-c66e-4d6b-8a9d-1c1a6d23299a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
    "visible": true
}