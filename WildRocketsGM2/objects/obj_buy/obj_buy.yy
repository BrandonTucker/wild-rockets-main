{
    "id": "943364ed-c1ad-4a0a-94d3-3281ef08c177",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_buy",
    "eventList": [
        {
            "id": "9b1e4ea9-38b0-46ac-b9c8-6d42d5db397f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "943364ed-c1ad-4a0a-94d3-3281ef08c177"
        },
        {
            "id": "9ef47155-8930-453a-9d20-3d1ab32dc039",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "943364ed-c1ad-4a0a-94d3-3281ef08c177"
        },
        {
            "id": "0c043c92-9727-4821-bd64-c06466bf1012",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "943364ed-c1ad-4a0a-94d3-3281ef08c177"
        },
        {
            "id": "0b7301d5-6f09-4890-a141-eb92bc98636d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "943364ed-c1ad-4a0a-94d3-3281ef08c177"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
    "visible": true
}