{
    "id": "d5cc4079-efef-49ca-9852-1cabb80529dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship34button",
    "eventList": [
        {
            "id": "948ffcd8-10c8-413b-9adf-4b17464d5583",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d5cc4079-efef-49ca-9852-1cabb80529dc"
        },
        {
            "id": "9df52ffe-fbc4-46c2-ac34-a9a76795c7a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d5cc4079-efef-49ca-9852-1cabb80529dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "84dca0ca-0709-4a23-8b69-d031e59a7465",
    "visible": true
}