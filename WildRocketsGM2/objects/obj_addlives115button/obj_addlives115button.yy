{
    "id": "3cdee578-6875-433e-abb4-eece947587d8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_addlives115button",
    "eventList": [
        {
            "id": "3c97e349-270c-4cfa-9555-7b23cb086a6a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3cdee578-6875-433e-abb4-eece947587d8"
        },
        {
            "id": "10cc7270-c8a1-4551-966b-6d70d335cb8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "3cdee578-6875-433e-abb4-eece947587d8"
        },
        {
            "id": "e51b8456-e524-4675-9e73-9335c128cb48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3cdee578-6875-433e-abb4-eece947587d8"
        },
        {
            "id": "659e0157-f362-46ab-ba2c-3656799d0504",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3cdee578-6875-433e-abb4-eece947587d8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "visible": true
}