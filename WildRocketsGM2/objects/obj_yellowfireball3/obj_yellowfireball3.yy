{
    "id": "e9685e76-ca88-48f3-83b3-40457e02ffcd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_yellowfireball3",
    "eventList": [
        {
            "id": "b58f8488-7b3e-472a-bc42-9c04901fe2ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e9685e76-ca88-48f3-83b3-40457e02ffcd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0989f38-e81c-4b6a-9efc-7ff51fabeb8c",
    "visible": true
}