{
    "id": "ee1f1bf5-683e-4d1d-baf3-a843eccecc27",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange16button",
    "eventList": [
        {
            "id": "e3ca6b4d-1b9b-4c70-bbcf-b5a02aeb9675",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ee1f1bf5-683e-4d1d-baf3-a843eccecc27"
        },
        {
            "id": "9ea71d0d-5384-4568-aca0-d161817ad0b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ee1f1bf5-683e-4d1d-baf3-a843eccecc27"
        },
        {
            "id": "b315a18b-f11f-45e6-bc4f-5650f88ea733",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ee1f1bf5-683e-4d1d-baf3-a843eccecc27"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e449ee5f-33dc-4377-9b06-2e4c0bd23a6c",
    "visible": true
}