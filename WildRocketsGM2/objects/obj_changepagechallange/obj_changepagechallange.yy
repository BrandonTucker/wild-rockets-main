{
    "id": "59c5a56d-bfcf-4647-b9d1-5d3818570a6a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_changepagechallange",
    "eventList": [
        {
            "id": "e0b84885-9a6d-4bb3-98d3-ee4abfe73053",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "59c5a56d-bfcf-4647-b9d1-5d3818570a6a"
        },
        {
            "id": "0b157ed9-9cb7-4839-ad2f-101cc2d1c471",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "59c5a56d-bfcf-4647-b9d1-5d3818570a6a"
        },
        {
            "id": "10fd132c-42e4-41b5-8d5d-024148d811dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "59c5a56d-bfcf-4647-b9d1-5d3818570a6a"
        },
        {
            "id": "545e04b7-a7bf-4b64-9cfa-8ea60dc4a6d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "59c5a56d-bfcf-4647-b9d1-5d3818570a6a"
        },
        {
            "id": "e4228533-6296-462b-a1c1-3474b9a4b271",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "59c5a56d-bfcf-4647-b9d1-5d3818570a6a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a132351-f771-425b-9ba7-169939928823",
    "visible": true
}