{
    "id": "52b10b7b-0de7-45ba-9e92-db546d141df8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rareship10",
    "eventList": [
        {
            "id": "cce5849b-677b-4e9e-9ffa-ac8c3ee0cc80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52b10b7b-0de7-45ba-9e92-db546d141df8"
        },
        {
            "id": "6630c0e1-96d6-42cd-a8c2-1a5eb13c060c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "52b10b7b-0de7-45ba-9e92-db546d141df8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d23bb1f-1edc-411b-96e6-de1f82a81d36",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
    "visible": true
}