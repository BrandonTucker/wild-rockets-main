{
    "id": "0fba20ba-ec25-4da5-a088-f3cf8ef354d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked32",
    "eventList": [
        {
            "id": "02355349-bbf8-4f42-8719-b6a7bce7cb34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0fba20ba-ec25-4da5-a088-f3cf8ef354d7"
        },
        {
            "id": "92196f63-bcbe-42a1-af79-f59720ea7dd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0fba20ba-ec25-4da5-a088-f3cf8ef354d7"
        },
        {
            "id": "f11e56ca-ef24-4eaa-bb6b-b60b8b4f3526",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0fba20ba-ec25-4da5-a088-f3cf8ef354d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
    "visible": true
}