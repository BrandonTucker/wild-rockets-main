{
    "id": "8a163dd2-63ab-4df9-b4bf-c8cb57357a95",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange23button",
    "eventList": [
        {
            "id": "0b4126f3-a868-4a04-b90a-7e2b46a05b02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a163dd2-63ab-4df9-b4bf-c8cb57357a95"
        },
        {
            "id": "a18207ec-72b9-40c1-ad0b-8fa41a6048d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8a163dd2-63ab-4df9-b4bf-c8cb57357a95"
        },
        {
            "id": "9ccd25d9-4762-4401-89fc-4d7008572220",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8a163dd2-63ab-4df9-b4bf-c8cb57357a95"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "25abf59e-169b-46de-ba1b-f2bc1097a59c",
    "visible": true
}