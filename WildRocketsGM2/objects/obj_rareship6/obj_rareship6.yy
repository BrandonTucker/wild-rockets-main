{
    "id": "a013f065-7519-4066-a329-15091fd19861",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rareship6",
    "eventList": [
        {
            "id": "f7a0120e-92af-4f54-a088-6660f987db6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a013f065-7519-4066-a329-15091fd19861"
        },
        {
            "id": "6a0a7b9a-4360-4733-8ea4-01d3153436dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a013f065-7519-4066-a329-15091fd19861"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d23bb1f-1edc-411b-96e6-de1f82a81d36",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
    "visible": true
}