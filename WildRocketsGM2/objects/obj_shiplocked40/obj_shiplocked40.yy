{
    "id": "2d835daf-ef36-4e3e-b17d-d9202222cf06",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked40",
    "eventList": [
        {
            "id": "652948b3-a0db-4a5b-bedd-c49cb350e957",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2d835daf-ef36-4e3e-b17d-d9202222cf06"
        },
        {
            "id": "57c1a267-ba0d-4bb2-9530-25c521d65ab3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2d835daf-ef36-4e3e-b17d-d9202222cf06"
        },
        {
            "id": "c771a15f-d488-4d40-9206-e4a5fdc1a354",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2d835daf-ef36-4e3e-b17d-d9202222cf06"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
    "visible": true
}