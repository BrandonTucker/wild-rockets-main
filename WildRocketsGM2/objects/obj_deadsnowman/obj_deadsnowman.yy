{
    "id": "77a24408-ddec-41f3-b2e6-d8b2c0ff3417",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_deadsnowman",
    "eventList": [
        {
            "id": "b5632ac7-3eb5-4d8f-b36d-70ba7400762a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77a24408-ddec-41f3-b2e6-d8b2c0ff3417"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
    "visible": true
}