{
    "id": "89248edb-62dd-45ad-bdbe-132da5352730",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wildrocketsanimation",
    "eventList": [
        {
            "id": "caee9341-42a7-4eef-bc35-4e9dac396efe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89248edb-62dd-45ad-bdbe-132da5352730"
        },
        {
            "id": "e99c5cb4-b2c2-4766-b5ff-634fcb837f5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "89248edb-62dd-45ad-bdbe-132da5352730"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "419b9bba-9661-4264-b503-f88028deaab6",
    "visible": true
}