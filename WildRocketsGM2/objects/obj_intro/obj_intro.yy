{
    "id": "0218f492-f7f3-49b6-beb9-64f2a03c3d22",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_intro",
    "eventList": [
        {
            "id": "43ba2c05-37e3-4cd5-9d93-5331a5e49a6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0218f492-f7f3-49b6-beb9-64f2a03c3d22"
        },
        {
            "id": "8c62328f-8616-4a5c-8f00-1dca4bae3279",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 9,
            "eventtype": 2,
            "m_owner": "0218f492-f7f3-49b6-beb9-64f2a03c3d22"
        },
        {
            "id": "4a2814ad-bfe1-4b16-9ec1-8426e0101d46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0218f492-f7f3-49b6-beb9-64f2a03c3d22"
        },
        {
            "id": "763f460e-e876-4ca4-8245-3aad2aab4b38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0218f492-f7f3-49b6-beb9-64f2a03c3d22"
        },
        {
            "id": "c5523436-0308-41c5-bc9e-af30f77e020e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "0218f492-f7f3-49b6-beb9-64f2a03c3d22"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}