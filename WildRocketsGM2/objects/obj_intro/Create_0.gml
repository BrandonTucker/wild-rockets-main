global.ingame = 0;
global.useLife = false;
global.challangenumber = 0;
global.lock = 0;
global.newshipunlocked = 0;
global.drawcoins = 0;
global.lifeCount = 0;
canclick = 0;
global.fuelframe = 0;
global.buyfullversionactive = 0;
global.free = 0;
global.buyfullversionroom = 0;
global.count = 0;
global.dailyReward = false;
global.lifeUsed = false;
global.shipcollected = false;
global.templock = 0;

global.ads_app_id = "ca-app-pub-2228303184657007~6391179659";
global.rewarded_id = "ca-app-pub-2228303184657007/1517819430";

GoogleMobileAds_Init(global.rewarded_id, global.ads_app_id);

GoogleMobileAds_LoadRewardedVideo(global.rewarded_id);
global.rewarded_loaded = false;
global.rewarded_viewed = false;


randomize();
ini_open(working_directory + "\\rocketlocksIni.ini");
for (i = 0; i < 39; i += 1){
   global.rocketLocksArray[i] = ini_read_real("locks", string(i), -1);
}
ini_close();



globalvar coins;
global.coins=0;

globalvar money;
global.money = 0;

ini_open(working_directory + "\\moneyIni.ini");
global.money = ini_read_real("money", "money", 0);
ini_close();


var i, file;

ini_open(working_directory + "\\challengelocksIni.ini");
for (i = 0; i < 95; i += 1){
   global.challengeLocksArray[i] = ini_read_real("locks", string(i), -1);
}
ini_close();


ini_open(working_directory + "\\HighScoreIni.ini");
global.highscore = ini_read_real("highscore", "highscore", -1);
ini_close();


ini_open(working_directory + "\\RecordComboIni.ini");
global.recordcombo = ini_read_real("recordcombo", "recordcombo", -1);
ini_close();

ini_open(working_directory + "\\introfileIni.ini");
global.firstopen = ini_read_real("intro", "intro", -1);
global.payday = ini_read_real("payday", "payday", 0);
ini_close();


ini_open(working_directory + "\\introfileIni.ini");
ini_write_real( "intro", "intro", 1);
ini_close();

ini_open(working_directory + "\\shipnumberIni.ini");
global.ship = ini_read_real("shipnumber", "shipnumber", 1);
ini_close();

ini_open(working_directory + "\\LastDailyRewardDateIni.ini");
var lastDate = ini_read_real("Date", "lastDate", 10);
ini_close();

ini_open(working_directory + "\\livesIni.ini");
global.lifeCount = ini_read_real("Lives", "lives", 0);
ini_close();


if(date_second_span(lastDate, date_current_datetime()) > 1){
	global.dailyReward = true;
	ini_open(working_directory + "\\LastDailyRewardDateIni.ini");
	ini_write_real("Date", "lastDate", date_current_datetime());
	ini_close();
}


global.ingame = 1;
canclick = 0;
action_set_alarm(45, 9);
script_execute(Scr_frame_rate,0)