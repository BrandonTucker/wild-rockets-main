action_set_relative(1);
action_sprite_set(spr_ship10explode, 6, 0);
var __b__;
__b__ = action_if_variable(global.ship, 10, 0);
if __b__
{
action_create_object(obj_crumpledship, 0, 0);
}
action_set_relative(0);

var __b__;

__b__ = action_if_variable(global.challange, 1, 0);
if __b__
{
{
	
	if(global.count > 1){
		
		action_create_object(obj_gameover, 320, __view_get( e__VW.YView, 0 )+700);
		action_create_object(obj_retrybutton, 320, __view_get( e__VW.YView, 0 )+575);
		action_create_object(obj_lifebutton, 320, __view_get( e__VW.YView, 0 )+702);
		action_create_object(obj_quitbutton, 320, __view_get( e__VW.YView, 0 )+830);
	}else{
		action_create_object(obj_gameover, 320, __view_get( e__VW.YView, 0 )+670);
		action_create_object(obj_retrybutton, 320, __view_get( e__VW.YView, 0 )+580);
		action_create_object(obj_quitbutton, 320, __view_get( e__VW.YView, 0 )+760);
	}

}
}
else
{
{
action_another_room(rm_highscore);
}
}


instance_destroy();