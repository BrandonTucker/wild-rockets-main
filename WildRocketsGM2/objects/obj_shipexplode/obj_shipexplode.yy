{
    "id": "d7e1915c-e9c5-4332-8e55-a3c6231bc0b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shipexplode",
    "eventList": [
        {
            "id": "1ee3d274-109a-4f3e-90d5-b109468c16f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7e1915c-e9c5-4332-8e55-a3c6231bc0b3"
        },
        {
            "id": "2b7a2714-6dd3-4d17-ac76-321204cd19b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d7e1915c-e9c5-4332-8e55-a3c6231bc0b3"
        },
        {
            "id": "90464504-7fb0-41cf-9d61-7ddb79ddd115",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d7e1915c-e9c5-4332-8e55-a3c6231bc0b3"
        },
        {
            "id": "55ae2117-a045-477c-b166-c656ad14d907",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "36f806d0-6138-48be-b271-e06756dafca8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d7e1915c-e9c5-4332-8e55-a3c6231bc0b3"
        },
        {
            "id": "47faf232-f6d1-41ce-bcd7-9b04a6f64dac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e58f85a3-957f-493b-bbd0-f7b212ae3a64",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d7e1915c-e9c5-4332-8e55-a3c6231bc0b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "57a998fe-54ba-46f4-971b-387f8312b674",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "0ace5cc8-3468-4f5b-8c1e-818969b54822",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a28f5a00-9c70-425a-a496-8e32a6c42e81",
    "visible": true
}