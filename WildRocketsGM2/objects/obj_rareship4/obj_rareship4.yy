{
    "id": "23976f0b-f35c-4496-a9d0-a771e654cac2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rareship4",
    "eventList": [
        {
            "id": "8355da3e-55f0-4646-bd0f-44ad3336fb1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "23976f0b-f35c-4496-a9d0-a771e654cac2"
        },
        {
            "id": "0740eafc-2b84-43c8-9d79-8a18ddecbd06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "23976f0b-f35c-4496-a9d0-a771e654cac2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d23bb1f-1edc-411b-96e6-de1f82a81d36",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
    "visible": true
}