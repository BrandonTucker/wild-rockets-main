{
    "id": "9bdb0f18-6811-46d2-8809-b394a70f9e31",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship5button",
    "eventList": [
        {
            "id": "8f1224f7-1a18-4122-8967-651f26e55d91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9bdb0f18-6811-46d2-8809-b394a70f9e31"
        },
        {
            "id": "c28d66bd-9b51-4310-b3f9-21231bf9248c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "9bdb0f18-6811-46d2-8809-b394a70f9e31"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "93fc3f63-34ec-4e7d-9e12-4bc21a839e60",
    "visible": true
}