{
    "id": "10608264-c4e3-4971-b590-6135a6b4a8c7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_changepage",
    "eventList": [
        {
            "id": "fe71b59a-454c-4d68-9a1f-eedcad1e6fe6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "10608264-c4e3-4971-b590-6135a6b4a8c7"
        },
        {
            "id": "3292b965-3cac-43d3-ae0a-d2be744ea026",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "10608264-c4e3-4971-b590-6135a6b4a8c7"
        },
        {
            "id": "d2ae12c9-83e5-453a-b34e-6b1aa1ab179b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "10608264-c4e3-4971-b590-6135a6b4a8c7"
        },
        {
            "id": "47adfab3-c920-421e-b8ed-11633e4bb3fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "10608264-c4e3-4971-b590-6135a6b4a8c7"
        },
        {
            "id": "ba453a80-7f05-441f-ae32-8fe55d6c50c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "10608264-c4e3-4971-b590-6135a6b4a8c7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a132351-f771-425b-9ba7-169939928823",
    "visible": true
}