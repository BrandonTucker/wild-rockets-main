{
    "id": "ad819f32-0178-4d04-94fe-7108cab7f0b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireball",
    "eventList": [
        {
            "id": "4f5199bb-da2a-4cf8-bcb6-13269166d5d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad819f32-0178-4d04-94fe-7108cab7f0b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fe1bf52d-7e80-4b7e-af54-0d5fa7ae5983",
    "visible": true
}