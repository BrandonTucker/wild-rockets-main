/// @description IAP
// Get the event type
var _id = iap_data[? "type"];
// Perform different code depending on the type
switch (_id){

    case iap_ev_storeload: // Check to see if the store has loaded or not
        if (iap_data[? "status"] == iap_storeload_ok){
            show_debug_message("STORE LOADED");
        }else{
            show_debug_message("STORE NOT LOADED");
        }
    break;


    case iap_ev_purchase:
        var _product = iap_data[? "index"];
        var _map = ds_map_create();
        iap_purchase_details(_product, _map);
        show_debug_message("PRODUCT PURCHASED - " + string(_product));
        show_debug_message("Product - " + string(_map[? "product"]));
        show_debug_message("Order - " + string(_map[? "order"]));
        show_debug_message("Token - " + string(_map[? "token"]));
        show_debug_message("Payload - " + string(_map[? "payload"]));
        show_debug_message("Receipt - " + string(_map[? "receipt"]));
        show_debug_message("Response - " + string(_map[? "response"]));
        
        /*  
        switch statement inside the case iap_ev_purchase to get 
        more details about the purchase. Like if it failed or 
        they cancelled etc.
        */
        
        switch (_map[? "status"]){
            case iap_available:
                show_debug_message("iap_available");
                break;
            case iap_failed:
                show_debug_message("iap_failed");
                break;
            case iap_purchased:
                show_debug_message("iap_purchased");
                    show_debug_message("Consumable IAP Purchased");
                    //iap_consume calls iap_ev_consume (below)
                    iap_consume(product_consumable); 
                break;
            case iap_canceled:
                show_debug_message("iap_canceled");
                break;
            case iap_refunded:
                show_debug_message("iap_refunded");
                break;
        }
    
    ds_map_destroy(_map);
    break;

    case iap_ev_consume:
        /*
        This is where you add the gold/gems. Make sure you
        save this data to a ini file or something. Beacuse Currently
        its just updating the ds_map.
        */
		global.lifeCount += 55;
        global.savemap[? "consumed"] = global.savemap[? "consumed"] + 55;
        ds_map_secure_save(global.savemap, "iap_data.dat");
        show_debug_message("55 Lives Added");
    break;

}

/* */
/*  */
