{
    "id": "cb3f37d2-1d3a-425c-9313-53fae6589b5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oControl",
    "eventList": [
        {
            "id": "c6efab2a-c0ea-4a4d-b233-32104c16ba43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 66,
            "eventtype": 7,
            "m_owner": "cb3f37d2-1d3a-425c-9313-53fae6589b5f"
        },
        {
            "id": "74a520a5-4de3-4fcb-89c8-1df47f5b18bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "cb3f37d2-1d3a-425c-9313-53fae6589b5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}