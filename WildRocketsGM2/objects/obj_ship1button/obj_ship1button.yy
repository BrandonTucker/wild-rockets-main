{
    "id": "e19cb166-6931-4cfa-96ea-60602e2ae5dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship1button",
    "eventList": [
        {
            "id": "5e7167cf-66a9-45bc-ac57-35d2fec150fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e19cb166-6931-4cfa-96ea-60602e2ae5dc"
        },
        {
            "id": "ad37bbb0-6f47-457a-88d9-d939ea5e9aa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e19cb166-6931-4cfa-96ea-60602e2ae5dc"
        },
        {
            "id": "dccac04f-cb8a-4e35-ad58-5567dedcdc1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e19cb166-6931-4cfa-96ea-60602e2ae5dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "38248c6d-0a4a-4a6d-8298-8c272d0a54d0",
    "visible": true
}