{
    "id": "713624fb-4467-49ee-8982-a85058a3b305",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange5button",
    "eventList": [
        {
            "id": "4c82784e-348e-4560-9aea-9c5760668f07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "713624fb-4467-49ee-8982-a85058a3b305"
        },
        {
            "id": "e1e24fb8-b4c5-4653-ab48-aff2c1e0350c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "713624fb-4467-49ee-8982-a85058a3b305"
        },
        {
            "id": "5653969c-a318-49cf-a232-9af96e1a7db2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "713624fb-4467-49ee-8982-a85058a3b305"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "89d497a0-f79b-455f-b61d-d0a347edae32",
    "visible": true
}