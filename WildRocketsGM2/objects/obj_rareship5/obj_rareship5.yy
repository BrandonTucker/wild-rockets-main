{
    "id": "c3efba12-5053-408f-916b-e87a79e82e3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rareship5",
    "eventList": [
        {
            "id": "95911163-0bf2-43fa-bb80-bdebec9dc548",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3efba12-5053-408f-916b-e87a79e82e3c"
        },
        {
            "id": "ebde4fdd-e521-4482-b10c-395b4caca7d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c3efba12-5053-408f-916b-e87a79e82e3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d23bb1f-1edc-411b-96e6-de1f82a81d36",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
    "visible": true
}