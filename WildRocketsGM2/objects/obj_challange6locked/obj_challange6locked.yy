{
    "id": "cb2ddd69-3a3e-427c-a39c-218c99e53828",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange6locked",
    "eventList": [
        {
            "id": "feec345e-902c-43d6-9a75-56a8cd054b6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb2ddd69-3a3e-427c-a39c-218c99e53828"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f1401ffc-b4d8-444a-b5a7-533581d15076",
    "visible": true
}