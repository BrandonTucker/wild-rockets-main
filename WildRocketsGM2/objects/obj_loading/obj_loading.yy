{
    "id": "96cfb002-e501-4faf-970a-a8cabda2807d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_loading",
    "eventList": [
        {
            "id": "097d375f-1141-48d4-b6a2-c5ac1223b926",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "96cfb002-e501-4faf-970a-a8cabda2807d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b42c5f79-1826-474d-9a3f-3cc6fc9caf43",
    "visible": true
}