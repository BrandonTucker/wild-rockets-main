{
    "id": "17cd8cf3-a9c7-4c74-914f-ccf5e3ec5ee7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange9button",
    "eventList": [
        {
            "id": "985095c2-1d10-47eb-ad72-e4fefe9744a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "17cd8cf3-a9c7-4c74-914f-ccf5e3ec5ee7"
        },
        {
            "id": "4ccd0261-d838-46f4-a283-e89af441c47a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "17cd8cf3-a9c7-4c74-914f-ccf5e3ec5ee7"
        },
        {
            "id": "9e8e73f2-31f4-419b-83fe-30a1c0ead319",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "17cd8cf3-a9c7-4c74-914f-ccf5e3ec5ee7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "99e5bb8a-9ebe-45cb-88c5-562d898915d1",
    "visible": true
}