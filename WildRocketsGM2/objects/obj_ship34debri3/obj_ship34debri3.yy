{
    "id": "6b3ce032-d54f-419f-ae40-44e9de74d6a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship34debri3",
    "eventList": [
        {
            "id": "774b5634-3abb-4415-ba3e-89ccca79e38e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6b3ce032-d54f-419f-ae40-44e9de74d6a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
    "visible": true
}