{
    "id": "95e9bee4-ee13-4545-a029-e4cf116c0ab1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_finishhighscore",
    "eventList": [
        {
            "id": "adee4210-c095-4cd4-9f5c-9f0e5ff8e99a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "95e9bee4-ee13-4545-a029-e4cf116c0ab1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87f61f77-134a-44f5-9819-bad6e67c5abd",
    "visible": true
}