{
    "id": "718569ba-e77c-4201-974c-a13ed00b29db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_unavailable",
    "eventList": [
        {
            "id": "460b4941-0eea-4861-96b0-e2cec7a9bba2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "718569ba-e77c-4201-974c-a13ed00b29db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
    "visible": true
}