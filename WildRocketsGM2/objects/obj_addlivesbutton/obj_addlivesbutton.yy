{
    "id": "163f7b13-cc37-47c4-85db-9c0141659fa7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_addlivesbutton",
    "eventList": [
        {
            "id": "f4568941-7251-425c-8dcd-0a745411f1e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "163f7b13-cc37-47c4-85db-9c0141659fa7"
        },
        {
            "id": "36632032-1e7d-4d87-ae01-940de80bb610",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "163f7b13-cc37-47c4-85db-9c0141659fa7"
        },
        {
            "id": "1e60068c-455f-494f-8b85-8ae6aff4bd00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "163f7b13-cc37-47c4-85db-9c0141659fa7"
        },
        {
            "id": "c00f9364-a04f-48a4-a91f-d5a7aa9f2e07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "163f7b13-cc37-47c4-85db-9c0141659fa7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "visible": true
}