{
    "id": "f4dfb160-3266-4cc8-ae6d-496a7af8359b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bluemetalchunk2",
    "eventList": [
        {
            "id": "4da9b224-1b69-47ed-8a4d-ed5d35891d41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f4dfb160-3266-4cc8-ae6d-496a7af8359b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
    "visible": true
}