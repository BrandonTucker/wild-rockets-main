{
    "id": "001d2a6c-71b8-4ab6-beab-4de0241d789c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lockedbuttonglow2",
    "eventList": [
        {
            "id": "fcf1f56e-ce1b-4107-ade9-17d23e992753",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "001d2a6c-71b8-4ab6-beab-4de0241d789c"
        },
        {
            "id": "f4f2b3f0-524e-40b4-ac85-66c46bc4231e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "001d2a6c-71b8-4ab6-beab-4de0241d789c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f66b6c3-303a-4139-9858-dfe2434a1aa4",
    "visible": true
}