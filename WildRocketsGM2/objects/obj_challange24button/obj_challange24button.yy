{
    "id": "78da480a-c318-4d8f-b589-dff50142699b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange24button",
    "eventList": [
        {
            "id": "68818c47-bff3-4249-8dcb-4eab99f8a1b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78da480a-c318-4d8f-b589-dff50142699b"
        },
        {
            "id": "cb91703a-68c3-4d3e-a051-34a8f642946d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78da480a-c318-4d8f-b589-dff50142699b"
        },
        {
            "id": "2450ad0a-700c-4ce1-b28a-d0c760a4eb28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "78da480a-c318-4d8f-b589-dff50142699b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e2865308-32b5-40d4-aceb-14ece7f29277",
    "visible": true
}