action_set_relative(1);
action_draw_text_transformed("Created by", 0, -40, scalebig, scalebig, 0);
action_draw_text_transformed("Erik Tucker", 0, 35, .8, .8, 0);
action_draw_text_transformed("Brandon Tucker", 0, 85, .8, .8, 0);
action_draw_text_transformed("Promoters", 0, 160, scalebig, scalebig, 0);

promoters[0] = "Austin Southcott";
promoters[1] = "Ian Southcott";
promoters[2] = "Jon King";
promoters[3] = "Nick Brumfield";
promoters[4] = "Sean Williams";
promoters[5] = "Carlos Bobadilla";
promoters[6] = "Tesla Akers";
promoters[7] = "Devin Smith";
promoters[8] = "AJ Tucker";
promoters[9] = "Josh Hudson";
promoters[10] = "Ryan Bayle";
promoters[11] = "Chris Southcott";
promoters[12] = "Savanah Brown";
promoters[13] = "Hannah Southcott";
promoters[14] = "Tave Cooper";
promoters[15] = "Josh Wilburn";

var i;
for(i = 0; i < array_length_1d(promoters); i+=1){
	action_draw_text_transformed(promoters[i], 0, 228+i*38, scalesmall, scalesmall, 0);
}

action_set_relative(0);
