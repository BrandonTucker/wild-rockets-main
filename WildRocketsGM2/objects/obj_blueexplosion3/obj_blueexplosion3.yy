{
    "id": "9962e98a-9e63-404a-aa6c-433b47ed4319",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blueexplosion3",
    "eventList": [
        {
            "id": "2c1832af-85a3-45ee-a65a-bd768346c7a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9962e98a-9e63-404a-aa6c-433b47ed4319"
        },
        {
            "id": "734bdcab-571c-441d-b25b-714ebd8db1d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9962e98a-9e63-404a-aa6c-433b47ed4319"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "8c572e0d-865b-431a-956d-0479f6a5ff72",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "5c98520e-619e-411f-8ab0-cff15048ae05",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1547e11e-dae4-47b2-ab52-e3400ace6404",
    "visible": true
}