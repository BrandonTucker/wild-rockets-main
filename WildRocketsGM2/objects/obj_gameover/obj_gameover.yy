{
    "id": "905759f8-f306-4403-b7c2-a2109b5b4cee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameover",
    "eventList": [
        {
            "id": "15441983-691b-4d59-8b6c-c241fca5480e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "905759f8-f306-4403-b7c2-a2109b5b4cee"
        },
        {
            "id": "d26bd513-5e11-4d1a-8bd9-7e3b16b051a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "905759f8-f306-4403-b7c2-a2109b5b4cee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
    "visible": true
}