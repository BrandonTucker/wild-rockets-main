{
    "id": "710c8b1d-6ad5-4c11-8100-24759bf03040",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cloud",
    "eventList": [
        {
            "id": "13729420-3d4f-4860-bf4e-e40feb896781",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "710c8b1d-6ad5-4c11-8100-24759bf03040"
        },
        {
            "id": "c20518b7-28b7-4390-a1e0-41c70d1b0335",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "710c8b1d-6ad5-4c11-8100-24759bf03040"
        },
        {
            "id": "29c7d20a-0692-44ea-a973-02edb596e0df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "710c8b1d-6ad5-4c11-8100-24759bf03040"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "746f0ca6-62b0-43e3-af8c-80762a087846",
    "visible": true
}