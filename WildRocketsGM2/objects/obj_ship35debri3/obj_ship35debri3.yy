{
    "id": "4271bbc3-02dd-48c0-8262-41f2d3b58775",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship35debri3",
    "eventList": [
        {
            "id": "0abfa527-89e6-4390-8647-2dbbe163b0c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4271bbc3-02dd-48c0-8262-41f2d3b58775"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
    "visible": true
}