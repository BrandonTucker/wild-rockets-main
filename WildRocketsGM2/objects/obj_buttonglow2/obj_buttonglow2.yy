{
    "id": "c5139257-f27b-4d81-80b8-83ef97518ad9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_buttonglow2",
    "eventList": [
        {
            "id": "091e8056-6d0d-4a46-a9fb-6355639b089d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c5139257-f27b-4d81-80b8-83ef97518ad9"
        },
        {
            "id": "bc83cf36-6734-4287-85a7-ae11405c4e9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c5139257-f27b-4d81-80b8-83ef97518ad9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f66b6c3-303a-4139-9858-dfe2434a1aa4",
    "visible": true
}