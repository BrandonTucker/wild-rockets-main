{
    "id": "e8e1ff52-c7b7-4000-9414-e4919b4488c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked26",
    "eventList": [
        {
            "id": "27f8ed40-414d-4b3b-9a33-f5eaf5bfe2cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8e1ff52-c7b7-4000-9414-e4919b4488c4"
        },
        {
            "id": "4acc8ddb-9e70-4aff-82bd-dae103714464",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e8e1ff52-c7b7-4000-9414-e4919b4488c4"
        },
        {
            "id": "07b84a15-6bc4-4522-b531-474e1823d755",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e8e1ff52-c7b7-4000-9414-e4919b4488c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e8ff5e15-f045-43a1-a54a-6cb97a0f1dc7",
    "visible": true
}