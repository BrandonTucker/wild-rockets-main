{
    "id": "70591138-70d4-4782-b6d7-c140d3320bf3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked28",
    "eventList": [
        {
            "id": "f917a26e-354d-45a7-850b-540f943e9eaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "70591138-70d4-4782-b6d7-c140d3320bf3"
        },
        {
            "id": "64827487-1527-4180-9bce-d24e6cf78af9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "70591138-70d4-4782-b6d7-c140d3320bf3"
        },
        {
            "id": "37b5e7b4-1c72-4c2e-9df9-0abfce7625cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "70591138-70d4-4782-b6d7-c140d3320bf3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a0a65eea-0774-4c60-9aa1-6819fc94064c",
    "visible": true
}