action_set_relative(1);
action_create_object(obj_blueexplosion2, random_range(-20,20), random_range(-20,20));
action_create_object(obj_blueexplosion1, random_range(-20,20), random_range(-20,20));
action_create_object(obj_blueexplosion1, random_range(-20,20), random_range(-20,20));
with (obj_backgroundfade) {
action_set_relative(0);
action_set_alarm(45, 0);
action_set_relative(1);
}
with (obj_wildrockets) {
action_set_relative(0);
action_set_alarm(45, 0);
action_set_relative(1);
}
with (obj_intro) {
action_set_relative(0);
action_set_alarm(110, 0);
action_set_relative(1);
}
action_kill_object();
action_set_relative(0);
