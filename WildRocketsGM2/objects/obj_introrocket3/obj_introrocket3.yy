{
    "id": "8f9d0760-0abd-4cb4-a468-82ae355aee12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_introrocket3",
    "eventList": [
        {
            "id": "ed1a5ea6-270b-4b8e-987d-99fdb2d07b7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f9d0760-0abd-4cb4-a468-82ae355aee12"
        },
        {
            "id": "48cea748-760b-4e12-aa91-4594013c3c1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "8f9d0760-0abd-4cb4-a468-82ae355aee12"
        },
        {
            "id": "cccfbf88-2469-4776-8781-dfed18e4fe55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "8f9d0760-0abd-4cb4-a468-82ae355aee12"
        },
        {
            "id": "576ced51-556f-4c74-87af-3215469a0a5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8f9d0760-0abd-4cb4-a468-82ae355aee12"
        },
        {
            "id": "94ab2afd-a5fe-4ec2-a4ee-41472b2fa880",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8f9d0760-0abd-4cb4-a468-82ae355aee12"
        },
        {
            "id": "5f6d6eac-bb8b-4f55-a4bf-3d5c984fb4a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f9d0760-0abd-4cb4-a468-82ae355aee12"
        },
        {
            "id": "05642cf6-c37a-4cba-93ee-f76093a67028",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b55e1166-db7c-4c00-a1f9-817cd9c75b01",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8f9d0760-0abd-4cb4-a468-82ae355aee12"
        },
        {
            "id": "e61a4411-4e85-4a6d-8350-fc22c4a1b80e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ec1632ed-b141-4912-a44b-371632a04e35",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8f9d0760-0abd-4cb4-a468-82ae355aee12"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
    "visible": true
}