{
    "id": "8d1841e0-6599-4dee-b128-225255622293",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship38button",
    "eventList": [
        {
            "id": "b9773632-00a8-4dc4-a04e-bfe95aa9579c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d1841e0-6599-4dee-b128-225255622293"
        },
        {
            "id": "c7b9181f-02be-4748-8a3e-badb460ed9f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8d1841e0-6599-4dee-b128-225255622293"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "10d046c9-c01d-458b-92bf-7e457f7c64cc",
    "visible": true
}