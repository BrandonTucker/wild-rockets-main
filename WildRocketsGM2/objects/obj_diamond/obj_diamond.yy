{
    "id": "321599a3-87c4-4249-94ca-28d22aa90be6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_diamond",
    "eventList": [
        {
            "id": "35c22df5-7ce1-4134-9b1c-508ae81c49aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "321599a3-87c4-4249-94ca-28d22aa90be6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
    "visible": true
}