{
    "id": "73bc41f3-5043-4dac-85a7-22eecf66d9e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship4button",
    "eventList": [
        {
            "id": "70f5ab77-1dbf-4a7f-8247-b8987d47fa81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "73bc41f3-5043-4dac-85a7-22eecf66d9e2"
        },
        {
            "id": "0fac0079-731c-4a2f-8a66-98da066df4ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "73bc41f3-5043-4dac-85a7-22eecf66d9e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9e2604e-bc70-4d5d-836d-0c048ec87a35",
    "visible": true
}