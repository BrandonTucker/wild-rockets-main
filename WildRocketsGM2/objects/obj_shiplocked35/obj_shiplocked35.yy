{
    "id": "c3e31372-0a66-4d1a-bdae-90863f183a51",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked35",
    "eventList": [
        {
            "id": "5ea099ac-8c4e-499f-915e-a27fbb6727b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3e31372-0a66-4d1a-bdae-90863f183a51"
        },
        {
            "id": "0a3c50a5-8c49-4c8f-8278-0649789c522f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c3e31372-0a66-4d1a-bdae-90863f183a51"
        },
        {
            "id": "0f08a57a-02d5-469c-9238-34e043c28a6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c3e31372-0a66-4d1a-bdae-90863f183a51"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
    "visible": true
}