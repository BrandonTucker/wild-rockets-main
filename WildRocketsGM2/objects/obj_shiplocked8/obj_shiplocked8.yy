{
    "id": "03733ec8-dcb5-4bc1-9343-5c666b432f6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked8",
    "eventList": [
        {
            "id": "a90271d1-1c9d-455b-97b5-30f4aeb712be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "03733ec8-dcb5-4bc1-9343-5c666b432f6f"
        },
        {
            "id": "1e01347a-54a5-473d-a83c-2b89b17a0208",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "03733ec8-dcb5-4bc1-9343-5c666b432f6f"
        },
        {
            "id": "5d02a8e8-9277-4d8d-be36-c1f6794acb73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "03733ec8-dcb5-4bc1-9343-5c666b432f6f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24d65620-c4b3-4b34-99bb-77c389504d05",
    "visible": true
}