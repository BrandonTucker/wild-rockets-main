{
    "id": "e2bd400f-9f89-4db7-925e-9426d8eb592a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange40button",
    "eventList": [
        {
            "id": "fe3a1584-681f-4a5f-bdc8-bba7fcecf917",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e2bd400f-9f89-4db7-925e-9426d8eb592a"
        },
        {
            "id": "3e8853c3-7630-4f1a-91dd-80dbfff144b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e2bd400f-9f89-4db7-925e-9426d8eb592a"
        },
        {
            "id": "ebdbb914-d6a4-43e5-b82f-f5d32601cde6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e2bd400f-9f89-4db7-925e-9426d8eb592a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "08623dac-3075-455c-8a85-daaab8950a63",
    "visible": true
}