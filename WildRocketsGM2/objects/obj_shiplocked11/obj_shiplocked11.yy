{
    "id": "0249e77c-e52b-451f-a1fa-37092fd57c63",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked11",
    "eventList": [
        {
            "id": "a9a26c58-5c18-4fd9-bccc-9b936ff9e7ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0249e77c-e52b-451f-a1fa-37092fd57c63"
        },
        {
            "id": "5a623894-24c0-494f-bc42-f3bff40633b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0249e77c-e52b-451f-a1fa-37092fd57c63"
        },
        {
            "id": "af985da7-df5b-4aa9-a974-65c85f1ba765",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0249e77c-e52b-451f-a1fa-37092fd57c63"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "89d24856-42fc-42c6-9f75-b85af1f07098",
    "visible": true
}