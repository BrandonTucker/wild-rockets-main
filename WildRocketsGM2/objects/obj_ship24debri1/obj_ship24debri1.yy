{
    "id": "efa6d60f-d029-46be-9702-91d9a0bae39e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship24debri1",
    "eventList": [
        {
            "id": "fbdd1a16-8a58-4250-9b93-aed86d666b2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "efa6d60f-d029-46be-9702-91d9a0bae39e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
    "visible": true
}