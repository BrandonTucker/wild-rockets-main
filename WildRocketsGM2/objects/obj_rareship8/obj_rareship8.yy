{
    "id": "a60611b2-173f-4f0d-b6bc-b09a46826fac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rareship8",
    "eventList": [
        {
            "id": "464cd1e9-e6ee-49c0-b20d-0623b59c84c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a60611b2-173f-4f0d-b6bc-b09a46826fac"
        },
        {
            "id": "fe6b3a0f-9726-4f7d-b90f-cd238b23745a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a60611b2-173f-4f0d-b6bc-b09a46826fac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d23bb1f-1edc-411b-96e6-de1f82a81d36",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
    "visible": true
}