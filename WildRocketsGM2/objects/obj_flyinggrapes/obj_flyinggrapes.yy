{
    "id": "aa257625-403c-434c-8a25-6ee8e104fbc1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flyinggrapes",
    "eventList": [
        {
            "id": "9a3e5737-5b51-42c0-93a6-1ed18c0593e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aa257625-403c-434c-8a25-6ee8e104fbc1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
    "visible": true
}