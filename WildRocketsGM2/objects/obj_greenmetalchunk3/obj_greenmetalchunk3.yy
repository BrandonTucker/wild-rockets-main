{
    "id": "60f466c4-5d67-4e33-8cb1-cb407c996d35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greenmetalchunk3",
    "eventList": [
        {
            "id": "add899e8-7234-4605-a9f1-13d981f9e6cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "60f466c4-5d67-4e33-8cb1-cb407c996d35"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
    "visible": true
}