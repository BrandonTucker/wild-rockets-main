{
    "id": "062ec263-b1d8-40a1-81fe-051a7f5693a7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_unlocksparkle",
    "eventList": [
        {
            "id": "61ebff49-3fb0-4747-af5c-1c5d02840526",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "062ec263-b1d8-40a1-81fe-051a7f5693a7"
        },
        {
            "id": "39388f01-0d32-4017-8312-847542a4bd7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "062ec263-b1d8-40a1-81fe-051a7f5693a7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
    "visible": true
}