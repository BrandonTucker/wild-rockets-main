{
    "id": "512a8a62-f103-4735-ad17-eb388e35edf0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shipunlockmessage",
    "eventList": [
        {
            "id": "73d29724-81bd-4e74-897f-d6d7464a0cd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "512a8a62-f103-4735-ad17-eb388e35edf0"
        },
        {
            "id": "1fae7f56-1742-4d5c-a171-25a7a5af17af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "512a8a62-f103-4735-ad17-eb388e35edf0"
        },
        {
            "id": "86ec257d-0e87-40fe-9153-18d2fb5ef158",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "512a8a62-f103-4735-ad17-eb388e35edf0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}