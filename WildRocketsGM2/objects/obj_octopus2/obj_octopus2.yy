{
    "id": "6923e52b-833d-487b-ba9d-742e21067322",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_octopus2",
    "eventList": [
        {
            "id": "982f2775-54db-4cc7-ac97-cfcba1754f36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6923e52b-833d-487b-ba9d-742e21067322"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b3b8eab4-7ef8-4901-939c-38eb69e3906d",
    "visible": true
}