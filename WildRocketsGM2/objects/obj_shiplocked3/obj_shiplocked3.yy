{
    "id": "6386f8d8-70af-4229-be87-11f10cb68f0b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked3",
    "eventList": [
        {
            "id": "aa6c6a96-8a98-49ca-a1de-285c65097ae6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6386f8d8-70af-4229-be87-11f10cb68f0b"
        },
        {
            "id": "a2af41ee-3d64-47b0-bf6d-a54874540d68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6386f8d8-70af-4229-be87-11f10cb68f0b"
        },
        {
            "id": "8867a46d-5da6-41b7-b42b-2833e1330a93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6386f8d8-70af-4229-be87-11f10cb68f0b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "26554abc-61e7-4277-a01e-e8c81f89775d",
    "visible": true
}