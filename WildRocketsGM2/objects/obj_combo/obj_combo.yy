{
    "id": "2101a002-b6cb-4c39-8af4-5f1c3b5a6668",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_combo",
    "eventList": [
        {
            "id": "7236e19a-24cc-4090-bb08-47b20d33a994",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2101a002-b6cb-4c39-8af4-5f1c3b5a6668"
        },
        {
            "id": "74b15625-1dcc-4fde-a204-fb432dc02f0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2101a002-b6cb-4c39-8af4-5f1c3b5a6668"
        },
        {
            "id": "d8ac918c-fbb5-49e9-9a05-e35afa0cbf20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "2101a002-b6cb-4c39-8af4-5f1c3b5a6668"
        },
        {
            "id": "02137457-f4bd-4eec-99c4-a6e1139767b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2101a002-b6cb-4c39-8af4-5f1c3b5a6668"
        },
        {
            "id": "48cc8799-5d43-4a38-bf50-6d29b5dc956c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "2101a002-b6cb-4c39-8af4-5f1c3b5a6668"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}