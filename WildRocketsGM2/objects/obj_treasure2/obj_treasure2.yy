{
    "id": "45c20263-94bc-4348-8fe1-a9f6a20d1a69",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_treasure2",
    "eventList": [
        {
            "id": "8d7692cf-cc70-4b33-9f1e-a2e77f6e6d63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "45c20263-94bc-4348-8fe1-a9f6a20d1a69"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
    "visible": true
}