{
    "id": "ba946c82-4361-43fe-b068-b1244baf5e7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship25button",
    "eventList": [
        {
            "id": "98cb2aa6-f95b-483f-881e-614529c03afc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ba946c82-4361-43fe-b068-b1244baf5e7c"
        },
        {
            "id": "fd33ea3c-cc4f-4aab-bd74-b8fef65cb360",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ba946c82-4361-43fe-b068-b1244baf5e7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e948847d-a441-49fb-988d-0e5916ffe9e6",
    "visible": true
}