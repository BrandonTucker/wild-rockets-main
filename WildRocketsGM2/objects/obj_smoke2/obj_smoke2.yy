{
    "id": "376fc0a7-476b-45fe-ae33-cd377fcce88a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_smoke2",
    "eventList": [
        {
            "id": "9f46ae99-e76e-48a8-96e3-35a8b3586e32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "376fc0a7-476b-45fe-ae33-cd377fcce88a"
        },
        {
            "id": "134db082-80aa-4d1c-b45f-8eb7d346d5c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "376fc0a7-476b-45fe-ae33-cd377fcce88a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "5f79584d-4244-4583-b731-f0c63700a8db",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "ba3b6b7c-402e-4b83-bc4e-af258faadc6c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "603c146c-5190-46ad-a266-7e4342072cb8",
    "visible": true
}