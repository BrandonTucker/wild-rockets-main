{
    "id": "81e9e7b2-1aae-45c0-942a-51e605cbf05c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange8button",
    "eventList": [
        {
            "id": "d6250a83-4f18-42a4-b7eb-119dc78bb970",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "81e9e7b2-1aae-45c0-942a-51e605cbf05c"
        },
        {
            "id": "38c606a8-5be3-420e-b8b3-631a8b5d793d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "81e9e7b2-1aae-45c0-942a-51e605cbf05c"
        },
        {
            "id": "ce0c1066-fb0a-4439-a3a4-e553089e518d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "81e9e7b2-1aae-45c0-942a-51e605cbf05c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e3ac22b7-fc1c-4b24-b0ec-3ab6e27beff4",
    "visible": true
}