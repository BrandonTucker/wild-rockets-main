{
    "id": "d947aa83-4559-4230-99d5-3855c8bb6b89",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_unlocknobutton",
    "eventList": [
        {
            "id": "dbbd4a5c-c4c8-4207-ba50-f5a7f1d72412",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d947aa83-4559-4230-99d5-3855c8bb6b89"
        },
        {
            "id": "ab1e4b77-d7bf-41ea-8175-7b8ca5ce0920",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "d947aa83-4559-4230-99d5-3855c8bb6b89"
        },
        {
            "id": "01f067c7-9cad-4e54-83c5-82bc0414a511",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d947aa83-4559-4230-99d5-3855c8bb6b89"
        },
        {
            "id": "96206122-032d-459e-a345-79b73f3aa88b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d947aa83-4559-4230-99d5-3855c8bb6b89"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9ef46c4-79be-4962-ad73-297f81e4fbe3",
    "visible": true
}