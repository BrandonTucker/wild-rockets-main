{
    "id": "07c3ccdd-7282-4640-8783-59d1dbc4c5e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship17button",
    "eventList": [
        {
            "id": "9e9231fe-bbe0-42bb-831f-ff80a66020de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "07c3ccdd-7282-4640-8783-59d1dbc4c5e6"
        },
        {
            "id": "1ea7cdd5-de14-423c-814f-b81cb868c295",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "07c3ccdd-7282-4640-8783-59d1dbc4c5e6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c6d7f37f-e10e-4354-9ed6-bc619cfe79a4",
    "visible": true
}