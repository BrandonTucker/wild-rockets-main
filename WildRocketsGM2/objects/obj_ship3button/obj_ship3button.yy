{
    "id": "0ffe66ab-ea05-4d50-b258-544785565212",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship3button",
    "eventList": [
        {
            "id": "c9980e0d-e4b1-4a39-8d22-e07bfe738dfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0ffe66ab-ea05-4d50-b258-544785565212"
        },
        {
            "id": "c10ca930-80c8-4ad3-b53f-cb803d9493cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0ffe66ab-ea05-4d50-b258-544785565212"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "82d47bdb-1129-492c-a4be-6d0d25a2d589",
    "visible": true
}