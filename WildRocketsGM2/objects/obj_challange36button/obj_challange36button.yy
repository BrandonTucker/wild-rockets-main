{
    "id": "dd05d83d-eb07-481f-a454-56b668eea2c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange36button",
    "eventList": [
        {
            "id": "c434f682-fa51-4148-95e9-c83b1c43db8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd05d83d-eb07-481f-a454-56b668eea2c3"
        },
        {
            "id": "67ad402e-d40f-4fbf-b1af-3f0d890cd132",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dd05d83d-eb07-481f-a454-56b668eea2c3"
        },
        {
            "id": "470ad2f9-d917-4fd4-a58b-74c9030e6b60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "dd05d83d-eb07-481f-a454-56b668eea2c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "44f73386-9224-4830-9c3a-3090840f65a0",
    "visible": true
}