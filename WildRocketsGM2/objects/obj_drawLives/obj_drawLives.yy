{
    "id": "a98a6bad-d4bb-4dc7-89e2-d4a38e24441c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_drawLives",
    "eventList": [
        {
            "id": "881f5d6b-3ccd-4a00-9f16-005c3a76c5e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a98a6bad-d4bb-4dc7-89e2-d4a38e24441c"
        },
        {
            "id": "43ba46ff-7887-4fde-9f81-73a16c3fe52f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "a98a6bad-d4bb-4dc7-89e2-d4a38e24441c"
        },
        {
            "id": "ca6f0be1-4811-4167-887f-bce90aa2ded2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a98a6bad-d4bb-4dc7-89e2-d4a38e24441c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3df27bd2-a74f-4f5d-b3a1-84c2a722a913",
    "visible": true
}