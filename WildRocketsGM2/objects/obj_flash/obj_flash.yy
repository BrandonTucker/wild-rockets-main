{
    "id": "3a2134cd-5766-4ff3-a4fc-7bd687199b7e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flash",
    "eventList": [
        {
            "id": "8fe75017-5a90-4eae-ba2a-105d2041d368",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a2134cd-5766-4ff3-a4fc-7bd687199b7e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
    "visible": true
}