{
    "id": "e5d1f5bc-d8c7-4647-bf6f-5e7d0b6c4abb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked25",
    "eventList": [
        {
            "id": "5407eeb6-b2c5-4da1-8133-4526a86eb153",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e5d1f5bc-d8c7-4647-bf6f-5e7d0b6c4abb"
        },
        {
            "id": "e29f43e9-2c32-42df-8f80-fd90c3b938ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e5d1f5bc-d8c7-4647-bf6f-5e7d0b6c4abb"
        },
        {
            "id": "89a5f345-64c1-4d02-8c2b-908ada563b2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e5d1f5bc-d8c7-4647-bf6f-5e7d0b6c4abb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "06c9feeb-f1ca-443c-8805-5edb76000f37",
    "visible": true
}