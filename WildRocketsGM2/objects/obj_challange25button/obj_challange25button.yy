{
    "id": "2add3f61-3ece-4293-a77c-d4b4c24609b1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange25button",
    "eventList": [
        {
            "id": "6a84631e-d1d0-4681-8e23-d57c1eb0f496",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2add3f61-3ece-4293-a77c-d4b4c24609b1"
        },
        {
            "id": "43be13ce-01e4-4abf-8895-e0901e99acd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2add3f61-3ece-4293-a77c-d4b4c24609b1"
        },
        {
            "id": "ea32a84c-b661-4691-a3f4-86a3365cdbcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2add3f61-3ece-4293-a77c-d4b4c24609b1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0d4b64dd-0645-4f02-b9fc-33cce76a2e99",
    "visible": true
}