{
    "id": "42939b94-c575-4535-a81c-e02b644fd1df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_quitbutton",
    "eventList": [
        {
            "id": "a484d750-71aa-451a-9ace-70d283fa1f69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42939b94-c575-4535-a81c-e02b644fd1df"
        },
        {
            "id": "e7945b86-eb3f-4e73-9538-38aeb162cb6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "42939b94-c575-4535-a81c-e02b644fd1df"
        },
        {
            "id": "09f5aa63-2d67-4e36-8f9c-585b9e78a4e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "42939b94-c575-4535-a81c-e02b644fd1df"
        },
        {
            "id": "84f723b7-ce75-45fe-bc93-c9f68461cd2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "42939b94-c575-4535-a81c-e02b644fd1df"
        },
        {
            "id": "7dfe4afb-961d-4465-8b1d-a90d761dfa91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "42939b94-c575-4535-a81c-e02b644fd1df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
    "visible": true
}