{
    "id": "032b5ee4-fe29-4fe5-a741-c66353ba8ac3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship21button",
    "eventList": [
        {
            "id": "2d52a198-5697-4a41-843f-0ddd3d3cbcd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "032b5ee4-fe29-4fe5-a741-c66353ba8ac3"
        },
        {
            "id": "ba47bf6d-181d-4ff6-af5b-8470a73eacd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "032b5ee4-fe29-4fe5-a741-c66353ba8ac3"
        },
        {
            "id": "a2fc5295-f0d6-49bd-a38b-6a2b0587a38a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "032b5ee4-fe29-4fe5-a741-c66353ba8ac3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "67c921f0-e72c-4677-893a-41df5d9c329d",
    "visible": true
}