{
    "id": "30137936-c010-4fff-9b4e-579bd2ef7669",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_optionbutton",
    "eventList": [
        {
            "id": "38f825d6-124d-4bde-9317-c0a0f94e4a1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "30137936-c010-4fff-9b4e-579bd2ef7669"
        },
        {
            "id": "2d09ba5c-d86c-4caa-9530-c52620ccb61b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "30137936-c010-4fff-9b4e-579bd2ef7669"
        },
        {
            "id": "624ddd22-e4fd-4415-a462-e76eaeca1027",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "30137936-c010-4fff-9b4e-579bd2ef7669"
        },
        {
            "id": "f373740b-2299-48c1-86a0-60101f466c85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "30137936-c010-4fff-9b4e-579bd2ef7669"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "visible": true
}