{
    "id": "0c436f0d-640c-490b-8acb-33f631a9ed46",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked21",
    "eventList": [
        {
            "id": "822ae563-eebc-4c96-aeb5-92817ea549ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0c436f0d-640c-490b-8acb-33f631a9ed46"
        },
        {
            "id": "43bcf6fd-c581-47fa-89f0-efebf764dac9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0c436f0d-640c-490b-8acb-33f631a9ed46"
        },
        {
            "id": "4c229f99-1857-4484-8d56-974b2c0f3a3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0c436f0d-640c-490b-8acb-33f631a9ed46"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7fae83b-1837-4f28-9bf7-cc875711b894",
    "visible": true
}