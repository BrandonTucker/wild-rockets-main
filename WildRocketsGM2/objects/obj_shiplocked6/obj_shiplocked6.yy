{
    "id": "c2e6eba4-25c2-4c15-87b8-743526fe44d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked6",
    "eventList": [
        {
            "id": "3b8ae8f1-755c-4b8c-a0ff-c17f0cdba16e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c2e6eba4-25c2-4c15-87b8-743526fe44d6"
        },
        {
            "id": "9e2dce19-355d-4bae-a9a9-3c2635c22ac9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c2e6eba4-25c2-4c15-87b8-743526fe44d6"
        },
        {
            "id": "d8ea563a-459a-4a13-b9bd-1cfba258f11f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c2e6eba4-25c2-4c15-87b8-743526fe44d6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ebb570a-ab2f-48de-82a9-9ee54b6ac0e0",
    "visible": true
}