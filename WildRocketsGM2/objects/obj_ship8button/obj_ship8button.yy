{
    "id": "d2863d7c-0f9c-405c-96f9-3dd01222cbea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship8button",
    "eventList": [
        {
            "id": "12c8959a-0230-4e31-ac49-9edbd9fdc2d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d2863d7c-0f9c-405c-96f9-3dd01222cbea"
        },
        {
            "id": "0ceaa05b-3234-419b-913f-cf47501de1a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d2863d7c-0f9c-405c-96f9-3dd01222cbea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d9894f94-5aad-481a-a269-262117b4efd3",
    "visible": true
}