{
    "id": "da870eb4-e8ce-4fb3-bda5-2864fbd7648c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blockdestructable",
    "eventList": [
        {
            "id": "99896d79-806d-4710-84bd-fdf282a7c1de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "da870eb4-e8ce-4fb3-bda5-2864fbd7648c"
        },
        {
            "id": "dea4a733-ca0d-4cbd-8f8a-b00165ad965f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "da870eb4-e8ce-4fb3-bda5-2864fbd7648c"
        },
        {
            "id": "582df136-9396-49be-8aaa-4194aa2d3ae0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e58f85a3-957f-493b-bbd0-f7b212ae3a64",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "da870eb4-e8ce-4fb3-bda5-2864fbd7648c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3c60cd49-2f3c-429f-bb1f-cf7114bf5d3b",
    "visible": true
}