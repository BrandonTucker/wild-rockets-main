{
    "id": "09034fe1-28c9-4c34-aa73-e8d75466b019",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_plusLife",
    "eventList": [
        {
            "id": "f52a4ddd-5bdb-4aa8-bf75-a91aa1a40a78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "09034fe1-28c9-4c34-aa73-e8d75466b019"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
    "visible": true
}