{
    "id": "1f4dd52f-1206-4dd2-a372-19bd96d5a5b6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange3button",
    "eventList": [
        {
            "id": "e2caef3f-d876-4959-b421-8356a24ee465",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f4dd52f-1206-4dd2-a372-19bd96d5a5b6"
        },
        {
            "id": "c11d2fca-083d-4ac9-81e8-7dd75a179542",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1f4dd52f-1206-4dd2-a372-19bd96d5a5b6"
        },
        {
            "id": "c648dc11-127e-450a-a063-4d4599219232",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "1f4dd52f-1206-4dd2-a372-19bd96d5a5b6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "453733d3-8a04-4ba6-a037-e9d9d943128d",
    "visible": true
}