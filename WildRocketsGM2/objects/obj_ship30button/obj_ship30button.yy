{
    "id": "4b50acfa-145c-4fd2-8b7a-a44816fd5d1f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship30button",
    "eventList": [
        {
            "id": "3f2eaffa-9261-4b92-80b6-d7afe099aa1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4b50acfa-145c-4fd2-8b7a-a44816fd5d1f"
        },
        {
            "id": "d1518b78-5d74-4d47-97b5-69574191e084",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4b50acfa-145c-4fd2-8b7a-a44816fd5d1f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "85c085d9-3160-4f65-878e-26bbd4553701",
    "visible": true
}