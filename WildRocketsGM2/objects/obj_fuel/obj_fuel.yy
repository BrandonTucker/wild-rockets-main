{
    "id": "050ae07b-ed7f-408c-a8f1-97a67c446350",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fuel",
    "eventList": [
        {
            "id": "db150848-4bd8-497c-ba9a-efe6a2cac7c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "050ae07b-ed7f-408c-a8f1-97a67c446350"
        },
        {
            "id": "83338b3c-9b2e-460b-b76c-f8a33838f925",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "050ae07b-ed7f-408c-a8f1-97a67c446350"
        },
        {
            "id": "08fd8135-1b70-4aaf-b1b6-8ea606496d89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "050ae07b-ed7f-408c-a8f1-97a67c446350"
        },
        {
            "id": "2a5d4066-85ae-464b-988b-b5a032561fff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "050ae07b-ed7f-408c-a8f1-97a67c446350"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "14fcf11c-4b1f-48fb-8417-d3c54b57c1ff",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "afb0332d-0900-48a9-a327-876e2230fb02",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 28
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
    "visible": true
}