/// @description Insert description here
// You can write your code in this editor
if(!global.lifeclicked){
	if(global.lifeCount < 10){
	action_draw_sprite(spr_lifebutton, __view_get( e__VW.XView, 0 )+510, __view_get( e__VW.YView, 0 )+28, image_index);
	action_color(1429482);
	action_font(fontcoin, 0);
	draw_text(545,__view_get( e__VW.YView, 0 ) - 5,string_hash_to_newline("x " + string(global.lifeCount)));
}
if(global.lifeCount > 9 && global.lifeCount < 100){
	action_draw_sprite(spr_lifebutton, __view_get( e__VW.XView, 0 )+480, __view_get( e__VW.YView, 0 )+28, image_index);
	action_color(1429482);
	action_font(fontcoin, 0);
	draw_text(515,__view_get( e__VW.YView, 0 ) - 5,string_hash_to_newline("x " + string(global.lifeCount)));
}
if(global.lifeCount > 99 && global.lifeCount < 1000){
	action_draw_sprite(spr_lifebutton, __view_get( e__VW.XView, 0 )+450, __view_get( e__VW.YView, 0 )+28, image_index);
	action_color(1429482);
	action_font(fontcoin, 0);
	draw_text(485,__view_get( e__VW.YView, 0 ) - 5,string_hash_to_newline("x " + string(global.lifeCount)));
}
if(global.lifeCount > 999 && global.lifeCount < 10000){
	action_draw_sprite(spr_lifebutton, __view_get( e__VW.XView, 0 )+420, __view_get( e__VW.YView, 0 )+28, image_index);
	action_color(1429482);
	action_font(fontcoin, 0);
	draw_text(455,__view_get( e__VW.YView, 0 ) - 5,string_hash_to_newline("x " + string(global.lifeCount)));
}
}
else{
	if(global.lifeCount < 10){
	action_draw_sprite(spr_lifebutton2, __view_get( e__VW.XView, 0 )+510, __view_get( e__VW.YView, 0 )+28, image_index);
	action_color(1429482);
	action_font(fontcoin, 0);
	draw_text(545,__view_get( e__VW.YView, 0 ) - 5,string_hash_to_newline("x " + string(global.lifeCount)));
}
if(global.lifeCount > 9 && global.lifeCount < 100){
	action_draw_sprite(spr_lifebutton2, __view_get( e__VW.XView, 0 )+480, __view_get( e__VW.YView, 0 )+28, image_index);
	action_color(1429482);
	action_font(fontcoin, 0);
	draw_text(515,__view_get( e__VW.YView, 0 ) - 5,string_hash_to_newline("x " + string(global.lifeCount)));
}
if(global.lifeCount > 99 && global.lifeCount < 1000){
	action_draw_sprite(spr_lifebutton2, __view_get( e__VW.XView, 0 )+450, __view_get( e__VW.YView, 0 )+28, image_index);
	action_color(1429482);
	action_font(fontcoin, 0);
	draw_text(485,__view_get( e__VW.YView, 0 ) - 5,string_hash_to_newline("x " + string(global.lifeCount)));
}
if(global.lifeCount > 999 && global.lifeCount < 10000){
	action_draw_sprite(spr_lifebutton2, __view_get( e__VW.XView, 0 )+420, __view_get( e__VW.YView, 0 )+28, image_index);
	action_color(1429482);
	action_font(fontcoin, 0);
	draw_text(455,__view_get( e__VW.YView, 0 ) - 5,string_hash_to_newline("x " + string(global.lifeCount)));
}
}
