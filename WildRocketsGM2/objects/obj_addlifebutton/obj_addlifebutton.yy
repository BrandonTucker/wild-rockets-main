{
    "id": "0026a3c4-4263-4320-b2e6-0b7e512860fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_addlifebutton",
    "eventList": [
        {
            "id": "bc0319af-3dbc-45e2-8181-5fd507c47557",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0026a3c4-4263-4320-b2e6-0b7e512860fe"
        },
        {
            "id": "93a7328e-11ff-4039-9f5e-f1e97bd5c29d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0026a3c4-4263-4320-b2e6-0b7e512860fe"
        },
        {
            "id": "aa4f871c-2b18-4b70-820e-56d9cb0136c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0026a3c4-4263-4320-b2e6-0b7e512860fe"
        },
        {
            "id": "a888377e-1403-4e99-8f9f-62325187e77e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0026a3c4-4263-4320-b2e6-0b7e512860fe"
        },
        {
            "id": "a5788f63-77fa-4ec6-a688-ede5b160c9ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0026a3c4-4263-4320-b2e6-0b7e512860fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
    "visible": true
}