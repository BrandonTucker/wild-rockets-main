{
    "id": "9825b884-052f-494a-bd0b-9e0b51ce8d3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grapes",
    "eventList": [
        {
            "id": "4c1ce248-9772-49c1-ae3f-6f3bc0f49702",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9825b884-052f-494a-bd0b-9e0b51ce8d3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
    "visible": true
}