{
    "id": "ae7944d7-7328-4a51-83d7-3b08e00d2d7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked34",
    "eventList": [
        {
            "id": "8f2c3497-c34f-464f-a7ba-b7fd19c37485",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ae7944d7-7328-4a51-83d7-3b08e00d2d7c"
        },
        {
            "id": "adf7530d-d7be-4df0-bcb9-fa4713c80419",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ae7944d7-7328-4a51-83d7-3b08e00d2d7c"
        },
        {
            "id": "78303e98-02f8-4ac3-8f6e-4630a51d7d02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ae7944d7-7328-4a51-83d7-3b08e00d2d7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
    "visible": true
}