{
    "id": "e1982b89-6980-43ac-83a9-a73649e9668e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_highscorebutton",
    "eventList": [
        {
            "id": "b4d9f707-a345-4e0d-becb-15b33cb63801",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e1982b89-6980-43ac-83a9-a73649e9668e"
        },
        {
            "id": "9309c793-be25-494b-9358-e92e5ead53a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "e1982b89-6980-43ac-83a9-a73649e9668e"
        },
        {
            "id": "c0f71c00-6e08-4a4a-9650-128d53d59051",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e1982b89-6980-43ac-83a9-a73649e9668e"
        },
        {
            "id": "6f5721a6-b124-4bca-8fd8-e75ff802a65a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e1982b89-6980-43ac-83a9-a73649e9668e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "visible": true
}