{
    "id": "e307f97e-7821-43cb-a498-16f8d72b23a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coin_gen",
    "eventList": [
        {
            "id": "887a5ab1-340e-4a69-9efd-eda60c35e3bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e307f97e-7821-43cb-a498-16f8d72b23a0"
        },
        {
            "id": "c8803380-d648-46c6-9e0c-a4918bf4d359",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e307f97e-7821-43cb-a498-16f8d72b23a0"
        },
        {
            "id": "04d8fe45-93bd-4f60-98e1-ffdfb22d007a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "e307f97e-7821-43cb-a498-16f8d72b23a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}