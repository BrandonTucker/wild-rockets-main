{
    "id": "c9e31d6a-212a-4561-805f-86a5fb69a147",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challangebutton",
    "eventList": [
        {
            "id": "8a0005fe-9093-4be0-a265-1a5a958a55c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9e31d6a-212a-4561-805f-86a5fb69a147"
        },
        {
            "id": "d9fdc080-5878-4610-9368-ae75b06cacb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c9e31d6a-212a-4561-805f-86a5fb69a147"
        },
        {
            "id": "a07873dd-ef1d-48a2-9bb1-97e324c4a660",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c9e31d6a-212a-4561-805f-86a5fb69a147"
        },
        {
            "id": "dfb0ede0-6a2c-469e-ab8a-5c0f58784765",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c9e31d6a-212a-4561-805f-86a5fb69a147"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "visible": true
}