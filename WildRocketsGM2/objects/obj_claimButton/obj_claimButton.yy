{
    "id": "f5b1a471-247d-4bb9-b3ed-22429307e4d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_claimButton",
    "eventList": [
        {
            "id": "fef4657c-3e4a-4cdb-b5b0-3fe819bc3925",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f5b1a471-247d-4bb9-b3ed-22429307e4d2"
        },
        {
            "id": "58a4eed4-5e81-4261-a1be-9c48ad04eb3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f5b1a471-247d-4bb9-b3ed-22429307e4d2"
        },
        {
            "id": "efb2cbaa-6c0c-4cc8-8780-7bf5e3dc2dd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f5b1a471-247d-4bb9-b3ed-22429307e4d2"
        },
        {
            "id": "6b218ee2-bf81-4c1e-8304-f2e7737f6410",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "f5b1a471-247d-4bb9-b3ed-22429307e4d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
    "visible": true
}