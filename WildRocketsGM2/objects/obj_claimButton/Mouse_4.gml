action_sound(click, 0);
action_sprite_set(spr_gameoverbuttons, 7, 0);

if(global.dailyItem = "diamond"){
	global.money += 100;
	ini_open(working_directory + "\\moneyIni.ini");
	ini_write_real( "money", "money", global.money);
	ini_close();
	audio_play_sound(snd_chaching1,10,false);
	instance_create(x+8,y-110,obj_amount);
	with(obj_diamond){
		instance_destroy();	
	}
}
if(global.dailyItem = "life"){
	global.lifeCount ++;
	instance_create(x+8,y-110,obj_plusLife);
	with(obj_life){
		instance_destroy();	
	}
	ini_open(working_directory + "\\livesIni.ini");
	ini_write_real("Lives", "lives", global.lifeCount);
	ini_close();
}



action_set_alarm(2, 0);

