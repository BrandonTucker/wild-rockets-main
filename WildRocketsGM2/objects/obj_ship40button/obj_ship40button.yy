{
    "id": "5cb4ef17-1766-4ac4-b8a1-b5bd56fff76b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship40button",
    "eventList": [
        {
            "id": "17aaf2de-4b55-44be-b4cb-036e718ff21d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5cb4ef17-1766-4ac4-b8a1-b5bd56fff76b"
        },
        {
            "id": "ab639e7b-8291-430b-b901-8edb8b2ee9db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "5cb4ef17-1766-4ac4-b8a1-b5bd56fff76b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "50b35ca3-3504-4e6c-8395-581aa57ba9f0",
    "visible": true
}