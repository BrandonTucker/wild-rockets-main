{
    "id": "726b79c9-74df-44cb-add4-e67f25853272",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked22",
    "eventList": [
        {
            "id": "0210a2f5-8e19-48e9-bbea-7dad989f37b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "726b79c9-74df-44cb-add4-e67f25853272"
        },
        {
            "id": "702a544d-8ff7-4219-bc9e-420f515b4efa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "726b79c9-74df-44cb-add4-e67f25853272"
        },
        {
            "id": "60ca6476-17d7-4f2e-a42a-a4d56118a204",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "726b79c9-74df-44cb-add4-e67f25853272"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "18ebbeed-2514-4e7a-b28e-03811db3df4e",
    "visible": true
}