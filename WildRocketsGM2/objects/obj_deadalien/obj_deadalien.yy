{
    "id": "ad67373b-7993-4edf-9c6e-e658cb6335e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_deadalien",
    "eventList": [
        {
            "id": "215a1088-f1c7-4695-998d-3e4a97bf2b30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad67373b-7993-4edf-9c6e-e658cb6335e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
    "visible": true
}