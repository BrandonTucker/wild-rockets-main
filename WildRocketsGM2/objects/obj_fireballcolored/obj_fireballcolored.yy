{
    "id": "116c730a-5b1a-49c1-bfa6-6f0f310d91f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireballcolored",
    "eventList": [
        {
            "id": "5ff7604d-9fd6-4ff8-ae31-b2aa913131f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "116c730a-5b1a-49c1-bfa6-6f0f310d91f4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1bad23a7-f376-4890-83ed-c4b35a34036b",
    "visible": true
}