{
    "id": "914fcb10-da3f-4ac1-952b-55f85cdd63b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange28locked",
    "eventList": [
        {
            "id": "a3cbf7a9-e9f1-4fe6-a662-dc8b25d77696",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "914fcb10-da3f-4ac1-952b-55f85cdd63b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f1401ffc-b4d8-444a-b5a7-533581d15076",
    "visible": true
}