{
    "id": "49dd76e5-0204-4a66-97cd-9bfbcd4a294f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship23button",
    "eventList": [
        {
            "id": "11713e40-8bfa-4f50-9c97-44fce79d16c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "49dd76e5-0204-4a66-97cd-9bfbcd4a294f"
        },
        {
            "id": "a95d371f-582d-4e21-9c65-5c3339695244",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "49dd76e5-0204-4a66-97cd-9bfbcd4a294f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "baebe8ff-9328-44f5-b69a-6b49c16e3409",
    "visible": true
}