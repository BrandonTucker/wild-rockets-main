{
    "id": "9c80ca6e-c5a8-4552-b94d-6e0661b82a9c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shipselect",
    "eventList": [
        {
            "id": "cdf87167-989b-4cb4-9d25-35e8d22dd271",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9c80ca6e-c5a8-4552-b94d-6e0661b82a9c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "46ac9a6f-d48a-4d48-9755-860814abd4a1",
    "visible": true
}