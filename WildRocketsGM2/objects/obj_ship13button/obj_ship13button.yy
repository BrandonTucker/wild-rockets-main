{
    "id": "ba60ac7b-2037-4dd1-ad61-6c159893c2b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship13button",
    "eventList": [
        {
            "id": "f3246c06-d978-4856-be1c-6780a998ff4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ba60ac7b-2037-4dd1-ad61-6c159893c2b5"
        },
        {
            "id": "33f154f1-973d-4c2d-9937-0f35aef3eafb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ba60ac7b-2037-4dd1-ad61-6c159893c2b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a40f695e-11b8-4942-8d62-f8858ad9f9f2",
    "visible": true
}