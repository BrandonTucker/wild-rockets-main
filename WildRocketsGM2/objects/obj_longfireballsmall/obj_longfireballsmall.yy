{
    "id": "1f2f5f8f-06b8-499f-b3de-ce2ad6bdcfe3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_longfireballsmall",
    "eventList": [
        {
            "id": "c23101f4-aca8-4329-8ca6-a6bea965d695",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1f2f5f8f-06b8-499f-b3de-ce2ad6bdcfe3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bd986112-5c21-4a62-8d47-0ffaa8a9543c",
    "visible": true
}