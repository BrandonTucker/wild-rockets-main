{
    "id": "7c6a3d8b-b2de-46f2-8edd-a05157c5690f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_backgroundcomputer",
    "eventList": [
        {
            "id": "1496d89b-cb9e-4529-9225-497113d4b036",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c6a3d8b-b2de-46f2-8edd-a05157c5690f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
    "visible": true
}