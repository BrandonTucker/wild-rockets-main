{
    "id": "b01ef426-73fe-4c12-ade6-0401f527626d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship2button",
    "eventList": [
        {
            "id": "bed2095d-6cfb-42d6-8d72-7bc0f0a88952",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b01ef426-73fe-4c12-ade6-0401f527626d"
        },
        {
            "id": "88aab57c-0df2-41e8-82b3-e140a9cdb292",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b01ef426-73fe-4c12-ade6-0401f527626d"
        },
        {
            "id": "a1fc2efa-d20e-4d87-ac1a-583bf7bbdb85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b01ef426-73fe-4c12-ade6-0401f527626d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "397c9cc3-b01d-49fe-8c69-ac314c175a7d",
    "visible": true
}