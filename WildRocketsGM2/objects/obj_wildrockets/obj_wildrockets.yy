{
    "id": "7167aa03-796c-4936-b4bb-04568fd00ef5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wildrockets",
    "eventList": [
        {
            "id": "d91b3ede-1c77-47b1-a24d-4eca85175167",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7167aa03-796c-4936-b4bb-04568fd00ef5"
        },
        {
            "id": "5ba9a599-9ee9-4c7e-a342-afdbea722ac7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7167aa03-796c-4936-b4bb-04568fd00ef5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fae216a5-4a6c-4dbb-aa79-b282683b40e2",
    "visible": true
}