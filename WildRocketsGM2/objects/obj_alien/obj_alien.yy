{
    "id": "8f03b0df-3e48-429c-a2fb-daa374c71569",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_alien",
    "eventList": [
        {
            "id": "bc93016d-eadb-4764-a382-739f2689a4dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f03b0df-3e48-429c-a2fb-daa374c71569"
        },
        {
            "id": "196ad887-9f7b-4b45-bf1a-408e32feb354",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8f03b0df-3e48-429c-a2fb-daa374c71569"
        },
        {
            "id": "4799ec74-85f9-4c66-a24f-7b8d3fc6053e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8f03b0df-3e48-429c-a2fb-daa374c71569"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "e03396a2-1f32-4915-9959-54b022acab0b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "b9c51174-9342-421a-9b48-a20879d0a444",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 28
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b83e738a-3a25-49a4-b0ce-bcbf1a072512",
    "visible": true
}