{
    "id": "41ccfa24-b628-415b-9922-ef52853dec6a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship7button",
    "eventList": [
        {
            "id": "6c9cc882-bf24-414e-9168-98f722774d2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "41ccfa24-b628-415b-9922-ef52853dec6a"
        },
        {
            "id": "7b0a8b9d-8778-45e4-a744-5a8b06d8ba6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "41ccfa24-b628-415b-9922-ef52853dec6a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6179ef99-9817-4d07-a661-3345becb1beb",
    "visible": true
}