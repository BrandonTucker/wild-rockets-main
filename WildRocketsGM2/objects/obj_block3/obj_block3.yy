{
    "id": "ae1aa394-cafd-408b-889b-408815018489",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_block3",
    "eventList": [
        {
            "id": "e13684f1-d182-4a68-a227-dda22cb154f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ae1aa394-cafd-408b-889b-408815018489"
        },
        {
            "id": "856ce71b-6b00-4fbd-b99b-f3b9cdb62f4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ae1aa394-cafd-408b-889b-408815018489"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8a124835-4f29-40d3-9a6c-675ab67e0468",
    "visible": true
}