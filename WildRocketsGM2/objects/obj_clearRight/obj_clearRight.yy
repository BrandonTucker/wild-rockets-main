{
    "id": "272b30d9-6a1f-409a-a002-eb4387f01538",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_clearRight",
    "eventList": [
        {
            "id": "ab6a3ac7-4c3c-4f0a-9941-6aa5a4a93ad3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "272b30d9-6a1f-409a-a002-eb4387f01538"
        },
        {
            "id": "ac13b20a-ca2c-456f-b007-314c335dc8f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e58f85a3-957f-493b-bbd0-f7b212ae3a64",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "272b30d9-6a1f-409a-a002-eb4387f01538"
        },
        {
            "id": "ca4b5a1b-ead8-4852-a6a1-2ec936aa69a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "272b30d9-6a1f-409a-a002-eb4387f01538"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a2bbf74b-19bc-4199-83ea-5e8006bd6547",
    "visible": true
}