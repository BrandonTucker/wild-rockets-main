{
    "id": "779b2cba-a416-43dc-802b-58fd62d9c1ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_changepagenoslide1",
    "eventList": [
        {
            "id": "973fa24e-3abe-40a4-8697-e4f0d6fd5812",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "779b2cba-a416-43dc-802b-58fd62d9c1ef"
        },
        {
            "id": "9b91c2c8-d33f-4646-a472-172879554b81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "779b2cba-a416-43dc-802b-58fd62d9c1ef"
        },
        {
            "id": "cab93e1a-04b5-40eb-ad60-2aa91df7eb6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "779b2cba-a416-43dc-802b-58fd62d9c1ef"
        },
        {
            "id": "4c726de9-5005-4ba6-8858-c13ecbf67e26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "779b2cba-a416-43dc-802b-58fd62d9c1ef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a132351-f771-425b-9ba7-169939928823",
    "visible": true
}