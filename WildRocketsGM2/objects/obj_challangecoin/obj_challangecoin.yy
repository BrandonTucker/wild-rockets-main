{
    "id": "c9078f7c-c1ce-4400-a36c-a41f2a35bf44",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challangecoin",
    "eventList": [
        {
            "id": "8efa5821-f998-46ad-a2bd-75177fb8fe6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9078f7c-c1ce-4400-a36c-a41f2a35bf44"
        },
        {
            "id": "669d6fa9-7543-4647-8cf3-ef9f219a94fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c9078f7c-c1ce-4400-a36c-a41f2a35bf44"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
    "visible": true
}