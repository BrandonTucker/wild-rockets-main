{
    "id": "831ef3fa-b0c2-43e7-aeba-ca578e352cb5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship40debri1",
    "eventList": [
        {
            "id": "4cc07a47-7570-44c9-9037-4895f0311aff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "831ef3fa-b0c2-43e7-aeba-ca578e352cb5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
    "visible": true
}