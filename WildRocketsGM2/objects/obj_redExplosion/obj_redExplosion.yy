{
    "id": "87b1052f-ab47-4ae6-87a3-216679b4881c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_redExplosion",
    "eventList": [
        {
            "id": "1596372f-d978-410a-92ef-c7c00d51d926",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87b1052f-ab47-4ae6-87a3-216679b4881c"
        },
        {
            "id": "e9f7dd36-7dcc-4ed6-869b-6fe3eb6ca550",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "87b1052f-ab47-4ae6-87a3-216679b4881c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "3a6611d5-99e7-480a-9140-4bf79c82186c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "cf461775-4613-41e9-b7ac-509fdb91d374",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "11b42630-1496-4c0e-96a9-e33d6583b1a4",
    "visible": true
}