{
    "id": "27f2f029-bd57-4e87-955e-7bc44bf06f28",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship25debri3",
    "eventList": [
        {
            "id": "6c813498-882b-48f9-a014-658e09e827c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27f2f029-bd57-4e87-955e-7bc44bf06f28"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
    "visible": true
}