{
    "id": "20d1e8b6-71d5-459b-bc47-157ffc16245c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange30button",
    "eventList": [
        {
            "id": "75892127-a358-4d1a-b169-0bf814dbb848",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20d1e8b6-71d5-459b-bc47-157ffc16245c"
        },
        {
            "id": "03b78a61-ec84-4929-b84b-c7480f4bf943",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "20d1e8b6-71d5-459b-bc47-157ffc16245c"
        },
        {
            "id": "854979d0-a424-4aef-bcd2-d7f07bd523ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "20d1e8b6-71d5-459b-bc47-157ffc16245c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b841c4e1-8070-4286-a1b2-6f2dd869d311",
    "visible": true
}