{
    "id": "d171e292-67e3-4090-90ab-33c4a3e179f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship22button",
    "eventList": [
        {
            "id": "6e41cfce-a0b5-4823-910f-d11e3454ad51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d171e292-67e3-4090-90ab-33c4a3e179f7"
        },
        {
            "id": "5e3c218a-d341-470b-aa24-edba8fed91a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d171e292-67e3-4090-90ab-33c4a3e179f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fe5c36db-0cca-4675-9817-8b31601c246e",
    "visible": true
}