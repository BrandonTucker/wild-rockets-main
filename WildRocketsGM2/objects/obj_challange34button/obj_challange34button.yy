{
    "id": "6f10ede3-a201-4e76-ae74-9a01c4f3367b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange34button",
    "eventList": [
        {
            "id": "b2e94bf7-a21d-4a27-92d0-911217f795c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6f10ede3-a201-4e76-ae74-9a01c4f3367b"
        },
        {
            "id": "265edb20-08f1-42ec-b26b-3a24cfe626ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6f10ede3-a201-4e76-ae74-9a01c4f3367b"
        },
        {
            "id": "b587a5f0-4d29-4743-a05f-0ff08ef4420d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6f10ede3-a201-4e76-ae74-9a01c4f3367b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c260ea39-2dcb-44be-8318-12e7f92414de",
    "visible": true
}