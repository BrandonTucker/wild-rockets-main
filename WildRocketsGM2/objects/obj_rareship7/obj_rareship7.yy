{
    "id": "a605f157-348d-411f-88b6-866223c36874",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rareship7",
    "eventList": [
        {
            "id": "58b1093c-3eba-47ad-ac04-57c13ac242cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a605f157-348d-411f-88b6-866223c36874"
        },
        {
            "id": "f0610dbb-a2f0-4ea0-b1ae-a94d7d1004c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a605f157-348d-411f-88b6-866223c36874"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d23bb1f-1edc-411b-96e6-de1f82a81d36",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
    "visible": true
}