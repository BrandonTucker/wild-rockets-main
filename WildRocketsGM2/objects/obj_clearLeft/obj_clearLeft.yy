{
    "id": "ba2156f9-ff14-4852-9100-4b778b1b6edc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_clearLeft",
    "eventList": [
        {
            "id": "68ea9b0f-c24d-4258-9d5b-bbaa0db67cdf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba2156f9-ff14-4852-9100-4b778b1b6edc"
        },
        {
            "id": "5ee27d56-85c9-41ab-8383-ad5eef243c98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e58f85a3-957f-493b-bbd0-f7b212ae3a64",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ba2156f9-ff14-4852-9100-4b778b1b6edc"
        },
        {
            "id": "4e6ccb16-5073-463d-9852-cda53fda7fc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "ba2156f9-ff14-4852-9100-4b778b1b6edc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a2bbf74b-19bc-4199-83ea-5e8006bd6547",
    "visible": true
}