{
    "id": "ae2cc8f3-731d-4440-b94a-6629cbb770d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cloud2",
    "eventList": [
        {
            "id": "3d08f9b9-42fe-4a88-959b-5db52e49f992",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ae2cc8f3-731d-4440-b94a-6629cbb770d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "746f0ca6-62b0-43e3-af8c-80762a087846",
    "visible": true
}