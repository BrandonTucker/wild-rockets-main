{
    "id": "02cff92d-c51c-4e8a-891b-f0165f9e936f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange27button",
    "eventList": [
        {
            "id": "94475290-bb3c-4c84-90ad-066e55e3b518",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02cff92d-c51c-4e8a-891b-f0165f9e936f"
        },
        {
            "id": "4c5010c4-dff2-435b-a5a4-4bbf915edf09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "02cff92d-c51c-4e8a-891b-f0165f9e936f"
        },
        {
            "id": "b9f85606-bdd1-41ff-9bd4-95e7e4dfb066",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "02cff92d-c51c-4e8a-891b-f0165f9e936f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "419b688d-4e67-453f-8bf4-690615a18ec4",
    "visible": true
}