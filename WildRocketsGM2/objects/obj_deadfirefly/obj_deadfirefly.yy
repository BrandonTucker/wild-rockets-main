{
    "id": "f6c3a2ea-e2bd-464e-8add-ef0dd82e2b4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_deadfirefly",
    "eventList": [
        {
            "id": "c04790f5-447e-46af-9f63-f56a1a1f0b72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6c3a2ea-e2bd-464e-8add-ef0dd82e2b4b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec400107-fa1f-4b9f-820c-a9e440b0c201",
    "visible": true
}