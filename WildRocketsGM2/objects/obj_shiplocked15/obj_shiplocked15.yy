{
    "id": "1d6e728c-dda9-4231-a69d-3e5499fc761e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked15",
    "eventList": [
        {
            "id": "c00fc1c4-0174-451c-b966-aefdd14bf15a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d6e728c-dda9-4231-a69d-3e5499fc761e"
        },
        {
            "id": "4920ce17-452e-491c-adde-a03212a8443e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1d6e728c-dda9-4231-a69d-3e5499fc761e"
        },
        {
            "id": "c450e724-db92-4518-9302-856e33c44477",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "1d6e728c-dda9-4231-a69d-3e5499fc761e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d360e026-f5b3-4b77-b77a-cf11faba1a6b",
    "visible": true
}