{
    "id": "13e16ff9-1bfd-4f81-99f6-dcdc43c872e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_notEnoughLivesMessage",
    "eventList": [
        {
            "id": "1d3c7f83-b3f1-461c-a427-5b356d8b981e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "13e16ff9-1bfd-4f81-99f6-dcdc43c872e3"
        },
        {
            "id": "ece398f0-c438-4e9b-a026-11933b81b961",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "13e16ff9-1bfd-4f81-99f6-dcdc43c872e3"
        },
        {
            "id": "b6a34edd-5d10-4e70-9c8c-7f737f3352cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "13e16ff9-1bfd-4f81-99f6-dcdc43c872e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}