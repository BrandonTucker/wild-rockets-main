{
    "id": "7b2a3f1c-5850-4bc2-9b6d-831c7f5fe64d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship36button",
    "eventList": [
        {
            "id": "061ae9c3-a494-4a52-aff0-8534ee26a1d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7b2a3f1c-5850-4bc2-9b6d-831c7f5fe64d"
        },
        {
            "id": "47c4ec56-3faa-441e-83c3-d5917e79c012",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7b2a3f1c-5850-4bc2-9b6d-831c7f5fe64d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc3606d4-3295-4f75-840e-a6f607ccbb79",
    "visible": true
}