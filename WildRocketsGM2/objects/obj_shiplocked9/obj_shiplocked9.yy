{
    "id": "4f1824ea-bc55-458d-af25-a20846428538",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked9",
    "eventList": [
        {
            "id": "dc9a6464-d329-4dff-b3cf-fd73b876a034",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4f1824ea-bc55-458d-af25-a20846428538"
        },
        {
            "id": "0ba3e322-6cb9-4ba3-b463-08947d2ac58f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4f1824ea-bc55-458d-af25-a20846428538"
        },
        {
            "id": "6371ddb1-f38d-4d4c-b160-a0af613ef63d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4f1824ea-bc55-458d-af25-a20846428538"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f05a2e54-2cdb-4e34-aac2-48f6fbb8dd93",
    "visible": true
}