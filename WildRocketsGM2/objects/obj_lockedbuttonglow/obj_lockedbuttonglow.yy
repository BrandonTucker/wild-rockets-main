{
    "id": "52813bc9-cee9-4e15-b5bd-7f72898c0180",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lockedbuttonglow",
    "eventList": [
        {
            "id": "d414152a-b70e-45a0-a3f1-286a455b5b01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52813bc9-cee9-4e15-b5bd-7f72898c0180"
        },
        {
            "id": "3554e561-a709-4768-a016-bec3c3e4ce87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "52813bc9-cee9-4e15-b5bd-7f72898c0180"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f66b6c3-303a-4139-9858-dfe2434a1aa4",
    "visible": true
}