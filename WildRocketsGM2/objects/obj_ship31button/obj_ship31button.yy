{
    "id": "3e8c1a20-bfb7-4e08-982f-32f038ca8e90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship31button",
    "eventList": [
        {
            "id": "04f6f515-dcec-4bfd-b545-71594aeeb469",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3e8c1a20-bfb7-4e08-982f-32f038ca8e90"
        },
        {
            "id": "47eecbef-b3fe-4c55-9339-0b8d4da6df8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3e8c1a20-bfb7-4e08-982f-32f038ca8e90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5242c6ca-9f84-4f80-9cfd-59b5530f34dd",
    "visible": true
}