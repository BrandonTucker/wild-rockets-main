{
    "id": "589c3857-142e-4ac1-aa59-9541f125594b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange4button",
    "eventList": [
        {
            "id": "62bb3280-7b96-4430-9cd2-160efd839864",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "589c3857-142e-4ac1-aa59-9541f125594b"
        },
        {
            "id": "74e08541-6d9b-451c-9aba-ff2f37de8f19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "589c3857-142e-4ac1-aa59-9541f125594b"
        },
        {
            "id": "1e3ddfc3-eca8-4a9a-9858-0daef10f0f55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "589c3857-142e-4ac1-aa59-9541f125594b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "086d938d-62cf-42b0-8904-a954553e1613",
    "visible": true
}