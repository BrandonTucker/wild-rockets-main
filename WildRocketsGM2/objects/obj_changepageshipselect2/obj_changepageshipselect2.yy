{
    "id": "32562798-0379-4f79-ab04-c3131cd908a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_changepageshipselect2",
    "eventList": [
        {
            "id": "aef01f6b-9cc9-483b-b1b9-cc95c429ea58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32562798-0379-4f79-ab04-c3131cd908a4"
        },
        {
            "id": "0b829621-9814-42aa-8aa7-ac7332f39f47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "32562798-0379-4f79-ab04-c3131cd908a4"
        },
        {
            "id": "6adfff1d-4af4-42ed-9551-be2a21331dab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "32562798-0379-4f79-ab04-c3131cd908a4"
        },
        {
            "id": "ecc23829-de00-4645-8eda-e4d074c67507",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "32562798-0379-4f79-ab04-c3131cd908a4"
        },
        {
            "id": "8aab51e8-4832-40f3-b277-e2704609815a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "32562798-0379-4f79-ab04-c3131cd908a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a132351-f771-425b-9ba7-169939928823",
    "visible": true
}