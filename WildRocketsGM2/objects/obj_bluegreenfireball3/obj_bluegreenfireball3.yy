{
    "id": "afa60e46-9ca6-431c-8024-ee3dc073367a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bluegreenfireball3",
    "eventList": [
        {
            "id": "f1bcf85e-4ad1-4ace-9bdb-249c068281b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "afa60e46-9ca6-431c-8024-ee3dc073367a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "35ed9158-887c-4608-94af-c01eaddf5b61",
    "visible": true
}