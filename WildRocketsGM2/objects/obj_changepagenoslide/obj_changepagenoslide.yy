{
    "id": "b842017e-c988-4757-a8bc-9e3bc792b36d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_changepagenoslide",
    "eventList": [
        {
            "id": "1ca73d72-8c8e-41f5-8493-39c60396daae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b842017e-c988-4757-a8bc-9e3bc792b36d"
        },
        {
            "id": "abe8a86c-4051-49c0-a3a1-75e452a1456e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b842017e-c988-4757-a8bc-9e3bc792b36d"
        },
        {
            "id": "cec6c32c-a13a-422e-9647-8571c5534606",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b842017e-c988-4757-a8bc-9e3bc792b36d"
        },
        {
            "id": "86bb7494-9e24-4ca5-9d49-b2d9f5e04abe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b842017e-c988-4757-a8bc-9e3bc792b36d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a132351-f771-425b-9ba7-169939928823",
    "visible": true
}