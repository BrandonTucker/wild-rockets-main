{
    "id": "454be9d0-c5fc-49a0-803e-4fcb73501f2a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange7button",
    "eventList": [
        {
            "id": "baf9ce34-99ff-4459-97ec-266e1101b1ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "454be9d0-c5fc-49a0-803e-4fcb73501f2a"
        },
        {
            "id": "a5dd7900-44d5-44c3-9fdf-f990e8067d48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "454be9d0-c5fc-49a0-803e-4fcb73501f2a"
        },
        {
            "id": "d32b29ad-7bc7-4d17-b7db-84ded19c906e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "454be9d0-c5fc-49a0-803e-4fcb73501f2a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d5b99ed3-952f-4bd3-81c0-94b5f0ad2203",
    "visible": true
}