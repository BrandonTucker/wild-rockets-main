{
    "id": "d9dbac41-4864-4537-9088-ce755a8262a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_metalchunk",
    "eventList": [
        {
            "id": "596c8c27-a80b-4e3b-901b-bb7d247a6fa1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9dbac41-4864-4537-9088-ce755a8262a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
    "visible": true
}