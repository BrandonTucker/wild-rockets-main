{
    "id": "ef8a44a6-3c3b-4ec0-b1d4-0954a5632bb2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_records",
    "eventList": [
        {
            "id": "b468dbab-38a5-436c-887f-0502f518cb82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef8a44a6-3c3b-4ec0-b1d4-0954a5632bb2"
        },
        {
            "id": "00f7a702-1994-4424-adb1-eed90f3845b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ef8a44a6-3c3b-4ec0-b1d4-0954a5632bb2"
        },
        {
            "id": "d1014c4e-cadf-4d0e-a636-a156a0eae617",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ef8a44a6-3c3b-4ec0-b1d4-0954a5632bb2"
        },
        {
            "id": "d28c5a1b-1d31-4c9a-bcd1-6869d9747a99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ef8a44a6-3c3b-4ec0-b1d4-0954a5632bb2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "visible": true
}