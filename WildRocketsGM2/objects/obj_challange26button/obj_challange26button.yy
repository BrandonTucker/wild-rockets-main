{
    "id": "0ea566e8-6550-4d56-8707-eca32e9fd9bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange26button",
    "eventList": [
        {
            "id": "eb2274d1-61e9-4156-8ed5-fe411563b2db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0ea566e8-6550-4d56-8707-eca32e9fd9bf"
        },
        {
            "id": "d9967bae-85f4-4216-b40a-a261aaf17e76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0ea566e8-6550-4d56-8707-eca32e9fd9bf"
        },
        {
            "id": "193cd7a7-8df1-4cc0-b3af-ca06faaa1878",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0ea566e8-6550-4d56-8707-eca32e9fd9bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "71042fe6-5fc5-4d6e-abbf-567e5cfa9061",
    "visible": true
}