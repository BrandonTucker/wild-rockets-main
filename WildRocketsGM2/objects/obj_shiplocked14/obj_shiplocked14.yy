{
    "id": "6db82fdd-9c91-43e6-bab4-33835a6ef399",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked14",
    "eventList": [
        {
            "id": "c5728e08-b68c-4de2-aa33-4a1f3c03b337",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6db82fdd-9c91-43e6-bab4-33835a6ef399"
        },
        {
            "id": "053bde4f-10b3-4cc3-ae93-51c1ccc24936",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6db82fdd-9c91-43e6-bab4-33835a6ef399"
        },
        {
            "id": "d875c3c5-f498-4b66-a374-60aab5e11643",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6db82fdd-9c91-43e6-bab4-33835a6ef399"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8035daf9-2885-45c5-ba7d-064165169889",
    "visible": true
}