{
    "id": "b1135235-251b-424c-a03a-6391cd266789",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked4",
    "eventList": [
        {
            "id": "c2f8e883-b238-4527-9c7c-c68427c79306",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1135235-251b-424c-a03a-6391cd266789"
        },
        {
            "id": "752dd742-f36f-4f4d-8778-8857bdb5e871",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b1135235-251b-424c-a03a-6391cd266789"
        },
        {
            "id": "5a226340-590e-47f3-8ffb-aed56b9187a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b1135235-251b-424c-a03a-6391cd266789"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "162591cd-c4f4-455a-83fe-e8023d278525",
    "visible": true
}