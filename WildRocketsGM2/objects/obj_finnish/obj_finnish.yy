{
    "id": "46470b78-eebc-46ac-a574-30a2bd056348",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_finnish",
    "eventList": [
        {
            "id": "9b75cfbf-ca47-4651-a2c8-5db91b37fdaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46470b78-eebc-46ac-a574-30a2bd056348"
        },
        {
            "id": "dc1f6c68-1489-4ad6-bdb0-d548a85d8c4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "46470b78-eebc-46ac-a574-30a2bd056348"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0b298d78-4504-4f2c-9f1f-b5fba0259958",
    "visible": true
}