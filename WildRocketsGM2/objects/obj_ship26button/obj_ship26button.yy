{
    "id": "d52e5d2b-734e-46cf-8f10-79d632d35bf0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship26button",
    "eventList": [
        {
            "id": "aba68c9c-7e1f-4498-8c2c-c0612bc195e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d52e5d2b-734e-46cf-8f10-79d632d35bf0"
        },
        {
            "id": "211ab067-abfb-4000-8aeb-2b69dee0c59b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d52e5d2b-734e-46cf-8f10-79d632d35bf0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3e44dd5f-c9ce-4302-a0ac-8cdbbf1f4cac",
    "visible": true
}