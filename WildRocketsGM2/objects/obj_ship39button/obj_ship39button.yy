{
    "id": "8b51407e-808f-4233-b938-2b90f61d4110",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship39button",
    "eventList": [
        {
            "id": "81b64a66-31d1-4624-8260-a2e7fad396b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8b51407e-808f-4233-b938-2b90f61d4110"
        },
        {
            "id": "5105b884-01ae-4f61-b6b2-0f73c5e6bc67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8b51407e-808f-4233-b938-2b90f61d4110"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b52b60ea-c57e-47bb-b199-b154b7ab2583",
    "visible": true
}