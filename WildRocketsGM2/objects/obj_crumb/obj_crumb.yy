{
    "id": "762e292a-72ca-4504-b6bc-1be59ed3877b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crumb",
    "eventList": [
        {
            "id": "f2a6df60-8ca7-4273-ae39-598c3b0afb23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "762e292a-72ca-4504-b6bc-1be59ed3877b"
        },
        {
            "id": "56862ca4-f5b6-45d3-8909-a65e0ea74105",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "762e292a-72ca-4504-b6bc-1be59ed3877b"
        },
        {
            "id": "0bffd067-21b3-43f4-9313-703fcb13420d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e58f85a3-957f-493b-bbd0-f7b212ae3a64",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "762e292a-72ca-4504-b6bc-1be59ed3877b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
    "visible": true
}