{
    "id": "433e6ca3-7832-442f-8360-9dc514dff545",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship35button",
    "eventList": [
        {
            "id": "e08dfdae-0cf5-45ff-8ed4-477e6fc23770",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "433e6ca3-7832-442f-8360-9dc514dff545"
        },
        {
            "id": "9931883b-93d0-4213-a8e2-7ae6eb1f8115",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "433e6ca3-7832-442f-8360-9dc514dff545"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f8851adc-8783-479f-9de3-f0f8771910ea",
    "visible": true
}