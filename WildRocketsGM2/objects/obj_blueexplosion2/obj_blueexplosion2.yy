{
    "id": "e80db6e8-c8f3-48a1-90e4-a9b08e0b40a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blueexplosion2",
    "eventList": [
        {
            "id": "44a3e2c8-d92f-40fa-a40a-3f7aee1e8923",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e80db6e8-c8f3-48a1-90e4-a9b08e0b40a9"
        },
        {
            "id": "e2d15e4f-cde8-4d2a-bb12-b6f8235ac275",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "e80db6e8-c8f3-48a1-90e4-a9b08e0b40a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "744e5b6a-d241-4c9e-8466-3f687de4e23b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "825863fd-fca4-4e01-a3e4-c29f84ec7134",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1547e11e-dae4-47b2-ab52-e3400ace6404",
    "visible": true
}