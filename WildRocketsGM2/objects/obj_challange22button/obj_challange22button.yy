{
    "id": "9db96b9f-1dd6-4f02-adec-46c2e92badd1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange22button",
    "eventList": [
        {
            "id": "b6ac1ad7-b95e-4bfe-be18-295fa96ac3af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9db96b9f-1dd6-4f02-adec-46c2e92badd1"
        },
        {
            "id": "ba5ade09-9700-4c83-9175-645c354c0283",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9db96b9f-1dd6-4f02-adec-46c2e92badd1"
        },
        {
            "id": "e4cda0df-cbcf-467d-9998-0fb4cc30fb61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "9db96b9f-1dd6-4f02-adec-46c2e92badd1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "23fff036-1f11-4f75-89b2-8e4b130fcd90",
    "visible": true
}