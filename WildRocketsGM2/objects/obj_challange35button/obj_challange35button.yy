{
    "id": "fd336870-54ec-4ce0-b790-37a5206232ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange35button",
    "eventList": [
        {
            "id": "f2942694-b5e7-43f0-a785-3bd0f30a5f01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd336870-54ec-4ce0-b790-37a5206232ff"
        },
        {
            "id": "50e3b47e-35e8-494c-ae44-14a04d6a36f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fd336870-54ec-4ce0-b790-37a5206232ff"
        },
        {
            "id": "5c71fce5-9fc3-4d8b-b007-488bf94f5ec6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "fd336870-54ec-4ce0-b790-37a5206232ff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7f6892cc-2d4a-466f-8820-7f8ec505d8d5",
    "visible": true
}