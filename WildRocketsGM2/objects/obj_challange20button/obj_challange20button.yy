{
    "id": "43275ee0-4963-454e-898e-a6270448f319",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange20button",
    "eventList": [
        {
            "id": "83951ae8-1ea8-422e-a1da-4fd603370b76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43275ee0-4963-454e-898e-a6270448f319"
        },
        {
            "id": "c02e40e3-1de9-48a9-9f64-94d394dabbea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43275ee0-4963-454e-898e-a6270448f319"
        },
        {
            "id": "bd6a66cd-5313-4e19-9869-addceb701c4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "43275ee0-4963-454e-898e-a6270448f319"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1adfe3b9-b5ae-44d7-8830-99c43c2af46a",
    "visible": true
}