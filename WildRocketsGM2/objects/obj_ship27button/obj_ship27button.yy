{
    "id": "fc752c67-9015-49ed-8399-cc61cce7e230",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship27button",
    "eventList": [
        {
            "id": "1cb6d22d-bf16-46b8-9961-4e3b6bb560ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fc752c67-9015-49ed-8399-cc61cce7e230"
        },
        {
            "id": "21220cc8-b395-4cf3-acaa-aee895dda957",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "fc752c67-9015-49ed-8399-cc61cce7e230"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "18150916-85a3-4111-bf1c-22a473d05262",
    "visible": true
}