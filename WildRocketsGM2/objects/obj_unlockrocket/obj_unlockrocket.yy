{
    "id": "3aee6cc1-8124-427f-b82f-ca1426ea01d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_unlockrocket",
    "eventList": [
        {
            "id": "546cb83a-3014-4626-b4f7-541a89618649",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3aee6cc1-8124-427f-b82f-ca1426ea01d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9d72671-5ad2-46e0-bce2-97dcb5f5a20d",
    "visible": true
}