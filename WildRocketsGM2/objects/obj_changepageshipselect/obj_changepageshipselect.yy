{
    "id": "78743de2-29b7-4ff6-81fe-3f080917f21b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_changepageshipselect",
    "eventList": [
        {
            "id": "fb754e1e-554f-4fb3-8ce6-71513f1c5fef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78743de2-29b7-4ff6-81fe-3f080917f21b"
        },
        {
            "id": "806b7e55-f5b8-4c81-b695-7690989d0383",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "78743de2-29b7-4ff6-81fe-3f080917f21b"
        },
        {
            "id": "875659c1-f19e-4871-9b12-cfd1be75e376",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "78743de2-29b7-4ff6-81fe-3f080917f21b"
        },
        {
            "id": "d73d4ec3-1bed-414e-806b-35c8a69ccf62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "78743de2-29b7-4ff6-81fe-3f080917f21b"
        },
        {
            "id": "e2a883c2-3b54-481c-ab13-654718d0a25e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "78743de2-29b7-4ff6-81fe-3f080917f21b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a132351-f771-425b-9ba7-169939928823",
    "visible": true
}