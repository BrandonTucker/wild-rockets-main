{
    "id": "264df042-92f1-4dce-aa58-e291296304e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked37",
    "eventList": [
        {
            "id": "07bc5550-ea49-4694-b9b9-c7c14ef8ebe1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "264df042-92f1-4dce-aa58-e291296304e4"
        },
        {
            "id": "98c1f0ff-ec08-4448-800e-781a477b8b98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "264df042-92f1-4dce-aa58-e291296304e4"
        },
        {
            "id": "3c6e999e-531b-44ff-aff5-cc1c1ed9becf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "264df042-92f1-4dce-aa58-e291296304e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
    "visible": true
}