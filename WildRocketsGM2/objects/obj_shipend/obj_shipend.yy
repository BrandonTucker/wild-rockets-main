{
    "id": "330c1eea-b3f1-4cde-a0c8-6a9708cf4a38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shipend",
    "eventList": [
        {
            "id": "e5f174d6-93b1-415a-891d-d6861d113984",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "330c1eea-b3f1-4cde-a0c8-6a9708cf4a38"
        },
        {
            "id": "5c07cc55-60bd-4b8a-bd3f-0fe2c37eac13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "330c1eea-b3f1-4cde-a0c8-6a9708cf4a38"
        },
        {
            "id": "adc011f2-8539-4f6d-9fa6-028db8aade0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "330c1eea-b3f1-4cde-a0c8-6a9708cf4a38"
        },
        {
            "id": "0bcfcb54-dbcd-456a-854f-95efbb5edcb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "330c1eea-b3f1-4cde-a0c8-6a9708cf4a38"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "d581a604-0edb-48fe-a17a-75f994202d6e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "73e0a9bd-466c-4b17-a2ac-f3d1e34863c8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 28
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
    "visible": true
}