action_set_relative(0);
action_sprite_transform(size, size, 0, 0);
size = 1;
{
action_set_relative(1);
action_create_object(obj_smoke2, choose(-15,-10,-5,0,5,10,15), 30);
action_set_relative(0);
}
var __b__;
__b__ = action_if_variable(abs(obj_shipend.x-320), 10, 2);
if __b__
{
{
__b__ = action_if_variable(obj_shipend.x, 320, 2);
if __b__
{
{
action_set_hspeed(-2);
}
}
__b__ = action_if_variable(obj_shipend.x, 320, 1);
if __b__
{
{
action_set_hspeed(2);
}
}
}
}
else
{
action_set_hspeed(0);
}
__b__ = action_if_variable(obj_shipend.y, 0, 1);
if __b__
{
{
action_create_object(obj_challangecomplete, 320, 300);
action_kill_object();
}
}
__b__ = action_if_variable(fireworks, 1, 0);
if __b__
{
{
{
action_set_relative(1);
action_create_object(obj_blueexplosion1, choose(-150,-110,-80,80,110,150), random_range(-30,30));
action_set_relative(0);
}
{
action_set_relative(1);
action_create_object(obj_blueexplosion2, choose(-150,-110,-80,80,110,150), random_range(-30,30));
action_set_relative(0);
}
}
}
action_set_relative(0);
