{
    "id": "e8467694-0c93-4ccd-abf9-3258ce7eaf62",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_finnishlinedis",
    "eventList": [
        {
            "id": "8d859810-87c6-44b8-af67-781128921c7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e8467694-0c93-4ccd-abf9-3258ce7eaf62"
        },
        {
            "id": "a7101479-6c79-4847-b292-fef1166616f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e8467694-0c93-4ccd-abf9-3258ce7eaf62"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
    "visible": true
}