{
    "id": "a7074366-7334-4759-8462-bf6e10aac442",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coinhighscore",
    "eventList": [
        {
            "id": "e2aaf8cf-cfda-40f0-8f27-7ec1c0511ac2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a7074366-7334-4759-8462-bf6e10aac442"
        },
        {
            "id": "857c5d96-e9d0-4895-bd8f-5cc7de3031a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a7074366-7334-4759-8462-bf6e10aac442"
        },
        {
            "id": "6e95ae95-6721-4618-888f-d3d125892a83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "da870eb4-e8ce-4fb3-bda5-2864fbd7648c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7074366-7334-4759-8462-bf6e10aac442"
        },
        {
            "id": "4a9523a0-6dd6-4c01-b310-6e0c017966c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e58f85a3-957f-493b-bbd0-f7b212ae3a64",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7074366-7334-4759-8462-bf6e10aac442"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
    "visible": true
}