{
    "id": "46f2e73c-a747-4d67-8360-091e1029ada1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange14button",
    "eventList": [
        {
            "id": "59807f82-a542-4121-aa30-46a323708894",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46f2e73c-a747-4d67-8360-091e1029ada1"
        },
        {
            "id": "8b47ae21-c77c-4580-abac-6f8d445ceb3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "46f2e73c-a747-4d67-8360-091e1029ada1"
        },
        {
            "id": "5f59d698-9aea-4843-be61-01dc0d9d95f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "46f2e73c-a747-4d67-8360-091e1029ada1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3faa7578-be8a-4cf8-a152-62b5bb4cfc24",
    "visible": true
}