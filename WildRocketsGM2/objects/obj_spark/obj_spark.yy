{
    "id": "3186a1ea-eb5d-4a86-9410-905b71099c0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spark",
    "eventList": [
        {
            "id": "bea21b00-834a-4807-b243-41b563baa1f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3186a1ea-eb5d-4a86-9410-905b71099c0c"
        },
        {
            "id": "3f370284-b817-46db-a258-05dc74d3d6da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "3186a1ea-eb5d-4a86-9410-905b71099c0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cc9cfa6d-858e-4bcd-90ed-57c4d505560e",
    "visible": true
}