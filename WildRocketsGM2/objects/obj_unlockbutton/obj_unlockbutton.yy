{
    "id": "d81745c4-fc47-44ee-9b2c-03f5f67cfcb1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_unlockbutton",
    "eventList": [
        {
            "id": "5e7b0ff1-7c48-4af3-89cb-d0dd507cb4d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d81745c4-fc47-44ee-9b2c-03f5f67cfcb1"
        },
        {
            "id": "22491854-8410-4075-a8d1-9fb72a778d6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d81745c4-fc47-44ee-9b2c-03f5f67cfcb1"
        },
        {
            "id": "175a73eb-cb50-4f56-8b5f-4c4ab509b2ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d81745c4-fc47-44ee-9b2c-03f5f67cfcb1"
        },
        {
            "id": "7bf32de3-2ee1-4016-b689-cb3580c59e10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d81745c4-fc47-44ee-9b2c-03f5f67cfcb1"
        },
        {
            "id": "4c2ac015-5058-43bf-8844-42cffd0d55bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d81745c4-fc47-44ee-9b2c-03f5f67cfcb1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5a986cdc-fc23-45ff-9083-d7fa358049ca",
    "visible": true
}