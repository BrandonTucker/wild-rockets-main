{
    "id": "c39692bb-9847-446a-9c6c-3c53e3033391",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship10button",
    "eventList": [
        {
            "id": "1a94e518-554e-4430-b1c6-21bc15dcb390",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c39692bb-9847-446a-9c6c-3c53e3033391"
        },
        {
            "id": "ec231833-67b9-440d-8691-4a343573e3d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c39692bb-9847-446a-9c6c-3c53e3033391"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8fe687e1-f8e4-4819-9b45-4dc617f8835e",
    "visible": true
}