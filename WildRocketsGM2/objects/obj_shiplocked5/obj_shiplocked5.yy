{
    "id": "6e5bddcf-3f80-4bfd-9f00-7b007c659d97",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked5",
    "eventList": [
        {
            "id": "b2a599e7-1a3a-42df-bc7a-fbb8866ae1f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6e5bddcf-3f80-4bfd-9f00-7b007c659d97"
        },
        {
            "id": "06aa8905-5025-4f15-b954-4ad4ccac6f51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6e5bddcf-3f80-4bfd-9f00-7b007c659d97"
        },
        {
            "id": "de74788b-aacf-405b-aab8-84ddaf9b7b6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6e5bddcf-3f80-4bfd-9f00-7b007c659d97"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fd1b0ff7-6f93-459d-9f71-dd331ede0a50",
    "visible": true
}