{
    "id": "4970b960-6798-4bbe-99a9-c31463442793",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_yellowfireball",
    "eventList": [
        {
            "id": "588ba679-eec1-42f6-802b-bd7e2ae4d2f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4970b960-6798-4bbe-99a9-c31463442793"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0989f38-e81c-4b6a-9efc-7ff51fabeb8c",
    "visible": true
}