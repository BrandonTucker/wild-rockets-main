{
    "id": "4a79273a-cbe0-4b1b-b699-9ea3ff895089",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_unlockyesbutton",
    "eventList": [
        {
            "id": "6c775b0b-224f-43d0-92ce-92c3fb8318c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a79273a-cbe0-4b1b-b699-9ea3ff895089"
        },
        {
            "id": "c9babcdb-af9a-4e93-8822-32e278507328",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "4a79273a-cbe0-4b1b-b699-9ea3ff895089"
        },
        {
            "id": "2159602c-617d-48ca-b056-b21209e1dd75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "4a79273a-cbe0-4b1b-b699-9ea3ff895089"
        },
        {
            "id": "cf365bb5-a6a7-4b78-9be6-8b0de5fe5c68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4a79273a-cbe0-4b1b-b699-9ea3ff895089"
        },
        {
            "id": "e80d40b7-e33f-41d4-a279-b08a21f87499",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4a79273a-cbe0-4b1b-b699-9ea3ff895089"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9ef46c4-79be-4962-ad73-297f81e4fbe3",
    "visible": true
}