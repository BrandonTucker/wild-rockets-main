{
    "id": "a9543da9-8af0-423f-823c-d9a1eca3c68d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship34debri2",
    "eventList": [
        {
            "id": "f9c8fb1a-e7d8-4938-b08c-3e6e621faa9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a9543da9-8af0-423f-823c-d9a1eca3c68d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
    "visible": true
}