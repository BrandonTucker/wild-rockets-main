{
    "id": "f44370e9-f5e9-4efd-b762-f4c838a14df2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange21button",
    "eventList": [
        {
            "id": "5f9c3c9d-6e88-45f6-bb21-1477ac634291",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f44370e9-f5e9-4efd-b762-f4c838a14df2"
        },
        {
            "id": "12386939-3ce8-4966-bd55-88f85b3f64e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f44370e9-f5e9-4efd-b762-f4c838a14df2"
        },
        {
            "id": "bc57f440-7d62-4df0-b6ff-0b6a83a47c59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "f44370e9-f5e9-4efd-b762-f4c838a14df2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aa48d7c4-c9ee-46be-9a2a-27899522402e",
    "visible": true
}