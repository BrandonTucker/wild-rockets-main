{
    "id": "c6063fc1-509b-4936-9711-168ff29e7a42",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange17button",
    "eventList": [
        {
            "id": "d9aa3d26-0682-4f5b-8e7f-3d10572fc314",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c6063fc1-509b-4936-9711-168ff29e7a42"
        },
        {
            "id": "d4b2f35e-08af-46c9-ab3a-cdceec12c20b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c6063fc1-509b-4936-9711-168ff29e7a42"
        },
        {
            "id": "12703d05-097d-492e-8bf7-e665fe952907",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c6063fc1-509b-4936-9711-168ff29e7a42"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e7915e79-8398-4c9d-8d2f-34716e045eb6",
    "visible": true
}