{
    "id": "de3995ba-4d90-460f-a859-0d2ecdbc173c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coinfly",
    "eventList": [
        {
            "id": "19ee9221-f7ef-4acf-ac4f-5dfdb36699a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de3995ba-4d90-460f-a859-0d2ecdbc173c"
        },
        {
            "id": "21aa8b18-a1f4-49f3-8dcb-dfe28a467cf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "de3995ba-4d90-460f-a859-0d2ecdbc173c"
        },
        {
            "id": "eeaf9e0c-d782-439e-95ac-1e99f9801ef3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "de3995ba-4d90-460f-a859-0d2ecdbc173c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
    "visible": true
}