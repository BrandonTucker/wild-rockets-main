{
    "id": "8ca6a1dd-7ad8-4092-98f0-bbded4d6b746",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked27",
    "eventList": [
        {
            "id": "1d022a8f-e994-4fdc-bc74-1a76356496dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ca6a1dd-7ad8-4092-98f0-bbded4d6b746"
        },
        {
            "id": "b079ae7e-174e-4050-9def-a6620f588c10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8ca6a1dd-7ad8-4092-98f0-bbded4d6b746"
        },
        {
            "id": "86438601-5983-4bfb-a7a1-dc81811f242b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8ca6a1dd-7ad8-4092-98f0-bbded4d6b746"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b2724177-762a-4901-83f0-3a6813bf42c6",
    "visible": true
}