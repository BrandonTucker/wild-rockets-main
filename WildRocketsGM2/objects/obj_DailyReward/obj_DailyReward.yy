{
    "id": "68b3b60c-0e12-4a1e-8631-93cfb6698645",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_DailyReward",
    "eventList": [
        {
            "id": "4e41ddcb-5142-446c-8048-890de5adac84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68b3b60c-0e12-4a1e-8631-93cfb6698645"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c2e1f965-a215-4df9-9553-55a4d0d8cbec",
    "visible": true
}