{
    "id": "b429b396-4979-4f9f-9595-e0469e013d41",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pineapple",
    "eventList": [
        {
            "id": "d66045c4-a641-4280-a5ba-0c6d28944149",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b429b396-4979-4f9f-9595-e0469e013d41"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
    "visible": true
}