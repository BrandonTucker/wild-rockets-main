{
    "id": "e1acf2bc-c47b-4d60-aed9-2cb1992dacb9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_highscorecomplete",
    "eventList": [
        {
            "id": "eb9ce29a-64b4-4703-8cb1-8a8515aa8d29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e1acf2bc-c47b-4d60-aed9-2cb1992dacb9"
        },
        {
            "id": "96345a8b-40ae-4a53-b4d9-0fbdd920bc33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "e1acf2bc-c47b-4d60-aed9-2cb1992dacb9"
        },
        {
            "id": "62771c1e-050c-4d1e-a416-49ca96725193",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e1acf2bc-c47b-4d60-aed9-2cb1992dacb9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}