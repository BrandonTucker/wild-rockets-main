{
    "id": "befa81bd-dff8-4b4b-9e2b-43df8ce55f69",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked18",
    "eventList": [
        {
            "id": "87ea301c-9909-4852-95e5-9dd58edd6d7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "befa81bd-dff8-4b4b-9e2b-43df8ce55f69"
        },
        {
            "id": "1d952c40-7e58-4ebf-bd5d-ad2627f2a1e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "befa81bd-dff8-4b4b-9e2b-43df8ce55f69"
        },
        {
            "id": "bc28048e-97dc-46df-90f4-9ab1fd3974f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "befa81bd-dff8-4b4b-9e2b-43df8ce55f69"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a406a008-4cc4-40e5-8edf-fd229bb50e25",
    "visible": true
}