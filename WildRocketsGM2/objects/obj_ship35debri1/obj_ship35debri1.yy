{
    "id": "718fd8f8-99b1-441c-bd60-e73b40cf4ab4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship35debri1",
    "eventList": [
        {
            "id": "5e14929b-03df-414f-80fe-67b34b5df83f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "718fd8f8-99b1-441c-bd60-e73b40cf4ab4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
    "visible": true
}