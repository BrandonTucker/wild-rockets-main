{
    "id": "29d11557-eab2-4f12-b2b4-0c609629ad3d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange31button",
    "eventList": [
        {
            "id": "153b6531-9cd1-486d-a6bd-a0581b3f97ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29d11557-eab2-4f12-b2b4-0c609629ad3d"
        },
        {
            "id": "3569dba7-c53a-476c-afd9-41ec003777a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29d11557-eab2-4f12-b2b4-0c609629ad3d"
        },
        {
            "id": "acedd448-117b-436d-a4f8-09594bf51da7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "29d11557-eab2-4f12-b2b4-0c609629ad3d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b1cbacb5-7185-4cdb-8904-443819774c6d",
    "visible": true
}