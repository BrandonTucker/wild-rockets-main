{
    "id": "027c29cb-a62b-41dc-8554-f39caf536af3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flyingpinapple",
    "eventList": [
        {
            "id": "fb1b3e8d-e884-494a-b608-f6bec3d2a2ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "027c29cb-a62b-41dc-8554-f39caf536af3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
    "visible": true
}