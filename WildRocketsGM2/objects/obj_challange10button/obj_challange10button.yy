{
    "id": "b8bb083b-26d1-4675-885b-93adb713e510",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange10button",
    "eventList": [
        {
            "id": "b7312844-94fd-40e2-a998-98dccac0430c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8bb083b-26d1-4675-885b-93adb713e510"
        },
        {
            "id": "0a857869-ec48-4c82-9a7f-f95a3eb57c83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b8bb083b-26d1-4675-885b-93adb713e510"
        },
        {
            "id": "33b32091-2774-4df8-a570-75b5d79ace80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b8bb083b-26d1-4675-885b-93adb713e510"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22273fa2-cd66-4357-a5da-e8c10b53df50",
    "visible": true
}