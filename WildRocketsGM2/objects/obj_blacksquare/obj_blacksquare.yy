{
    "id": "b55e1166-db7c-4c00-a1f9-817cd9c75b01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blacksquare",
    "eventList": [
        {
            "id": "238c0e8b-08e6-4892-a389-7e36e01c13d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b55e1166-db7c-4c00-a1f9-817cd9c75b01"
        },
        {
            "id": "f600d387-14a4-4226-8397-8f9cae6f8ae2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "b55e1166-db7c-4c00-a1f9-817cd9c75b01"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
    "visible": true
}