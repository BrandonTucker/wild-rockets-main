{
    "id": "175152ba-517e-4128-8737-63391201a10e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rareship3",
    "eventList": [
        {
            "id": "47d72f22-4044-4cbd-bcf5-299b417f0141",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "175152ba-517e-4128-8737-63391201a10e"
        },
        {
            "id": "6feb7306-3365-43c8-ae98-2300b835cf8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "175152ba-517e-4128-8737-63391201a10e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d23bb1f-1edc-411b-96e6-de1f82a81d36",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
    "visible": true
}