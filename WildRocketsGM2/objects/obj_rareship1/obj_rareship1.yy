{
    "id": "9eda4fb4-6b90-451e-9978-c619ecd9dbd6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rareship1",
    "eventList": [
        {
            "id": "93e487ad-9164-48f8-8298-17aab9e4792c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9eda4fb4-6b90-451e-9978-c619ecd9dbd6"
        },
        {
            "id": "e5f6931b-5247-44e8-b16f-5ef6431ceaad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9eda4fb4-6b90-451e-9978-c619ecd9dbd6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d23bb1f-1edc-411b-96e6-de1f82a81d36",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1332a035-f772-437c-addd-a65068078d7f",
    "visible": true
}