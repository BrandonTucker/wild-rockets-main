{
    "id": "b2c806e7-6a9a-4f3b-9b91-2d48f96fd054",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked10",
    "eventList": [
        {
            "id": "a92a4d37-3c70-4f4f-9882-feaaa3f47b77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2c806e7-6a9a-4f3b-9b91-2d48f96fd054"
        },
        {
            "id": "5bb80f47-fb43-43bd-80d5-ff7d7d8eb1ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2c806e7-6a9a-4f3b-9b91-2d48f96fd054"
        },
        {
            "id": "d7b33d86-8933-48f0-b02e-d5725822da80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b2c806e7-6a9a-4f3b-9b91-2d48f96fd054"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9513ff19-1a88-4059-a1b7-919a717582c6",
    "visible": true
}