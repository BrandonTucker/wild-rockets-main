{
    "id": "808e9a51-78f3-45ce-9d20-d735c36a4a1f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_exit",
    "eventList": [
        {
            "id": "86596ddb-cd84-4e0d-9982-a752fe9a6981",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "808e9a51-78f3-45ce-9d20-d735c36a4a1f"
        },
        {
            "id": "c3783a91-fad0-4e25-97f4-53aa2c0cb7b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "808e9a51-78f3-45ce-9d20-d735c36a4a1f"
        },
        {
            "id": "f5e82aa1-5056-4dba-bd4d-9f158316224b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "808e9a51-78f3-45ce-9d20-d735c36a4a1f"
        },
        {
            "id": "cfcc7087-4e12-49ef-9816-45a23f2d864d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "808e9a51-78f3-45ce-9d20-d735c36a4a1f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "102617c5-cacf-4458-8d4a-9756e75ef5d8",
    "visible": true
}