{
    "id": "8c0d0c65-ddb4-4e64-831a-34c03f387ad3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_greenmetalchunk",
    "eventList": [
        {
            "id": "202304bb-0b3a-41e3-9827-1063db561325",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8c0d0c65-ddb4-4e64-831a-34c03f387ad3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
    "visible": true
}