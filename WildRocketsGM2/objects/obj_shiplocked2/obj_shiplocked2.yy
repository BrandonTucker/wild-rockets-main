{
    "id": "8be623a7-d460-4025-8872-ea893c808d84",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked2",
    "eventList": [
        {
            "id": "b2d50e2b-b51f-4dac-8949-40c1d235a88c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8be623a7-d460-4025-8872-ea893c808d84"
        },
        {
            "id": "b88f0ab6-d8d9-478e-89be-2b7f5028bee2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8be623a7-d460-4025-8872-ea893c808d84"
        },
        {
            "id": "972bf1cb-92cf-42ea-b8a3-e1ccae9bdff0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8be623a7-d460-4025-8872-ea893c808d84"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1b3076f1-8590-4cf5-802a-e21d8cef52e6",
    "visible": true
}