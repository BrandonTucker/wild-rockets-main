{
    "id": "eb39ecb2-97e5-469c-8eb7-905456797bd5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_emerald",
    "eventList": [
        {
            "id": "9d313bbe-8a79-4b5c-a5da-758b5b847fbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eb39ecb2-97e5-469c-8eb7-905456797bd5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
    "visible": true
}