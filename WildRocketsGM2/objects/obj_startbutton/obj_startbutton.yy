{
    "id": "67990d87-1ecc-4ce8-9bcb-993cac49fa70",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_startbutton",
    "eventList": [
        {
            "id": "6d462807-4624-4374-a599-cf1e73dc2e14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "67990d87-1ecc-4ce8-9bcb-993cac49fa70"
        },
        {
            "id": "b93a5ccd-be1b-40b8-85c5-f41a958e5065",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "67990d87-1ecc-4ce8-9bcb-993cac49fa70"
        },
        {
            "id": "347a7115-7c61-4156-81f6-f8294f833ff1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "67990d87-1ecc-4ce8-9bcb-993cac49fa70"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0294c4de-1353-4639-be1d-7336257d3444",
    "visible": true
}