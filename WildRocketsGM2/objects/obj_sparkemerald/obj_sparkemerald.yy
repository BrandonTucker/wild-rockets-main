{
    "id": "3fc1c89e-ea4f-4e26-891d-4ebca4b014c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sparkemerald",
    "eventList": [
        {
            "id": "007e5540-a9ff-450f-be46-355e480808de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3fc1c89e-ea4f-4e26-891d-4ebca4b014c3"
        },
        {
            "id": "a6a3802e-c3d0-4fad-b78b-5ad8a2d71044",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "3fc1c89e-ea4f-4e26-891d-4ebca4b014c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5aca8933-e4fd-456c-b437-6f311388a154",
    "visible": true
}