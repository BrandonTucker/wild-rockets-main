{
    "id": "dc173227-3e46-4762-bfdc-f8989313fec5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange33button",
    "eventList": [
        {
            "id": "cb141983-1c9c-4136-9e35-51a06c1b87d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc173227-3e46-4762-bfdc-f8989313fec5"
        },
        {
            "id": "26011a3f-5821-450d-b1c5-a7549409fe21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc173227-3e46-4762-bfdc-f8989313fec5"
        },
        {
            "id": "25b11c90-b76a-463b-be6f-7dff79736669",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "dc173227-3e46-4762-bfdc-f8989313fec5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2febb205-97f9-444e-9236-51be3b155ed9",
    "visible": true
}