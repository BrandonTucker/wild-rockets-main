action_sound(snd_birds, 1);
action_end_sound(snd_computer);
global.ingame = 0;
script_execute(Scr_frame_rate,0);

if(global.lifeCount < 10){
	instance_create( __view_get( e__VW.XView, 0 )+510, __view_get( e__VW.YView, 0 )+28, obj_addlifebutton);
}
if(global.lifeCount > 9 && global.lifeCount < 100){
	
	instance_create( __view_get( e__VW.XView, 0 )+480, __view_get( e__VW.YView, 0 )+28, obj_addlifebutton);
}
if(global.lifeCount > 99 && global.lifeCount < 1000){
	
	instance_create( __view_get( e__VW.XView, 0 )+450, __view_get( e__VW.YView, 0 )+28, obj_addlifebutton);
}
if(global.lifeCount > 999 && global.lifeCount < 10000){
	instance_create( __view_get( e__VW.XView, 0 )+420, __view_get( e__VW.YView, 0 )+28, obj_addlifebutton);
}