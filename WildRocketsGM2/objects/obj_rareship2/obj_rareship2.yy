{
    "id": "cfa5c26f-a263-4ef6-9917-d56ae16de713",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rareship2",
    "eventList": [
        {
            "id": "5d3e9391-2997-415a-981f-0e4bc698be8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cfa5c26f-a263-4ef6-9917-d56ae16de713"
        },
        {
            "id": "e2b9f086-ae0d-40c0-9a77-6e4ef959e473",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cfa5c26f-a263-4ef6-9917-d56ae16de713"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d23bb1f-1edc-411b-96e6-de1f82a81d36",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
    "visible": true
}