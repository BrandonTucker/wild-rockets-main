{
    "id": "400829f1-0e3f-4047-b4a2-d07fe15b7507",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship16button",
    "eventList": [
        {
            "id": "f38ee71c-c2fe-4502-bf1e-efb8bde79aee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "400829f1-0e3f-4047-b4a2-d07fe15b7507"
        },
        {
            "id": "3bcb5853-403f-4d9c-8da5-71f6e7fee59f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "400829f1-0e3f-4047-b4a2-d07fe15b7507"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cb589006-0c61-4e2b-8986-d282c2e9e39f",
    "visible": true
}