{
    "id": "fc5b3041-079b-42da-9a2c-457dedc4048a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange19button",
    "eventList": [
        {
            "id": "23873fe9-0dd4-4287-85bc-7c0e471719c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fc5b3041-079b-42da-9a2c-457dedc4048a"
        },
        {
            "id": "8f6041c1-ed1b-40d8-b4ed-ccb380aef33f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fc5b3041-079b-42da-9a2c-457dedc4048a"
        },
        {
            "id": "9da2cdda-72a3-4cd7-9db5-2757b9ae6542",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "fc5b3041-079b-42da-9a2c-457dedc4048a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3ecd907b-5d0f-400c-9451-0014c8664325",
    "visible": true
}