{
    "id": "1fd3dfb1-4efe-45cd-9fc5-bd77be3a27be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blueexplosion1",
    "eventList": [
        {
            "id": "9d1af071-7350-4805-99d0-2afaaaebd4ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1fd3dfb1-4efe-45cd-9fc5-bd77be3a27be"
        },
        {
            "id": "900b8137-5f76-4003-a0d9-4a590857defd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "1fd3dfb1-4efe-45cd-9fc5-bd77be3a27be"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "647e8bd5-c119-41be-8a34-2a00c34a0bd6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "12e2390c-b1d3-4110-8c28-51d4b3044514",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fde030e1-8fdc-4b1c-90ef-495592f89bf3",
    "visible": true
}