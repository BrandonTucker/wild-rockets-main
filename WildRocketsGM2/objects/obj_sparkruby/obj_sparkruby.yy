{
    "id": "2e4035b1-2bab-4469-a781-d42654259a3e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sparkruby",
    "eventList": [
        {
            "id": "cf206867-facb-4548-816c-2f49b31951a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e4035b1-2bab-4469-a781-d42654259a3e"
        },
        {
            "id": "def16a35-cf65-40db-8aa3-f0d0f5a5e845",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "2e4035b1-2bab-4469-a781-d42654259a3e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f39edcec-d026-41cc-9ced-6f337c83100b",
    "visible": true
}