{
    "id": "b0d3562d-986c-428c-89d5-42d2c9d15b67",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_backgroundfade",
    "eventList": [
        {
            "id": "808157ab-6e65-4553-90a7-ccfef5a3e227",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0d3562d-986c-428c-89d5-42d2c9d15b67"
        },
        {
            "id": "2a65d2a6-777f-429d-b471-d0c01907e872",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b0d3562d-986c-428c-89d5-42d2c9d15b67"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "885940b9-737a-42e7-8745-614be2fb04d4",
    "visible": true
}