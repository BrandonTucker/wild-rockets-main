{
    "id": "43fa9638-f866-4e4f-80b2-75d56dce398f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship11button",
    "eventList": [
        {
            "id": "573c9fd7-d70d-4fbd-b116-b8ecb4a5a85e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43fa9638-f866-4e4f-80b2-75d56dce398f"
        },
        {
            "id": "2ff54846-142e-4d8f-893f-cf57051ebb22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "43fa9638-f866-4e4f-80b2-75d56dce398f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c20c443f-e6aa-4997-9e82-06d56d3c9881",
    "visible": true
}