{
    "id": "25c754a9-fc14-435d-973b-8c6845a31719",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_octopus",
    "eventList": [
        {
            "id": "3e764b5c-b717-46db-afc3-0083d599c25e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "25c754a9-fc14-435d-973b-8c6845a31719"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
    "visible": true
}