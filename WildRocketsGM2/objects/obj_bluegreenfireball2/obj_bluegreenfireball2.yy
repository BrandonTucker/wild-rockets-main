{
    "id": "26bcf3dd-1669-46df-9c98-f66a68d5391f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bluegreenfireball2",
    "eventList": [
        {
            "id": "2554f9f9-5dbb-4d92-8f8b-bb877ff1d9fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26bcf3dd-1669-46df-9c98-f66a68d5391f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6007a803-2d8f-453d-a159-6b68d8f00100",
    "visible": true
}