{
    "id": "14877d3d-da2c-40c5-859d-46fbfe5aaeb4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange1button",
    "eventList": [
        {
            "id": "19a118d3-db78-4c5b-99e1-a3d9e611d857",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "14877d3d-da2c-40c5-859d-46fbfe5aaeb4"
        },
        {
            "id": "412042d2-6543-4650-ab25-379ce27ac336",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "14877d3d-da2c-40c5-859d-46fbfe5aaeb4"
        },
        {
            "id": "780ab2b8-c497-4306-8e94-7a290fb81baa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "14877d3d-da2c-40c5-859d-46fbfe5aaeb4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "354a4ded-4bb2-44bf-8779-1fb90259a218",
    "visible": true
}