{
    "id": "ba32cde2-55cb-44d5-b885-12f66f796fab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_life",
    "eventList": [
        {
            "id": "8f54b6c9-ff3d-4a73-9108-7ba79372a266",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba32cde2-55cb-44d5-b885-12f66f796fab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3df27bd2-a74f-4f5d-b3a1-84c2a722a913",
    "visible": true
}