{
    "id": "71a46ada-080b-4f26-a68d-9b3074566725",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship15button",
    "eventList": [
        {
            "id": "b2be8681-6761-4a24-9924-371ce7c1b3e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "71a46ada-080b-4f26-a68d-9b3074566725"
        },
        {
            "id": "33e2755d-c8b0-4ef9-9a36-c83d452fe60d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "71a46ada-080b-4f26-a68d-9b3074566725"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e19f206a-de28-4612-ae5b-0bd40369d38b",
    "visible": true
}