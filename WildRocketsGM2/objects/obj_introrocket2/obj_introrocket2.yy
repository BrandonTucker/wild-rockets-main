{
    "id": "ec1632ed-b141-4912-a44b-371632a04e35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_introrocket2",
    "eventList": [
        {
            "id": "dfc8312e-1ead-412a-84b4-669068f2490a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec1632ed-b141-4912-a44b-371632a04e35"
        },
        {
            "id": "dd622329-ea18-4f4c-8677-8af2e78ac569",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ec1632ed-b141-4912-a44b-371632a04e35"
        },
        {
            "id": "95d14eb3-9c66-46d1-b861-718b25e34920",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b55e1166-db7c-4c00-a1f9-817cd9c75b01",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ec1632ed-b141-4912-a44b-371632a04e35"
        },
        {
            "id": "f1dc85f8-ee8c-4c3e-957e-a9fe2013af67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8f9d0760-0abd-4cb4-a468-82ae355aee12",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ec1632ed-b141-4912-a44b-371632a04e35"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
    "visible": true
}