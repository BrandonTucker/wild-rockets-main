{
    "id": "c0951b43-a59e-48dc-9a06-5973e10fe873",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_purplealien",
    "eventList": [
        {
            "id": "9ff95b57-f8da-4e87-b331-706f0c9dd850",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c0951b43-a59e-48dc-9a06-5973e10fe873"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
    "visible": true
}