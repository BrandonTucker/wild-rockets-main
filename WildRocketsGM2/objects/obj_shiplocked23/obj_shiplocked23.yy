{
    "id": "ae901645-2d65-40ad-9b06-1f9f92e42ce4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked23",
    "eventList": [
        {
            "id": "170e5a1e-5e6b-46db-b5f3-da3e3273efd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ae901645-2d65-40ad-9b06-1f9f92e42ce4"
        },
        {
            "id": "8bb80b88-2e3f-4625-9586-89a45bcd5c9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ae901645-2d65-40ad-9b06-1f9f92e42ce4"
        },
        {
            "id": "4cd648b4-125a-4934-adb0-03af99c0cd81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ae901645-2d65-40ad-9b06-1f9f92e42ce4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "96fdeedb-3dcd-4cd0-934c-4da390ba7770",
    "visible": true
}