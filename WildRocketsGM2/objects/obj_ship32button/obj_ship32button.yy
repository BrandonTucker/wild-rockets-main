{
    "id": "f7c9ab9a-9b8d-4991-bc34-65795ec3a69f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship32button",
    "eventList": [
        {
            "id": "7681b39e-d7e0-48e0-af85-55311c786e9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f7c9ab9a-9b8d-4991-bc34-65795ec3a69f"
        },
        {
            "id": "17fed61a-4393-4601-b8bb-921d48654592",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "f7c9ab9a-9b8d-4991-bc34-65795ec3a69f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0d194405-e561-4fb4-b220-c80f71569445",
    "visible": true
}