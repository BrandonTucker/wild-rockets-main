{
    "id": "f86c4ed2-dde3-4ee0-8397-38540a83b447",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_loadingintro",
    "eventList": [
        {
            "id": "a281fdea-9c09-4c21-97d2-c68c21fa3973",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f86c4ed2-dde3-4ee0-8397-38540a83b447"
        },
        {
            "id": "9a5c9db8-1fd1-4f63-b83e-c175ed117149",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f86c4ed2-dde3-4ee0-8397-38540a83b447"
        },
        {
            "id": "89e7f22c-54ab-4b34-a399-e8bbe36e7839",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f86c4ed2-dde3-4ee0-8397-38540a83b447"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}