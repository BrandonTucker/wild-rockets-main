{
    "id": "051795f0-a965-4215-8ec7-cff5f49bc3bc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sparkdiamond",
    "eventList": [
        {
            "id": "118dd4fd-afa1-4f1d-bfb8-e785d58c5b22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "051795f0-a965-4215-8ec7-cff5f49bc3bc"
        },
        {
            "id": "b5e78870-f699-4154-bdcb-f9b358edd32e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "051795f0-a965-4215-8ec7-cff5f49bc3bc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0c2f1def-3d3f-470b-886f-399052e9f954",
    "visible": true
}