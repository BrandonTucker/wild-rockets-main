{
    "id": "2930a242-0394-43f2-9207-26bc4b32172a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship25debri1",
    "eventList": [
        {
            "id": "f06eb0bb-1312-4125-b494-53503acc8fe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2930a242-0394-43f2-9207-26bc4b32172a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
    "visible": true
}