{
    "id": "f5ca84bc-52ee-458d-beb1-380611ace823",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lifebutton",
    "eventList": [
        {
            "id": "a5ef2fbe-14dd-46f7-9aa7-d58c3fc8a69b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f5ca84bc-52ee-458d-beb1-380611ace823"
        },
        {
            "id": "ca4a168b-4d36-437c-9142-77b0e2038d0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f5ca84bc-52ee-458d-beb1-380611ace823"
        },
        {
            "id": "30b32dd7-1c08-4858-80df-fcd63141c376",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f5ca84bc-52ee-458d-beb1-380611ace823"
        },
        {
            "id": "f06a1e12-5a08-4f2c-9576-ba8eaa177c23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "f5ca84bc-52ee-458d-beb1-380611ace823"
        },
        {
            "id": "8481cb26-e84c-4351-8771-ffd3d0bef36d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f5ca84bc-52ee-458d-beb1-380611ace823"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
    "visible": true
}