
if(global.lifeCount > 0){
	global.lifeCount--;
	ini_open(working_directory + "\\livesIni.ini");
	ini_write_real("Lives", "lives", global.lifeCount);
	ini_close();
	instance_create(view_xview + view_wview * 0.5,global.lifeY+20,obj_clearLeft);
	instance_create(view_xview + view_wview * 0.5,global.lifeY+20,obj_clearRight);
	instance_create(view_xview + view_wview * 0.5,global.lifeY+20,obj_minusLife);

	
	global.useLife = true;
	global.tempCoins = global.coins;

	instance_destroy();
	
}
else{
	instance_create(x,y,obj_notEnoughLivesMessage);
}


with(obj_ghostblock){
instance_change(obj_block,true);	
instance_create(x, y, obj_spark);
}
