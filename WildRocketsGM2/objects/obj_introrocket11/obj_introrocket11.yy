{
    "id": "9b8a0059-26aa-4d7e-bddf-8baf9be4c42c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_introrocket11",
    "eventList": [
        {
            "id": "3d7d00e0-320d-4f8a-86e8-227614ca7fda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9b8a0059-26aa-4d7e-bddf-8baf9be4c42c"
        },
        {
            "id": "b517809b-ab4c-4d9e-84b9-54d384c89b45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9b8a0059-26aa-4d7e-bddf-8baf9be4c42c"
        },
        {
            "id": "59782112-6e7f-4b51-a2c6-5fed193c3b59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "9b8a0059-26aa-4d7e-bddf-8baf9be4c42c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "61bff5f0-43c0-4226-927c-ab7a47f473c3",
    "visible": true
}