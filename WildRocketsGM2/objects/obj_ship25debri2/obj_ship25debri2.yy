{
    "id": "722230ec-f8a6-48c9-8de6-0ad01ef0c9cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship25debri2",
    "eventList": [
        {
            "id": "126a8431-55d3-4f79-a882-666d52ea61a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "722230ec-f8a6-48c9-8de6-0ad01ef0c9cc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
    "visible": true
}