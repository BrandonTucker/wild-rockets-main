{
    "id": "7b0462ac-dffd-4db7-8479-925ee703dbec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked13",
    "eventList": [
        {
            "id": "6f86ffe2-5eb6-4424-9883-02403002aca7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7b0462ac-dffd-4db7-8479-925ee703dbec"
        },
        {
            "id": "04084ecf-fda8-4976-8bf9-4c5aeb952db0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7b0462ac-dffd-4db7-8479-925ee703dbec"
        },
        {
            "id": "d947b7c9-55ba-4f13-a020-e83f6bb0bd7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7b0462ac-dffd-4db7-8479-925ee703dbec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d4f1dab-ce61-4e75-83ff-b408f134b1b1",
    "visible": true
}