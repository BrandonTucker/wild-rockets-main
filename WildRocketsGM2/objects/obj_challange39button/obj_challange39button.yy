{
    "id": "46961ada-ab58-4cb8-bcc9-1c338fcca8f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange39button",
    "eventList": [
        {
            "id": "7108f01d-b2d6-4311-9f33-ce474e92c7e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46961ada-ab58-4cb8-bcc9-1c338fcca8f1"
        },
        {
            "id": "c4d55fd8-3818-493c-bd3b-59f4f8af3652",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "46961ada-ab58-4cb8-bcc9-1c338fcca8f1"
        },
        {
            "id": "69e3c729-f53e-467f-9381-e30896cf02f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "46961ada-ab58-4cb8-bcc9-1c338fcca8f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5dcd1170-fb01-437e-9727-dcf384d05d76",
    "visible": true
}