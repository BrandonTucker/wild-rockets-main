{
    "id": "11385734-d8a5-48c4-a012-3b0ed6f2361f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_introrocket4",
    "eventList": [
        {
            "id": "c18f1ec6-b5b8-40a7-97ee-3f842272322e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11385734-d8a5-48c4-a012-3b0ed6f2361f"
        },
        {
            "id": "41c5d42a-b5ca-46b0-9d80-d4364f0be4d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "11385734-d8a5-48c4-a012-3b0ed6f2361f"
        },
        {
            "id": "1d61375f-d8eb-4075-83c9-1cc584240525",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "11385734-d8a5-48c4-a012-3b0ed6f2361f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
    "visible": true
}