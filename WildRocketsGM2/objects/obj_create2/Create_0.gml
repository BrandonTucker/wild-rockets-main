randomize();
var purplblock = false;
var center = choose(8,9,10,11,12);
var gap = choose(7,8,9);


if(global.count < 34){
    gap = choose(8,9,10,11);
    
    center = choose(7,8,9,10,11,12,13);
   
}
if(global.count > 33){
    gap = choose(6,7,8,9);
    center = choose(7,8,9,10,11,12,13);
}
if(global.count > 66){
    gap = choose(5,6,7,8,9);
    center = choose(6,7,8,9,10,11,12,13,14);
}
if(global.count > 99){
    gap = choose(5,5,5,6,6,7,7,8,9);
    center = choose(5,6,7,8,9,10,11,12,13,14,15);
}
if(global.count > 132){
    gap = choose(4,4,4,5,5,5,6,6,7,8,9);
    center = choose(4,5,6,7,8,9,10,11,12,13,14,15,16);
}
if(global.count > 165){
    gap = choose(4,4,4,5,5,6,6,7,8,9);
    center = choose(3,4,5,6,7,8,9,10,11,12,13,14,15,16,17);
}
if(global.count > 198){
    gap = choose(3,4,4,4,4,5,5,5,6,6,7,8,9);
    center = choose(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18);
}

instance_create_depth((center*32)-((gap*32)/2),y,-1,obj_block3);
instance_create_depth((center*32)+((gap*32)/2),y,-1,obj_block2);


if(random(10)==1){
	var xpos = 32;
	var blockx = (center*32)-((gap*32)/2);
	var purplblock = true;
	for(i = 1; i < gap; i++){
	    instance_create(blockx+xpos,y,obj_blockdestructable2);
	    xpos+=32;
	}
}


var chance = irandom(10000);

if(purplblock == false){
    if(chance > 9980) instance_create(center*32,y,obj_diamond);
    if(chance > 9890 && chance < 9980) instance_create(center*32,y,obj_ruby);
    if(chance > 9750 && chance < 9890) instance_create(center*32,y,obj_emerald);
    if(chance > 9500 && chance < 9750) instance_create(center*32,y,obj_bananas);
    if(chance > 9250 && chance < 9500) instance_create(center*32,y,obj_pineapple);
    if(chance > 9000 && chance < 9250) instance_create(center*32,y,obj_grapes);
    if(chance > 7000 && chance < 9000) instance_create(center*32,y,obj_coinstring);
    if(chance > 6500 && chance < 7000) {
        instance_create(center*32+20,y,obj_coinstring);
        instance_create(center*32-20,y,obj_coinstring);
    }
    if(chance > 6250 && chance < 6500){
        instance_create(64,y+96,obj_coin);
        instance_create(128,y+96,obj_coin);
        instance_create(196,y+96,obj_coin);
        instance_create(256,y+96,obj_coin);
        instance_create(320,y+96,obj_coin);
        instance_create(384,y+96,obj_coin);
        instance_create(448,y+96,obj_coin);
        instance_create(512,y+96,obj_coin);
        instance_create(576,y+96,obj_coin);
    } 
	if(chance > 0 && chance < 10){
		instance_create(center*32,y,obj_life);
	}
    
}

