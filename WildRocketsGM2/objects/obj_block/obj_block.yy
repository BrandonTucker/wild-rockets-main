{
    "id": "e58f85a3-957f-493b-bbd0-f7b212ae3a64",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_block",
    "eventList": [
        {
            "id": "cd4008c3-6809-460c-8327-f4517143e76e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e58f85a3-957f-493b-bbd0-f7b212ae3a64"
        },
        {
            "id": "bff44d80-94d0-4622-a3bb-1c9b4063d54e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e58f85a3-957f-493b-bbd0-f7b212ae3a64"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6aeec2d8-c23b-4e28-a5ae-22a2814c4e79",
    "visible": true
}