{
    "id": "14aa1955-235e-4acd-8b53-8f17989142ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange28button",
    "eventList": [
        {
            "id": "830471db-727b-47cb-80bb-4c25359f56fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "14aa1955-235e-4acd-8b53-8f17989142ec"
        },
        {
            "id": "23507624-a9f4-4877-8357-51c371dec152",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "14aa1955-235e-4acd-8b53-8f17989142ec"
        },
        {
            "id": "46692f4c-587a-4e9a-b992-0f85a23c0742",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "14aa1955-235e-4acd-8b53-8f17989142ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6ced8832-de65-458f-b099-93dae60a627d",
    "visible": true
}