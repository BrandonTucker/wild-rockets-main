{
    "id": "ebbaa8da-3afc-4ba1-9397-a34217d64e36",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked16",
    "eventList": [
        {
            "id": "f969ef91-2d23-45b0-866e-38a6243e7f13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ebbaa8da-3afc-4ba1-9397-a34217d64e36"
        },
        {
            "id": "71a11492-515e-481a-86a5-df9973d0af15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ebbaa8da-3afc-4ba1-9397-a34217d64e36"
        },
        {
            "id": "20179d46-36e9-46ca-b02c-3efffbbc3742",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ebbaa8da-3afc-4ba1-9397-a34217d64e36"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7c35020-5903-45b8-bf07-18d381b0724a",
    "visible": true
}