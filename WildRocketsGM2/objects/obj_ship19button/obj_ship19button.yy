{
    "id": "3c7e8cce-74f4-48f8-86b3-8107624b8cb4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship19button",
    "eventList": [
        {
            "id": "2ddb6fa0-657e-404d-a345-a1d5c4c0e7e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3c7e8cce-74f4-48f8-86b3-8107624b8cb4"
        },
        {
            "id": "0e8a3b73-bfe1-45c9-9616-a56bbec8bffb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3c7e8cce-74f4-48f8-86b3-8107624b8cb4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "19693492-cd41-4a0f-a653-8c4753951dc5",
    "visible": true
}