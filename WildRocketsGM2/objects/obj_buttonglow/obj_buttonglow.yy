{
    "id": "768193f9-79d5-4906-afb9-ef4a1a238323",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_buttonglow",
    "eventList": [
        {
            "id": "19565a51-a721-4696-9edd-4761ce0a9ccf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "768193f9-79d5-4906-afb9-ef4a1a238323"
        },
        {
            "id": "5d3aa7c7-abee-487f-86e5-f508af16761f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "768193f9-79d5-4906-afb9-ef4a1a238323"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f66b6c3-303a-4139-9858-dfe2434a1aa4",
    "visible": true
}