{
    "id": "b145972c-ab4e-46c5-83d9-8f03186b48aa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rareship9",
    "eventList": [
        {
            "id": "635d0ea7-52f1-4cbb-9a5e-dd866c87be50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b145972c-ab4e-46c5-83d9-8f03186b48aa"
        },
        {
            "id": "7269b8ed-68e2-4808-b439-72e6b52c7212",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e5419aa-bd22-4671-a95a-a960a11745cf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b145972c-ab4e-46c5-83d9-8f03186b48aa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0d23bb1f-1edc-411b-96e6-de1f82a81d36",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
    "visible": true
}