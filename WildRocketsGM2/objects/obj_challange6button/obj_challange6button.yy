{
    "id": "7e72d3df-d989-49b5-a8d1-4feb768dd089",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange6button",
    "eventList": [
        {
            "id": "8c0e5209-8e27-46e5-82ff-1e0762267bea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7e72d3df-d989-49b5-a8d1-4feb768dd089"
        },
        {
            "id": "ec6e8ad0-080b-4270-8da4-97366cfaf1f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7e72d3df-d989-49b5-a8d1-4feb768dd089"
        },
        {
            "id": "b452f609-aeb8-4e09-95fb-58821d4b5503",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7e72d3df-d989-49b5-a8d1-4feb768dd089"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "35e27fdc-4594-41a2-9cfb-1c2d56c2f790",
    "visible": true
}