{
    "id": "545a1daa-3b69-49ca-86d4-d82760eb57a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_treasure4",
    "eventList": [
        {
            "id": "23b30dc4-fb24-416c-bef7-3c883d6472d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "545a1daa-3b69-49ca-86d4-d82760eb57a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
    "visible": true
}