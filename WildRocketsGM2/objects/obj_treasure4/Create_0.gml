scale = random_range(.5,.75);
action_set_motion(random_range(0,360), choose(.2,.4,.6,.8,1));
action_sprite_transform(scale, scale, 0, 0);
action_sprite_set(spr_diamond, 0, random_range(-1,1));
