{
    "id": "8697dd09-82c4-4867-9e2d-1e8224f638fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange38button",
    "eventList": [
        {
            "id": "0043074b-c911-4fea-a3cc-b4846d0153d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8697dd09-82c4-4867-9e2d-1e8224f638fd"
        },
        {
            "id": "7f16ae5e-8109-4435-9322-87484a3ec892",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8697dd09-82c4-4867-9e2d-1e8224f638fd"
        },
        {
            "id": "11bfc422-b4bd-4b29-ba43-ef905f8fd841",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8697dd09-82c4-4867-9e2d-1e8224f638fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b0620c59-93e9-4c81-aeac-d15fa3c2e459",
    "visible": true
}