{
    "id": "57d12489-791c-427d-89e5-3d19c273f31c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_snowflake2",
    "eventList": [
        {
            "id": "212a4410-f7e7-4a3b-b5ec-7a538449e328",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "57d12489-791c-427d-89e5-3d19c273f31c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
    "visible": true
}