{
    "id": "6f3a2c8e-8b18-4380-8ac0-189e6ef4d2c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange35locked",
    "eventList": [
        {
            "id": "a816578a-6076-4d11-9204-7c941cbf87fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6f3a2c8e-8b18-4380-8ac0-189e6ef4d2c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f1401ffc-b4d8-444a-b5a7-533581d15076",
    "visible": true
}