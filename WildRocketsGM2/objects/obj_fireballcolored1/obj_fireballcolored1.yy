{
    "id": "bfda423c-412e-415a-9a2d-5134e7052201",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireballcolored1",
    "eventList": [
        {
            "id": "94786ce9-2b59-4c5d-9a53-7f0d779b3f2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bfda423c-412e-415a-9a2d-5134e7052201"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f224853e-e368-46ea-a33d-810489abc67c",
    "visible": true
}