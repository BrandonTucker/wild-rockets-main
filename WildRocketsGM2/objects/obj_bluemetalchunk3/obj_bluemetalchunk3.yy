{
    "id": "bd5c6ef6-f5d8-4a22-ad47-bb3df0f2b7de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bluemetalchunk3",
    "eventList": [
        {
            "id": "77ca016e-c901-4c37-aae9-d4c741b449b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd5c6ef6-f5d8-4a22-ad47-bb3df0f2b7de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
    "visible": true
}