{
    "id": "c9257568-37b5-45d6-bdfb-e737774776ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange11button",
    "eventList": [
        {
            "id": "228f6ba3-fc3d-4aa3-9db6-3bc4adf975e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9257568-37b5-45d6-bdfb-e737774776ef"
        },
        {
            "id": "249265c8-c18a-42b6-88ba-03d21c330b68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c9257568-37b5-45d6-bdfb-e737774776ef"
        },
        {
            "id": "96af4745-4ce8-49ea-b780-5858f6583d04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c9257568-37b5-45d6-bdfb-e737774776ef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "11afebba-d734-44ff-8d14-220fa2d57016",
    "visible": true
}