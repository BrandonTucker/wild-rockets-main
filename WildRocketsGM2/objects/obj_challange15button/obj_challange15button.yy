{
    "id": "594c4adf-519a-40e2-9d07-139edfcc541e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange15button",
    "eventList": [
        {
            "id": "37d5c696-3b75-4be9-9e3c-9aab1f2acb99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "594c4adf-519a-40e2-9d07-139edfcc541e"
        },
        {
            "id": "f103ac72-78bb-492c-9424-6adaab114509",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "594c4adf-519a-40e2-9d07-139edfcc541e"
        },
        {
            "id": "e5937ebc-8e76-4c98-ba55-f58a2643bbd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "594c4adf-519a-40e2-9d07-139edfcc541e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "482f9621-40e7-4c62-b9b0-f027f46e7c41",
    "visible": true
}