{
    "id": "bc5f8501-9c9b-48e6-80da-b84add70b719",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_changeshipbutton",
    "eventList": [
        {
            "id": "6ad2800f-766c-40c3-89c9-440184a755c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc5f8501-9c9b-48e6-80da-b84add70b719"
        },
        {
            "id": "58ccc3b1-c920-4abf-803c-3d61f3d386d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "bc5f8501-9c9b-48e6-80da-b84add70b719"
        },
        {
            "id": "c9a264b2-5a5b-4747-bcc4-e3cc1c00ce0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bc5f8501-9c9b-48e6-80da-b84add70b719"
        },
        {
            "id": "28f1983f-39e4-443e-b4eb-039c178e4173",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "bc5f8501-9c9b-48e6-80da-b84add70b719"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "visible": true
}