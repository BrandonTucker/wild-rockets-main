{
    "id": "80486614-2e2b-42f7-bf0c-6c35b9fe3fc9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange9locked",
    "eventList": [
        {
            "id": "fa2ede29-844d-4719-9f79-5ef57a3b144c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "80486614-2e2b-42f7-bf0c-6c35b9fe3fc9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f1401ffc-b4d8-444a-b5a7-533581d15076",
    "visible": true
}