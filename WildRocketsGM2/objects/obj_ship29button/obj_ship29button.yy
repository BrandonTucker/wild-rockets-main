{
    "id": "4ca1fbb5-0125-4987-b862-6e829c4a1007",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship29button",
    "eventList": [
        {
            "id": "fe691031-3824-4393-943d-2ec66cd80da9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4ca1fbb5-0125-4987-b862-6e829c4a1007"
        },
        {
            "id": "949d2f8b-c17c-4f60-8a54-abb4c9555cc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4ca1fbb5-0125-4987-b862-6e829c4a1007"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f4821cc2-8c67-40b3-b4c6-d88a94bd2f21",
    "visible": true
}