{
    "id": "234bc3e4-0488-41bb-b80a-f2ddd7b83f19",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_deadastronaut",
    "eventList": [
        {
            "id": "27bd66df-81e1-4dee-a73a-922b04df40f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "234bc3e4-0488-41bb-b80a-f2ddd7b83f19"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
    "visible": true
}