{
    "id": "53c0f192-07cd-47a3-921d-f13548eefe01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flyingbanana",
    "eventList": [
        {
            "id": "cccc8eb7-86ca-4862-ba52-8a71c4e39e2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53c0f192-07cd-47a3-921d-f13548eefe01"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
    "visible": true
}