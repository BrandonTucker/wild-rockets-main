{
    "id": "a197a135-d0c3-4449-805a-46907356c8ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cloud3",
    "eventList": [
        {
            "id": "7052ee6c-ece8-44c4-b7df-b3d133d4c556",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a197a135-d0c3-4449-805a-46907356c8ba"
        },
        {
            "id": "7f7ad750-6d56-4636-a2a6-898ee32ccaa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a197a135-d0c3-4449-805a-46907356c8ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "746f0ca6-62b0-43e3-af8c-80762a087846",
    "visible": true
}