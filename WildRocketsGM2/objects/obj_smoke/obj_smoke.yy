{
    "id": "c058e2cd-9292-416a-80c0-a8cc2c3dc6d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_smoke",
    "eventList": [
        {
            "id": "ccb5003e-4945-4bb3-b68c-334e3a8cc3f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c058e2cd-9292-416a-80c0-a8cc2c3dc6d1"
        },
        {
            "id": "47092be1-3f5e-4151-9cda-fd7bcd703f9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c058e2cd-9292-416a-80c0-a8cc2c3dc6d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef786960-5354-4640-8ff8-1a99e26c74ac",
    "visible": true
}