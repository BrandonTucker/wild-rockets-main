{
    "id": "6fff427a-9fa2-427b-a611-2dcf212643bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_changepagechallange2",
    "eventList": [
        {
            "id": "a1413946-e853-4d4a-87dc-5e9bb796f216",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6fff427a-9fa2-427b-a611-2dcf212643bd"
        },
        {
            "id": "9bd72092-0929-4439-9d8c-9b6ddbdb8a28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "6fff427a-9fa2-427b-a611-2dcf212643bd"
        },
        {
            "id": "4a47f18b-9083-4439-a854-b49a18b6a75f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "6fff427a-9fa2-427b-a611-2dcf212643bd"
        },
        {
            "id": "be685a59-bac5-4c52-ba7a-8fceb7cdf465",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6fff427a-9fa2-427b-a611-2dcf212643bd"
        },
        {
            "id": "0188b012-59cb-4c0c-bbd5-661cce808b35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6fff427a-9fa2-427b-a611-2dcf212643bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a132351-f771-425b-9ba7-169939928823",
    "visible": true
}