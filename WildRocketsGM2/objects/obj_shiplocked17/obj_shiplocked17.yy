{
    "id": "16801368-21c7-4af3-a451-cee9bf0f922e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked17",
    "eventList": [
        {
            "id": "3667531e-7469-4c51-b8ab-ea9b0378ac17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "16801368-21c7-4af3-a451-cee9bf0f922e"
        },
        {
            "id": "670222c8-abc7-4c89-a884-216ef89a9874",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "16801368-21c7-4af3-a451-cee9bf0f922e"
        },
        {
            "id": "5a470ab7-7293-4891-85e9-023be6bb38d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "16801368-21c7-4af3-a451-cee9bf0f922e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7e01f260-7eab-495f-a0da-a95e7f564650",
    "visible": true
}