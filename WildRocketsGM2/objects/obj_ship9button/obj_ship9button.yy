{
    "id": "56299df5-5f9e-4f9e-9094-6ffc41454da6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship9button",
    "eventList": [
        {
            "id": "d8fd410e-6322-459c-ba16-59494550f443",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "56299df5-5f9e-4f9e-9094-6ffc41454da6"
        },
        {
            "id": "03eb8b85-5722-487a-a1e8-60eb1560612a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "56299df5-5f9e-4f9e-9094-6ffc41454da6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "773ae2ec-391e-4a4b-8e5e-7b09052e0f98",
    "visible": true
}