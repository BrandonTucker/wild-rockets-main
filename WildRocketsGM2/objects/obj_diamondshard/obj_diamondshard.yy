{
    "id": "1ad45f24-5012-4e36-9997-78cd6b86e88c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_diamondshard",
    "eventList": [
        {
            "id": "f2476087-a3ba-4666-b5fb-525b6b657919",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ad45f24-5012-4e36-9997-78cd6b86e88c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
    "visible": true
}