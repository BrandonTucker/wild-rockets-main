{
    "id": "7f39bc1a-89c8-4f4c-99f1-1983a98bd312",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship6button",
    "eventList": [
        {
            "id": "2ac9acf3-f30f-4ffa-aa5c-62b69f529f60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f39bc1a-89c8-4f4c-99f1-1983a98bd312"
        },
        {
            "id": "a9bd7c4b-2c3f-4937-aa57-984c03bee78b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7f39bc1a-89c8-4f4c-99f1-1983a98bd312"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d1f64c38-cdd0-460c-94cc-496f95a24cd2",
    "visible": true
}