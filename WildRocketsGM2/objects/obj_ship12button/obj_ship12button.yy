{
    "id": "1bea7165-c3b7-4cd5-9801-af625047f4d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship12button",
    "eventList": [
        {
            "id": "3e849091-4676-4dc6-ac11-6de24b52b719",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1bea7165-c3b7-4cd5-9801-af625047f4d4"
        },
        {
            "id": "ae6ad425-1f4b-4f78-bb54-e5fbdea9b0fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "1bea7165-c3b7-4cd5-9801-af625047f4d4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a502464-c915-4218-a2a4-2d6862f9a4c7",
    "visible": true
}