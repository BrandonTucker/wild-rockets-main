{
    "id": "bc3723f9-c7b7-4749-8f88-7c5698a615c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bluemetalchunk",
    "eventList": [
        {
            "id": "083ccad2-493c-45da-bb92-baad8634fb9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc3723f9-c7b7-4749-8f88-7c5698a615c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
    "visible": true
}