{
    "id": "3735aea5-1065-4af5-af0c-e1fa7db4e868",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship26debri2",
    "eventList": [
        {
            "id": "813e9ff6-f21e-49b2-a12a-5e3d3987c29e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3735aea5-1065-4af5-af0c-e1fa7db4e868"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d859ace7-b20b-467c-b759-5730db396f98",
    "visible": true
}