{
    "id": "50098136-ad8f-4dc3-97a5-808e2084c34f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange18button",
    "eventList": [
        {
            "id": "4225cfe9-89e0-49a4-ad86-a70e5d14b41a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "50098136-ad8f-4dc3-97a5-808e2084c34f"
        },
        {
            "id": "63f3d5d8-d5a5-4d7b-86c7-bec1129bcda2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "50098136-ad8f-4dc3-97a5-808e2084c34f"
        },
        {
            "id": "11592b69-bc10-4158-a3af-c9d8430dc03f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "50098136-ad8f-4dc3-97a5-808e2084c34f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75729b4d-5fae-45bd-a775-eea8adf050cb",
    "visible": true
}