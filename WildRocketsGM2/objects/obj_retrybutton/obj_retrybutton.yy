{
    "id": "e68ff25b-c54e-41e0-9833-bbf5af8f7e1d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_retrybutton",
    "eventList": [
        {
            "id": "2ff4bbc9-abcb-493e-a7ea-151184b5d2be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e68ff25b-c54e-41e0-9833-bbf5af8f7e1d"
        },
        {
            "id": "71d4c6a0-230f-4e65-aae0-45dde33582d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "e68ff25b-c54e-41e0-9833-bbf5af8f7e1d"
        },
        {
            "id": "a0098005-2f7f-4099-846f-ba3377cfc3ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e68ff25b-c54e-41e0-9833-bbf5af8f7e1d"
        },
        {
            "id": "66667eff-63ab-4c03-aa9f-1a3a82c5dc12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e68ff25b-c54e-41e0-9833-bbf5af8f7e1d"
        },
        {
            "id": "02f84a47-7828-4001-a5f3-56e00a8de214",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e68ff25b-c54e-41e0-9833-bbf5af8f7e1d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
    "visible": true
}