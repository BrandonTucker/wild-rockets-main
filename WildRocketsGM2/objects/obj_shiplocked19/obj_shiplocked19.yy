{
    "id": "29b8f9f6-d8b2-4a30-8f1a-3cfa54bc38aa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked19",
    "eventList": [
        {
            "id": "33aaec60-6fea-494f-8def-de6e1a59f403",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29b8f9f6-d8b2-4a30-8f1a-3cfa54bc38aa"
        },
        {
            "id": "e478ff1b-90f0-44af-ac88-3dde15a8d20c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29b8f9f6-d8b2-4a30-8f1a-3cfa54bc38aa"
        },
        {
            "id": "50f3d1d9-1402-42ef-a4a2-26277260d176",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "29b8f9f6-d8b2-4a30-8f1a-3cfa54bc38aa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dd66ba7f-a222-4ccc-b45a-2f08b04bb514",
    "visible": true
}