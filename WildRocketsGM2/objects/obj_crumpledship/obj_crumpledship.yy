{
    "id": "4f22db73-279b-4bd0-ae99-77d55ac7cac4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crumpledship",
    "eventList": [
        {
            "id": "7521166b-7420-4085-9e85-27e3a39ec57e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4f22db73-279b-4bd0-ae99-77d55ac7cac4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ab21622-4f93-4497-80f6-0a5408289433",
    "visible": true
}