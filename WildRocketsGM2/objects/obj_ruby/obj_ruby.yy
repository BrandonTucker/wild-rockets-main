{
    "id": "98d11913-a7aa-4d47-a205-5de960642e3d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ruby",
    "eventList": [
        {
            "id": "2e67ed66-8a5a-42e1-863a-8bbf3d7d22b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "98d11913-a7aa-4d47-a205-5de960642e3d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b3792838-14da-4265-82d4-35c67160ab93",
    "visible": true
}