{
    "id": "4a3c5b96-01f6-4338-a108-3fa97518e9ae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked20",
    "eventList": [
        {
            "id": "6276d191-fbb0-4666-84a2-25e469bcefb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a3c5b96-01f6-4338-a108-3fa97518e9ae"
        },
        {
            "id": "47e24d10-83ff-4d3e-afe7-fcf619e2e3ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4a3c5b96-01f6-4338-a108-3fa97518e9ae"
        },
        {
            "id": "32e68e40-a021-460c-b5b7-af41e1f03e42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4a3c5b96-01f6-4338-a108-3fa97518e9ae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "21b131d9-25b9-4634-9ebb-282825dcd604",
    "visible": true
}