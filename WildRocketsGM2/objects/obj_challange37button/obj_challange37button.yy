{
    "id": "44226ceb-1a91-46f7-ab1b-d5c2136f3d1d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange37button",
    "eventList": [
        {
            "id": "ba4f99b6-5a07-45eb-a1ff-050561ecbb8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "44226ceb-1a91-46f7-ab1b-d5c2136f3d1d"
        },
        {
            "id": "e4c03168-19ac-40a8-85a1-85988b7e3520",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "44226ceb-1a91-46f7-ab1b-d5c2136f3d1d"
        },
        {
            "id": "a4932e68-fb64-46bc-9ef2-4e5a1fa51b0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "44226ceb-1a91-46f7-ab1b-d5c2136f3d1d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7005f84d-79a3-43a8-b6d9-42bb8b5b5014",
    "visible": true
}