scale = random_range(.5,1.5);
action_set_motion(random_range(0,360), choose(1,2,3,4,5));
action_sprite_transform(scale, scale, 0, 0);
action_sprite_set(spr_borkenwing2, 0, random_range(-1,1));
