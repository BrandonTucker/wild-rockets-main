{
    "id": "08dfab8e-2f5b-4404-b116-527ad32403fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dollarsign",
    "eventList": [
        {
            "id": "b958f98f-c04a-4423-b94a-97a334f6381f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08dfab8e-2f5b-4404-b116-527ad32403fc"
        },
        {
            "id": "560eeb7d-5452-4b3b-a2b7-363c7d6041a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "08dfab8e-2f5b-4404-b116-527ad32403fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
    "visible": true
}