{
    "id": "ce0c3164-6523-46c7-9858-05f1599df9a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_create",
    "eventList": [
        {
            "id": "72ab5d66-99c0-40ab-9753-44315b543fb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce0c3164-6523-46c7-9858-05f1599df9a0"
        },
        {
            "id": "637918d4-f1c2-4152-88a4-720d546fe668",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ce0c3164-6523-46c7-9858-05f1599df9a0"
        },
        {
            "id": "25911ab4-102c-4073-8364-d824b217f6e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ce0c3164-6523-46c7-9858-05f1599df9a0"
        },
        {
            "id": "d5fbcb5e-1908-4c53-8b87-fd59ebc22254",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e58f85a3-957f-493b-bbd0-f7b212ae3a64",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ce0c3164-6523-46c7-9858-05f1599df9a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6aeec2d8-c23b-4e28-a5ae-22a2814c4e79",
    "visible": true
}