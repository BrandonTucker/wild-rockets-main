action_set_relative(1);
var __b__;
__b__ = action_if_variable(global.count, 231, 2);
if __b__
{
{
action_kill_position(kill, 0);
__b__ = action_if_dice(2);
if __b__
{
action_kill_position(kill-32, 0);
}
__b__ = action_if_dice(2);
if __b__
{
action_kill_position(kill+32, 0);
}
}
}
else
{
{
__b__ = action_if_variable(global.count, 198, 2);
if __b__
{
{
action_kill_position(kill, 0);
action_kill_position(kill-32, 0);
action_kill_position(kill+32, 0);
}
}
else
{
{
__b__ = action_if_variable(global.count, 165, 2);
if __b__
{
{
action_kill_position(kill, 0);
action_kill_position(kill-32, 0);
action_kill_position(kill+32, 0);
}
}
else
{
{
__b__ = action_if_variable(global.count, 132, 2);
if __b__
{
{
action_kill_position(kill, 0);
action_kill_position(kill-32, 0);
action_kill_position(kill+32, 0);
action_kill_position(kill-64, 0);
}
}
else
{
{
__b__ = action_if_variable(global.count, 99, 2);
if __b__
{
{
action_kill_position(kill, 0);
action_kill_position(kill-32, 0);
action_kill_position(kill+32, 0);
action_kill_position(kill-64, 0);
action_kill_position(kill+64, 0);
}
}
else
{
{
__b__ = action_if_variable(global.count, 66, 2);
if __b__
{
{
action_kill_position(kill, 0);
action_kill_position(kill-32, 0);
action_kill_position(kill+32, 0);
action_kill_position(kill-64, 0);
action_kill_position(kill+64, 0);
}
}
else
{
{
__b__ = action_if_variable(global.count, 33, 2);
if __b__
{
{
action_kill_position(kill, 0);
action_kill_position(kill-32, 0);
action_kill_position(kill+32, 0);
action_kill_position(kill-64, 0);
action_kill_position(kill+64, 0);
action_kill_position(kill-96, 0);
action_kill_position(kill+96, 0);
}
}
else
{
{
action_kill_position(kill, 0);
action_kill_position(kill-32, 0);
action_kill_position(kill+32, 0);
action_kill_position(kill-64, 0);
action_kill_position(kill+64, 0);
action_kill_position(kill-96, 0);
action_kill_position(kill+96, 0);
action_kill_position(kill+128, 0);
action_kill_position(kill-128, 0);
}
}
}
}
}
}
}
}
}
}
}
}
}
}
__b__ = action_if_variable(global.count, 33, 2);
if __b__
{
{
__b__ = action_if_dice(50);
if __b__
{
{
action_create_object(obj_blockdestructable, kill-32, 0);
action_create_object(obj_blockdestructable, kill, 0);
action_create_object(obj_blockdestructable, kill+32, 0);
action_create_object(obj_blockdestructable, kill+64, 0);
action_create_object(obj_blockdestructable, kill-64, 0);
action_create_object(obj_blockdestructable, kill+96, 0);
action_create_object(obj_blockdestructable, kill-96, 0);
}
}
}
}
__b__ = action_if_dice(30);
if __b__
{
{
with (other) {
action_kill_position(kill, 0);
}
var chance = choose(0,1,2);

if(chance == 0)
    instance_create(x+kill,y,obj_pineapple);
if(chance == 1)
    instance_create(x+kill,y,obj_grapes);
if(chance == 2)
    instance_create(x+kill,y,obj_bananas);

}
}
__b__ = action_if_dice(180);
if __b__
{
{
with (other) {
action_kill_position(kill, 0);
}
var chance2 = choose(0,1);

if(chance2 == 0)
    instance_create(x+kill,y,obj_ruby);
else
    instance_create(x+kill,y,obj_emerald);


}
}
__b__ = action_if_dice(500);
if __b__
{
{
with (other) {
action_kill_position(kill, 0);
}
action_create_object(obj_diamond, kill, 0);
}
}
action_set_relative(0);
