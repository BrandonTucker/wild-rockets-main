scale = random_range(.5,.75);
action_set_motion(random_range(0,360), choose(.5,.75,1,2,3));
action_sprite_transform(scale, scale, 0, 0);
action_sprite_set(spr_coin, 0, random_range(-1,1));
