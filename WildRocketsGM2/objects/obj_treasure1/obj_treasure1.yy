{
    "id": "8682469e-5f8c-4d3c-a23f-85fbfba77bd0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_treasure1",
    "eventList": [
        {
            "id": "df406488-7f11-4562-a622-0cd0c807e00b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8682469e-5f8c-4d3c-a23f-85fbfba77bd0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
    "visible": true
}