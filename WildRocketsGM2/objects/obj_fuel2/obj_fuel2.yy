{
    "id": "ba5528be-35d1-4809-8569-24960266ff76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fuel2",
    "eventList": [
        {
            "id": "7f692a3e-7a0c-4697-b2f9-fda1b18a8813",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba5528be-35d1-4809-8569-24960266ff76"
        },
        {
            "id": "1b517ff0-13e2-4ad5-aa58-0b0cae9aa3a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "ba5528be-35d1-4809-8569-24960266ff76"
        },
        {
            "id": "8ffb071b-c0d7-44d6-97b9-679922d695d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ba5528be-35d1-4809-8569-24960266ff76"
        },
        {
            "id": "25d49406-61dd-41e7-832b-5f75fbdd5956",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ba5528be-35d1-4809-8569-24960266ff76"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "2f72220b-6b57-4ccf-b958-dc48c864fa0d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "b5d54bcc-d99f-48c6-85d8-9db849654901",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 28
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
    "visible": true
}