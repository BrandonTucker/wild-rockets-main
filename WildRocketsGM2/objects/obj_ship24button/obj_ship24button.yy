{
    "id": "54bd8fb4-2074-4b6a-8940-7cbb42d907eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship24button",
    "eventList": [
        {
            "id": "a6c83e37-96b2-48d5-9d1d-bc8710688259",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "54bd8fb4-2074-4b6a-8940-7cbb42d907eb"
        },
        {
            "id": "7da10b8c-9db5-4cd1-8d9c-5681bc132242",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "54bd8fb4-2074-4b6a-8940-7cbb42d907eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d74df95d-a819-48a0-92d5-c0802d5129ea",
    "visible": true
}