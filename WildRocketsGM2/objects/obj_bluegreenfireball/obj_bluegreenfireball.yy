{
    "id": "d9fb5fcc-c101-40ab-b770-dcaa4b237106",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bluegreenfireball",
    "eventList": [
        {
            "id": "4b0ca1b5-3e1c-444e-ab77-27a46fe3dfa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9fb5fcc-c101-40ab-b770-dcaa4b237106"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "409a320a-78c9-4284-91f4-ac8c6c1636bb",
    "visible": true
}