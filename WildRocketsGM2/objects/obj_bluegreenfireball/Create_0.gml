scale = random_range(.7,2);
action_set_motion(random_range(0,360), choose(.5,.75,1,2,3));
action_sprite_transform(scale, scale, 0, 0);
action_sprite_set(spr_bluegreenfireball, 0, random_range(-1,1));
image_angle=direction+90;

