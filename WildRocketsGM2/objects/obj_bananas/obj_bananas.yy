{
    "id": "95e58032-f09e-4e7d-b6dc-83629c809350",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bananas",
    "eventList": [
        {
            "id": "ca6c13a0-ed9e-49b2-baa9-af042ac906ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95e58032-f09e-4e7d-b6dc-83629c809350"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "53162758-122a-47e4-9819-5bd8c5132503",
    "visible": true
}