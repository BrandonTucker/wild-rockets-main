{
    "id": "e6d9914b-3bd3-47a5-9bcf-01e75961b95b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coin",
    "eventList": [
        {
            "id": "32795782-6ce4-4edd-af9f-f6d6ffef3077",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e6d9914b-3bd3-47a5-9bcf-01e75961b95b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
    "visible": true
}