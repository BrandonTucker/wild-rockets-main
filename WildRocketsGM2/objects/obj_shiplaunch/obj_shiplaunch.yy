{
    "id": "1e95ba52-aee6-4a99-a3c9-df6cf799ee37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplaunch",
    "eventList": [
        {
            "id": "1242d91d-fd17-4911-908c-fd83f2bfdf5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e95ba52-aee6-4a99-a3c9-df6cf799ee37"
        },
        {
            "id": "76ba8e6a-2581-4267-bffb-a7bf9a4e2d13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1e95ba52-aee6-4a99-a3c9-df6cf799ee37"
        },
        {
            "id": "370380a3-bf03-44a9-90ab-086f1f197e81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1e95ba52-aee6-4a99-a3c9-df6cf799ee37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "52018168-deb7-468b-a677-ad4a005447b6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "75e4b917-5094-4bb6-abbd-9590ef4298fd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 28,
            "y": 28
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
    "visible": true
}