{
    "id": "342b1d90-5d25-4dcc-9501-a204e3bcb9bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_metalchunk2",
    "eventList": [
        {
            "id": "619ca7c8-b833-46ea-847e-d8e538adbf52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "342b1d90-5d25-4dcc-9501-a204e3bcb9bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
    "visible": true
}