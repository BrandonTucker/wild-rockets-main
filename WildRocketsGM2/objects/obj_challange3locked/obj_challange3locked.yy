{
    "id": "97f9266e-0edd-4962-9bd3-42f26c330588",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange3locked",
    "eventList": [
        {
            "id": "56c0e40e-fcca-4947-be19-3c64a7145ff6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97f9266e-0edd-4962-9bd3-42f26c330588"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f1401ffc-b4d8-444a-b5a7-533581d15076",
    "visible": true
}