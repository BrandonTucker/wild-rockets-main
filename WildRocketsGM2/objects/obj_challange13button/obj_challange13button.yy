{
    "id": "8e0ebb50-434c-43de-9e02-5fc5e72e6fb2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange13button",
    "eventList": [
        {
            "id": "27ea82a2-4ecc-4013-affd-6bccc22cbdec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e0ebb50-434c-43de-9e02-5fc5e72e6fb2"
        },
        {
            "id": "b7c50d98-74bf-46f7-8442-4d652fc922c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8e0ebb50-434c-43de-9e02-5fc5e72e6fb2"
        },
        {
            "id": "faccdc2a-aa18-4000-8a13-7336237cb612",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8e0ebb50-434c-43de-9e02-5fc5e72e6fb2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97a012b4-7de9-4ed8-982a-6736e91f0702",
    "visible": true
}