{
    "id": "64f6b28d-225c-4563-b9ad-fd646ddbf351",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange32button",
    "eventList": [
        {
            "id": "08b388bb-5558-4ebf-a80c-032b12ca1580",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "64f6b28d-225c-4563-b9ad-fd646ddbf351"
        },
        {
            "id": "ab4969d0-e000-4c20-a54a-d947002fdd03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "64f6b28d-225c-4563-b9ad-fd646ddbf351"
        },
        {
            "id": "f94572f2-211c-45b1-928d-570edde7604c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "64f6b28d-225c-4563-b9ad-fd646ddbf351"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "239fcfbf-5e1e-4e17-b4c3-e032cee56e4a",
    "visible": true
}