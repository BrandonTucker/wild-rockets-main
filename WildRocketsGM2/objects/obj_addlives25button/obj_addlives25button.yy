{
    "id": "8546f459-578f-4c4c-b72f-933542a82316",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_addlives25button",
    "eventList": [
        {
            "id": "17098e7d-6167-49b3-9ff8-6120ecdce8a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8546f459-578f-4c4c-b72f-933542a82316"
        },
        {
            "id": "97d1483a-d9c0-4e42-b11c-867069929e62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8546f459-578f-4c4c-b72f-933542a82316"
        },
        {
            "id": "e900c380-01a5-4b53-a745-0ae4d9bcb8c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8546f459-578f-4c4c-b72f-933542a82316"
        },
        {
            "id": "130cab05-9bc3-4438-87b1-74d268f9b2fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8546f459-578f-4c4c-b72f-933542a82316"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "visible": true
}