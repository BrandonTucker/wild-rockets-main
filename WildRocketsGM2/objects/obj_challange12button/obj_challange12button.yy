{
    "id": "8745a1e2-fd84-42af-86ef-26fd76e5d7ea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challange12button",
    "eventList": [
        {
            "id": "e4380b65-8c9f-45af-a84b-1b54eea4e77e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8745a1e2-fd84-42af-86ef-26fd76e5d7ea"
        },
        {
            "id": "367611d0-7659-412c-98a0-e64667367012",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8745a1e2-fd84-42af-86ef-26fd76e5d7ea"
        },
        {
            "id": "12389082-330d-4862-8873-2acf50868745",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8745a1e2-fd84-42af-86ef-26fd76e5d7ea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "29651981-1a85-4e1b-ac24-5e8a4f7e07a4",
    "visible": true
}