{
    "id": "087de1bd-d544-41b0-9458-378e0a516531",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shiplocked30",
    "eventList": [
        {
            "id": "a8ba535a-5ed1-4b21-81eb-1f4580317ada",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "087de1bd-d544-41b0-9458-378e0a516531"
        },
        {
            "id": "86c41994-f79c-4f48-9bd8-5232b171e6aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "087de1bd-d544-41b0-9458-378e0a516531"
        },
        {
            "id": "9847021c-fd7c-4279-862f-abc26d714491",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "087de1bd-d544-41b0-9458-378e0a516531"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "28499dae-81da-4c9b-9b88-75e0cc1e7e2f",
    "visible": true
}