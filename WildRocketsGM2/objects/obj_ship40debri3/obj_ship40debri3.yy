{
    "id": "ff3d4c4f-b5cd-439e-80a6-3cf557b2ccef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship40debri3",
    "eventList": [
        {
            "id": "20adb4f0-9fcc-4e57-a591-396af9155b02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff3d4c4f-b5cd-439e-80a6-3cf557b2ccef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
    "visible": true
}