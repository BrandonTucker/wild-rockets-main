{
    "id": "0d47a45b-eb9d-4c06-9358-ec13e8329c42",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_addlives55button",
    "eventList": [
        {
            "id": "6f338045-054b-495b-aa19-82056ddc9eaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d47a45b-eb9d-4c06-9358-ec13e8329c42"
        },
        {
            "id": "0dad98a1-ae5c-406f-9f25-bb49a1f09333",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0d47a45b-eb9d-4c06-9358-ec13e8329c42"
        },
        {
            "id": "c5b14b96-fccb-4835-9491-1f64291a2920",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0d47a45b-eb9d-4c06-9358-ec13e8329c42"
        },
        {
            "id": "c2d7b6c9-844f-48a0-ae84-0c0e11c03802",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0d47a45b-eb9d-4c06-9358-ec13e8329c42"
        },
        {
            "id": "d71dbb6c-bf36-4d4b-a2ef-986e0f19752b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0d47a45b-eb9d-4c06-9358-ec13e8329c42"
        },
        {
            "id": "e594f53d-d3a6-402c-bb64-5d6c5ac09d89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 7,
            "m_owner": "0d47a45b-eb9d-4c06-9358-ec13e8329c42"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "visible": true
}