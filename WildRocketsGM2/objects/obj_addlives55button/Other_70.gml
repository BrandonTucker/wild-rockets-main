/// @description Insert description here
// You can write your code in this editor
var _id = async_load[? "id"];
    if _id == GoogleMobileAds_ASyncEvent
    {
    var ident = async_load[? "type"]
    switch (ident)
        {
        case "rewardedvideo_adopened":
            show_debug_message("Rewards Video Opened");
            break;
        case "rewardedvideo_videostarted":
            show_debug_message("Rewards Video Started");
            break;
        case "rewardedvideo_watched":
            show_debug_message("Rewards Video Watched");
            global.rewarded_viewed = true;
            global.Lives += async_load[? "amount"];
            break;
		case "rewardedvideo_loadfailed":
	        show_debug_message("Error Code: " + string(async_load[? "errorcode"]));
	        GoogleMobileAds_LoadRewardedVideo(global.rewarded_id);
	        break;
        case "rewardedvideo_adclosed":
            show_debug_message("Rewards Video Closed");
            if global.rewarded_viewed == false
                {
                // Reward video was closed before the end
                // Here you can set button states, show a message, etc...
                }
            else global.rewarded_viewed = false;
            GoogleMobileAds_LoadRewardedVideo("ca-app-pub-2228303184657007~6391179659");
            break;
        }
    }