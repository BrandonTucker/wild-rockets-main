action_set_relative(1);
global.count += 1;
global.lifeY = y;
{
action_set_relative(0);
can_move = true;
action_set_relative(1);
}
count += 1;
{
action_set_relative(0);
global.fuelframe = 0;
action_set_relative(1);
}
var shipindex = global.ship;

if shipindex = 1 { sprite_index = spr_ship1
}
if shipindex = 2 { sprite_index = spr_ship2
}
if shipindex = 3 { sprite_index = spr_ship3
}
if shipindex = 4 { sprite_index = spr_ship4
}
if shipindex = 5 { sprite_index = spr_ship5
}
if shipindex = 6 { sprite_index = spr_ship6
}
if shipindex = 7 { sprite_index = spr_ship7
}
if shipindex = 8 { sprite_index = spr_ship8
}
if shipindex = 9 { sprite_index = spr_ship9
}
if shipindex = 10 { sprite_index = spr_ship10
}
if shipindex = 11 { sprite_index = spr_ship11
}
if shipindex = 12 { sprite_index = spr_ship12
}
if shipindex = 13 { sprite_index = spr_ship13
}
if shipindex = 14 { sprite_index = spr_ship14
}
if shipindex = 15 { sprite_index = spr_ship15
}
if shipindex = 16 { sprite_index = spr_ship16
}
if shipindex = 17 { sprite_index = spr_ship17
}
if shipindex = 18 { sprite_index = spr_ship18
}
if shipindex = 19 { sprite_index = spr_ship19
}
if shipindex = 20 { sprite_index = spr_ship20
}
if shipindex = 21 { sprite_index = spr_ship21
}
if shipindex = 22 { sprite_index = spr_ship22
}
if shipindex = 23 { sprite_index = spr_ship23
}
if shipindex = 24 { sprite_index = spr_ship24
}
if shipindex = 25 { sprite_index = spr_ship25
}
if shipindex = 26 { sprite_index = spr_ship26
}
if shipindex = 27 { sprite_index = spr_ship27
}
if shipindex = 28 { sprite_index = spr_ship28
}
if shipindex = 29 { sprite_index = spr_ship29
}
if shipindex = 30 { sprite_index = spr_ship30
}
if shipindex = 31 { sprite_index = spr_ship31
}
if shipindex = 32 { sprite_index = spr_ship32
}
if shipindex = 33 { sprite_index = spr_ship33
}
if shipindex = 34 { sprite_index = spr_ship34
}
if shipindex = 35 { sprite_index = spr_ship35
}
if shipindex = 36 { sprite_index = spr_ship36
}
if shipindex = 37 { sprite_index = spr_ship37
}
if shipindex = 38 { sprite_index = spr_ship38
}
if shipindex = 39 { sprite_index = spr_ship39
}
if shipindex = 40 { sprite_index = spr_ship40
}

if(count == 35){
    viewoffset = __view_get( e__VW.YView, 0 ) - _.y;
    
    _.y = 8096;
    instance_activate_all();
    obj_fuel2.y=_.y-200;
    obj_combo.y = _.y;
    obj_control.y = _.y;
    if(instance_number(obj_challangecontrol) == 1){
        obj_challangecontrol.y = _.y;
    }
    
    
    //view_yview[0] = 8348; 
    __view_set( e__VW.YView, 0, 8096 + viewoffset ); 
    count = 0;
    
    with (obj_create2)
    {
     instance_destroy();
    }
    with (obj_coin)
    {
     instance_destroy();
    }
    with (obj_blockdestructable2)
    {
     instance_destroy();
    }
    with (obj_bananas)
    {
     instance_destroy();
    }
    with (obj_pineapple)
    {
     instance_destroy();
    }
    with (obj_grapes)
    {
     instance_destroy();
    }
    with (obj_diamond)
    {
     instance_destroy();
    }
    with (obj_ruby)
    {
     instance_destroy();
    }
    with (obj_emerald)
    {
     instance_destroy();
    }
    with (obj_block2)
    {
     instance_destroy();
    }
    with (obj_blockgap)
    {
     instance_destroy();
    }
    with (obj_block3)
    {
     instance_destroy();
    }
    
  //instance_create(0,832,obj_create)
  //instance_create(0,1056,obj_create)
  instance_create(0,1280,obj_create2)
  instance_create(0,1504,obj_create2)
  instance_create(0,1728,obj_create2)
  instance_create(0,1952,obj_create2)
  instance_create(0,2176,obj_create2)
  instance_create(0,2400,obj_create2)
  instance_create(0,2624,obj_create2)
  instance_create(0,2848,obj_create2)
  instance_create(0,3072,obj_create2)
  instance_create(0,3296,obj_create2)
  instance_create(0,3520,obj_create2)
  instance_create(0,3744,obj_create2)
  instance_create(0,3968,obj_create2)
  instance_create(0,4192,obj_create2)
  instance_create(0,4416,obj_create2)
  instance_create(0,4640,obj_create2)
  instance_create(0,4864,obj_create2)
  instance_create(0,5088,obj_create2)
  instance_create(0,5312,obj_create2)
  instance_create(0,5536,obj_create2)
  instance_create(0,5760,obj_create2)
  instance_create(0,5984,obj_create2)
  instance_create(0,6208,obj_create2)
  instance_create(0,6432,obj_create2)
  instance_create(0,6656,obj_create2)
  instance_create(0,6880,obj_create2)
  instance_create(0,7104,obj_create2)
  instance_create(0,7328,obj_create2)
  instance_create(0,7552,obj_create2)
  //instance_create(0,7776,obj_create)
}

with (obj_combo) {
action_set_relative(0);
action_move_to(_.x, _.y);
action_set_relative(1);
}
with (obj_control) {
action_set_relative(0);
action_move_to(_.x, _.y);
action_set_relative(1);
}
with (obj_challangecontrol) {
action_set_relative(0);
action_move_to(_.x, _.y);
action_set_relative(1);
}

with (obj_drawLives) {
action_set_relative(0);
action_move_to(_.x, _.y);
action_set_relative(1);
}


action_set_alarm(120, 1);


instance_activate_region(__view_get( e__VW.XView, 0 )-64-480,__view_get( e__VW.YView, 0 )-256,__view_get( e__VW.WView, 0 )+128+480,__view_get( e__VW.HView, 0 )+512,true);
instance_deactivate_region(__view_get( e__VW.XView, 0 )-64-480,__view_get( e__VW.YView, 0 )-256,__view_get( e__VW.WView, 0 )+128+480,__view_get( e__VW.HView, 0 )+512,false,true);






