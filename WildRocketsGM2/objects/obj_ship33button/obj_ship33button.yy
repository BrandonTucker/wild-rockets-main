{
    "id": "b2c5fbfa-b0e1-40db-bcb8-c4415a05b743",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship33button",
    "eventList": [
        {
            "id": "fe64fdb0-b913-4479-8988-4201b68220d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2c5fbfa-b0e1-40db-bcb8-c4415a05b743"
        },
        {
            "id": "4cd8dbca-9480-42c1-8f6e-8cc0b0cbe8cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b2c5fbfa-b0e1-40db-bcb8-c4415a05b743"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75af407b-23a2-49e1-a535-43b890cec698",
    "visible": true
}