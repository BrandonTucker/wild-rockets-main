{
    "id": "09cc613a-e688-4e03-aeba-ca8e97af06cb",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font4",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b3bf6375-5cde-4af8-9fc2-727b0b76443c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 456,
                "y": 254
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1aec9006-317b-4e2c-9dc3-34c5bc5a9c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 61,
                "offset": 4,
                "shift": 18,
                "w": 9,
                "x": 124,
                "y": 317
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a85d2eb9-2211-4976-a26a-d5086da71b43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 61,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 253,
                "y": 254
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5f6dd672-8c2d-4ac9-8fd2-db9d22e414d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 254,
                "y": 128
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "dc5d09f9-0710-4520-919f-a8d7cc7adceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 118,
                "y": 191
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "22096fe0-bd24-45f7-b08a-b0327442dacc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 61,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b9673056-5a61-4566-bdc9-8461d94e377d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 61,
                "offset": 2,
                "shift": 38,
                "w": 36,
                "x": 79,
                "y": 65
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "cce5e323-4af5-4e33-82d2-e1d573f9f4c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 61,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 91,
                "y": 317
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f9765e0e-70c9-46a2-af92-812bae9692ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 61,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 490,
                "y": 254
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0103e0d2-11c7-4d28-81dc-f87b2929ec89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 14,
                "x": 34,
                "y": 317
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3680606b-6a80-4294-b11b-2e66e6985255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 61,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 276,
                "y": 254
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "fade7d53-1abd-4b53-8a0d-a68776adb3e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 89,
                "y": 191
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2db50416-ff09-4897-b96d-854381859ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 157,
                "y": 317
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "74595dbf-9f00-4407-974a-2e3dbd0b163f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 61,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 473,
                "y": 254
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c0839f6f-ae1a-43df-9f06-e733500d5d09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 102,
                "y": 317
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b688befb-dc1b-4093-8531-0c7e08a9b4e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 61,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 420,
                "y": 254
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "6b5956d9-176d-4af3-b01a-c6eb2166d90f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 226,
                "y": 254
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3eaad6a0-0efa-4014-a9e9-3094e363754c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 61,
                "offset": 4,
                "shift": 29,
                "w": 17,
                "x": 401,
                "y": 254
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "baa84cb2-4903-426a-9545-a1740ffa9391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 198,
                "y": 254
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1aca8786-6d81-49ef-b72c-b3c39ace4316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 170,
                "y": 254
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e4bc89e5-4513-498c-9610-013206795b84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 285,
                "y": 128
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7d002388-05cb-432e-9351-dd4db5c3ff13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 2,
                "y": 254
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "813ce64b-49b9-4bb0-8ee0-4f7d21d4b41e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 142,
                "y": 254
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2545d6c9-d0da-4ec8-b75a-1aa554126fc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 58,
                "y": 254
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b94c073c-5036-4295-a3cc-8cef9c4c6c1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 405,
                "y": 191
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "cf1e5752-4cdd-4c8c-ad29-d2a19d502534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 263,
                "y": 191
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4aebfb85-7549-4c15-9e07-442e7f7b7656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 61,
                "offset": 5,
                "shift": 18,
                "w": 8,
                "x": 167,
                "y": 317
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "14f7c675-ef04-4abe-85c0-1ebf55a2e870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 61,
                "offset": 4,
                "shift": 18,
                "w": 9,
                "x": 135,
                "y": 317
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "05a22cb7-ccac-4ae7-ad36-29cf7d8de6d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 205,
                "y": 191
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c28edee5-322a-4712-a0c8-9fe348b542f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 176,
                "y": 191
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c60d4556-f3f5-4fec-9338-54c7f4722b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 147,
                "y": 191
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9c33543f-77f2-449e-b227-55a7bb4fbca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 436,
                "y": 128
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4e41a814-c997-4138-a3fc-2b67bf6a423f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 61,
                "offset": 1,
                "shift": 52,
                "w": 51,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b8c43551-9f45-4c13-8292-2092fbf6f6d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 61,
                "offset": 0,
                "shift": 38,
                "w": 39,
                "x": 280,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "264c9275-e8f8-4354-89cb-ae2070db2eb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 228,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c484248b-15b3-456f-96ef-b822775b163f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 61,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 192,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e974cf0c-2ea6-4eb0-888e-46ff20540179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 263,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "0d171a59-2772-4ae8-8559-9dd44de01e3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 61,
                "offset": 3,
                "shift": 35,
                "w": 30,
                "x": 35,
                "y": 128
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "03a18bd2-a027-4b0f-b41e-86169b3a90f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 234,
                "y": 191
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4f9665ec-3f29-4015-a412-28c9a581e806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0a40ec9b-a8b9-4db2-a04b-f3396fdad1d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 434,
                "y": 65
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4650ecd8-1677-4feb-8df6-c86be37da14d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 113,
                "y": 317
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0cb433a9-ddd1-45bc-aa93-37126fa62645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 26,
                "x": 433,
                "y": 191
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ce9a0378-38f2-4708-a3d2-918769f9d370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 36,
                "x": 117,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3c755fad-b188-47bb-9c2e-2c059a453219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 61,
                "offset": 4,
                "shift": 32,
                "w": 27,
                "x": 292,
                "y": 191
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ee8787ed-9a64-47c3-8754-71ad752271b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 61,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 321,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0439c6c0-3d86-4972-a189-6e9da0b15356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 400,
                "y": 65
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "180ea161-6386-4343-82e6-55ea8ccd0e34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 38,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7b1291d9-253a-4dc1-a9e1-e646078644ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 61,
                "offset": 3,
                "shift": 35,
                "w": 30,
                "x": 67,
                "y": 128
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7c686147-0a28-4ff1-84b6-c1c197f863f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 39,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a2e0286a-908d-48db-9382-87fc658866cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 35,
                "x": 155,
                "y": 65
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "399edf0a-e549-40d2-bf42-5a7570c50005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 61,
                "offset": 1,
                "shift": 35,
                "w": 32,
                "x": 366,
                "y": 65
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f34b0aac-7f4b-45dd-9e97-2c0dd9d4a50c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 61,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 2,
                "y": 128
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "79b259a6-f270-4474-a003-758c9a41d65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 298,
                "y": 65
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "23a0d888-b6e0-4dbb-b9c8-854912b7e620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 61,
                "offset": -1,
                "shift": 35,
                "w": 37,
                "x": 440,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1e2896e5-e013-4e0e-afa7-c0385425684d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 61,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4d079cec-65b9-43e7-9817-d8499106906e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 61,
                "offset": 0,
                "shift": 35,
                "w": 36,
                "x": 41,
                "y": 65
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a7be799f-8ee8-4148-b7ce-276e497e0c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 61,
                "offset": -1,
                "shift": 35,
                "w": 37,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9c1ad82f-2a38-43c7-8f6c-13fa9eb8ab63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 61,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 332,
                "y": 65
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "11f1083c-53e0-436c-8e4b-3c6de8f0cc49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 61,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 2,
                "y": 317
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d253986c-52b0-49e6-a72a-eea8a8c198e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 61,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 438,
                "y": 254
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2073453d-05a1-4788-8d0b-fceac47edc4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 61,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 18,
                "y": 317
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "48e5bd40-721a-4782-ad74-416022c99b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 461,
                "y": 191
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7f6ea9d6-ad2a-48bf-9853-7f38b52bb253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 61,
                "offset": -1,
                "shift": 29,
                "w": 31,
                "x": 468,
                "y": 65
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "91eddcfb-c344-4dc7-8bb4-9d1a8f3ad295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 12,
                "x": 66,
                "y": 317
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c606255d-6b4c-4bd4-81ad-b0ea63f7a9d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 60,
                "y": 191
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d2f69674-fda8-4b5a-9148-070d0a51a1bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 28,
                "x": 406,
                "y": 128
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1b1d3c10-c881-4756-a758-ed246de4984f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 27,
                "x": 31,
                "y": 191
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4253b98f-406b-4f1d-a965-dd2557877429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 376,
                "y": 128
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5e29fd7a-c1c6-465a-8b1e-3baa3b2f94e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 2,
                "y": 191
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "becddacc-26f7-445e-90de-a36c8045ae94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 61,
                "offset": 0,
                "shift": 18,
                "w": 20,
                "x": 298,
                "y": 254
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "23b0694b-d479-42a1-9fba-89841eec308f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 27,
                "x": 466,
                "y": 128
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "102e1269-ed2b-4096-a4cc-09cd44944d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 349,
                "y": 191
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "98c66dc1-6098-4a28-8ba1-328317de90d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 80,
                "y": 317
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d3bcd899-6ca1-4597-bf15-3a10179b4b9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 61,
                "offset": -3,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 317
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "421686dc-2237-4b5e-9d12-2ef52c266aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 26,
                "x": 114,
                "y": 254
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "14069862-c14f-49f0-b183-d85526ae4576",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 146,
                "y": 317
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "11b8d80d-a136-46a5-a2f0-648f90aff507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 61,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "dd01e4c1-b879-4654-a579-a6f5a45d88e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 30,
                "y": 254
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c90e4ea1-01cb-4804-9f0d-5aefad6d6bca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 99,
                "y": 128
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a256732c-ab72-4c62-9f4c-48f62ff43938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 28,
                "x": 316,
                "y": 128
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "67f0a36a-42e3-4d73-8aaf-40c62f0876c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 346,
                "y": 128
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "cd14f72f-4947-4595-82ae-566544e3d181",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 61,
                "offset": 3,
                "shift": 21,
                "w": 19,
                "x": 320,
                "y": 254
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b416e33b-5a5e-4724-80a7-3e5cbc527600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 321,
                "y": 191
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "97b358ff-e70d-4a48-9bb3-c92f29e1a2d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 61,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 382,
                "y": 254
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b6ec6586-eb5a-488f-8dfb-848e94be26e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 86,
                "y": 254
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c9692d50-6098-4895-bf6a-18d4cd9d66a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 161,
                "y": 128
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e1fac7ab-5249-4466-ba9f-ccd9c6e676f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 61,
                "offset": 0,
                "shift": 41,
                "w": 42,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3a0caca9-c497-4b4c-944c-fc889acea9de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 130,
                "y": 128
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b4b0ec83-2136-4bc7-9dab-542f4877b675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 192,
                "y": 128
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "67e54b7e-48d7-47fe-b1af-21c5ee0f668d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 61,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 377,
                "y": 191
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c4c06bdb-ebc0-47fc-8dc4-51d393f9cfbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 61,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 341,
                "y": 254
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f9eab299-af1b-48c7-8e7e-263d36bd39c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 177,
                "y": 317
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2b9deb8b-755b-44c3-949d-34cdcc8d92f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 61,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 362,
                "y": 254
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "65e874ff-578b-4efc-8939-18bba727634e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 61,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 223,
                "y": 128
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "2b29aafe-f687-4e98-9fc9-34676eaf9099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "1c5336af-98ba-47bb-8b46-ca27b710ec83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "f99d9b2b-6042-4c42-aded-25aafa546024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "2343a5b7-95f8-4dda-9f17-a80d0df8f6cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "28a46f32-6dd2-41f1-a8a7-6ae989c31183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "43608144-47d0-44e7-bfce-9d1674dcf59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "d6ef596a-b0f7-4d68-ac17-ba8cfecd5336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "bc494061-9bfb-4210-8669-453da1b93707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "a3f101df-8da5-4436-85ee-5f9cb158b7b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 49
        },
        {
            "id": "c581703f-20ea-45c8-af86-afec05496f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "aa484358-f59d-4e0a-8481-f07494c0fc28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "7a1fd059-51e2-4a27-a1f6-82735ce15ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "585e51b2-5644-41fa-b8ff-6a576047366c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 87
        },
        {
            "id": "acbc8f9f-cbf4-4fed-8c1f-e9004c823fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 89
        },
        {
            "id": "8e5c072c-9222-4329-9cbc-fc5752eaf5ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "c6c6db95-2874-40d9-ae8e-0abf6b058b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "7b5806f2-115b-430d-ab8f-89e1a84de4e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 121
        },
        {
            "id": "85f6555c-3fd5-4fcc-aadc-d1978b1be56b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "93bfcf3e-f58b-441d-9731-5031c6957468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "568ad8f0-f239-4753-89b7-d407a6977102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 44
        },
        {
            "id": "a6da7c21-44bc-4d93-8573-9257c5d4c722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 46
        },
        {
            "id": "29226bb0-4c27-4bca-a7fb-d55a60b78de7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "7fd08d84-3a30-46e4-ad09-3f6e8896bf5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "e8af165a-50c6-4b4b-9f14-dc5332674fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "92356cee-4194-4283-bd5d-88f8464247e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "0fa1d334-7e27-4161-9b51-280ecebef7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "19f1f5aa-72a7-4672-8a63-528af072ec8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 89
        },
        {
            "id": "797d5f1f-7fe6-4a7b-b685-8b949ba76568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "7315abd3-4385-4d38-bcea-16526ee58888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "834858a6-48b0-47c2-a560-75aa3bd1cc47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "f1de184d-b719-44be-bc1b-563fcf5553f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "9de35ac4-5e3f-4c03-882a-e9de180c74b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 44
        },
        {
            "id": "315ceec4-5354-45f0-9386-20c2c70c19f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 46
        },
        {
            "id": "71382bae-a378-4060-b31b-bcac8b5dd4d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "19fde8de-87b8-4e0e-ab02-a367f4fcac10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "7b5d924f-fd5b-47c2-aa0b-f791e82e221f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "2bdbdca2-7f54-4646-bc89-526a92b91a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "b357851f-0a1e-49f2-8128-219602f5a36c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "cb9e19b8-b5dc-4817-a932-9f2d89e0f54a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 44
        },
        {
            "id": "48a84ddf-83b2-4e4a-8bf7-4f0929161e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "028f5c32-29ad-4a78-b48d-4e67ee065050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 46
        },
        {
            "id": "6624201c-c509-4c25-b54d-10e0be901743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 58
        },
        {
            "id": "45cc52b8-1b35-4f22-87ea-69ab3cab4144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 59
        },
        {
            "id": "24d5599c-6b46-469a-b1f1-ab205a3975a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "ad6bccd3-38aa-4492-b74f-de44bb11f5ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "943ea554-0ce3-4c87-9df7-630822713f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "379e5888-2a0e-49be-b57b-d73999419814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "bf0d29cb-7326-47b5-a5b1-b6f038b856c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "25de0a9b-a796-40b8-809e-dcaa209af600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "8f410d6d-ba40-4b07-9560-7f579a47f291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "0ebbce0d-0157-4066-9398-e61a3044a14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 114
        },
        {
            "id": "ee767bc3-6232-4f28-9242-9c91e6960891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "91d2624b-ced7-4395-a9f1-965618a6a347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 117
        },
        {
            "id": "6b0a0c22-618f-46ad-a044-eb2ef1b7acbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 119
        },
        {
            "id": "ba144b24-c5d2-47dc-8a72-73910e9a2f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 121
        },
        {
            "id": "8a79d630-41f7-4ecb-8b42-441609c085a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "b9ce47e0-efcc-438a-90df-99f154b640a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 894
        },
        {
            "id": "b55054e0-78cc-4917-ac76-3ec2ffe14cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 44
        },
        {
            "id": "703cde1f-5647-4374-b27a-d31bbeea1d28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "36ae5f40-1129-41b6-b87d-460eecd7aa98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 46
        },
        {
            "id": "57a77788-76e8-42a1-ba7c-4ac0b4f361df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 58
        },
        {
            "id": "a6e8da77-a2ad-4d58-bd69-cf91c80fd04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 59
        },
        {
            "id": "28a519a7-1a15-4e8e-a727-05041b7b7854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "c5a95b7f-c5ae-447d-a71c-d908147a3ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "33624ada-c3e2-4f4e-a10c-c97ecbd7ac9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "39c91e72-bbc0-4db3-afa6-7c63e3431408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "d07a9976-9cfd-43a6-a657-f31347e88f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 111
        },
        {
            "id": "4cdb68d2-97f6-4bea-b608-39837da5431c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 114
        },
        {
            "id": "e58dac76-4a14-48a6-ac84-1d99b8c4d456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "e7c6c46f-bb62-4689-9527-63e23abf3483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "85c6d085-8ee1-4479-a4ac-c7f3574e8bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 173
        },
        {
            "id": "3c8192d4-1beb-485d-b8a7-928616219828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 894
        },
        {
            "id": "7500270a-5238-41e9-ba34-46b4e8099389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "a73af19a-8e78-4e50-9ecd-c9dd026f878b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "2f06c278-5bba-41cd-a13f-01096aa74481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "6b66ccf5-46a3-4961-ad2b-d26d3d6dc3cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "d51ad573-9207-444e-8c33-e216e5756a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "459a8e90-0abf-4f92-8e64-d91171d0a585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 65
        },
        {
            "id": "54aa0f1c-7150-49c3-9f06-65487447ab4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "3bd0cfb9-b396-4f0c-a53c-5462c7756465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "07d1458c-81b8-4d5c-be20-415e6d2d6ebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "638c4485-111e-4674-b540-477e621cb63a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "3437cb47-60e4-4b10-b712-04f93d979c24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "762dfcff-4948-4bb5-b9af-9723160bfe2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "3dfb6a77-980d-42e4-9d2a-d2d797bade1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "bd1401b8-21c2-44aa-aa6a-8b7d9e739812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "75ad7ed1-d94f-45d3-bae9-c51928d9d203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "87afb545-61c3-491d-8290-00af18ef5fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 44
        },
        {
            "id": "8e32a2e6-8978-49e4-b300-aa3b0a0e342f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "183de51e-98bd-4ee9-bcc6-f62560d36812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 46
        },
        {
            "id": "ad64fdde-c8d0-4bd9-a720-192d7d9b6438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 58
        },
        {
            "id": "62efd07c-56f1-4f14-8689-fee65be05a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 59
        },
        {
            "id": "7165ed3a-3b32-4cf8-bd61-308949f3e089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 65
        },
        {
            "id": "3fe345bb-063d-476e-92d3-296d72d91449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "2f85f076-b95e-4593-ba1d-6e41f403de3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "801d0899-30a8-4868-8fd1-8c746129b577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "672e4b43-8400-49ba-9cd7-d8e0791c5bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "04278dfd-183c-4dde-831f-3d6ef31f7ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 112
        },
        {
            "id": "3016f80c-5421-4efa-850a-70208f2f116e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "188e4cce-377d-4266-8de7-0fa4d6d59e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "e0a612d0-9ff1-47b4-9bcc-2ae42fff56ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 118
        },
        {
            "id": "416d9e7e-eb0e-45eb-8acb-accd19306286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "12e8f3d1-1bdf-4aea-8b03-48b0a755cf6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 173
        },
        {
            "id": "33438129-5be4-40ff-942b-1f97c06befe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 894
        },
        {
            "id": "86dac6bd-45db-4085-af1d-f267b41dcb74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "718e7487-dcbe-45ac-b6df-5e103cf4b2f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "c6381026-f6a5-48b4-be88-8dc2d5392c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "3a931113-debe-49e5-973b-2f93402aeaa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "29dc0eb7-ba6f-453f-9793-afa9812b819d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "91ccb031-51ed-48ad-9915-824a5e3949b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "6bf77cfe-f5be-451a-944a-7d2cfea66d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "d7ec6004-be1a-414b-888d-4638f6daca8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "79aed122-882d-4241-9677-069377762409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "e2928ebb-f2ea-43a8-9da6-77e853311b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 40,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}