{
    "id": "f5103678-aea1-4cff-bb57-178e89fe271d",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "David",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2331dda9-6e97-4832-91a1-da80f23482f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 133,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 603,
                "y": 677
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4c7f8957-09ad-4067-b3e2-d047edfd3271",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 133,
                "offset": 6,
                "shift": 40,
                "w": 17,
                "x": 830,
                "y": 677
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "fd6131d6-9765-413c-8c28-b86c9dc427c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 133,
                "offset": 7,
                "shift": 59,
                "w": 45,
                "x": 97,
                "y": 677
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "8c7dc2ce-08db-47ea-9b0f-070634512b57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 133,
                "offset": 0,
                "shift": 55,
                "w": 55,
                "x": 176,
                "y": 542
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1f6af6a6-24b6-4e42-b848-c7f1676c624f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 133,
                "offset": 4,
                "shift": 59,
                "w": 49,
                "x": 714,
                "y": 542
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "03904e47-f512-4e16-af00-288e7089b16b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 133,
                "offset": 8,
                "shift": 103,
                "w": 87,
                "x": 747,
                "y": 137
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b740dc9e-aa8c-44ea-af6c-fa91c6a6f2cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 133,
                "offset": 2,
                "shift": 85,
                "w": 81,
                "x": 925,
                "y": 137
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "93197bf1-e162-4fdb-a314-caed91248fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 133,
                "offset": 5,
                "shift": 29,
                "w": 18,
                "x": 810,
                "y": 677
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7fb3a53d-209e-4ae7-9840-681b7b91ab89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 133,
                "offset": 6,
                "shift": 33,
                "w": 29,
                "x": 634,
                "y": 677
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "234a9ee2-191f-447a-88f9-4b1a82d78d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 133,
                "offset": -2,
                "shift": 33,
                "w": 29,
                "x": 665,
                "y": 677
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d756f2a8-c179-47a2-888d-2b32be6f8da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 133,
                "offset": 7,
                "shift": 48,
                "w": 35,
                "x": 431,
                "y": 677
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "82b72ea8-e027-465a-89fd-b331074559ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 133,
                "offset": 10,
                "shift": 81,
                "w": 63,
                "x": 534,
                "y": 407
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "235a6201-7365-45cc-a8b7-b57fc50304bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 133,
                "offset": 7,
                "shift": 33,
                "w": 17,
                "x": 849,
                "y": 677
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8c861cba-4d20-458c-b62a-c53ca1199567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 133,
                "offset": 4,
                "shift": 44,
                "w": 37,
                "x": 276,
                "y": 677
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8431cbb0-0960-474e-8bc2-39dfd28bae39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 133,
                "offset": 7,
                "shift": 33,
                "w": 17,
                "x": 868,
                "y": 677
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "fcaf6769-94e6-4f83-a9cb-97cd845e800d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 133,
                "offset": -1,
                "shift": 44,
                "w": 46,
                "x": 865,
                "y": 542
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1599119a-eb83-4ac4-9c9e-155b52331ed8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 133,
                "offset": 4,
                "shift": 59,
                "w": 52,
                "x": 345,
                "y": 542
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ea443375-2769-4111-9d37-8e78dbb94cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 133,
                "offset": 7,
                "shift": 59,
                "w": 45,
                "x": 144,
                "y": 677
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "62e3eb42-68cc-4088-91e4-cd0c7182c375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 133,
                "offset": 5,
                "shift": 59,
                "w": 46,
                "x": 913,
                "y": 542
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "bbf369fd-db68-447e-8ddb-da34cd92529a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 133,
                "offset": 8,
                "shift": 59,
                "w": 46,
                "x": 2,
                "y": 677
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c231f085-4293-4558-9670-594c49f9726f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 133,
                "offset": 3,
                "shift": 59,
                "w": 55,
                "x": 119,
                "y": 542
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9492abed-5654-4391-aa11-4865677edbaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 133,
                "offset": 7,
                "shift": 59,
                "w": 45,
                "x": 50,
                "y": 677
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "00e982fe-0083-4c08-aed0-9530b129d43d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 133,
                "offset": 5,
                "shift": 59,
                "w": 50,
                "x": 559,
                "y": 542
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "33b6854c-a34d-4d42-8f28-e811106de0ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 133,
                "offset": 5,
                "shift": 59,
                "w": 51,
                "x": 453,
                "y": 542
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ec76d9ed-ebde-4cff-a17d-8ba85bd05be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 133,
                "offset": 6,
                "shift": 59,
                "w": 47,
                "x": 816,
                "y": 542
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bbdcba2a-39e8-4b61-a433-1989f36ed804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 133,
                "offset": 5,
                "shift": 59,
                "w": 50,
                "x": 611,
                "y": 542
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "149610eb-3f43-40ae-b9ef-6536eed42b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 133,
                "offset": 9,
                "shift": 52,
                "w": 16,
                "x": 905,
                "y": 677
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c8b318e8-6071-450d-87fa-ad5809301a51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 133,
                "offset": 9,
                "shift": 52,
                "w": 16,
                "x": 887,
                "y": 677
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0e7e353c-c10f-4f98-b626-ae28fc15e202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 133,
                "offset": 8,
                "shift": 74,
                "w": 58,
                "x": 914,
                "y": 407
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "18513f43-9f0a-4999-881c-9bbbec2bc953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 133,
                "offset": 10,
                "shift": 81,
                "w": 63,
                "x": 664,
                "y": 407
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8330cd44-9e5c-4a7c-ad5e-a3478d919789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 133,
                "offset": 8,
                "shift": 74,
                "w": 58,
                "x": 2,
                "y": 542
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "46f3c4bb-1cd4-480d-ba35-97444ab5f03d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 133,
                "offset": 10,
                "shift": 48,
                "w": 27,
                "x": 756,
                "y": 677
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "35098a2e-c064-4740-8f0e-7d13fff78327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 133,
                "offset": 6,
                "shift": 122,
                "w": 114,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "42a13c15-42ff-4b2a-88d6-d4a8b40cea4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 133,
                "offset": 1,
                "shift": 96,
                "w": 94,
                "x": 194,
                "y": 137
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "619b472b-00ab-4705-9c99-cc565b999b44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 133,
                "offset": 2,
                "shift": 89,
                "w": 80,
                "x": 85,
                "y": 272
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "df1de2ff-15f1-457b-b2d7-13f11ef1813f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 133,
                "offset": 4,
                "shift": 89,
                "w": 81,
                "x": 2,
                "y": 272
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0ae286bd-9b03-4506-aa74-a7cc707d48be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 133,
                "offset": 2,
                "shift": 96,
                "w": 89,
                "x": 477,
                "y": 137
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "57867727-63fe-4883-b96b-1036f1e1ca9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 133,
                "offset": 2,
                "shift": 81,
                "w": 77,
                "x": 167,
                "y": 272
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "fcd4f083-2d07-4b85-924d-1c82a72a9dd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 133,
                "offset": 2,
                "shift": 74,
                "w": 67,
                "x": 908,
                "y": 272
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c9f43809-433c-4cd4-8026-ea2b7ff57ce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 133,
                "offset": 4,
                "shift": 96,
                "w": 91,
                "x": 384,
                "y": 137
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a9ec5692-2446-433f-815a-ff00a247ddf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 133,
                "offset": 2,
                "shift": 96,
                "w": 92,
                "x": 290,
                "y": 137
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "92eed2be-a74c-485f-a6fb-0ab99ea54366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 133,
                "offset": 3,
                "shift": 44,
                "w": 39,
                "x": 235,
                "y": 677
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a427da52-d6e3-4824-8694-9a088ed39dc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 133,
                "offset": 2,
                "shift": 52,
                "w": 49,
                "x": 663,
                "y": 542
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1c639226-2c4f-44c8-8e00-1030065e1db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 133,
                "offset": 2,
                "shift": 96,
                "w": 96,
                "x": 563,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5241a82c-5ae0-4e97-92ba-daf8d309de91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 133,
                "offset": 2,
                "shift": 81,
                "w": 77,
                "x": 325,
                "y": 272
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5d27b8b2-36be-4787-b94c-7263b8013399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 133,
                "offset": 2,
                "shift": 118,
                "w": 114,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ab9c1718-e047-45f0-84e7-6e32bfec6e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 133,
                "offset": -2,
                "shift": 96,
                "w": 97,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d993f824-d8bb-4baf-9f45-a3d2336c3ed8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 133,
                "offset": 4,
                "shift": 96,
                "w": 87,
                "x": 836,
                "y": 137
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ee813e83-8409-4d48-a5c0-a623744ee4b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 133,
                "offset": 2,
                "shift": 74,
                "w": 68,
                "x": 700,
                "y": 272
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c1e0a0c1-a8c1-41d1-abef-f5c704d5a917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 133,
                "offset": 4,
                "shift": 96,
                "w": 87,
                "x": 658,
                "y": 137
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5cdd8c4c-cb6a-41eb-af0f-febcb4d055c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 133,
                "offset": 2,
                "shift": 89,
                "w": 88,
                "x": 568,
                "y": 137
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "949c9498-3b48-4e8d-88bd-f71b524c5a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 133,
                "offset": 8,
                "shift": 74,
                "w": 59,
                "x": 793,
                "y": 407
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "962a0369-5904-47ad-a71c-a4b969c4f18c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 133,
                "offset": 4,
                "shift": 81,
                "w": 74,
                "x": 482,
                "y": 272
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "38ae8e6a-d400-4ad5-934b-aceb9db00836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 133,
                "offset": 0,
                "shift": 96,
                "w": 95,
                "x": 855,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "98d25466-3c08-4295-9a5a-147d45256a84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 133,
                "offset": 1,
                "shift": 96,
                "w": 94,
                "x": 98,
                "y": 137
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1d698794-2e2a-4ca6-8e58-bfd8fbaede99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 133,
                "offset": 1,
                "shift": 126,
                "w": 124,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6e7cb292-414e-44ac-8921-76d6d12f39bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 133,
                "offset": 0,
                "shift": 96,
                "w": 95,
                "x": 758,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6e171954-de42-4d7c-9245-dfb2463b3757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 133,
                "offset": 1,
                "shift": 96,
                "w": 94,
                "x": 2,
                "y": 137
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6c13e7a9-da6b-4a9c-a2f0-bb2bcaff7775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 133,
                "offset": 1,
                "shift": 81,
                "w": 77,
                "x": 246,
                "y": 272
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c1d59493-b60f-41dd-825f-f3b836b25734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 133,
                "offset": 4,
                "shift": 33,
                "w": 28,
                "x": 726,
                "y": 677
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "363d7636-904b-4568-a1f9-733af70e91f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 133,
                "offset": 0,
                "shift": 37,
                "w": 36,
                "x": 393,
                "y": 677
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f2f1f166-0071-4e71-a31b-95507df390bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 133,
                "offset": 1,
                "shift": 33,
                "w": 28,
                "x": 696,
                "y": 677
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6735b101-6b46-471d-a12e-4508b98e54b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 133,
                "offset": 1,
                "shift": 51,
                "w": 49,
                "x": 765,
                "y": 542
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "511fa753-7713-475a-8a0c-89ca32269dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 133,
                "offset": 0,
                "shift": 67,
                "w": 68,
                "x": 630,
                "y": 272
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f653372e-ab48-4760-81e8-eccad99b8e57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 133,
                "offset": 7,
                "shift": 44,
                "w": 23,
                "x": 785,
                "y": 677
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "90ecbb4d-1a72-4685-9fff-660017962815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 133,
                "offset": 4,
                "shift": 59,
                "w": 55,
                "x": 62,
                "y": 542
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6df9584c-9c4a-410f-a63b-6a367b0ecc24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 133,
                "offset": -1,
                "shift": 66,
                "w": 63,
                "x": 404,
                "y": 407
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "cbf212d6-0504-4e72-a842-9c49d2b06247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 133,
                "offset": 4,
                "shift": 59,
                "w": 51,
                "x": 506,
                "y": 542
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0cc2a115-b274-4819-bb55-b0f41875e2a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 133,
                "offset": 4,
                "shift": 67,
                "w": 63,
                "x": 339,
                "y": 407
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "24bb80a1-b098-4970-9c83-ecd50551c8a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 133,
                "offset": 4,
                "shift": 59,
                "w": 52,
                "x": 399,
                "y": 542
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "da4664e6-1426-4c43-bc08-73b439250e3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 133,
                "offset": 5,
                "shift": 44,
                "w": 53,
                "x": 290,
                "y": 542
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4451f81e-3e88-4178-b4e6-73f841b6508a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 133,
                "offset": 3,
                "shift": 67,
                "w": 62,
                "x": 729,
                "y": 407
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "69d881e4-6349-43a8-a7f4-8c9bf7cb7c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 133,
                "offset": 0,
                "shift": 67,
                "w": 66,
                "x": 2,
                "y": 407
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d3ce8d40-456c-481c-94c0-b1f3608b2f77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 133,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 570,
                "y": 677
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b186970e-af9a-4c6a-9103-32279cc31017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 133,
                "offset": -11,
                "shift": 37,
                "w": 37,
                "x": 315,
                "y": 677
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "916b7d68-197e-4596-bdcd-1e8582067769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 133,
                "offset": 1,
                "shift": 67,
                "w": 67,
                "x": 770,
                "y": 272
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "73d6cc49-ea94-486b-8fc0-82ad781c5dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 133,
                "offset": 3,
                "shift": 37,
                "w": 32,
                "x": 536,
                "y": 677
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "18fd1a41-5ce9-47f9-9a39-78fc370be62e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 133,
                "offset": 1,
                "shift": 103,
                "w": 102,
                "x": 360,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0eeae8b5-8083-4bba-adfa-4d530b2605b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 133,
                "offset": 0,
                "shift": 67,
                "w": 66,
                "x": 70,
                "y": 407
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "84bf3294-01d6-4add-bd06-3d38eb60cc1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 133,
                "offset": 4,
                "shift": 67,
                "w": 58,
                "x": 854,
                "y": 407
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "26febaae-5019-4e9f-95dd-758792ef012b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 133,
                "offset": -1,
                "shift": 66,
                "w": 63,
                "x": 469,
                "y": 407
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8da13ca7-6875-4350-b214-be55d960d889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 133,
                "offset": 4,
                "shift": 67,
                "w": 63,
                "x": 599,
                "y": 407
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e58d7ce7-47e3-40f5-9554-aa355af73d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 133,
                "offset": 0,
                "shift": 44,
                "w": 46,
                "x": 961,
                "y": 542
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "77b8cfb3-be99-4971-8b44-f7535d00e038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 133,
                "offset": 6,
                "shift": 52,
                "w": 42,
                "x": 191,
                "y": 677
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a5473662-e21c-4427-8392-f6b7da51aa75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 133,
                "offset": 1,
                "shift": 37,
                "w": 37,
                "x": 354,
                "y": 677
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "292cfaa1-5651-44a1-8a96-32878c49a2f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 133,
                "offset": 0,
                "shift": 67,
                "w": 67,
                "x": 839,
                "y": 272
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "61023539-4358-49b6-8f50-fc915ce0e06f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 133,
                "offset": 1,
                "shift": 67,
                "w": 65,
                "x": 206,
                "y": 407
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e9c87853-0769-4635-8f1b-f3cc719a448c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 133,
                "offset": 0,
                "shift": 96,
                "w": 95,
                "x": 661,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9ec79089-a92b-4229-a0b2-b1d396629af1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 133,
                "offset": 1,
                "shift": 67,
                "w": 64,
                "x": 273,
                "y": 407
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "40f4d93b-f180-4bc4-b6c5-d97174ab9bee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 133,
                "offset": 0,
                "shift": 67,
                "w": 66,
                "x": 138,
                "y": 407
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "01366069-895e-472b-b0bd-50b03fc6899b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 133,
                "offset": 2,
                "shift": 59,
                "w": 55,
                "x": 233,
                "y": 542
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ce3c7cd1-3633-469b-a17e-a520bb4d26cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 133,
                "offset": 14,
                "shift": 64,
                "w": 32,
                "x": 468,
                "y": 677
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f8851481-d5b3-4b98-83a0-1823fdb101df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 133,
                "offset": 4,
                "shift": 17,
                "w": 9,
                "x": 923,
                "y": 677
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8678a053-54cd-48fb-bcce-3043df8435da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 133,
                "offset": 18,
                "shift": 64,
                "w": 32,
                "x": 502,
                "y": 677
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "207c4259-a3ef-47c4-87ee-a91610c1b847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 133,
                "offset": 1,
                "shift": 72,
                "w": 70,
                "x": 558,
                "y": 272
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "85fab04f-91f3-4e6e-bc3a-9883145855e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 133,
                "offset": 2,
                "shift": 80,
                "w": 76,
                "x": 404,
                "y": 272
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 100,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}