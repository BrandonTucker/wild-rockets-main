{
    "id": "c0956fc2-5d66-4975-ab0e-7495d3241488",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font1",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "MV Boli",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c2ebbf77-67eb-4bb6-b2e6-63e4fe7ec4b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 44,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 311,
                "y": 43
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "386d7ff3-a536-4ea8-8181-f675c552a75d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 35,
                "offset": 3,
                "shift": 13,
                "w": 11,
                "x": 302,
                "y": 133
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f5f53bb8-0dfd-4101-9d31-d8e4ac9cee20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 14,
                "x": 387,
                "y": 133
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c9f41aa7-b263-4c16-bed6-020370203758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 35,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 119,
                "y": 43
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "931b6360-c7f4-4edc-a2b0-5ff9363df739",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 35,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 216,
                "y": 89
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4ad399df-fdd2-4080-9523-355a861c02ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 35,
                "offset": 2,
                "shift": 24,
                "w": 24,
                "x": 396,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f0e40043-a5dd-4d69-b25b-bce49808d602",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 35,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 370,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8722cfd2-950b-47c6-b6cb-abf261ab411c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 4,
                "shift": 8,
                "w": 7,
                "x": 436,
                "y": 133
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a198f544-7f30-4678-a27e-fc87436adb7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 42,
                "offset": 3,
                "shift": 19,
                "w": 18,
                "x": 251,
                "y": 43
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "13dd265a-8621-4ada-b149-cb827334d861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 42,
                "offset": -1,
                "shift": 19,
                "w": 18,
                "x": 291,
                "y": 43
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cb9eab38-87a9-4796-9bb5-944a85510fb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 3,
                "shift": 13,
                "w": 13,
                "x": 403,
                "y": 133
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7932a07f-f65f-48a9-a3da-46f3e492430c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 32,
                "offset": 3,
                "shift": 19,
                "w": 17,
                "x": 56,
                "y": 133
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "db9af7ec-db73-4b80-95e5-917ed2ed1934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 40,
                "offset": 2,
                "shift": 13,
                "w": 6,
                "x": 428,
                "y": 133
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fc0a4268-12aa-43e1-8a73-1f656e252512",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 373,
                "y": 133
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a706d33f-62c8-45c8-9df0-5d237c0fa605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 5,
                "shift": 13,
                "w": 5,
                "x": 445,
                "y": 133
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0b14a5df-9a4c-4607-beb6-2474b22739ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 35,
                "offset": 0,
                "shift": 18,
                "w": 21,
                "x": 468,
                "y": 43
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1aa8d427-2b32-4372-a145-be4e07111b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 35,
                "offset": 3,
                "shift": 24,
                "w": 21,
                "x": 445,
                "y": 43
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c70e7327-43a5-4e5b-808c-2e63ffdcaa01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 35,
                "offset": 4,
                "shift": 14,
                "w": 12,
                "x": 255,
                "y": 133
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "52c120c5-8d83-4270-afce-4443bcc326fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 35,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 353,
                "y": 43
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "edce0b93-bb99-4c50-bc5a-663a0276465b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 35,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 399,
                "y": 43
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0605ec36-82c5-4f4e-8072-e952d225013e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 35,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 71,
                "y": 89
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2a60471c-8d0d-4dc5-89ab-01a02bc0ba2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 35,
                "offset": 1,
                "shift": 23,
                "w": 23,
                "x": 2,
                "y": 43
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5a5cf8ed-5e05-42aa-9336-ca9cd6b48607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 35,
                "offset": 3,
                "shift": 23,
                "w": 22,
                "x": 167,
                "y": 43
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "3e989051-3b0a-434b-b4c7-724e249c06ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 35,
                "offset": 5,
                "shift": 21,
                "w": 19,
                "x": 195,
                "y": 89
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5afd1118-d025-43e1-9724-606304ba3a72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 35,
                "offset": 1,
                "shift": 23,
                "w": 25,
                "x": 266,
                "y": 2
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "87623c7b-1302-45d1-974d-9498872ca3be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 35,
                "offset": 4,
                "shift": 23,
                "w": 19,
                "x": 174,
                "y": 89
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1098c35d-8196-4400-ac78-fcdcb23dbde3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 35,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 418,
                "y": 133
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3b638ffe-b489-43b1-865f-e0978cc5039b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 40,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 315,
                "y": 133
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d011d874-03e4-458e-bf7c-980c3e091b4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 3,
                "shift": 19,
                "w": 18,
                "x": 95,
                "y": 133
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "cf8724c1-0396-47df-87e8-314345bf0d6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 29,
                "offset": 3,
                "shift": 19,
                "w": 17,
                "x": 191,
                "y": 133
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "866ab4ba-30ba-4433-ba19-d56793e06651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 75,
                "y": 133
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1499075a-7e5a-42e1-8b87-d88bedd131f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 35,
                "offset": 5,
                "shift": 19,
                "w": 17,
                "x": 392,
                "y": 89
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "36ac6c11-3615-4472-b7f2-994fa3fd32bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 4,
                "shift": 30,
                "w": 28,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "85f764cb-656f-4a58-8fc0-2aebf047297d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 35,
                "offset": 3,
                "shift": 24,
                "w": 21,
                "x": 48,
                "y": 89
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1b73bdfb-3149-4aa1-ba29-c7aee1c5f82d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 35,
                "offset": 3,
                "shift": 21,
                "w": 21,
                "x": 376,
                "y": 43
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b2a7b078-d29f-496c-af7b-8560240001d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 34,
                "offset": 4,
                "shift": 21,
                "w": 18,
                "x": 334,
                "y": 89
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "217b0e69-b5a0-4143-9412-b84dedb50d49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 35,
                "offset": 2,
                "shift": 24,
                "w": 24,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b1c12b0f-28e6-46c9-ab2f-4511a7d6646a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 35,
                "offset": 3,
                "shift": 21,
                "w": 23,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "cddc3aa1-2342-4968-a81e-e8e7ecfa0127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 35,
                "offset": 4,
                "shift": 19,
                "w": 20,
                "x": 115,
                "y": 89
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "85f63e27-420f-446f-b10d-41400dc888b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 35,
                "offset": 4,
                "shift": 23,
                "w": 21,
                "x": 422,
                "y": 43
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "167c3531-a54e-429b-a32a-17552c82b9e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 35,
                "offset": 3,
                "shift": 25,
                "w": 25,
                "x": 293,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "25521ff3-7017-4684-9c27-25f79ffed008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 35,
                "offset": 4,
                "shift": 11,
                "w": 10,
                "x": 338,
                "y": 133
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "8753941c-2e04-4433-8421-1c8ef4a588e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 27,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "951cb586-9e0e-4a3a-9550-06e8d860db3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 35,
                "offset": 4,
                "shift": 24,
                "w": 21,
                "x": 330,
                "y": 43
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d8d566aa-3cab-4712-8abd-75703c5c1794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 35,
                "offset": 4,
                "shift": 21,
                "w": 18,
                "x": 274,
                "y": 89
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b0e4402b-806c-458e-906f-b687a925a200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 2,
                "shift": 32,
                "w": 32,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fff0dad9-4dc0-471e-97c7-8f2834959676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 35,
                "offset": 3,
                "shift": 27,
                "w": 27,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "81a11336-39cc-435b-8724-7e918f9faa50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 35,
                "offset": 3,
                "shift": 25,
                "w": 24,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0023d5b1-50de-4fc3-a6e2-bced93344676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 35,
                "offset": 3,
                "shift": 23,
                "w": 24,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "51649b52-3e79-46e7-a428-361f6f75c686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 39,
                "offset": 4,
                "shift": 24,
                "w": 23,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "eb3e6ba6-18dc-49ac-aded-7b80a071e07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 35,
                "offset": 4,
                "shift": 24,
                "w": 21,
                "x": 25,
                "y": 89
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2ea7d973-1e20-4798-bcb1-06ed792dbaaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 35,
                "offset": 3,
                "shift": 21,
                "w": 20,
                "x": 93,
                "y": 89
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e96a33df-35fd-4b60-8a98-66838b5f092e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 34,
                "offset": 6,
                "shift": 22,
                "w": 23,
                "x": 94,
                "y": 43
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9d2bbf30-c997-4303-ad53-9eb4bbe1b8b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 35,
                "offset": 5,
                "shift": 24,
                "w": 22,
                "x": 143,
                "y": 43
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7c41598c-ddfb-4a10-88ea-c77c6bb029a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 35,
                "offset": 5,
                "shift": 22,
                "w": 21,
                "x": 2,
                "y": 89
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a5913ab3-a327-4402-a9a2-6aef6d2cf9fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 35,
                "offset": 4,
                "shift": 32,
                "w": 30,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3bc62e29-8d32-4ee4-ba2d-9e4d89de911c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 27,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d0083709-3950-4475-9d3a-60865f186adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 39,
                "offset": 5,
                "shift": 23,
                "w": 22,
                "x": 320,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "bfff8338-4d29-4199-8aed-bf4b9e9ad5ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 35,
                "offset": 3,
                "shift": 28,
                "w": 26,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "dacb690f-494b-4867-bd63-519c879dd19e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 42,
                "offset": 1,
                "shift": 15,
                "w": 18,
                "x": 231,
                "y": 43
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bb50d72a-1386-460c-b0d0-1bc4da59ce03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 35,
                "offset": 5,
                "shift": 18,
                "w": 10,
                "x": 350,
                "y": 133
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2f9bd524-c6f3-4d81-b42b-c17360213e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 42,
                "offset": -2,
                "shift": 15,
                "w": 18,
                "x": 191,
                "y": 43
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e71d0df8-2486-4b0f-961c-296ff87683da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 23,
                "offset": 4,
                "shift": 17,
                "w": 17,
                "x": 283,
                "y": 133
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "067b879d-f377-41a6-8d7a-0eb6864f9074",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 41,
                "offset": -1,
                "shift": 17,
                "w": 17,
                "x": 137,
                "y": 89
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2af2bf52-545c-4612-b402-a6d64e36e258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 11,
                "shift": 18,
                "w": 7,
                "x": 452,
                "y": 133
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ce7e95e2-6db7-401f-8ecf-75b6d7b430b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 15,
                "x": 157,
                "y": 133
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d265c759-afa8-4695-8d7f-9a523ad0aaeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 35,
                "offset": 3,
                "shift": 19,
                "w": 17,
                "x": 354,
                "y": 89
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9cab7f92-6e5d-4d43-b1db-953864f6910d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 35,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 269,
                "y": 133
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8ba610d0-abcb-4500-8202-1838e0bc8f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 35,
                "offset": 3,
                "shift": 19,
                "w": 18,
                "x": 314,
                "y": 89
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2adfbdde-22f1-47ce-ae9a-cc0cc207c82e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 210,
                "y": 133
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7c793499-bb66-4762-9588-2fc748c79560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 35,
                "offset": 3,
                "shift": 14,
                "w": 18,
                "x": 237,
                "y": 89
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "71467816-7487-4d6f-b38f-0b377e87f7a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 42,
                "offset": 3,
                "shift": 17,
                "w": 15,
                "x": 257,
                "y": 89
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7c83d05d-6476-430e-98c3-b1029432c80f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 35,
                "offset": 3,
                "shift": 19,
                "w": 16,
                "x": 482,
                "y": 89
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "bab4bbf0-f6b5-4305-a4b1-9f01be6d6768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 35,
                "offset": 3,
                "shift": 9,
                "w": 9,
                "x": 362,
                "y": 133
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e8b9cdb2-9282-4425-8504-fd14970a0f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 42,
                "offset": -3,
                "shift": 9,
                "w": 14,
                "x": 430,
                "y": 89
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "476892ce-d37a-4b22-b185-5f0ee6f1ad0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 133
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3f1df041-77df-4594-9a45-edbc77b89fb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 35,
                "offset": 3,
                "shift": 9,
                "w": 10,
                "x": 326,
                "y": 133
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0fecaebe-bf86-443a-8493-d497695452aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 35,
                "offset": 3,
                "shift": 29,
                "w": 26,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0bfd08ed-2667-4ba7-b168-dff49f97c889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 35,
                "offset": 3,
                "shift": 19,
                "w": 17,
                "x": 373,
                "y": 89
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d5ac9d86-43d5-43ce-9470-be02d35ce8df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 35,
                "offset": 3,
                "shift": 18,
                "w": 16,
                "x": 446,
                "y": 89
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "95f07174-2bfd-4917-ba22-556cf0561f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 42,
                "offset": 2,
                "shift": 20,
                "w": 19,
                "x": 73,
                "y": 43
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5ae8fc1d-373d-4cb6-8bf1-7538d0c3f00b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 42,
                "offset": 3,
                "shift": 18,
                "w": 16,
                "x": 156,
                "y": 89
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e6b5ab50-8cd2-4a96-85da-7d0302353f0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 35,
                "offset": 3,
                "shift": 15,
                "w": 15,
                "x": 174,
                "y": 133
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9460e977-fdd5-4681-b369-1a142645014c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 35,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 240,
                "y": 133
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "dc4c495c-3e17-4b8c-8dad-775cadc2d62f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 35,
                "offset": 3,
                "shift": 18,
                "w": 16,
                "x": 464,
                "y": 89
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "69898584-1d66-47b7-8e00-1025ebe47d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 35,
                "offset": 4,
                "shift": 19,
                "w": 16,
                "x": 38,
                "y": 133
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f2b6add1-d3e5-4dfa-9ccf-6d7ed6199781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 35,
                "offset": 4,
                "shift": 17,
                "w": 16,
                "x": 20,
                "y": 133
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "862ede11-ccd8-4e6c-a018-1581096f36b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 35,
                "offset": 4,
                "shift": 25,
                "w": 23,
                "x": 27,
                "y": 43
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "23fb8c3a-d268-445a-be49-373b08a33b28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 18,
                "x": 294,
                "y": 89
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8452d7d8-83e4-4190-a05d-fbf5d08d076f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 42,
                "offset": 2,
                "shift": 18,
                "w": 18,
                "x": 211,
                "y": 43
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1fbbec6d-0a12-4995-9c9d-4a24bad4bb80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 411,
                "y": 89
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "13080169-5c08-4123-a89b-d5b6802f7b99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 42,
                "offset": 2,
                "shift": 16,
                "w": 18,
                "x": 271,
                "y": 43
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9d5c458a-6bfc-4be6-9554-9411a0a2910a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 40,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 226,
                "y": 133
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9d2f73f5-e467-4fb9-ae13-1b547365447f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 42,
                "offset": -3,
                "shift": 16,
                "w": 19,
                "x": 52,
                "y": 43
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "282852b4-81c4-41de-9940-eca63dc5b389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 4,
                "shift": 21,
                "w": 19,
                "x": 136,
                "y": 133
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "f37b9f35-752c-4eb1-aa23-55825c58db38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 28,
                "offset": 4,
                "shift": 21,
                "w": 19,
                "x": 115,
                "y": 133
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 25,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}