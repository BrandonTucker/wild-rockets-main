{
    "id": "e2cb6771-c601-470b-99ef-3ed49c5c1f01",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_Credits",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6ef10874-2c52-4ca2-aecb-6a6f8a1dd7aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 456,
                "y": 254
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e72acc9e-f5aa-4fa1-96fa-e333bae453c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 61,
                "offset": 4,
                "shift": 18,
                "w": 9,
                "x": 124,
                "y": 317
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3d00b2bf-c838-4393-be70-c4e3576a2cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 61,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 253,
                "y": 254
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "bd120655-e7ce-41dd-8d19-f726250aff05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 254,
                "y": 128
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d7c2c6db-cd03-46fe-8225-e142c2aca9f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 118,
                "y": 191
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bd38904d-ba04-4898-8c8f-a9b57e3d509d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 61,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "65bd516f-7ccd-4d43-86a0-d05e753e0153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 61,
                "offset": 2,
                "shift": 38,
                "w": 36,
                "x": 79,
                "y": 65
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e3b1a28f-1d1b-4d0f-8fe9-786890f5b4ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 61,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 91,
                "y": 317
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f20f5c34-3214-4f6d-9506-f468dcd7db36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 61,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 490,
                "y": 254
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f5dfccdd-55f2-4041-ab3e-884fd04e8895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 14,
                "x": 34,
                "y": 317
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "abeb34ab-0677-40c9-9885-e9f1c5730375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 61,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 276,
                "y": 254
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "43ae0b9d-c0ed-4a31-b0d8-8b21820984a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 89,
                "y": 191
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3c1f51f6-af44-4bb0-bc6e-7004ddc53ee3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 157,
                "y": 317
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "923c903f-f665-4a6b-ad5e-c6142a83579e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 61,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 473,
                "y": 254
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "bbcb6d06-8c42-47f3-b4d0-c03ac992c308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 102,
                "y": 317
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "675ea056-becc-48c3-a7d0-a17116b150a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 61,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 420,
                "y": 254
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a38297f6-c130-4ace-9f15-40da893b9c02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 226,
                "y": 254
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "17ee9e00-8366-449b-9a57-364290939675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 61,
                "offset": 4,
                "shift": 29,
                "w": 17,
                "x": 401,
                "y": 254
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "12c2fd97-8854-4078-956f-2aefb581dd5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 198,
                "y": 254
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3d889384-e10f-4b10-9607-168b49a9383a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 170,
                "y": 254
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0c187ad8-9562-4f77-addd-3af5bb3d2dc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 285,
                "y": 128
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "22d08dd3-392a-4403-822d-a51df35132dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 2,
                "y": 254
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fa49a8e2-5f3a-4e3d-94b4-19d11e0b3d21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 142,
                "y": 254
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2ad7ecda-c61d-40cc-b21b-7ef98b32834a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 58,
                "y": 254
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3fe85e95-a83c-4200-a08c-12110c059301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 405,
                "y": 191
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "40968fb7-004e-4fc3-abc6-853ab72d5e3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 263,
                "y": 191
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "736ae15c-b31a-4485-b857-844c80a0ea09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 61,
                "offset": 5,
                "shift": 18,
                "w": 8,
                "x": 167,
                "y": 317
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "54d21a6c-61f0-448e-9fa9-41a4cb2983b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 61,
                "offset": 4,
                "shift": 18,
                "w": 9,
                "x": 135,
                "y": 317
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e08cea1f-0e2e-4aeb-91e7-5f53a1f81396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 205,
                "y": 191
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b584c530-310d-4f74-af87-3e2795e99955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 176,
                "y": 191
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bd1d0acc-6316-4619-a38b-b523b9d9854d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 147,
                "y": 191
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3cd42ec8-cf24-48bf-b4c1-5a04041bf3c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 436,
                "y": 128
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2645ab1f-e553-4d85-9959-e6799639ddbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 61,
                "offset": 1,
                "shift": 52,
                "w": 51,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5afae0a4-1aae-4d4b-998f-bb194315d711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 61,
                "offset": 0,
                "shift": 38,
                "w": 39,
                "x": 280,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "04ee8565-8574-4d07-a927-bab46b3e689b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 228,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "49b059af-f8d0-4dde-8cb6-61d5086da7f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 61,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 192,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7d19ec28-3a1c-4717-a095-a7d5839e6dec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 263,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2cabc09a-05da-4cfc-841a-573428594089",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 61,
                "offset": 3,
                "shift": 35,
                "w": 30,
                "x": 35,
                "y": 128
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9ce82fba-d0df-4566-b785-cdda64599aeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 234,
                "y": 191
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cf3d565c-a3a3-4522-afcf-1cb9191f2b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "01768168-2fdf-46c4-b0e3-75d6276dc98d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 434,
                "y": 65
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2b97681d-0d91-450f-9483-ec2de4d09b97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 113,
                "y": 317
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d192b4a1-f546-495c-a39d-41084eb2d114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 26,
                "x": 433,
                "y": 191
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1f689f46-b48b-4ed4-a0f7-1e245bb15ab8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 36,
                "x": 117,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c8e667b2-86c2-4c1b-a26b-044f1c78a224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 61,
                "offset": 4,
                "shift": 32,
                "w": 27,
                "x": 292,
                "y": 191
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "755df325-225a-48bb-9386-2d72eda4cecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 61,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 321,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d029ad5e-dbb1-43c4-b4b9-4407674d839f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 400,
                "y": 65
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d023252e-88b6-4231-85e0-7eb37b1eb078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 38,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9d49d428-344d-4b3b-a573-d464e6ec6cce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 61,
                "offset": 3,
                "shift": 35,
                "w": 30,
                "x": 67,
                "y": 128
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d3086408-a6ba-480e-9fb7-cca69ec5209d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 39,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5589bb2d-b416-4883-9b65-4e39578fc1aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 35,
                "x": 155,
                "y": 65
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6e0b33a8-ff30-4ebc-b4bb-16a3fc0bea42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 61,
                "offset": 1,
                "shift": 35,
                "w": 32,
                "x": 366,
                "y": 65
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "33b3ff57-0116-4fce-8d20-29066a6f3c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 61,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 2,
                "y": 128
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b195873e-a779-4c14-b217-e26dd0315061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 61,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 298,
                "y": 65
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5b71a634-e979-4738-85bd-6cd07536f70a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 61,
                "offset": -1,
                "shift": 35,
                "w": 37,
                "x": 440,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7d4cb07d-e988-4438-9b22-1fb8e0a04f22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 61,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f125e414-82ad-4b07-8cd1-7953f0e161bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 61,
                "offset": 0,
                "shift": 35,
                "w": 36,
                "x": 41,
                "y": 65
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8336e05d-38e2-42e2-a167-168fe7fbe083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 61,
                "offset": -1,
                "shift": 35,
                "w": 37,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d9145f1d-e461-49f2-90c0-982fd52144bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 61,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 332,
                "y": 65
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "05ccd55f-b29f-49da-952e-c5e7163b3a73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 61,
                "offset": 3,
                "shift": 18,
                "w": 14,
                "x": 2,
                "y": 317
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "079d6f80-f7b6-4b2b-9fce-728d453942fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 61,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 438,
                "y": 254
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "15d3efff-3893-4e78-a255-2c1be8e3ff30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 61,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 18,
                "y": 317
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "063785a0-0f32-4246-91dc-edc4c1be51aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 461,
                "y": 191
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ea5662ce-3d52-41f5-a8a0-33db7e8bc45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 61,
                "offset": -1,
                "shift": 29,
                "w": 31,
                "x": 468,
                "y": 65
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ece0df5d-aa22-48d5-9b21-5eed2a886444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 12,
                "x": 66,
                "y": 317
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b9dfa456-f69e-47f1-a6a6-e5493942ae27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 60,
                "y": 191
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1ff1c2ce-c23b-408f-9873-98e935875b4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 28,
                "x": 406,
                "y": 128
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a5cbb96d-61a5-4db5-a8e2-bcae3c38faf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 27,
                "x": 31,
                "y": 191
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "027cfe6b-b062-4ebe-8fbc-53bcdfe621ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 376,
                "y": 128
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1daa668a-2717-4775-b309-69fbcec10cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 2,
                "y": 191
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "90796161-a670-4d3c-8176-50cee1c18075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 61,
                "offset": 0,
                "shift": 18,
                "w": 20,
                "x": 298,
                "y": 254
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a39b7e89-388c-42b7-afae-0b050a17eb8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 27,
                "x": 466,
                "y": 128
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "253f4e07-2196-4bdb-9dcb-def0dae5ca1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 349,
                "y": 191
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b9117f3a-1258-40a1-ab29-94b63385506a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 80,
                "y": 317
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5992bb75-6d83-4579-846f-3dacc10130bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 61,
                "offset": -3,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 317
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "90175f68-6c45-46fc-9bef-bc8ee907bcbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 26,
                "x": 114,
                "y": 254
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "278e63dc-1088-4a53-aab8-25a98ea981b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 146,
                "y": 317
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e5691594-512d-4bdf-8c69-5e484b20ba3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 61,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d8def819-d613-4218-b97e-bfa972ee15b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 30,
                "y": 254
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "fd136628-7e43-42b2-8ef9-8d2208625bd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 99,
                "y": 128
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9664a97b-502f-4c4f-8a79-294c35e51237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 28,
                "x": 316,
                "y": 128
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "885444eb-7096-4fd7-a5fd-cfa0e038107d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 61,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 346,
                "y": 128
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f3d12a78-2ebe-4dfe-9e08-4637be3e3104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 61,
                "offset": 3,
                "shift": 21,
                "w": 19,
                "x": 320,
                "y": 254
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a95c102e-ac2b-46e9-bd75-62d87e562676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 321,
                "y": 191
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1d130ade-1d45-4c9b-b899-3b26d44a3b68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 61,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 382,
                "y": 254
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3180fa80-2d3f-4023-aaf3-315c28bd626b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 61,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 86,
                "y": 254
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "fece57eb-eb38-4338-b244-11dcfb7a7b03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 161,
                "y": 128
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "39b0aef8-2038-4898-8d74-68bb06c07aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 61,
                "offset": 0,
                "shift": 41,
                "w": 42,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1f28ddca-94cb-4f5a-82b8-646a9a2b3bdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 130,
                "y": 128
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b9663d48-7405-4602-b780-06acd732e769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 192,
                "y": 128
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ed32deef-b3c9-4171-bb5e-e5f191982c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 61,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 377,
                "y": 191
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c00acade-275f-46a7-8461-8b68a9c6945f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 61,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 341,
                "y": 254
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7a92fedf-bfef-4bb3-b4ea-5305179b0c02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 177,
                "y": 317
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "25f60596-dc00-4a77-a80b-49cb0cec1a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 61,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 362,
                "y": 254
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6c82c4f4-eb17-413f-90a8-807e56275ba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 61,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 223,
                "y": 128
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "2ceecb2e-7b1d-483e-9ab0-a48d13902224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "c64fde0c-51e4-40c4-b9b1-c06666e362f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "d0f76229-823f-4d8d-a91c-d457d777bdc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "0e2c6211-8ad8-455f-bf1a-80c58bc76146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "38648e21-1e09-41e9-9dc8-506665802fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "bcdab30e-dccb-4dfb-9003-ead380b84669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "08a2b816-9217-411e-af42-a0b74e3edee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "8210ea2f-c274-495c-a6d3-7fac7ce7ed4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "04b0fe75-d259-4d96-b31e-36c909e4e4e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 49
        },
        {
            "id": "6e929deb-3e7a-415f-ae78-79b7a0a6c530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "336b58f0-a340-42be-ab3f-ef4a7c2cc7a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "8ed409d3-b904-4e18-a89f-a8bbbb2ad1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "784821f8-15a5-4fdf-bcd8-f56b21e7398c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 87
        },
        {
            "id": "ed80f384-2f78-403d-91ec-5a2c3b981fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 89
        },
        {
            "id": "45f36f1b-3adf-4f1c-b4b5-03e09cf1e3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "37ae297f-efd4-4806-a1af-888eda0c08ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "9ebd9153-afd0-42d4-a25c-e6f35b908809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 121
        },
        {
            "id": "4916901c-21eb-4155-805f-7d90dc6c1259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "3f186b5b-8194-49ba-9306-5116c3679654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "a9cfb99b-cd8f-4c5d-862f-b1f73108369b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 44
        },
        {
            "id": "432c0eae-8d82-4350-becc-a63bfb37c481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 46
        },
        {
            "id": "6880fe44-bc7f-45dd-9a58-7ef41b53e889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "23676ca4-7f2a-4f3b-a7a1-07b9406d9ecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "3f465f58-f72a-4f42-a649-27a817175d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "ce6be4b1-8319-4227-a8a2-86576fa6448a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "58c80276-d7e8-4061-bc06-00f0aa24d935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "21be2f3e-f4c7-47b0-8197-000745addcd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 89
        },
        {
            "id": "b0d8707c-1b3a-450a-90aa-679805ab00e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "6d511718-5e5f-470a-b33c-353bcc471725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "e076fb63-7ca9-4f10-87c0-6e93b1d39ec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "9234e9ac-3baf-424e-8e81-fed006624007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "328bb4b3-9382-4ff7-be6b-4e698c3ccfe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 44
        },
        {
            "id": "ebbf107d-3745-4969-ae2e-67d704ee86e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 46
        },
        {
            "id": "a34bef13-b05d-43e2-b2f2-ef567c93cbd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "0fd0992e-7ac9-45e8-bb28-ccffc8f6b6fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "f4054566-7c26-4bfc-adff-fffd0c0b7804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "534400b0-7314-4fc4-b815-cb87cbc63b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "8b3a39b5-f971-488f-8ce7-65f23ad3460a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "221d4cc4-fd4d-4172-b015-45c39007f116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 44
        },
        {
            "id": "c9516b89-508f-45a5-8174-6419bc07c4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "c11c86eb-deea-4ef6-a6d3-fbd7511f3134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 46
        },
        {
            "id": "4462fbfd-a160-4d43-8b73-0f4ff416685f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 58
        },
        {
            "id": "4a1a98a5-9d24-4596-826e-e2bef61a84af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 59
        },
        {
            "id": "7114ed3a-5dea-4703-8cad-f2159ac8496a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "91cd76b1-0559-4535-a8f8-caaeb567948d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "4fc98773-b23a-44d9-8443-92eb82853c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "33c2e802-88e6-45e4-a39e-758e31e12c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "f160f94d-1c28-4ac1-b6a9-9b0b6e8a0c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "c44b7dd5-4daf-471b-bb83-5e5cd44fe4be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "5e0d11dc-9324-4854-ac9d-1f91724a0fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "2b2022aa-1122-47bd-9839-758ce562498e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 114
        },
        {
            "id": "06135285-069a-4dec-b07a-f569d561e736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "8ceb2a54-5ebb-46f7-830e-74ba68111732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 117
        },
        {
            "id": "9bf8f6f7-4294-48e6-98b2-29aa232102a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 119
        },
        {
            "id": "ec4946f8-8273-47b7-a2e2-96e3decfc295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 121
        },
        {
            "id": "cbe1dbf2-2951-4c4f-9f31-9a1540d85b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "950660a7-73b6-4dec-92bc-06400ecbac40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 894
        },
        {
            "id": "2bb8e4c9-53b4-4e18-90fe-d6d8f0011492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 44
        },
        {
            "id": "b6c02139-a22c-4ecf-a3a6-acb599944d8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "c6cb944c-c7dd-430f-b6d9-00fd177629d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 46
        },
        {
            "id": "4ddd1ad5-3b5f-4d3b-9018-76795e02d360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 58
        },
        {
            "id": "192d648e-6740-4cc2-8289-c89fca5f3979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 59
        },
        {
            "id": "9ecfb9d9-c611-4d0c-980e-3973b9420227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "52ebf649-7bc0-4658-85c9-1074af81d0d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "22b40f69-95f0-4340-b81e-62f4f9db1bea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "025e2c1f-dcf8-4467-bcd5-f50d7fce9536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "7ef4e79c-f17b-480a-8cdb-37d75f0b3fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 111
        },
        {
            "id": "7808323b-777d-46a5-a1fb-fbba2dabedaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 114
        },
        {
            "id": "d312185e-0a8c-4048-a930-5f7d530be0d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "f1af5ebe-d928-41e6-aff1-d8e68d049284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "d945275e-bf9c-4fcb-ac7c-fa49ddc458fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 173
        },
        {
            "id": "7875acc5-1845-48ec-8729-5a96b226c9de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 894
        },
        {
            "id": "3a828d8d-1c66-49d7-a1c3-65c3bd0f2af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "d271943c-f38e-4301-99ad-06aec317cf49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "f7f2bd14-e2d4-4fbc-aff0-de3db3e20ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "debcc469-c6a5-498d-a0df-afc30a2d09f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "8668caa8-7411-42b2-a9e0-d20ad87498d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "f27d7dba-e75a-4d21-8256-cf22a8d2ba96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 65
        },
        {
            "id": "412f0886-ba6d-477a-8536-6cfafd19ffd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "d5c7c9b9-f8bf-40b1-b91f-15f0db5ce55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "2e6cce8b-cdfa-4085-af57-8fb1581b1def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "8da18167-f6b4-45a5-baa5-24e3793e6c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "0e7a4a47-24c4-4cb6-a010-182078fe3783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "90325cd6-b562-45e2-ada1-f8115de3d55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "0ac7d0df-a9bd-4a2e-a6ce-ac706d6d0af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "a148c758-cf8b-48f4-aace-5a2a1d6fd9cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "5c539114-a2de-43d1-9b1d-1de89cc38e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "504d8426-78ea-43de-8f78-a8775c283986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 44
        },
        {
            "id": "29adc56e-bf05-4268-a7dd-e83b6e3c929c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "8b0b6707-bc20-4647-b53a-72fd51e66249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 46
        },
        {
            "id": "1e92c858-21bd-4627-91f1-484ed1cfde9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 58
        },
        {
            "id": "1064dfb8-6c0f-48f6-a76d-278049347610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 59
        },
        {
            "id": "4df083ab-6cca-413a-a5b9-e85e26cf7a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 65
        },
        {
            "id": "771196d5-ac35-454e-8a96-0c64de18f0d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "5427f06e-c264-4a8b-91bf-995e526c7911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "5c586b19-cfe1-46ba-88aa-67408970871a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "3e0a0912-70a0-4ccb-bcc4-5a2ec9c694b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "0e3af83c-9125-4516-9678-4bfc2b1cab97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 112
        },
        {
            "id": "425d52ce-fdf7-4c6f-9e88-9af1553ed373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "fd323960-1e7e-4433-9583-ec84542e29f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "a1f418a7-555e-43f1-81c9-fc1e6cbd074e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 118
        },
        {
            "id": "07a7f4a4-2947-4263-9e1a-f5c0507cef06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "64898796-3c57-4564-958f-ae7bdd371a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 173
        },
        {
            "id": "58c53e2a-a245-4fa1-9a50-b2e33b4d0cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 894
        },
        {
            "id": "dfd8e72c-c050-4c43-9cc9-119b28c7f894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "261866e0-e63e-4212-a26c-a92a036b109f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "1c17b9cc-079c-4e21-afc9-f5cebd403505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "92a06b9e-f7eb-4428-b770-4f882b200d4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "acaa13f6-658f-4294-a042-58ff6a3c912c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "490c91ec-9be6-46cf-925e-4b4862c5403b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "bee30a2e-7c0a-4ccd-b691-8aeaed37e082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "48ff76f3-d86d-4c5d-ae51-5ee7ea66f65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "5f7ea0e1-4f27-435c-9385-5219dac418ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "bd620df9-4b1f-4a19-9bf6-c76c13b1192f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 40,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}