{
    "id": "8dcdf573-c007-411b-827e-4d75769193de",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fontcombo",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "David",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "008589e0-0bf1-4c7f-8b73-e426b921d590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 93,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 968,
                "y": 287
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "89141e98-f825-416e-a063-038f3970b945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 93,
                "offset": 4,
                "shift": 28,
                "w": 12,
                "x": 99,
                "y": 382
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "83233f8a-6017-418c-8aa6-89e234374eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 93,
                "offset": 5,
                "shift": 41,
                "w": 32,
                "x": 549,
                "y": 287
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a13e9089-da96-44b6-b46f-8c2201e2c391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 93,
                "offset": 0,
                "shift": 39,
                "w": 38,
                "x": 2,
                "y": 287
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1bd5753a-b5f9-41b6-a6a8-04355635e479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 93,
                "offset": 3,
                "shift": 41,
                "w": 34,
                "x": 339,
                "y": 287
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "53bc6f5b-fee7-4129-a70b-ad90df823cf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 93,
                "offset": 5,
                "shift": 72,
                "w": 62,
                "x": 131,
                "y": 97
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e8c0ae1e-6e12-4972-a530-fcf1aa7b25b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 93,
                "offset": 2,
                "shift": 60,
                "w": 56,
                "x": 321,
                "y": 97
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9174e05d-e920-4423-aee2-92c34ea3901e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 93,
                "offset": 4,
                "shift": 21,
                "w": 12,
                "x": 113,
                "y": 382
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4974d186-8b6a-4909-b113-0870ec615c40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 93,
                "offset": 4,
                "shift": 23,
                "w": 21,
                "x": 945,
                "y": 287
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3db19e03-23d6-403d-ae97-862ac9024ca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 93,
                "offset": -1,
                "shift": 23,
                "w": 20,
                "x": 24,
                "y": 382
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4a9a7d62-2c52-4757-99bd-d24bf67b633d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 93,
                "offset": 5,
                "shift": 33,
                "w": 24,
                "x": 823,
                "y": 287
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b9b0aefe-f442-4c93-96f4-1ba1f0eb1ad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 93,
                "offset": 7,
                "shift": 57,
                "w": 44,
                "x": 480,
                "y": 192
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "930a0780-f62a-4231-841c-cf8047dba612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 93,
                "offset": 5,
                "shift": 23,
                "w": 12,
                "x": 127,
                "y": 382
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c42f477a-3c60-400d-b6d4-0d14a599608c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 93,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 710,
                "y": 287
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b499c6a2-0adb-4480-b66c-dfc3d40ffc44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 93,
                "offset": 5,
                "shift": 23,
                "w": 12,
                "x": 141,
                "y": 382
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "780a0360-9b38-4a1c-9c47-282aff0e869e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 93,
                "offset": -1,
                "shift": 31,
                "w": 33,
                "x": 480,
                "y": 287
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7db6ef23-df09-4e95-b855-e7c2849735ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 93,
                "offset": 3,
                "shift": 41,
                "w": 36,
                "x": 80,
                "y": 287
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c9ff91ef-7553-4685-8cdc-3a223cf8887a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 93,
                "offset": 5,
                "shift": 41,
                "w": 31,
                "x": 617,
                "y": 287
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "04709c8c-b749-4b3c-8c40-de04b6e91462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 93,
                "offset": 3,
                "shift": 41,
                "w": 33,
                "x": 445,
                "y": 287
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ef936c07-608d-4145-87e2-ba866bbd504e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 93,
                "offset": 5,
                "shift": 41,
                "w": 33,
                "x": 410,
                "y": 287
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d8042cea-4606-4729-9140-4e32658346b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 93,
                "offset": 2,
                "shift": 41,
                "w": 39,
                "x": 918,
                "y": 192
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "38c68ead-1161-412f-97b8-86a63bca33db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 93,
                "offset": 5,
                "shift": 41,
                "w": 32,
                "x": 583,
                "y": 287
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "69b5dd39-210b-4d19-8b11-e77f5851e8b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 93,
                "offset": 4,
                "shift": 41,
                "w": 35,
                "x": 156,
                "y": 287
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a2673275-613c-4b61-9d82-33d6c47f5d48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 93,
                "offset": 4,
                "shift": 41,
                "w": 35,
                "x": 230,
                "y": 287
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d19b957e-f042-45cd-9694-b074eb9e3c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 93,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 375,
                "y": 287
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "eb8a4223-a4f8-47ce-9b71-ef3d88aba39e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 93,
                "offset": 4,
                "shift": 41,
                "w": 34,
                "x": 267,
                "y": 287
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "73e65922-d938-4f24-9a61-51bc891bc036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 93,
                "offset": 6,
                "shift": 36,
                "w": 12,
                "x": 85,
                "y": 382
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8d80c4f2-f889-4663-bc60-e3b719c0a3ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 93,
                "offset": 6,
                "shift": 36,
                "w": 12,
                "x": 155,
                "y": 382
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "01498cc0-9467-49e5-b531-4e3c19e563fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 93,
                "offset": 6,
                "shift": 52,
                "w": 41,
                "x": 707,
                "y": 192
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "40ddc879-15ea-40d9-aea1-37318629a50c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 93,
                "offset": 7,
                "shift": 57,
                "w": 44,
                "x": 572,
                "y": 192
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "cd4f963c-bfab-4c53-b87b-5c21c3f5e004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 93,
                "offset": 6,
                "shift": 52,
                "w": 41,
                "x": 750,
                "y": 192
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6a06c31e-1d97-4da1-811d-c725207f2959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 93,
                "offset": 7,
                "shift": 33,
                "w": 19,
                "x": 46,
                "y": 382
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e880b253-3550-446d-9ece-6d5e8da5bb4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 93,
                "offset": 4,
                "shift": 86,
                "w": 80,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "65394c76-96f6-4cc6-bd30-3bf12fe6b4e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 93,
                "offset": 0,
                "shift": 67,
                "w": 67,
                "x": 744,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6d40cd16-fc72-4ef2-8933-203291bbf5ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 93,
                "offset": 1,
                "shift": 62,
                "w": 56,
                "x": 437,
                "y": 97
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "341b91b7-361a-4a48-93b0-94db53c156f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 93,
                "offset": 3,
                "shift": 62,
                "w": 56,
                "x": 379,
                "y": 97
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c8f23ba2-b5f2-4ba3-ab7e-922e883fe364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 93,
                "offset": 1,
                "shift": 67,
                "w": 63,
                "x": 948,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "05014741-26f3-495d-a8e4-7a05c57b7358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 93,
                "offset": 1,
                "shift": 57,
                "w": 54,
                "x": 551,
                "y": 97
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5d491406-1ba4-481c-9fac-df469774b00c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 93,
                "offset": 1,
                "shift": 52,
                "w": 47,
                "x": 100,
                "y": 192
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "845dfb1e-87a6-4727-8b22-e74dc44b1888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 93,
                "offset": 3,
                "shift": 67,
                "w": 63,
                "x": 2,
                "y": 97
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4951ec96-e6e0-423a-9b65-7b46f56ddbd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 93,
                "offset": 1,
                "shift": 67,
                "w": 65,
                "x": 881,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f5a4845e-de23-423c-abb1-d1dec4a456a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 93,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 739,
                "y": 287
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2036c048-7bd5-45f1-8b1d-651cbd7c0bdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 93,
                "offset": 1,
                "shift": 36,
                "w": 35,
                "x": 193,
                "y": 287
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a4732e77-45b5-4ede-9937-1ca8b430cd5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 93,
                "offset": 1,
                "shift": 67,
                "w": 67,
                "x": 675,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c8d4ab68-58f8-4d62-a6f4-53a9dd473471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 93,
                "offset": 1,
                "shift": 57,
                "w": 54,
                "x": 495,
                "y": 97
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ad4a1a01-5cbd-437f-a03b-6d10c6f8876a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 93,
                "offset": 1,
                "shift": 83,
                "w": 80,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7e30e1d1-5765-49a2-b100-77433c13a112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 93,
                "offset": -2,
                "shift": 67,
                "w": 68,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "07cfbdd1-0693-476f-b452-79165aa8c809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 93,
                "offset": 3,
                "shift": 67,
                "w": 61,
                "x": 258,
                "y": 97
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d6e7a90b-1e4c-4950-b220-d994e4e2ed36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 93,
                "offset": 1,
                "shift": 52,
                "w": 48,
                "x": 925,
                "y": 97
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "70dbdaaa-1fe4-4980-b4de-3215fe6d6603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 93,
                "offset": 3,
                "shift": 67,
                "w": 61,
                "x": 195,
                "y": 97
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1f4b31f5-21ac-44ac-8c75-ef3d2231f10e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 93,
                "offset": 1,
                "shift": 62,
                "w": 62,
                "x": 67,
                "y": 97
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5e52839d-2870-433c-b403-5b20216a4b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 93,
                "offset": 5,
                "shift": 52,
                "w": 42,
                "x": 663,
                "y": 192
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "40ab8ae8-2c07-4748-ace7-a81829a2785f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 93,
                "offset": 2,
                "shift": 57,
                "w": 53,
                "x": 663,
                "y": 97
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2e451a7c-2c8b-4a01-9596-18967854fcbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 93,
                "offset": 0,
                "shift": 67,
                "w": 67,
                "x": 606,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "17ed3a25-1183-4651-aa12-c5921101b856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 93,
                "offset": 0,
                "shift": 67,
                "w": 67,
                "x": 537,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8d551552-d9e2-4697-9972-36a6aebb088c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 93,
                "offset": 1,
                "shift": 88,
                "w": 87,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ee8fcceb-b551-4ee7-b2e0-42797056fe47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 93,
                "offset": 0,
                "shift": 67,
                "w": 67,
                "x": 468,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "df4af9d1-2920-4b9f-b60b-a310ed134d10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 93,
                "offset": 0,
                "shift": 67,
                "w": 66,
                "x": 813,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "aa54222d-4f91-4a77-b81a-0dd5ab1ce235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 93,
                "offset": 1,
                "shift": 57,
                "w": 54,
                "x": 607,
                "y": 97
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2a613fa3-292a-4317-8f3f-bcd8b1b0abaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 93,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 2,
                "y": 382
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c5789be6-2efb-4be4-b1ad-ed57d35589a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 93,
                "offset": 0,
                "shift": 26,
                "w": 25,
                "x": 796,
                "y": 287
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1f2fc5f2-92c2-4ba5-892f-95e2f709e0ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 93,
                "offset": 0,
                "shift": 23,
                "w": 20,
                "x": 991,
                "y": 287
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "74960c7c-4c72-46d7-bb41-d81da6d0bf33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 93,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 303,
                "y": 287
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fcfb972a-39b4-4235-a2d3-9eee6ef693f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 93,
                "offset": 0,
                "shift": 47,
                "w": 48,
                "x": 875,
                "y": 97
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "787a09fb-9fc3-4f09-9058-3d67f2288a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 93,
                "offset": 5,
                "shift": 31,
                "w": 16,
                "x": 67,
                "y": 382
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9dd96a1c-b2b4-458c-ad3c-7bab3d1a92be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 93,
                "offset": 3,
                "shift": 41,
                "w": 39,
                "x": 836,
                "y": 192
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3a088e35-7e5b-4ee3-bea2-9590dc9267bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 93,
                "offset": -1,
                "shift": 46,
                "w": 45,
                "x": 293,
                "y": 192
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ac9c9c78-f5a1-4549-9937-057f9fc900eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 93,
                "offset": 3,
                "shift": 41,
                "w": 36,
                "x": 42,
                "y": 287
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f9c4f91d-a39c-4e3c-a30a-a981d322be57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 93,
                "offset": 3,
                "shift": 47,
                "w": 44,
                "x": 434,
                "y": 192
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d09c3cae-b45b-4fb3-81be-feaef2d0e427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 93,
                "offset": 3,
                "shift": 41,
                "w": 36,
                "x": 118,
                "y": 287
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fc835681-8fca-4df6-a089-277c8c012ef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 93,
                "offset": 3,
                "shift": 31,
                "w": 38,
                "x": 959,
                "y": 192
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f9404e89-9594-4342-ae9c-2d190aff454b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 93,
                "offset": 2,
                "shift": 47,
                "w": 43,
                "x": 618,
                "y": 192
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e860e40e-6855-4c1b-97f1-4a61ac8bf71b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 93,
                "offset": 0,
                "shift": 47,
                "w": 46,
                "x": 197,
                "y": 192
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "bb0d8531-360f-426a-b972-8a43edca5413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 93,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 849,
                "y": 287
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "c2a5f9ad-3951-4daa-a87a-7ae07d8b2adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 93,
                "offset": -8,
                "shift": 26,
                "w": 27,
                "x": 681,
                "y": 287
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0251727e-4a92-40f2-b5b2-08a1f1b3e27b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 93,
                "offset": 0,
                "shift": 47,
                "w": 48,
                "x": 825,
                "y": 97
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fcbc190c-7f70-4dc7-babf-8ce5dffdb250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 93,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 873,
                "y": 287
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8b5cce84-812a-4a3d-b1d9-c75aa6ef46c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 93,
                "offset": 0,
                "shift": 72,
                "w": 72,
                "x": 255,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bc37376e-3369-442f-bab1-ebcc4612ae7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 93,
                "offset": 0,
                "shift": 47,
                "w": 47,
                "x": 2,
                "y": 192
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "20b3bf94-81c3-4010-8760-89d63f820580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 93,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 793,
                "y": 192
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0a36e9bf-0d6d-48a4-b306-e494681253d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 93,
                "offset": -1,
                "shift": 46,
                "w": 45,
                "x": 387,
                "y": 192
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5d218751-7bdb-48fc-a1e2-507151a54d77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 93,
                "offset": 3,
                "shift": 47,
                "w": 44,
                "x": 526,
                "y": 192
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1d2ec11f-d783-495e-a34e-4dc79fcdcf47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 93,
                "offset": 0,
                "shift": 31,
                "w": 32,
                "x": 515,
                "y": 287
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4251ef7b-db12-40ea-861b-d7c1c89697f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 93,
                "offset": 4,
                "shift": 36,
                "w": 29,
                "x": 650,
                "y": 287
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d2168d7f-f006-4adf-81dd-eac6e5fb909c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 93,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 768,
                "y": 287
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "63d89bbd-8703-43f5-9158-ee7e8d48cdd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 93,
                "offset": 0,
                "shift": 47,
                "w": 47,
                "x": 51,
                "y": 192
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "127a3b9c-6ace-407e-bcdb-0da28d683a3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 93,
                "offset": 0,
                "shift": 47,
                "w": 46,
                "x": 245,
                "y": 192
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a70c279a-9d7f-4fc0-a0d5-c18b5d17f166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 93,
                "offset": 0,
                "shift": 67,
                "w": 67,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "68fcbd6c-2858-497a-be90-724b8e9894e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 93,
                "offset": 1,
                "shift": 47,
                "w": 45,
                "x": 340,
                "y": 192
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4145e4ce-97ce-41a2-a38d-458b01266249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 93,
                "offset": 0,
                "shift": 47,
                "w": 46,
                "x": 149,
                "y": 192
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "07ddc3dd-3410-46b3-b0e3-c1a8c18bf6be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 93,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 877,
                "y": 192
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f6179c88-9c28-4bbc-aec2-8533b20d0fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 93,
                "offset": 10,
                "shift": 45,
                "w": 22,
                "x": 897,
                "y": 287
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "51b8add4-9dc3-4553-b05c-a923b2313e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 93,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 169,
                "y": 382
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2fcba8d2-0831-4850-bbd0-a2af30dc802c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 93,
                "offset": 13,
                "shift": 45,
                "w": 22,
                "x": 921,
                "y": 287
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4077c3ec-13e1-4d97-a4c7-68468fe01745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 93,
                "offset": 0,
                "shift": 51,
                "w": 50,
                "x": 773,
                "y": 97
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "e6a6fcf4-4439-4e4e-a192-9134373c7677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 93,
                "offset": 1,
                "shift": 56,
                "w": 53,
                "x": 718,
                "y": 97
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 70,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}