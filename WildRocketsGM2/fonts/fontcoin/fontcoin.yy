{
    "id": "e68c1b6e-9603-4ffd-bdbf-393b240b4bba",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fontcoin",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "Calibri",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e327ee86-f4dd-4190-b6c6-fc040babadd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 73,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 415,
                "y": 302
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a1295c93-8306-4f4f-a1dc-531dfb328a93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 73,
                "offset": 6,
                "shift": 20,
                "w": 9,
                "x": 61,
                "y": 377
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "158ebab0-de37-4357-bc1b-151e391f2577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 73,
                "offset": 3,
                "shift": 26,
                "w": 19,
                "x": 263,
                "y": 302
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "de490412-bd36-43d6-b897-1c597897ec79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 73,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 272,
                "y": 77
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "91489dd0-3007-457f-8003-b73a41283412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 312,
                "y": 227
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b3512a7c-47e9-40d8-9768-9298088487f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 73,
                "offset": 1,
                "shift": 44,
                "w": 41,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a22e4711-738e-48a7-ad34-dfe9e8fcdac2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 73,
                "offset": 0,
                "shift": 42,
                "w": 41,
                "x": 290,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f5f0241d-6971-45c3-9c75-0b81086691da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 73,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 92,
                "y": 377
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c73329e6-9f42-4e84-b6b3-aa66795cbbce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 73,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 462,
                "y": 302
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ad2be40a-02ea-486f-b829-90b914a42467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 73,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 431,
                "y": 302
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "94ec1434-f1c0-416a-b78f-fc81d581883d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 73,
                "offset": 3,
                "shift": 30,
                "w": 23,
                "x": 53,
                "y": 302
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e236f6d5-cd70-4c10-9a9f-3993f5f6853f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 73,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 182,
                "y": 152
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "128708b4-6b23-42d1-b40b-7f06e141e94c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 73,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 477,
                "y": 302
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7b036252-271d-4b95-8f3d-3104eb9d0fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 73,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 362,
                "y": 302
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5af347bc-aed2-46af-aa6d-590d3c1cb84d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 73,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 16,
                "y": 377
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "3bf75d50-8ff4-4ec3-9ed2-fedceb707226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 73,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 88,
                "y": 227
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cb8aa39c-453e-47fd-b0a9-07e0c0b13624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 73,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 212,
                "y": 152
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fbab4937-e049-48ab-b212-64cd65b543a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 73,
                "offset": 4,
                "shift": 30,
                "w": 24,
                "x": 478,
                "y": 227
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ad2f04ac-eeaa-4664-9b3b-3060484e3290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 418,
                "y": 152
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a392dd78-4042-4c70-8e18-ec2a3b6f7d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 368,
                "y": 227
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5acb725b-7fa0-485a-8a36-9c9498ed37cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 73,
                "offset": 1,
                "shift": 30,
                "w": 29,
                "x": 336,
                "y": 77
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ab27f994-285f-4eef-bbba-72b5202f1de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 200,
                "y": 227
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6663ed90-141f-486c-8764-7d5d0fe2a285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 476,
                "y": 152
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9c325e16-721d-4dc9-8037-00ce9e65c480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 228,
                "y": 227
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7a74ecb9-41f0-454e-ac52-1bcff94d43fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 73,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 272,
                "y": 152
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5c471632-b5f7-4111-ab32-b1b2d05c86d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 73,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 242,
                "y": 152
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "264297fd-7e96-4e20-ae02-901041b08e90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 73,
                "offset": 4,
                "shift": 17,
                "w": 9,
                "x": 50,
                "y": 377
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "980a938a-b326-4374-8ce0-b94cf41e5abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 73,
                "offset": 1,
                "shift": 17,
                "w": 12,
                "x": 2,
                "y": 377
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "838bfb20-2479-42e4-86ad-0ab40665a303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 25,
                "x": 451,
                "y": 227
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0e8da33c-8447-48f1-a801-0904158e25b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 340,
                "y": 227
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "651f52dd-e08e-4dda-86d2-c501840d2636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 396,
                "y": 227
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d6443583-6717-475a-8c42-869aeb48fbf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 73,
                "offset": 3,
                "shift": 28,
                "w": 23,
                "x": 28,
                "y": 302
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3c1ad18a-e260-4813-8be9-7fd1ec9cd2c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 73,
                "offset": 0,
                "shift": 54,
                "w": 51,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a186c078-cfcc-4a33-a8f9-b9736d8ff36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 73,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 414,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "fa0c5217-0269-4a12-aa0f-2090cb4af650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 73,
                "offset": 4,
                "shift": 34,
                "w": 28,
                "x": 92,
                "y": 152
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2c35fdf2-1bf4-46f8-828d-85ff27ed000c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 73,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 429,
                "y": 77
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c19df253-625d-4165-ad40-871e147aea97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 73,
                "offset": 4,
                "shift": 38,
                "w": 32,
                "x": 71,
                "y": 77
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "be1adc02-41dd-4017-923f-1cb5865d3439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 73,
                "offset": 4,
                "shift": 29,
                "w": 23,
                "x": 78,
                "y": 302
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "77341b80-55f9-4c0b-b085-f5e693167007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 73,
                "offset": 4,
                "shift": 28,
                "w": 22,
                "x": 127,
                "y": 302
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "20519a87-ddcc-441b-9411-9295af7861e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 73,
                "offset": 2,
                "shift": 38,
                "w": 33,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "12cf03f8-b8ee-461d-965b-d6b34ed62c26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 73,
                "offset": 4,
                "shift": 38,
                "w": 30,
                "x": 240,
                "y": 77
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1940281c-e9f8-419d-8ea8-4525f8fcd74c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 73,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 82,
                "y": 377
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "81d78dee-cc4f-4114-8f47-22a14a8781f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 73,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 380,
                "y": 302
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "10c9d90a-98b6-43d0-971c-c1f7be3cded6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 73,
                "offset": 4,
                "shift": 33,
                "w": 28,
                "x": 32,
                "y": 152
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e383ba6b-01e2-417a-ab6e-26ff9bd48c24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 73,
                "offset": 4,
                "shift": 25,
                "w": 21,
                "x": 151,
                "y": 302
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "07d9372b-45fa-47fc-aaaf-171746c3e34c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 73,
                "offset": 4,
                "shift": 52,
                "w": 45,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6492b441-0ceb-4c29-9f19-526e273621eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 73,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 173,
                "y": 77
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "64644180-6e23-479b-a39c-1366fff9dd37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 73,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 375,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "24997fd2-e9f3-4498-84d9-efee47f4fc71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 73,
                "offset": 4,
                "shift": 32,
                "w": 27,
                "x": 31,
                "y": 227
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "145488a7-b720-4597-8b8a-9ae393b49dd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 73,
                "offset": 2,
                "shift": 41,
                "w": 40,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6ac3c828-f998-4db7-a303-399a208afdd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 73,
                "offset": 4,
                "shift": 34,
                "w": 29,
                "x": 367,
                "y": 77
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d65ea8ae-c33a-4d30-b13b-96c83021b8c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 73,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 144,
                "y": 227
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fd05d6cd-caee-45aa-9a74-17c46c48ad41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 73,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 304,
                "y": 77
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "300246d4-6568-46c5-9ecb-204435199162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 73,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 139,
                "y": 77
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "62bb94f6-0eb3-4e41-abf3-df72ea195083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 73,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ad1368c4-ce25-4a47-9c53-44f564a303f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 73,
                "offset": 1,
                "shift": 54,
                "w": 53,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "aee96b49-9170-44c8-b3f9-cf33c2861bb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 73,
                "offset": 0,
                "shift": 33,
                "w": 32,
                "x": 105,
                "y": 77
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4623afdc-5cfc-4eb2-9be9-dbf0d81e8d1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 73,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 207,
                "y": 77
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "890029f2-f96f-40c1-bdaa-9408eaddd434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 73,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 389,
                "y": 152
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5ab14b49-65dd-4f94-a77e-20a87d33c83a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 73,
                "offset": 4,
                "shift": 19,
                "w": 13,
                "x": 492,
                "y": 302
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7e5c5eef-f5af-4503-ac5e-0a5523e02103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 73,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 116,
                "y": 227
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5f5e83a1-3f7a-40b3-9c7e-7a420e8b7e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 73,
                "offset": 2,
                "shift": 19,
                "w": 13,
                "x": 447,
                "y": 302
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "189f4f4f-848c-41f8-9259-e7866c61edd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 73,
                "offset": 0,
                "shift": 30,
                "w": 29,
                "x": 398,
                "y": 77
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e62f273c-6aeb-429f-b34b-32aea28cf5ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 73,
                "offset": -1,
                "shift": 30,
                "w": 32,
                "x": 37,
                "y": 77
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0acd8665-e2ff-4124-8905-2b7c94baf5f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 73,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 344,
                "y": 302
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "76db58dc-d703-49b7-8a04-f2bc68a7610f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 24,
                "x": 2,
                "y": 302
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b6c75cce-1923-45e0-bc1c-4ea259c519a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 73,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 302,
                "y": 152
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d3ee8e33-7870-41c4-a816-2c8dd8af9148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 73,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 103,
                "y": 302
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c4d6cebb-73ed-4e92-9032-638223c1f687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 73,
                "offset": 2,
                "shift": 32,
                "w": 27,
                "x": 331,
                "y": 152
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5c24adc0-8013-44ed-a477-fa7e8c566406",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 73,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 60,
                "y": 227
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "54e129b9-8069-4215-a832-e004a5f2d400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 73,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 219,
                "y": 302
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "942a4a4a-cc33-4aa9-a0cb-0a3bf085335d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 73,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 122,
                "y": 152
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c635f78d-45f4-4928-854d-2ffd33a07fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 73,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 172,
                "y": 227
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "68de293f-33d5-4187-a7a2-63a66a45b605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 73,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 39,
                "y": 377
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5cac39c5-9e67-402c-9856-4a159d123d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 73,
                "offset": -2,
                "shift": 15,
                "w": 15,
                "x": 398,
                "y": 302
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8b195cb7-7e2c-4515-91bd-639e75c424b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 73,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 424,
                "y": 227
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9590d320-7ac4-4e1d-a438-b43058b4f24b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 73,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 28,
                "y": 377
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "de8c1d13-e657-4878-a293-8c95baca6654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 73,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "83bcc416-10b0-49a8-9b59-7119273ca52e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 73,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 256,
                "y": 227
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d0891730-820f-456e-93f4-2633f5f2667c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 73,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 460,
                "y": 77
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1e0041d6-0676-491e-9ac3-7becc0976c9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 73,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 447,
                "y": 152
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "179b96d2-ff33-433c-a7f2-af82c79c9fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 73,
                "offset": 2,
                "shift": 32,
                "w": 27,
                "x": 360,
                "y": 152
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "55917dd5-3605-445d-a67c-079448f4fd35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 73,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 324,
                "y": 302
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c488626c-80f4-40d3-9c40-194a4a319453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 73,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 174,
                "y": 302
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "17e9e9b3-2136-45ba-ae10-8dda48272d8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 73,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 241,
                "y": 302
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f65ba61b-68ea-4c3c-a863-8b8cac24f05a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 73,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 284,
                "y": 227
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f52734ca-04a0-4ed8-8daa-a908a043f073",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 73,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 62,
                "y": 152
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3a954365-e243-4a0f-b0c7-47c8816f0791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 73,
                "offset": 1,
                "shift": 45,
                "w": 43,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fb47001c-43e6-43ae-9966-73239ef11949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 73,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 2,
                "y": 227
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d2bf2051-8a23-4ee1-b32c-edd7b3a7c3c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 73,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "45c5f53c-8529-4024-99da-a795fca131f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 73,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 197,
                "y": 302
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "90493963-71bf-49b6-809d-0b9b1f09e1f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 73,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 284,
                "y": 302
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "54ee365d-72c0-4cad-8f02-bc2c24498fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 73,
                "offset": 10,
                "shift": 29,
                "w": 8,
                "x": 72,
                "y": 377
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "be6fb646-ebec-4aaa-af5f-fd9816a371fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 73,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 304,
                "y": 302
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0f868d0e-95fc-4068-97c5-5d47b4fba59e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 73,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 152,
                "y": 152
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "54492890-6f72-4496-a204-a15fcd916626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "ee100d04-f285-4912-b0c3-accf797686c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 45
        },
        {
            "id": "0b788566-5693-402d-a2ab-d5cfe39716cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 63
        },
        {
            "id": "e8ab7268-5843-4850-97df-2d0a2fcc552e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "3bf8e8b9-53ea-4f23-8a8e-7b9427fed5a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "6666d2b4-0b08-43cc-af76-07665f5b3631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "9dfc7d60-da25-4d89-924d-b3f15590d567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "1611e635-b48f-4b58-807d-3ae6f1ec75fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "e5f7a97f-32d8-4c63-be1a-eb91b0fb796e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 84
        },
        {
            "id": "2dfe20ba-24e5-4ab9-baf3-c86da331071f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "fdc8ae73-904d-47aa-8d25-1b6c4b007ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "91e186be-cbc7-4bfe-ba64-e57b434a6a4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "79a209a2-10e2-4d02-9908-d8504ba8ad06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 89
        },
        {
            "id": "2fa3769d-e47f-4416-acd5-32d2eb537aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 116
        },
        {
            "id": "747effc4-79dd-408d-aa44-882132a31db7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "a4dee4ee-2cf2-45db-8119-95601641d8ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 121
        },
        {
            "id": "ebbbaf07-e49c-49b5-a7fd-bd1e92bec745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 173
        },
        {
            "id": "45168e2f-efd4-47a8-8f6b-897ccf1c5797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 199
        },
        {
            "id": "6e9d314e-ba1d-42af-846f-d859db565330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 210
        },
        {
            "id": "5e95ff5a-aca6-4edc-b4c3-cc13ed4c23e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 211
        },
        {
            "id": "b12e1592-0639-4ec1-97b5-c72e9d1199a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 212
        },
        {
            "id": "5f67e73e-f661-4e86-be16-de3760a7bb9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 213
        },
        {
            "id": "a0c08d15-7bd0-4e29-ad68-4e18bee0f973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 214
        },
        {
            "id": "49d807ac-5c61-4d90-bf91-1e0666e0dbc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 217
        },
        {
            "id": "e650afd0-3f89-41e1-af63-e747d6b9eb76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 218
        },
        {
            "id": "3de92625-1825-4e0a-ac0f-e6f6bee81027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 219
        },
        {
            "id": "ba72b09b-446e-4c66-90a5-3d7cbda93f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 220
        },
        {
            "id": "a1d50f9a-4df1-4ad6-8fc3-e404b3617fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 221
        },
        {
            "id": "74c1a55e-d3e6-4898-bf7f-f29667678a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 253
        },
        {
            "id": "839bae59-45dd-4b98-ac5e-1a954ed18aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 255
        },
        {
            "id": "8aa3e1fc-0a23-4197-aa76-623a0ffbdd25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 262
        },
        {
            "id": "e19f7a75-f2ca-4fcc-af1a-c313315e1522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 264
        },
        {
            "id": "899e46f2-32f7-4865-92b4-28ea588d048a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 266
        },
        {
            "id": "243d8f1b-a0c3-45d9-87eb-b8f0cbae26f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 268
        },
        {
            "id": "b9218901-05ca-41e5-b0d7-114c95d7a759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 284
        },
        {
            "id": "e958d9c0-99c9-46b4-b36e-f437de46dc64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 286
        },
        {
            "id": "6da41be0-6a10-4644-b37b-5243f39c774f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 288
        },
        {
            "id": "11226487-61a2-4011-9d5e-9bffc82cef17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 290
        },
        {
            "id": "fc47ab16-9121-4de0-9cd4-d4bddfbde514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 308
        },
        {
            "id": "e310bb9f-6f93-4d96-846c-b257e84b2a84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 332
        },
        {
            "id": "f4a9a391-fbcc-4ac7-a321-8ec1cbf17325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 334
        },
        {
            "id": "34f8e545-dc20-4b49-b0df-f5e4183e73b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 336
        },
        {
            "id": "08af3c8e-b523-406c-9e80-bcd5477589b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 338
        },
        {
            "id": "44a4ce9d-7683-4464-b975-0aa8525f23d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 356
        },
        {
            "id": "3101ef27-f300-46c3-9342-64969f13cde3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 357
        },
        {
            "id": "b65b1de1-3684-411c-b3ab-4c9f4406e0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 360
        },
        {
            "id": "82d0b0ff-9cbd-4d75-8f94-ef1a29de8815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 362
        },
        {
            "id": "84079ab7-9d6d-4f7c-9733-c001024a107e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 364
        },
        {
            "id": "347f3a84-41b9-4bed-bd44-11361481266a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 366
        },
        {
            "id": "a3377690-9493-4ef4-a315-6f0ffaae6207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 368
        },
        {
            "id": "39c4c2c1-b442-4dad-8d8f-d8bc1783a777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 370
        },
        {
            "id": "5d3aac4c-2ee1-405f-8aa0-c20997ea257a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 372
        },
        {
            "id": "1b2f48dc-ee20-450f-9ec6-a5f0a5159c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 374
        },
        {
            "id": "a5d9d8eb-04a9-41b2-8d28-5c99a69533ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 375
        },
        {
            "id": "67b878ee-218d-4bc3-8ea2-eb578ff7af0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 376
        },
        {
            "id": "65524c6c-1493-4f9c-a8cc-8cb8dbb8c1d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 416
        },
        {
            "id": "d67ea8f5-f679-4aec-9be7-afc4fb44eafb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 431
        },
        {
            "id": "027d5304-5346-4dd8-8b22-4a998aa3c38a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 538
        },
        {
            "id": "01d71bca-4795-4a86-b541-63fae036a23f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 539
        },
        {
            "id": "19217091-43df-4837-85b9-46f9369577c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7808
        },
        {
            "id": "7c12ed85-b1c4-4ed7-ae35-34f6d94bda05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7810
        },
        {
            "id": "ea86f53c-904a-4eca-9809-31be38c0668a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7812
        },
        {
            "id": "68547720-c091-4c29-bc17-fc258cff853b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7884
        },
        {
            "id": "dc4e487b-3e89-44cd-aa99-c7ec14ace7cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7886
        },
        {
            "id": "2cf7b5e4-a675-4d38-b05d-5fb63c294d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7888
        },
        {
            "id": "5ea2efe7-6513-4790-86bc-1ac17f9c4ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7890
        },
        {
            "id": "5956768a-4996-4448-9b51-e67710e98049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7892
        },
        {
            "id": "5d2d060e-115e-4d1a-b631-d59cb402a573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7894
        },
        {
            "id": "d9c48c04-55e4-4283-8459-21f526772462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7896
        },
        {
            "id": "2b68d7cb-58ea-49dd-ad42-b4a96aea2109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7898
        },
        {
            "id": "a5e2193a-ad9d-4d00-89a8-ef7a01c1dbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7900
        },
        {
            "id": "9901dfdd-a47f-497d-a4c5-ca0ab99d34a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7902
        },
        {
            "id": "16400bd5-6f89-4e10-998a-af7f517c1a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7904
        },
        {
            "id": "70bc3634-c635-4518-9dae-83efd6c80ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7906
        },
        {
            "id": "e0eea6ee-e6a8-4bfa-9884-b1595250da39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7908
        },
        {
            "id": "529d8207-7d35-4557-9f9f-522a2bae1e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7910
        },
        {
            "id": "93434040-388c-401d-aede-e7b36880c373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7912
        },
        {
            "id": "7fc01fe1-ffb0-4b29-8a3b-8be284103459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7914
        },
        {
            "id": "0a36c430-ccce-41df-be5a-a8c39395677d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7916
        },
        {
            "id": "da269de5-3f92-4dab-a2c7-d1f571b7662e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7918
        },
        {
            "id": "8e576a31-1b28-4e6a-8d05-a908a75a9455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7920
        },
        {
            "id": "4a82317d-54fd-4f8e-ada1-672fc03bf742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 7922
        },
        {
            "id": "d113f9fd-b19e-4a4a-b828-319082ba5184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7923
        },
        {
            "id": "9d606b02-4846-40c2-8c05-534df32c39b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 7924
        },
        {
            "id": "23090533-1d50-441a-b13b-f4d4cde0dd57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7925
        },
        {
            "id": "e0e45f7f-4444-4902-ba9a-71817d213200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 7926
        },
        {
            "id": "31704d1d-f1e3-45cb-a343-0e800b7139a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7927
        },
        {
            "id": "d89e31e2-0200-4718-a4ac-46c48a463d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 7928
        },
        {
            "id": "11b4b955-2872-4bd8-a3c2-33bce9b4eb5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7929
        },
        {
            "id": "fd2c7b18-0ddb-4119-ad63-0e76d49ec780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8208
        },
        {
            "id": "154d27e6-91bc-4ae7-8b61-206bd9172104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 8216
        },
        {
            "id": "7737a8f0-10da-435a-ab08-ec502e6766d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "9d19e877-d104-44f2-bf09-3831783bccbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 8220
        },
        {
            "id": "824c3593-7897-444a-9f6d-943f920f0f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "cbc045be-6fb7-45ce-8b34-9ab15777a6ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 44
        },
        {
            "id": "28b3811a-0aed-44c0-bc4e-0d48d38ee98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 65
        },
        {
            "id": "5e0ae65d-6a07-41f0-a57d-a2e614b251ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "1e36f328-d835-416d-a0ae-8d8de0108540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "930fb54b-894c-4050-89d1-e9f82c77534d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "f3d04c52-3b5f-42ba-a99a-8b023536d9ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "4b45c47a-45a1-4808-889c-cc0b1577ad02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 89
        },
        {
            "id": "3bf1ecc0-37dd-4adf-8a36-ddeaffbb6a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 90
        },
        {
            "id": "ede26fe4-963f-40c2-8008-b12c81422ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 102
        },
        {
            "id": "f1b8471c-1e31-4057-b673-3586fcd75ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 116
        },
        {
            "id": "1d5da866-c2c5-44d9-b6f7-442c515ecb25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 118
        },
        {
            "id": "531087ef-8b0d-44dd-9ab0-fc72c4dbba02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 120
        },
        {
            "id": "6042290d-eaf9-4da3-a10f-1914c0a888ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 121
        },
        {
            "id": "d8d8cbad-4f21-41e6-8bc0-dc5103ee7b31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 192
        },
        {
            "id": "0665661d-cf33-43d3-9131-318a1547d681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 193
        },
        {
            "id": "03150219-ab8e-4410-881e-761ab9534150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 194
        },
        {
            "id": "422e3db1-530b-4811-a909-04fc7525e6d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 195
        },
        {
            "id": "60b234f7-83d4-49e8-a4e3-95f70095f78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 196
        },
        {
            "id": "b1044953-33c7-458e-b5b6-b4606ffb6061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 197
        },
        {
            "id": "af146c1e-25b4-4cd1-963a-a972e566e239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 221
        },
        {
            "id": "ec0cef09-7300-4d9e-aac1-f5204185166c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 253
        },
        {
            "id": "eba5ce1b-4dc0-4a32-9178-5887cb85365c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 255
        },
        {
            "id": "08f14952-36ca-45f0-981a-d5c7d3ee30bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 256
        },
        {
            "id": "809be7b3-b5be-4169-86a1-303e6768a908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 258
        },
        {
            "id": "3f2b2efa-47f3-476f-a5d8-e2641aed1a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 260
        },
        {
            "id": "dae53879-d9d7-4253-b149-77773b19ba22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 356
        },
        {
            "id": "92ed4c9f-4483-4ebf-a5fa-a3be4c841ad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 357
        },
        {
            "id": "328bc7e5-6f0d-4afa-be49-1db6b948fdf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 372
        },
        {
            "id": "65f3781f-4654-4db8-ba9c-6b828df08b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 374
        },
        {
            "id": "72d88f06-eeec-4e3a-b6d1-a4a527834668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 375
        },
        {
            "id": "ca417d37-120c-4519-8032-4aac8fda24a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 376
        },
        {
            "id": "c28709d3-b757-43a0-8638-00e2b46b98e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 377
        },
        {
            "id": "50f196ab-8a7b-49a4-bda2-0bfb48a6c780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 379
        },
        {
            "id": "4947227c-7fae-417f-9b23-8b623e4d478d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 381
        },
        {
            "id": "a3b7b255-f3b7-4adf-90bb-e7e76d6c0b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 506
        },
        {
            "id": "cbf9832d-134a-42da-acc2-570707cb8c6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 538
        },
        {
            "id": "70318ef0-ee45-44b1-9afb-0ff3311837f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 539
        },
        {
            "id": "1f40faf3-a5f2-4dee-89c8-c32ec86f59dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7808
        },
        {
            "id": "356c86de-9115-4185-937a-dcfefc538cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7810
        },
        {
            "id": "6e3dad66-d224-443d-9bb7-02149707722d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7812
        },
        {
            "id": "f9713009-ae52-4127-a903-c3d3c34a289f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7840
        },
        {
            "id": "ffde6455-daee-4a5f-bdf1-5662425e8bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7842
        },
        {
            "id": "b8da255f-69d1-48b3-b4fb-aea29b9c59d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7844
        },
        {
            "id": "87c07658-0b56-46c3-9d89-ae7ac8d19a25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7846
        },
        {
            "id": "4a0a02d1-4cb0-401f-94f9-b1987eb26134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7848
        },
        {
            "id": "2ac22846-9bec-43ab-b989-70c9c3ecf40e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7850
        },
        {
            "id": "44edae35-2d11-4542-8d11-b51e9b1ef7a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7852
        },
        {
            "id": "ef526c9a-d634-4bb3-953a-9c0ad22c6fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7854
        },
        {
            "id": "014c910f-6479-4e5a-9826-424b938f4a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7856
        },
        {
            "id": "e8cb2e8b-bbd2-40e2-904c-15f15e80a782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7858
        },
        {
            "id": "d7638e9b-c372-425d-89b7-743d76ac94f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7860
        },
        {
            "id": "138fbe93-84f8-4ba0-a81e-c49d9306906a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7862
        },
        {
            "id": "68d32fb2-3971-47e2-8db1-90fd206ebc4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 7922
        },
        {
            "id": "bfd542e5-2a68-4a65-bc09-add1abdf6b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7923
        },
        {
            "id": "c2c8a691-a531-45db-b95f-37429158a78a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 7924
        },
        {
            "id": "abbd61a6-0902-4c70-95a8-b55a0b8c3270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7925
        },
        {
            "id": "0aa2ae6b-dd03-429d-a15c-0960a62eea92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 7926
        },
        {
            "id": "b1c6d906-e4ee-42ab-8f4b-526f6bc6b0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7927
        },
        {
            "id": "559b17ba-4a64-4d84-9fde-5a4f251f1afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 7928
        },
        {
            "id": "a225a250-cbe7-4016-816b-ddb797eb67c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7929
        },
        {
            "id": "d58795ae-2f21-473b-a065-575f8b3b0549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8216
        },
        {
            "id": "c2bcd8cd-d5fc-4c56-b4b5-db980fc01db7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8220
        },
        {
            "id": "2d225ca6-8c6b-4ec2-8d16-67e8328a7e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 64256
        },
        {
            "id": "4b222129-40f3-4643-8c1e-c7a56ea2144a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 64257
        },
        {
            "id": "f4aded6b-684d-415e-9b3c-63d8c9f8a011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 64258
        },
        {
            "id": "87a7d798-8ba9-4c84-98f9-8bea27a42764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 64259
        },
        {
            "id": "a7f20c71-326d-40f2-8188-cd1c67ea4953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 64260
        },
        {
            "id": "8e7f0c0e-8f00-4a43-b1c0-ecf9a4cbee1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 198
        },
        {
            "id": "995d2710-a077-4fea-9d8f-4b48af3990fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 67,
            "second": 8217
        },
        {
            "id": "cb7f7b4f-33dc-4ecf-a129-a2f0c9307f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 67,
            "second": 8221
        },
        {
            "id": "e6fa976b-f20c-463a-9ea6-0b4328dcca59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "9d00e036-eb55-4ecf-9448-47d84805029e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "475e3cd7-f2ce-46b3-823b-ec4a546810c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "c467607b-d217-4cb7-a449-eae684d06e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "c1af82c5-2fdf-4238-9153-da6d75b25fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "3e800334-2e66-444d-ac61-01020c1549c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "8469fbfa-9f42-4c11-a853-f701d1ff6736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "50a61ce8-e0d3-4fd4-8335-965106168f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "9139b138-1614-4ea9-9d31-da0671f63d69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 89
        },
        {
            "id": "9ba2b71f-6af7-46b4-8c77-20d26e32a2ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "7ddbe722-771b-404a-a58c-a49121985782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "f3959120-c9c2-42ba-9ac7-3748fca76daf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "38733b9f-58cb-47ff-8561-467d17c4bc06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "ecaab377-6cb3-4f71-ab97-3eadb4dc6093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "197154e0-75c8-494f-bcd6-64a181b37a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "cdce49fb-0dc9-44d4-a2bf-44bcf660cf23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "45bca70a-a6c2-4bac-b7de-57c7c6e488b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 198
        },
        {
            "id": "fe8c8c4b-5e14-4196-a307-6d335c8a492d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 221
        },
        {
            "id": "8b31045c-48b5-4413-975b-9e7b6da66584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "e269e382-341c-461b-9b56-780d80ca6ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "738b3464-51d5-45de-8086-270f838c1061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "7330676f-5d22-49be-b714-0f58a8a76a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 308
        },
        {
            "id": "a2e6a823-6dda-4def-8473-abeae90be860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 356
        },
        {
            "id": "285c02ca-82cb-4487-a3d2-800ea287b2ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 372
        },
        {
            "id": "a0268eca-1d1b-4cd2-ad6f-5212a86af08a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 374
        },
        {
            "id": "5de35c34-af33-493d-b8c8-076f8f635847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 376
        },
        {
            "id": "e7c6aa82-8554-49dc-87e3-bab57bc6a0a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 377
        },
        {
            "id": "08699d39-58e0-4854-94f0-0824793eb30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 379
        },
        {
            "id": "d8cfdb65-677a-4a95-a1e0-5e398d44d526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 381
        },
        {
            "id": "469efcea-5296-40d4-a2ac-81c724d70a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "c10668a3-d900-4ce6-a0eb-d56ec588bae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 538
        },
        {
            "id": "ea40d305-b304-45bb-897e-9037133d9d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7808
        },
        {
            "id": "5524ba17-5cf6-44de-a937-ed9d1d0e48cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7810
        },
        {
            "id": "ae02b8e8-6ada-4610-b4ed-765e13a48afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7812
        },
        {
            "id": "3faf09bf-3aa7-4975-a478-06024b5a9a09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "4d095cee-8051-4d4a-8fa5-b0a0eb5bc921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "53274c08-ffd9-4deb-bdcc-f05c3525c17e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "74aec817-b945-42f9-b86e-2ea6082713a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "816e38ca-40d9-4009-b9c1-64f9b9aed169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "0234615d-721f-4df6-8b9c-e26afafe1a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "be9ba19e-af04-47eb-b96f-95a48c533217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "5d4dac43-ccb3-4831-8983-0ca41888bad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "092910da-c30f-413a-9b5e-32b5295f1e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "59616c29-26d0-4ff7-9a99-eaaee9fb99f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "8b2cc207-dd0e-4187-b82e-f6e10627f24c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "3edde6be-2488-41ac-8338-ad9178b459c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "202dccd5-e426-4c29-879f-783bbab2eef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 7922
        },
        {
            "id": "51e6988a-2692-47ce-aeb9-c63d69c2b334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 7924
        },
        {
            "id": "dd22342a-d8d5-4389-af18-b89f5620ff54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 7926
        },
        {
            "id": "fdf0dbe8-d024-479f-a260-e214d40cb4fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 7928
        },
        {
            "id": "50ce3319-723b-4312-8d64-9a548e5ad32a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 45
        },
        {
            "id": "27da60a8-5137-4470-b4de-bba9b46fc892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 65
        },
        {
            "id": "862de251-ae6e-42e6-a0f3-bf8d67d0ef01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 67
        },
        {
            "id": "de36d86c-a857-4048-80d7-fdee6e019fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 71
        },
        {
            "id": "16bed52d-5d78-4905-bbe1-f1d2552a8b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 79
        },
        {
            "id": "24c4911b-922a-41df-8f57-a1545bb5c7f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 81
        },
        {
            "id": "516e10b8-a370-4877-b8d2-ee6f3ca43183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 83
        },
        {
            "id": "c297f11a-5bd8-4b85-a2c7-a7d48793a612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 97
        },
        {
            "id": "8310cbf9-5199-4130-bd8c-4a35f5b3c2d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 99
        },
        {
            "id": "32aa73e7-9707-4e1a-b244-08f84e3efb09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 100
        },
        {
            "id": "692fb4ca-6256-42dd-b287-0600a0a495b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 101
        },
        {
            "id": "fcc8a7af-168c-486a-a74b-5930ea055b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 102
        },
        {
            "id": "ffc4b016-fda6-4598-94e9-9f1d62fb78da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 111
        },
        {
            "id": "52a4a8bc-01a5-45f3-b2e8-1f90bdf54912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 113
        },
        {
            "id": "675a35c7-cc96-4377-877d-84187c94129c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 116
        },
        {
            "id": "8d225436-53e3-4b8c-85b2-7af7badf1590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 118
        },
        {
            "id": "d2e99177-d3fe-4dbf-ab5c-9eb5502c16d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 119
        },
        {
            "id": "b5b7de7b-f592-417d-8165-ef3fabff8245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 121
        },
        {
            "id": "3dcc489a-5668-4cbb-bdad-23e08904145e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 173
        },
        {
            "id": "9ff4c05d-ed41-4ad3-b45a-448ec315c5b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 192
        },
        {
            "id": "d11acd1d-e950-4cd4-a2d4-1205b64c5fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 193
        },
        {
            "id": "4905bbd4-769c-4ed9-a1e7-507328236018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 194
        },
        {
            "id": "16bfad13-0303-496c-a23e-9d93f9deb2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 195
        },
        {
            "id": "540ec492-505e-4c96-8ab4-4a9fb32a6d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 196
        },
        {
            "id": "46afac46-4859-4fa6-876d-36058ef6662f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 197
        },
        {
            "id": "9dd1ee67-7eed-448f-a9cb-6cab1b35c2df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 199
        },
        {
            "id": "24d151de-b83e-4b0c-b416-e01f68129368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 210
        },
        {
            "id": "74500729-9303-4a26-8ff2-db22295f68dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 211
        },
        {
            "id": "4b380879-5c1a-4fe3-9c03-287e276a0d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 212
        },
        {
            "id": "4baf73bb-83bd-4561-85fb-e9463d179fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 213
        },
        {
            "id": "9bd8d680-2fc6-4822-8430-8ea4346de8e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 214
        },
        {
            "id": "dbd19432-558c-460a-998b-7fd4db74e9fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 224
        },
        {
            "id": "c8946960-5e46-4d14-9004-5bc7b2c0ba75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 225
        },
        {
            "id": "f2573316-f64a-4780-993c-8f8397600527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 226
        },
        {
            "id": "38ea633d-5929-4725-92f5-f28c4d923318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 227
        },
        {
            "id": "0027ed15-83d2-4999-a174-650422153423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 228
        },
        {
            "id": "de990d43-b0ab-46cf-b03e-84044e6b4477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 229
        },
        {
            "id": "050e36a8-3a78-4e6d-aa4a-6abb73e5ff73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 230
        },
        {
            "id": "05a0eac9-2d42-4722-9b6d-1fd9843a8bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 231
        },
        {
            "id": "e0ae5be0-5900-40fb-9bb9-1e5958ed67ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 232
        },
        {
            "id": "e5186297-91ce-4187-89b1-089411a371ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 233
        },
        {
            "id": "f8120bdc-2b0e-4847-b535-3567c4133568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 234
        },
        {
            "id": "d615f89a-8612-4d73-b6fa-c623ec768e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 235
        },
        {
            "id": "5df854a4-b273-4145-a820-377f2c203382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 242
        },
        {
            "id": "42b7d769-7a48-4149-8a99-4fad41283b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 243
        },
        {
            "id": "3ac1bb41-d9c8-4861-b996-1796a9a27484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 244
        },
        {
            "id": "3795f032-ac5e-49c3-a3a6-860b6f3d10e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 245
        },
        {
            "id": "01e85c59-d6e5-40f1-8c88-1d2cd6479625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 246
        },
        {
            "id": "f414624b-4f9b-4ff9-95a8-5ba5e2af8ebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 248
        },
        {
            "id": "3b14c217-8fbc-4f43-9d78-14baa7e9a650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 253
        },
        {
            "id": "b29f4caa-b5e2-4898-b8ff-7987dc9ebb33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 255
        },
        {
            "id": "d2f53150-27c8-49c4-ba50-b93397110ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 256
        },
        {
            "id": "62e13f50-3446-4fdd-b840-32cba81b37e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 257
        },
        {
            "id": "c94fb522-7708-4e5c-86cd-bc0f1f81ba1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 258
        },
        {
            "id": "254478fa-72c6-4175-b186-640c05a11a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 259
        },
        {
            "id": "9ca11681-5a00-43d6-b7d2-fc646b576afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 260
        },
        {
            "id": "417667fb-f342-48cc-9f68-bd996fedfa9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 262
        },
        {
            "id": "543f1553-6865-473a-8011-5e0d9ea1efaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 263
        },
        {
            "id": "8400e319-11c0-4b07-b775-26af597ef718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 264
        },
        {
            "id": "db6049f9-e5c2-4d73-9fcb-e2d3758e01ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 265
        },
        {
            "id": "b3529a80-13c1-4980-8df2-e5b89af8ab20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 266
        },
        {
            "id": "38cd9249-9863-4b95-aef3-be46e6e2d880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 267
        },
        {
            "id": "441a6927-ae9a-4567-90d4-adf81ea3fa9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 268
        },
        {
            "id": "05330cfa-9ad6-409c-8ee5-b756f6aa7dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 269
        },
        {
            "id": "74299ab7-d16d-47cb-8d75-0b29ea905123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 275
        },
        {
            "id": "139a3131-f110-4a04-8a68-1a02ed3c7617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 277
        },
        {
            "id": "986e3a87-42e5-423b-b816-7dd81f38de21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 279
        },
        {
            "id": "b288a73a-73be-4de8-929f-2b52ca4d909a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 281
        },
        {
            "id": "cde53df2-ca46-4011-a5e7-afc87eca2e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 283
        },
        {
            "id": "b2a6e209-be40-4bfb-955f-13d39a602e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 284
        },
        {
            "id": "b004c97b-f4e2-47e6-ba56-fffb28ee11cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 286
        },
        {
            "id": "c7228960-b1b6-41d0-970b-8c50e9faee98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 288
        },
        {
            "id": "0cf419f5-d2bf-4e6f-a34f-8f47dfb8d306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 290
        },
        {
            "id": "aa8dfad8-879c-49de-bdb5-effb10b73557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 332
        },
        {
            "id": "c3751a87-e9b4-41a2-b15b-81795e8c1d28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 333
        },
        {
            "id": "07416bd7-c1b4-49c9-b26d-cc9ac4914c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 334
        },
        {
            "id": "b389c8a3-f94b-4b80-a20b-19763a0e713c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 335
        },
        {
            "id": "9a9b806a-a7b1-488a-9392-3e7c4f06d14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 336
        },
        {
            "id": "5139b382-c708-417f-a196-430bed0e9078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 337
        },
        {
            "id": "b803fb79-ba4a-4633-9833-ae0bbba013c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 338
        },
        {
            "id": "bf1559d3-bffc-4a65-a2ee-cdffe518a04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 339
        },
        {
            "id": "bceab329-3075-4b3c-ad71-76c246599b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 346
        },
        {
            "id": "55b006ed-1980-4f22-9513-0cc0926834ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 348
        },
        {
            "id": "89fd0a74-7e8a-4c81-b228-dd711e75fcaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 350
        },
        {
            "id": "07a6a7ad-9284-428b-9301-27608260ba51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 352
        },
        {
            "id": "407aaf88-7fc8-45d9-a786-7146a09c385b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 357
        },
        {
            "id": "c65e32fe-a4fc-4950-842e-9ae83435a488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 373
        },
        {
            "id": "fd2de8c1-5175-4faa-b7cf-28c09b4ad634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 375
        },
        {
            "id": "773b3b82-e350-4769-bf8f-14040220bab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 416
        },
        {
            "id": "de93cd3d-224e-414b-92d5-e7c0b6a51584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 417
        },
        {
            "id": "235bdc30-b0da-421c-b98c-999a6fb401e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 506
        },
        {
            "id": "c1ca9630-3ed7-43f0-88b4-b2707b143ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 507
        },
        {
            "id": "10a9f8ee-1fc7-4df7-bf8e-4b106098cdfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 511
        },
        {
            "id": "ad2bd9b2-f14b-4407-8cb4-670d8657cc6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 536
        },
        {
            "id": "79e04843-b3d8-4cb0-a6f5-81dc0e90d478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 539
        },
        {
            "id": "be4353c7-7d6a-4824-9d02-67066f7ed6c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7809
        },
        {
            "id": "74b4d2dd-a30f-47ad-8e7d-8a5fe337db80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7811
        },
        {
            "id": "60b4c363-7bbb-463d-a0bb-30909a829e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7813
        },
        {
            "id": "b8a4322b-e204-4a60-8f94-95d0e709919b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7840
        },
        {
            "id": "81478bb1-9f92-42b9-9e1b-8762ab781a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7841
        },
        {
            "id": "ce86ba8a-3ae8-46ca-acc3-6a37face7358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7842
        },
        {
            "id": "06ad1dbd-076f-42ed-b93b-55a95145cd07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7843
        },
        {
            "id": "bd9244b2-4775-472a-b572-c9921fa7196a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7844
        },
        {
            "id": "da29678b-7539-4eab-ba9f-41be30d3b269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7845
        },
        {
            "id": "e45fff71-7eca-42ad-a0c8-fed595dfe451",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7846
        },
        {
            "id": "594555ea-e321-45c2-b2bf-6b1ce8df3dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7847
        },
        {
            "id": "577aabe4-cf0c-4746-aab0-d30a640c7f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7848
        },
        {
            "id": "69148856-a552-4689-9688-199513d2c7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7849
        },
        {
            "id": "ca34fc25-806c-47dd-904d-136e8aaf00cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7850
        },
        {
            "id": "195de18c-39b1-4516-a57e-1ce35d8fe2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7851
        },
        {
            "id": "7d36410a-ce09-44b3-83e9-bfca9b12b793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7852
        },
        {
            "id": "abdd86a6-7321-4e89-8f2e-9676b901762a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7853
        },
        {
            "id": "cbd9a5bf-7553-4b46-a077-8bc4942ef084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7854
        },
        {
            "id": "3dae0b8d-1849-425b-b9ef-57dbaca651c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7855
        },
        {
            "id": "95c43283-2abb-45f5-a2bb-aad0d607fc31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7856
        },
        {
            "id": "31c5845a-2dbd-4f05-8242-98dfde362e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7857
        },
        {
            "id": "53dd6681-1603-49e6-be29-0d5f565e5729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7858
        },
        {
            "id": "4c3e8b4a-3859-4a6e-8f60-8aa2eb8946b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7859
        },
        {
            "id": "9b5b5710-a5d9-4c8d-8f96-52bfaab47da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7860
        },
        {
            "id": "9bf7a800-25f0-4dc6-9704-ad45d76ee54a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7861
        },
        {
            "id": "4558dcad-34df-430b-bace-43aead38c9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7862
        },
        {
            "id": "436ae46c-c272-48bb-a7c2-020457540140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7863
        },
        {
            "id": "aa29a91d-6924-4f7e-b0f6-15f713310068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7865
        },
        {
            "id": "13f3832f-7b98-4c47-a2dd-2a150ae29a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7867
        },
        {
            "id": "e1e06133-e810-499d-9243-7f16919a6a38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7869
        },
        {
            "id": "3b17bbd5-64e5-4bde-920f-d8cc441c769c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7871
        },
        {
            "id": "d40c2dfd-0308-4f33-a80b-2d3027083ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7873
        },
        {
            "id": "f349999a-fd92-4102-a75e-36d40d4d2004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7875
        },
        {
            "id": "9a4332bb-1c77-4224-a6f5-97387f9e239d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7877
        },
        {
            "id": "d98aedb8-fdcc-49b2-a615-d23fdd649b83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7879
        },
        {
            "id": "4b7531a0-13e0-497c-aa80-eac8abb5f874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7884
        },
        {
            "id": "0eaa1ac8-ff73-4d7b-a904-0d3ba9a7a065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7885
        },
        {
            "id": "de5006d6-d3e2-4d3e-b221-878ce88e8727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7886
        },
        {
            "id": "24741c03-3f91-4d05-9051-d301e799f9cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7887
        },
        {
            "id": "cf5141cf-0840-4e54-9a97-2ada893421ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7888
        },
        {
            "id": "481670c8-38dc-4dd5-a515-f98672058d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7889
        },
        {
            "id": "5c5f6a98-d79e-49c7-a292-b10e587ac678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7890
        },
        {
            "id": "06f3cd9e-3917-4a5e-ae06-741003a40d1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7891
        },
        {
            "id": "11ecbcd5-0351-4eb9-9486-a979a495b27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7892
        },
        {
            "id": "86be02d1-c385-45a5-9284-0a7a24b63ea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7893
        },
        {
            "id": "8e13f32e-3d52-4ee4-b490-115251f14f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7894
        },
        {
            "id": "bb9c2045-753a-4810-98e5-bd5bde7b68ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7895
        },
        {
            "id": "06226a77-d20b-4dc1-952a-f608fa8eb11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7896
        },
        {
            "id": "e9242757-4f26-4495-a19f-7a52bdd77ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7897
        },
        {
            "id": "c87126f6-30e4-4df8-8f71-d5d238a00407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7898
        },
        {
            "id": "7a6fa6ae-b72e-407d-a03f-3ff1e7e34645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7899
        },
        {
            "id": "9dd604f5-fac8-42ad-851c-dac379c5bd67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7900
        },
        {
            "id": "c551d8ec-813e-412f-933b-b7e3c52b545b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7901
        },
        {
            "id": "bbc755fe-eb9d-450b-ac2c-9df3b64c7f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7902
        },
        {
            "id": "05430354-068f-446d-adf5-03370cdfd33a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7903
        },
        {
            "id": "b475e766-5565-49b4-986c-f3aaa82f53a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7904
        },
        {
            "id": "d4e2b4e1-a4d9-4006-a432-c650c755b81f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7905
        },
        {
            "id": "b01b42c4-77e6-4990-b6f9-6ebe26d604cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7906
        },
        {
            "id": "70db4723-dabb-4215-942a-262f953f5e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 7907
        },
        {
            "id": "392b2307-a8ff-4b62-a470-d066bd6f7f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 7923
        },
        {
            "id": "f25fe6a4-74ed-4552-8c7b-1ec57f31dab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 7925
        },
        {
            "id": "c8d86dc2-f207-4bee-8bf5-7d32539bc662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 7927
        },
        {
            "id": "7404752c-9e4e-492a-afe5-c613a2d4842d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 7929
        },
        {
            "id": "cda2e283-9ad9-42e4-838d-460b4f22b291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 8208
        },
        {
            "id": "03128820-4598-43f4-84ff-05f3e60631b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 64256
        },
        {
            "id": "3b5d9f1b-21f6-4a60-9df4-1a8d8dc8b62b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 64257
        },
        {
            "id": "32be40f7-a01c-478f-9617-d93244d8248d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 64258
        },
        {
            "id": "ccfce719-3fd2-48a1-bf0f-13af5b222f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 64259
        },
        {
            "id": "e45a1908-519e-4088-a868-709d0f0b5be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 64260
        },
        {
            "id": "8c063188-8eb7-4f05-8663-5c82995288c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "056ae980-de33-4b6d-9237-d2c93eaecfd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 46
        },
        {
            "id": "835ce227-b253-4579-9481-13cc84725e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 47
        },
        {
            "id": "61ea6b3e-5307-4204-9f36-1cc3896a0a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "0618bb16-a209-450b-88b9-f386b9d236fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 74
        },
        {
            "id": "0fabb785-dfa8-44f3-9897-5b97e4ed5906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 83
        },
        {
            "id": "3c796565-cde0-4499-a0f1-0c2774e8ba41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 88
        },
        {
            "id": "f45a4edf-bdc3-4ce8-a1c8-b178f9235656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 97
        },
        {
            "id": "310120ff-a9ce-40d4-8e45-39fa18f83931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 99
        },
        {
            "id": "ce5c6682-f689-4309-8cfd-040e78ac91f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 100
        },
        {
            "id": "6de5536d-c609-471f-b759-6492ea98402a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "5d12a14a-d896-4fe2-ae31-3cce7a739fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "5ccc8974-c980-4c5a-a43e-e3ceb3510ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 113
        },
        {
            "id": "01c3a147-be11-424a-8375-e5cf1ab5c4ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 115
        },
        {
            "id": "d641a309-fa2a-4f20-8fff-f35101837ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 192
        },
        {
            "id": "9ff005fe-b4de-412f-a05a-19dbddbb3b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 193
        },
        {
            "id": "71986624-cdce-4e21-88f1-fb779e982aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 194
        },
        {
            "id": "b94e6393-7dba-4365-91fb-c738b3a6121b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 195
        },
        {
            "id": "da775a46-6208-450e-b518-8202457997ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 196
        },
        {
            "id": "990a7591-6e19-4ef6-9ca5-16244444647a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 197
        },
        {
            "id": "ecc53034-21f9-49a0-8e3d-02f89998fd1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 198
        },
        {
            "id": "1a4bd326-6a43-49ed-ac95-503eeb52cecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 224
        },
        {
            "id": "c38ff8a3-3dbd-4b57-8b2a-e28dfee3241f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 225
        },
        {
            "id": "5f410344-8aec-4a53-82d1-dfb82b5f8b58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 226
        },
        {
            "id": "d0c9744f-26ee-4396-9889-26a517135905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 227
        },
        {
            "id": "887f52e8-70e2-46b1-96b5-dc2d5a278787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 228
        },
        {
            "id": "2f2d7aa9-4bcd-4535-9ad6-d35e165da0d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 229
        },
        {
            "id": "5e535570-f05e-4822-b16a-89056307db52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 230
        },
        {
            "id": "3f85d692-e39b-4eac-b648-bea2b5ee90b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 231
        },
        {
            "id": "11203352-500f-4804-8138-1d1af54ae346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 232
        },
        {
            "id": "480f3347-63d8-4335-9f0e-7df676126612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 233
        },
        {
            "id": "982e023f-9014-49e6-b32f-6d438211abe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 234
        },
        {
            "id": "4d830649-32dc-41ab-9fd4-5c4c361027ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 235
        },
        {
            "id": "9a2152b2-0eef-4449-ad0b-868c78c6cc00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 238
        },
        {
            "id": "4cd542c8-7a5c-4820-9413-34985fc5689b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 239
        },
        {
            "id": "ee2421fa-cd68-4be5-ab34-7c1ff226309e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 242
        },
        {
            "id": "6488f7e5-0e04-489a-b1b9-8908e5ad556a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 243
        },
        {
            "id": "0f6fd337-7c34-4a1c-814b-391e3e53bbee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 244
        },
        {
            "id": "2400150a-d625-40e4-b69c-c7471a70b92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 245
        },
        {
            "id": "6d41ea53-fa11-499b-85b2-c3bd0b079f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 246
        },
        {
            "id": "f8c6692a-1529-4526-b2fd-32e0f0fa9df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 248
        },
        {
            "id": "d9bfc610-c8d1-4dca-89a2-244f699d0e21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 256
        },
        {
            "id": "6c6395e3-3e3c-4715-9972-288659a19f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 257
        },
        {
            "id": "b5bee513-2969-4270-b8fe-a3c4cbdef7ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 258
        },
        {
            "id": "7643ecc1-8db3-486e-ad08-5ff3b93dbe45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 259
        },
        {
            "id": "5c503b14-9d9f-441d-a560-13dccc3d0cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 260
        },
        {
            "id": "071e8781-c79b-4733-8840-e7a6e9a42f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 263
        },
        {
            "id": "479e7241-aa27-42b2-8766-a4b3a207010a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 265
        },
        {
            "id": "7565540e-8108-4278-865f-48d7d688c545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 267
        },
        {
            "id": "cce7e530-9765-495d-9849-3fc4782c01e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 269
        },
        {
            "id": "7ecbf3a7-1218-43cd-818e-c6987918c011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 275
        },
        {
            "id": "c3d69ade-e894-4ea9-bdea-fa3e1e5c5adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 277
        },
        {
            "id": "b430b526-ed72-4872-bc43-58821a5f6194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 279
        },
        {
            "id": "1654a992-a6a5-474f-bd98-03fb98cb62b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 281
        },
        {
            "id": "6a93856e-2ae4-4f8a-9889-78b3e3021d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 283
        },
        {
            "id": "1b1c4b15-85a0-4bd2-b9fe-46456fb4c915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 70,
            "second": 297
        },
        {
            "id": "065b397b-93ce-4704-89eb-603b26b91437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 299
        },
        {
            "id": "332cc2fd-811c-4c34-bc32-cb0828ac3e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 301
        },
        {
            "id": "8e6f2430-125b-494a-98e7-1cf78181f130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 308
        },
        {
            "id": "584704ba-1f99-4e0c-a1cb-e8ba317b851c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 309
        },
        {
            "id": "0f1736ad-2617-4cbc-b247-bd46a2185113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 333
        },
        {
            "id": "4da37ca8-dfaa-428f-b65b-4d3677671c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 335
        },
        {
            "id": "65783190-2938-47b3-b078-661d50997544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 337
        },
        {
            "id": "5e764f63-1480-41cd-a8df-7d47ef86776e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 339
        },
        {
            "id": "557b2837-823b-4bf3-a6b5-758dfacecaae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 346
        },
        {
            "id": "693af43f-c056-44ac-9df3-79a74f21a491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 347
        },
        {
            "id": "ef0f1c90-9f58-4d81-b914-9ccf794c29ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 348
        },
        {
            "id": "7b58448f-a209-4e30-b929-dc8e1043df02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 349
        },
        {
            "id": "62e9430b-7a62-48c3-8aca-bf9da8a094f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 350
        },
        {
            "id": "1d9339bc-bc40-4f52-abc3-7c216d33192d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 351
        },
        {
            "id": "f84a7856-dd45-45f7-9b52-1d8d9255f829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 352
        },
        {
            "id": "d8564dcd-3be3-4cca-8af0-f717d6a9d14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 353
        },
        {
            "id": "3bf657f9-c3f9-4016-af35-b9a80a7bf0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 417
        },
        {
            "id": "00c93efa-ea2d-49ad-a641-cbad9cdab54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 506
        },
        {
            "id": "0dd65b45-4677-4f5a-a8b4-3612f21bbc1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 507
        },
        {
            "id": "e56e6a92-85e8-4474-893a-4f1eaf6c4b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 511
        },
        {
            "id": "d2320ad1-1042-464f-910a-8dbbdf4c6fa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 536
        },
        {
            "id": "883d2aa7-93ee-4390-814a-0cbc9e08abbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 537
        },
        {
            "id": "ade90112-66a4-47c1-a2aa-184c35666f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7840
        },
        {
            "id": "6f235e89-8061-4fd6-b070-0702d23b5e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7841
        },
        {
            "id": "b71a39f9-4e05-4c98-9851-b81a0bb6eeb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7842
        },
        {
            "id": "4ba316f2-394b-4d01-9d98-0e8c21a1f19f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7843
        },
        {
            "id": "2ca452ad-5b24-41ab-b716-9816af83f1c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7844
        },
        {
            "id": "9011617d-f4a2-4e8b-a6c1-2494df450741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7845
        },
        {
            "id": "4a35a9f8-e4d0-461c-a128-579c731017e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7846
        },
        {
            "id": "c096bfbc-6da3-4825-b0b1-8ff09948fb1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7847
        },
        {
            "id": "bd022c20-fc7c-412a-a077-8a4922f914a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7848
        },
        {
            "id": "34376ebe-d96d-4087-af7c-ce9dbb77ced3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7849
        },
        {
            "id": "58ace399-bbc7-43d7-ac4c-7f555a94f063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7850
        },
        {
            "id": "d178378b-625b-4f94-b7a4-58ec83cebf38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7851
        },
        {
            "id": "d3ef1eb6-d4b6-443e-8c1f-a7ce59aba39c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7852
        },
        {
            "id": "9e95c15c-802d-429e-843f-01521bfef25d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7853
        },
        {
            "id": "f419583b-ad1c-46b6-9552-a860dd8912a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7854
        },
        {
            "id": "2c824417-72c9-46df-b5ae-97a0ec8e8c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7855
        },
        {
            "id": "ca5b7a12-bc15-4e62-b641-e0a9eecf53a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7856
        },
        {
            "id": "88341d4c-6f73-47cd-aef2-827a18bc7d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7857
        },
        {
            "id": "3aa95fe4-c97d-4282-ae8a-fb7badc62b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7858
        },
        {
            "id": "9860676b-4626-4280-ad0e-2af0af511bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7859
        },
        {
            "id": "85bb9ba4-f9a6-456e-8750-bbaeeb3d56e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7860
        },
        {
            "id": "f7bbde3b-8e78-4abf-a38d-420ffbe1ef24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7861
        },
        {
            "id": "72ebf4cc-4f9c-4acc-8e4b-9d410f44d3d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 7862
        },
        {
            "id": "09dc9ae2-9edc-42bb-b728-6df1784c756d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 7863
        },
        {
            "id": "e65e7c60-5dcc-40dc-937e-489a84a07683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7865
        },
        {
            "id": "255c5e68-4494-468d-a2c6-bdd90a5396f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7867
        },
        {
            "id": "ed21e7d2-74bb-4e3c-a9b9-b74362e4cf51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7869
        },
        {
            "id": "45e7dc66-2b06-4b29-af44-ca5ec6fd6e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7871
        },
        {
            "id": "ffbe4153-9db2-4349-9bd1-4db366ec4746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7873
        },
        {
            "id": "55ad97be-63bc-4ea1-a3ed-7873841482ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7875
        },
        {
            "id": "746874c7-640a-4b78-9e80-20cf01afdb05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7877
        },
        {
            "id": "0daa698d-0976-4f50-8150-a43225969dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7879
        },
        {
            "id": "488e9130-ba19-4a72-9230-80521bfb7fd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7885
        },
        {
            "id": "11164514-b9f1-4028-9a88-84e67c2bf0eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7887
        },
        {
            "id": "10a69cc2-cedd-47c2-99f5-f8653633321f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7889
        },
        {
            "id": "891a36b0-7f41-43d8-8778-b338aa3c9d4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7891
        },
        {
            "id": "239bc444-3317-4e06-b60a-f443c1336eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7893
        },
        {
            "id": "5d26fad7-2931-49e8-88c6-618d733d6d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7895
        },
        {
            "id": "550c07d1-e3f5-4484-8bcc-461665ae6479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7897
        },
        {
            "id": "4e819de2-a5fd-49e4-bb44-f280344f93d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7899
        },
        {
            "id": "9472c90f-c572-4254-9650-b9244f07c1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7901
        },
        {
            "id": "782c7da1-acae-4eb0-a8c1-23a20acd18e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7903
        },
        {
            "id": "9d192ccd-55ea-4c31-85e4-23c2058981e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7905
        },
        {
            "id": "0d152513-9260-445d-b2fe-061dbb1d3fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7907
        },
        {
            "id": "882817d5-7be6-4db1-81b4-a434015becd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "5ed004ba-7666-440e-adf2-60290a65bda9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "5da82589-b967-4f7f-93fe-f410f6fd01ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 119
        },
        {
            "id": "7442f41a-3a5a-4c27-8550-2a7ed542825a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 120
        },
        {
            "id": "359baf62-3246-4b10-b754-cbf6d152fba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "5294b52b-6e13-4810-979a-b39911f36612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 221
        },
        {
            "id": "f1f58449-f798-4054-97e4-c37ae0fba8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 253
        },
        {
            "id": "8711b12a-7e51-4117-9c78-aa523c4cd149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 255
        },
        {
            "id": "e115381a-c0eb-453e-8e80-fe2d376d749c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 373
        },
        {
            "id": "cb5ab7bc-f911-4735-a630-bd0454670ad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 374
        },
        {
            "id": "3479688f-89ac-4978-87a9-e687107f1c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 375
        },
        {
            "id": "db50bb0b-d626-4b91-911b-2f6ad0f9c263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 376
        },
        {
            "id": "50f50766-c716-446a-8d7d-6e28745fd19d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7809
        },
        {
            "id": "36bfb80d-cc1c-43fc-9194-c456b4d7dc3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7811
        },
        {
            "id": "3defba3c-e6d3-4792-b7fc-c24c516b4b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7813
        },
        {
            "id": "fb944f75-5d1f-4b8a-87a1-abdd62c87315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7922
        },
        {
            "id": "4b2adb24-7f5a-4f59-8602-9da3b44244ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7923
        },
        {
            "id": "d00717d6-b688-4c81-9d1e-8341e2b86225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7924
        },
        {
            "id": "b87070cc-253f-4105-941c-25bb8850d586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7925
        },
        {
            "id": "43779037-1218-46f7-8b43-ff90bc464ae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7926
        },
        {
            "id": "f975ce90-b8d4-485c-88de-d05accd17361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7927
        },
        {
            "id": "37d3ede5-a34a-4ffd-936e-9af59656bd52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7928
        },
        {
            "id": "36557393-e20f-4e0c-ae67-0c0b8a4f0ee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 7929
        },
        {
            "id": "b759a28a-4b44-48a8-a088-f3063bd33b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 297
        },
        {
            "id": "4f8bb276-fdc8-4dbf-879b-b78732fa92e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "f9abc644-7b1a-4dbd-8f76-ac46c8f1fcef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "b292c064-0679-44f5-a08a-4df6db989f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 88
        },
        {
            "id": "0aed3e62-e79c-43bb-8d0e-8b95c7fd010e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 192
        },
        {
            "id": "8cae9cd6-cfe6-4af3-a6d6-eab1de1f9447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 193
        },
        {
            "id": "dbb14964-774f-4283-ab24-21054cd816cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 194
        },
        {
            "id": "91d7b4d6-4edd-42c9-adb5-35946daf2b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 195
        },
        {
            "id": "60e151e1-75f8-4183-be15-fee5832e9c43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 196
        },
        {
            "id": "35aa80a6-c9f0-4c1d-8512-e945ffcc3cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "d31a4be0-fbe5-4beb-ab94-8f8a4238e8e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 198
        },
        {
            "id": "4bb47439-fc1f-4c52-8a93-bc0cbe848d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 256
        },
        {
            "id": "1ba2c96e-9ac6-480e-a9d7-151bdfd4b56d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 258
        },
        {
            "id": "f1e3d38a-a938-49fb-8e57-96c2bdb348b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 260
        },
        {
            "id": "a8bf4c7a-633c-4b65-b804-4953b8f27f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 506
        },
        {
            "id": "4e002afc-840e-49b8-8b21-4ceb2c4b7756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7840
        },
        {
            "id": "743b375b-7a24-4d9d-bff5-d2814fb53ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7842
        },
        {
            "id": "a5ade6cc-1aa3-4b3c-abcb-65cddc5605d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7844
        },
        {
            "id": "9c58945e-2a4b-46bf-9806-11616382640d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7846
        },
        {
            "id": "e388005f-d999-4575-a128-be63c1c42b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7848
        },
        {
            "id": "32022006-4891-4f96-87cf-0ee0e8efe87c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7850
        },
        {
            "id": "7550aef5-77d4-48db-89c6-14e631ccaced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7852
        },
        {
            "id": "df09b7fd-8341-4c2d-bc29-ba39e956c54e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7854
        },
        {
            "id": "53dc5970-b246-43c2-b6da-761adcaff15a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7856
        },
        {
            "id": "3c905a71-51e4-41f7-89f0-fd6767f523c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7858
        },
        {
            "id": "eec4c4e7-85e2-4d7f-8353-197b07c252cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7860
        },
        {
            "id": "a86f0a58-3471-4677-8642-9d4415a63351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 7862
        },
        {
            "id": "bb8cf3e5-cec5-4f71-87c7-2a147e49ad68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "bea7115a-0f18-4d6f-b2cf-ef63d38d687c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 67
        },
        {
            "id": "d854465b-4ea9-4007-99c2-b33f1e6a9105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 71
        },
        {
            "id": "5c0eca3c-9d2d-4cb3-b6f1-1c738f839e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 79
        },
        {
            "id": "cb7d374d-aaa7-41e1-b5f8-0a4752bc10be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 81
        },
        {
            "id": "0a170304-45a6-40dc-9b41-6747a2f06fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 85
        },
        {
            "id": "839c3b62-b6f5-45d4-8f4b-06b3a3acbec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "25006aec-dbab-4f84-a5a8-f76fa7c68963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 97
        },
        {
            "id": "a06cd5d6-16b6-46e2-a6df-34f453f4ddaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 99
        },
        {
            "id": "155d886f-3915-4c9e-94ac-0a4f2b0db1d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "ca23d237-b89a-4d5c-a712-3bfc5c6527cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "36bd7d48-d292-44c2-9b15-5fd02db11938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 102
        },
        {
            "id": "dad291a8-b207-4ce9-a77c-10efcbc8fe63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 109
        },
        {
            "id": "4adeb222-c7e0-48ce-bd1a-b49cdec02524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 110
        },
        {
            "id": "3e98fa71-6834-416e-bd0d-e9dc785e4f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "28f7d7ba-850d-4084-9a8a-2fda88da794e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 112
        },
        {
            "id": "4e249cb1-6f56-4285-9f08-db610264c862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 113
        },
        {
            "id": "f9924522-5604-4796-890d-13557ea7c02e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 114
        },
        {
            "id": "449efad3-3bfa-4874-846b-04abd3bae068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "d548874e-0f40-4b64-83ee-687ab72c02be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 117
        },
        {
            "id": "8d95cc32-7b7f-4746-830d-e67f8efe576b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 118
        },
        {
            "id": "20162789-7248-488b-a185-1eafb4cc37a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 119
        },
        {
            "id": "df6eab3d-f1c6-4286-a939-6edd45b26d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 121
        },
        {
            "id": "02894870-b495-447a-b243-d60d3e798aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 173
        },
        {
            "id": "6ad7384d-e548-4d67-9975-e7739987771e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 199
        },
        {
            "id": "f2c2708f-60f1-4d55-81e9-0f1cc343a434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 210
        },
        {
            "id": "49146d71-ebf4-4549-afa4-c8e9152f2cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 211
        },
        {
            "id": "50e00b13-a021-4b45-92df-3e85cfa5c520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 212
        },
        {
            "id": "575dbcfb-7b1c-410f-b066-13d421af6f65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 213
        },
        {
            "id": "f0d8c897-ba25-4636-b303-89067f9b480b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 214
        },
        {
            "id": "1ec2b0a2-53a3-4931-a8e2-05f4c6c4713e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 217
        },
        {
            "id": "e03a9557-3807-41a5-ac90-a18cb453a04b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 218
        },
        {
            "id": "6a0a9386-32cb-4219-9948-33b85c959599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 219
        },
        {
            "id": "14e88835-2479-43a0-952e-d2cce9d66fa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 220
        },
        {
            "id": "953fbd56-b6f4-4765-bd7d-ac177c0dad9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 224
        },
        {
            "id": "a9cfc641-29b7-4b02-9b15-8864d5c8aa48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 225
        },
        {
            "id": "40a6af6a-3ea5-414b-addd-9309bdfa7b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 226
        },
        {
            "id": "e6a3b8fa-eb69-4be4-a69a-c8dd9e5bff06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 227
        },
        {
            "id": "4f516267-f9da-4891-875f-f47c3e3dc59a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 228
        },
        {
            "id": "971c2c55-9b24-47d2-bb89-7670d5b20098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 229
        },
        {
            "id": "8ef2ca7f-f334-4101-971e-9cb9da04b042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 230
        },
        {
            "id": "1702ae03-b41b-4749-9040-976281efa0aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 231
        },
        {
            "id": "1b353ee6-d3fa-4b10-9bf9-a491e57751c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 232
        },
        {
            "id": "1918829c-ab15-43d0-af27-b31e1d8e3875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 233
        },
        {
            "id": "305afe10-7d73-4d1c-91e8-aafb1413c63e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 234
        },
        {
            "id": "a92c0e57-067e-4c25-96b1-e923c5b939bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 235
        },
        {
            "id": "2309ddd2-9091-4056-b973-7f0ebcdcc66a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 236
        },
        {
            "id": "565d2523-118d-4e3c-b212-a0fc7d61c24a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 238
        },
        {
            "id": "529f4c9a-9df5-4927-b7e4-125068afbe8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 239
        },
        {
            "id": "86c76489-301b-4aee-a5a9-61fb43abf7dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 241
        },
        {
            "id": "2ba7e4d4-fbbd-493f-82e1-4890f6970cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 242
        },
        {
            "id": "d64ca406-b410-47e6-9d49-3c475293ac06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 243
        },
        {
            "id": "a5d622d5-cf0a-4687-a800-003d26ac53c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 244
        },
        {
            "id": "2fb5c00d-8808-40e8-80cd-d912ce9f8329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 245
        },
        {
            "id": "129e5e1a-4c42-4e74-8e13-61a1e3b179fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 246
        },
        {
            "id": "9a4eb23d-c09c-4f92-8f0f-fc46fa8592ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 248
        },
        {
            "id": "3e334e70-81be-46c7-a21c-50d9ed70aec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 249
        },
        {
            "id": "a6ac7062-1033-4444-8d4d-1842464aaa3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 250
        },
        {
            "id": "05ee4cce-aabd-40b2-a5f2-cd8dd9b8e76a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 251
        },
        {
            "id": "2a4d2579-df80-4078-b632-be15413f0511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 252
        },
        {
            "id": "016f1029-4564-47a7-92fe-ead070060706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 253
        },
        {
            "id": "47c1ddac-77cc-496a-9a6a-464d6b6ffe7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 255
        },
        {
            "id": "87560a83-e4af-4ed6-bc7b-0e161f898089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 257
        },
        {
            "id": "4d86847c-fd80-4629-bb55-01d849326b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 259
        },
        {
            "id": "d53933b6-7f99-4c62-8ad2-6aab2b9bd9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 262
        },
        {
            "id": "a3848bc7-4e50-4841-9a92-304a35893665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 263
        },
        {
            "id": "355b7c14-6cfe-4509-940d-1411b397684c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 264
        },
        {
            "id": "e07c9a40-caf0-4dd9-b53a-e06190755f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 265
        },
        {
            "id": "3b80109f-ae9f-4e55-88b4-e2fe25881942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 266
        },
        {
            "id": "ab64341a-8c87-4d9b-914e-d49375465ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 267
        },
        {
            "id": "131d1d57-6b58-4060-b040-c2471e78b27b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 268
        },
        {
            "id": "9c2f702c-ce51-4b3a-a763-8ed2ad45b0ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 269
        },
        {
            "id": "3156b491-6344-438e-8869-bbb1a7a5a7db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 275
        },
        {
            "id": "744ad557-32f8-4dde-9e30-0dd30b0a7719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 277
        },
        {
            "id": "68d7a5e1-b635-4461-af4b-2139eaa4f378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 279
        },
        {
            "id": "a47245a0-283a-42a9-a3e4-eab087ee1b2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 281
        },
        {
            "id": "98e858ef-4f98-4b22-900a-18df87ce6ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 283
        },
        {
            "id": "bce74bc8-4ffa-4724-b47f-4010d9c03314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 284
        },
        {
            "id": "49ab09d4-1b61-4f6f-87e5-2d06c40e5cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 286
        },
        {
            "id": "02d85ee6-f7dc-49b9-973b-da74050d2b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 288
        },
        {
            "id": "f0b77d5e-adc4-43fe-9599-7b1bec8de293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 290
        },
        {
            "id": "995410a3-85d0-4555-9dd9-5363d7112c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 75,
            "second": 297
        },
        {
            "id": "44628f46-7324-490c-a1ea-299b49e95630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 299
        },
        {
            "id": "79effb01-7f70-4955-8b3b-397af01b7e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 301
        },
        {
            "id": "87f1a0f1-b614-43f5-bdab-756b1744f35c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 305
        },
        {
            "id": "ea97bb12-6cd8-4b36-be6e-64ce619280ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 309
        },
        {
            "id": "8d59fb67-7ea1-4d24-aa15-20a65772eee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 324
        },
        {
            "id": "d8ee7d3f-71d8-4f91-a313-e9240afb86f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 326
        },
        {
            "id": "68dc98f9-9dd0-47b5-89ec-e90a2658057d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 328
        },
        {
            "id": "8136d273-867f-439f-bfd2-52cfe15eb5b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 331
        },
        {
            "id": "9844a291-7289-42a8-9561-b745c73a35a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 332
        },
        {
            "id": "c4a0ba59-c50d-4ea7-b698-5b36c7cdf54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 333
        },
        {
            "id": "7a0b4f93-d246-464a-89d5-dcaf2b39d190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 334
        },
        {
            "id": "2d82ff86-4584-47ef-b773-8ac23d419e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 335
        },
        {
            "id": "c9241232-dcfb-4ace-9a28-deb566598bb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 336
        },
        {
            "id": "86d16d4c-336c-42e1-9cf3-be3b01233617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 337
        },
        {
            "id": "547661ea-4fe8-4237-a0d8-23702f15bb44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 338
        },
        {
            "id": "92aaddab-630f-4c31-8faf-ecf4e9f3983a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 339
        },
        {
            "id": "a583a7f7-118b-42f4-b14f-c9c72b2db750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 341
        },
        {
            "id": "0aa5f00b-6d6b-4554-83be-4487ae34a027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 343
        },
        {
            "id": "d21fb5a7-cb90-4122-8565-087c7ea2fc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 345
        },
        {
            "id": "676da392-5bc6-4d75-9751-0da3b84ed409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 357
        },
        {
            "id": "e060bc5c-3627-4815-9dfb-207692bc9622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 360
        },
        {
            "id": "aedb7c55-555e-4434-9072-f9039d4918ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 361
        },
        {
            "id": "7545f4c0-b867-4c71-9e9b-fae0d05e7b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 362
        },
        {
            "id": "64726f3a-1eaa-44e3-802c-123bac92b600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 363
        },
        {
            "id": "3f5ab57f-c56d-4942-9362-e4cbab356712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 364
        },
        {
            "id": "b8db4dc6-fb03-4c45-8742-e8c368cae151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 365
        },
        {
            "id": "12c845b8-0174-4d84-8a5b-2d9b7f5f803d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 366
        },
        {
            "id": "95bf1677-73a6-4a4e-9a4f-fc89bbe12755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 367
        },
        {
            "id": "a70041f3-929a-41cf-9c7f-edfa229fb246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 368
        },
        {
            "id": "eaf8f00f-4b15-4e2e-8265-5aa3f89956f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 369
        },
        {
            "id": "c587e3da-7a67-42bc-97c5-f98e24f79546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 370
        },
        {
            "id": "d1c9b155-f085-4735-b84b-e573dc1e0aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 371
        },
        {
            "id": "8f138f3e-73f5-4190-a48c-4111a4239589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 372
        },
        {
            "id": "e5a0a516-c6e0-477d-967b-92a0336d20d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 373
        },
        {
            "id": "3b2610dd-894e-4329-9a88-95164072cfb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 375
        },
        {
            "id": "47da6e47-0c3c-432f-b6e5-667f01a7e41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 416
        },
        {
            "id": "fd188e8d-57db-4a20-971d-7d3177a92dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 417
        },
        {
            "id": "7036e454-8137-4ffe-b0eb-4738c11b6fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 431
        },
        {
            "id": "b1c57c80-0384-46bf-8d7d-7a426c6302f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 432
        },
        {
            "id": "fcd6721e-da41-4735-a94d-e64f4f78afad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 507
        },
        {
            "id": "3433bf4c-4957-470c-98f2-85be07a3fd4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 511
        },
        {
            "id": "31584854-ce0e-426a-a8f8-f3d91393ec19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 539
        },
        {
            "id": "7d01b2e5-9c7e-4f33-8100-a98f7cb31896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7808
        },
        {
            "id": "f61ca278-6588-475e-9c38-acdf4f9b4e99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7809
        },
        {
            "id": "1bdc2325-77b8-430f-bdeb-872bbf25a7dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7810
        },
        {
            "id": "c201f75c-2d85-41cd-b875-3070c77eba34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7811
        },
        {
            "id": "1b1e4818-2735-436d-9075-bb8951892bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7812
        },
        {
            "id": "05592be0-6ee7-4512-92ab-c9d52912a45c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7813
        },
        {
            "id": "34abf756-212d-439f-a186-b8c810a3bda8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7841
        },
        {
            "id": "39389752-3713-4c7c-b98e-cb33bfe0b02b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7843
        },
        {
            "id": "7ecccb1a-7719-45b1-a6d2-31bbf364a752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7845
        },
        {
            "id": "b4fab9db-63ef-4bbb-be0c-3586c8a47d96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7847
        },
        {
            "id": "48a3091d-fdb3-444a-b391-eb6d42e327b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7849
        },
        {
            "id": "dabc98cb-e648-402c-b654-d501575c7db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7851
        },
        {
            "id": "e9192cbd-527a-4773-8f27-c83e3ef3e7e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7853
        },
        {
            "id": "031dbc78-e908-4dca-aa1e-b007e9ac247c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7855
        },
        {
            "id": "e3669d85-c8ab-4b0f-997a-6393c92cb945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7857
        },
        {
            "id": "9df32afa-c6df-4f47-a7c9-97af902b54a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7859
        },
        {
            "id": "30826551-3b04-465e-a0d1-ebc16f7c4d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7861
        },
        {
            "id": "10d25593-0f82-481c-9303-8ccc0b1ce878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7863
        },
        {
            "id": "4a2bd614-7a32-4b10-b700-a5318916423e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7865
        },
        {
            "id": "94e05d7d-34ee-4c39-ab70-000cb9ef77e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7867
        },
        {
            "id": "43d86a66-6c5e-4288-b455-0debf057a2e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7869
        },
        {
            "id": "4484ba22-b3a8-4759-89a6-7d9d5a6b5825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7871
        },
        {
            "id": "9b944b16-18cb-4338-b1ba-392c89ed43b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7873
        },
        {
            "id": "dd2386bf-bdf4-4bf2-979d-d2106bca8132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7875
        },
        {
            "id": "266a5854-da98-4e07-887c-af055b765218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7877
        },
        {
            "id": "1c1aa076-7502-454e-9c8f-f4f2fa348ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7879
        },
        {
            "id": "4cbc9448-4bb6-4fed-a748-87b1b6e216fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7884
        },
        {
            "id": "c78417be-856b-45a1-80a8-0222013a4ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7885
        },
        {
            "id": "17af07a7-c0c6-4fac-9942-c103a968bf49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7886
        },
        {
            "id": "7d21e59f-6c19-44fb-be17-5fead5820d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7887
        },
        {
            "id": "c0490233-d7c8-46c8-b392-925e40545773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7888
        },
        {
            "id": "46170c0a-9c08-446b-8e59-b592541e29de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7889
        },
        {
            "id": "2ee53d0d-2dd7-4c64-8b77-9833ae72bc15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7890
        },
        {
            "id": "7a8ad58a-20fb-4c4e-9ba9-8981c51951c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7891
        },
        {
            "id": "bb2303dc-2459-4855-9709-16fa92fdfaf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7892
        },
        {
            "id": "c027ae8d-7b0c-4322-bd42-221d1aa32bdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7893
        },
        {
            "id": "202f5b18-088e-493a-8661-6816a829a61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7894
        },
        {
            "id": "3e5ec4e1-67f8-4c6c-b106-795834153bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7895
        },
        {
            "id": "861c846a-fc42-4bcb-99b4-262b74ba7755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7896
        },
        {
            "id": "4ed5e229-3603-4822-81fa-8a9d11473062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7897
        },
        {
            "id": "215b3d4b-1b58-432e-8449-a1b267716448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7898
        },
        {
            "id": "1506ae05-10df-41fa-ad3b-aeb603173b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7899
        },
        {
            "id": "fb6407ac-7377-45e2-95c4-175f2d253f85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7900
        },
        {
            "id": "ef0cc380-153e-4678-bcf1-6504ca384361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7901
        },
        {
            "id": "4800ad82-6d53-4238-a254-110b04b7cef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7902
        },
        {
            "id": "4f1813d2-9999-4482-8a3b-d5f16834463b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7903
        },
        {
            "id": "c9fbf13f-ad8f-42c4-a5ae-b5822893c9df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7904
        },
        {
            "id": "186567b2-d352-474e-8a84-bd4536bf5139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7905
        },
        {
            "id": "cab92958-2685-4430-8994-778cf2883f98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 7906
        },
        {
            "id": "43a576d2-8add-48f6-b783-d6d8952d9bce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7907
        },
        {
            "id": "8d7cf812-5524-4c81-b616-22994ea61849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7908
        },
        {
            "id": "17dfed02-761a-4d8d-8fe8-2eea39ff7a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7909
        },
        {
            "id": "211fe87b-a8a6-438f-9517-57e8716dd1d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7910
        },
        {
            "id": "f985fff5-bc42-444a-93c1-d418a9acd6fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7911
        },
        {
            "id": "76da754a-828e-4e3c-95f9-d7b35cf7c461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7912
        },
        {
            "id": "9a2d939e-8827-4638-9032-c0a494bfc40e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7913
        },
        {
            "id": "87713103-96b2-4884-acc7-2a6d4cf173e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7914
        },
        {
            "id": "f65ac803-2bb3-43c3-904b-823a7769afcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7915
        },
        {
            "id": "4e6db515-cc26-49ec-b7be-114f5e4e44d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7916
        },
        {
            "id": "a8fa6fa2-ab6e-4f98-9a7c-05aa3915bcc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7917
        },
        {
            "id": "4c8e672d-5a16-45dd-8b69-46689116f1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7918
        },
        {
            "id": "bfa3c94a-70cf-4c39-a34b-91dc6c0e2539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7919
        },
        {
            "id": "663510bc-30a8-4c06-b35d-908ca721724b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7920
        },
        {
            "id": "00e40b3e-8edd-4a72-82c7-89fa014af04f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7921
        },
        {
            "id": "0c3dec9c-3309-4850-aa85-2ba10fd1ec50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7923
        },
        {
            "id": "18d9bd6f-10f3-47bb-a6c1-a73dd2f7c3dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7925
        },
        {
            "id": "08019e82-4264-4fb4-bcbe-707f30839369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7927
        },
        {
            "id": "c7126c0e-c2a8-4cd8-bd9c-8e1b59248fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 7929
        },
        {
            "id": "85a2cc8f-b423-4ee1-b6a0-b933fff30b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8208
        },
        {
            "id": "551c2374-55eb-4df2-b805-fe9a118caf5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64256
        },
        {
            "id": "9717d452-889d-498e-8220-aca9c26be748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64257
        },
        {
            "id": "9b548b1d-ed30-495e-8049-3fe6ff057d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64258
        },
        {
            "id": "92999760-32c0-4093-99b5-607522b24d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64259
        },
        {
            "id": "f4cb5b64-d00f-4b15-a1b1-04209ed2f5ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64260
        },
        {
            "id": "90f47405-0a4e-407f-af33-c96a0ec4b26f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 44
        },
        {
            "id": "af28475d-c01b-4923-93cc-d6ad892e6a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "5efb191f-4ff1-4cc4-bceb-3b3af7821960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "6a247f66-d2ed-4859-9cb3-8aade8938a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "90e80de9-e2ef-4af8-a131-61b130b14a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "9b1df533-a221-4d3b-8bbd-f6f933fda57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "a695126a-5072-4a90-9f15-02f897405226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "16c8c234-5815-4d3b-9d70-de20af4876bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "2cc090a0-9b4d-4df4-9382-9f881654b7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 86
        },
        {
            "id": "6c69f245-20e1-4cd1-87bc-c083b3c976a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "c3374681-5b55-4003-a3c8-a22dd23efd78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 89
        },
        {
            "id": "76b01dc6-fd0a-4195-a1a6-898d723bcc3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 102
        },
        {
            "id": "c33ff3af-c11f-4ac1-b076-a8e3c8dc4073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 116
        },
        {
            "id": "486a6103-0c0f-4b65-a129-d719454a71dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 118
        },
        {
            "id": "736d8034-48c6-40c0-b7ff-8bad13f9fc33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 119
        },
        {
            "id": "27610241-f2e2-44ef-98dc-b8722bd896bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "712720d2-3233-4fd4-9cfa-eeaa00984a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 183
        },
        {
            "id": "4dd17d53-f79f-4192-ada0-2c42cf486bf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 198
        },
        {
            "id": "51afbf24-883a-4c77-9ae3-1fcb05129815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "2d982ce9-667f-4ac5-a6a2-05efced5a5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "88ea92ca-2bf8-49b0-b036-caab394370fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "ecb00c91-5390-4498-a971-30e8758eeadb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "ec643098-0dc9-4e08-960e-6f11cf6d8281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "7c98b7a1-85d5-47a8-ad9e-5729de702183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "e5a70bc2-8885-4fc9-a332-d1805f2928b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 217
        },
        {
            "id": "a0c19537-8ecc-4f15-8349-ea36d6c02eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 218
        },
        {
            "id": "7535ced5-ade8-4597-9b1b-e03a3160646a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 219
        },
        {
            "id": "e6ba36ab-d6b9-4376-b0c2-669a3dd39d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "30ec55c2-7382-47d0-bc4e-275ff8eb27d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 221
        },
        {
            "id": "79b96c8a-141a-4dc6-9592-99e587279996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 253
        },
        {
            "id": "c2e1c1ee-8d92-4858-b542-d25090e5e705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 255
        },
        {
            "id": "5ab7547f-656b-4abc-ab9d-42f7a6fd6fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 262
        },
        {
            "id": "99b356a4-3708-4606-9293-10b4b87836a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 264
        },
        {
            "id": "bfbaf526-4184-4c8d-b414-9d7af65497bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 266
        },
        {
            "id": "d56542a6-7b53-4a03-bebb-19bea0125244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 268
        },
        {
            "id": "5c658625-fc3b-4de3-b4ec-ee6980261ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "a6f76d2c-5a40-4b11-9ce2-952385910bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "907bc0a5-6170-4fe8-8107-78cddfcded62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "f74f16a0-f1d0-4ecc-a42b-41382333cdea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "dd1606b4-ca2f-4bd8-88fd-b6750fcda7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 308
        },
        {
            "id": "08f47416-d128-4852-b623-94901463421e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "5b02457a-b5d4-4abb-b6c6-d5eb3c750d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "9a21d3fa-7ff7-4322-9b21-cd259da27ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "a5845f2d-e1c1-443e-ae10-7aa0f87f5384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "a8a67944-69cc-4d91-b094-b42128ac8461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 356
        },
        {
            "id": "6181020e-0184-4aff-b56f-bb1689026ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 357
        },
        {
            "id": "e163c263-d579-45a6-862a-76e5dad00a9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 360
        },
        {
            "id": "57fe5078-2ec9-4fdf-a352-379514c9c53c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 362
        },
        {
            "id": "2d818950-dcc6-4406-86c0-1e1720c5a124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 364
        },
        {
            "id": "bc38ff16-f4e3-4c6e-bfa1-8ae6c778d031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 366
        },
        {
            "id": "099dc795-11a3-4ce7-bbbe-a09e4ff429bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 368
        },
        {
            "id": "e33a0957-5382-4611-83a9-5fc26d67c785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 370
        },
        {
            "id": "263c6bfd-496a-43f1-86da-a4a64f111765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 372
        },
        {
            "id": "5bb52d0d-db96-43c3-b45a-6502f96db823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 373
        },
        {
            "id": "099ac3b9-20b6-4a89-baba-37d62afe469d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 374
        },
        {
            "id": "3d90dbeb-ebd4-4d83-ab99-69f06a0c1cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 375
        },
        {
            "id": "fcc4586b-3253-4efa-b2c1-b9a7a77da037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 376
        },
        {
            "id": "6367efea-2e11-472b-b27a-c2b9ea7bbc49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 416
        },
        {
            "id": "0efb2673-6f8c-4423-ba98-46f8a4f0f1a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 431
        },
        {
            "id": "d6fb6a88-82d0-4ed6-9b24-94f29b52fd9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 538
        },
        {
            "id": "13af6a8a-70b3-4c83-908e-9f1bb08e799e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 539
        },
        {
            "id": "bad5fac1-f172-4fd3-b55f-dbe637006f1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7808
        },
        {
            "id": "9dfef4de-ab8f-4a6a-be11-325dea4c9ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7809
        },
        {
            "id": "28e30c5d-2aae-419e-9b76-7abb4e910359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7810
        },
        {
            "id": "e8a7aac9-8bdd-4056-a256-880188f722f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7811
        },
        {
            "id": "68d25aab-af84-4093-9917-cf52d47cac85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7812
        },
        {
            "id": "1aaa8c2a-7f46-469d-8aaa-c76bf24dab0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7813
        },
        {
            "id": "ec0af476-a832-4c06-8c6e-2a57292cd919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7884
        },
        {
            "id": "d48ff3ee-8da3-4ca4-af51-7f69a8925822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7886
        },
        {
            "id": "6afbee83-689a-4384-8bc9-68c9ac3ba56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7888
        },
        {
            "id": "78decaaf-01bd-4844-832e-c1d6be9c843f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7890
        },
        {
            "id": "b99e6e70-731a-4349-afef-3a9d97ada2ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7892
        },
        {
            "id": "7ffa0875-ab34-40ca-b005-7ef69efae221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7894
        },
        {
            "id": "2223f163-7bab-4510-8253-f39ab34476f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7896
        },
        {
            "id": "9e4aaca7-9577-4bec-ae25-1867a8f49a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7898
        },
        {
            "id": "73ca18b8-a1af-4ebb-ad2e-ffaa8c8027d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7900
        },
        {
            "id": "8cf72bc0-1f28-4683-9a1d-d03ba1142aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7902
        },
        {
            "id": "5c63fbbe-6fbb-4820-9fca-fc7ddf14880b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7904
        },
        {
            "id": "e4391467-659d-4060-aa1c-aceb77736c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7906
        },
        {
            "id": "7a4a1fdb-2ccb-4a86-886c-51ecad0b56f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7908
        },
        {
            "id": "f858e7f0-8df3-4504-b37f-c29868d850b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7910
        },
        {
            "id": "a833939f-4331-4d3a-9010-4a0a2eefd37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7912
        },
        {
            "id": "968089a8-8944-4299-aced-8b8e80e3f37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7914
        },
        {
            "id": "f4e7263a-5eb8-436d-89c3-1b007a30b4fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7916
        },
        {
            "id": "c1fbad29-2872-4b2a-bb20-365ebf401ece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7918
        },
        {
            "id": "799d9ad5-d600-41e9-822b-fd32e23fcc65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7920
        },
        {
            "id": "3f3355f3-6bf2-4eb3-86fc-9d3861459ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 7922
        },
        {
            "id": "79191f93-1dd5-49b9-a645-ec0b51688b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7923
        },
        {
            "id": "400094f8-b1c7-428c-96cd-a60e87f72865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 7924
        },
        {
            "id": "9bec82be-86e7-4082-ad42-427831f58960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7925
        },
        {
            "id": "8d5fd29a-6f44-49bf-8020-df3f4f71c9d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 7926
        },
        {
            "id": "64aec520-c5d4-4bc4-b109-d0c13b9891f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7927
        },
        {
            "id": "f85d2635-73ba-4379-92a8-3c64e9f7dabe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 7928
        },
        {
            "id": "4ed32e4d-2f0d-4074-a709-5c99b91db50d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7929
        },
        {
            "id": "98d10396-1ac6-400a-8531-59756fa58186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 8216
        },
        {
            "id": "91bff422-64a5-4837-b3af-ddaaab9dffee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8217
        },
        {
            "id": "811c6a01-9875-4970-86b5-ef6ac6e621eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 8220
        },
        {
            "id": "b69d7338-dffe-49c0-a5d6-e28861cbdb71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8221
        },
        {
            "id": "662cc788-3aba-443a-bd09-4815f7385437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8729
        },
        {
            "id": "aee379a0-0122-47a8-a6eb-ceb0d5c560d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 64256
        },
        {
            "id": "940c9a78-2d59-4458-a5e5-7e12e1268535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 64257
        },
        {
            "id": "1091753c-ad50-4d14-9dc4-5961c55e0456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 64258
        },
        {
            "id": "a2b89b2c-34ef-4f75-92e6-ebe7cd79defb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 64259
        },
        {
            "id": "2e19a240-3a84-4d8b-835c-aa73925e8a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 64260
        },
        {
            "id": "637f2a23-cb1f-4732-8c9c-d2ead90b51be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "4ae0d630-3236-4fed-8ff3-29b89427dc13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "bf3d71f1-4fe1-413a-8d33-4251129f6b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "48192f05-948e-4502-86aa-34aa91465eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 74
        },
        {
            "id": "1716ddd2-76ac-4b45-8f40-bf1d24923d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "462738ca-4f7e-458a-8547-cbcabfac8d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 86
        },
        {
            "id": "296aa16a-bc67-4983-8bb1-a1a178ed68ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "db08c2e2-d80b-4aa4-ab9b-58802697515a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 88
        },
        {
            "id": "3e259008-56a6-4d8d-bed1-7757c8ce6ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 89
        },
        {
            "id": "4d3cbec4-994a-420e-ae71-cdeab68b5fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "c0123be8-ef44-4f73-adf2-7fc38d43f241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 192
        },
        {
            "id": "08944192-b0e0-4bd3-89c4-76171b531b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 193
        },
        {
            "id": "d70c7825-3a97-437d-a610-4d729141381b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 194
        },
        {
            "id": "87c65d28-a92f-4171-bf7a-57317b74dea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 195
        },
        {
            "id": "9299e5d8-5774-44d1-89c0-0728bd248ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 196
        },
        {
            "id": "d540d047-3941-4c67-b426-ce75880942b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 197
        },
        {
            "id": "3192f6e3-6250-4f51-9cb5-db79ca02a719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 198
        },
        {
            "id": "5f54968b-36e2-42a3-a736-7969ba88db86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 221
        },
        {
            "id": "69786d34-dd88-4bd4-84f8-19622c4822ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 256
        },
        {
            "id": "26dcda6e-eaad-4d22-a1ed-70875eb616a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 258
        },
        {
            "id": "099a4290-273a-4526-b53c-12287cf9d339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 260
        },
        {
            "id": "727614da-21d4-42c0-aa07-a448c34a98df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 308
        },
        {
            "id": "6beb5c81-b697-4ee8-8151-e58d44b7b64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "d188d7b0-4591-4463-a57a-41139fde2da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 372
        },
        {
            "id": "459552e4-5c1f-4531-b419-4702e2aecc39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 374
        },
        {
            "id": "4081dd72-1d48-463e-8e1b-82de96f918b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 376
        },
        {
            "id": "060ec1ec-32a5-4a39-97c6-610012accefb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 377
        },
        {
            "id": "e073b53c-41ca-412f-9107-329b22c68b0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 379
        },
        {
            "id": "3045a9ff-5976-4ab6-9276-b0ebfadea85f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 381
        },
        {
            "id": "b54f1980-f9a9-4511-9906-ee6877d7c5f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 506
        },
        {
            "id": "a9cf9a2f-b557-463b-9399-47c909f11212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 538
        },
        {
            "id": "8550e58a-8625-4e83-9940-9a837c611dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7808
        },
        {
            "id": "3e6b071a-f0f9-4314-8723-c0f47623f740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7810
        },
        {
            "id": "508d4e29-a456-4105-96fa-0e1da79c3f33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7812
        },
        {
            "id": "a25dc310-544d-4fa0-97d7-54799c85381a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7840
        },
        {
            "id": "cc068c00-9c8c-4b55-a5c2-111a84faa280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7842
        },
        {
            "id": "6391bb75-d5cc-4411-9f4f-ac9489a989fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7844
        },
        {
            "id": "f0afa7f8-6dab-46f0-9f2f-e9fad35c6254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7846
        },
        {
            "id": "40ab1332-bcd7-4c91-98c4-9e8b84a9279a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7848
        },
        {
            "id": "65a82311-8f96-482b-9800-31cba80dccd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7850
        },
        {
            "id": "6d5aa532-dd48-4b0f-bbf5-57d820cf090e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7852
        },
        {
            "id": "19b917a3-ad59-4591-969c-6cc3818bef04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7854
        },
        {
            "id": "f7c8f336-14c4-4923-bd4f-8512f83e203d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7856
        },
        {
            "id": "e7c6c8f4-f1a9-4622-9cbf-af2953841d4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7858
        },
        {
            "id": "dfda6125-62bb-4579-9662-baf33f853809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7860
        },
        {
            "id": "a7d79f5c-03cf-4058-a071-519137988b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7862
        },
        {
            "id": "758db995-92ad-469e-9aea-f771b7edb5ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 7922
        },
        {
            "id": "917ff701-9032-480e-9218-2cba0a3ff6ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 7924
        },
        {
            "id": "b06ef922-0f83-4d19-8ed2-65db098b1084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 7926
        },
        {
            "id": "4a55a7db-c80a-4f84-87bb-1abfc8a4c510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 7928
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 45,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}