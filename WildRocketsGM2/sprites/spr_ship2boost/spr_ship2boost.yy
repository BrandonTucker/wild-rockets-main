{
    "id": "2bce0f14-fcbf-4efa-bbdb-0cf28f6eeec5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship2boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 27,
    "bbox_right": 52,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "164e5663-c1f0-4fd7-b097-e05e30a86958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bce0f14-fcbf-4efa-bbdb-0cf28f6eeec5",
            "compositeImage": {
                "id": "65f9ab92-dbc8-4d62-8bcb-11da13015f19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164e5663-c1f0-4fd7-b097-e05e30a86958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3556f7fe-5808-406e-a870-80a9aac2b7a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164e5663-c1f0-4fd7-b097-e05e30a86958",
                    "LayerId": "b78041c2-1be0-4d4a-b2f7-e57021de5e6c"
                }
            ]
        },
        {
            "id": "eb0323d4-c261-4de8-970e-74f8fc5f6e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bce0f14-fcbf-4efa-bbdb-0cf28f6eeec5",
            "compositeImage": {
                "id": "72defe00-88b1-4d07-ba5f-0d0d446a2af1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb0323d4-c261-4de8-970e-74f8fc5f6e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d351259b-b4f0-4edd-8557-e748f0a81ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb0323d4-c261-4de8-970e-74f8fc5f6e3c",
                    "LayerId": "b78041c2-1be0-4d4a-b2f7-e57021de5e6c"
                }
            ]
        },
        {
            "id": "4c55d7dd-0487-4511-9417-c0a57e1d4d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bce0f14-fcbf-4efa-bbdb-0cf28f6eeec5",
            "compositeImage": {
                "id": "531eaa05-837e-4737-b821-1a8ff6e177a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c55d7dd-0487-4511-9417-c0a57e1d4d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01847882-82cb-428b-b89a-b4b188dd2c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c55d7dd-0487-4511-9417-c0a57e1d4d31",
                    "LayerId": "b78041c2-1be0-4d4a-b2f7-e57021de5e6c"
                }
            ]
        },
        {
            "id": "25ef49b3-7115-486d-93ff-160789da1b81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bce0f14-fcbf-4efa-bbdb-0cf28f6eeec5",
            "compositeImage": {
                "id": "654b9d5b-c81d-4b35-92c5-b16f5ce813ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25ef49b3-7115-486d-93ff-160789da1b81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "282451d7-286a-4b43-aa55-3d21267551ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25ef49b3-7115-486d-93ff-160789da1b81",
                    "LayerId": "b78041c2-1be0-4d4a-b2f7-e57021de5e6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "b78041c2-1be0-4d4a-b2f7-e57021de5e6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bce0f14-fcbf-4efa-bbdb-0cf28f6eeec5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 39,
    "yorig": 41
}