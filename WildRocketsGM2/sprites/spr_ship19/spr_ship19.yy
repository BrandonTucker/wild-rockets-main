{
    "id": "57f6fd78-cd14-480d-80f6-608da1915d00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship19",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b9f40b0-de2b-4019-a256-7f4caf1291fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57f6fd78-cd14-480d-80f6-608da1915d00",
            "compositeImage": {
                "id": "4ffb76c2-4cd1-4fad-87de-f0e720da37ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b9f40b0-de2b-4019-a256-7f4caf1291fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21325c63-4a97-4d57-87f7-cb2756625c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b9f40b0-de2b-4019-a256-7f4caf1291fa",
                    "LayerId": "2ef6eff5-3b26-4f20-b8d6-d173025ecb75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 104,
    "layers": [
        {
            "id": "2ef6eff5-3b26-4f20-b8d6-d173025ecb75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57f6fd78-cd14-480d-80f6-608da1915d00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 87,
    "xorig": 43,
    "yorig": 45
}