{
    "id": "2821057b-a48b-4da8-af10-41bbe693e621",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship38explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5806f5c8-4f4b-462c-8a41-761b9ae4ea9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2821057b-a48b-4da8-af10-41bbe693e621",
            "compositeImage": {
                "id": "75798b34-bd9e-4308-a446-6400c16686d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5806f5c8-4f4b-462c-8a41-761b9ae4ea9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af87f069-2685-4691-be18-6612a4e8ecb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5806f5c8-4f4b-462c-8a41-761b9ae4ea9f",
                    "LayerId": "382d93ad-4453-476f-90c0-d12e04d65286"
                }
            ]
        },
        {
            "id": "8b3ff15d-902c-4487-a07f-76360ba2043b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2821057b-a48b-4da8-af10-41bbe693e621",
            "compositeImage": {
                "id": "227d4cb4-1204-48b5-921a-cad8379cae52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3ff15d-902c-4487-a07f-76360ba2043b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5466117-8eb8-4a76-8a70-050290f6e19b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3ff15d-902c-4487-a07f-76360ba2043b",
                    "LayerId": "382d93ad-4453-476f-90c0-d12e04d65286"
                }
            ]
        },
        {
            "id": "2be57c38-20d0-446d-86b1-1fe5464592bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2821057b-a48b-4da8-af10-41bbe693e621",
            "compositeImage": {
                "id": "0bd35c63-1210-4ad8-b0a1-7f2b16fb0661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2be57c38-20d0-446d-86b1-1fe5464592bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c844f0a7-c717-4e1b-963b-a0bc59765e5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2be57c38-20d0-446d-86b1-1fe5464592bd",
                    "LayerId": "382d93ad-4453-476f-90c0-d12e04d65286"
                }
            ]
        },
        {
            "id": "1573121a-aa04-464b-96e9-48a626abb119",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2821057b-a48b-4da8-af10-41bbe693e621",
            "compositeImage": {
                "id": "f81a7de3-63df-4793-85e3-9839ae71ca90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1573121a-aa04-464b-96e9-48a626abb119",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a98fb1f-207b-4d9d-a800-629583bfb479",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1573121a-aa04-464b-96e9-48a626abb119",
                    "LayerId": "382d93ad-4453-476f-90c0-d12e04d65286"
                }
            ]
        },
        {
            "id": "064e9ab3-1724-4396-a6b0-247b37c26616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2821057b-a48b-4da8-af10-41bbe693e621",
            "compositeImage": {
                "id": "79409b97-9a7f-433f-a7b6-66778c3405fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "064e9ab3-1724-4396-a6b0-247b37c26616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a63652d-af9a-463f-88e6-68d336c56207",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "064e9ab3-1724-4396-a6b0-247b37c26616",
                    "LayerId": "382d93ad-4453-476f-90c0-d12e04d65286"
                }
            ]
        },
        {
            "id": "5bddc864-ce98-48cb-ab14-4c035d5ac887",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2821057b-a48b-4da8-af10-41bbe693e621",
            "compositeImage": {
                "id": "ecc9156a-0941-4c8a-8a22-186e44acad47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bddc864-ce98-48cb-ab14-4c035d5ac887",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec97164c-2af7-4f6a-9476-7a6e4fdc21d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bddc864-ce98-48cb-ab14-4c035d5ac887",
                    "LayerId": "382d93ad-4453-476f-90c0-d12e04d65286"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 260,
    "layers": [
        {
            "id": "382d93ad-4453-476f-90c0-d12e04d65286",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2821057b-a48b-4da8-af10-41bbe693e621",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 262,
    "xorig": 133,
    "yorig": 124
}