{
    "id": "603c146c-5190-46ad-a266-7e4342072cb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smoke2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f271d2be-fc7f-4294-ab5b-f15d85eca96b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603c146c-5190-46ad-a266-7e4342072cb8",
            "compositeImage": {
                "id": "c7df0d3e-8bbc-4888-88db-eadc82847b24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f271d2be-fc7f-4294-ab5b-f15d85eca96b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6d42322-4912-4eca-b4c7-a5de6778d331",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f271d2be-fc7f-4294-ab5b-f15d85eca96b",
                    "LayerId": "8cfbce0d-9d2d-42fe-9207-42abca65805d"
                }
            ]
        },
        {
            "id": "39e8346f-55bc-43df-934b-52835c89632a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603c146c-5190-46ad-a266-7e4342072cb8",
            "compositeImage": {
                "id": "8f9e5170-030d-4d14-886a-7ef13729e832",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39e8346f-55bc-43df-934b-52835c89632a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6152471-d2cb-4a07-a2bd-3a772dcb3d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39e8346f-55bc-43df-934b-52835c89632a",
                    "LayerId": "8cfbce0d-9d2d-42fe-9207-42abca65805d"
                }
            ]
        },
        {
            "id": "871892f4-4b09-411e-b7ce-44e51f9eb179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603c146c-5190-46ad-a266-7e4342072cb8",
            "compositeImage": {
                "id": "51744a13-ab0a-4bf6-827a-24cdbfb89559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "871892f4-4b09-411e-b7ce-44e51f9eb179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28e43090-b19f-4312-8626-334d52b3871e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "871892f4-4b09-411e-b7ce-44e51f9eb179",
                    "LayerId": "8cfbce0d-9d2d-42fe-9207-42abca65805d"
                }
            ]
        },
        {
            "id": "9ffc576a-e523-45a0-a3ad-a61fd8e2ab3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603c146c-5190-46ad-a266-7e4342072cb8",
            "compositeImage": {
                "id": "1835d75b-806e-4f0d-bcc1-158dca04688f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ffc576a-e523-45a0-a3ad-a61fd8e2ab3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b4287db-118e-4194-91be-6a8584ff6462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ffc576a-e523-45a0-a3ad-a61fd8e2ab3e",
                    "LayerId": "8cfbce0d-9d2d-42fe-9207-42abca65805d"
                }
            ]
        },
        {
            "id": "62f253c2-cf19-4d34-8a0e-ee2139e708dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603c146c-5190-46ad-a266-7e4342072cb8",
            "compositeImage": {
                "id": "9dca1789-d907-4ed2-ae92-d4bcd2bb40a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62f253c2-cf19-4d34-8a0e-ee2139e708dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e79601e5-bc72-438c-a902-85264b9c4e44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62f253c2-cf19-4d34-8a0e-ee2139e708dc",
                    "LayerId": "8cfbce0d-9d2d-42fe-9207-42abca65805d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8cfbce0d-9d2d-42fe-9207-42abca65805d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "603c146c-5190-46ad-a266-7e4342072cb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 16
}