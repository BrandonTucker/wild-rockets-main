{
    "id": "6f5114eb-e3e6-42e0-90b8-6329aa1c7d63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship34locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff0a3bb0-ce9f-4b5b-80e4-cb5609736c1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5114eb-e3e6-42e0-90b8-6329aa1c7d63",
            "compositeImage": {
                "id": "bb2aa908-419f-4c21-b073-083f2a405d4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff0a3bb0-ce9f-4b5b-80e4-cb5609736c1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f00987f-65ed-4e28-ac7b-46726dc5ed16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff0a3bb0-ce9f-4b5b-80e4-cb5609736c1f",
                    "LayerId": "99499936-4ef2-435e-8aa6-7df7fd237c59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "99499936-4ef2-435e-8aa6-7df7fd237c59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f5114eb-e3e6-42e0-90b8-6329aa1c7d63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}