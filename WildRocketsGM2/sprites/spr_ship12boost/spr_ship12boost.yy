{
    "id": "1f90b622-32f9-4e7e-9fab-e6e263d2a5d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship12boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 22,
    "bbox_right": 47,
    "bbox_top": -8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ec25541-5f2c-4954-ab8b-37efce4fa290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f90b622-32f9-4e7e-9fab-e6e263d2a5d1",
            "compositeImage": {
                "id": "cfb52def-78ed-407d-8d39-ee4ab6b9f835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec25541-5f2c-4954-ab8b-37efce4fa290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc20d80d-fd1b-4461-b5f8-9fbd8833fbb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec25541-5f2c-4954-ab8b-37efce4fa290",
                    "LayerId": "0ea3e05e-e2a1-4742-8a5a-38ee4f1253de"
                }
            ]
        },
        {
            "id": "6adf90b0-5aca-4bd1-9e97-350f57be0806",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f90b622-32f9-4e7e-9fab-e6e263d2a5d1",
            "compositeImage": {
                "id": "180a24a2-95e6-4134-bfa5-47acf043418a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6adf90b0-5aca-4bd1-9e97-350f57be0806",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a93fce49-d143-4ce1-921d-9c46c48261ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6adf90b0-5aca-4bd1-9e97-350f57be0806",
                    "LayerId": "0ea3e05e-e2a1-4742-8a5a-38ee4f1253de"
                }
            ]
        },
        {
            "id": "14600e5a-75d7-4498-8793-4c717fc5313e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f90b622-32f9-4e7e-9fab-e6e263d2a5d1",
            "compositeImage": {
                "id": "5695f0cc-56df-459e-a15b-be3edf80a9ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14600e5a-75d7-4498-8793-4c717fc5313e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c900e4-fca8-4c15-8ffb-9ce9356a845f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14600e5a-75d7-4498-8793-4c717fc5313e",
                    "LayerId": "0ea3e05e-e2a1-4742-8a5a-38ee4f1253de"
                }
            ]
        },
        {
            "id": "c0698b7c-9ef2-49ed-800d-685c74fb26c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f90b622-32f9-4e7e-9fab-e6e263d2a5d1",
            "compositeImage": {
                "id": "c8c8a5a3-a009-41b9-82fe-61a18ee10475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0698b7c-9ef2-49ed-800d-685c74fb26c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "250b96f1-ed61-4a50-945c-0b7e777dec18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0698b7c-9ef2-49ed-800d-685c74fb26c3",
                    "LayerId": "0ea3e05e-e2a1-4742-8a5a-38ee4f1253de"
                }
            ]
        },
        {
            "id": "ae91caf2-13fa-4d2e-9a93-60f5e7561a45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f90b622-32f9-4e7e-9fab-e6e263d2a5d1",
            "compositeImage": {
                "id": "fbe2affd-6a20-460d-91de-a1d147de3e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae91caf2-13fa-4d2e-9a93-60f5e7561a45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a74a447-b686-4e89-a3e4-48182cb5ccfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae91caf2-13fa-4d2e-9a93-60f5e7561a45",
                    "LayerId": "0ea3e05e-e2a1-4742-8a5a-38ee4f1253de"
                }
            ]
        },
        {
            "id": "f6856bd2-678f-49f3-9e1d-567ede143d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f90b622-32f9-4e7e-9fab-e6e263d2a5d1",
            "compositeImage": {
                "id": "25d3eba0-ba30-4631-a3f7-470b03e4a98e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6856bd2-678f-49f3-9e1d-567ede143d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa7ff3b1-faf6-4157-9237-ec58a4175036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6856bd2-678f-49f3-9e1d-567ede143d41",
                    "LayerId": "0ea3e05e-e2a1-4742-8a5a-38ee4f1253de"
                }
            ]
        },
        {
            "id": "1a9b943a-c2a8-4c3f-bbb2-3c55ca756ed7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f90b622-32f9-4e7e-9fab-e6e263d2a5d1",
            "compositeImage": {
                "id": "71a217c3-b917-4c8c-901d-db8e39c7885a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a9b943a-c2a8-4c3f-bbb2-3c55ca756ed7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3af722fe-e50b-4ea6-a7ee-12c9415673ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9b943a-c2a8-4c3f-bbb2-3c55ca756ed7",
                    "LayerId": "0ea3e05e-e2a1-4742-8a5a-38ee4f1253de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 122,
    "layers": [
        {
            "id": "0ea3e05e-e2a1-4742-8a5a-38ee4f1253de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f90b622-32f9-4e7e-9fab-e6e263d2a5d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 34,
    "yorig": 26
}