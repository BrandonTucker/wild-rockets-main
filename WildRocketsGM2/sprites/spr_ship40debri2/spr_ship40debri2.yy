{
    "id": "044b166d-d19b-49cc-a67a-910058b5098b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship40debri2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 0,
    "bbox_right": 73,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29116b6b-3814-4822-a200-68de5b967e3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "8831cd4c-6a20-465f-a821-f2fa05cc41c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29116b6b-3814-4822-a200-68de5b967e3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99672149-83ac-4f9e-aa9b-9a115c35aca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29116b6b-3814-4822-a200-68de5b967e3f",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "cae3acfa-f072-461e-8402-f53c7d380b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "3b49a42f-147d-42dd-9111-c2b5d3b08e7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cae3acfa-f072-461e-8402-f53c7d380b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f59b3439-f669-4d7b-8814-5f0161b5d78d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cae3acfa-f072-461e-8402-f53c7d380b83",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "8af23eb9-5197-49c3-bcca-4ae828ea7ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "47a3e5df-e420-4f23-9e39-ada1255d063c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8af23eb9-5197-49c3-bcca-4ae828ea7ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4723918-36f3-4020-bf13-013f318b994b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8af23eb9-5197-49c3-bcca-4ae828ea7ffe",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "5f0b51d2-d39e-4b04-8f8f-173a33d9eeb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "2958740a-db43-4de1-9bb7-835faca1030c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f0b51d2-d39e-4b04-8f8f-173a33d9eeb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "359553b3-23b1-4fcf-a837-c9c10bc0795b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f0b51d2-d39e-4b04-8f8f-173a33d9eeb0",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "2dcbffb6-e034-4f16-a0bf-b5ae879b2658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "95855093-f98e-4f58-8666-b651ae21e36c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dcbffb6-e034-4f16-a0bf-b5ae879b2658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8091984-d5b8-4999-a6a1-871653290946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dcbffb6-e034-4f16-a0bf-b5ae879b2658",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "57851c6c-e688-4dc0-9133-5319a9fc3da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "57df1624-b59a-462e-beb5-432c2395bdae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57851c6c-e688-4dc0-9133-5319a9fc3da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e4cdb6-af58-485e-ad3b-9aee17db5f57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57851c6c-e688-4dc0-9133-5319a9fc3da5",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "c2287cdd-249a-48f1-acfa-d74fb654a302",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "51f3093f-fcbb-4542-aeca-ae6ef06559d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2287cdd-249a-48f1-acfa-d74fb654a302",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d15fecee-984a-4adf-858b-64e81a18cad8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2287cdd-249a-48f1-acfa-d74fb654a302",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "1ec5fd8a-920a-47e3-abad-a600541760a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "bb85b030-60f1-402b-bef9-e02eb2988646",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec5fd8a-920a-47e3-abad-a600541760a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb596b0-1c03-4700-8d79-f0ac51be759c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec5fd8a-920a-47e3-abad-a600541760a8",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "b37844e4-179b-4fed-b728-a67c818061bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "0d3285c6-c03c-4382-a53d-4bd7aeb60ef1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b37844e4-179b-4fed-b728-a67c818061bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e35844cb-fc96-47a3-a400-d0c02187cba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b37844e4-179b-4fed-b728-a67c818061bb",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "d0a58cbd-1702-46ea-85cb-1052e8a0f415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "0e25b6f4-3b92-4e2a-9f2c-d7d8bf126b66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0a58cbd-1702-46ea-85cb-1052e8a0f415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "739102bc-55dd-4ec5-a962-bc6b1b1fb86c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0a58cbd-1702-46ea-85cb-1052e8a0f415",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "0b564eb3-f45c-411e-8f8a-bba4c0dc9dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "3771e1b5-92be-435e-ba9b-a2b4387ebc0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b564eb3-f45c-411e-8f8a-bba4c0dc9dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ad474e2-45e0-4451-83a3-879ca10bf4d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b564eb3-f45c-411e-8f8a-bba4c0dc9dfe",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "6cfafcd3-ed7e-46bb-9b5e-90f284115f3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "194b8aa1-b6f9-4e6a-a3f4-61c52ebadd96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cfafcd3-ed7e-46bb-9b5e-90f284115f3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "542f9a0b-f2aa-4f2e-b1d9-b79b94935826",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cfafcd3-ed7e-46bb-9b5e-90f284115f3c",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "4b521c28-1747-43a4-be2a-82ff42b05e9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "dca3bb99-8745-4301-be90-fbc317261878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b521c28-1747-43a4-be2a-82ff42b05e9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66140de1-ef0d-40f1-ba61-b15074c94f3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b521c28-1747-43a4-be2a-82ff42b05e9d",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "c2c17049-fe6d-48a2-8acd-26bac2da3bf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "f70ff974-59fa-4c66-92e1-7a3b5bc25867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c17049-fe6d-48a2-8acd-26bac2da3bf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d77a469-fe27-45d2-9ada-96f294503073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c17049-fe6d-48a2-8acd-26bac2da3bf9",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        },
        {
            "id": "23b07442-70ba-4369-95c9-316bc3249da6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "compositeImage": {
                "id": "3cf38312-4308-4082-8bfa-42c5e71883e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23b07442-70ba-4369-95c9-316bc3249da6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26cee3ce-943f-46e3-bb20-fa18a42d9ed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23b07442-70ba-4369-95c9-316bc3249da6",
                    "LayerId": "ccb36486-8d79-4f6e-9b16-09bf4c131f23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 74,
    "layers": [
        {
            "id": "ccb36486-8d79-4f6e-9b16-09bf4c131f23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "044b166d-d19b-49cc-a67a-910058b5098b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 35,
    "yorig": 31
}