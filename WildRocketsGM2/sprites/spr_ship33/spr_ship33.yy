{
    "id": "183b9e9e-b978-4b59-9a72-b78d0e98476e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship33",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 152,
    "bbox_left": 54,
    "bbox_right": 79,
    "bbox_top": 60,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "454f6697-cec8-4243-88d0-efdb575cb4e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183b9e9e-b978-4b59-9a72-b78d0e98476e",
            "compositeImage": {
                "id": "ce8cd58a-89da-4931-9967-731fa3054ada",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "454f6697-cec8-4243-88d0-efdb575cb4e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7a5152b-dd78-4dc6-b9dc-884bab469c52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "454f6697-cec8-4243-88d0-efdb575cb4e0",
                    "LayerId": "e0884c5c-d40f-428f-85d6-75840514e167"
                }
            ]
        },
        {
            "id": "1198f540-ba4f-4366-8681-6690823c1ed7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183b9e9e-b978-4b59-9a72-b78d0e98476e",
            "compositeImage": {
                "id": "cbb40623-29c8-45fa-9eb4-920510e8e715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1198f540-ba4f-4366-8681-6690823c1ed7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f93a28ac-7b8f-48aa-9f4c-94b61367d121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1198f540-ba4f-4366-8681-6690823c1ed7",
                    "LayerId": "e0884c5c-d40f-428f-85d6-75840514e167"
                }
            ]
        },
        {
            "id": "78ebe85d-373d-4dd5-bc31-c96b64bbf722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183b9e9e-b978-4b59-9a72-b78d0e98476e",
            "compositeImage": {
                "id": "39de6ebd-500b-4e71-91a6-98b19a5874e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78ebe85d-373d-4dd5-bc31-c96b64bbf722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e6e190b-4c2d-4a9e-bd14-80ec6dd95231",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78ebe85d-373d-4dd5-bc31-c96b64bbf722",
                    "LayerId": "e0884c5c-d40f-428f-85d6-75840514e167"
                }
            ]
        },
        {
            "id": "3db67061-3204-4653-b913-41a043e8fa15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183b9e9e-b978-4b59-9a72-b78d0e98476e",
            "compositeImage": {
                "id": "8a25dd5b-1570-4b8d-b316-78a4a746d406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3db67061-3204-4653-b913-41a043e8fa15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03fb5d13-2c9c-4b5d-84c7-560c7f265137",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3db67061-3204-4653-b913-41a043e8fa15",
                    "LayerId": "e0884c5c-d40f-428f-85d6-75840514e167"
                }
            ]
        },
        {
            "id": "300561c3-9489-4613-8916-9a35f73ee9b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183b9e9e-b978-4b59-9a72-b78d0e98476e",
            "compositeImage": {
                "id": "45d6b809-ab7b-4d59-86f6-e1958ad6bf3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "300561c3-9489-4613-8916-9a35f73ee9b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecbe929f-a8ce-4d37-8039-ff99bc406668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "300561c3-9489-4613-8916-9a35f73ee9b0",
                    "LayerId": "e0884c5c-d40f-428f-85d6-75840514e167"
                }
            ]
        },
        {
            "id": "9b97ccc9-ff53-42d8-bffa-7f7c49d8c5a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183b9e9e-b978-4b59-9a72-b78d0e98476e",
            "compositeImage": {
                "id": "848631cd-00a1-4e5b-8c6e-1e2a4e3948de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b97ccc9-ff53-42d8-bffa-7f7c49d8c5a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3858858-c1ea-4ba6-8f9f-fd7a6f711a68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b97ccc9-ff53-42d8-bffa-7f7c49d8c5a2",
                    "LayerId": "e0884c5c-d40f-428f-85d6-75840514e167"
                }
            ]
        },
        {
            "id": "c54825a3-caca-46aa-a5e8-a4456e34058b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183b9e9e-b978-4b59-9a72-b78d0e98476e",
            "compositeImage": {
                "id": "23db81fa-81b2-4e79-b080-e872b41c1a55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c54825a3-caca-46aa-a5e8-a4456e34058b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc58285-60b7-4e7f-9bff-15bbbb7b8224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c54825a3-caca-46aa-a5e8-a4456e34058b",
                    "LayerId": "e0884c5c-d40f-428f-85d6-75840514e167"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 207,
    "layers": [
        {
            "id": "e0884c5c-d40f-428f-85d6-75840514e167",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "183b9e9e-b978-4b59-9a72-b78d0e98476e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 67,
    "yorig": 100
}