{
    "id": "a038f436-c573-4969-9eb1-dd2b1af9c4ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship7locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "679e6c9f-3e59-4964-a64e-012196d4d59d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a038f436-c573-4969-9eb1-dd2b1af9c4ec",
            "compositeImage": {
                "id": "dfc10beb-1426-4465-8cb1-9e1d4e1373b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "679e6c9f-3e59-4964-a64e-012196d4d59d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a75533d-c975-414d-8a5a-522733519c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "679e6c9f-3e59-4964-a64e-012196d4d59d",
                    "LayerId": "fa8d7266-99c2-4fec-a124-5d93c9ce7e0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "fa8d7266-99c2-4fec-a124-5d93c9ce7e0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a038f436-c573-4969-9eb1-dd2b1af9c4ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}