{
    "id": "482f9621-40e7-4c62-b9b0-f027f46e7c41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage15button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bda0c8b-3a12-4856-ba15-e580dd7dc973",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "482f9621-40e7-4c62-b9b0-f027f46e7c41",
            "compositeImage": {
                "id": "a419df9d-7e39-4d04-a514-eed32525b691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bda0c8b-3a12-4856-ba15-e580dd7dc973",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48b52c6e-3e85-4a8b-a6a7-82b654fbeb80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bda0c8b-3a12-4856-ba15-e580dd7dc973",
                    "LayerId": "1a973cd8-c8e8-44a4-aa94-b9b6ff17744c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1a973cd8-c8e8-44a4-aa94-b9b6ff17744c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "482f9621-40e7-4c62-b9b0-f027f46e7c41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}