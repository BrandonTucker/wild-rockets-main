{
    "id": "89dd3da6-f20a-48c8-bc11-fafe2456b163",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship28boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e7b244f-12d4-470e-931c-922b5290964f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89dd3da6-f20a-48c8-bc11-fafe2456b163",
            "compositeImage": {
                "id": "46e78645-e811-4747-b792-3082db83dcec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e7b244f-12d4-470e-931c-922b5290964f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1fff1e8-d540-493a-a138-e7518325acb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e7b244f-12d4-470e-931c-922b5290964f",
                    "LayerId": "94f1eb54-b600-4fbd-933c-3ad6fbd872fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "94f1eb54-b600-4fbd-933c-3ad6fbd872fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89dd3da6-f20a-48c8-bc11-fafe2456b163",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 50
}