{
    "id": "b8ed2210-b224-4a3c-869f-2cff7d7b0c23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_block2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de0dc7fe-1b81-4361-bff5-976fa9594598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ed2210-b224-4a3c-869f-2cff7d7b0c23",
            "compositeImage": {
                "id": "764d7420-044f-42fc-8fc6-6a7b5f31450c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de0dc7fe-1b81-4361-bff5-976fa9594598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40a1997-c2c8-48f5-b842-837e543a89a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de0dc7fe-1b81-4361-bff5-976fa9594598",
                    "LayerId": "ca1bed7a-5e71-4305-9883-138739cb0054"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ca1bed7a-5e71-4305-9883-138739cb0054",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8ed2210-b224-4a3c-869f-2cff7d7b0c23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 16,
    "yorig": 16
}