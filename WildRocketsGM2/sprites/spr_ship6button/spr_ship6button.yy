{
    "id": "d1f64c38-cdd0-460c-94cc-496f95a24cd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship6button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 88,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "886b989a-3d72-4773-8f28-031df95c8533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1f64c38-cdd0-460c-94cc-496f95a24cd2",
            "compositeImage": {
                "id": "61e5a6c6-92ee-482e-a36a-c438ade761a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886b989a-3d72-4773-8f28-031df95c8533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fdb0e2d-61b5-4ac3-8034-093a9e382c2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886b989a-3d72-4773-8f28-031df95c8533",
                    "LayerId": "9307f252-a54a-4ec2-a270-a79ad9400402"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "9307f252-a54a-4ec2-a270-a79ad9400402",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1f64c38-cdd0-460c-94cc-496f95a24cd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}