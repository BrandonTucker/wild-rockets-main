{
    "id": "a0866381-3857-4240-9c9a-7508a3d03c86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_snowflake",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c955852a-f50b-4683-90b1-d4768435d6e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "b96a0d09-62ea-4eea-9ecb-254ba51d9363",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c955852a-f50b-4683-90b1-d4768435d6e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea88523-3743-4de3-bdb5-4298a5c2db9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c955852a-f50b-4683-90b1-d4768435d6e0",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "c7105322-00de-4775-bc1d-12b413bc5280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "54a368c1-b94c-42f1-9794-883eda63989e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7105322-00de-4775-bc1d-12b413bc5280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44e6a526-1c37-4e69-8096-0d00a6b7f3e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7105322-00de-4775-bc1d-12b413bc5280",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "6bf6e066-c7a1-4b97-96e8-a266d59081fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "499fb189-3ee6-4f41-bf33-5a83f753e56b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bf6e066-c7a1-4b97-96e8-a266d59081fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd56234c-c2f0-432c-bf89-463294946779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bf6e066-c7a1-4b97-96e8-a266d59081fd",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "713da2d5-75b9-4c20-bac2-504f781abc09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "ca900ee2-78cd-406a-ab0e-f91f7631aa12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "713da2d5-75b9-4c20-bac2-504f781abc09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3644f0d2-4768-4fca-8634-08b964a61420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "713da2d5-75b9-4c20-bac2-504f781abc09",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "36ad40e6-fabf-4223-9614-ac04c0636165",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "8bf19b95-68d1-45af-afa2-b79a3132a576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36ad40e6-fabf-4223-9614-ac04c0636165",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "539652d5-55a4-45aa-8b73-4b2a8620796d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36ad40e6-fabf-4223-9614-ac04c0636165",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "9fd5891f-b80f-4cd5-84df-c58d5bb5d947",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "9f0468cc-83e6-414b-82dd-a7f5d989cca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fd5891f-b80f-4cd5-84df-c58d5bb5d947",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7550b308-066e-4cd2-abd2-d5ae1bae7818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fd5891f-b80f-4cd5-84df-c58d5bb5d947",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "249bfe4a-2862-4e3f-b490-2ffa37bd6d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "58c679c1-3d05-4564-8efa-46206a18a31a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "249bfe4a-2862-4e3f-b490-2ffa37bd6d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b22184-ca9e-4470-b629-b7874d16593f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "249bfe4a-2862-4e3f-b490-2ffa37bd6d4b",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "d2f0ddfe-3ced-40eb-8962-71bb017758ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "2551de10-b5c0-4444-b149-3397412eff95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2f0ddfe-3ced-40eb-8962-71bb017758ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1925e3-ed19-494e-9551-d6b88b01c702",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2f0ddfe-3ced-40eb-8962-71bb017758ce",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "c449e543-af28-4930-a405-87a49390bb42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "10751f3d-7d66-4fb2-92f8-10dcb372a164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c449e543-af28-4930-a405-87a49390bb42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f970539a-f237-4f15-a8c7-320439f16f47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c449e543-af28-4930-a405-87a49390bb42",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "933f469e-0546-42cc-ba37-d2f86cc76f90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "61abfff2-bb31-45c8-9003-a565b13909a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933f469e-0546-42cc-ba37-d2f86cc76f90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1e0e5ec-6e2a-421c-b7e4-11e239ba6339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933f469e-0546-42cc-ba37-d2f86cc76f90",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "b29d60b6-3e7a-46de-9327-ca0f924f1a21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "f4942147-513b-4ec2-a6d2-ce57f1f968ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b29d60b6-3e7a-46de-9327-ca0f924f1a21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30508c60-de32-49f3-81cd-291368065845",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b29d60b6-3e7a-46de-9327-ca0f924f1a21",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "e6018392-dc1a-431a-9986-a3763d25ed4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "e6c60d9e-34ca-4914-862a-d3ca20f65cee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6018392-dc1a-431a-9986-a3763d25ed4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36929be5-7c1b-4e40-be54-bc0ef8b933bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6018392-dc1a-431a-9986-a3763d25ed4d",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "a101042a-32a0-41de-83ae-f0af92ba4938",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "a5f288a9-01f7-4dd0-8c00-cd93eccee231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a101042a-32a0-41de-83ae-f0af92ba4938",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20fd48d-5c78-482f-a714-8aa3a13ddedf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a101042a-32a0-41de-83ae-f0af92ba4938",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "47978900-6289-4cbf-acf2-7d9b19e2c637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "b197b77f-1f5e-4b29-9d64-e6a9227ae0a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47978900-6289-4cbf-acf2-7d9b19e2c637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c56821e6-a9d9-4e22-a82f-0b0301279918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47978900-6289-4cbf-acf2-7d9b19e2c637",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        },
        {
            "id": "4832bc1b-a60c-4515-a7a7-0791ddb2a824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "compositeImage": {
                "id": "56b05f67-9f7a-4991-87fe-3ad336829d9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4832bc1b-a60c-4515-a7a7-0791ddb2a824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "489f874a-af86-4862-9520-e97a3f344b85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4832bc1b-a60c-4515-a7a7-0791ddb2a824",
                    "LayerId": "a4602275-f8d1-4713-854d-b66aebfaff7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "a4602275-f8d1-4713-854d-b66aebfaff7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0866381-3857-4240-9c9a-7508a3d03c86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 24,
    "yorig": 24
}