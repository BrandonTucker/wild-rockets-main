{
    "id": "f2ce4372-55fb-40b6-bc32-039e1c73e096",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background2",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e4bc630-2e78-4dc0-a535-c1557c2f2125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2ce4372-55fb-40b6-bc32-039e1c73e096",
            "compositeImage": {
                "id": "465129a4-2416-4143-a7c8-bc608b0d6704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e4bc630-2e78-4dc0-a535-c1557c2f2125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f90eba9c-e9b9-4918-b90f-ea9c8ab3d1a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e4bc630-2e78-4dc0-a535-c1557c2f2125",
                    "LayerId": "b01206a9-c67f-426e-a095-3266ed81d2d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b01206a9-c67f-426e-a095-3266ed81d2d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2ce4372-55fb-40b6-bc32-039e1c73e096",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}