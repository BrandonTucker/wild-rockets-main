{
    "id": "d9894f94-5aad-481a-a269-262117b4efd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship8button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 12,
    "bbox_right": 89,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1667f7fd-94a8-42a9-afbe-1b8e886174da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9894f94-5aad-481a-a269-262117b4efd3",
            "compositeImage": {
                "id": "812bf599-4044-4907-9535-a1f6d928a060",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1667f7fd-94a8-42a9-afbe-1b8e886174da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80b3c57b-deb1-4d0b-acda-2a4448f5d058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1667f7fd-94a8-42a9-afbe-1b8e886174da",
                    "LayerId": "a8cb56ec-eb8e-4f41-82c8-ce5153ca2989"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "a8cb56ec-eb8e-4f41-82c8-ce5153ca2989",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9894f94-5aad-481a-a269-262117b4efd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}