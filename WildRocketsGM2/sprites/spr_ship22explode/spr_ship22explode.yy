{
    "id": "e9003ac4-9694-4582-a3d0-dec4f8521143",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship22explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 25,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c78d4737-98fc-4d2d-891a-f4d86b42b06c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9003ac4-9694-4582-a3d0-dec4f8521143",
            "compositeImage": {
                "id": "436e161f-b726-440c-8d07-1f905fc80b29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c78d4737-98fc-4d2d-891a-f4d86b42b06c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268de3eb-45f4-45e4-a4fd-6f8f4d935cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c78d4737-98fc-4d2d-891a-f4d86b42b06c",
                    "LayerId": "7e7f2eb7-ba88-479b-b0c7-c20ada003a98"
                }
            ]
        },
        {
            "id": "7456a2e2-490c-4845-8fc9-60e38ebb741c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9003ac4-9694-4582-a3d0-dec4f8521143",
            "compositeImage": {
                "id": "c2add439-06ad-42f8-b7a8-d99ab49ea5a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7456a2e2-490c-4845-8fc9-60e38ebb741c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffae80c5-3340-402d-a48f-1a92c62e4751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7456a2e2-490c-4845-8fc9-60e38ebb741c",
                    "LayerId": "7e7f2eb7-ba88-479b-b0c7-c20ada003a98"
                }
            ]
        },
        {
            "id": "be2e2957-fb9e-4e24-ab83-ce5be1adfa18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9003ac4-9694-4582-a3d0-dec4f8521143",
            "compositeImage": {
                "id": "aaeacc0c-8dd0-4763-85b2-e65059c5a22f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be2e2957-fb9e-4e24-ab83-ce5be1adfa18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd1d8f71-3fc3-43ea-b803-266f02f45e0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be2e2957-fb9e-4e24-ab83-ce5be1adfa18",
                    "LayerId": "7e7f2eb7-ba88-479b-b0c7-c20ada003a98"
                }
            ]
        },
        {
            "id": "cd8268a2-958d-48fe-a37c-5a11f9a9b6ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9003ac4-9694-4582-a3d0-dec4f8521143",
            "compositeImage": {
                "id": "7abffd21-46ff-4624-87d7-4fcd426537c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd8268a2-958d-48fe-a37c-5a11f9a9b6ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dcc8c4b-546f-4cf9-a7e0-b09262fc26bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd8268a2-958d-48fe-a37c-5a11f9a9b6ab",
                    "LayerId": "7e7f2eb7-ba88-479b-b0c7-c20ada003a98"
                }
            ]
        },
        {
            "id": "786742bd-04cc-42e8-b731-2e4f0af1e03a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9003ac4-9694-4582-a3d0-dec4f8521143",
            "compositeImage": {
                "id": "6e95204e-8bb8-423f-b6f9-b2229fb13656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "786742bd-04cc-42e8-b731-2e4f0af1e03a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c11d932-95c8-4194-b906-154fc776912c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "786742bd-04cc-42e8-b731-2e4f0af1e03a",
                    "LayerId": "7e7f2eb7-ba88-479b-b0c7-c20ada003a98"
                }
            ]
        },
        {
            "id": "ba341303-5b8f-4289-9a27-de553e15cd1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9003ac4-9694-4582-a3d0-dec4f8521143",
            "compositeImage": {
                "id": "02edfdec-6951-427d-bbb0-1a27a8893bcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba341303-5b8f-4289-9a27-de553e15cd1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "682cda38-53d7-4a09-9512-82ff40599823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba341303-5b8f-4289-9a27-de553e15cd1d",
                    "LayerId": "7e7f2eb7-ba88-479b-b0c7-c20ada003a98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "7e7f2eb7-ba88-479b-b0c7-c20ada003a98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9003ac4-9694-4582-a3d0-dec4f8521143",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 49
}