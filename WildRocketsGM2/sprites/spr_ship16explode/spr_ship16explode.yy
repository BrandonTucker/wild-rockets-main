{
    "id": "a46a417a-d1df-4002-b6c9-359cb2d9773b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship16explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 33,
    "bbox_right": 56,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93960d4a-3450-4141-8af6-b321d2a882ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a46a417a-d1df-4002-b6c9-359cb2d9773b",
            "compositeImage": {
                "id": "5ec1d70d-2687-4d64-aa82-31ca64608a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93960d4a-3450-4141-8af6-b321d2a882ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c9de7c7-8a16-4ed5-8bc8-25b9578ecaa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93960d4a-3450-4141-8af6-b321d2a882ce",
                    "LayerId": "292429a1-41a7-4926-b8de-b29661243ae1"
                }
            ]
        },
        {
            "id": "bed60955-7f95-4567-a311-16f47ac8797f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a46a417a-d1df-4002-b6c9-359cb2d9773b",
            "compositeImage": {
                "id": "1020db63-9a74-4f0a-9537-e35ee15a7973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed60955-7f95-4567-a311-16f47ac8797f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e83ac6d5-8afd-4851-b185-6fc322e94700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed60955-7f95-4567-a311-16f47ac8797f",
                    "LayerId": "292429a1-41a7-4926-b8de-b29661243ae1"
                }
            ]
        },
        {
            "id": "1ca1d837-4e57-46d2-8639-5e4dbce9c77a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a46a417a-d1df-4002-b6c9-359cb2d9773b",
            "compositeImage": {
                "id": "3341a9f0-24cd-4423-835b-86e51e227e0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ca1d837-4e57-46d2-8639-5e4dbce9c77a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1335ce88-f02a-4ac0-94a9-a7fe9f7f368d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ca1d837-4e57-46d2-8639-5e4dbce9c77a",
                    "LayerId": "292429a1-41a7-4926-b8de-b29661243ae1"
                }
            ]
        },
        {
            "id": "03800790-7731-4da6-a7da-9cede693b74e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a46a417a-d1df-4002-b6c9-359cb2d9773b",
            "compositeImage": {
                "id": "fea83a2b-5776-4501-9dff-9341feb89848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03800790-7731-4da6-a7da-9cede693b74e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ea07ef-012b-4292-8ef9-3297c86784f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03800790-7731-4da6-a7da-9cede693b74e",
                    "LayerId": "292429a1-41a7-4926-b8de-b29661243ae1"
                }
            ]
        },
        {
            "id": "c3327cf7-7c83-4514-a1fe-50e3e3098be8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a46a417a-d1df-4002-b6c9-359cb2d9773b",
            "compositeImage": {
                "id": "1878bcc8-233a-4d46-a809-15cfac3ba8d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3327cf7-7c83-4514-a1fe-50e3e3098be8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08b1a739-94d9-413c-b05f-ffb8934d39e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3327cf7-7c83-4514-a1fe-50e3e3098be8",
                    "LayerId": "292429a1-41a7-4926-b8de-b29661243ae1"
                }
            ]
        },
        {
            "id": "5f6ca222-334d-4254-93e8-7a33f3f1c7f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a46a417a-d1df-4002-b6c9-359cb2d9773b",
            "compositeImage": {
                "id": "7425f707-9a9e-4179-bae0-7a2e3fe342a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f6ca222-334d-4254-93e8-7a33f3f1c7f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e6aaad-0e2b-438e-8ee7-5b702521ff64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f6ca222-334d-4254-93e8-7a33f3f1c7f9",
                    "LayerId": "292429a1-41a7-4926-b8de-b29661243ae1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "292429a1-41a7-4926-b8de-b29661243ae1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a46a417a-d1df-4002-b6c9-359cb2d9773b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 43,
    "yorig": 41
}