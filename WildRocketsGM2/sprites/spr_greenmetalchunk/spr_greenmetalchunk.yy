{
    "id": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_greenmetalchunk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 0,
    "bbox_right": 77,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfac5c8b-1cb6-4928-9e3c-e9e0f8cb2d92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "31f7986f-aa1a-4092-85db-80d510f4fe4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfac5c8b-1cb6-4928-9e3c-e9e0f8cb2d92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0bab98b-bf5a-427d-bc95-3a68cd2b0e8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfac5c8b-1cb6-4928-9e3c-e9e0f8cb2d92",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "70b993b8-98e9-4da8-b3c4-155a9503f742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "7a4b2ba9-cff9-4b18-901b-d06e9698ad2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70b993b8-98e9-4da8-b3c4-155a9503f742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3978a74b-ebd2-45bd-a053-10a8c7ddf2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b993b8-98e9-4da8-b3c4-155a9503f742",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "c7c4aaad-65dd-4b34-a294-c868b2f5c429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "95b97139-f1b1-40e5-84cf-4f6184296463",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7c4aaad-65dd-4b34-a294-c868b2f5c429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5021ae34-f0fc-487c-a7ee-74a13a0c40f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7c4aaad-65dd-4b34-a294-c868b2f5c429",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "6ee31f2c-331c-4d84-8c52-7d45de43d2c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "60f50cd0-4d40-478a-9ce9-9896eb3ecb09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee31f2c-331c-4d84-8c52-7d45de43d2c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ea9c7e-0a6e-4200-b54e-e735b2169405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee31f2c-331c-4d84-8c52-7d45de43d2c1",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "bdb4d1d2-94ec-4e18-bf66-a0f76210a5d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "d417c55d-d304-4e4e-90a8-b8a082619261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdb4d1d2-94ec-4e18-bf66-a0f76210a5d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79c3b474-9c83-4b5a-aa88-4d4fdd68bece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdb4d1d2-94ec-4e18-bf66-a0f76210a5d9",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "9cbdebc5-9ac5-4d62-a8d5-e0b9781e6c7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "cee9dbb8-040b-4eca-96fd-5e01b894667e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cbdebc5-9ac5-4d62-a8d5-e0b9781e6c7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af3577b3-62ec-44e9-b66c-735b2f5be985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cbdebc5-9ac5-4d62-a8d5-e0b9781e6c7d",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "68aaede4-af11-425e-8f41-1ef80e65bb92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "6e49b2e1-556b-4c03-9c7f-c321a85db2df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68aaede4-af11-425e-8f41-1ef80e65bb92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfb14318-006f-4351-9405-37337e1280a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68aaede4-af11-425e-8f41-1ef80e65bb92",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "ca402190-d286-416f-85dd-4bcbe9c9e136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "a570d42c-34f6-46a8-be2d-d842097d08f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca402190-d286-416f-85dd-4bcbe9c9e136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de3a4c29-99f0-46d7-98b6-ac66357722d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca402190-d286-416f-85dd-4bcbe9c9e136",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "89122f8a-6af9-4a85-85b0-007ea91678ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "85bc69df-2b98-4926-a783-d5f622d00608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89122f8a-6af9-4a85-85b0-007ea91678ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f64fac9b-779f-4264-987b-cc9611f29e77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89122f8a-6af9-4a85-85b0-007ea91678ec",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "468693cc-9784-4853-846a-f5882e81cbfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "d1efee5a-9f34-492c-a6bf-3b16ea436a90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "468693cc-9784-4853-846a-f5882e81cbfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9eaaa46-2189-4212-8348-9249ffda0eca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "468693cc-9784-4853-846a-f5882e81cbfb",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "536c6df9-bf2a-4adc-aefc-5ead256618e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "5e3ff4fd-355f-494b-b0c8-36792a47aefe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "536c6df9-bf2a-4adc-aefc-5ead256618e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "558361c0-06b9-42b7-b619-4f201ff57a11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "536c6df9-bf2a-4adc-aefc-5ead256618e7",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "e6c5aadf-f921-4070-9a0c-f90fe1e73341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "1d46dbe8-612e-4d7e-9288-2ce7bfc9dd21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6c5aadf-f921-4070-9a0c-f90fe1e73341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9780cee9-01e5-4826-92d8-5c1b7f3683d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6c5aadf-f921-4070-9a0c-f90fe1e73341",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "69d30c0d-4fcf-4b66-ada3-748ff7a96804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "ab7544d7-ed66-45f5-9c77-8bd22a0b2616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69d30c0d-4fcf-4b66-ada3-748ff7a96804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31ff2c5c-e73c-42c3-8ee0-ee1538f78a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69d30c0d-4fcf-4b66-ada3-748ff7a96804",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "a95c9857-7bc5-48fb-a056-6a3be238871b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "b1ecdcdc-87fc-4bc6-81b3-68a1df0427e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a95c9857-7bc5-48fb-a056-6a3be238871b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9501c03-78c2-4fe2-9639-cefaf784bbc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a95c9857-7bc5-48fb-a056-6a3be238871b",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        },
        {
            "id": "863ed4a2-b552-4cbc-9adb-2b8e66a00767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "compositeImage": {
                "id": "a935ec88-fc04-4d08-8768-3b15627fd9fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "863ed4a2-b552-4cbc-9adb-2b8e66a00767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fd97867-9324-4be6-8c70-4093b307f235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "863ed4a2-b552-4cbc-9adb-2b8e66a00767",
                    "LayerId": "21b6bfb7-f091-43f8-992a-28b4129116df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "21b6bfb7-f091-43f8-992a-28b4129116df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e37bd0a2-2f6e-4f30-a496-d5ad5175947d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 39,
    "yorig": 45
}