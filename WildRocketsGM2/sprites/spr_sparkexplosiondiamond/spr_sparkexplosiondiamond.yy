{
    "id": "0c2f1def-3d3f-470b-886f-399052e9f954",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sparkexplosiondiamond",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 165,
    "bbox_left": 7,
    "bbox_right": 207,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "101b68bc-fbd7-41c8-9983-c6f2956c71ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2f1def-3d3f-470b-886f-399052e9f954",
            "compositeImage": {
                "id": "7340c4c9-ea0b-486e-ae26-7243fe1e55fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "101b68bc-fbd7-41c8-9983-c6f2956c71ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23c5262a-c9ab-4b49-a879-bc3d22ea8e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "101b68bc-fbd7-41c8-9983-c6f2956c71ff",
                    "LayerId": "d92f256c-5438-4c87-ac53-c7dca992b4e3"
                }
            ]
        },
        {
            "id": "1325cab3-360d-4aac-8ece-829f0c925b8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2f1def-3d3f-470b-886f-399052e9f954",
            "compositeImage": {
                "id": "42dfd259-1fce-4f40-9e58-4b6660c3d17d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1325cab3-360d-4aac-8ece-829f0c925b8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d00304b8-13d4-4749-97d1-47f3f0660151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1325cab3-360d-4aac-8ece-829f0c925b8e",
                    "LayerId": "d92f256c-5438-4c87-ac53-c7dca992b4e3"
                }
            ]
        },
        {
            "id": "92e7482f-e3cc-46d7-b68e-e7b7db25e9f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2f1def-3d3f-470b-886f-399052e9f954",
            "compositeImage": {
                "id": "255adc66-ac91-42f9-8c57-781eb9315f1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92e7482f-e3cc-46d7-b68e-e7b7db25e9f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25852c2b-660d-4834-8e97-73c31fddc491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e7482f-e3cc-46d7-b68e-e7b7db25e9f2",
                    "LayerId": "d92f256c-5438-4c87-ac53-c7dca992b4e3"
                }
            ]
        },
        {
            "id": "9be38fdb-237b-421d-bca8-2e12fa55cba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2f1def-3d3f-470b-886f-399052e9f954",
            "compositeImage": {
                "id": "ec9a2910-968f-45d7-9228-10baa7e24b56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9be38fdb-237b-421d-bca8-2e12fa55cba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb1507a-9d6f-4346-acd6-e670c49f3d55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9be38fdb-237b-421d-bca8-2e12fa55cba5",
                    "LayerId": "d92f256c-5438-4c87-ac53-c7dca992b4e3"
                }
            ]
        },
        {
            "id": "1d3f65be-5a21-4453-a8cb-8670d4d17267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2f1def-3d3f-470b-886f-399052e9f954",
            "compositeImage": {
                "id": "39ba7a9e-63d1-4587-8564-b4f2e0b98ed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d3f65be-5a21-4453-a8cb-8670d4d17267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b45f4612-0665-437c-ae36-de6483f08241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3f65be-5a21-4453-a8cb-8670d4d17267",
                    "LayerId": "d92f256c-5438-4c87-ac53-c7dca992b4e3"
                }
            ]
        },
        {
            "id": "02e46d8d-cd41-4e95-b425-f283f93c170e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c2f1def-3d3f-470b-886f-399052e9f954",
            "compositeImage": {
                "id": "d810ae33-ac86-45b1-baee-bba7a4bf9c33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02e46d8d-cd41-4e95-b425-f283f93c170e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae166cec-0c14-486b-963d-2c3e74fe6003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02e46d8d-cd41-4e95-b425-f283f93c170e",
                    "LayerId": "d92f256c-5438-4c87-ac53-c7dca992b4e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 166,
    "layers": [
        {
            "id": "d92f256c-5438-4c87-ac53-c7dca992b4e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c2f1def-3d3f-470b-886f-399052e9f954",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 211,
    "xorig": 104,
    "yorig": 79
}