{
    "id": "af350bff-e67a-423c-90fb-b7b1376889d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship34debri1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 0,
    "bbox_right": 73,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fd210a9-5288-44d4-ab83-d42104367394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "f5392e6b-dce4-4e3c-b3f0-e073202428df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd210a9-5288-44d4-ab83-d42104367394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e62b95a-d36d-4c52-bd3d-65f1044bc31f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd210a9-5288-44d4-ab83-d42104367394",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "bbec25e5-452e-4570-8c1c-b82971916564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "7848f84b-182b-4aba-ade8-c8897d3d140d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbec25e5-452e-4570-8c1c-b82971916564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17d417cd-54d5-47e5-9bfd-ab5b62e95957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbec25e5-452e-4570-8c1c-b82971916564",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "afc610fb-24f3-40bf-b263-86d3e632b7a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "80014f99-a388-4ebc-9c2f-2e566e6e988a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afc610fb-24f3-40bf-b263-86d3e632b7a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cceabb7-d3b6-450e-b2e5-ca19cecc4032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afc610fb-24f3-40bf-b263-86d3e632b7a8",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "fdd448d2-b9ef-4616-8cd4-61251eb06a4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "df02c44c-03d3-424a-b571-7a38aa89a0f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdd448d2-b9ef-4616-8cd4-61251eb06a4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a9873a-d469-41f0-86dc-df90518f86d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdd448d2-b9ef-4616-8cd4-61251eb06a4c",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "5bbcd897-daee-45c4-b971-bb0cd0700ce5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "c9e217c7-8591-47a9-8f19-cedfb187af51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bbcd897-daee-45c4-b971-bb0cd0700ce5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "404ca0f0-899e-4f69-a5da-e21958eea0a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bbcd897-daee-45c4-b971-bb0cd0700ce5",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "89443ace-70ec-4230-a987-a7d010c22d2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "4c4bc32a-28b7-4687-9cea-70885a2bdc30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89443ace-70ec-4230-a987-a7d010c22d2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c4bc269-1475-4e31-8d3f-c0cb72f63fd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89443ace-70ec-4230-a987-a7d010c22d2c",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "bc073011-9298-495c-b11d-abaa72720a94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "54215bac-507c-467e-a528-a541473cb4bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc073011-9298-495c-b11d-abaa72720a94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daa2d7aa-f845-4f73-8cb0-5f5e49dd05d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc073011-9298-495c-b11d-abaa72720a94",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "2c284755-f729-42da-a588-c10d4ba3392b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "67f00ee2-ec44-4e59-b781-804863a8db1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c284755-f729-42da-a588-c10d4ba3392b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ce9c900-4772-4e4e-9e1d-c30c2ca2b3d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c284755-f729-42da-a588-c10d4ba3392b",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "64fe86ec-fecc-4196-a0fa-77aca0c0e6c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "4163c96c-b355-487f-9b6b-f871c714382d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64fe86ec-fecc-4196-a0fa-77aca0c0e6c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cba11676-c35d-4cca-a241-48826662577f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64fe86ec-fecc-4196-a0fa-77aca0c0e6c4",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "ed9fccd8-8e74-4763-9a7a-153fe56dbd60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "d388f109-dc7c-4961-b3fa-2f58a76fb766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9fccd8-8e74-4763-9a7a-153fe56dbd60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ad20778-3c48-4c79-9132-4ecf265b511a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9fccd8-8e74-4763-9a7a-153fe56dbd60",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "223039dc-2166-4686-8054-e8cf25b88fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "7422c7ba-ec4b-48e3-b8de-97c7a9210afd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "223039dc-2166-4686-8054-e8cf25b88fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35fd01b1-3b77-4e8b-8f76-d2813b556fbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "223039dc-2166-4686-8054-e8cf25b88fa0",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "901d6faa-bbcd-4b2d-819b-14113654585f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "9a11d9ca-b0dd-46c1-b902-5fd9a6fa41de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "901d6faa-bbcd-4b2d-819b-14113654585f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7ef6ade-f8cb-4fe8-a01f-b861c5064f63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "901d6faa-bbcd-4b2d-819b-14113654585f",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "89389a86-0844-4c66-82e4-66bb0c49d854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "697629fe-a086-4b62-8d9b-cc52d457f8f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89389a86-0844-4c66-82e4-66bb0c49d854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a238fa6-24d1-49b4-8e0e-00d2b4938cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89389a86-0844-4c66-82e4-66bb0c49d854",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "fdddc07d-10c7-49f8-b373-f787d161a3fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "6f929555-a604-471a-b13a-722cc50e083d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdddc07d-10c7-49f8-b373-f787d161a3fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91cf9a6b-0ad8-4cd9-bdff-4b59b3eb33b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdddc07d-10c7-49f8-b373-f787d161a3fd",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        },
        {
            "id": "76f27092-c94b-4e84-87be-a2dcd1c162ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "compositeImage": {
                "id": "4aae5561-cc83-4194-9c42-18acb8dc885a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76f27092-c94b-4e84-87be-a2dcd1c162ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e082ff1-035e-4449-99d6-8b14f1695095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76f27092-c94b-4e84-87be-a2dcd1c162ed",
                    "LayerId": "38ce03ef-e275-4413-a5cd-bda84d05eb89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 74,
    "layers": [
        {
            "id": "38ce03ef-e275-4413-a5cd-bda84d05eb89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af350bff-e67a-423c-90fb-b7b1376889d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 41,
    "yorig": 37
}