{
    "id": "397c9cc3-b01d-49fe-8c69-ac314c175a7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship2button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 88,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bc17303-20db-4e80-a95d-b97ed081a10f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397c9cc3-b01d-49fe-8c69-ac314c175a7d",
            "compositeImage": {
                "id": "7deaa986-2c4b-43a8-86f1-56a5553157e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bc17303-20db-4e80-a95d-b97ed081a10f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b05b196d-7cb1-47e0-9c6a-2f898649d4e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bc17303-20db-4e80-a95d-b97ed081a10f",
                    "LayerId": "1810cdbe-84e0-47c6-b45a-9827104a4992"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1810cdbe-84e0-47c6-b45a-9827104a4992",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "397c9cc3-b01d-49fe-8c69-ac314c175a7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}