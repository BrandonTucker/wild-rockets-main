{
    "id": "a2bbf74b-19bc-4199-83ea-5e8006bd6547",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clearFloor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 187,
    "bbox_left": 8,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f6228b7-4b92-4e8e-85a5-9c47750985f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2bbf74b-19bc-4199-83ea-5e8006bd6547",
            "compositeImage": {
                "id": "9e9161b5-ef30-41f9-b5ba-c7ebeff4c348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f6228b7-4b92-4e8e-85a5-9c47750985f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b8b08a4-ca5c-4d08-a9cb-62ba1fde8f80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f6228b7-4b92-4e8e-85a5-9c47750985f9",
                    "LayerId": "9ae72eab-dba6-417d-8c69-2b492779da44"
                }
            ]
        },
        {
            "id": "c1137e6b-5057-4169-bf1a-658933e32b86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2bbf74b-19bc-4199-83ea-5e8006bd6547",
            "compositeImage": {
                "id": "744cb94e-9079-49f3-8efa-4f5802e3fcf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1137e6b-5057-4169-bf1a-658933e32b86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1ed14ba-dee5-4942-8316-48d9142bcfe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1137e6b-5057-4169-bf1a-658933e32b86",
                    "LayerId": "9ae72eab-dba6-417d-8c69-2b492779da44"
                }
            ]
        },
        {
            "id": "b63ede61-4267-435f-98ec-5caf2dff8c7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2bbf74b-19bc-4199-83ea-5e8006bd6547",
            "compositeImage": {
                "id": "f9850741-2e1a-477d-b8d1-916c0831e91c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b63ede61-4267-435f-98ec-5caf2dff8c7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7435e6a4-acd5-4ef6-a0dc-068105a51fce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b63ede61-4267-435f-98ec-5caf2dff8c7f",
                    "LayerId": "9ae72eab-dba6-417d-8c69-2b492779da44"
                }
            ]
        },
        {
            "id": "02c1407c-e1da-43a0-b247-395b85c0768f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2bbf74b-19bc-4199-83ea-5e8006bd6547",
            "compositeImage": {
                "id": "5dd09c53-ce74-48c5-afe4-e2ce92b439e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02c1407c-e1da-43a0-b247-395b85c0768f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb20b53-2d78-4c2d-a737-06b9baa542f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02c1407c-e1da-43a0-b247-395b85c0768f",
                    "LayerId": "9ae72eab-dba6-417d-8c69-2b492779da44"
                }
            ]
        },
        {
            "id": "c158881d-3ff5-40bc-8282-365470f99107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2bbf74b-19bc-4199-83ea-5e8006bd6547",
            "compositeImage": {
                "id": "1ceee981-d876-42f3-960a-bf1179be14aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c158881d-3ff5-40bc-8282-365470f99107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01587925-9a9f-44de-928b-1edf0a7dde70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c158881d-3ff5-40bc-8282-365470f99107",
                    "LayerId": "9ae72eab-dba6-417d-8c69-2b492779da44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 190,
    "layers": [
        {
            "id": "9ae72eab-dba6-417d-8c69-2b492779da44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2bbf74b-19bc-4199-83ea-5e8006bd6547",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 95
}