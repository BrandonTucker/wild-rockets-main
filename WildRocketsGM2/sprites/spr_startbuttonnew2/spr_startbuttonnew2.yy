{
    "id": "2650a614-375d-4368-8a98-317e663879fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startbuttonnew2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 12,
    "bbox_right": 127,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cf1b802-4c7a-4d08-b8c2-9de8b93c7c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2650a614-375d-4368-8a98-317e663879fa",
            "compositeImage": {
                "id": "9c19368d-d403-4139-b82a-e35c34a1feaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cf1b802-4c7a-4d08-b8c2-9de8b93c7c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "001c5704-1e53-4f8b-a4cd-a7deae6879b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cf1b802-4c7a-4d08-b8c2-9de8b93c7c47",
                    "LayerId": "61e2d0f4-7d3c-4de8-a4b9-712578fbd038"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "61e2d0f4-7d3c-4de8-a4b9-712578fbd038",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2650a614-375d-4368-8a98-317e663879fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 55,
    "yorig": 50
}