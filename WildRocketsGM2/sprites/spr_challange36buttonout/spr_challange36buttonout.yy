{
    "id": "cd058183-8250-47c5-936c-f1843bbd337c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange36buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50353963-7f12-457b-be12-f2da9699a2bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "compositeImage": {
                "id": "ea334b91-a43c-4b63-aebf-63646211a28c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50353963-7f12-457b-be12-f2da9699a2bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38fde6b-74db-4176-aa12-95c7ff46eb1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50353963-7f12-457b-be12-f2da9699a2bc",
                    "LayerId": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee"
                }
            ]
        },
        {
            "id": "065852ae-0450-4a51-bd29-475d470e0136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "compositeImage": {
                "id": "fb0bca76-bc38-4297-b341-ea2d9bc29029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "065852ae-0450-4a51-bd29-475d470e0136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9938f5e9-08f2-4672-a54d-7ace4a2c628b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "065852ae-0450-4a51-bd29-475d470e0136",
                    "LayerId": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee"
                }
            ]
        },
        {
            "id": "be1baf64-8678-4400-9d71-089d01619d3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "compositeImage": {
                "id": "c1642a06-b044-4068-9756-5e45aa20897b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be1baf64-8678-4400-9d71-089d01619d3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1cb659a-6b35-49ec-afea-494de0e44e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1baf64-8678-4400-9d71-089d01619d3c",
                    "LayerId": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee"
                }
            ]
        },
        {
            "id": "da4c05d3-b4ce-4829-9900-306c3de95655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "compositeImage": {
                "id": "67bf2348-7944-4ae1-a0d1-4f3bd378c965",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da4c05d3-b4ce-4829-9900-306c3de95655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f305ffa1-d01c-4869-8a6d-492fca3977b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4c05d3-b4ce-4829-9900-306c3de95655",
                    "LayerId": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee"
                }
            ]
        },
        {
            "id": "6fb57ab9-60f2-4714-969c-154544476818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "compositeImage": {
                "id": "70cb1bfc-5b40-445b-b8a4-b7ec40103381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fb57ab9-60f2-4714-969c-154544476818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "012346ce-2039-47ce-9ddf-6dbf0915645f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fb57ab9-60f2-4714-969c-154544476818",
                    "LayerId": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee"
                }
            ]
        },
        {
            "id": "a0ae9753-f38e-4a45-b565-4368b4e6d987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "compositeImage": {
                "id": "f59d048a-677d-4fbb-8c0e-ad3d6d6d9cd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0ae9753-f38e-4a45-b565-4368b4e6d987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21f2e40f-8e60-45a8-8b02-763906bd8a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0ae9753-f38e-4a45-b565-4368b4e6d987",
                    "LayerId": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee"
                }
            ]
        },
        {
            "id": "8ddcdc19-4450-45d7-a084-ccd587f81ce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "compositeImage": {
                "id": "473340d7-7cb5-422d-aa56-84387d2924f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ddcdc19-4450-45d7-a084-ccd587f81ce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d83f4016-7172-423c-bfa6-3b693c502097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ddcdc19-4450-45d7-a084-ccd587f81ce9",
                    "LayerId": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee"
                }
            ]
        },
        {
            "id": "bf7863a4-613a-4790-9e1e-e509e864be8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "compositeImage": {
                "id": "379c80ef-f5da-47da-9ebf-d1034e0c48ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf7863a4-613a-4790-9e1e-e509e864be8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0266091-6459-4e8a-994e-072f65eb9c32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf7863a4-613a-4790-9e1e-e509e864be8d",
                    "LayerId": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee"
                }
            ]
        },
        {
            "id": "5607fae3-a5eb-448f-9262-dc9e884d1088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "compositeImage": {
                "id": "c329f2fa-df5c-4a3a-826b-5b7f0d018f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5607fae3-a5eb-448f-9262-dc9e884d1088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5015dd34-0b81-4ab5-88a5-69e224b23e1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5607fae3-a5eb-448f-9262-dc9e884d1088",
                    "LayerId": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee"
                }
            ]
        },
        {
            "id": "8c9fdde7-ca9a-400e-aabf-6185a2af82f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "compositeImage": {
                "id": "9793e5c0-d4f9-43d7-9996-dc21939572e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c9fdde7-ca9a-400e-aabf-6185a2af82f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c50babf9-87db-4449-8719-9bcbc3650340",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c9fdde7-ca9a-400e-aabf-6185a2af82f4",
                    "LayerId": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "0c7fd2e8-4e00-4e08-8fbf-cbbe6dd5ebee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd058183-8250-47c5-936c-f1843bbd337c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}