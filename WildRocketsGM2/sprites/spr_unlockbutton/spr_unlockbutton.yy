{
    "id": "5a986cdc-fc23-45ff-9083-d7fa358049ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_unlockbutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 34,
    "bbox_right": 350,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96998236-28e8-4ccc-b76b-749230cf0cf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a986cdc-fc23-45ff-9083-d7fa358049ca",
            "compositeImage": {
                "id": "b118241c-d7ed-4b5a-9cc7-4a443aaf739c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96998236-28e8-4ccc-b76b-749230cf0cf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fa3d4b7-2827-4ba2-952b-dac4ccfd2a04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96998236-28e8-4ccc-b76b-749230cf0cf6",
                    "LayerId": "a14cb7be-b86a-4321-9bd4-8e24f2022eea"
                }
            ]
        },
        {
            "id": "b4d7e387-fa09-4c59-81f6-b8b3fae8f01e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a986cdc-fc23-45ff-9083-d7fa358049ca",
            "compositeImage": {
                "id": "fa231a14-4839-4b66-920f-e00ffd366e39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d7e387-fa09-4c59-81f6-b8b3fae8f01e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd93b0a-163b-4c97-b9ea-dbdb64781da5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d7e387-fa09-4c59-81f6-b8b3fae8f01e",
                    "LayerId": "a14cb7be-b86a-4321-9bd4-8e24f2022eea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 116,
    "layers": [
        {
            "id": "a14cb7be-b86a-4321-9bd4-8e24f2022eea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a986cdc-fc23-45ff-9083-d7fa358049ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 395,
    "xorig": 197,
    "yorig": 58
}