{
    "id": "6691cbd1-6456-4fa4-a053-fed9d3e968c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship1locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f579618-2241-497d-816d-23d4b881e049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6691cbd1-6456-4fa4-a053-fed9d3e968c8",
            "compositeImage": {
                "id": "67a03166-13af-40f8-8d67-f5c16fb7c8bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f579618-2241-497d-816d-23d4b881e049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7840576-4031-4fb2-9df9-5af612bde970",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f579618-2241-497d-816d-23d4b881e049",
                    "LayerId": "ea3e7254-59cf-4d71-857a-c6326d2c00f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "ea3e7254-59cf-4d71-857a-c6326d2c00f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6691cbd1-6456-4fa4-a053-fed9d3e968c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}