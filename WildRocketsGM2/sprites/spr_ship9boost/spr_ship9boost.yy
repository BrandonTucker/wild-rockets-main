{
    "id": "f0b35170-5335-4511-a8d7-40b1441f5897",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship9boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 34,
    "bbox_right": 59,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee513fd1-df30-4841-a684-59fb32f4ea8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0b35170-5335-4511-a8d7-40b1441f5897",
            "compositeImage": {
                "id": "13ac5d3a-6e50-4461-af48-c559289b8e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee513fd1-df30-4841-a684-59fb32f4ea8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f31df9a-3249-4807-8c90-28a85c22d18d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee513fd1-df30-4841-a684-59fb32f4ea8d",
                    "LayerId": "4335b8ac-a389-4e2e-a9d7-5ff1fe463390"
                }
            ]
        },
        {
            "id": "f373dfc7-1251-4acf-9c56-66130d5f008d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0b35170-5335-4511-a8d7-40b1441f5897",
            "compositeImage": {
                "id": "bf60bf0f-e98d-42b9-a552-b39d30682595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f373dfc7-1251-4acf-9c56-66130d5f008d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7da79e3c-df33-41a6-85f6-4df74481b3f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f373dfc7-1251-4acf-9c56-66130d5f008d",
                    "LayerId": "4335b8ac-a389-4e2e-a9d7-5ff1fe463390"
                }
            ]
        },
        {
            "id": "381e34e0-5107-40bf-9ac3-e8ff0f8fc203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0b35170-5335-4511-a8d7-40b1441f5897",
            "compositeImage": {
                "id": "d9a3be21-379d-42ce-8f8f-0f06983aaf16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "381e34e0-5107-40bf-9ac3-e8ff0f8fc203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67da76f0-da6d-4c04-8719-b2b43f8d3cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "381e34e0-5107-40bf-9ac3-e8ff0f8fc203",
                    "LayerId": "4335b8ac-a389-4e2e-a9d7-5ff1fe463390"
                }
            ]
        },
        {
            "id": "3a8394c5-5b0d-4a18-8d51-244a672f24e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0b35170-5335-4511-a8d7-40b1441f5897",
            "compositeImage": {
                "id": "3d595027-6028-42eb-a214-265a90b380ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a8394c5-5b0d-4a18-8d51-244a672f24e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b21b75cd-8205-4d75-9387-e6c0554777e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a8394c5-5b0d-4a18-8d51-244a672f24e8",
                    "LayerId": "4335b8ac-a389-4e2e-a9d7-5ff1fe463390"
                }
            ]
        },
        {
            "id": "081d4221-dce3-495c-ac89-c316bde2a2dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0b35170-5335-4511-a8d7-40b1441f5897",
            "compositeImage": {
                "id": "e9a40568-f39e-46f6-b207-4515eb72955e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "081d4221-dce3-495c-ac89-c316bde2a2dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a3e728e-127c-4037-91e9-f0431e1a19d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "081d4221-dce3-495c-ac89-c316bde2a2dd",
                    "LayerId": "4335b8ac-a389-4e2e-a9d7-5ff1fe463390"
                }
            ]
        },
        {
            "id": "29c82649-859d-4f3d-bd9f-1dc27c0d7386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0b35170-5335-4511-a8d7-40b1441f5897",
            "compositeImage": {
                "id": "560e56d0-5a35-4b94-8aa1-f31f2faeeeb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c82649-859d-4f3d-bd9f-1dc27c0d7386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da94bc0b-b2ae-4408-90e1-d580342d0bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c82649-859d-4f3d-bd9f-1dc27c0d7386",
                    "LayerId": "4335b8ac-a389-4e2e-a9d7-5ff1fe463390"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 147,
    "layers": [
        {
            "id": "4335b8ac-a389-4e2e-a9d7-5ff1fe463390",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0b35170-5335-4511-a8d7-40b1441f5897",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 46,
    "yorig": 51
}