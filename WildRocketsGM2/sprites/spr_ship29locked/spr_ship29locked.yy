{
    "id": "c1f4b280-3cd5-4296-92e2-2f9ad6186b31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship29locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76b06e46-ec0a-4362-add5-4ec2ccafe5ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1f4b280-3cd5-4296-92e2-2f9ad6186b31",
            "compositeImage": {
                "id": "80fc809c-275a-493e-b0a7-1507cffb2383",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76b06e46-ec0a-4362-add5-4ec2ccafe5ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03d2ddb5-4245-4b0b-8f36-9618c9181c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76b06e46-ec0a-4362-add5-4ec2ccafe5ac",
                    "LayerId": "4dffd5b0-a735-42f6-8914-19cdbb5f1cbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4dffd5b0-a735-42f6-8914-19cdbb5f1cbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1f4b280-3cd5-4296-92e2-2f9ad6186b31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}