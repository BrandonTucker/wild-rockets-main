{
    "id": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rareship9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 132,
    "bbox_left": 39,
    "bbox_right": 86,
    "bbox_top": 53,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b19a6f91-f32d-4191-b6e6-1ed57a53f597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "compositeImage": {
                "id": "2c7cbd48-1dbf-4a4a-941a-a5ed07aa7b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b19a6f91-f32d-4191-b6e6-1ed57a53f597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f55ee2-6d9d-4708-bc65-0627e5e63192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b19a6f91-f32d-4191-b6e6-1ed57a53f597",
                    "LayerId": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a"
                }
            ]
        },
        {
            "id": "ac6f94f4-d73a-414e-8c81-be416b135cf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "compositeImage": {
                "id": "b6caf004-1fe9-4835-9032-21087499a359",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac6f94f4-d73a-414e-8c81-be416b135cf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91c4805c-c94d-477f-93de-c88cab677b30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac6f94f4-d73a-414e-8c81-be416b135cf1",
                    "LayerId": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a"
                }
            ]
        },
        {
            "id": "a5feb417-10b5-436e-b84d-78667afccc5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "compositeImage": {
                "id": "02e886be-9801-4ba1-b8a2-3f484f2bded8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5feb417-10b5-436e-b84d-78667afccc5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7be804a-5f92-4a34-9f51-0d1dce0f7770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5feb417-10b5-436e-b84d-78667afccc5b",
                    "LayerId": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a"
                }
            ]
        },
        {
            "id": "01fc079d-a661-410b-8e9a-5c7eb4cdc0d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "compositeImage": {
                "id": "bc420089-7aaf-4f3a-814c-87e6fcea9035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01fc079d-a661-410b-8e9a-5c7eb4cdc0d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0db35e72-bda4-4894-a285-2477d12121c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01fc079d-a661-410b-8e9a-5c7eb4cdc0d2",
                    "LayerId": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a"
                }
            ]
        },
        {
            "id": "7e7323b6-9cc5-47bc-92cc-a8eacab7689c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "compositeImage": {
                "id": "d01e8bc1-b378-480d-b417-a9bd92c97ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e7323b6-9cc5-47bc-92cc-a8eacab7689c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e894eea1-2899-48b8-9294-8adfe297c538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e7323b6-9cc5-47bc-92cc-a8eacab7689c",
                    "LayerId": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a"
                }
            ]
        },
        {
            "id": "ab75ed2d-dc27-49b5-b817-9159f095ab9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "compositeImage": {
                "id": "e9cddfce-8399-493d-a59d-6a0fc47a3c5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab75ed2d-dc27-49b5-b817-9159f095ab9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24903c3a-f34b-402d-9c46-b8180f4dbf09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab75ed2d-dc27-49b5-b817-9159f095ab9d",
                    "LayerId": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a"
                }
            ]
        },
        {
            "id": "9ea0961e-5a37-434a-b5bf-0929478fd64a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "compositeImage": {
                "id": "c524f326-3216-4dd6-87e8-be204ce68691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ea0961e-5a37-434a-b5bf-0929478fd64a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d8ee2e1-0ee3-4c63-9576-bbb6e4753d34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ea0961e-5a37-434a-b5bf-0929478fd64a",
                    "LayerId": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a"
                }
            ]
        },
        {
            "id": "f652316d-3eb4-4290-a16f-b67c563d41c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "compositeImage": {
                "id": "923e3504-74a9-4fed-b854-d4394957ea2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f652316d-3eb4-4290-a16f-b67c563d41c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e8db393-a462-445d-ae76-859895d59750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f652316d-3eb4-4290-a16f-b67c563d41c5",
                    "LayerId": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a"
                }
            ]
        },
        {
            "id": "26e5e7c9-253b-4d0d-bee7-89cec8a1b10c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "compositeImage": {
                "id": "a8368f4f-0e4c-4953-a331-7c8ec7a19437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26e5e7c9-253b-4d0d-bee7-89cec8a1b10c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8376e928-f21c-4d40-9da7-ae53c66070c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26e5e7c9-253b-4d0d-bee7-89cec8a1b10c",
                    "LayerId": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a"
                }
            ]
        },
        {
            "id": "bf0411b5-d4d2-4ea7-8fd4-212fb36fbc3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "compositeImage": {
                "id": "a8cdf523-42d7-4fa6-815f-638eb9a11845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf0411b5-d4d2-4ea7-8fd4-212fb36fbc3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ba3537-0966-4dc1-a50c-b0101f4f64e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf0411b5-d4d2-4ea7-8fd4-212fb36fbc3b",
                    "LayerId": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "59e2cbad-fc0a-4f53-95e8-f76bf6cc7f1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5e5b51b-4ed2-49f8-85bd-88e7ef41bf47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 62,
    "yorig": 93
}