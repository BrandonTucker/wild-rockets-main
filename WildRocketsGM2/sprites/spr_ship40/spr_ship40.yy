{
    "id": "8a2dc00f-51ea-4f7d-8d14-56cf8f9cabc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship40",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 136,
    "bbox_left": 47,
    "bbox_right": 71,
    "bbox_top": 44,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b62cc165-fe06-4989-bfb8-89eea1aa0b60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a2dc00f-51ea-4f7d-8d14-56cf8f9cabc7",
            "compositeImage": {
                "id": "2e8b7592-9fb8-4343-a876-ce3617605e59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b62cc165-fe06-4989-bfb8-89eea1aa0b60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd19d154-028c-4c4a-be8b-8a2e585a02e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b62cc165-fe06-4989-bfb8-89eea1aa0b60",
                    "LayerId": "9a4ac520-6809-4d76-9cf8-14161e75051b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "9a4ac520-6809-4d76-9cf8-14161e75051b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a2dc00f-51ea-4f7d-8d14-56cf8f9cabc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 60,
    "yorig": 67
}