{
    "id": "20405e58-8b17-4b76-9b86-47451ab97716",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange40buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19ed813d-ac97-40e1-a161-9a65545ae0fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "compositeImage": {
                "id": "b6a279a8-f0ee-45c4-b2b6-8be9ee2d0a01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ed813d-ac97-40e1-a161-9a65545ae0fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb91b5b7-f821-4b80-85f0-37f641304950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ed813d-ac97-40e1-a161-9a65545ae0fa",
                    "LayerId": "15201e3b-a0d0-489c-b81c-690afc719e56"
                }
            ]
        },
        {
            "id": "46787b11-e52d-48ec-89a4-70c0d6ac8fd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "compositeImage": {
                "id": "ed71f9bc-c020-4d9e-a556-f89aaf0e4765",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46787b11-e52d-48ec-89a4-70c0d6ac8fd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4c081a9-51ae-4d5f-b3dd-0314038cffba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46787b11-e52d-48ec-89a4-70c0d6ac8fd0",
                    "LayerId": "15201e3b-a0d0-489c-b81c-690afc719e56"
                }
            ]
        },
        {
            "id": "56d69664-7c52-410e-99c5-af3d9aeafc38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "compositeImage": {
                "id": "09e630f3-35ec-417c-940b-3dcbf9479c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56d69664-7c52-410e-99c5-af3d9aeafc38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9814fc8f-95fe-4781-b4bc-41a312d7f9fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56d69664-7c52-410e-99c5-af3d9aeafc38",
                    "LayerId": "15201e3b-a0d0-489c-b81c-690afc719e56"
                }
            ]
        },
        {
            "id": "5c210a19-a0c6-4c40-981c-56b665da9b6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "compositeImage": {
                "id": "74027e65-d5d9-4143-b851-27b27a0f0b6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c210a19-a0c6-4c40-981c-56b665da9b6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "098dcbf9-cb85-49fc-a7e8-0aa77a3e4381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c210a19-a0c6-4c40-981c-56b665da9b6d",
                    "LayerId": "15201e3b-a0d0-489c-b81c-690afc719e56"
                }
            ]
        },
        {
            "id": "462d279d-bb03-49c9-9398-a0619c089a3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "compositeImage": {
                "id": "88fb805d-7c9d-42d2-b305-df28085b58ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "462d279d-bb03-49c9-9398-a0619c089a3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2634d21-1758-4584-8ff9-8fc663c4ccc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462d279d-bb03-49c9-9398-a0619c089a3c",
                    "LayerId": "15201e3b-a0d0-489c-b81c-690afc719e56"
                }
            ]
        },
        {
            "id": "45761002-baa8-4334-bf63-0ec157b42ea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "compositeImage": {
                "id": "10389f4d-95f6-461b-a40b-378d57ea561d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45761002-baa8-4334-bf63-0ec157b42ea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d025f48-1954-4199-a39e-a9ecca433083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45761002-baa8-4334-bf63-0ec157b42ea3",
                    "LayerId": "15201e3b-a0d0-489c-b81c-690afc719e56"
                }
            ]
        },
        {
            "id": "442b31cb-729a-4a4c-842b-1f91aa92ec00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "compositeImage": {
                "id": "30337a7b-bd39-4c01-bd46-0032d324ece9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "442b31cb-729a-4a4c-842b-1f91aa92ec00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af856712-96be-4015-85c7-d490d3a6b7b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "442b31cb-729a-4a4c-842b-1f91aa92ec00",
                    "LayerId": "15201e3b-a0d0-489c-b81c-690afc719e56"
                }
            ]
        },
        {
            "id": "9f7384f2-e97c-4ec1-ab47-836b5de6fbcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "compositeImage": {
                "id": "d74f3ae7-8a2f-48a6-9631-1d9828ba6dbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f7384f2-e97c-4ec1-ab47-836b5de6fbcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab2e9e18-45f2-4f9a-8041-ac02e89dc5e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f7384f2-e97c-4ec1-ab47-836b5de6fbcf",
                    "LayerId": "15201e3b-a0d0-489c-b81c-690afc719e56"
                }
            ]
        },
        {
            "id": "993edbd4-d87a-413b-84bb-eec1ef1976fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "compositeImage": {
                "id": "83f3f0e6-9610-48ec-9853-db3c30dbc093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "993edbd4-d87a-413b-84bb-eec1ef1976fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ceb2225-955e-4c45-ae6a-32ac01dd6a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "993edbd4-d87a-413b-84bb-eec1ef1976fb",
                    "LayerId": "15201e3b-a0d0-489c-b81c-690afc719e56"
                }
            ]
        },
        {
            "id": "f374dc81-8ace-42fb-acc2-313a2c042e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "compositeImage": {
                "id": "cc2607eb-c0ae-494c-a4e0-9b18e7f76ba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f374dc81-8ace-42fb-acc2-313a2c042e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "369d7069-d62f-4d49-92b6-25bdce81e398",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f374dc81-8ace-42fb-acc2-313a2c042e3c",
                    "LayerId": "15201e3b-a0d0-489c-b81c-690afc719e56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "15201e3b-a0d0-489c-b81c-690afc719e56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20405e58-8b17-4b76-9b86-47451ab97716",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 52
}