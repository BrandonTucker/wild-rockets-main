{
    "id": "b3792838-14da-4265-82d4-35c67160ab93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ruby",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 9,
    "bbox_right": 64,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43cb1859-ca33-42ac-bcac-8fdfd58e5193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "801996a7-6800-4700-a17e-836be7e58c04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43cb1859-ca33-42ac-bcac-8fdfd58e5193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bed9a8e8-1c9d-41c7-bb59-f08fecd4d945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43cb1859-ca33-42ac-bcac-8fdfd58e5193",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        },
        {
            "id": "5ae291a7-e1e4-4d9e-b53e-14fbfd11174a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "8f25fc5d-1ac9-4e69-a516-f11857ffb419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ae291a7-e1e4-4d9e-b53e-14fbfd11174a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c48e9c7f-b727-4648-bf75-59c3906de059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ae291a7-e1e4-4d9e-b53e-14fbfd11174a",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        },
        {
            "id": "4490a09b-2d8f-4f04-b759-486ce9f7646f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "afd619b3-c6fa-4d30-9092-2b6d59a8df34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4490a09b-2d8f-4f04-b759-486ce9f7646f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d1dfe25-3db3-47e3-88a3-694b31b268b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4490a09b-2d8f-4f04-b759-486ce9f7646f",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        },
        {
            "id": "bf898744-c132-4707-8b78-d59b1dd91e28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "09a084dd-ac7f-4cb7-aa0a-60cb1140fd60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf898744-c132-4707-8b78-d59b1dd91e28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "680dfed3-b86f-4192-9087-438f94fd71db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf898744-c132-4707-8b78-d59b1dd91e28",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        },
        {
            "id": "1baff1a8-04ff-4386-8161-4475d82befad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "e6159d40-3d9e-4d5a-9331-11582d02a497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1baff1a8-04ff-4386-8161-4475d82befad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cd19b96-e639-4735-bfe1-27a95fd9a44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1baff1a8-04ff-4386-8161-4475d82befad",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        },
        {
            "id": "1e582710-b9ef-4e97-8f97-ba767e885ec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "a4f1aff5-7dc9-4709-b004-b766442b98ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e582710-b9ef-4e97-8f97-ba767e885ec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "729e745a-7856-40ee-b5c3-b0fea30c8554",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e582710-b9ef-4e97-8f97-ba767e885ec6",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        },
        {
            "id": "75b9ac19-8fb1-4e9b-88f1-ac98ae219e53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "073dc7f4-1c56-4648-867e-cca76335490b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75b9ac19-8fb1-4e9b-88f1-ac98ae219e53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1682cae3-1c0a-4067-a017-256669e3b015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75b9ac19-8fb1-4e9b-88f1-ac98ae219e53",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        },
        {
            "id": "b7742bd1-955b-44b6-bae0-410a3ceff476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "94f24d84-c582-47d7-bf0e-34b5ff69ca1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7742bd1-955b-44b6-bae0-410a3ceff476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3824e926-2cd4-4911-aa49-65a1a5d6055c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7742bd1-955b-44b6-bae0-410a3ceff476",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        },
        {
            "id": "e9fd267a-f8b8-4ef8-9cf0-07e66baf8d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "e3c8cf55-4164-4d3a-af5a-bb64e202f7a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9fd267a-f8b8-4ef8-9cf0-07e66baf8d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c049561-2300-4d23-9d4c-85a70bf202fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9fd267a-f8b8-4ef8-9cf0-07e66baf8d30",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        },
        {
            "id": "2b91ad36-dfb5-4b12-8356-e1cd30556ccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "34e52499-1e1e-4d60-9348-9e8d9446ec8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b91ad36-dfb5-4b12-8356-e1cd30556ccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "975e7d2b-ca7d-49db-b401-e0e179b27a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b91ad36-dfb5-4b12-8356-e1cd30556ccf",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        },
        {
            "id": "617b6955-b4da-49f3-98de-a297c6de39b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "compositeImage": {
                "id": "f75b8c3c-47d7-419b-88d9-539fc607f1e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "617b6955-b4da-49f3-98de-a297c6de39b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "531a7ab8-d38e-4eed-b698-05da364c6021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "617b6955-b4da-49f3-98de-a297c6de39b7",
                    "LayerId": "862c8fa9-7d1e-40be-b25b-930b3115e0ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "862c8fa9-7d1e-40be-b25b-930b3115e0ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3792838-14da-4265-82d4-35c67160ab93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 37,
    "yorig": 48
}