{
    "id": "76bbd08c-7cd2-4ee1-a53d-6a3ad441109d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship25",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b631ed35-970f-4414-a95b-9ce4eab0c94d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76bbd08c-7cd2-4ee1-a53d-6a3ad441109d",
            "compositeImage": {
                "id": "a8b90b5e-a602-4f75-b2a0-37e9989dceea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b631ed35-970f-4414-a95b-9ce4eab0c94d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21cb1ebf-651e-4a7e-b808-b4989f3c5fde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b631ed35-970f-4414-a95b-9ce4eab0c94d",
                    "LayerId": "c1849d66-e07a-4371-933c-392b9e91f7b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "c1849d66-e07a-4371-933c-392b9e91f7b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76bbd08c-7cd2-4ee1-a53d-6a3ad441109d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 51
}