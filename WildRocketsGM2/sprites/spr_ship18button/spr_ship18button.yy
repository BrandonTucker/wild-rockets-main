{
    "id": "40c7553a-055c-49bc-8116-04183668eedf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship18button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ff86f9f-f910-468d-94e1-944f7487d1ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40c7553a-055c-49bc-8116-04183668eedf",
            "compositeImage": {
                "id": "98bbb396-6e70-4e87-a6a7-969904258f6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ff86f9f-f910-468d-94e1-944f7487d1ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c743082c-5a3a-423d-b2d7-5d0b3934783f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ff86f9f-f910-468d-94e1-944f7487d1ec",
                    "LayerId": "cb253baf-0695-4e72-a77f-5f2de5e3d62d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "cb253baf-0695-4e72-a77f-5f2de5e3d62d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40c7553a-055c-49bc-8116-04183668eedf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}