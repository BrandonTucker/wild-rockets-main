{
    "id": "786c6eaa-4d8e-4878-915a-8d65845d70db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship22",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3445a9dd-6664-468a-9982-63514cf3e491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "compositeImage": {
                "id": "f8d95ff5-b86c-48fd-bd9c-013fd8ba207e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3445a9dd-6664-468a-9982-63514cf3e491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f68076d-bcde-4503-9988-9d7e58476139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3445a9dd-6664-468a-9982-63514cf3e491",
                    "LayerId": "3543b55d-6530-4666-9737-d2c4f0096db9"
                }
            ]
        },
        {
            "id": "e9a8dc4b-3236-4d40-857f-c61a64244aa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "compositeImage": {
                "id": "002c31a6-4631-4633-b6ac-408aff4bd9c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9a8dc4b-3236-4d40-857f-c61a64244aa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b450b8ca-de03-4816-b66a-aa217c894ede",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9a8dc4b-3236-4d40-857f-c61a64244aa7",
                    "LayerId": "3543b55d-6530-4666-9737-d2c4f0096db9"
                }
            ]
        },
        {
            "id": "c30ae7db-cadf-449b-bdc7-67bb2f2364b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "compositeImage": {
                "id": "f39ae77a-00d6-47a8-9d3a-88036957b3c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c30ae7db-cadf-449b-bdc7-67bb2f2364b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bd54b15-c5d3-44f7-9e73-0d446ebb9efc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c30ae7db-cadf-449b-bdc7-67bb2f2364b0",
                    "LayerId": "3543b55d-6530-4666-9737-d2c4f0096db9"
                }
            ]
        },
        {
            "id": "1a9a3488-8bc9-4a69-9f90-00de141d7c34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "compositeImage": {
                "id": "acdee3e2-4f9a-4d36-b6cf-710ba7814aaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a9a3488-8bc9-4a69-9f90-00de141d7c34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d50e9b6-8790-4422-82a5-2638d3320eef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9a3488-8bc9-4a69-9f90-00de141d7c34",
                    "LayerId": "3543b55d-6530-4666-9737-d2c4f0096db9"
                }
            ]
        },
        {
            "id": "af5579df-fc28-4898-b702-e5a0427cfad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "compositeImage": {
                "id": "49f78298-6e40-453c-8e47-d86679cb6d9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af5579df-fc28-4898-b702-e5a0427cfad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7b31648-adac-4293-9176-fcfd52294dc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af5579df-fc28-4898-b702-e5a0427cfad9",
                    "LayerId": "3543b55d-6530-4666-9737-d2c4f0096db9"
                }
            ]
        },
        {
            "id": "872d51d3-523b-4ffb-b94e-1a23a467d0b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "compositeImage": {
                "id": "6dd6b73d-4562-4b90-94d1-05ca84eb18fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "872d51d3-523b-4ffb-b94e-1a23a467d0b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76ffa7a7-49cb-4458-9771-1c337281f194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "872d51d3-523b-4ffb-b94e-1a23a467d0b2",
                    "LayerId": "3543b55d-6530-4666-9737-d2c4f0096db9"
                }
            ]
        },
        {
            "id": "33d6d758-a1d5-4eb7-9cee-5fc7897d2da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "compositeImage": {
                "id": "1ca6522c-19c7-4e86-a22d-ce53ba448f59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d6d758-a1d5-4eb7-9cee-5fc7897d2da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff0a387-f369-40dd-b46b-b2f7dbc8c3aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d6d758-a1d5-4eb7-9cee-5fc7897d2da1",
                    "LayerId": "3543b55d-6530-4666-9737-d2c4f0096db9"
                }
            ]
        },
        {
            "id": "885bf4c4-1be4-49c7-bd30-af1efe9abbe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "compositeImage": {
                "id": "35cb76a4-3afb-411b-a49a-371216d24d6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "885bf4c4-1be4-49c7-bd30-af1efe9abbe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cc8944a-33d6-42ad-8b14-a29a768e960a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "885bf4c4-1be4-49c7-bd30-af1efe9abbe5",
                    "LayerId": "3543b55d-6530-4666-9737-d2c4f0096db9"
                }
            ]
        },
        {
            "id": "e6d06261-0300-4d20-b35f-d3e031e4e09d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "compositeImage": {
                "id": "d1ccb815-e153-45e6-a237-d4642211b8ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6d06261-0300-4d20-b35f-d3e031e4e09d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9390cf99-c26f-4ac2-8291-6e515ca82892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6d06261-0300-4d20-b35f-d3e031e4e09d",
                    "LayerId": "3543b55d-6530-4666-9737-d2c4f0096db9"
                }
            ]
        },
        {
            "id": "a74b01ab-5e14-4506-a1ca-4f219ccb7fd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "compositeImage": {
                "id": "7a7dc17d-6494-4a73-8577-7b77d2008a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a74b01ab-5e14-4506-a1ca-4f219ccb7fd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12fb875c-00c4-4157-a645-eed619d6a711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a74b01ab-5e14-4506-a1ca-4f219ccb7fd9",
                    "LayerId": "3543b55d-6530-4666-9737-d2c4f0096db9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "3543b55d-6530-4666-9737-d2c4f0096db9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "786c6eaa-4d8e-4878-915a-8d65845d70db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 49
}