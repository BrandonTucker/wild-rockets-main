{
    "id": "227ea4c9-5cb7-4a59-9ed3-234bbce68ef4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 23,
    "bbox_right": 48,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbecfda2-ef0f-447f-8361-cd364f461d71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "227ea4c9-5cb7-4a59-9ed3-234bbce68ef4",
            "compositeImage": {
                "id": "231fffaa-3b04-4a67-bb5a-f788b38f0af9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbecfda2-ef0f-447f-8361-cd364f461d71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72f5ccd4-82fb-4fe3-acc3-124606138b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbecfda2-ef0f-447f-8361-cd364f461d71",
                    "LayerId": "581efe9f-f2fb-4ec2-b767-d3aff0284432"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "581efe9f-f2fb-4ec2-b767-d3aff0284432",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "227ea4c9-5cb7-4a59-9ed3-234bbce68ef4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 36,
    "yorig": 40
}