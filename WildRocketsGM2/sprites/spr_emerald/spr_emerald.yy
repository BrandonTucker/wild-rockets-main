{
    "id": "860f0946-d17b-4774-8d87-b8567641c51a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_emerald",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": -11,
    "bbox_right": 43,
    "bbox_top": -3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "174e8338-c42c-4023-a0f6-e6842fb9361f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
            "compositeImage": {
                "id": "0d533d7a-541a-4103-bff6-435a23b91be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "174e8338-c42c-4023-a0f6-e6842fb9361f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2afea90b-e8c2-4ecd-9eff-6aa355781905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "174e8338-c42c-4023-a0f6-e6842fb9361f",
                    "LayerId": "900f1617-5b53-4147-a684-0a590205d343"
                }
            ]
        },
        {
            "id": "0ec50cfe-c269-4e89-8ee5-3343b8eac01d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
            "compositeImage": {
                "id": "a0173fdc-b90e-469a-9156-0e308d235d1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ec50cfe-c269-4e89-8ee5-3343b8eac01d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d9d2eca-5f5d-4b9c-abce-5e617a98298f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ec50cfe-c269-4e89-8ee5-3343b8eac01d",
                    "LayerId": "900f1617-5b53-4147-a684-0a590205d343"
                }
            ]
        },
        {
            "id": "c0b44b14-0970-464e-a7bb-22f4e8935b8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
            "compositeImage": {
                "id": "7077fcb5-eee8-41f0-b0e9-2b3235ecd556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0b44b14-0970-464e-a7bb-22f4e8935b8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f23b036d-5ea7-4afe-9fcc-3dcba4246903",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0b44b14-0970-464e-a7bb-22f4e8935b8a",
                    "LayerId": "900f1617-5b53-4147-a684-0a590205d343"
                }
            ]
        },
        {
            "id": "b29bed01-3a7b-4b15-bd5c-4394a0c7c584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
            "compositeImage": {
                "id": "dceda113-d8d8-4dca-a7cb-70beb0f14dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b29bed01-3a7b-4b15-bd5c-4394a0c7c584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc310593-bc54-4b44-8287-2fde8da2d626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b29bed01-3a7b-4b15-bd5c-4394a0c7c584",
                    "LayerId": "900f1617-5b53-4147-a684-0a590205d343"
                }
            ]
        },
        {
            "id": "7f1e23e4-5f87-4cfd-8b51-dce850a8a2de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
            "compositeImage": {
                "id": "53e062df-2a64-4ae5-836a-0a9335567287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f1e23e4-5f87-4cfd-8b51-dce850a8a2de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd79540f-8d07-48c8-a4f1-1410259b67e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f1e23e4-5f87-4cfd-8b51-dce850a8a2de",
                    "LayerId": "900f1617-5b53-4147-a684-0a590205d343"
                }
            ]
        },
        {
            "id": "99161719-462a-4c1b-bf15-18ddbaaa4506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
            "compositeImage": {
                "id": "703118c7-9da5-4a08-8a65-b6ab95e42282",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99161719-462a-4c1b-bf15-18ddbaaa4506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c14b67ad-369e-4e8b-807d-9d424e21e0c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99161719-462a-4c1b-bf15-18ddbaaa4506",
                    "LayerId": "900f1617-5b53-4147-a684-0a590205d343"
                }
            ]
        },
        {
            "id": "4ea4288b-eb31-4eb9-9810-d3b9bfeed519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
            "compositeImage": {
                "id": "b01d200a-ffec-484f-9690-ef00150b684b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ea4288b-eb31-4eb9-9810-d3b9bfeed519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4777543f-3efc-45a4-b6ec-18adffd423ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ea4288b-eb31-4eb9-9810-d3b9bfeed519",
                    "LayerId": "900f1617-5b53-4147-a684-0a590205d343"
                }
            ]
        },
        {
            "id": "29165d88-ae1b-4cf1-a824-4cff9ef8ca06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
            "compositeImage": {
                "id": "9e4d7005-e092-4a29-ae15-0c55e0606758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29165d88-ae1b-4cf1-a824-4cff9ef8ca06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea3b417-cb14-44e0-873d-6941b2be9466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29165d88-ae1b-4cf1-a824-4cff9ef8ca06",
                    "LayerId": "900f1617-5b53-4147-a684-0a590205d343"
                }
            ]
        },
        {
            "id": "10226b0e-9e53-4ed4-a1a2-79251443c470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
            "compositeImage": {
                "id": "492d9bba-000e-4b1e-a033-41edf3e7e6f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10226b0e-9e53-4ed4-a1a2-79251443c470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97dabdce-1e26-4693-92d5-3583b0c5d02c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10226b0e-9e53-4ed4-a1a2-79251443c470",
                    "LayerId": "900f1617-5b53-4147-a684-0a590205d343"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "900f1617-5b53-4147-a684-0a590205d343",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "860f0946-d17b-4774-8d87-b8567641c51a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 17,
    "yorig": 16
}