{
    "id": "97a012b4-7de9-4ed8-982a-6736e91f0702",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage13button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ba95e15-406e-49e0-9793-11981fc41355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97a012b4-7de9-4ed8-982a-6736e91f0702",
            "compositeImage": {
                "id": "3e2ea3e2-590d-49fd-93b1-e99d6b0d41fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ba95e15-406e-49e0-9793-11981fc41355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e812a31-48d8-4234-9858-c6bbd55b27b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba95e15-406e-49e0-9793-11981fc41355",
                    "LayerId": "27954e06-c0db-4f7b-a794-8b54f3087c17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "27954e06-c0db-4f7b-a794-8b54f3087c17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97a012b4-7de9-4ed8-982a-6736e91f0702",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}