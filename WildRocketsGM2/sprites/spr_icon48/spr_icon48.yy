{
    "id": "a3aaf0eb-63c4-47db-837f-7ef03ee9de6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon48",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 4,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8bc85ec-e241-47a7-bf6f-086048257652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3aaf0eb-63c4-47db-837f-7ef03ee9de6e",
            "compositeImage": {
                "id": "6590563c-11a3-41ac-8f8f-11d1b3dd68fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8bc85ec-e241-47a7-bf6f-086048257652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545cb09a-b877-4342-9d9b-cd43e3fda2ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8bc85ec-e241-47a7-bf6f-086048257652",
                    "LayerId": "24ea8793-acdd-44d3-8b2f-d9c5a2a957ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "24ea8793-acdd-44d3-8b2f-d9c5a2a957ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3aaf0eb-63c4-47db-837f-7ef03ee9de6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}