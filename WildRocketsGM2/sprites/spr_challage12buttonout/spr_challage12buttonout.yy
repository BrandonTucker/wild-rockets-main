{
    "id": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage12buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ccd747c-54e2-4fca-8069-cbd390299a43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "compositeImage": {
                "id": "a9f84420-1d5d-4972-8ab5-72bd075c566d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ccd747c-54e2-4fca-8069-cbd390299a43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e25d810e-6b7a-423c-a67e-680087cb927a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ccd747c-54e2-4fca-8069-cbd390299a43",
                    "LayerId": "3d3df1a7-682d-434b-9bc9-72c231dd42cd"
                }
            ]
        },
        {
            "id": "83f267d7-4731-4d75-80d3-78f7bbbc69cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "compositeImage": {
                "id": "6c6989ac-0997-4ea5-ba4c-0b9a1863abd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83f267d7-4731-4d75-80d3-78f7bbbc69cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f252e514-7088-49ce-82fb-8ac86cba5859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83f267d7-4731-4d75-80d3-78f7bbbc69cc",
                    "LayerId": "3d3df1a7-682d-434b-9bc9-72c231dd42cd"
                }
            ]
        },
        {
            "id": "faa7bc2b-0549-44c8-b1f3-949ec0f61f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "compositeImage": {
                "id": "87b8554d-cfe4-4c08-a833-0fd818535873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faa7bc2b-0549-44c8-b1f3-949ec0f61f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff2f8c59-aa35-4ea8-a0c4-1d5c85d0347c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faa7bc2b-0549-44c8-b1f3-949ec0f61f84",
                    "LayerId": "3d3df1a7-682d-434b-9bc9-72c231dd42cd"
                }
            ]
        },
        {
            "id": "68b6bcd5-132f-446a-91ae-e0bd6f826496",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "compositeImage": {
                "id": "88a65b1f-ea05-4632-be9a-a8535f9347d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68b6bcd5-132f-446a-91ae-e0bd6f826496",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ebc8bac-1425-4a9b-bf07-6385208fd83f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68b6bcd5-132f-446a-91ae-e0bd6f826496",
                    "LayerId": "3d3df1a7-682d-434b-9bc9-72c231dd42cd"
                }
            ]
        },
        {
            "id": "1daa5853-7b47-425c-a77c-bd6293cf4ea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "compositeImage": {
                "id": "5bee0e7f-94ff-4123-a3d6-3becadad28d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1daa5853-7b47-425c-a77c-bd6293cf4ea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23683e32-f125-4193-ab30-c7e2fdeebc7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1daa5853-7b47-425c-a77c-bd6293cf4ea4",
                    "LayerId": "3d3df1a7-682d-434b-9bc9-72c231dd42cd"
                }
            ]
        },
        {
            "id": "b75cc47e-b3a7-4725-8074-db3c95cf1eeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "compositeImage": {
                "id": "5f5eb395-b79f-4709-8e5c-5e9fa7b64cdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b75cc47e-b3a7-4725-8074-db3c95cf1eeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e5dc2e9-92a2-4a69-a12d-a24a80f986fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b75cc47e-b3a7-4725-8074-db3c95cf1eeb",
                    "LayerId": "3d3df1a7-682d-434b-9bc9-72c231dd42cd"
                }
            ]
        },
        {
            "id": "c3e7b8b6-3096-453f-b1d9-ba049430ccfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "compositeImage": {
                "id": "36b4ea9a-b4f8-428f-9d28-1499c55230ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3e7b8b6-3096-453f-b1d9-ba049430ccfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b195db4-e5c7-4fee-b50e-ec1640ece420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3e7b8b6-3096-453f-b1d9-ba049430ccfa",
                    "LayerId": "3d3df1a7-682d-434b-9bc9-72c231dd42cd"
                }
            ]
        },
        {
            "id": "affbbc60-7596-451c-b9ee-cc2675b30517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "compositeImage": {
                "id": "5fdaa03f-d3e6-4fa4-b337-48a579d2e400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "affbbc60-7596-451c-b9ee-cc2675b30517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ebc5dcb-977c-40c8-82a2-98e116777ce8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "affbbc60-7596-451c-b9ee-cc2675b30517",
                    "LayerId": "3d3df1a7-682d-434b-9bc9-72c231dd42cd"
                }
            ]
        },
        {
            "id": "71b55bd8-3c86-419f-b06c-1cd0b4bde8f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "compositeImage": {
                "id": "ce4f6664-a396-426e-8927-01a24263f914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b55bd8-3c86-419f-b06c-1cd0b4bde8f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "478404c2-0d9e-416a-8a54-b6f83164fb48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b55bd8-3c86-419f-b06c-1cd0b4bde8f1",
                    "LayerId": "3d3df1a7-682d-434b-9bc9-72c231dd42cd"
                }
            ]
        },
        {
            "id": "d62f1168-bfa9-4a12-847f-ffefa7142ce7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "compositeImage": {
                "id": "3ace00cb-fa36-410c-bcdf-bb118a3523d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d62f1168-bfa9-4a12-847f-ffefa7142ce7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c18a0c57-d6cd-434f-8cfc-1bf74cc20e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d62f1168-bfa9-4a12-847f-ffefa7142ce7",
                    "LayerId": "3d3df1a7-682d-434b-9bc9-72c231dd42cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "3d3df1a7-682d-434b-9bc9-72c231dd42cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a19b862-1c69-4e15-a0e7-a625d8c33887",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}