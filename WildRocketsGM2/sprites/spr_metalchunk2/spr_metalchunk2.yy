{
    "id": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_metalchunk2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cffff3b2-4773-4335-b522-4f7adcdc8972",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "8a0e5df7-4897-4fcb-8eb0-73bcbc67e26e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cffff3b2-4773-4335-b522-4f7adcdc8972",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16300f08-9771-42d6-a186-d4b26b3ad94f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffff3b2-4773-4335-b522-4f7adcdc8972",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "837a1c03-1fc0-4807-b89e-586b49cca20b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "814b5e1b-5b70-457d-90b3-1c9513f6a9fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "837a1c03-1fc0-4807-b89e-586b49cca20b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "924d9868-161a-4fd3-9930-1048b3267d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "837a1c03-1fc0-4807-b89e-586b49cca20b",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "6937723d-a962-4517-8b53-c5e900176f01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "310b9905-443f-47f4-95ce-4d1c78f6a79d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6937723d-a962-4517-8b53-c5e900176f01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50116dd6-3d58-42b2-8393-f64f721c36d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6937723d-a962-4517-8b53-c5e900176f01",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "123e0c59-a074-4b1a-9e89-1d8ec6e47e92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "909b5cbe-47f5-4b6a-914c-08764a433ab1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "123e0c59-a074-4b1a-9e89-1d8ec6e47e92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051b44b3-6e72-4849-a458-87f3d7ef033f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "123e0c59-a074-4b1a-9e89-1d8ec6e47e92",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "0c5ef7de-2c40-4b35-8219-39b188d90487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "22039a69-94d6-4435-8777-a5ae02a3af97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c5ef7de-2c40-4b35-8219-39b188d90487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54de2c03-a3c5-46b0-b050-978d6b1b1bb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c5ef7de-2c40-4b35-8219-39b188d90487",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "5beb0cf1-f503-4fe6-b57b-dce1f3f3aeab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "c6fd03fd-da6b-4053-88e8-c02b223a4356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5beb0cf1-f503-4fe6-b57b-dce1f3f3aeab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "122b6471-00ad-4a23-99fd-1f7b250e4df6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5beb0cf1-f503-4fe6-b57b-dce1f3f3aeab",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "09d72a61-f3a8-4c5b-8a92-2a25c6a8e6fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "28134ea7-d38c-4922-8a12-d790ad3f5a62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09d72a61-f3a8-4c5b-8a92-2a25c6a8e6fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b15120f6-852e-4659-b253-cfdbf071e361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09d72a61-f3a8-4c5b-8a92-2a25c6a8e6fb",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "f78bc9b3-08ed-4474-b519-8227b054474a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "27da7045-e3ea-4333-a288-de3f3851915e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f78bc9b3-08ed-4474-b519-8227b054474a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31752271-34d0-4c18-998a-de4674f2d9e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f78bc9b3-08ed-4474-b519-8227b054474a",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "a3684dae-8e03-49aa-88c8-7cf7eb9b7192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "4dfe46c1-caa3-4e2c-968d-ead65e8410b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3684dae-8e03-49aa-88c8-7cf7eb9b7192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d85c941-dc76-4d70-8230-8f6218c7496e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3684dae-8e03-49aa-88c8-7cf7eb9b7192",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "f31afe3a-ba58-4f8f-b9d4-213f00ff8560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "31287bc6-af04-4b26-b66b-6b5921986384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f31afe3a-ba58-4f8f-b9d4-213f00ff8560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25ba8acc-7a04-4aa6-b81a-346eb9aa55a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f31afe3a-ba58-4f8f-b9d4-213f00ff8560",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "aff44fbf-eab0-4708-af2b-17d298563df9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "6919ee19-bf13-44af-83a7-5385f0a7665d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aff44fbf-eab0-4708-af2b-17d298563df9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4203458-4f8e-48ee-8173-a9129de815b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aff44fbf-eab0-4708-af2b-17d298563df9",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "855ab391-07d4-438b-a952-6ee624d4643f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "b73b007c-e425-4017-bfe2-12e60e5d3b4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "855ab391-07d4-438b-a952-6ee624d4643f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54555766-c1b4-424d-a87d-8240db3847b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "855ab391-07d4-438b-a952-6ee624d4643f",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "c82a8d96-cf4d-46dd-b1e1-759f445b6a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "7d7fc87a-0359-454e-bdce-e2808ea586dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c82a8d96-cf4d-46dd-b1e1-759f445b6a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cff717eb-348e-489f-9270-49eb4e67ffac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c82a8d96-cf4d-46dd-b1e1-759f445b6a12",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "8631c87c-8210-4e2e-a947-ccdae07a99ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "86fd5ef6-a09f-4e4e-a475-80baa749b87c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8631c87c-8210-4e2e-a947-ccdae07a99ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e02d5176-3680-4d72-b713-fae64764675d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8631c87c-8210-4e2e-a947-ccdae07a99ca",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        },
        {
            "id": "21316cf7-ceb9-45a7-b210-67c052a62dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "compositeImage": {
                "id": "39b45c4b-85c0-4f8e-88c3-fa4dd0f78a65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21316cf7-ceb9-45a7-b210-67c052a62dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e324983-636c-4bd9-9255-2f387177c162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21316cf7-ceb9-45a7-b210-67c052a62dfe",
                    "LayerId": "bcd26f24-2772-4594-9f0d-e77a7442a4f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "bcd26f24-2772-4594-9f0d-e77a7442a4f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8ea0e4a-651d-4091-a656-37b1facfafa8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 40
}