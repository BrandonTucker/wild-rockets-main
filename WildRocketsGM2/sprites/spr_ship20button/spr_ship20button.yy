{
    "id": "72d575c9-e41f-4e81-b4fc-8541811d0071",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship20button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "720a4e8f-11e4-4bf7-80f6-f24b3d4eda51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72d575c9-e41f-4e81-b4fc-8541811d0071",
            "compositeImage": {
                "id": "b6e10d63-3a64-4a72-9126-a5f704ba1031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "720a4e8f-11e4-4bf7-80f6-f24b3d4eda51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d5dcfde-10a8-4d09-9a07-50ab6cf6c559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "720a4e8f-11e4-4bf7-80f6-f24b3d4eda51",
                    "LayerId": "798187ef-65d7-4b06-bdf5-9fb7ee343256"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "798187ef-65d7-4b06-bdf5-9fb7ee343256",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72d575c9-e41f-4e81-b4fc-8541811d0071",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}