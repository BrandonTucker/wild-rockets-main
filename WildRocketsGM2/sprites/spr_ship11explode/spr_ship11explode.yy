{
    "id": "ce738372-3b5d-4518-ae40-7b724546e58d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship11explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 24,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b846624-7040-4ced-a593-93f61217495e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce738372-3b5d-4518-ae40-7b724546e58d",
            "compositeImage": {
                "id": "3ea88fe4-0880-4dd9-89c3-73b9bb300cb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b846624-7040-4ced-a593-93f61217495e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6baae14f-6328-447c-9fe3-e7c7f3e460a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b846624-7040-4ced-a593-93f61217495e",
                    "LayerId": "f7640269-7038-4f2f-8526-b46fca7d256b"
                }
            ]
        },
        {
            "id": "ede07437-2dbe-4077-a921-51cbd0b5b33a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce738372-3b5d-4518-ae40-7b724546e58d",
            "compositeImage": {
                "id": "a3cfa538-37de-4698-ad74-2f956c86f991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ede07437-2dbe-4077-a921-51cbd0b5b33a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82358456-e42f-4913-9fc7-1a9fcdce8800",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ede07437-2dbe-4077-a921-51cbd0b5b33a",
                    "LayerId": "f7640269-7038-4f2f-8526-b46fca7d256b"
                }
            ]
        },
        {
            "id": "cd5b3e49-25cd-479a-bb41-5e802169bbb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce738372-3b5d-4518-ae40-7b724546e58d",
            "compositeImage": {
                "id": "f74e2f32-2aff-4160-9d5d-c40b60511595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd5b3e49-25cd-479a-bb41-5e802169bbb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f36fcc01-9f66-436b-a368-be5e2996ff0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd5b3e49-25cd-479a-bb41-5e802169bbb1",
                    "LayerId": "f7640269-7038-4f2f-8526-b46fca7d256b"
                }
            ]
        },
        {
            "id": "057e2d5c-6ac2-4bb5-b0b2-c86cc478ded1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce738372-3b5d-4518-ae40-7b724546e58d",
            "compositeImage": {
                "id": "95997754-8112-444c-883e-f54e650bcc21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "057e2d5c-6ac2-4bb5-b0b2-c86cc478ded1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0ced659-0ccf-4751-8638-16ad2aa5cc15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "057e2d5c-6ac2-4bb5-b0b2-c86cc478ded1",
                    "LayerId": "f7640269-7038-4f2f-8526-b46fca7d256b"
                }
            ]
        },
        {
            "id": "c9633357-4323-4d12-a2be-db8b64dbaffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce738372-3b5d-4518-ae40-7b724546e58d",
            "compositeImage": {
                "id": "1743ebe4-cf81-4841-a374-ac0e04ff82f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9633357-4323-4d12-a2be-db8b64dbaffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a798cd72-d644-48bf-9685-7df6fe3af99b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9633357-4323-4d12-a2be-db8b64dbaffe",
                    "LayerId": "f7640269-7038-4f2f-8526-b46fca7d256b"
                }
            ]
        },
        {
            "id": "52c64090-feb4-4438-a0b6-94134cc891ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce738372-3b5d-4518-ae40-7b724546e58d",
            "compositeImage": {
                "id": "c25a3d5a-93a4-4dda-a72a-fb4bc1c5ff24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52c64090-feb4-4438-a0b6-94134cc891ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da649c0f-ad35-42d1-8522-9d50cdd20efc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52c64090-feb4-4438-a0b6-94134cc891ea",
                    "LayerId": "f7640269-7038-4f2f-8526-b46fca7d256b"
                }
            ]
        },
        {
            "id": "d7a84825-79bb-4758-bf29-7d8e308419c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce738372-3b5d-4518-ae40-7b724546e58d",
            "compositeImage": {
                "id": "aec079b7-2854-4e66-8e81-d93d782089e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a84825-79bb-4758-bf29-7d8e308419c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1313f084-2d7f-4648-b85f-53d551b077e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a84825-79bb-4758-bf29-7d8e308419c1",
                    "LayerId": "f7640269-7038-4f2f-8526-b46fca7d256b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "f7640269-7038-4f2f-8526-b46fca7d256b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce738372-3b5d-4518-ae40-7b724546e58d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 42
}