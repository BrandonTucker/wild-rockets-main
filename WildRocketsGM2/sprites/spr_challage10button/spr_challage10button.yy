{
    "id": "22273fa2-cd66-4357-a5da-e8c10b53df50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage10button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0afbe049-b034-4b10-9b19-38bf597650f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22273fa2-cd66-4357-a5da-e8c10b53df50",
            "compositeImage": {
                "id": "a4c519ae-ff97-4ab5-a8ed-77130e11e560",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0afbe049-b034-4b10-9b19-38bf597650f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eb7d10c-356f-4dc7-94a4-6c8a3850f89b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0afbe049-b034-4b10-9b19-38bf597650f9",
                    "LayerId": "1b88c150-5dd8-4c6a-86b6-4d8544b0b608"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1b88c150-5dd8-4c6a-86b6-4d8544b0b608",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22273fa2-cd66-4357-a5da-e8c10b53df50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}