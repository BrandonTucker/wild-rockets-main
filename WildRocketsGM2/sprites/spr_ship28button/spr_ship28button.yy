{
    "id": "eb5046a0-1ba5-4939-872a-a4452528107d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship28button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "650353bb-afc7-49d7-a5b2-6ce5ac1af66e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb5046a0-1ba5-4939-872a-a4452528107d",
            "compositeImage": {
                "id": "3e164258-cfcf-4b8b-bdd1-a6d8b94b3ec7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "650353bb-afc7-49d7-a5b2-6ce5ac1af66e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39a487b1-c85b-4fe1-9d29-834bb6d7e6a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "650353bb-afc7-49d7-a5b2-6ce5ac1af66e",
                    "LayerId": "29f2f922-5cc2-4087-870b-5a5f780dd5d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "29f2f922-5cc2-4087-870b-5a5f780dd5d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb5046a0-1ba5-4939-872a-a4452528107d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}