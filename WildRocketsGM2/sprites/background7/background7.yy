{
    "id": "e4ab1cf1-3a42-478e-9ef6-51f0be298c5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background7",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 959,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "940f00f0-dcea-45ab-8398-16c7439db120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4ab1cf1-3a42-478e-9ef6-51f0be298c5c",
            "compositeImage": {
                "id": "2f4fefee-ccee-4a5e-8d99-62050543e4c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "940f00f0-dcea-45ab-8398-16c7439db120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a4f2a84-4080-4ebf-8894-5bb509c0fa96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940f00f0-dcea-45ab-8398-16c7439db120",
                    "LayerId": "fbe17e2d-99af-46ee-baa1-91ff993c28d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "fbe17e2d-99af-46ee-baa1-91ff993c28d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4ab1cf1-3a42-478e-9ef6-51f0be298c5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}