{
    "id": "e4a583a8-f53f-4f79-bdd3-84f765233174",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage4buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 1,
    "bbox_right": 108,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a114811a-7d05-4255-af62-b621d5e079c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "compositeImage": {
                "id": "4f5f003c-d846-4912-b49c-dfe1dd2d9a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a114811a-7d05-4255-af62-b621d5e079c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a30f460-b13b-4ed3-b4c6-018796328bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a114811a-7d05-4255-af62-b621d5e079c1",
                    "LayerId": "7e925f55-5ef9-45c2-a533-884b96e158d4"
                }
            ]
        },
        {
            "id": "b5a4c5b3-54e6-45ad-8b6a-5bbc0fcb3e9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "compositeImage": {
                "id": "75eed3f7-4291-4c80-aee3-a2eba9cf6036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5a4c5b3-54e6-45ad-8b6a-5bbc0fcb3e9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80c3a1b6-ddf0-41b3-b4a2-1cad6936a399",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5a4c5b3-54e6-45ad-8b6a-5bbc0fcb3e9a",
                    "LayerId": "7e925f55-5ef9-45c2-a533-884b96e158d4"
                }
            ]
        },
        {
            "id": "6c6dacba-d8cb-4e7e-aa5b-e8873f8e8de1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "compositeImage": {
                "id": "c33a8838-3423-4d3f-a57b-0b2841d035c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c6dacba-d8cb-4e7e-aa5b-e8873f8e8de1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d104847-d3fc-44ed-ac6d-4957fbecc241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c6dacba-d8cb-4e7e-aa5b-e8873f8e8de1",
                    "LayerId": "7e925f55-5ef9-45c2-a533-884b96e158d4"
                }
            ]
        },
        {
            "id": "c6d87f85-a82c-4229-bcd0-4ad8d4b6de0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "compositeImage": {
                "id": "cd6d93fe-617b-4d0c-870c-d3801b9dd870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d87f85-a82c-4229-bcd0-4ad8d4b6de0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b35ad07e-8ac0-43bb-af6b-933ef649e1ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d87f85-a82c-4229-bcd0-4ad8d4b6de0b",
                    "LayerId": "7e925f55-5ef9-45c2-a533-884b96e158d4"
                }
            ]
        },
        {
            "id": "5b97ed4c-e167-469b-9dcb-b4cfafe79478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "compositeImage": {
                "id": "39086e4b-0274-4b70-aaea-58e18e2197e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b97ed4c-e167-469b-9dcb-b4cfafe79478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfce6485-5d56-4286-9512-e459e3376da2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b97ed4c-e167-469b-9dcb-b4cfafe79478",
                    "LayerId": "7e925f55-5ef9-45c2-a533-884b96e158d4"
                }
            ]
        },
        {
            "id": "a7698661-0912-4a2d-890f-32259ba4d305",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "compositeImage": {
                "id": "e6cd7d9d-0fb2-4c81-8908-f84fe9fb4df8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7698661-0912-4a2d-890f-32259ba4d305",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79f4b9fc-66fd-41e8-8589-f45efa98cb42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7698661-0912-4a2d-890f-32259ba4d305",
                    "LayerId": "7e925f55-5ef9-45c2-a533-884b96e158d4"
                }
            ]
        },
        {
            "id": "164c3c05-527f-4410-ba7d-0cfd9f604f4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "compositeImage": {
                "id": "446c8da8-eebd-4cba-9106-aaa296bf18a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164c3c05-527f-4410-ba7d-0cfd9f604f4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86d0b62b-6f5a-4503-8f52-c5b8229ce044",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164c3c05-527f-4410-ba7d-0cfd9f604f4b",
                    "LayerId": "7e925f55-5ef9-45c2-a533-884b96e158d4"
                }
            ]
        },
        {
            "id": "32fe4733-6ed3-4166-8032-f75dd22001ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "compositeImage": {
                "id": "1b88ddee-d4ae-4a97-ba86-e3e7a6e3b61a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32fe4733-6ed3-4166-8032-f75dd22001ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a821c7d-132e-420e-9142-db95d09b6f05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32fe4733-6ed3-4166-8032-f75dd22001ff",
                    "LayerId": "7e925f55-5ef9-45c2-a533-884b96e158d4"
                }
            ]
        },
        {
            "id": "9dcea237-113d-4908-a523-16c311c46ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "compositeImage": {
                "id": "55dfc563-e04c-41f5-8b89-89fb9b491b66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dcea237-113d-4908-a523-16c311c46ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0fb7254-538e-4529-a00b-cc6ac53d0ae4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dcea237-113d-4908-a523-16c311c46ff7",
                    "LayerId": "7e925f55-5ef9-45c2-a533-884b96e158d4"
                }
            ]
        },
        {
            "id": "084e8f5a-07c3-4ca0-820b-43901a2a9394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "compositeImage": {
                "id": "f7d17be3-138f-4888-9cc3-65fa21650a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "084e8f5a-07c3-4ca0-820b-43901a2a9394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3413444-b88b-4815-84d6-0c23d6a2e2ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "084e8f5a-07c3-4ca0-820b-43901a2a9394",
                    "LayerId": "7e925f55-5ef9-45c2-a533-884b96e158d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "7e925f55-5ef9-45c2-a533-884b96e158d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4a583a8-f53f-4f79-bdd3-84f765233174",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 50,
    "yorig": 48
}