{
    "id": "818e3f0c-8b93-4457-8d54-90c7019be058",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship39",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 144,
    "bbox_left": 49,
    "bbox_right": 74,
    "bbox_top": 52,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1cbc7cf-4874-4ed4-8db4-745385732f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "818e3f0c-8b93-4457-8d54-90c7019be058",
            "compositeImage": {
                "id": "fd822e67-5242-483b-a6b3-bef506710b87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1cbc7cf-4874-4ed4-8db4-745385732f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b867e70a-f98e-489b-99e7-2b2ad4a29f36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1cbc7cf-4874-4ed4-8db4-745385732f43",
                    "LayerId": "482f784c-919e-4bd3-a395-c948f37d1900"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "482f784c-919e-4bd3-a395-c948f37d1900",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "818e3f0c-8b93-4457-8d54-90c7019be058",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 62,
    "yorig": 78
}