{
    "id": "84dca0ca-0709-4a23-8b69-d031e59a7465",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship34button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "749aefcf-61ac-4184-83b5-efc840e7d2c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84dca0ca-0709-4a23-8b69-d031e59a7465",
            "compositeImage": {
                "id": "70860c98-e872-41e1-ae68-3041c9caf993",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749aefcf-61ac-4184-83b5-efc840e7d2c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98b9a81d-4a53-4f2e-ad8d-b41c200816b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749aefcf-61ac-4184-83b5-efc840e7d2c9",
                    "LayerId": "76843b0e-32f1-45f4-96a4-075bb7719e75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "76843b0e-32f1-45f4-96a4-075bb7719e75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84dca0ca-0709-4a23-8b69-d031e59a7465",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}