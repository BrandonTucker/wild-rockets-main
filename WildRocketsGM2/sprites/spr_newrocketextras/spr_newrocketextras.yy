{
    "id": "75980cfb-148b-45a9-9ab4-9bc0dd133b2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_newrocketextras",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 0,
    "bbox_right": 87,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56e3abf8-7115-44c1-a2c1-e773cfff683a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75980cfb-148b-45a9-9ab4-9bc0dd133b2b",
            "compositeImage": {
                "id": "3c8635c1-892c-4864-a67c-2656c071b085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e3abf8-7115-44c1-a2c1-e773cfff683a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c513cd5-15e3-46dc-a7ff-2a389ffcb66b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e3abf8-7115-44c1-a2c1-e773cfff683a",
                    "LayerId": "ce4a6c01-6f3f-4531-9d97-b2c0982a4f32"
                }
            ]
        },
        {
            "id": "5766f90b-346f-47ea-83f8-a11a62f6482d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75980cfb-148b-45a9-9ab4-9bc0dd133b2b",
            "compositeImage": {
                "id": "5d808714-36df-4c25-a7cb-f6f5dc725d82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5766f90b-346f-47ea-83f8-a11a62f6482d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bcb8435-5db6-400c-9c4c-2f5daabb9c19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5766f90b-346f-47ea-83f8-a11a62f6482d",
                    "LayerId": "ce4a6c01-6f3f-4531-9d97-b2c0982a4f32"
                }
            ]
        },
        {
            "id": "fce9dc18-065c-4fb7-8b58-41e0cc516315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75980cfb-148b-45a9-9ab4-9bc0dd133b2b",
            "compositeImage": {
                "id": "f303ad37-7a18-4a45-a001-de9283e7bfa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fce9dc18-065c-4fb7-8b58-41e0cc516315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49e5eb10-9e08-453c-81b4-9ad045acc017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fce9dc18-065c-4fb7-8b58-41e0cc516315",
                    "LayerId": "ce4a6c01-6f3f-4531-9d97-b2c0982a4f32"
                }
            ]
        },
        {
            "id": "00f0896f-2eb4-4206-b6e9-fdfb59ee8529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75980cfb-148b-45a9-9ab4-9bc0dd133b2b",
            "compositeImage": {
                "id": "42847621-f40e-45be-a8b2-599b644f4938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00f0896f-2eb4-4206-b6e9-fdfb59ee8529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90604030-1261-4c81-a78e-bf38f15d79ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00f0896f-2eb4-4206-b6e9-fdfb59ee8529",
                    "LayerId": "ce4a6c01-6f3f-4531-9d97-b2c0982a4f32"
                }
            ]
        },
        {
            "id": "57df7f99-76e6-44c8-bad0-e738fed198f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75980cfb-148b-45a9-9ab4-9bc0dd133b2b",
            "compositeImage": {
                "id": "5980e927-006f-4e03-ad9a-a41c0d9f550d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57df7f99-76e6-44c8-bad0-e738fed198f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfcc771b-fc01-4bf1-91c6-c126c8cd337f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57df7f99-76e6-44c8-bad0-e738fed198f4",
                    "LayerId": "ce4a6c01-6f3f-4531-9d97-b2c0982a4f32"
                }
            ]
        },
        {
            "id": "bc97f7bc-4353-4f60-9cd4-fd85dbe8a7e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75980cfb-148b-45a9-9ab4-9bc0dd133b2b",
            "compositeImage": {
                "id": "c1d5a401-6e26-4b9d-bb13-8fc1ebbb5fa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc97f7bc-4353-4f60-9cd4-fd85dbe8a7e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70f4b16b-43d0-446f-825e-a2064014f670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc97f7bc-4353-4f60-9cd4-fd85dbe8a7e5",
                    "LayerId": "ce4a6c01-6f3f-4531-9d97-b2c0982a4f32"
                }
            ]
        },
        {
            "id": "d926a07d-6937-432f-83c2-7e5a10a6d551",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75980cfb-148b-45a9-9ab4-9bc0dd133b2b",
            "compositeImage": {
                "id": "902af9be-20b4-4763-8388-547327e52727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d926a07d-6937-432f-83c2-7e5a10a6d551",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6034dfb5-5a6e-4ad1-86d3-27b2e409dd6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d926a07d-6937-432f-83c2-7e5a10a6d551",
                    "LayerId": "ce4a6c01-6f3f-4531-9d97-b2c0982a4f32"
                }
            ]
        },
        {
            "id": "f5071d34-e577-4cb1-b5bd-c8ce38251a42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75980cfb-148b-45a9-9ab4-9bc0dd133b2b",
            "compositeImage": {
                "id": "d60d0767-b417-41d0-a74d-cc501bee1773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5071d34-e577-4cb1-b5bd-c8ce38251a42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86753128-42f3-4926-bbbf-0897bcc6d86d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5071d34-e577-4cb1-b5bd-c8ce38251a42",
                    "LayerId": "ce4a6c01-6f3f-4531-9d97-b2c0982a4f32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "ce4a6c01-6f3f-4531-9d97-b2c0982a4f32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75980cfb-148b-45a9-9ab4-9bc0dd133b2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 0,
    "yorig": 0
}