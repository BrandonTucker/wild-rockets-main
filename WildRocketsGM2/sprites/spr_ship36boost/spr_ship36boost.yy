{
    "id": "840e9b51-c174-4fbc-ae06-f840290ea402",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship36boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 185,
    "bbox_left": 49,
    "bbox_right": 74,
    "bbox_top": 93,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a82ef90c-d456-4a3e-ae6d-b6bf77a7bc30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "840e9b51-c174-4fbc-ae06-f840290ea402",
            "compositeImage": {
                "id": "d066c45a-d7d7-4c9d-b3b2-9c209fdc8bc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a82ef90c-d456-4a3e-ae6d-b6bf77a7bc30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beeeb6de-85f5-43ea-9984-2c7b59ff3f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a82ef90c-d456-4a3e-ae6d-b6bf77a7bc30",
                    "LayerId": "10c16e32-1f85-47c9-9511-36b9b8ab0d65"
                }
            ]
        },
        {
            "id": "34151d16-3c2e-42e8-9441-251427d9e7ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "840e9b51-c174-4fbc-ae06-f840290ea402",
            "compositeImage": {
                "id": "1ba4da45-a713-4b47-86d3-c5c2ea44ae79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34151d16-3c2e-42e8-9441-251427d9e7ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ee350c9-25ab-4b99-9ff9-ee2475d741e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34151d16-3c2e-42e8-9441-251427d9e7ff",
                    "LayerId": "10c16e32-1f85-47c9-9511-36b9b8ab0d65"
                }
            ]
        },
        {
            "id": "2c4d8592-74ba-4e1b-bda4-00298c18b80c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "840e9b51-c174-4fbc-ae06-f840290ea402",
            "compositeImage": {
                "id": "dc072368-cc66-426c-9280-2afcb8cfea45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c4d8592-74ba-4e1b-bda4-00298c18b80c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788a143a-b3f5-451a-acb0-8018156ec65d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c4d8592-74ba-4e1b-bda4-00298c18b80c",
                    "LayerId": "10c16e32-1f85-47c9-9511-36b9b8ab0d65"
                }
            ]
        },
        {
            "id": "33a95b14-efb6-4715-9f0d-4271f528ddfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "840e9b51-c174-4fbc-ae06-f840290ea402",
            "compositeImage": {
                "id": "9521a181-d677-4962-b4de-7641a4b96173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33a95b14-efb6-4715-9f0d-4271f528ddfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a3e3175-d355-4da8-bddc-f06a5a14fa02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33a95b14-efb6-4715-9f0d-4271f528ddfc",
                    "LayerId": "10c16e32-1f85-47c9-9511-36b9b8ab0d65"
                }
            ]
        },
        {
            "id": "6f82d66c-407f-4e9d-8135-a3ce89fa761e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "840e9b51-c174-4fbc-ae06-f840290ea402",
            "compositeImage": {
                "id": "549899a8-841f-44b8-ad13-db78c7224429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f82d66c-407f-4e9d-8135-a3ce89fa761e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e82fd47a-d233-4f86-b87a-aee9d8c58c0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f82d66c-407f-4e9d-8135-a3ce89fa761e",
                    "LayerId": "10c16e32-1f85-47c9-9511-36b9b8ab0d65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 266,
    "layers": [
        {
            "id": "10c16e32-1f85-47c9-9511-36b9b8ab0d65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "840e9b51-c174-4fbc-ae06-f840290ea402",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 61,
    "yorig": 137
}