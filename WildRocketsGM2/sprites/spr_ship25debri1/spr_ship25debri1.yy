{
    "id": "856a49b3-77e7-42de-9360-6ee221830dde",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship25debri1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63bd5c9b-bceb-4dea-93e3-35d039b14ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "compositeImage": {
                "id": "3e0f02b4-d8da-40c7-a2e2-7f71b2813bfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63bd5c9b-bceb-4dea-93e3-35d039b14ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b3ee0dd-b8e0-4e5b-96b3-f3d5a0f1d5e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63bd5c9b-bceb-4dea-93e3-35d039b14ecf",
                    "LayerId": "6d943904-b20b-43d8-a951-1ac5392f2e74"
                }
            ]
        },
        {
            "id": "c0a95493-7a4f-4a4c-bbad-aa803bc2921b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "compositeImage": {
                "id": "8d2c71e2-c998-4367-b73a-96be2c5c9166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0a95493-7a4f-4a4c-bbad-aa803bc2921b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52eaac35-d82c-4cf6-a006-99125af06514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a95493-7a4f-4a4c-bbad-aa803bc2921b",
                    "LayerId": "6d943904-b20b-43d8-a951-1ac5392f2e74"
                }
            ]
        },
        {
            "id": "8c771348-c3d0-4730-a41c-44b5d7e634bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "compositeImage": {
                "id": "b9e61b06-9119-4054-ba89-d8196f84e497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c771348-c3d0-4730-a41c-44b5d7e634bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bb187bc-169d-442a-a09d-5d3873bdf20a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c771348-c3d0-4730-a41c-44b5d7e634bf",
                    "LayerId": "6d943904-b20b-43d8-a951-1ac5392f2e74"
                }
            ]
        },
        {
            "id": "1c4b596d-31ea-4241-94a2-88e641b5a067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "compositeImage": {
                "id": "abc4e95e-ae72-4294-8126-c17229600946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c4b596d-31ea-4241-94a2-88e641b5a067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d710a3a-ebfb-483c-9447-166f94f9df59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c4b596d-31ea-4241-94a2-88e641b5a067",
                    "LayerId": "6d943904-b20b-43d8-a951-1ac5392f2e74"
                }
            ]
        },
        {
            "id": "ae96e4cc-7ea2-4efe-856a-87448d256c98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "compositeImage": {
                "id": "c2e58d35-a565-4bec-bdb3-0f7b277a1203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae96e4cc-7ea2-4efe-856a-87448d256c98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cd9c75b-a8d0-4116-afec-8dd7b69d5e4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae96e4cc-7ea2-4efe-856a-87448d256c98",
                    "LayerId": "6d943904-b20b-43d8-a951-1ac5392f2e74"
                }
            ]
        },
        {
            "id": "f64b36aa-791c-4c9d-a7d1-d7b581304c0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "compositeImage": {
                "id": "f9e9a8ad-e875-4d22-b0f5-c423373ff2d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f64b36aa-791c-4c9d-a7d1-d7b581304c0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e7f309f-73ca-4f7c-9746-ae09ae8ab2ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f64b36aa-791c-4c9d-a7d1-d7b581304c0b",
                    "LayerId": "6d943904-b20b-43d8-a951-1ac5392f2e74"
                }
            ]
        },
        {
            "id": "035cc15f-d589-4c75-b44a-01ba3b45491e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "compositeImage": {
                "id": "437d2294-220e-41a6-a0ed-0576f017f107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "035cc15f-d589-4c75-b44a-01ba3b45491e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fead4833-08d3-4bee-9f95-fe7ddfad501c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "035cc15f-d589-4c75-b44a-01ba3b45491e",
                    "LayerId": "6d943904-b20b-43d8-a951-1ac5392f2e74"
                }
            ]
        },
        {
            "id": "9326acd0-f54e-4b6e-abc0-04bb0f9295df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "compositeImage": {
                "id": "f30c261c-f550-4b7a-a17d-f221fb1a9172",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9326acd0-f54e-4b6e-abc0-04bb0f9295df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab7c080e-d38f-4cc4-a6b4-fad672a8d456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9326acd0-f54e-4b6e-abc0-04bb0f9295df",
                    "LayerId": "6d943904-b20b-43d8-a951-1ac5392f2e74"
                }
            ]
        },
        {
            "id": "a5410b6e-7720-4a9e-bc19-aa9fed383ba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "compositeImage": {
                "id": "f0b02a07-de40-4131-b9cf-f4147a16646c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5410b6e-7720-4a9e-bc19-aa9fed383ba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cedb51f-84b1-4b02-8b82-481a7d86c6b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5410b6e-7720-4a9e-bc19-aa9fed383ba3",
                    "LayerId": "6d943904-b20b-43d8-a951-1ac5392f2e74"
                }
            ]
        },
        {
            "id": "b993ec11-ce45-42e4-863c-d4f1d330abcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "compositeImage": {
                "id": "4f872f58-161f-4204-b155-dc30ee26e495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b993ec11-ce45-42e4-863c-d4f1d330abcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aa8dfb7-c1ed-4e7f-b9d8-c7b900a516fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b993ec11-ce45-42e4-863c-d4f1d330abcc",
                    "LayerId": "6d943904-b20b-43d8-a951-1ac5392f2e74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "6d943904-b20b-43d8-a951-1ac5392f2e74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "856a49b3-77e7-42de-9360-6ee221830dde",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 19,
    "yorig": 18
}