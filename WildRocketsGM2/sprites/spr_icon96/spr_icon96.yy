{
    "id": "f4c661f1-79c1-43d2-a54b-3d25e4b4198c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon96",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 9,
    "bbox_right": 86,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "452bacd1-87d3-4916-8b96-a7ad82406962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c661f1-79c1-43d2-a54b-3d25e4b4198c",
            "compositeImage": {
                "id": "69681b53-4ad1-47a9-b26b-a0f23c37b3b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "452bacd1-87d3-4916-8b96-a7ad82406962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d046a02-c839-4943-aaec-38d6544c90cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "452bacd1-87d3-4916-8b96-a7ad82406962",
                    "LayerId": "89b08420-35bd-4149-a515-6c8ece6b516f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "89b08420-35bd-4149-a515-6c8ece6b516f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4c661f1-79c1-43d2-a54b-3d25e4b4198c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}