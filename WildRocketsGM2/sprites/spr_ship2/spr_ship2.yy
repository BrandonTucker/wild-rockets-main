{
    "id": "9a0cf401-54a7-4811-a762-d09d6291794b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 27,
    "bbox_right": 52,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "480bb3fa-e142-41cb-8e05-22230970f85b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a0cf401-54a7-4811-a762-d09d6291794b",
            "compositeImage": {
                "id": "cfed5d62-ff97-457d-8ba8-5870b1403861",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "480bb3fa-e142-41cb-8e05-22230970f85b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8a933d7-569f-43f6-8c5e-ec5c856e7f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "480bb3fa-e142-41cb-8e05-22230970f85b",
                    "LayerId": "64822b16-d02d-4dcd-ad83-ced0b861706c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "64822b16-d02d-4dcd-ad83-ced0b861706c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a0cf401-54a7-4811-a762-d09d6291794b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 39,
    "yorig": 41
}