{
    "id": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship26debri1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 68,
    "bbox_left": 0,
    "bbox_right": 68,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73b51130-50b2-46c9-8ec7-3391b5006ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "5c476b51-cf18-4477-95b9-4da3ab2fce66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73b51130-50b2-46c9-8ec7-3391b5006ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee3b6ec-e622-4689-803b-80d4b91ba0b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73b51130-50b2-46c9-8ec7-3391b5006ca9",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "bfecc3b8-2c17-4a2e-94b9-ec2144e500a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "c478d65c-40a5-4a49-85e7-e3e1a06acf52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfecc3b8-2c17-4a2e-94b9-ec2144e500a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a2baab5-12be-4064-ad75-c14559745f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfecc3b8-2c17-4a2e-94b9-ec2144e500a8",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "8076ea6b-4409-4ebb-88b5-74d7507e213b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "1d090b18-4022-4e4d-8d4e-8bb4e695229b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8076ea6b-4409-4ebb-88b5-74d7507e213b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2265c6e5-9147-47ca-a046-73981b997b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8076ea6b-4409-4ebb-88b5-74d7507e213b",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "892dd181-4ddd-4c67-a3f5-584982a339c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "55c9baf8-f5b9-45e8-8296-9faf8f878cfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "892dd181-4ddd-4c67-a3f5-584982a339c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36234780-d49f-406e-b8a5-860094ba9f38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "892dd181-4ddd-4c67-a3f5-584982a339c9",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "98172559-f6b3-4ecf-ae7f-15209e697eda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "6f9ecf20-8fc8-450a-862a-38c89c25ab6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98172559-f6b3-4ecf-ae7f-15209e697eda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56bb26c9-5a68-4995-9631-41d895507ca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98172559-f6b3-4ecf-ae7f-15209e697eda",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "0302ff21-4c0e-4afa-9fc4-63ca4a5ccb89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "ee0bdc68-e458-4011-9992-6b27d035ed90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0302ff21-4c0e-4afa-9fc4-63ca4a5ccb89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35a5e0c9-560a-4c5a-893c-1c2f8e333724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0302ff21-4c0e-4afa-9fc4-63ca4a5ccb89",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "b2b1fc1c-2174-4c9e-a03b-562e2ee36366",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "d916ca91-a67a-426d-8e98-4364bcf7988d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2b1fc1c-2174-4c9e-a03b-562e2ee36366",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b58e6625-f54b-4d7c-b920-c66f2540976d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2b1fc1c-2174-4c9e-a03b-562e2ee36366",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "d4c8c4f6-5b0d-4ceb-848c-c06e43a0d990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "f56c8c7b-c940-4705-9756-413b9f6d4910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4c8c4f6-5b0d-4ceb-848c-c06e43a0d990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d4495ad-166a-45cb-9744-cc09717bb88e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4c8c4f6-5b0d-4ceb-848c-c06e43a0d990",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "73097c54-32f0-46f3-abb3-0e121d9507af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "a82cc5ab-5e3a-4b6b-9081-9e65b097b5e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73097c54-32f0-46f3-abb3-0e121d9507af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c14a1c13-43ac-43eb-ab76-3adf59f280d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73097c54-32f0-46f3-abb3-0e121d9507af",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "76e18582-c2b9-4c5a-a3d6-5c0e0b084ddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "f3c08834-594b-4db6-9349-e2659d356746",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e18582-c2b9-4c5a-a3d6-5c0e0b084ddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16cb6781-d4ff-48bb-85be-1dc331a5b955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e18582-c2b9-4c5a-a3d6-5c0e0b084ddb",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "123d28a6-ed1c-4481-9045-6321ca0ad4b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "c9c69970-5e0d-4981-a99c-103698e3f21f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "123d28a6-ed1c-4481-9045-6321ca0ad4b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e997d16a-d547-4657-8ddd-dca70dd49688",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "123d28a6-ed1c-4481-9045-6321ca0ad4b3",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "2cc0dac0-6f3d-43a9-86b9-87cd2afa2a89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "2903994e-d1c1-45b9-8a2b-f0fabffcdca5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cc0dac0-6f3d-43a9-86b9-87cd2afa2a89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf10409e-35a2-414f-a5ab-281d084be80f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cc0dac0-6f3d-43a9-86b9-87cd2afa2a89",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "d0c4eb42-87fa-4eca-819b-5ec4ccbe7408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "2d1ec74d-5878-4693-a75e-4ad9bbc97ba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c4eb42-87fa-4eca-819b-5ec4ccbe7408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a14e487-3281-4267-bda3-5b218d3b66c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c4eb42-87fa-4eca-819b-5ec4ccbe7408",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "3a76c0df-a6ce-4e01-a4f3-e4059709b535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "b86984e3-35c4-46db-b22c-b060aae2c1d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a76c0df-a6ce-4e01-a4f3-e4059709b535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e6c2de-dd9c-4c7e-bc9f-8a207b7b4a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a76c0df-a6ce-4e01-a4f3-e4059709b535",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        },
        {
            "id": "9d21dbbd-5d54-4e00-95b3-e43f766869f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "compositeImage": {
                "id": "60a77b5e-9caf-457c-a02e-3c28759ccb90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d21dbbd-5d54-4e00-95b3-e43f766869f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "426e23b1-afe2-46fc-bc50-83a949e4b04b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d21dbbd-5d54-4e00-95b3-e43f766869f8",
                    "LayerId": "7ddddf63-8eb9-43ac-878a-c2f938d093c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 69,
    "layers": [
        {
            "id": "7ddddf63-8eb9-43ac-878a-c2f938d093c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f087d52-b8ce-4eee-aaa2-dbde7e3915a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 69,
    "xorig": 35,
    "yorig": 31
}