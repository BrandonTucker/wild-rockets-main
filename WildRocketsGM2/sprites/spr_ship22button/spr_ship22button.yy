{
    "id": "fe5c36db-0cca-4675-9817-8b31601c246e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship22button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f2c8da0-8430-4775-90c0-71409396af1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5c36db-0cca-4675-9817-8b31601c246e",
            "compositeImage": {
                "id": "e326c83d-cf49-4620-93a6-53b96f036624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f2c8da0-8430-4775-90c0-71409396af1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e8f4260-58b2-4ed8-a511-207077b68f94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f2c8da0-8430-4775-90c0-71409396af1c",
                    "LayerId": "2733a149-0aac-4754-8f4f-ebdfc4bede45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "2733a149-0aac-4754-8f4f-ebdfc4bede45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe5c36db-0cca-4675-9817-8b31601c246e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}