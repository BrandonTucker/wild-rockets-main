{
    "id": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange39buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0117240d-d389-4c8c-906d-fdea2658d7c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "compositeImage": {
                "id": "6ad46be7-d61c-4c7b-b9f3-a7ca636e3e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0117240d-d389-4c8c-906d-fdea2658d7c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "469c54ac-8e7a-4210-a457-88bc2c209ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0117240d-d389-4c8c-906d-fdea2658d7c7",
                    "LayerId": "827e3b03-7a55-4e22-b59c-573a51ede70b"
                }
            ]
        },
        {
            "id": "b6b69920-2c84-454e-819d-35db7744f1db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "compositeImage": {
                "id": "ebaaaf97-882f-4966-ae3c-a765d8ebb131",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6b69920-2c84-454e-819d-35db7744f1db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd90860e-b7df-423f-894c-8b18c13bb31b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6b69920-2c84-454e-819d-35db7744f1db",
                    "LayerId": "827e3b03-7a55-4e22-b59c-573a51ede70b"
                }
            ]
        },
        {
            "id": "84a6f47b-6127-4b25-a532-d231bbceb31f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "compositeImage": {
                "id": "812e988c-d5fa-4c94-8638-71f50ceb6d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84a6f47b-6127-4b25-a532-d231bbceb31f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8612a56-2dce-4452-8722-9c222034d259",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84a6f47b-6127-4b25-a532-d231bbceb31f",
                    "LayerId": "827e3b03-7a55-4e22-b59c-573a51ede70b"
                }
            ]
        },
        {
            "id": "65119856-45b7-4a54-b029-f1fcefcf45c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "compositeImage": {
                "id": "2363d7eb-b747-48f8-8505-63014860e9ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65119856-45b7-4a54-b029-f1fcefcf45c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eef401d-24c4-4e5a-90c0-2c07c1941952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65119856-45b7-4a54-b029-f1fcefcf45c3",
                    "LayerId": "827e3b03-7a55-4e22-b59c-573a51ede70b"
                }
            ]
        },
        {
            "id": "a8d7b3b8-70f9-49dd-be90-bec8ec802e57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "compositeImage": {
                "id": "14174c8e-9650-4405-892b-28836840a9f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8d7b3b8-70f9-49dd-be90-bec8ec802e57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32fd8fe5-f7ea-471e-903f-da0cdf7b3cdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8d7b3b8-70f9-49dd-be90-bec8ec802e57",
                    "LayerId": "827e3b03-7a55-4e22-b59c-573a51ede70b"
                }
            ]
        },
        {
            "id": "492cea1c-d625-406f-80a2-bb7d9308f79c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "compositeImage": {
                "id": "989098a9-e98f-4ae5-886f-e1a13887143e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "492cea1c-d625-406f-80a2-bb7d9308f79c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3a000b3-6f7f-4647-9605-ebde585b78fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "492cea1c-d625-406f-80a2-bb7d9308f79c",
                    "LayerId": "827e3b03-7a55-4e22-b59c-573a51ede70b"
                }
            ]
        },
        {
            "id": "b879c64e-c07e-4655-b6df-fd5e7879b89d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "compositeImage": {
                "id": "28883d2c-9bf4-45d6-8a35-899f8a16376d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b879c64e-c07e-4655-b6df-fd5e7879b89d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4284241b-1b2a-4823-8e28-a3fe1e1666fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b879c64e-c07e-4655-b6df-fd5e7879b89d",
                    "LayerId": "827e3b03-7a55-4e22-b59c-573a51ede70b"
                }
            ]
        },
        {
            "id": "4e74eb4f-d335-47b8-be98-d7e4dcd9686d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "compositeImage": {
                "id": "521b21b1-10e0-48f6-afac-a3ca89dfe5d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e74eb4f-d335-47b8-be98-d7e4dcd9686d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a3ce5d8-462f-482b-b8de-dee66d66a340",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e74eb4f-d335-47b8-be98-d7e4dcd9686d",
                    "LayerId": "827e3b03-7a55-4e22-b59c-573a51ede70b"
                }
            ]
        },
        {
            "id": "618d2565-2779-46f9-a24d-ee83ce3817ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "compositeImage": {
                "id": "a15ededd-43e1-4475-ab63-015923929c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "618d2565-2779-46f9-a24d-ee83ce3817ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50031e34-5a7f-49e0-8542-903b5b40547d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "618d2565-2779-46f9-a24d-ee83ce3817ad",
                    "LayerId": "827e3b03-7a55-4e22-b59c-573a51ede70b"
                }
            ]
        },
        {
            "id": "ff5d3b90-a501-44b1-abe5-ed98248545fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "compositeImage": {
                "id": "92b60196-ca5a-4b7e-9f9f-052919704447",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff5d3b90-a501-44b1-abe5-ed98248545fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57819493-060e-499a-80be-f8ca0313a9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff5d3b90-a501-44b1-abe5-ed98248545fd",
                    "LayerId": "827e3b03-7a55-4e22-b59c-573a51ede70b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "827e3b03-7a55-4e22-b59c-573a51ede70b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abc347ea-ea7a-4366-b801-7a1ec30f24dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 52
}