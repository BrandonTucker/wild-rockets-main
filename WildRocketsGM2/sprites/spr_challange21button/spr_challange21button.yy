{
    "id": "aa48d7c4-c9ee-46be-9a2a-27899522402e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange21button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 0,
    "bbox_right": 98,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5866c26c-0366-4396-a536-9738b8d09b67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa48d7c4-c9ee-46be-9a2a-27899522402e",
            "compositeImage": {
                "id": "047dcec9-a974-4b39-af3e-1f6334194091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5866c26c-0366-4396-a536-9738b8d09b67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "497597a7-3d86-4901-9aa5-84964c8246a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5866c26c-0366-4396-a536-9738b8d09b67",
                    "LayerId": "4a9d135c-7d5c-43da-abb0-3fc7989f8c47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4a9d135c-7d5c-43da-abb0-3fc7989f8c47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa48d7c4-c9ee-46be-9a2a-27899522402e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}