{
    "id": "2d4f1dab-ce61-4e75-83ff-b408f134b1b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship13locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ebae77a-0c3c-4dc2-a610-f9d2b64029d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d4f1dab-ce61-4e75-83ff-b408f134b1b1",
            "compositeImage": {
                "id": "0b21e6bb-0b1c-48aa-b73c-28045540be61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ebae77a-0c3c-4dc2-a610-f9d2b64029d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dea0cd9c-314b-4366-b20e-99abb21dbc5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ebae77a-0c3c-4dc2-a610-f9d2b64029d3",
                    "LayerId": "75d55318-1f50-4d68-abd0-ef68630719eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "75d55318-1f50-4d68-abd0-ef68630719eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d4f1dab-ce61-4e75-83ff-b408f134b1b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}