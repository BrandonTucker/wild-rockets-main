{
    "id": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr82small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8aa45b9f-2072-4f0a-b582-bf0db68ffa19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "7ba5eb43-a1e4-4436-9d28-91789a400dba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa45b9f-2072-4f0a-b582-bf0db68ffa19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c4a928b-f1b6-49d7-8d00-933bf776a965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa45b9f-2072-4f0a-b582-bf0db68ffa19",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "680e33c4-175c-48aa-a02c-05302f05accd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "ecaa9edb-d812-4f20-85b8-ab5cf680d06c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "680e33c4-175c-48aa-a02c-05302f05accd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d01f91-04df-4448-afc3-b47306dd403f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "680e33c4-175c-48aa-a02c-05302f05accd",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "afb0a3ae-846c-4b01-82ff-30524c5871ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "b9d346f5-9e8e-416e-825c-42f8521f1b1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb0a3ae-846c-4b01-82ff-30524c5871ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea7abd57-c9e8-4e07-8a23-d46710011075",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb0a3ae-846c-4b01-82ff-30524c5871ff",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "6d7b7784-7b54-42a6-8a0c-b658558358de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "7375662e-a96d-4baf-b64e-585f3bf2edb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d7b7784-7b54-42a6-8a0c-b658558358de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c73855-9328-49c0-bbb3-5027645c77d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d7b7784-7b54-42a6-8a0c-b658558358de",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "f7a3866f-62b8-4cda-be31-cd0e4706367a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "72f8815b-674b-4b93-90b2-990c4a3bb2cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7a3866f-62b8-4cda-be31-cd0e4706367a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80058403-8642-4020-9ae7-d9ff01f93f38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7a3866f-62b8-4cda-be31-cd0e4706367a",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "a6df0f0c-7e9c-44cc-9300-2ee67c5e9192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "fad29fb7-2400-40c3-9cf9-1ae5e02070f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6df0f0c-7e9c-44cc-9300-2ee67c5e9192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d3382b0-32ba-4a9f-ad64-db658ea21126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6df0f0c-7e9c-44cc-9300-2ee67c5e9192",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "c029adfa-fea1-4ff2-8d6b-69eccf5b9d55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "84809c73-2495-4f4f-b9e2-87a6d1728d20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c029adfa-fea1-4ff2-8d6b-69eccf5b9d55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69708dc3-fba9-4e64-94cc-32b72b20a139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c029adfa-fea1-4ff2-8d6b-69eccf5b9d55",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "a5e8377b-8d50-4e25-979b-264b2b1cbd55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "0886adc2-986b-4dce-b095-9a916b4a69c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5e8377b-8d50-4e25-979b-264b2b1cbd55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64b4d46e-dec8-4d15-9aa3-21899abbc8c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5e8377b-8d50-4e25-979b-264b2b1cbd55",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "c37505ba-d399-4099-90a0-34607d7e014f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "64e18bbd-4972-426e-8980-a370686d513b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37505ba-d399-4099-90a0-34607d7e014f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb271ca-e74f-4344-8a80-7d8e5f57bb2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37505ba-d399-4099-90a0-34607d7e014f",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "a2ff37e7-3576-4755-953e-dc3aed5e7223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "82a5a77a-98a3-44af-a7c7-c3a72fe57abc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2ff37e7-3576-4755-953e-dc3aed5e7223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cf53a17-375e-47a0-a9d9-ced543bbeeb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2ff37e7-3576-4755-953e-dc3aed5e7223",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "017aefd3-2671-4556-8f76-61e0165af53b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "04ae816e-fb83-4d58-8c56-3e3d9de15b99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "017aefd3-2671-4556-8f76-61e0165af53b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fc212a1-d2a4-4b6a-82d6-aa9f0d84cfde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "017aefd3-2671-4556-8f76-61e0165af53b",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "9144caa1-9627-46fe-8784-ec2b223240b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "29bd641c-c1ad-49fb-9862-f97e7eef4d36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9144caa1-9627-46fe-8784-ec2b223240b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c6905b5-bbe0-4fd7-9bd0-d0dbf4b11fb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9144caa1-9627-46fe-8784-ec2b223240b3",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "fa8061d2-8710-49fd-bebf-3fc0b7687e30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "c8ec7c90-5dba-4fcc-8193-d0c2f292a681",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa8061d2-8710-49fd-bebf-3fc0b7687e30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ce831c0-9dcb-4e0c-a71d-8b9d5b315ce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa8061d2-8710-49fd-bebf-3fc0b7687e30",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "8f1d38f4-6cd6-4b6d-94d6-4bc90009e18c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "e4bc47a4-f873-465a-ab62-a2d259abc269",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f1d38f4-6cd6-4b6d-94d6-4bc90009e18c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57b3ffc8-6dd5-4ce9-98a0-1ddb4bdbae21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f1d38f4-6cd6-4b6d-94d6-4bc90009e18c",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "c9b0cbf9-e1ae-40ea-83cb-8b7600c2d4c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "bc088c3f-8548-4e29-a634-56d6e1278941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9b0cbf9-e1ae-40ea-83cb-8b7600c2d4c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d32e6f6-7e1b-4632-8e4e-9ef178184ea8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9b0cbf9-e1ae-40ea-83cb-8b7600c2d4c2",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "d685b6cf-6864-4f90-9317-0239bbd53dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "fff87c04-e6d9-4c44-a25c-5c21364bd214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d685b6cf-6864-4f90-9317-0239bbd53dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "509d3bc9-5dab-45ba-81ae-ddd14b849a38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d685b6cf-6864-4f90-9317-0239bbd53dbd",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "5a53415b-848e-4dd5-ad5b-85e818beece2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "501163d2-3e0b-4efb-9693-98895937923c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a53415b-848e-4dd5-ad5b-85e818beece2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1f015f0-57de-484c-8b33-14bfa2824783",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a53415b-848e-4dd5-ad5b-85e818beece2",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "51ef7687-eb16-4d89-8012-8e6252f26823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "7ef84f0f-5f33-4522-91b1-2c94012f0459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ef7687-eb16-4d89-8012-8e6252f26823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d14e736-7487-4edd-a5a9-8a27b376aa5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ef7687-eb16-4d89-8012-8e6252f26823",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "c9d941af-9264-462b-8d01-a00c0e4dfb7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "863abb8b-0e92-47c0-8237-2061e0930bbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9d941af-9264-462b-8d01-a00c0e4dfb7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef4052dd-d3bf-4350-81c3-bf97eecda351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9d941af-9264-462b-8d01-a00c0e4dfb7a",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "2b9054ec-c7f2-4c6e-9f16-793b485f3d2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "28716d67-f0b3-466c-9b5b-37aa1f509bd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b9054ec-c7f2-4c6e-9f16-793b485f3d2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dc9e626-b643-4722-a562-0c22bcf5f8c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b9054ec-c7f2-4c6e-9f16-793b485f3d2f",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "dd1f04b0-18d2-4a90-bf43-2b39817d59de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "f5e1bae6-d388-4693-9610-0f177fd0f8a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd1f04b0-18d2-4a90-bf43-2b39817d59de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f73248ae-f77d-4942-bb1d-3adec6efdf12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd1f04b0-18d2-4a90-bf43-2b39817d59de",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "0ef5f110-63c2-4805-9155-0908c1191cce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "1de01d54-a2ac-49e8-894c-9ab349fd0415",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ef5f110-63c2-4805-9155-0908c1191cce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f8f732d-4de9-4f24-ab1e-7c2fce29ef66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef5f110-63c2-4805-9155-0908c1191cce",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "8005378e-a1fe-4fd7-bc12-4851c6be45d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "bfcd9409-9542-4678-ad4f-d99e8817f74a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8005378e-a1fe-4fd7-bc12-4851c6be45d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff44706b-3dd0-4c86-a2f7-71ec8eb47592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8005378e-a1fe-4fd7-bc12-4851c6be45d7",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "5279a672-3612-46fb-aac0-2559fc34b830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "eab60373-ff17-4417-981c-0e249420f6c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5279a672-3612-46fb-aac0-2559fc34b830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97527ba4-fede-4015-a58b-622b73be4740",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5279a672-3612-46fb-aac0-2559fc34b830",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "75ac654e-ffa7-4923-9c7d-0b3185e4be8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "3c913a61-0fe3-4207-ae78-d16916da8b3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ac654e-ffa7-4923-9c7d-0b3185e4be8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4b1ad1c-ee99-4cbd-8b19-0e670cc3ab88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ac654e-ffa7-4923-9c7d-0b3185e4be8a",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "2e5c9c3f-c66c-4786-b3bf-2cc6b7c850c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "95bd6df9-4918-4fc3-8d47-847ac3d7e5ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e5c9c3f-c66c-4786-b3bf-2cc6b7c850c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b21f0435-f994-4afb-9341-4dea6fc479d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e5c9c3f-c66c-4786-b3bf-2cc6b7c850c1",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "e97ace79-d930-4b50-931d-9ddccd956454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "e3166ee9-5656-455f-a2d3-b9690e3ff0c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97ace79-d930-4b50-931d-9ddccd956454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7bae76e-7e91-4b62-af48-7be1809f162a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97ace79-d930-4b50-931d-9ddccd956454",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "29652e83-c580-4c12-90a1-4766e35adf11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "bb9346d1-7b1b-4192-98ac-47fa5cf643fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29652e83-c580-4c12-90a1-4766e35adf11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95b19044-ee57-4e2a-8dae-1e0ee9dc62ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29652e83-c580-4c12-90a1-4766e35adf11",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "aedd4936-1ab0-4b68-9165-16f695959cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "367a0ff0-bfc0-4d9f-a351-d9506de8d203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aedd4936-1ab0-4b68-9165-16f695959cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3339e7a2-b718-4ed7-8d20-17deab4e2f3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aedd4936-1ab0-4b68-9165-16f695959cd1",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "9c30ad75-eeae-479b-8740-939e951a8790",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "ec9336e2-741e-457a-863f-2d260827d7af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c30ad75-eeae-479b-8740-939e951a8790",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df678504-31b6-42cb-a14f-893fdf8decb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c30ad75-eeae-479b-8740-939e951a8790",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "930c0145-b1ef-4276-afb4-6fb0ff12862a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "ebc15916-8733-488b-8035-eed198b515f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "930c0145-b1ef-4276-afb4-6fb0ff12862a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4440ceab-1171-4882-b686-f82cef6c965f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "930c0145-b1ef-4276-afb4-6fb0ff12862a",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "b06f888a-d240-4fba-b923-b9d7c1b09eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "a85ed5fa-68a6-43f2-9633-bdd15f8afa2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b06f888a-d240-4fba-b923-b9d7c1b09eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ace0dca2-a8be-495d-9463-9cd5a6988db1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b06f888a-d240-4fba-b923-b9d7c1b09eef",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "dc073e66-aec7-48d0-8146-7bda9f90ddea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "d3aa3fc2-168b-4c0c-aac1-710b6b04df64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc073e66-aec7-48d0-8146-7bda9f90ddea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e52559f-10d8-465b-98c8-eb2b87addd21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc073e66-aec7-48d0-8146-7bda9f90ddea",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "16008832-4a1f-4258-9867-c9c511cbdc44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "7c095aea-41ec-4c68-aa47-8ad10e901ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16008832-4a1f-4258-9867-c9c511cbdc44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fd8a93a-16c7-4fcf-a7b0-175945260f11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16008832-4a1f-4258-9867-c9c511cbdc44",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "2490700e-5632-455c-8c0c-a78501a23b88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "807c833e-da39-4c7d-bf09-605b9bfde105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2490700e-5632-455c-8c0c-a78501a23b88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f321d7be-4db9-41db-9aa5-e93dea9c6623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2490700e-5632-455c-8c0c-a78501a23b88",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "9cd01734-5618-4b1b-8a1e-2075c555f15d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "961c7812-59ab-46db-8496-92ce36b54d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd01734-5618-4b1b-8a1e-2075c555f15d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62b97e5d-fc7a-4c26-bb75-22ae9f673a7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd01734-5618-4b1b-8a1e-2075c555f15d",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "a562c3a1-3a55-4fb9-8cee-a9524c5ba0fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "0dc35e5a-9a96-48ab-9034-dc0c5703e867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a562c3a1-3a55-4fb9-8cee-a9524c5ba0fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56389632-7629-4f5a-b047-2b8aa8afcbfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a562c3a1-3a55-4fb9-8cee-a9524c5ba0fa",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "237db902-fbd7-4be5-9aed-00d6a7ef183c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "fbc3aa82-e836-49c2-9751-cda64af52f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "237db902-fbd7-4be5-9aed-00d6a7ef183c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3a7a383-1684-436b-8b6f-6e7839f235ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237db902-fbd7-4be5-9aed-00d6a7ef183c",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "a2db44cb-6871-4b86-b99e-c5535c22fd80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "1a0cefa7-a3b3-4044-ba32-0cd351e8fdb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2db44cb-6871-4b86-b99e-c5535c22fd80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38f588a4-12fd-48ce-b047-80c427443dc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2db44cb-6871-4b86-b99e-c5535c22fd80",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        },
        {
            "id": "e49e0dda-f672-4023-aed4-16aa4afc9f54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "compositeImage": {
                "id": "a8178e10-c121-45e0-8d33-d080d7cd1b61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e49e0dda-f672-4023-aed4-16aa4afc9f54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af75e8a2-c9ac-4202-8ff6-7a3cd4300dac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e49e0dda-f672-4023-aed4-16aa4afc9f54",
                    "LayerId": "1fa59265-c488-44af-98cf-4807951f45b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "1fa59265-c488-44af-98cf-4807951f45b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bb0eea5-ff6d-4ffc-8681-a4888d1f196e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 21,
    "yorig": 24
}