{
    "id": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage20buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20201ec7-8761-4429-a53e-fcf8760bd341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "compositeImage": {
                "id": "0e9be320-3d60-4927-a1e2-0854854218cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20201ec7-8761-4429-a53e-fcf8760bd341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b02f45-c6c7-4b14-8dea-0f567f4c62a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20201ec7-8761-4429-a53e-fcf8760bd341",
                    "LayerId": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d"
                }
            ]
        },
        {
            "id": "55e44351-1776-49c0-a9d2-d34a73f87f66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "compositeImage": {
                "id": "6934a81e-046e-4224-8207-137dcb4264df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55e44351-1776-49c0-a9d2-d34a73f87f66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e8a17a4-9ec7-4d11-aa32-94fab01a3829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55e44351-1776-49c0-a9d2-d34a73f87f66",
                    "LayerId": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d"
                }
            ]
        },
        {
            "id": "2b546b09-31dc-41aa-90c0-d301d099dd17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "compositeImage": {
                "id": "7039cac1-bc1e-4c04-964a-805a5e249765",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b546b09-31dc-41aa-90c0-d301d099dd17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64406baa-f108-4d89-855a-1562264b836e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b546b09-31dc-41aa-90c0-d301d099dd17",
                    "LayerId": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d"
                }
            ]
        },
        {
            "id": "46d427d7-446a-4703-9f44-e653e2d5c186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "compositeImage": {
                "id": "7826c3dd-cfac-4dec-9b44-f72e6f810c0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d427d7-446a-4703-9f44-e653e2d5c186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb12cd80-2544-4bea-8e98-b71b5f64e3ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d427d7-446a-4703-9f44-e653e2d5c186",
                    "LayerId": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d"
                }
            ]
        },
        {
            "id": "c9d6eb36-799d-4670-812e-f760cf8184db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "compositeImage": {
                "id": "5120e6da-d9fe-4849-b341-40c5d18d865d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9d6eb36-799d-4670-812e-f760cf8184db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35f7dc88-ae99-4be0-bcf8-f3405bb48ae6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9d6eb36-799d-4670-812e-f760cf8184db",
                    "LayerId": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d"
                }
            ]
        },
        {
            "id": "f4d7b68e-80f1-4ff9-a7b2-f3f0ad04194c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "compositeImage": {
                "id": "52fa448b-9f35-4f2f-bf02-8f7b41e4db04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4d7b68e-80f1-4ff9-a7b2-f3f0ad04194c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "037f795a-6b23-479e-9224-c22cd9fc548a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4d7b68e-80f1-4ff9-a7b2-f3f0ad04194c",
                    "LayerId": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d"
                }
            ]
        },
        {
            "id": "bc5ef60e-6ca6-4215-83cf-055292b1a1e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "compositeImage": {
                "id": "37bbeca3-144a-4373-bdfa-d5c4278aec1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc5ef60e-6ca6-4215-83cf-055292b1a1e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cadaee26-3ef8-4427-9c3d-1d7f00519724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc5ef60e-6ca6-4215-83cf-055292b1a1e5",
                    "LayerId": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d"
                }
            ]
        },
        {
            "id": "c11b4d73-67bf-42d7-b32d-25adc01907aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "compositeImage": {
                "id": "340141c7-902c-4865-abf1-596f7faca007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c11b4d73-67bf-42d7-b32d-25adc01907aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ae2687c-97ed-480b-bafd-c2c456c66d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c11b4d73-67bf-42d7-b32d-25adc01907aa",
                    "LayerId": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d"
                }
            ]
        },
        {
            "id": "6cb1003b-118a-40e9-acd1-cbe3709d087e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "compositeImage": {
                "id": "345513dc-8c7e-43f5-8e81-8c1dc1f12835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cb1003b-118a-40e9-acd1-cbe3709d087e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3829de0e-9246-4212-86ea-b6d059b0defc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cb1003b-118a-40e9-acd1-cbe3709d087e",
                    "LayerId": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d"
                }
            ]
        },
        {
            "id": "0268b05b-5578-404e-9104-71017fee6058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "compositeImage": {
                "id": "8554777b-a94c-4560-a7f1-ecbdb07ceea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0268b05b-5578-404e-9104-71017fee6058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dde70df9-63ba-42ea-9e60-5052864f627b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0268b05b-5578-404e-9104-71017fee6058",
                    "LayerId": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "aec4fc5b-0b3d-4998-bf3b-9c31676f204d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d35a18aa-6a33-4ff5-9f6e-56c796cc9b5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}