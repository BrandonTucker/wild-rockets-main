{
    "id": "943e34fc-d68d-41bb-a2e2-4ea1599c8f51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship31boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 40,
    "bbox_right": 65,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d754bf7d-580a-49dc-b4f5-b670f6f872fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "943e34fc-d68d-41bb-a2e2-4ea1599c8f51",
            "compositeImage": {
                "id": "22fb9132-827c-406f-949d-e3da44452ada",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d754bf7d-580a-49dc-b4f5-b670f6f872fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ac58d04-dd92-4f3f-84bc-7e48a8fc01ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d754bf7d-580a-49dc-b4f5-b670f6f872fd",
                    "LayerId": "346ab078-899b-4f03-8116-83eba5116feb"
                }
            ]
        },
        {
            "id": "09829a1c-1b4c-4450-b1db-ec5edc3bc3db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "943e34fc-d68d-41bb-a2e2-4ea1599c8f51",
            "compositeImage": {
                "id": "71459e78-9789-4cd9-954a-cf61e34913e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09829a1c-1b4c-4450-b1db-ec5edc3bc3db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fa7bf19-c36a-49f2-b736-2e8e9a1a63d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09829a1c-1b4c-4450-b1db-ec5edc3bc3db",
                    "LayerId": "346ab078-899b-4f03-8116-83eba5116feb"
                }
            ]
        },
        {
            "id": "527caf58-3af0-4def-9b32-29b0115abb9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "943e34fc-d68d-41bb-a2e2-4ea1599c8f51",
            "compositeImage": {
                "id": "26741608-c839-42d7-a75a-d144df396fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "527caf58-3af0-4def-9b32-29b0115abb9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05cfebea-89cb-47c5-aa9b-dd455652a1ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "527caf58-3af0-4def-9b32-29b0115abb9e",
                    "LayerId": "346ab078-899b-4f03-8116-83eba5116feb"
                }
            ]
        },
        {
            "id": "67844855-231d-40df-b2c9-9f4c7fb9081d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "943e34fc-d68d-41bb-a2e2-4ea1599c8f51",
            "compositeImage": {
                "id": "9ea874b6-97ee-490c-a8b4-acb8c0963e42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67844855-231d-40df-b2c9-9f4c7fb9081d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "914ad197-9e4d-4d20-bb78-de1481b8f4d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67844855-231d-40df-b2c9-9f4c7fb9081d",
                    "LayerId": "346ab078-899b-4f03-8116-83eba5116feb"
                }
            ]
        },
        {
            "id": "ea6a666e-599b-45f1-9503-c541ed45b4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "943e34fc-d68d-41bb-a2e2-4ea1599c8f51",
            "compositeImage": {
                "id": "5d9ee534-5892-4379-b5ab-af8f01d7868a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea6a666e-599b-45f1-9503-c541ed45b4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56e65390-089a-4b90-bcf7-5b27909d079a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea6a666e-599b-45f1-9503-c541ed45b4b0",
                    "LayerId": "346ab078-899b-4f03-8116-83eba5116feb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "346ab078-899b-4f03-8116-83eba5116feb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "943e34fc-d68d-41bb-a2e2-4ea1599c8f51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 53,
    "yorig": 49
}