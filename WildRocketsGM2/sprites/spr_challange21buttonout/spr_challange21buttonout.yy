{
    "id": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange21buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 0,
    "bbox_right": 104,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2de994f-50c9-4448-8bf1-8de9f8939207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "compositeImage": {
                "id": "55cb0af4-e444-4d51-b58b-04a6957a095e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2de994f-50c9-4448-8bf1-8de9f8939207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20ebb79-61c3-4ea9-903e-5c0d3f77d664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2de994f-50c9-4448-8bf1-8de9f8939207",
                    "LayerId": "c40a7e26-19e0-4721-af18-c02c6272902a"
                }
            ]
        },
        {
            "id": "64b87423-2d41-4bd4-af71-8fc6b92ff14e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "compositeImage": {
                "id": "b0de0630-2539-44a4-b872-52fadeb5ca10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64b87423-2d41-4bd4-af71-8fc6b92ff14e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81129d5c-5c5d-4675-8028-9cae8debdd4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64b87423-2d41-4bd4-af71-8fc6b92ff14e",
                    "LayerId": "c40a7e26-19e0-4721-af18-c02c6272902a"
                }
            ]
        },
        {
            "id": "06b9eb6f-5e51-484a-aed5-ddc5fe8a9d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "compositeImage": {
                "id": "ba85a83b-3435-4472-b298-6724f01eb2bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06b9eb6f-5e51-484a-aed5-ddc5fe8a9d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72128454-a1b9-43c1-bead-50058a87b1b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06b9eb6f-5e51-484a-aed5-ddc5fe8a9d4b",
                    "LayerId": "c40a7e26-19e0-4721-af18-c02c6272902a"
                }
            ]
        },
        {
            "id": "e7c99ba2-987b-4809-92d7-04d986e46ff9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "compositeImage": {
                "id": "6d49bd0c-ed37-4f53-8ea4-5fcf54310883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c99ba2-987b-4809-92d7-04d986e46ff9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d833e27-a24c-4dd5-98b4-f81bdbe09cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c99ba2-987b-4809-92d7-04d986e46ff9",
                    "LayerId": "c40a7e26-19e0-4721-af18-c02c6272902a"
                }
            ]
        },
        {
            "id": "93d7d89e-fb21-4e1c-99f0-85a9106b9cf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "compositeImage": {
                "id": "6eb62509-9bfb-4e4b-b832-ee15bb5ccc44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93d7d89e-fb21-4e1c-99f0-85a9106b9cf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b0af2a-af57-4930-acba-f478939c9f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93d7d89e-fb21-4e1c-99f0-85a9106b9cf4",
                    "LayerId": "c40a7e26-19e0-4721-af18-c02c6272902a"
                }
            ]
        },
        {
            "id": "8faac171-f6b6-4157-807b-07f5ed7d452a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "compositeImage": {
                "id": "e6e395b1-6b2d-4454-b9cc-25027bbe1e99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8faac171-f6b6-4157-807b-07f5ed7d452a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "330e6ad8-c7ac-497c-8298-82b9aaa5ab67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8faac171-f6b6-4157-807b-07f5ed7d452a",
                    "LayerId": "c40a7e26-19e0-4721-af18-c02c6272902a"
                }
            ]
        },
        {
            "id": "0aecb4a3-792c-40c9-8dad-eb00496727fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "compositeImage": {
                "id": "8519ce4a-cda2-4bc6-9b19-0595488aff80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aecb4a3-792c-40c9-8dad-eb00496727fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e78932-89c6-4466-b8b6-63f7dbef9965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aecb4a3-792c-40c9-8dad-eb00496727fd",
                    "LayerId": "c40a7e26-19e0-4721-af18-c02c6272902a"
                }
            ]
        },
        {
            "id": "4dec104a-ace8-42fe-9cb9-bfd6259b3df9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "compositeImage": {
                "id": "686459f4-2cd7-4ff0-94cd-f937018c1f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dec104a-ace8-42fe-9cb9-bfd6259b3df9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "433b0be1-6762-4a36-a35b-6117d65bd169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dec104a-ace8-42fe-9cb9-bfd6259b3df9",
                    "LayerId": "c40a7e26-19e0-4721-af18-c02c6272902a"
                }
            ]
        },
        {
            "id": "4185eca3-f98a-4dbf-95a7-e643d815bd70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "compositeImage": {
                "id": "75091530-240a-4df9-88b4-db34600798e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4185eca3-f98a-4dbf-95a7-e643d815bd70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f33f85fb-7494-4ebb-9e14-211315b8223a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4185eca3-f98a-4dbf-95a7-e643d815bd70",
                    "LayerId": "c40a7e26-19e0-4721-af18-c02c6272902a"
                }
            ]
        },
        {
            "id": "56deb996-abbb-471a-ac07-b3986534d6c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "compositeImage": {
                "id": "7f7affff-2247-4936-badd-3bd6bf25fef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56deb996-abbb-471a-ac07-b3986534d6c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "312dc99c-ff5f-462a-8b37-d5ef1e54ee49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56deb996-abbb-471a-ac07-b3986534d6c2",
                    "LayerId": "c40a7e26-19e0-4721-af18-c02c6272902a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "c40a7e26-19e0-4721-af18-c02c6272902a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e43839c-4446-4ce6-a007-79fc2d35c54f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 53,
    "yorig": 52
}