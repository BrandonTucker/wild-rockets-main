{
    "id": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challangebutton2out",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 0,
    "bbox_right": 105,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97fbd51d-4c10-45c4-bdc3-7a544b5c1a30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "compositeImage": {
                "id": "27cbfc02-758e-4ab2-8035-43781f8d28c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97fbd51d-4c10-45c4-bdc3-7a544b5c1a30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa9aa404-4028-45b9-a563-3f3ef6941489",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97fbd51d-4c10-45c4-bdc3-7a544b5c1a30",
                    "LayerId": "1631344d-3cb8-484b-8986-f653bfec9869"
                }
            ]
        },
        {
            "id": "af20a8c5-ebab-4ecb-a43a-e2f195fc8000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "compositeImage": {
                "id": "2f6d4a09-1e57-44e2-b4a8-9a85a1461666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af20a8c5-ebab-4ecb-a43a-e2f195fc8000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab3872a9-2e12-48cf-aff5-5daa7489fbfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af20a8c5-ebab-4ecb-a43a-e2f195fc8000",
                    "LayerId": "1631344d-3cb8-484b-8986-f653bfec9869"
                }
            ]
        },
        {
            "id": "82a0288c-dd59-4da9-9235-3bc810c2e0ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "compositeImage": {
                "id": "f2637fd9-6837-4234-8d6b-08cefa96fc91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a0288c-dd59-4da9-9235-3bc810c2e0ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6c993eb-5ac6-4d57-bbc8-3d83f6bf8169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a0288c-dd59-4da9-9235-3bc810c2e0ab",
                    "LayerId": "1631344d-3cb8-484b-8986-f653bfec9869"
                }
            ]
        },
        {
            "id": "b70eda90-d616-4ee7-bae1-0a1f8d790cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "compositeImage": {
                "id": "669d9e6a-5943-459d-82b0-50193b521f18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b70eda90-d616-4ee7-bae1-0a1f8d790cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "390a38fc-243a-4588-bf1a-cbbacb6a2a19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b70eda90-d616-4ee7-bae1-0a1f8d790cd1",
                    "LayerId": "1631344d-3cb8-484b-8986-f653bfec9869"
                }
            ]
        },
        {
            "id": "0a5d38c5-4b5d-4658-81ca-634a4bfd6a74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "compositeImage": {
                "id": "fb6ec249-d0e6-4cdd-a82c-d1e554c1d4ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a5d38c5-4b5d-4658-81ca-634a4bfd6a74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a613ab8-b47b-444c-9684-6a04fef4a7f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a5d38c5-4b5d-4658-81ca-634a4bfd6a74",
                    "LayerId": "1631344d-3cb8-484b-8986-f653bfec9869"
                }
            ]
        },
        {
            "id": "5e860912-cac9-4889-9431-7a460598a1ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "compositeImage": {
                "id": "51dc1404-8c48-479e-940c-27467080ccbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e860912-cac9-4889-9431-7a460598a1ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "680796a8-6342-49dc-a395-c45cd09fa591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e860912-cac9-4889-9431-7a460598a1ed",
                    "LayerId": "1631344d-3cb8-484b-8986-f653bfec9869"
                }
            ]
        },
        {
            "id": "26919c9a-f2ec-4a59-8e9f-4607f5e208ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "compositeImage": {
                "id": "c44e02d9-a0fe-401b-ae59-7534742f7cbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26919c9a-f2ec-4a59-8e9f-4607f5e208ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf9f6df-7728-437a-b783-6c80aaddd3bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26919c9a-f2ec-4a59-8e9f-4607f5e208ec",
                    "LayerId": "1631344d-3cb8-484b-8986-f653bfec9869"
                }
            ]
        },
        {
            "id": "16072c05-161a-4c28-a83c-361c7e9734be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "compositeImage": {
                "id": "5f6adab1-4412-4078-bb07-4b9db7a4d4aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16072c05-161a-4c28-a83c-361c7e9734be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07928b95-c610-48ef-bfbe-24851d2a7025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16072c05-161a-4c28-a83c-361c7e9734be",
                    "LayerId": "1631344d-3cb8-484b-8986-f653bfec9869"
                }
            ]
        },
        {
            "id": "578f1341-e0c5-40e1-80d0-5f77f0109b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "compositeImage": {
                "id": "05e80d4b-94d7-4024-93c5-f62347601771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "578f1341-e0c5-40e1-80d0-5f77f0109b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2728bbe7-51ef-4554-8faa-7e58b8aa1a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "578f1341-e0c5-40e1-80d0-5f77f0109b42",
                    "LayerId": "1631344d-3cb8-484b-8986-f653bfec9869"
                }
            ]
        },
        {
            "id": "75f1a6e9-b518-41e0-ae60-f2071ea8b77b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "compositeImage": {
                "id": "f66bcba4-5d18-41cb-ac70-eaeb15d33615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75f1a6e9-b518-41e0-ae60-f2071ea8b77b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9c8be97-5e7e-4c17-8c45-d143007c623d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75f1a6e9-b518-41e0-ae60-f2071ea8b77b",
                    "LayerId": "1631344d-3cb8-484b-8986-f653bfec9869"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "1631344d-3cb8-484b-8986-f653bfec9869",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80f59a5b-1b62-42b7-9e3e-271dc2c50fe3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 106,
    "xorig": 53,
    "yorig": 50
}