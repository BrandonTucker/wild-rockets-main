{
    "id": "25abf59e-169b-46de-ba1b-f2bc1097a59c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange23button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 0,
    "bbox_right": 98,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97c67c00-1516-4c69-a1e1-db44a3ffdeb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25abf59e-169b-46de-ba1b-f2bc1097a59c",
            "compositeImage": {
                "id": "aeda8b7a-95b6-4f56-9269-0d4b6b2f128d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97c67c00-1516-4c69-a1e1-db44a3ffdeb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91d684f0-203f-4128-bc88-7acb1eabfc61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c67c00-1516-4c69-a1e1-db44a3ffdeb5",
                    "LayerId": "ed1a7c20-f4d2-4441-9503-94147db7e323"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "ed1a7c20-f4d2-4441-9503-94147db7e323",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25abf59e-169b-46de-ba1b-f2bc1097a59c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}