{
    "id": "c2e1f965-a215-4df9-9553-55a4d0d8cbec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rewardBackground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 313,
    "bbox_left": 0,
    "bbox_right": 419,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6c23e89-9d54-4803-8994-2d51d36a2ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2e1f965-a215-4df9-9553-55a4d0d8cbec",
            "compositeImage": {
                "id": "e1ba7b1c-735b-47f8-bddb-7ef2cfa63756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6c23e89-9d54-4803-8994-2d51d36a2ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbe99f9e-a1b1-4664-b473-d4c752fae219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6c23e89-9d54-4803-8994-2d51d36a2ad0",
                    "LayerId": "6d2cfd73-99bf-43c7-9568-6a0259b2a6a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 314,
    "layers": [
        {
            "id": "6d2cfd73-99bf-43c7-9568-6a0259b2a6a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2e1f965-a215-4df9-9553-55a4d0d8cbec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 420,
    "xorig": 210,
    "yorig": 157
}