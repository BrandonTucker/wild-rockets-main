{
    "id": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crumb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 25,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "785c3f36-332c-4217-a3e7-4d735243b65d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "compositeImage": {
                "id": "499f928c-04d6-4a96-be3d-4c9bf8af94e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "785c3f36-332c-4217-a3e7-4d735243b65d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29801067-8562-4523-86c9-ec9f0ba79c86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "785c3f36-332c-4217-a3e7-4d735243b65d",
                    "LayerId": "12b6aec3-ecb3-4260-acca-00c85b078161"
                }
            ]
        },
        {
            "id": "8eb27b6f-7e7b-41fe-adbb-5c116f97f8b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "compositeImage": {
                "id": "38c111c9-4bab-470c-b242-07f7622a6a93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eb27b6f-7e7b-41fe-adbb-5c116f97f8b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b32c8c0-2bc6-4d6e-b7bc-e5f039550654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eb27b6f-7e7b-41fe-adbb-5c116f97f8b5",
                    "LayerId": "12b6aec3-ecb3-4260-acca-00c85b078161"
                }
            ]
        },
        {
            "id": "6aee26c5-82ee-42a6-8701-c639048ad599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "compositeImage": {
                "id": "e2398eef-7db3-4a57-bc03-646c25c2d809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aee26c5-82ee-42a6-8701-c639048ad599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "527d8ece-b9df-41cd-a64f-fc6c1626f5ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aee26c5-82ee-42a6-8701-c639048ad599",
                    "LayerId": "12b6aec3-ecb3-4260-acca-00c85b078161"
                }
            ]
        },
        {
            "id": "264a584f-6928-4c6e-a705-552125693cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "compositeImage": {
                "id": "a8a88fc5-a4bd-421f-be95-825cf74359df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "264a584f-6928-4c6e-a705-552125693cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c8c476e-6c5d-49f0-a6fa-9d1400d3716f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "264a584f-6928-4c6e-a705-552125693cc1",
                    "LayerId": "12b6aec3-ecb3-4260-acca-00c85b078161"
                }
            ]
        },
        {
            "id": "4f4f56ba-cfc7-45d1-8cb2-68cb3975c23d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "compositeImage": {
                "id": "ecbd6d03-05f5-40c5-8100-86956acc2c47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f4f56ba-cfc7-45d1-8cb2-68cb3975c23d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7425bb8f-caf4-4648-96c7-4e3c3ee213b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f4f56ba-cfc7-45d1-8cb2-68cb3975c23d",
                    "LayerId": "12b6aec3-ecb3-4260-acca-00c85b078161"
                }
            ]
        },
        {
            "id": "fd2b2da1-a46a-4f19-871b-bbb37d85c6c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "compositeImage": {
                "id": "53a3bd19-d93c-4a7d-9511-8ffa97b7ea73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd2b2da1-a46a-4f19-871b-bbb37d85c6c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62d60a06-518a-4773-8cc4-c956864e5ea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd2b2da1-a46a-4f19-871b-bbb37d85c6c4",
                    "LayerId": "12b6aec3-ecb3-4260-acca-00c85b078161"
                }
            ]
        },
        {
            "id": "2a943f9f-c160-4822-b3b0-7377c4cbb14a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "compositeImage": {
                "id": "42ef0afa-94ad-47ba-bee2-57cd29d8db4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a943f9f-c160-4822-b3b0-7377c4cbb14a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db080d5d-fe33-45ac-8bc4-ea589e5f0e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a943f9f-c160-4822-b3b0-7377c4cbb14a",
                    "LayerId": "12b6aec3-ecb3-4260-acca-00c85b078161"
                }
            ]
        },
        {
            "id": "2582dcd7-a4d8-495a-be22-dae9f1152840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "compositeImage": {
                "id": "88456345-18f2-48c7-84de-87d82fca6f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2582dcd7-a4d8-495a-be22-dae9f1152840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79c738c6-de16-4922-a067-7f5106ae3e57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2582dcd7-a4d8-495a-be22-dae9f1152840",
                    "LayerId": "12b6aec3-ecb3-4260-acca-00c85b078161"
                }
            ]
        },
        {
            "id": "b51cac94-40ad-4741-a20d-9dcb0ccc58a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "compositeImage": {
                "id": "2e3bf78d-5e5f-42c5-8f7b-41df956815dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b51cac94-40ad-4741-a20d-9dcb0ccc58a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aa6c94b-6b2d-42ba-b5be-464db33acebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b51cac94-40ad-4741-a20d-9dcb0ccc58a5",
                    "LayerId": "12b6aec3-ecb3-4260-acca-00c85b078161"
                }
            ]
        },
        {
            "id": "25ca8968-bba5-4502-95cb-aa7e8c81d344",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "compositeImage": {
                "id": "1ac09119-f28c-43d5-9f28-fe5c0436bc57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25ca8968-bba5-4502-95cb-aa7e8c81d344",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e75fd25-d579-4013-8c84-4068500e82ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25ca8968-bba5-4502-95cb-aa7e8c81d344",
                    "LayerId": "12b6aec3-ecb3-4260-acca-00c85b078161"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "12b6aec3-ecb3-4260-acca-00c85b078161",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f6cdc7e-87c0-4ef1-9c29-d6c3be955dc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 7,
    "yorig": 6
}