{
    "id": "93fc3f63-34ec-4e7d-9e12-4bc21a839e60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship5button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "355714da-0968-4233-ae77-dda3470e2d4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93fc3f63-34ec-4e7d-9e12-4bc21a839e60",
            "compositeImage": {
                "id": "78f89aeb-468b-4d12-9efa-a2f5d88653ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "355714da-0968-4233-ae77-dda3470e2d4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa91610-096b-428b-98e4-bda7ccc3295a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "355714da-0968-4233-ae77-dda3470e2d4a",
                    "LayerId": "f867c689-d24c-4cce-8d83-f29d9e3c444d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "f867c689-d24c-4cce-8d83-f29d9e3c444d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93fc3f63-34ec-4e7d-9e12-4bc21a839e60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}