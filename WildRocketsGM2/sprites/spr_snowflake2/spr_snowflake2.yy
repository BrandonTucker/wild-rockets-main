{
    "id": "23de7638-977a-49f5-b699-fb2305e9e6a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_snowflake2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e9e3464-76cb-4fd3-acf4-67f96558512a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "46a98fdd-ea63-4f2a-a439-2ea9d5f0d787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e9e3464-76cb-4fd3-acf4-67f96558512a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f254fa-ec3c-443d-82db-30a42232bafc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e9e3464-76cb-4fd3-acf4-67f96558512a",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "13256200-8deb-4850-b677-826ce2fd4830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "03f95aa8-c821-43ee-b54d-1cf23898fdf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13256200-8deb-4850-b677-826ce2fd4830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c3a0d4-2632-49bf-b016-0f91b8967cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13256200-8deb-4850-b677-826ce2fd4830",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "b286b315-1c46-4963-9eea-3b21a5d370d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "454fb6b5-ae77-4ec3-b504-ed5f44c4afec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b286b315-1c46-4963-9eea-3b21a5d370d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f2aa1a7-fe98-482b-b93c-202882e7b22f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b286b315-1c46-4963-9eea-3b21a5d370d4",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "377f0aef-75ac-458d-b453-23195284a8fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "1e27dd11-a6ec-4cca-b0af-7ba35cb4029d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "377f0aef-75ac-458d-b453-23195284a8fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a96e93cd-4efb-4b91-b303-bb9b81c007fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "377f0aef-75ac-458d-b453-23195284a8fb",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "e8bf3433-9d7f-4301-880a-3e7d63159edb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "0c0e145a-ab1a-449d-aab0-21c657493d7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8bf3433-9d7f-4301-880a-3e7d63159edb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "056952b5-9e9e-4ed3-9da8-9932e04fc0a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8bf3433-9d7f-4301-880a-3e7d63159edb",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "db843478-22e0-465b-a226-32acb71d7e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "02e9a519-5843-4738-bb6c-7a927a1b1400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db843478-22e0-465b-a226-32acb71d7e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22df22c3-b3a7-468e-9eab-0beb3f235528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db843478-22e0-465b-a226-32acb71d7e16",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "f77b29aa-3254-4900-9ca1-7a64585bbaa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "35b6cc27-6ea6-40e6-9949-937c61f30fa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f77b29aa-3254-4900-9ca1-7a64585bbaa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f2bdd6e-61ba-4dcc-a317-f7090194bec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f77b29aa-3254-4900-9ca1-7a64585bbaa9",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "fcd24bca-bd7b-46e3-a105-82dc2bbab39a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "a2b00587-0ea5-42f6-b200-bac664292059",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd24bca-bd7b-46e3-a105-82dc2bbab39a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d40c4f8-778a-4b2a-98e7-7bb6c5f6be40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd24bca-bd7b-46e3-a105-82dc2bbab39a",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "715dac21-dee0-4998-9138-75445bc4a0ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "0638ea4d-da26-4f13-b20d-4619ba863e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "715dac21-dee0-4998-9138-75445bc4a0ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c17d4c40-b391-4174-b9f7-b8d70cc00175",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "715dac21-dee0-4998-9138-75445bc4a0ac",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "0b60efe5-4983-474d-9afd-c2ce7f03b061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "039d23f4-27d0-44ee-9b8c-e30c19c6ba49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b60efe5-4983-474d-9afd-c2ce7f03b061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c737bc2-ff48-4e34-8c8b-62d0c87a0b88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b60efe5-4983-474d-9afd-c2ce7f03b061",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "87a4b910-d60e-438c-bb2f-1baf5aa817d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "44ecd286-9353-48f6-8547-80e0f9171442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87a4b910-d60e-438c-bb2f-1baf5aa817d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac964322-b051-4816-a50b-9e5c944014fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87a4b910-d60e-438c-bb2f-1baf5aa817d5",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "bbf67b58-8788-4212-bb29-da5e303a36c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "a4b1033f-9f86-4055-a012-1665b7a4406b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbf67b58-8788-4212-bb29-da5e303a36c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da62a3b7-f088-430b-bbbc-5abd10df836a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf67b58-8788-4212-bb29-da5e303a36c8",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "317f2d24-fa4f-4c7d-9eb2-91b2790860f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "f97ebfb7-9221-4720-8539-552930c9ed8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "317f2d24-fa4f-4c7d-9eb2-91b2790860f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a34ff7d-666f-4f6e-9668-cb3d7175de96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "317f2d24-fa4f-4c7d-9eb2-91b2790860f1",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "26f7d3f8-cbcd-42d5-b9ab-c0cb2f9b3693",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "0689718f-54ca-428c-89a9-a635c22c0647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26f7d3f8-cbcd-42d5-b9ab-c0cb2f9b3693",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "816763bc-ca57-468c-ba5e-053a14abcd82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26f7d3f8-cbcd-42d5-b9ab-c0cb2f9b3693",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        },
        {
            "id": "97baf99e-8dec-4fc5-8b25-cf121f9629f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "compositeImage": {
                "id": "0f3d558d-30d8-46be-98e9-e2d3d56e7f22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97baf99e-8dec-4fc5-8b25-cf121f9629f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c6eb0cf-ee84-4fd6-9899-d3e3480e1ddf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97baf99e-8dec-4fc5-8b25-cf121f9629f3",
                    "LayerId": "f3015c88-16ca-479e-b042-c7dc56c27ec6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f3015c88-16ca-479e-b042-c7dc56c27ec6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23de7638-977a-49f5-b699-fb2305e9e6a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 15,
    "yorig": 17
}