{
    "id": "7349ed4c-8297-45d5-ac3c-2399940a8065",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship23boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 24,
    "bbox_right": 49,
    "bbox_top": -3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22e89ba3-9bf0-49f3-952a-90c30abf3381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7349ed4c-8297-45d5-ac3c-2399940a8065",
            "compositeImage": {
                "id": "d4d000e1-a3e6-4aee-a399-2f5d92c9b8ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e89ba3-9bf0-49f3-952a-90c30abf3381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d218140-d9ca-4b17-9325-fcc77e36465e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e89ba3-9bf0-49f3-952a-90c30abf3381",
                    "LayerId": "0d6b4cd3-dcc7-44e9-9a66-64f2cc968afd"
                }
            ]
        },
        {
            "id": "947a6f61-12b9-4e6b-915d-74b9f2131af1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7349ed4c-8297-45d5-ac3c-2399940a8065",
            "compositeImage": {
                "id": "0bd76e3b-593e-4f3f-956c-89d436ca17c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "947a6f61-12b9-4e6b-915d-74b9f2131af1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2415bf7c-ee2c-4403-b4e2-ae39ed248773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "947a6f61-12b9-4e6b-915d-74b9f2131af1",
                    "LayerId": "0d6b4cd3-dcc7-44e9-9a66-64f2cc968afd"
                }
            ]
        },
        {
            "id": "855af50c-f475-4b31-8671-0a98277146b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7349ed4c-8297-45d5-ac3c-2399940a8065",
            "compositeImage": {
                "id": "1aee9888-ede1-435e-aba8-255f625b181f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "855af50c-f475-4b31-8671-0a98277146b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f263439d-bd6d-43aa-8928-e31245879d43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "855af50c-f475-4b31-8671-0a98277146b0",
                    "LayerId": "0d6b4cd3-dcc7-44e9-9a66-64f2cc968afd"
                }
            ]
        },
        {
            "id": "67653c5b-05ca-468d-b4af-cdc165d9236b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7349ed4c-8297-45d5-ac3c-2399940a8065",
            "compositeImage": {
                "id": "b066edec-f4b9-473a-849c-cf4b6b3d2dfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67653c5b-05ca-468d-b4af-cdc165d9236b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f56fcc6e-ae7f-41e9-8f6e-a8a917d92db1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67653c5b-05ca-468d-b4af-cdc165d9236b",
                    "LayerId": "0d6b4cd3-dcc7-44e9-9a66-64f2cc968afd"
                }
            ]
        },
        {
            "id": "204b105e-ebaf-4f9c-b145-cbcbf6b995cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7349ed4c-8297-45d5-ac3c-2399940a8065",
            "compositeImage": {
                "id": "169a2a38-ba13-40a8-8fb4-435018b2c535",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "204b105e-ebaf-4f9c-b145-cbcbf6b995cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11481dab-aaa0-43ca-a2cc-863a9df110a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "204b105e-ebaf-4f9c-b145-cbcbf6b995cf",
                    "LayerId": "0d6b4cd3-dcc7-44e9-9a66-64f2cc968afd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "0d6b4cd3-dcc7-44e9-9a66-64f2cc968afd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7349ed4c-8297-45d5-ac3c-2399940a8065",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 37,
    "yorig": 32
}