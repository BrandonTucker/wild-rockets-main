{
    "id": "cec9c939-64bb-40b9-bbbf-666694104d12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange22buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 0,
    "bbox_right": 104,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0df268b-1ee6-4e30-a417-c8c110802c18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "compositeImage": {
                "id": "4bab4f69-d182-47d9-bc74-4b1cb4e5d278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0df268b-1ee6-4e30-a417-c8c110802c18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c124feb-d25c-447b-94e8-6278be0fa1a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0df268b-1ee6-4e30-a417-c8c110802c18",
                    "LayerId": "595b16e2-9398-4681-955e-63c25cbd7d6a"
                }
            ]
        },
        {
            "id": "37dafaac-db6c-47a0-a409-5c0682587045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "compositeImage": {
                "id": "d6fef30e-77cc-44f2-8cdb-178b677af747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37dafaac-db6c-47a0-a409-5c0682587045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51db2ba5-5236-4524-93bc-615e1dcc1609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37dafaac-db6c-47a0-a409-5c0682587045",
                    "LayerId": "595b16e2-9398-4681-955e-63c25cbd7d6a"
                }
            ]
        },
        {
            "id": "c13ae157-cdbf-4eb6-9586-1057ce6eccd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "compositeImage": {
                "id": "feb7f72d-fcff-41f5-806b-53293ba81896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c13ae157-cdbf-4eb6-9586-1057ce6eccd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a90afd3-69ca-41dd-9677-bdb8ebc4bea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c13ae157-cdbf-4eb6-9586-1057ce6eccd9",
                    "LayerId": "595b16e2-9398-4681-955e-63c25cbd7d6a"
                }
            ]
        },
        {
            "id": "69bafeb7-3afb-4fc2-89d4-fe397d3a1680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "compositeImage": {
                "id": "8bb654da-d8e2-4da1-9b15-3563c7dd5194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69bafeb7-3afb-4fc2-89d4-fe397d3a1680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a876d3-6432-469c-b628-8836047b7149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69bafeb7-3afb-4fc2-89d4-fe397d3a1680",
                    "LayerId": "595b16e2-9398-4681-955e-63c25cbd7d6a"
                }
            ]
        },
        {
            "id": "f2281461-3402-4c22-b5c8-17cc73285149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "compositeImage": {
                "id": "14966b08-c672-4a99-9664-21bb14f32d16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2281461-3402-4c22-b5c8-17cc73285149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31cb6339-a9ef-43ea-bf2a-504d0b1970ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2281461-3402-4c22-b5c8-17cc73285149",
                    "LayerId": "595b16e2-9398-4681-955e-63c25cbd7d6a"
                }
            ]
        },
        {
            "id": "8d6be0d1-7729-4993-975e-e617ab968eb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "compositeImage": {
                "id": "0e0e2bbf-f23d-4b2a-b234-bba6ea09224b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d6be0d1-7729-4993-975e-e617ab968eb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7ec90dd-ad48-41d0-8f70-d7cda6e7a792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d6be0d1-7729-4993-975e-e617ab968eb8",
                    "LayerId": "595b16e2-9398-4681-955e-63c25cbd7d6a"
                }
            ]
        },
        {
            "id": "cf939891-0ad5-4733-aeaf-5ff46b99fd08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "compositeImage": {
                "id": "e92f2208-9129-4d77-91a5-d48479d29778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf939891-0ad5-4733-aeaf-5ff46b99fd08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ca9f7ea-817a-4de6-a606-cc1f940ed1a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf939891-0ad5-4733-aeaf-5ff46b99fd08",
                    "LayerId": "595b16e2-9398-4681-955e-63c25cbd7d6a"
                }
            ]
        },
        {
            "id": "bc1fa70c-993e-4548-ae98-9805b39e2935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "compositeImage": {
                "id": "28c33a3b-ad41-42fb-88aa-f0173382161c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc1fa70c-993e-4548-ae98-9805b39e2935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eed56f4-09be-4826-92aa-b7f932762318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc1fa70c-993e-4548-ae98-9805b39e2935",
                    "LayerId": "595b16e2-9398-4681-955e-63c25cbd7d6a"
                }
            ]
        },
        {
            "id": "8a182c78-4c37-4a84-b8d6-a9e106276a37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "compositeImage": {
                "id": "5e20d1c9-9c27-41d4-a1a3-60cbbb610bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a182c78-4c37-4a84-b8d6-a9e106276a37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4dfab60-b159-418b-aeb4-cb25b2e3f959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a182c78-4c37-4a84-b8d6-a9e106276a37",
                    "LayerId": "595b16e2-9398-4681-955e-63c25cbd7d6a"
                }
            ]
        },
        {
            "id": "4b5485bd-eb60-4bdd-8f01-c8165e7075ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "compositeImage": {
                "id": "348a0a84-50b3-486c-991d-49a173342b20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b5485bd-eb60-4bdd-8f01-c8165e7075ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1c34748-da39-441b-8ac0-3ed21bf76c43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b5485bd-eb60-4bdd-8f01-c8165e7075ff",
                    "LayerId": "595b16e2-9398-4681-955e-63c25cbd7d6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "595b16e2-9398-4681-955e-63c25cbd7d6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cec9c939-64bb-40b9-bbbf-666694104d12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 53,
    "yorig": 52
}