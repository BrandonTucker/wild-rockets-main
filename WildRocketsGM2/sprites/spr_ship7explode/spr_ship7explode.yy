{
    "id": "a28f5a00-9c70-425a-a496-8e32a6c42e81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship7explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 27,
    "bbox_right": 87,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "345ddb0c-0eb6-4067-bc0e-c1f5c494d718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a28f5a00-9c70-425a-a496-8e32a6c42e81",
            "compositeImage": {
                "id": "e654cddd-dd9b-4403-beba-7c440636d4d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "345ddb0c-0eb6-4067-bc0e-c1f5c494d718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38f2b8f0-1423-4097-9024-12113a5d8153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "345ddb0c-0eb6-4067-bc0e-c1f5c494d718",
                    "LayerId": "6652f0ea-aa85-450b-9706-df63db0c6181"
                }
            ]
        },
        {
            "id": "4e6f4cd0-eee9-4e9c-95e6-4f93044d9d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a28f5a00-9c70-425a-a496-8e32a6c42e81",
            "compositeImage": {
                "id": "f84d699e-f576-4316-900d-ef1a0cb90b72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e6f4cd0-eee9-4e9c-95e6-4f93044d9d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3322fa61-fe0b-4b25-a6e4-26e58690baee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e6f4cd0-eee9-4e9c-95e6-4f93044d9d5d",
                    "LayerId": "6652f0ea-aa85-450b-9706-df63db0c6181"
                }
            ]
        },
        {
            "id": "90dc9e97-ed49-4c51-8658-713d9d253cf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a28f5a00-9c70-425a-a496-8e32a6c42e81",
            "compositeImage": {
                "id": "e665faa9-348a-43dd-aed1-ab792709a2ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90dc9e97-ed49-4c51-8658-713d9d253cf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30a37a83-3d7e-4599-81fb-3a717a2bf2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90dc9e97-ed49-4c51-8658-713d9d253cf3",
                    "LayerId": "6652f0ea-aa85-450b-9706-df63db0c6181"
                }
            ]
        },
        {
            "id": "8b652e46-79c0-4440-96df-fb293edb7f47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a28f5a00-9c70-425a-a496-8e32a6c42e81",
            "compositeImage": {
                "id": "69d87ca1-9884-4633-9f7a-78debb596f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b652e46-79c0-4440-96df-fb293edb7f47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e57883-f21a-43e5-ac0b-145898cdbe84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b652e46-79c0-4440-96df-fb293edb7f47",
                    "LayerId": "6652f0ea-aa85-450b-9706-df63db0c6181"
                }
            ]
        },
        {
            "id": "e2ebda7e-785b-4a0e-810b-b9a47dfe2e1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a28f5a00-9c70-425a-a496-8e32a6c42e81",
            "compositeImage": {
                "id": "c3737df7-6ba1-41c7-9b6d-7824313724c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2ebda7e-785b-4a0e-810b-b9a47dfe2e1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a82aba50-d657-471b-911f-499bc00074d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2ebda7e-785b-4a0e-810b-b9a47dfe2e1f",
                    "LayerId": "6652f0ea-aa85-450b-9706-df63db0c6181"
                }
            ]
        },
        {
            "id": "39cdda8a-9082-47c9-8c1b-af340fc8c142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a28f5a00-9c70-425a-a496-8e32a6c42e81",
            "compositeImage": {
                "id": "91b26b6c-6401-428e-bf68-d9fe6d1d722d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39cdda8a-9082-47c9-8c1b-af340fc8c142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d63a785-893f-43f2-9a93-a1111de58a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39cdda8a-9082-47c9-8c1b-af340fc8c142",
                    "LayerId": "6652f0ea-aa85-450b-9706-df63db0c6181"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 112,
    "layers": [
        {
            "id": "6652f0ea-aa85-450b-9706-df63db0c6181",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a28f5a00-9c70-425a-a496-8e32a6c42e81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 116,
    "xorig": 61,
    "yorig": 34
}