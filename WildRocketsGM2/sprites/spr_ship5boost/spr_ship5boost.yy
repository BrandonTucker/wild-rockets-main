{
    "id": "2b77c154-cf81-4323-9a44-edba02c57cdf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship5boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 28,
    "bbox_right": 53,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "198c1543-4c53-42e9-b8d1-747fcec546ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b77c154-cf81-4323-9a44-edba02c57cdf",
            "compositeImage": {
                "id": "72d77ef2-d1ae-43cf-ae10-592f78062f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "198c1543-4c53-42e9-b8d1-747fcec546ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dea7c778-f188-45a5-a1bc-f8c04819daa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "198c1543-4c53-42e9-b8d1-747fcec546ff",
                    "LayerId": "23f4c875-05bf-41f8-86e5-d6ed5ef26f4f"
                }
            ]
        },
        {
            "id": "b52cb2ea-00f0-4210-a142-6aa18fea084d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b77c154-cf81-4323-9a44-edba02c57cdf",
            "compositeImage": {
                "id": "4ef099ec-1fc9-4d5c-bd87-7d79f63ebdef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b52cb2ea-00f0-4210-a142-6aa18fea084d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e587912c-1ebb-4c84-ab46-c9bf3a19c2a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b52cb2ea-00f0-4210-a142-6aa18fea084d",
                    "LayerId": "23f4c875-05bf-41f8-86e5-d6ed5ef26f4f"
                }
            ]
        },
        {
            "id": "67a9a5e8-89e3-4ba0-97ec-3d36cb8a4197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b77c154-cf81-4323-9a44-edba02c57cdf",
            "compositeImage": {
                "id": "ace0750d-adeb-4800-9599-2bd5b4db3c30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a9a5e8-89e3-4ba0-97ec-3d36cb8a4197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "459eec09-fecc-40c5-a6b4-9ae1bd8b41f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a9a5e8-89e3-4ba0-97ec-3d36cb8a4197",
                    "LayerId": "23f4c875-05bf-41f8-86e5-d6ed5ef26f4f"
                }
            ]
        },
        {
            "id": "3bed974a-8049-4cd0-8d6e-844149af9757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b77c154-cf81-4323-9a44-edba02c57cdf",
            "compositeImage": {
                "id": "facb3c5e-f867-4632-874f-bd3e87ebb105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bed974a-8049-4cd0-8d6e-844149af9757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78ccb4a5-84e7-4dff-a835-69fd1c486a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bed974a-8049-4cd0-8d6e-844149af9757",
                    "LayerId": "23f4c875-05bf-41f8-86e5-d6ed5ef26f4f"
                }
            ]
        },
        {
            "id": "e7cd40a4-4578-4a0d-96dc-2785e6ec56c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b77c154-cf81-4323-9a44-edba02c57cdf",
            "compositeImage": {
                "id": "eddb658b-ae68-4b03-a935-b003c240cf26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7cd40a4-4578-4a0d-96dc-2785e6ec56c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58eb36ff-f585-4ffc-b496-b1fdd6834342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7cd40a4-4578-4a0d-96dc-2785e6ec56c2",
                    "LayerId": "23f4c875-05bf-41f8-86e5-d6ed5ef26f4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "23f4c875-05bf-41f8-86e5-d6ed5ef26f4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b77c154-cf81-4323-9a44-edba02c57cdf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 46
}