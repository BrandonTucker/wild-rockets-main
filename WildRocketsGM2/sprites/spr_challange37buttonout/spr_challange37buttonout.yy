{
    "id": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange37buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bab62f6-359a-42b6-8d12-b2ed0d8943fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "compositeImage": {
                "id": "46bb9111-d544-424b-b3f8-7b38a2c70705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bab62f6-359a-42b6-8d12-b2ed0d8943fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "746eec8e-5a2d-4c37-9111-11b63cce124d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bab62f6-359a-42b6-8d12-b2ed0d8943fa",
                    "LayerId": "fea50ca7-c86b-463a-94d3-f009095a3e4d"
                }
            ]
        },
        {
            "id": "cf1292a9-11bd-4a27-94ad-f18c28ffded6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "compositeImage": {
                "id": "842a401c-eee2-4204-bef0-4df13ef8264d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf1292a9-11bd-4a27-94ad-f18c28ffded6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b52afb14-d399-418d-af4e-fef6628a1679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf1292a9-11bd-4a27-94ad-f18c28ffded6",
                    "LayerId": "fea50ca7-c86b-463a-94d3-f009095a3e4d"
                }
            ]
        },
        {
            "id": "8558766f-d12e-456e-8ece-6c0a7e06a5a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "compositeImage": {
                "id": "48b376f4-d0b8-4507-b6f8-ec2f6a284c0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8558766f-d12e-456e-8ece-6c0a7e06a5a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a544cf-6028-4342-aba2-bce169dbb3ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8558766f-d12e-456e-8ece-6c0a7e06a5a3",
                    "LayerId": "fea50ca7-c86b-463a-94d3-f009095a3e4d"
                }
            ]
        },
        {
            "id": "bebeb5ed-c5d8-43d0-b53f-3c9c2fab623a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "compositeImage": {
                "id": "0efbd309-1cab-4d9a-885e-34c7a85611dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bebeb5ed-c5d8-43d0-b53f-3c9c2fab623a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcca432a-3fd7-41b1-91e4-5bd62f394dbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bebeb5ed-c5d8-43d0-b53f-3c9c2fab623a",
                    "LayerId": "fea50ca7-c86b-463a-94d3-f009095a3e4d"
                }
            ]
        },
        {
            "id": "8f0dbc6e-5981-45c5-ab06-8fe379a97074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "compositeImage": {
                "id": "5dc43722-3234-4a23-a7a7-8d64df3545ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f0dbc6e-5981-45c5-ab06-8fe379a97074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d468ca-dc8c-4943-b9a5-55341ce0bc84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f0dbc6e-5981-45c5-ab06-8fe379a97074",
                    "LayerId": "fea50ca7-c86b-463a-94d3-f009095a3e4d"
                }
            ]
        },
        {
            "id": "2c18a082-85f3-4304-bd41-66856de3c342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "compositeImage": {
                "id": "0da21c31-d118-403a-a10b-8756bfaa13b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c18a082-85f3-4304-bd41-66856de3c342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3ca2962-3d3c-4ef0-9d07-0b802f9959e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c18a082-85f3-4304-bd41-66856de3c342",
                    "LayerId": "fea50ca7-c86b-463a-94d3-f009095a3e4d"
                }
            ]
        },
        {
            "id": "b9ac4cd5-3219-4159-aac9-fff3a2a5d027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "compositeImage": {
                "id": "7b3b951d-aca1-4cbd-b7dd-3a67a71f63d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9ac4cd5-3219-4159-aac9-fff3a2a5d027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0122ff10-6ec9-4c53-bc38-07caedf6cd2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ac4cd5-3219-4159-aac9-fff3a2a5d027",
                    "LayerId": "fea50ca7-c86b-463a-94d3-f009095a3e4d"
                }
            ]
        },
        {
            "id": "a914b158-78e9-4b3c-a839-0779367e91ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "compositeImage": {
                "id": "f153881f-834f-4e67-bd75-234471da8afc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a914b158-78e9-4b3c-a839-0779367e91ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f0091e8-ab12-4e7b-9ab4-e688d698fa57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a914b158-78e9-4b3c-a839-0779367e91ad",
                    "LayerId": "fea50ca7-c86b-463a-94d3-f009095a3e4d"
                }
            ]
        },
        {
            "id": "36f2883a-4421-4c7f-a798-c78e2b2994e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "compositeImage": {
                "id": "9ae7e369-c228-4588-805a-13b4a7dc9f43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36f2883a-4421-4c7f-a798-c78e2b2994e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "782f3e36-d687-4ec3-acf9-521c5b60b2f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36f2883a-4421-4c7f-a798-c78e2b2994e4",
                    "LayerId": "fea50ca7-c86b-463a-94d3-f009095a3e4d"
                }
            ]
        },
        {
            "id": "ecf81fd1-6198-409c-8f9b-53dff0dd67f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "compositeImage": {
                "id": "a8cad505-7c2e-4669-9dc9-6a99148d9354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf81fd1-6198-409c-8f9b-53dff0dd67f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbbc2449-1f0e-4d0b-989b-6aef8954c206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf81fd1-6198-409c-8f9b-53dff0dd67f2",
                    "LayerId": "fea50ca7-c86b-463a-94d3-f009095a3e4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "fea50ca7-c86b-463a-94d3-f009095a3e4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6bf31b12-7fb7-4db0-86bf-e453134c1cdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 52
}