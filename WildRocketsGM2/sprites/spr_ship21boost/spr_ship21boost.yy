{
    "id": "ea63632c-6364-49c0-ad55-4a380689eb3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship21boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 35,
    "bbox_right": 60,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18a77d1d-4986-4917-8493-1c78b6dd03cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea63632c-6364-49c0-ad55-4a380689eb3d",
            "compositeImage": {
                "id": "7c5f7d25-9522-4451-bba2-43edae9fb3c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18a77d1d-4986-4917-8493-1c78b6dd03cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257e4fd7-7056-4de1-99a6-859229786a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18a77d1d-4986-4917-8493-1c78b6dd03cd",
                    "LayerId": "8eb1c9d2-b7c7-443e-973c-5ac894e1cbd3"
                }
            ]
        },
        {
            "id": "f85547c7-c8d6-4527-a489-8be0d9c7a842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea63632c-6364-49c0-ad55-4a380689eb3d",
            "compositeImage": {
                "id": "1db1013a-cd04-4c8d-b10c-3e9077686278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f85547c7-c8d6-4527-a489-8be0d9c7a842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e2b677e-a825-4016-bcfb-7c4568837abc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f85547c7-c8d6-4527-a489-8be0d9c7a842",
                    "LayerId": "8eb1c9d2-b7c7-443e-973c-5ac894e1cbd3"
                }
            ]
        },
        {
            "id": "70c03e93-71f7-4603-bcbb-e6c54c1b6fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea63632c-6364-49c0-ad55-4a380689eb3d",
            "compositeImage": {
                "id": "e93b9ecb-b5ad-4689-89fd-c26c23222dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70c03e93-71f7-4603-bcbb-e6c54c1b6fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2e6bc55-0516-4f61-b7e5-a2f7b93a7fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70c03e93-71f7-4603-bcbb-e6c54c1b6fac",
                    "LayerId": "8eb1c9d2-b7c7-443e-973c-5ac894e1cbd3"
                }
            ]
        },
        {
            "id": "a28b1909-75ca-4c5f-956b-76c99abcebcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea63632c-6364-49c0-ad55-4a380689eb3d",
            "compositeImage": {
                "id": "25030d5e-e5ff-417d-b65a-efe92480555f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a28b1909-75ca-4c5f-956b-76c99abcebcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a16db23-a8fc-4996-a57e-6ad510907cc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a28b1909-75ca-4c5f-956b-76c99abcebcd",
                    "LayerId": "8eb1c9d2-b7c7-443e-973c-5ac894e1cbd3"
                }
            ]
        },
        {
            "id": "dfdc804a-1c8f-451a-a84e-5464271732ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea63632c-6364-49c0-ad55-4a380689eb3d",
            "compositeImage": {
                "id": "d0288045-a7e1-4299-8cf8-d7829cbd2e2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfdc804a-1c8f-451a-a84e-5464271732ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a9e8b8b-1036-4808-b508-bab7731a8cec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfdc804a-1c8f-451a-a84e-5464271732ec",
                    "LayerId": "8eb1c9d2-b7c7-443e-973c-5ac894e1cbd3"
                }
            ]
        },
        {
            "id": "4da83a34-18b4-4898-90ee-f3922483c43a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea63632c-6364-49c0-ad55-4a380689eb3d",
            "compositeImage": {
                "id": "b61b2d23-1853-4228-bcee-0e5794cbaa5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4da83a34-18b4-4898-90ee-f3922483c43a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62966060-e463-49a6-b49a-31c19c8343c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4da83a34-18b4-4898-90ee-f3922483c43a",
                    "LayerId": "8eb1c9d2-b7c7-443e-973c-5ac894e1cbd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 131,
    "layers": [
        {
            "id": "8eb1c9d2-b7c7-443e-973c-5ac894e1cbd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea63632c-6364-49c0-ad55-4a380689eb3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 97,
    "xorig": 44,
    "yorig": 49
}