{
    "id": "8290bc02-7230-49e0-a104-c1aab4807647",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship39explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92fca6b8-2955-4bc6-b444-002aad7f317f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8290bc02-7230-49e0-a104-c1aab4807647",
            "compositeImage": {
                "id": "c9b86236-3622-474b-9d89-bb02c0f13ee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92fca6b8-2955-4bc6-b444-002aad7f317f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0415d493-9967-4ff3-bcaa-5a4d4fc0c3b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92fca6b8-2955-4bc6-b444-002aad7f317f",
                    "LayerId": "dc20b9b2-0449-40fe-bcce-39ea2b551013"
                }
            ]
        },
        {
            "id": "cfc0a7a5-714c-49a0-aaee-30d5506dafb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8290bc02-7230-49e0-a104-c1aab4807647",
            "compositeImage": {
                "id": "ba5543a4-6bfd-4cee-9bb2-8dea7e82d278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfc0a7a5-714c-49a0-aaee-30d5506dafb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5274cf56-1200-4e93-90d6-82d45578a914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfc0a7a5-714c-49a0-aaee-30d5506dafb7",
                    "LayerId": "dc20b9b2-0449-40fe-bcce-39ea2b551013"
                }
            ]
        },
        {
            "id": "59cd8801-6efb-498f-be4e-29ad0848863a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8290bc02-7230-49e0-a104-c1aab4807647",
            "compositeImage": {
                "id": "d2e8bb34-c76e-4cd6-a478-4410fda8df4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59cd8801-6efb-498f-be4e-29ad0848863a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d792593-07dd-4737-bce4-94c2cf2ff1e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59cd8801-6efb-498f-be4e-29ad0848863a",
                    "LayerId": "dc20b9b2-0449-40fe-bcce-39ea2b551013"
                }
            ]
        },
        {
            "id": "06c021c8-ac90-4ff7-bcf1-89e87c885b68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8290bc02-7230-49e0-a104-c1aab4807647",
            "compositeImage": {
                "id": "38891136-4472-4aac-9ad0-3dfe2700a93f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06c021c8-ac90-4ff7-bcf1-89e87c885b68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ab44e49-e608-4131-bc37-f3317b15d126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06c021c8-ac90-4ff7-bcf1-89e87c885b68",
                    "LayerId": "dc20b9b2-0449-40fe-bcce-39ea2b551013"
                }
            ]
        },
        {
            "id": "387ee9a4-0a71-42bc-a2a8-5dd58cc97562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8290bc02-7230-49e0-a104-c1aab4807647",
            "compositeImage": {
                "id": "c587a75c-423a-4770-9ac6-96ca9af8020c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "387ee9a4-0a71-42bc-a2a8-5dd58cc97562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a73ac81e-eb37-46e2-8bb7-30c0a9b78958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "387ee9a4-0a71-42bc-a2a8-5dd58cc97562",
                    "LayerId": "dc20b9b2-0449-40fe-bcce-39ea2b551013"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 254,
    "layers": [
        {
            "id": "dc20b9b2-0449-40fe-bcce-39ea2b551013",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8290bc02-7230-49e0-a104-c1aab4807647",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 149,
    "xorig": 74,
    "yorig": 115
}