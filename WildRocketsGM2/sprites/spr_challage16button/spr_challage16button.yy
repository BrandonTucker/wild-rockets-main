{
    "id": "e449ee5f-33dc-4377-9b06-2e4c0bd23a6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage16button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abeacd07-5428-43af-883a-b21be6efcdc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e449ee5f-33dc-4377-9b06-2e4c0bd23a6c",
            "compositeImage": {
                "id": "56f80ce6-e36e-408c-af06-3b7e4481c49b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abeacd07-5428-43af-883a-b21be6efcdc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6699aa9-cb9c-477f-a637-b7bdc76e7ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abeacd07-5428-43af-883a-b21be6efcdc7",
                    "LayerId": "985966ff-03f4-4f68-ada7-8157673aa3c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "985966ff-03f4-4f68-ada7-8157673aa3c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e449ee5f-33dc-4377-9b06-2e4c0bd23a6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}