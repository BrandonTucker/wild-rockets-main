{
    "id": "4dfa77b8-e32b-4182-9e36-6e24da8d63d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship28",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1e925bd-9ccd-482c-b626-b4540db631dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dfa77b8-e32b-4182-9e36-6e24da8d63d2",
            "compositeImage": {
                "id": "76fee15e-9606-4c24-9d37-bc3424bd51b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1e925bd-9ccd-482c-b626-b4540db631dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98b77ddb-7db0-4f39-9566-24265dcf0d53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1e925bd-9ccd-482c-b626-b4540db631dd",
                    "LayerId": "6f208c71-edc6-4e12-a511-fd38aeb179ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "6f208c71-edc6-4e12-a511-fd38aeb179ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4dfa77b8-e32b-4182-9e36-6e24da8d63d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 50
}