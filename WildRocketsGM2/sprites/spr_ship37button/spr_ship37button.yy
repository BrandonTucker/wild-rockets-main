{
    "id": "bf5a0591-b61b-4be2-ae56-f02adb1ea4f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship37button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ade01879-b538-4130-81b7-a5bb571fecdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5a0591-b61b-4be2-ae56-f02adb1ea4f5",
            "compositeImage": {
                "id": "23f341cd-5983-43e2-8334-e55d7c645a70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ade01879-b538-4130-81b7-a5bb571fecdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f21f8254-dee9-4381-8760-08c92122b802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ade01879-b538-4130-81b7-a5bb571fecdc",
                    "LayerId": "6e9b947a-c60b-4b41-9ac2-e394f416da52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6e9b947a-c60b-4b41-9ac2-e394f416da52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf5a0591-b61b-4be2-ae56-f02adb1ea4f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}