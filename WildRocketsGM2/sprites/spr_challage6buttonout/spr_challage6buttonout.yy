{
    "id": "019245af-05b1-4f61-9379-09895dc7f083",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage6buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 2,
    "bbox_right": 108,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfed8ed5-538f-46b6-bf06-0e659955afe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "compositeImage": {
                "id": "35a38d32-b5b2-4ad1-b5be-c236fc303f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfed8ed5-538f-46b6-bf06-0e659955afe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b8fdcd-55f1-43eb-be07-d616729a50b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfed8ed5-538f-46b6-bf06-0e659955afe2",
                    "LayerId": "ff15582f-69dc-4d10-b5c4-1c962a9260ef"
                }
            ]
        },
        {
            "id": "0772ff98-187e-4897-a69e-070aa7d707e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "compositeImage": {
                "id": "6dcecf2f-3e70-45e7-af42-0ec290e27637",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0772ff98-187e-4897-a69e-070aa7d707e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf9eb4de-3299-4043-86cb-94aaa98bd275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0772ff98-187e-4897-a69e-070aa7d707e4",
                    "LayerId": "ff15582f-69dc-4d10-b5c4-1c962a9260ef"
                }
            ]
        },
        {
            "id": "05d3c6bc-9598-482a-99b6-b9df7354d6fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "compositeImage": {
                "id": "d968af6d-3e5a-449f-86c3-fdd0b84b6da7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05d3c6bc-9598-482a-99b6-b9df7354d6fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1e63eed-1e71-49c3-b729-0452adb99c84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05d3c6bc-9598-482a-99b6-b9df7354d6fe",
                    "LayerId": "ff15582f-69dc-4d10-b5c4-1c962a9260ef"
                }
            ]
        },
        {
            "id": "a8edde27-3994-45cb-baa2-2849b0e3e24e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "compositeImage": {
                "id": "e341c2e0-6415-480d-a319-66162bda0c99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8edde27-3994-45cb-baa2-2849b0e3e24e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a96048dd-37ba-448a-b909-ddb0695a7063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8edde27-3994-45cb-baa2-2849b0e3e24e",
                    "LayerId": "ff15582f-69dc-4d10-b5c4-1c962a9260ef"
                }
            ]
        },
        {
            "id": "1d352251-a887-4470-9411-03d0cc12ad9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "compositeImage": {
                "id": "97c47a51-06ec-4703-877b-8a33b4b63933",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d352251-a887-4470-9411-03d0cc12ad9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ea605f2-9833-4d40-9705-14c3d17fa033",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d352251-a887-4470-9411-03d0cc12ad9c",
                    "LayerId": "ff15582f-69dc-4d10-b5c4-1c962a9260ef"
                }
            ]
        },
        {
            "id": "5dc60c4a-6545-468f-82c8-e5597a1b757b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "compositeImage": {
                "id": "4e4ab4a0-18bc-4d39-a7f2-3af6c5b98477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dc60c4a-6545-468f-82c8-e5597a1b757b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b4b015-2198-4422-a564-dc5955fec125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dc60c4a-6545-468f-82c8-e5597a1b757b",
                    "LayerId": "ff15582f-69dc-4d10-b5c4-1c962a9260ef"
                }
            ]
        },
        {
            "id": "04635230-7b69-41b0-a3df-3a2f1d3c185f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "compositeImage": {
                "id": "fb57d06f-b291-4608-bdaa-d92f7a2c9c0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04635230-7b69-41b0-a3df-3a2f1d3c185f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f26f7980-8f88-4a90-8fb6-8dbbf439f304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04635230-7b69-41b0-a3df-3a2f1d3c185f",
                    "LayerId": "ff15582f-69dc-4d10-b5c4-1c962a9260ef"
                }
            ]
        },
        {
            "id": "99fb54ff-0cf6-405a-8bb1-0b629b0c6245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "compositeImage": {
                "id": "9af01930-b110-465e-82cc-40bb91e76655",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99fb54ff-0cf6-405a-8bb1-0b629b0c6245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3508f0a7-dfaf-490c-aab1-45dae30e228f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99fb54ff-0cf6-405a-8bb1-0b629b0c6245",
                    "LayerId": "ff15582f-69dc-4d10-b5c4-1c962a9260ef"
                }
            ]
        },
        {
            "id": "2ffcab3a-287c-4621-bd8a-3d1233d0f115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "compositeImage": {
                "id": "9b27e410-7a65-49d6-a683-23b6b688886f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ffcab3a-287c-4621-bd8a-3d1233d0f115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ffbf771-d417-401d-8572-742b393fb2f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ffcab3a-287c-4621-bd8a-3d1233d0f115",
                    "LayerId": "ff15582f-69dc-4d10-b5c4-1c962a9260ef"
                }
            ]
        },
        {
            "id": "cb079e2b-e8e4-4144-8417-cbe1c6fbe84c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "compositeImage": {
                "id": "ab951bcb-8209-42dd-bb05-ea5cf7111777",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb079e2b-e8e4-4144-8417-cbe1c6fbe84c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eec5eb1-979a-4c13-83a3-a50e9b2dfd5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb079e2b-e8e4-4144-8417-cbe1c6fbe84c",
                    "LayerId": "ff15582f-69dc-4d10-b5c4-1c962a9260ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "ff15582f-69dc-4d10-b5c4-1c962a9260ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "019245af-05b1-4f61-9379-09895dc7f083",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 50,
    "yorig": 48
}