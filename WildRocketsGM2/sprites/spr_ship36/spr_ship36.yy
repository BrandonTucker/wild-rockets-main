{
    "id": "9c340372-9f69-46c9-873c-708bde14b93e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship36",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 140,
    "bbox_left": 48,
    "bbox_right": 73,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40f8ffd6-755b-40da-bf46-7ec4c6e04ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c340372-9f69-46c9-873c-708bde14b93e",
            "compositeImage": {
                "id": "ab7a16d8-9541-4192-ab82-ff6d44952d3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40f8ffd6-755b-40da-bf46-7ec4c6e04ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b7dea01-aa83-40d4-a7b1-224b072d569a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40f8ffd6-755b-40da-bf46-7ec4c6e04ec1",
                    "LayerId": "d4981d4c-f29c-445a-b0ed-aa3b93012712"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "d4981d4c-f29c-445a-b0ed-aa3b93012712",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c340372-9f69-46c9-873c-708bde14b93e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 61,
    "yorig": 93
}