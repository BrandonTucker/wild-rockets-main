{
    "id": "6285b8aa-5b7a-4533-bfaf-1fa06ad6bc85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship24boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 41,
    "bbox_right": 66,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98cc91b2-948e-48fa-b739-4fff049fe981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6285b8aa-5b7a-4533-bfaf-1fa06ad6bc85",
            "compositeImage": {
                "id": "3060a7a1-c316-47ab-a2e5-a6b20f2b2d80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98cc91b2-948e-48fa-b739-4fff049fe981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed5bb0c-d2a4-43e2-8e4e-b68bb1aba7eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98cc91b2-948e-48fa-b739-4fff049fe981",
                    "LayerId": "1862fab9-70da-4202-9387-5293725745b1"
                }
            ]
        },
        {
            "id": "3e2a19fa-4d37-44dc-af0b-972f7243dfb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6285b8aa-5b7a-4533-bfaf-1fa06ad6bc85",
            "compositeImage": {
                "id": "9a3aaed4-24db-45f0-87fe-1eb23a4603c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e2a19fa-4d37-44dc-af0b-972f7243dfb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efc7e28a-199d-4676-bf88-99806fc8f717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e2a19fa-4d37-44dc-af0b-972f7243dfb0",
                    "LayerId": "1862fab9-70da-4202-9387-5293725745b1"
                }
            ]
        },
        {
            "id": "e8dc7733-de9a-4576-be68-a5e7a9f73ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6285b8aa-5b7a-4533-bfaf-1fa06ad6bc85",
            "compositeImage": {
                "id": "086ec28d-d378-4597-a9ad-0d522f9bf410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8dc7733-de9a-4576-be68-a5e7a9f73ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "451934af-c4c2-4917-a012-095091858273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8dc7733-de9a-4576-be68-a5e7a9f73ae3",
                    "LayerId": "1862fab9-70da-4202-9387-5293725745b1"
                }
            ]
        },
        {
            "id": "5b8199e6-07bc-415d-81b7-7f102b725efc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6285b8aa-5b7a-4533-bfaf-1fa06ad6bc85",
            "compositeImage": {
                "id": "652a03eb-195e-42c6-9b48-80b320f1ec08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b8199e6-07bc-415d-81b7-7f102b725efc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56854546-5c2a-4a6e-955e-fb01e7d7fdea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b8199e6-07bc-415d-81b7-7f102b725efc",
                    "LayerId": "1862fab9-70da-4202-9387-5293725745b1"
                }
            ]
        },
        {
            "id": "16350a58-96ae-4445-8f34-5a89d9cf86ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6285b8aa-5b7a-4533-bfaf-1fa06ad6bc85",
            "compositeImage": {
                "id": "d7b99878-2f2d-4228-aa09-79ac682f3a62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16350a58-96ae-4445-8f34-5a89d9cf86ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf4d7bf-c1f4-4947-9c16-434ff7375c62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16350a58-96ae-4445-8f34-5a89d9cf86ea",
                    "LayerId": "1862fab9-70da-4202-9387-5293725745b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 173,
    "layers": [
        {
            "id": "1862fab9-70da-4202-9387-5293725745b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6285b8aa-5b7a-4533-bfaf-1fa06ad6bc85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 64
}