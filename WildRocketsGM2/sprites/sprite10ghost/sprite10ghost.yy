{
    "id": "8b24c17f-9c8a-4714-a775-095171399af9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite10ghost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 3,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61422470-1a59-4fff-999d-67136f4a42db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b24c17f-9c8a-4714-a775-095171399af9",
            "compositeImage": {
                "id": "23444088-4abb-4123-a609-77365d65f921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61422470-1a59-4fff-999d-67136f4a42db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "529ac7f2-fbce-4836-acee-e34679ff8e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61422470-1a59-4fff-999d-67136f4a42db",
                    "LayerId": "ef97e839-e928-4f94-ae13-9f41d596a55c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ef97e839-e928-4f94-ae13-9f41d596a55c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b24c17f-9c8a-4714-a775-095171399af9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}