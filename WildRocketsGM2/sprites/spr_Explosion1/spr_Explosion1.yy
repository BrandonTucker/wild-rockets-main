{
    "id": "ae8491ff-0296-4f0f-b9ec-58c7b1133798",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Explosion1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b6f1098-b3d4-4689-85cd-d81b7940e703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae8491ff-0296-4f0f-b9ec-58c7b1133798",
            "compositeImage": {
                "id": "468ae8a0-5c97-4c64-a9df-ccd6334e79ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b6f1098-b3d4-4689-85cd-d81b7940e703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "778dbb94-7465-4667-91f1-bf033998d2f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b6f1098-b3d4-4689-85cd-d81b7940e703",
                    "LayerId": "11be5cb4-15fb-4994-91fd-97bd81bcfbc8"
                }
            ]
        },
        {
            "id": "4f34a449-aa6b-46a7-8e1b-3e572392e5ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae8491ff-0296-4f0f-b9ec-58c7b1133798",
            "compositeImage": {
                "id": "2ab363c0-5a05-4481-8949-ef6a4ebfec15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f34a449-aa6b-46a7-8e1b-3e572392e5ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da09f948-3add-47d8-889c-2b5c0ad9e76f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f34a449-aa6b-46a7-8e1b-3e572392e5ca",
                    "LayerId": "11be5cb4-15fb-4994-91fd-97bd81bcfbc8"
                }
            ]
        },
        {
            "id": "897b4499-5615-44f2-8143-f71ad6cc568d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae8491ff-0296-4f0f-b9ec-58c7b1133798",
            "compositeImage": {
                "id": "e43a4ba8-8501-4597-bfe0-bdb60d419adc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "897b4499-5615-44f2-8143-f71ad6cc568d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e42e7fbe-0031-4bd3-8965-37158291382b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "897b4499-5615-44f2-8143-f71ad6cc568d",
                    "LayerId": "11be5cb4-15fb-4994-91fd-97bd81bcfbc8"
                }
            ]
        },
        {
            "id": "a7492767-3348-4fc6-86b5-5e6a8f9c861a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae8491ff-0296-4f0f-b9ec-58c7b1133798",
            "compositeImage": {
                "id": "8e7361c1-25b4-4314-bbe8-835b1dd66ece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7492767-3348-4fc6-86b5-5e6a8f9c861a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "402b9381-2675-45d1-ad3d-7dc98c49d140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7492767-3348-4fc6-86b5-5e6a8f9c861a",
                    "LayerId": "11be5cb4-15fb-4994-91fd-97bd81bcfbc8"
                }
            ]
        },
        {
            "id": "d9d2304b-5b92-4fb6-8113-ad52321140de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae8491ff-0296-4f0f-b9ec-58c7b1133798",
            "compositeImage": {
                "id": "b451d9ab-fb46-4add-8aa4-2022117ddc7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d2304b-5b92-4fb6-8113-ad52321140de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e13a6f57-b76e-4f43-bd8b-7062f3bd875c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d2304b-5b92-4fb6-8113-ad52321140de",
                    "LayerId": "11be5cb4-15fb-4994-91fd-97bd81bcfbc8"
                }
            ]
        },
        {
            "id": "ea158073-fd10-4d00-8876-41971a894235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae8491ff-0296-4f0f-b9ec-58c7b1133798",
            "compositeImage": {
                "id": "ff1ca7d6-dde3-4944-a857-240f68d91bb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea158073-fd10-4d00-8876-41971a894235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88f05435-a121-4707-a5a6-f92926bb75cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea158073-fd10-4d00-8876-41971a894235",
                    "LayerId": "11be5cb4-15fb-4994-91fd-97bd81bcfbc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "11be5cb4-15fb-4994-91fd-97bd81bcfbc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae8491ff-0296-4f0f-b9ec-58c7b1133798",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}