{
    "id": "f05a2e54-2cdb-4e34-aac2-48f6fbb8dd93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship9locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fe29074-451f-4345-a1a5-270608fd7fc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f05a2e54-2cdb-4e34-aac2-48f6fbb8dd93",
            "compositeImage": {
                "id": "eceffb32-f80b-4297-9873-24d69404dc12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe29074-451f-4345-a1a5-270608fd7fc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40883677-26cd-4a70-8c12-9e1368a0a683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe29074-451f-4345-a1a5-270608fd7fc2",
                    "LayerId": "0cab502f-c132-4f0f-8a85-4879f1763d14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "0cab502f-c132-4f0f-8a85-4879f1763d14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f05a2e54-2cdb-4e34-aac2-48f6fbb8dd93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}