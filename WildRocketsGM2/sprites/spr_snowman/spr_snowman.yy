{
    "id": "56268842-a00b-4e6f-bb66-ebd421685b7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_snowman",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 34,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecb56e0c-cd96-4830-a58b-81d5644405c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "5a0f1e92-42d0-49ec-9602-33d3272e2d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecb56e0c-cd96-4830-a58b-81d5644405c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32323cdc-ec7f-43ea-af81-93a4c5f531c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecb56e0c-cd96-4830-a58b-81d5644405c6",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "1e3d8f5c-9e7f-4b94-a5b7-33303713a0fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "abfe1e18-deb5-4bbe-9341-67d2f5e80781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e3d8f5c-9e7f-4b94-a5b7-33303713a0fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd98fb07-8f73-4176-8f58-0052d2f6cb73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e3d8f5c-9e7f-4b94-a5b7-33303713a0fd",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "11a6e83a-e9aa-45aa-82b7-7093d5e3a1c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "0c7cc5bc-33e1-42df-a0f8-f8db7d8b1d96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11a6e83a-e9aa-45aa-82b7-7093d5e3a1c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3c29354-b0de-423e-9472-58dac9b9f4b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11a6e83a-e9aa-45aa-82b7-7093d5e3a1c8",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "9a67d3ec-2b74-4e28-811f-ec7bf4dfcfb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "71f4001c-63c1-4505-a433-051f0cd9af2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a67d3ec-2b74-4e28-811f-ec7bf4dfcfb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca4f5436-9d68-46c3-84d2-8a9ceb6199c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a67d3ec-2b74-4e28-811f-ec7bf4dfcfb1",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "bc6ab2e2-e6bb-402a-ae08-1bec20633bc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "03ef0a5b-949a-4a0a-8f1b-975df1063712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc6ab2e2-e6bb-402a-ae08-1bec20633bc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0bd23ab-1905-4272-a417-b395ac4c0dec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc6ab2e2-e6bb-402a-ae08-1bec20633bc6",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "d1e19375-cfb0-4eec-9cae-3b025d1d67a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "f7581162-3d2e-4861-ae63-b3989dca0098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1e19375-cfb0-4eec-9cae-3b025d1d67a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aac445a-1d18-4322-b5ed-e0a3814bd041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e19375-cfb0-4eec-9cae-3b025d1d67a8",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "fcf2ff5e-33cc-472b-b642-f776ff47a657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "62f8b309-753e-4c09-a3b4-552cfcd3c65a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf2ff5e-33cc-472b-b642-f776ff47a657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f45826d-8744-46e1-b652-d896797f5fc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf2ff5e-33cc-472b-b642-f776ff47a657",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "a8b8714f-8104-4bea-82e5-0d48617588cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "7f74187f-1d5b-4622-8f4c-f2ac61997705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8b8714f-8104-4bea-82e5-0d48617588cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a36a241-ba6c-454c-9a29-7ac529de6196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8b8714f-8104-4bea-82e5-0d48617588cd",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "78de5a14-6bae-4630-afb7-bd0b8a2a0589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "3602dcb4-55ee-428b-a684-59aa8381ee5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78de5a14-6bae-4630-afb7-bd0b8a2a0589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bc2225b-c437-48cd-8c5c-269a7a01b254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78de5a14-6bae-4630-afb7-bd0b8a2a0589",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "cfaa2bf3-f90d-4b6f-9256-2dd678048ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "a2eff0c9-90b7-488a-a5f2-fac50a11c2b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfaa2bf3-f90d-4b6f-9256-2dd678048ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea250128-3f96-4959-a0a5-2ce492441464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfaa2bf3-f90d-4b6f-9256-2dd678048ca9",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "8b198d52-af23-491d-a9c7-1f6eda561171",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "09037c5d-baa2-4f2e-b758-40fb30af5355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b198d52-af23-491d-a9c7-1f6eda561171",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aedc1d96-ab35-4029-a705-c6f9903c5076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b198d52-af23-491d-a9c7-1f6eda561171",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "5a2e773e-b6fa-48f0-b304-8db54f9cd8c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "7b4bc5fe-2e00-4b2c-9650-041e9e23258c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a2e773e-b6fa-48f0-b304-8db54f9cd8c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d2409fa-d001-4a68-9c45-8d5a7dcc1a15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a2e773e-b6fa-48f0-b304-8db54f9cd8c5",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "9809ca61-61b8-4f80-85a2-85c4c363cf16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "544abed2-a6c2-46b2-bb29-c56986c6274b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9809ca61-61b8-4f80-85a2-85c4c363cf16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f74eb28c-3420-4917-9087-504c14b73ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9809ca61-61b8-4f80-85a2-85c4c363cf16",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "1cc42b69-c6cf-4230-86d2-a9e48a5b0936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "af64690e-f74f-4e3c-9e77-48f127cbdda6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cc42b69-c6cf-4230-86d2-a9e48a5b0936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca850aee-9204-405d-b3a1-1c4a7d7b446a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cc42b69-c6cf-4230-86d2-a9e48a5b0936",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        },
        {
            "id": "87957e46-bd23-44db-879a-74ae14e0f519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "compositeImage": {
                "id": "7654b352-2b0b-4136-92d1-c0b2b8cd3b97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87957e46-bd23-44db-879a-74ae14e0f519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba4828d6-c689-4dd2-8341-4172747de812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87957e46-bd23-44db-879a-74ae14e0f519",
                    "LayerId": "3d11d8ed-5628-4ba5-ab26-2534e36c2084"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "3d11d8ed-5628-4ba5-ab26-2534e36c2084",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56268842-a00b-4e6f-bb66-ebd421685b7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 17,
    "yorig": 17
}