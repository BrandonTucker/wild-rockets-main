{
    "id": "a341ec7b-e0f5-452b-83e3-a16277c0732a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship37",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 140,
    "bbox_left": 47,
    "bbox_right": 72,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f7ce9b2-808d-4fa4-9e39-8d89b9829c0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a341ec7b-e0f5-452b-83e3-a16277c0732a",
            "compositeImage": {
                "id": "97f21da5-3572-42ff-9a79-37794ee5f10b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f7ce9b2-808d-4fa4-9e39-8d89b9829c0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c27ceb27-4b1f-434e-a3f4-0e950a6aed8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7ce9b2-808d-4fa4-9e39-8d89b9829c0a",
                    "LayerId": "29f40480-cd6b-4e4f-b9dc-667c8a694f15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "29f40480-cd6b-4e4f-b9dc-667c8a694f15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a341ec7b-e0f5-452b-83e3-a16277c0732a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 60,
    "yorig": 75
}