{
    "id": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship34debri3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 0,
    "bbox_right": 108,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39589fbf-007e-4153-bea0-b6f6fb032943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "22d128db-e6ac-4463-9d28-983d3a18a579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39589fbf-007e-4153-bea0-b6f6fb032943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2490b8d7-abc5-4c25-a39c-1cf128e9d64f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39589fbf-007e-4153-bea0-b6f6fb032943",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "7fe7839e-d9f1-4079-b55b-2d641d063187",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "fa29e584-75d1-4b24-96ee-c75ffddcb6c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fe7839e-d9f1-4079-b55b-2d641d063187",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfe8e7f9-54dc-440e-b9ea-aadd0cf66cf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fe7839e-d9f1-4079-b55b-2d641d063187",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "e885c148-5ec6-4e8b-8a8d-bb9a689270b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "83f2a5a9-6a7f-42a4-9878-8e66327c9d94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e885c148-5ec6-4e8b-8a8d-bb9a689270b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf1c8fed-9b6c-4efd-bf88-d5f56bf1ff05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e885c148-5ec6-4e8b-8a8d-bb9a689270b0",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "d132c8cd-7787-4600-b37e-a239bcf7daa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "247e03c3-9557-4c1c-81f4-fb564c086a1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d132c8cd-7787-4600-b37e-a239bcf7daa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c998517-0ac5-4b53-870d-b66e1a67a01d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d132c8cd-7787-4600-b37e-a239bcf7daa6",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "06bbbb4d-a60c-4857-a4f5-c53953378c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "4107f59a-64af-4848-bb9a-48bf8befa530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06bbbb4d-a60c-4857-a4f5-c53953378c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cdfd6fb-db65-4764-b6a7-70a04f7002f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06bbbb4d-a60c-4857-a4f5-c53953378c35",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "d2c46087-034d-4144-a705-4f2ffd7773b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "6a8ff660-aafc-4113-9ae6-90c19aef40e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2c46087-034d-4144-a705-4f2ffd7773b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d64092e-12c1-4314-9f76-abb4bc79e585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2c46087-034d-4144-a705-4f2ffd7773b8",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "305a4786-ebf1-4e95-8fd0-874318db161e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "76cd88c0-910b-4c42-be1e-036332cc26f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305a4786-ebf1-4e95-8fd0-874318db161e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20ec8a3-37bd-4008-9850-8ae6d23e7799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305a4786-ebf1-4e95-8fd0-874318db161e",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "05d9c34a-ba85-4e50-bd0d-392b31aa5a3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "8bedb10e-3143-4650-8372-ed246557bfee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05d9c34a-ba85-4e50-bd0d-392b31aa5a3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "157fb9ec-2c90-45bc-8dd4-fffa099e747b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05d9c34a-ba85-4e50-bd0d-392b31aa5a3a",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "9caa5492-2e5d-4b10-887d-c702677c5c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "e5482aef-c3b1-458b-a13e-e870878bba0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9caa5492-2e5d-4b10-887d-c702677c5c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5860f4c-94d1-4376-bcf8-923179e7beca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9caa5492-2e5d-4b10-887d-c702677c5c01",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "57cc7ccc-440b-421f-84b1-889de3655094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "59b4c9be-516d-41bb-af8f-a50f46a079df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57cc7ccc-440b-421f-84b1-889de3655094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70159302-95bd-4f69-974a-99cd39c280cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57cc7ccc-440b-421f-84b1-889de3655094",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "4e621c07-48ea-4a20-b18c-6b0d7fe9d636",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "723471f4-7df0-4ee7-a1ad-5ae61810a063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e621c07-48ea-4a20-b18c-6b0d7fe9d636",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf1161fe-c216-467e-a7fe-33020e24255c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e621c07-48ea-4a20-b18c-6b0d7fe9d636",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "feba756e-32cd-4485-834f-b8598c33a1d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "0241fb79-eecf-4209-9e37-c5613e92f68f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feba756e-32cd-4485-834f-b8598c33a1d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e08b9042-424d-45e4-a52f-b09314f67d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feba756e-32cd-4485-834f-b8598c33a1d6",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "960a2b4b-c4f6-4dc0-9405-b0791a9ea987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "8091c75c-d799-4d59-8b95-6d1268290537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960a2b4b-c4f6-4dc0-9405-b0791a9ea987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3056e447-5b97-49b0-8c71-b2eaf3bdb227",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960a2b4b-c4f6-4dc0-9405-b0791a9ea987",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "bc440a11-fcf6-410b-b2f7-253e886b2a7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "47c7eb1b-1797-44ff-bbf9-908359e088ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc440a11-fcf6-410b-b2f7-253e886b2a7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f3e653e-9f11-4965-9e0b-bd2414b2d551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc440a11-fcf6-410b-b2f7-253e886b2a7a",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        },
        {
            "id": "684c75d2-6b0a-43ca-8aac-ee2efc19e565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "compositeImage": {
                "id": "08699a86-4ba3-4c43-9978-4e0e234586dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "684c75d2-6b0a-43ca-8aac-ee2efc19e565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e343fad-b2eb-489d-84c8-701cca625652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "684c75d2-6b0a-43ca-8aac-ee2efc19e565",
                    "LayerId": "b81b82e2-77d5-4aa2-9478-78a8946b0c24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "b81b82e2-77d5-4aa2-9478-78a8946b0c24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "972c2c21-3c4a-43aa-8a8d-3c56d15f2807",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 109,
    "xorig": 53,
    "yorig": 47
}