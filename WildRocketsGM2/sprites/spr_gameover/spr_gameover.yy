{
    "id": "8749f915-954d-42e7-a9f6-5dbcf23299be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gameover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 313,
    "bbox_left": 0,
    "bbox_right": 419,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e3be6cb-cb26-487e-bb64-2241c3875944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8749f915-954d-42e7-a9f6-5dbcf23299be",
            "compositeImage": {
                "id": "a0d4d31c-7590-4b26-9dfa-03923edd580d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e3be6cb-cb26-487e-bb64-2241c3875944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8bc3447-4beb-4c78-b304-49d1121dbb80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e3be6cb-cb26-487e-bb64-2241c3875944",
                    "LayerId": "dfff848a-88ac-41f5-bb2a-d4cf491d5095"
                }
            ]
        },
        {
            "id": "c3ca6794-ff9c-4e8e-ab41-6d364524fb60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8749f915-954d-42e7-a9f6-5dbcf23299be",
            "compositeImage": {
                "id": "2af40d08-5dec-4ce5-a47f-b0035b148746",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3ca6794-ff9c-4e8e-ab41-6d364524fb60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c951c45-0728-4726-8dca-74b08aa1d0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3ca6794-ff9c-4e8e-ab41-6d364524fb60",
                    "LayerId": "dfff848a-88ac-41f5-bb2a-d4cf491d5095"
                }
            ]
        },
        {
            "id": "74d8301a-ee31-4d7b-8bb8-448c27c328fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8749f915-954d-42e7-a9f6-5dbcf23299be",
            "compositeImage": {
                "id": "1060d105-84eb-4a12-9dde-db3a89ca98e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74d8301a-ee31-4d7b-8bb8-448c27c328fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "609d7785-05e9-45d3-9faf-77c4d427b2c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74d8301a-ee31-4d7b-8bb8-448c27c328fc",
                    "LayerId": "dfff848a-88ac-41f5-bb2a-d4cf491d5095"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 314,
    "layers": [
        {
            "id": "dfff848a-88ac-41f5-bb2a-d4cf491d5095",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8749f915-954d-42e7-a9f6-5dbcf23299be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 420,
    "xorig": 210,
    "yorig": 157
}