{
    "id": "66536ce0-6505-4e6f-819f-74e7a2242a41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship9explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "839099bb-396f-4ca1-a599-f156e75fa198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66536ce0-6505-4e6f-819f-74e7a2242a41",
            "compositeImage": {
                "id": "802643f2-76c8-4a27-a18b-b4a543915778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "839099bb-396f-4ca1-a599-f156e75fa198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e627bb9f-c02d-4c34-883b-ea91bee513bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "839099bb-396f-4ca1-a599-f156e75fa198",
                    "LayerId": "cf845987-31c9-4976-abaa-7f3769260e5b"
                }
            ]
        },
        {
            "id": "ac9458dd-4f92-462f-87e3-59962edb018f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66536ce0-6505-4e6f-819f-74e7a2242a41",
            "compositeImage": {
                "id": "075c1507-0010-44cf-b925-1777d75c3176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac9458dd-4f92-462f-87e3-59962edb018f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84d8f0b-609d-40dc-bf19-7f3d99561341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac9458dd-4f92-462f-87e3-59962edb018f",
                    "LayerId": "cf845987-31c9-4976-abaa-7f3769260e5b"
                }
            ]
        },
        {
            "id": "25bd5d6c-eb1c-4018-a271-fa41a88ff22d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66536ce0-6505-4e6f-819f-74e7a2242a41",
            "compositeImage": {
                "id": "860670a1-c592-49a6-96e7-bb17dde306b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25bd5d6c-eb1c-4018-a271-fa41a88ff22d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b4846e7-0ec7-442a-95e5-fc5171c80833",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25bd5d6c-eb1c-4018-a271-fa41a88ff22d",
                    "LayerId": "cf845987-31c9-4976-abaa-7f3769260e5b"
                }
            ]
        },
        {
            "id": "646ac86b-5818-4785-9420-09719727cbcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66536ce0-6505-4e6f-819f-74e7a2242a41",
            "compositeImage": {
                "id": "e1939e49-af12-4e14-b6d9-51a286e1b3e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "646ac86b-5818-4785-9420-09719727cbcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a42963e-bc0b-4c34-ad11-bbaede91b56c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "646ac86b-5818-4785-9420-09719727cbcc",
                    "LayerId": "cf845987-31c9-4976-abaa-7f3769260e5b"
                }
            ]
        },
        {
            "id": "08f80c2f-3bbc-40a2-8ae0-e8a574d410c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66536ce0-6505-4e6f-819f-74e7a2242a41",
            "compositeImage": {
                "id": "778c038c-4f9e-42bc-99d6-26b971f2463c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f80c2f-3bbc-40a2-8ae0-e8a574d410c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2887805e-9c98-400b-b567-91192b00d1d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f80c2f-3bbc-40a2-8ae0-e8a574d410c1",
                    "LayerId": "cf845987-31c9-4976-abaa-7f3769260e5b"
                }
            ]
        },
        {
            "id": "b578a062-6be3-41b5-81df-340151074d17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66536ce0-6505-4e6f-819f-74e7a2242a41",
            "compositeImage": {
                "id": "aafdf859-195b-4e1b-b154-278db5759c21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b578a062-6be3-41b5-81df-340151074d17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7631a75b-64bc-4594-adaa-a40983d3f2aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b578a062-6be3-41b5-81df-340151074d17",
                    "LayerId": "cf845987-31c9-4976-abaa-7f3769260e5b"
                }
            ]
        },
        {
            "id": "c5d23c2a-e675-41b7-93f8-6f94fd0ebc60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66536ce0-6505-4e6f-819f-74e7a2242a41",
            "compositeImage": {
                "id": "2c6b9d26-f9a1-41f4-8dd1-69f2f28eba4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d23c2a-e675-41b7-93f8-6f94fd0ebc60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b56b6e13-84e3-4f41-962b-b1bb00e2485d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d23c2a-e675-41b7-93f8-6f94fd0ebc60",
                    "LayerId": "cf845987-31c9-4976-abaa-7f3769260e5b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "cf845987-31c9-4976-abaa-7f3769260e5b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66536ce0-6505-4e6f-819f-74e7a2242a41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 46,
    "yorig": 51
}