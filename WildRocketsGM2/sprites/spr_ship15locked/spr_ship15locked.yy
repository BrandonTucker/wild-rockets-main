{
    "id": "d360e026-f5b3-4b77-b77a-cf11faba1a6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship15locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9aeadec8-b81a-40f8-8f6a-7a048e7812c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d360e026-f5b3-4b77-b77a-cf11faba1a6b",
            "compositeImage": {
                "id": "f286c9ba-cc1e-462b-bc1a-1ff714a59940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aeadec8-b81a-40f8-8f6a-7a048e7812c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e646823-ba2b-433a-9797-ee66e1a8a623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aeadec8-b81a-40f8-8f6a-7a048e7812c5",
                    "LayerId": "a706f6d6-487f-4421-a167-2f59c59af328"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "a706f6d6-487f-4421-a167-2f59c59af328",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d360e026-f5b3-4b77-b77a-cf11faba1a6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}