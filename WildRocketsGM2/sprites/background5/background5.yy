{
    "id": "920d529f-5567-4b6b-9c52-4a3015bd3d2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background5",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 1198,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52821809-6f69-44ce-a731-d991446fcb6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "920d529f-5567-4b6b-9c52-4a3015bd3d2c",
            "compositeImage": {
                "id": "37d15d0d-3349-4a6c-8372-d71b1232e8e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52821809-6f69-44ce-a731-d991446fcb6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c27ce31b-ef71-41a6-b91c-e454cecba5f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52821809-6f69-44ce-a731-d991446fcb6f",
                    "LayerId": "5a1863e8-b81a-4a8a-af0b-37f490c1d035"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1199,
    "layers": [
        {
            "id": "5a1863e8-b81a-4a8a-af0b-37f490c1d035",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "920d529f-5567-4b6b-9c52-4a3015bd3d2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}