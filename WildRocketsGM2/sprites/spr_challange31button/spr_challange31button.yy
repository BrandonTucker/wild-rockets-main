{
    "id": "b1cbacb5-7185-4cdb-8904-443819774c6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange31button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afc3da93-2499-47ca-994e-cba884ef6051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1cbacb5-7185-4cdb-8904-443819774c6d",
            "compositeImage": {
                "id": "27e3a06b-898f-4fc4-952c-c274879ed6f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afc3da93-2499-47ca-994e-cba884ef6051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edcd2997-9ad8-4b04-a1c0-2cb45a08c27c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afc3da93-2499-47ca-994e-cba884ef6051",
                    "LayerId": "806e0571-63f8-44b8-9bc8-9917b55bb459"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "806e0571-63f8-44b8-9bc8-9917b55bb459",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1cbacb5-7185-4cdb-8904-443819774c6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}