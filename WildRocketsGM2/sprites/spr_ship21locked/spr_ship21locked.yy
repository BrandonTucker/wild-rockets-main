{
    "id": "d7fae83b-1837-4f28-9bf7-cc875711b894",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship21locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c51bb306-03e8-41f1-80a3-23f0e60c154d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7fae83b-1837-4f28-9bf7-cc875711b894",
            "compositeImage": {
                "id": "a8a15027-2ed4-4965-8487-cd437783f18f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c51bb306-03e8-41f1-80a3-23f0e60c154d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8b4e8cf-f844-4540-b2d6-8bffc4ab70fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c51bb306-03e8-41f1-80a3-23f0e60c154d",
                    "LayerId": "31e0d83f-5d0c-4327-90d0-a15f55ad7bce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "31e0d83f-5d0c-4327-90d0-a15f55ad7bce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7fae83b-1837-4f28-9bf7-cc875711b894",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}