{
    "id": "fe1bf52d-7e80-4b7e-af54-0d5fa7ae5983",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa664cbc-17c3-4565-859a-29b086846111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe1bf52d-7e80-4b7e-af54-0d5fa7ae5983",
            "compositeImage": {
                "id": "ae1c25a8-d575-4433-a348-f10f69d1c96c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa664cbc-17c3-4565-859a-29b086846111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b513dfcd-8bae-420c-ab8b-f9632b48dcc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa664cbc-17c3-4565-859a-29b086846111",
                    "LayerId": "77cc0353-cd78-4cf3-85e3-ecc0fa503fec"
                }
            ]
        },
        {
            "id": "9a33c633-cf29-4a32-8d8d-d264cb063edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe1bf52d-7e80-4b7e-af54-0d5fa7ae5983",
            "compositeImage": {
                "id": "7d8b3a1a-5a92-4234-b9ce-2de68086e92f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a33c633-cf29-4a32-8d8d-d264cb063edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c53a855-d994-49c2-92cc-717dbd4bb3ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a33c633-cf29-4a32-8d8d-d264cb063edf",
                    "LayerId": "77cc0353-cd78-4cf3-85e3-ecc0fa503fec"
                }
            ]
        },
        {
            "id": "9fef30da-d50f-48c6-ae64-90f4844e63af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe1bf52d-7e80-4b7e-af54-0d5fa7ae5983",
            "compositeImage": {
                "id": "7a2008c1-6be8-4703-91f1-2baf44a0ac28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fef30da-d50f-48c6-ae64-90f4844e63af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4bc8ad2-54df-4f1a-afe0-584137c097cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fef30da-d50f-48c6-ae64-90f4844e63af",
                    "LayerId": "77cc0353-cd78-4cf3-85e3-ecc0fa503fec"
                }
            ]
        },
        {
            "id": "a2db6199-e98b-455b-8f5d-e8833f0a3024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe1bf52d-7e80-4b7e-af54-0d5fa7ae5983",
            "compositeImage": {
                "id": "c262db54-ea79-4590-9714-096105daa9d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2db6199-e98b-455b-8f5d-e8833f0a3024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdcd7448-ea64-4c77-bd09-97867d5b3c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2db6199-e98b-455b-8f5d-e8833f0a3024",
                    "LayerId": "77cc0353-cd78-4cf3-85e3-ecc0fa503fec"
                }
            ]
        },
        {
            "id": "6ccdfd0a-08aa-4873-a842-358d1a5630d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe1bf52d-7e80-4b7e-af54-0d5fa7ae5983",
            "compositeImage": {
                "id": "616759d8-84dd-4816-8256-a1d6c60c7449",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ccdfd0a-08aa-4873-a842-358d1a5630d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f881f54-6382-479f-95ad-33bb91c62263",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ccdfd0a-08aa-4873-a842-358d1a5630d9",
                    "LayerId": "77cc0353-cd78-4cf3-85e3-ecc0fa503fec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "77cc0353-cd78-4cf3-85e3-ecc0fa503fec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe1bf52d-7e80-4b7e-af54-0d5fa7ae5983",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 16
}