{
    "id": "419b9bba-9661-4264-b503-f88028deaab6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wildrocketsanimation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 355,
    "bbox_left": 0,
    "bbox_right": 613,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8e993f3-e761-4c04-afc0-963210f65ed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "838077cd-5b73-42eb-b134-b5187787dff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e993f3-e761-4c04-afc0-963210f65ed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74b1f318-ed9b-4f82-b5b8-e088b3a9163e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e993f3-e761-4c04-afc0-963210f65ed9",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "cfc041af-e9c9-4f53-9cc0-a0664f4374f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "9297c559-5939-4a8d-844d-091086d55bf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfc041af-e9c9-4f53-9cc0-a0664f4374f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f1d0e64-b9aa-4337-b64a-c17caed73f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfc041af-e9c9-4f53-9cc0-a0664f4374f3",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "f0241cd7-9d51-4db0-9d36-7177d8e8fa29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "d15e2337-e24c-4168-a9a9-e41eab3a7829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0241cd7-9d51-4db0-9d36-7177d8e8fa29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5da6cac7-488b-4788-90b0-7eed5a14d1d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0241cd7-9d51-4db0-9d36-7177d8e8fa29",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "69b6f76a-a599-43a4-bd0a-762da96f4c7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "2a1e7dea-16b8-4029-811d-a9c7f48e096b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69b6f76a-a599-43a4-bd0a-762da96f4c7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40fb7a6e-e746-4e43-8a60-730036fe4d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69b6f76a-a599-43a4-bd0a-762da96f4c7c",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "9166bf76-4f11-4b22-aab9-0e1f38563bf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "2db9a235-f0db-464e-b62d-be640c365d4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9166bf76-4f11-4b22-aab9-0e1f38563bf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6bba243-5bd5-467a-afd2-f0827945bf01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9166bf76-4f11-4b22-aab9-0e1f38563bf0",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "bb5edb59-c41c-4ad6-89ab-6e5ed9834eb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "98b8f7d2-7c5c-442f-914d-04c46d79b831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb5edb59-c41c-4ad6-89ab-6e5ed9834eb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4b4b400-1746-4b01-8276-ffd580cc6b6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb5edb59-c41c-4ad6-89ab-6e5ed9834eb8",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "9a43df3c-1c18-4fcc-bc05-fab21973a0bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "900a9b2a-ce23-4bd3-9231-83251b07ad8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a43df3c-1c18-4fcc-bc05-fab21973a0bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45af9859-a47f-4e8f-ac72-04da0d40309d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a43df3c-1c18-4fcc-bc05-fab21973a0bc",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "59a6c873-3894-44a9-85cf-870709accd3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "c04ae949-d100-4567-9d65-17b692d67540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a6c873-3894-44a9-85cf-870709accd3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23ef55c0-1af6-4b99-965f-a631fdb0355c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a6c873-3894-44a9-85cf-870709accd3b",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "e214d150-04c1-4a5b-af4c-edeb4f700b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "2bd56ecc-fb8c-4923-b7c0-05a58c19ca16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e214d150-04c1-4a5b-af4c-edeb4f700b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d8b7a2-2383-4feb-aa1d-394394a2c80c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e214d150-04c1-4a5b-af4c-edeb4f700b08",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "9d498f12-e889-4c3b-b275-7fad6d0a6fbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "f46d4526-7b02-4b97-9289-e9ea5f35e208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d498f12-e889-4c3b-b275-7fad6d0a6fbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75e9e0cb-f28e-4915-b4f3-a9501a786213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d498f12-e889-4c3b-b275-7fad6d0a6fbf",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "777ab81f-8cff-414e-81f2-f119f1cde110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "d276f3be-0361-4ffc-b9b3-7c6f6f3317b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "777ab81f-8cff-414e-81f2-f119f1cde110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d25e40fc-3e15-4e9e-a19b-f399ca9e9104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "777ab81f-8cff-414e-81f2-f119f1cde110",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "5b42af71-846d-454c-8448-7cfc540c4b1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "c1653e3a-ec28-4e10-809a-0dea4fde3312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b42af71-846d-454c-8448-7cfc540c4b1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e536a797-bc97-4cb6-b6fb-e99245b79544",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b42af71-846d-454c-8448-7cfc540c4b1b",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "ea2680c6-03d1-46cc-923b-b50398de64b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "5eb4880e-6eaa-43d3-add6-a7d74a0df699",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea2680c6-03d1-46cc-923b-b50398de64b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b0bfe36-415d-4ade-a043-a798b5a4e788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea2680c6-03d1-46cc-923b-b50398de64b5",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "8f008e59-9cc6-4f6c-b753-e128349f21e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "feeeba97-b919-4a15-9e79-403e27a8c49f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f008e59-9cc6-4f6c-b753-e128349f21e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d1bd86-b745-4dd1-ab46-1727e0376fb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f008e59-9cc6-4f6c-b753-e128349f21e7",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        },
        {
            "id": "075773c7-b280-45f8-a915-5063faaa6bda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "compositeImage": {
                "id": "d36f6c5a-0237-468d-ae5d-81ceb9b18ff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "075773c7-b280-45f8-a915-5063faaa6bda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71206860-a3a3-421b-983d-c52e32ddcb98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "075773c7-b280-45f8-a915-5063faaa6bda",
                    "LayerId": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 356,
    "layers": [
        {
            "id": "4a6089ea-b23f-4266-bcd0-a340b9f9e4b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "419b9bba-9661-4264-b503-f88028deaab6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 614,
    "xorig": 286,
    "yorig": 120
}