{
    "id": "44f73386-9224-4830-9c3a-3090840f65a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange36button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2586ada1-c482-4858-bf0b-1d7cedde86c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44f73386-9224-4830-9c3a-3090840f65a0",
            "compositeImage": {
                "id": "f301139c-1a12-4c0f-932b-a47d4bf52221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2586ada1-c482-4858-bf0b-1d7cedde86c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c254d62e-4f71-4fae-8af4-43ae1ad7dc7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2586ada1-c482-4858-bf0b-1d7cedde86c1",
                    "LayerId": "4fc9044e-1195-4980-a3bc-bbb281dcf4e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4fc9044e-1195-4980-a3bc-bbb281dcf4e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44f73386-9224-4830-9c3a-3090840f65a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}