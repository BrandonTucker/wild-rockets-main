{
    "id": "9767a559-6719-481f-a7df-0a1fc9e8c774",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship35debri1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 0,
    "bbox_right": 101,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0436e305-4ddc-42d3-9cb3-f149b0a77e25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "b1fd714a-ddba-459f-bdf2-35af6ef275de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0436e305-4ddc-42d3-9cb3-f149b0a77e25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a336b8c-bb6b-46b1-bcf4-81a3c4ebc74e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0436e305-4ddc-42d3-9cb3-f149b0a77e25",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "ecf03ae1-f365-4887-8e21-1984fba03a4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "aa8127ae-8869-4203-8b9f-30f7f5c5b97c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf03ae1-f365-4887-8e21-1984fba03a4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dccaf711-586e-4905-bb13-b6f917c7f39c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf03ae1-f365-4887-8e21-1984fba03a4a",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "43f49842-52ca-49be-a411-cc44053a762b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "a01b24cf-0206-4973-aaac-8a72216675e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43f49842-52ca-49be-a411-cc44053a762b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aa990b4-4743-44b1-bf7c-ffd784fe8687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43f49842-52ca-49be-a411-cc44053a762b",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "62e90d43-aeec-4ec9-aad2-a2832a5c3651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "3859f1eb-f41c-42bb-b63b-2889eb3f3b8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62e90d43-aeec-4ec9-aad2-a2832a5c3651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e58caf-5ecb-42f6-9c5c-043e8cb8ba51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62e90d43-aeec-4ec9-aad2-a2832a5c3651",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "87240fb1-2cc2-49a4-8371-5327f4661f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "41d59689-5b6b-4c0c-ac38-173090bc6294",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87240fb1-2cc2-49a4-8371-5327f4661f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2064ad37-f521-46c9-89f1-416a2bb82f45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87240fb1-2cc2-49a4-8371-5327f4661f43",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "ce857f77-0c2b-44ca-a3aa-7f60625410c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "85c09675-5df1-4946-afa0-72fd3dfd6672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce857f77-0c2b-44ca-a3aa-7f60625410c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46bb9e5a-37df-40e6-beee-61baab813df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce857f77-0c2b-44ca-a3aa-7f60625410c0",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "903e975c-1a3c-463d-927c-76eeb2462fc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "9e420847-1394-4cf9-9fdb-26d5e1c4af0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "903e975c-1a3c-463d-927c-76eeb2462fc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcff4ca0-695c-493d-bdd9-d36dd15d8589",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "903e975c-1a3c-463d-927c-76eeb2462fc4",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "2e5a5a6c-4572-41db-b36e-7d60828bb9e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "7fb391f1-ec8f-43ce-9f77-619d3e722655",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e5a5a6c-4572-41db-b36e-7d60828bb9e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c03d6a3c-f9b8-40c6-aad8-7e6b3c566a38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e5a5a6c-4572-41db-b36e-7d60828bb9e3",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "cca34c5f-b7ab-42dc-bd2a-7bfe9ac85e22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "92a537c3-b2ad-47f6-8172-8c7f1c9d855d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cca34c5f-b7ab-42dc-bd2a-7bfe9ac85e22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73081858-e438-4003-b282-e5fc3d97a84f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cca34c5f-b7ab-42dc-bd2a-7bfe9ac85e22",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "ba55075b-9be6-4c86-912b-b20d078682f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "62701c28-5955-43dd-8c0a-eaa51cc90479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba55075b-9be6-4c86-912b-b20d078682f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "802aa9db-98d6-4f5b-8888-4ca7e1f03acc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba55075b-9be6-4c86-912b-b20d078682f8",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "12394b70-90de-44cd-99f5-c7d54c3386d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "f0918610-db13-49e1-9339-dfab483a3185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12394b70-90de-44cd-99f5-c7d54c3386d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a3f4ca-3a60-4255-b485-bcfa47780287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12394b70-90de-44cd-99f5-c7d54c3386d4",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "b9b1ecd8-4d78-43bf-b28c-1b8e314833e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "a79d90d3-9aee-4c0e-bf1c-d6adc7458608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9b1ecd8-4d78-43bf-b28c-1b8e314833e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a00c1903-e893-43c2-b274-de90e23cd356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b1ecd8-4d78-43bf-b28c-1b8e314833e6",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "6e4a4814-2be3-4122-b85a-cd34f3515207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "970b49de-59ac-47cb-b55b-5f4fcaec5eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e4a4814-2be3-4122-b85a-cd34f3515207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efaf6d82-8735-4036-9269-a8c8273eb7c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e4a4814-2be3-4122-b85a-cd34f3515207",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "96304a38-c66b-49b1-8b91-5e2e75d6c53c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "87b7ed7a-f829-4c73-b868-2f51fed229a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96304a38-c66b-49b1-8b91-5e2e75d6c53c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "501b47a4-32aa-462c-bf55-7f269a4451b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96304a38-c66b-49b1-8b91-5e2e75d6c53c",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        },
        {
            "id": "07f000fa-f944-4999-a0a4-3db4cae942dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "compositeImage": {
                "id": "d8be7be1-eab0-4167-a06e-bac3fba0906e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07f000fa-f944-4999-a0a4-3db4cae942dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "357348ec-085f-4669-b903-c64c438636f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07f000fa-f944-4999-a0a4-3db4cae942dd",
                    "LayerId": "e806bee7-a581-4d5d-9fb9-1380b1378224"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 102,
    "layers": [
        {
            "id": "e806bee7-a581-4d5d-9fb9-1380b1378224",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9767a559-6719-481f-a7df-0a1fc9e8c774",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 102,
    "xorig": 41,
    "yorig": 37
}