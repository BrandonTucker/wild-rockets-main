{
    "id": "99e5bb8a-9ebe-45cb-88c5-562d898915d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage9button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54711168-fcc0-4b9d-8882-4d1416a3b707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99e5bb8a-9ebe-45cb-88c5-562d898915d1",
            "compositeImage": {
                "id": "92a481c8-6868-4a95-a6c0-0f4de5ab218a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54711168-fcc0-4b9d-8882-4d1416a3b707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4793f2ad-bdec-4eda-8de9-984f61d31a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54711168-fcc0-4b9d-8882-4d1416a3b707",
                    "LayerId": "e767e909-ba97-4785-ba18-3c3e0dbf6c62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "e767e909-ba97-4785-ba18-3c3e0dbf6c62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99e5bb8a-9ebe-45cb-88c5-562d898915d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}