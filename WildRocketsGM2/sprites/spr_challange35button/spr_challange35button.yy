{
    "id": "7f6892cc-2d4a-466f-8820-7f8ec505d8d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange35button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b979c60-80d6-48c5-90a8-9b77a300abde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f6892cc-2d4a-466f-8820-7f8ec505d8d5",
            "compositeImage": {
                "id": "f299c297-44f7-4693-8122-078980920aef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b979c60-80d6-48c5-90a8-9b77a300abde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7b98b3b-8a69-44f6-a68e-1413fbe128d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b979c60-80d6-48c5-90a8-9b77a300abde",
                    "LayerId": "9ffdfa1f-92d3-4012-a632-ad1e91325621"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "9ffdfa1f-92d3-4012-a632-ad1e91325621",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f6892cc-2d4a-466f-8820-7f8ec505d8d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}