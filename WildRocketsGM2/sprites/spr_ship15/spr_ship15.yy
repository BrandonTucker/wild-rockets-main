{
    "id": "51537e3c-e448-45d9-9229-3e6976746c64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship15",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 30,
    "bbox_right": 55,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18b370ce-b2e0-4b5c-ac07-dccf03586e29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51537e3c-e448-45d9-9229-3e6976746c64",
            "compositeImage": {
                "id": "7f8cfb8f-44bc-4fde-859c-ed09abf8ed3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18b370ce-b2e0-4b5c-ac07-dccf03586e29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faaf1b17-9540-45b3-8493-c43e7ff9d853",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18b370ce-b2e0-4b5c-ac07-dccf03586e29",
                    "LayerId": "c733f3f1-9a45-4c87-9488-edb18237c4af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "c733f3f1-9a45-4c87-9488-edb18237c4af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51537e3c-e448-45d9-9229-3e6976746c64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 43,
    "yorig": 40
}