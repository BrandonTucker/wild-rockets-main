{
    "id": "11b42630-1496-4c0e-96a9-e33d6583b1a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blockExplode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 0,
    "bbox_right": 73,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66325256-f1d8-4c1c-8d39-03077c6b6642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11b42630-1496-4c0e-96a9-e33d6583b1a4",
            "compositeImage": {
                "id": "dcc50a98-5fbf-4084-9728-3a5d1bb04c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66325256-f1d8-4c1c-8d39-03077c6b6642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5430f93-3dc3-4052-bad5-d5f85baa6bce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66325256-f1d8-4c1c-8d39-03077c6b6642",
                    "LayerId": "2ed9aea2-7fe8-4b89-a73e-3bb636ba8658"
                }
            ]
        },
        {
            "id": "2673deed-9610-45f9-b4d4-96837031dab8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11b42630-1496-4c0e-96a9-e33d6583b1a4",
            "compositeImage": {
                "id": "271a2425-a737-41c4-8ff4-34e2eaff9be8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2673deed-9610-45f9-b4d4-96837031dab8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcc14b95-cbd4-443f-87ef-423e2b7e8e50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2673deed-9610-45f9-b4d4-96837031dab8",
                    "LayerId": "2ed9aea2-7fe8-4b89-a73e-3bb636ba8658"
                }
            ]
        },
        {
            "id": "bcaf9b4e-98bf-47d7-a608-c35986d3353f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11b42630-1496-4c0e-96a9-e33d6583b1a4",
            "compositeImage": {
                "id": "d2a995d8-7104-4468-b1d4-d24d7dbef5a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcaf9b4e-98bf-47d7-a608-c35986d3353f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bffe6d10-6168-404f-b1c3-4d4b7b949812",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcaf9b4e-98bf-47d7-a608-c35986d3353f",
                    "LayerId": "2ed9aea2-7fe8-4b89-a73e-3bb636ba8658"
                }
            ]
        },
        {
            "id": "ea264074-3356-4a7b-898c-3672febbe2d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11b42630-1496-4c0e-96a9-e33d6583b1a4",
            "compositeImage": {
                "id": "0486eab9-05e9-40a0-b5e9-c2ccf4511c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea264074-3356-4a7b-898c-3672febbe2d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b466dc27-a2ee-4678-96c4-06d2d8246962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea264074-3356-4a7b-898c-3672febbe2d4",
                    "LayerId": "2ed9aea2-7fe8-4b89-a73e-3bb636ba8658"
                }
            ]
        },
        {
            "id": "beb0d05d-fdbd-42c0-9424-4a01c956d597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11b42630-1496-4c0e-96a9-e33d6583b1a4",
            "compositeImage": {
                "id": "0576a907-325c-473e-ab7c-c39a8a7d9a71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beb0d05d-fdbd-42c0-9424-4a01c956d597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da92955d-631c-4730-9014-b7c5732a9b15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beb0d05d-fdbd-42c0-9424-4a01c956d597",
                    "LayerId": "2ed9aea2-7fe8-4b89-a73e-3bb636ba8658"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "2ed9aea2-7fe8-4b89-a73e-3bb636ba8658",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11b42630-1496-4c0e-96a9-e33d6583b1a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 37,
    "yorig": 38
}