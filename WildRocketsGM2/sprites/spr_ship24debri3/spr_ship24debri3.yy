{
    "id": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship24debri3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 0,
    "bbox_right": 114,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bf456f4-00bf-4b9d-ac63-c8e8bb90943b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "0a4dc6b8-7d0e-4716-93ac-4e8b812d5e83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bf456f4-00bf-4b9d-ac63-c8e8bb90943b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cde887b-b68c-4cb3-a5ce-481565dedd95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf456f4-00bf-4b9d-ac63-c8e8bb90943b",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "79a9c544-4de3-4b96-888e-8a2a7bfebf89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "25fd8ba4-7045-4264-8768-4c283794434c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79a9c544-4de3-4b96-888e-8a2a7bfebf89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb9116c9-b03a-44c0-ad2e-f7c6f7c377ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79a9c544-4de3-4b96-888e-8a2a7bfebf89",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "e7431ef1-03d5-4bd7-901f-afde2076b85b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "2475aed2-0d8b-4bf2-8b2b-f9c05ecca794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7431ef1-03d5-4bd7-901f-afde2076b85b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "440ffef3-9c9f-44fc-ba50-98d0f8b6113a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7431ef1-03d5-4bd7-901f-afde2076b85b",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "1b6ae680-97ae-434c-b932-3080dca58f49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "edd095ec-d149-4a73-8132-03350b4c6cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b6ae680-97ae-434c-b932-3080dca58f49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddc588d8-81ba-4573-8802-134d6f77aff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b6ae680-97ae-434c-b932-3080dca58f49",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "a649e130-3001-4ca1-8f29-17ffd79d3300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "46901d45-73b1-4c96-b3cb-35ca7bfb5158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a649e130-3001-4ca1-8f29-17ffd79d3300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2cc6cfc-8df9-4586-9ecd-fa0715c53de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a649e130-3001-4ca1-8f29-17ffd79d3300",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "9e7999d1-7d18-4bfd-b9a4-61dd2e432953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "4ef782e0-56bc-4612-8045-43f90e26f27e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e7999d1-7d18-4bfd-b9a4-61dd2e432953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f76aca40-0c74-4e11-9cfb-ff262393423d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e7999d1-7d18-4bfd-b9a4-61dd2e432953",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "ebbe8d4c-367a-4520-828b-278d1ea69922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "b16861ac-c93d-464a-ab30-6c5ca2a47aa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebbe8d4c-367a-4520-828b-278d1ea69922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf80d6c6-d86c-4662-87ac-ff0c84cb0318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebbe8d4c-367a-4520-828b-278d1ea69922",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "9b2f8fca-3c93-45c3-9808-ad5a6717331c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "1cb8f011-ce25-432d-9d9f-cc6e6997c054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b2f8fca-3c93-45c3-9808-ad5a6717331c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aaa5e73-bbd5-4099-9076-02063db77c61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b2f8fca-3c93-45c3-9808-ad5a6717331c",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "dd5b667e-481c-4fb7-bf47-6782e3ea485b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "953e8af4-f1de-4276-a58d-47e76f167fd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd5b667e-481c-4fb7-bf47-6782e3ea485b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f95f23a9-4a1b-4a77-89d9-074715eee860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd5b667e-481c-4fb7-bf47-6782e3ea485b",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "5850da66-2b39-40d6-9c40-1fd09cc10830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "64fa2c16-77f1-4339-942c-4e951898cf1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5850da66-2b39-40d6-9c40-1fd09cc10830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c17c7537-791f-4b7c-ab98-70e00f9d0e03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5850da66-2b39-40d6-9c40-1fd09cc10830",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "c6f7e894-8c8e-4be2-9b9f-d36fda88a88b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "05318c14-147c-4d8d-8fd7-d0af203e64b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f7e894-8c8e-4be2-9b9f-d36fda88a88b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3c39570-1161-45ef-b0f6-565b6b96ba2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f7e894-8c8e-4be2-9b9f-d36fda88a88b",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "045d3c37-fe5c-4173-a95c-09c153560dc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "5d0b57ba-4331-4767-9a6e-3fa2cc8e6050",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "045d3c37-fe5c-4173-a95c-09c153560dc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d26ecfac-6a46-49bc-b788-9aecb287dbbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "045d3c37-fe5c-4173-a95c-09c153560dc3",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "6aa50ad7-0677-4700-b15a-e444e41a5f61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "20bef1da-887c-43e2-b23c-b75fb6b734d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aa50ad7-0677-4700-b15a-e444e41a5f61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb17499a-cf2e-4462-955b-da5f0975cc6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aa50ad7-0677-4700-b15a-e444e41a5f61",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "371a583b-57ca-4c6e-80c5-ad1571642d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "5f566d50-4062-408b-88f3-141d42cf0b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "371a583b-57ca-4c6e-80c5-ad1571642d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d03a3f89-4bec-49d7-9ddd-c380a8619965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "371a583b-57ca-4c6e-80c5-ad1571642d3b",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        },
        {
            "id": "421bb4d6-0475-4ac0-89ba-72e52358a4ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "compositeImage": {
                "id": "bc765f36-4122-470a-9af6-e5305f0015bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "421bb4d6-0475-4ac0-89ba-72e52358a4ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aa47141-6ac8-47d7-b15a-010a49bd3944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "421bb4d6-0475-4ac0-89ba-72e52358a4ed",
                    "LayerId": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "9d8d6232-6cd2-4dda-a3a1-3640c2785e39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c46f376b-375a-4acc-9a6f-ce1ac4e17c31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}