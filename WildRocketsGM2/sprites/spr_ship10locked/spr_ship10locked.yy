{
    "id": "9513ff19-1a88-4059-a1b7-919a717582c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship10locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70d29a3e-2be6-4a58-807e-4a94e12b9166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9513ff19-1a88-4059-a1b7-919a717582c6",
            "compositeImage": {
                "id": "5d1b354a-9e5d-4585-8438-ca9afa416135",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70d29a3e-2be6-4a58-807e-4a94e12b9166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb6455ed-d958-4ab5-927c-0d75da8fcedc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70d29a3e-2be6-4a58-807e-4a94e12b9166",
                    "LayerId": "3ec04ad3-2095-4b5a-ac6d-c4640d9fcb7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "3ec04ad3-2095-4b5a-ac6d-c4640d9fcb7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9513ff19-1a88-4059-a1b7-919a717582c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}