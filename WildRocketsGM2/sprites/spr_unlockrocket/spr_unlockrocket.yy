{
    "id": "a9d72671-5ad2-46e0-bce2-97dcb5f5a20d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_unlockrocket",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 313,
    "bbox_left": 0,
    "bbox_right": 419,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33ec6d32-086e-4787-8f46-bc4d3ff42594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9d72671-5ad2-46e0-bce2-97dcb5f5a20d",
            "compositeImage": {
                "id": "7b4e01a6-d077-469b-b02c-c1383fcdcf90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ec6d32-086e-4787-8f46-bc4d3ff42594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0040a5a6-375c-4ada-9114-fbbaa28b4754",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ec6d32-086e-4787-8f46-bc4d3ff42594",
                    "LayerId": "26b5500f-cf7f-4686-b7f8-b3ef74e7472f"
                }
            ]
        },
        {
            "id": "82c53030-e97c-4463-87cf-e931a68649dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9d72671-5ad2-46e0-bce2-97dcb5f5a20d",
            "compositeImage": {
                "id": "f40d5e1f-9b6a-4da7-a926-1db0b72a6620",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c53030-e97c-4463-87cf-e931a68649dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cca6922-a0a6-4a11-9f8f-b5dc87b038cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c53030-e97c-4463-87cf-e931a68649dc",
                    "LayerId": "26b5500f-cf7f-4686-b7f8-b3ef74e7472f"
                }
            ]
        },
        {
            "id": "2d80fc1e-fe62-41fa-bf40-22bf2b4eb206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9d72671-5ad2-46e0-bce2-97dcb5f5a20d",
            "compositeImage": {
                "id": "0cdbe3be-4709-4483-a39a-678d06a588e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d80fc1e-fe62-41fa-bf40-22bf2b4eb206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a481aebd-069c-45d5-beea-fd9125566f8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d80fc1e-fe62-41fa-bf40-22bf2b4eb206",
                    "LayerId": "26b5500f-cf7f-4686-b7f8-b3ef74e7472f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 314,
    "layers": [
        {
            "id": "26b5500f-cf7f-4686-b7f8-b3ef74e7472f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9d72671-5ad2-46e0-bce2-97dcb5f5a20d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 420,
    "xorig": 210,
    "yorig": 157
}