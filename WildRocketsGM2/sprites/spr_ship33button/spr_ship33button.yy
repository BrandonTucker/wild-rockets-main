{
    "id": "75af407b-23a2-49e1-a535-43b890cec698",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship33button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d7477b7-5dc6-4d4d-922e-49a69da48b71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75af407b-23a2-49e1-a535-43b890cec698",
            "compositeImage": {
                "id": "7f0711e6-4d92-4d79-8322-40aa146744fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d7477b7-5dc6-4d4d-922e-49a69da48b71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6de783c-ef1b-4b5e-b5da-60a8353edd36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d7477b7-5dc6-4d4d-922e-49a69da48b71",
                    "LayerId": "7ebd6971-73c0-4716-8525-261ee0a2e392"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7ebd6971-73c0-4716-8525-261ee0a2e392",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75af407b-23a2-49e1-a535-43b890cec698",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}