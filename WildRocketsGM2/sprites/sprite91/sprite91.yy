{
    "id": "b83e738a-3a25-49a4-b0ce-bcbf1a072512",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite91",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 10,
    "bbox_right": 56,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "270d4fd4-a30d-4d7e-9102-007e96e8e90b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83e738a-3a25-49a4-b0ce-bcbf1a072512",
            "compositeImage": {
                "id": "75573cc7-551f-4c0d-9227-a5e3b724f868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "270d4fd4-a30d-4d7e-9102-007e96e8e90b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3913035-1afc-48f7-89a0-79f03a890421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "270d4fd4-a30d-4d7e-9102-007e96e8e90b",
                    "LayerId": "9e993d50-cc32-47e8-a35a-6270281bfa19"
                }
            ]
        },
        {
            "id": "86331664-0c89-48f0-b2d6-bcd3c241c384",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83e738a-3a25-49a4-b0ce-bcbf1a072512",
            "compositeImage": {
                "id": "ce1a7abf-1a25-4651-850c-ac94d6dcff06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86331664-0c89-48f0-b2d6-bcd3c241c384",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89ddbf45-4fec-44cb-97ac-f0a52ec109f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86331664-0c89-48f0-b2d6-bcd3c241c384",
                    "LayerId": "9e993d50-cc32-47e8-a35a-6270281bfa19"
                }
            ]
        },
        {
            "id": "67bab8ae-eb5e-4f79-869d-368fdc6c2d6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83e738a-3a25-49a4-b0ce-bcbf1a072512",
            "compositeImage": {
                "id": "87775de0-968b-4d64-a82b-5f5142823b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67bab8ae-eb5e-4f79-869d-368fdc6c2d6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e25aeebe-5abf-4222-a87f-736129e906a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67bab8ae-eb5e-4f79-869d-368fdc6c2d6b",
                    "LayerId": "9e993d50-cc32-47e8-a35a-6270281bfa19"
                }
            ]
        },
        {
            "id": "4884e8f5-0189-4e2b-8350-ff6f93b1b2f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83e738a-3a25-49a4-b0ce-bcbf1a072512",
            "compositeImage": {
                "id": "afadb76a-8ce5-4a24-859a-fa840653ddd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4884e8f5-0189-4e2b-8350-ff6f93b1b2f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49af4c6e-5898-467a-858f-8fc37d681e64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4884e8f5-0189-4e2b-8350-ff6f93b1b2f3",
                    "LayerId": "9e993d50-cc32-47e8-a35a-6270281bfa19"
                }
            ]
        },
        {
            "id": "45df4140-de48-4e82-9170-c426d6e95740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83e738a-3a25-49a4-b0ce-bcbf1a072512",
            "compositeImage": {
                "id": "a01283cd-6ebb-4a44-abb3-f9934b33eb52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45df4140-de48-4e82-9170-c426d6e95740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "586ff37d-3b5d-470b-a0b6-7ae9c954ad92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45df4140-de48-4e82-9170-c426d6e95740",
                    "LayerId": "9e993d50-cc32-47e8-a35a-6270281bfa19"
                }
            ]
        },
        {
            "id": "eb998e75-01e8-416e-ac5e-49e3a031e237",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83e738a-3a25-49a4-b0ce-bcbf1a072512",
            "compositeImage": {
                "id": "df3488eb-4df4-4834-a037-c2d4a8c98847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb998e75-01e8-416e-ac5e-49e3a031e237",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "884352a1-eb0c-41c1-958a-cd815b76ecaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb998e75-01e8-416e-ac5e-49e3a031e237",
                    "LayerId": "9e993d50-cc32-47e8-a35a-6270281bfa19"
                }
            ]
        },
        {
            "id": "473619f4-b12d-44db-978a-806b0708108d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b83e738a-3a25-49a4-b0ce-bcbf1a072512",
            "compositeImage": {
                "id": "ed7af170-20c4-45b9-bb55-1d5c952944ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "473619f4-b12d-44db-978a-806b0708108d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d29ee527-2edd-4f71-87ce-76c7d83bff92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "473619f4-b12d-44db-978a-806b0708108d",
                    "LayerId": "9e993d50-cc32-47e8-a35a-6270281bfa19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9e993d50-cc32-47e8-a35a-6270281bfa19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b83e738a-3a25-49a4-b0ce-bcbf1a072512",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}