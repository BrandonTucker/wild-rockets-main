{
    "id": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 36,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1733e841-c7d2-4d6e-9e89-ff57548cf3db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "compositeImage": {
                "id": "72b2040c-5307-483e-8aa1-45d712045601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1733e841-c7d2-4d6e-9e89-ff57548cf3db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d18ab53f-55d3-4702-a33c-f2e6083539a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1733e841-c7d2-4d6e-9e89-ff57548cf3db",
                    "LayerId": "5253b132-b79b-4fe3-9568-417e18c257cc"
                }
            ]
        },
        {
            "id": "5fa3017b-37b9-4978-b02f-c9954599b7d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "compositeImage": {
                "id": "1641cc4c-71ca-425d-aca3-394589894cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa3017b-37b9-4978-b02f-c9954599b7d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a63c5d0d-8b43-4258-8f98-557645565f31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa3017b-37b9-4978-b02f-c9954599b7d1",
                    "LayerId": "5253b132-b79b-4fe3-9568-417e18c257cc"
                }
            ]
        },
        {
            "id": "a64b8974-79a0-47ef-9024-3a8a38ee589e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "compositeImage": {
                "id": "dd9e2488-7459-48f8-ba1a-f3e5aa233f6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a64b8974-79a0-47ef-9024-3a8a38ee589e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de56505d-14e9-4c0a-aea1-d3f36c6b65fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a64b8974-79a0-47ef-9024-3a8a38ee589e",
                    "LayerId": "5253b132-b79b-4fe3-9568-417e18c257cc"
                }
            ]
        },
        {
            "id": "c704bb64-9fa9-40cf-bef7-df1aadae72ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "compositeImage": {
                "id": "6773ea91-080d-4559-aa81-06859c40d92f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c704bb64-9fa9-40cf-bef7-df1aadae72ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d01d9b4d-1c1f-4ff3-90d2-bae50528ec39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c704bb64-9fa9-40cf-bef7-df1aadae72ab",
                    "LayerId": "5253b132-b79b-4fe3-9568-417e18c257cc"
                }
            ]
        },
        {
            "id": "37722aa0-2120-4ea2-b175-f52fd36f94ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "compositeImage": {
                "id": "a9621145-ce84-49dd-b0cc-fe1c458ba8a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37722aa0-2120-4ea2-b175-f52fd36f94ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2838d3b1-5960-48c0-82b4-2a59e10019b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37722aa0-2120-4ea2-b175-f52fd36f94ba",
                    "LayerId": "5253b132-b79b-4fe3-9568-417e18c257cc"
                }
            ]
        },
        {
            "id": "eae8bf23-8dbb-4a1a-93c3-bf9eb704acbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "compositeImage": {
                "id": "34c85299-f9b5-4e3a-8f27-950b80b7e070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eae8bf23-8dbb-4a1a-93c3-bf9eb704acbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f71cff27-1eac-4e58-8166-fba10db54633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eae8bf23-8dbb-4a1a-93c3-bf9eb704acbd",
                    "LayerId": "5253b132-b79b-4fe3-9568-417e18c257cc"
                }
            ]
        },
        {
            "id": "8c12ab34-4ac7-4b7c-8395-9a58793317de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "compositeImage": {
                "id": "2a44bbd0-afd6-4fd0-89a4-f7816a09aa5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c12ab34-4ac7-4b7c-8395-9a58793317de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19250380-d3c1-45ef-a8c8-5d037a677b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c12ab34-4ac7-4b7c-8395-9a58793317de",
                    "LayerId": "5253b132-b79b-4fe3-9568-417e18c257cc"
                }
            ]
        },
        {
            "id": "648adb6e-e87f-43ef-b6c6-2598133dac8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "compositeImage": {
                "id": "cff736c6-3bc7-4afc-9c65-4f40c4b89dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "648adb6e-e87f-43ef-b6c6-2598133dac8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eebac9fa-8e82-4a12-8999-061aa62e4d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "648adb6e-e87f-43ef-b6c6-2598133dac8b",
                    "LayerId": "5253b132-b79b-4fe3-9568-417e18c257cc"
                }
            ]
        },
        {
            "id": "5f18f234-23d4-44aa-aff8-d8ab09e68daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "compositeImage": {
                "id": "3e10542a-bc1e-4965-826b-9805dc3c0743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f18f234-23d4-44aa-aff8-d8ab09e68daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "052402f3-b0c7-4ce7-91d9-c868fbd5df18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f18f234-23d4-44aa-aff8-d8ab09e68daf",
                    "LayerId": "5253b132-b79b-4fe3-9568-417e18c257cc"
                }
            ]
        },
        {
            "id": "e2ce20ca-2e07-4cfb-a356-e32ace1ddaf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "compositeImage": {
                "id": "477cc86c-c871-4fc3-bd20-66b9b7ea0641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2ce20ca-2e07-4cfb-a356-e32ace1ddaf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b40613-689c-47e1-af5c-960adb43073f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2ce20ca-2e07-4cfb-a356-e32ace1ddaf1",
                    "LayerId": "5253b132-b79b-4fe3-9568-417e18c257cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "5253b132-b79b-4fe3-9568-417e18c257cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03a68df8-fdde-4c5d-a91f-17d8e99c1b91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 18
}