{
    "id": "e19f206a-de28-4612-ae5b-0bd40369d38b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship15button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "400ff66d-e85d-4d1a-8561-f1f4806ef1b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e19f206a-de28-4612-ae5b-0bd40369d38b",
            "compositeImage": {
                "id": "781d2d88-978e-4164-b3d6-51a1ba7d3248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "400ff66d-e85d-4d1a-8561-f1f4806ef1b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76ed93ee-5e43-40be-970b-d82b2d9a33a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "400ff66d-e85d-4d1a-8561-f1f4806ef1b7",
                    "LayerId": "601c7d05-cfa2-49c0-8c4d-c1ea233ecbaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "601c7d05-cfa2-49c0-8c4d-c1ea233ecbaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e19f206a-de28-4612-ae5b-0bd40369d38b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}