{
    "id": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship24debri2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 68,
    "bbox_left": 0,
    "bbox_right": 67,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ad19214-cd50-40c3-a5e3-a740210e055a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "97459433-2cce-47ab-868b-1d4b307e5398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ad19214-cd50-40c3-a5e3-a740210e055a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8ca4232-b07d-48d6-ac2d-ad21dfab39ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ad19214-cd50-40c3-a5e3-a740210e055a",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "4f912d36-f0de-4006-aa8a-597c16416211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "1ce27fbb-1f92-4ed4-b7e5-9916e9daf8b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f912d36-f0de-4006-aa8a-597c16416211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad660ef4-849e-45fd-90cd-7dc6dad48938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f912d36-f0de-4006-aa8a-597c16416211",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "aadc4b8d-6407-494f-abca-82f70500f485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "802bf634-2dff-4e70-812b-f3e74e67643b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aadc4b8d-6407-494f-abca-82f70500f485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12107a91-fc4d-49ca-8142-1a87f65e46ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aadc4b8d-6407-494f-abca-82f70500f485",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "829c2596-9093-4999-9987-5a019c6f7f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "c7efbdbb-0838-4cad-999c-453a95dc86c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "829c2596-9093-4999-9987-5a019c6f7f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23937594-cca9-486d-a634-f95470a24153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "829c2596-9093-4999-9987-5a019c6f7f28",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "bd8d410d-1d2e-4462-aee8-aa1ae484645b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "9cc0ec22-3e81-4f47-8b97-030d38958d2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd8d410d-1d2e-4462-aee8-aa1ae484645b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc37cd5f-c390-4af8-a3c5-c6b142ae1056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd8d410d-1d2e-4462-aee8-aa1ae484645b",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "96ef5b25-b8a3-4bd5-8e36-f2859c579850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "3d5c1b3d-8487-4fca-a0f8-ca377f230db7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ef5b25-b8a3-4bd5-8e36-f2859c579850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e885c79f-6c48-4e28-adae-4218df79aae6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ef5b25-b8a3-4bd5-8e36-f2859c579850",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "223abc6b-6cbf-4685-ae62-ba5fab3760a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "f91633ab-c126-4d42-811b-e9f25c5f2303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "223abc6b-6cbf-4685-ae62-ba5fab3760a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9cf63f2-024e-4e75-ac65-8cdf8d29a90f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "223abc6b-6cbf-4685-ae62-ba5fab3760a5",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "5c8a40bb-28ec-47a4-9965-08d2f89c783a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "0414f487-e26d-4282-aa2f-2ba486163aa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c8a40bb-28ec-47a4-9965-08d2f89c783a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13c37e05-67ad-4843-896b-6064a794f0a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c8a40bb-28ec-47a4-9965-08d2f89c783a",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "a2910e62-2d49-4050-86a3-ba14be502fea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "75f7b399-05fb-4f70-b5e6-76d79552201c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2910e62-2d49-4050-86a3-ba14be502fea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43ad7153-7302-48bf-8653-ce907df74a53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2910e62-2d49-4050-86a3-ba14be502fea",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "18369b13-91b8-4f48-a1ee-06bf40c928f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "5ea3e83a-e254-4ba3-b097-0ebe7061dd03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18369b13-91b8-4f48-a1ee-06bf40c928f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76fec7a0-04f9-487c-a5fc-31af17f4221d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18369b13-91b8-4f48-a1ee-06bf40c928f4",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "0d6743b7-1dff-44c7-b47c-7160c70fe573",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "fe8b3321-dcce-457e-b244-8537596e0f54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d6743b7-1dff-44c7-b47c-7160c70fe573",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e16d91f5-e2bb-4213-b3f6-8a24e4fb87e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d6743b7-1dff-44c7-b47c-7160c70fe573",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "10fa7df2-615a-4451-9c6e-0a97db562aa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "ab372ceb-317c-41ed-9567-8a82394fe6df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10fa7df2-615a-4451-9c6e-0a97db562aa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ee55289-c502-4aa2-b6d6-e69e269f80af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10fa7df2-615a-4451-9c6e-0a97db562aa8",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "608dba97-7f5a-435f-8550-5018dca13ca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "122fc7c1-b4f4-4bea-9377-0d8695dbe429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "608dba97-7f5a-435f-8550-5018dca13ca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef6cdc20-a44d-4e63-b239-5c8af965bc2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "608dba97-7f5a-435f-8550-5018dca13ca7",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "e3eee6d0-cb94-48c9-b69d-e602595c9e51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "4f542917-9ff1-4892-9425-ffd9b22767eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3eee6d0-cb94-48c9-b69d-e602595c9e51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82ab8f37-ba69-43e0-9623-9c163c05f732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3eee6d0-cb94-48c9-b69d-e602595c9e51",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        },
        {
            "id": "2330ca6c-7b00-42e9-b023-03cc7cc6f29d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "compositeImage": {
                "id": "1c17f111-0671-4860-a934-9a00b0577e95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2330ca6c-7b00-42e9-b023-03cc7cc6f29d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c8f09bf-6c85-41ce-b5e3-f5f9e1eabc16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2330ca6c-7b00-42e9-b023-03cc7cc6f29d",
                    "LayerId": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 69,
    "layers": [
        {
            "id": "bcd043da-a8c3-4822-8dfd-49ff22fdd58b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6aefdacf-4027-4b10-b2bd-1ded4037a9d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 56,
    "yorig": -13
}