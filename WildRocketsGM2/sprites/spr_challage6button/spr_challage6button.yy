{
    "id": "35e27fdc-4594-41a2-9cfb-1c2d56c2f790",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage6button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b5a1dec-94a8-4911-a7a0-f959adfd7c62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35e27fdc-4594-41a2-9cfb-1c2d56c2f790",
            "compositeImage": {
                "id": "51072622-e48f-4765-b660-bcaa964da454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b5a1dec-94a8-4911-a7a0-f959adfd7c62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13ee4b09-b7c7-4468-b78b-d7bc4b2c8c2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b5a1dec-94a8-4911-a7a0-f959adfd7c62",
                    "LayerId": "fba7491f-c66c-43d1-a3c2-2e35faa004a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "fba7491f-c66c-43d1-a3c2-2e35faa004a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35e27fdc-4594-41a2-9cfb-1c2d56c2f790",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}