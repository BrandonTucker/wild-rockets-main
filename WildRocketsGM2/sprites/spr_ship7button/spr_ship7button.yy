{
    "id": "6179ef99-9817-4d07-a661-3345becb1beb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship7button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d322168c-183f-4072-9dd4-c55bbceec7d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6179ef99-9817-4d07-a661-3345becb1beb",
            "compositeImage": {
                "id": "d268e552-6c93-4d07-9984-c1253f535684",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d322168c-183f-4072-9dd4-c55bbceec7d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d241a34-cdef-4218-9220-6a644c3d64d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d322168c-183f-4072-9dd4-c55bbceec7d9",
                    "LayerId": "29702993-69a5-4f48-911c-ec3799719eef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "29702993-69a5-4f48-911c-ec3799719eef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6179ef99-9817-4d07-a661-3345becb1beb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}