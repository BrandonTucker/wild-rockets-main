{
    "id": "e7915e79-8398-4c9d-8d2f-34716e045eb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage17button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 1,
    "bbox_right": 99,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "942f5b1a-2bd0-46ec-b112-80e1940b857e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7915e79-8398-4c9d-8d2f-34716e045eb6",
            "compositeImage": {
                "id": "19db31e5-7bbd-4602-9281-bb249e37abeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "942f5b1a-2bd0-46ec-b112-80e1940b857e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0709bf39-98db-4296-b4c3-6621a57e60fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "942f5b1a-2bd0-46ec-b112-80e1940b857e",
                    "LayerId": "ff744960-b19f-4680-ab03-e0c7da949880"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "ff744960-b19f-4680-ab03-e0c7da949880",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7915e79-8398-4c9d-8d2f-34716e045eb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}