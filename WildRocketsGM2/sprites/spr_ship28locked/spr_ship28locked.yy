{
    "id": "a0a65eea-0774-4c60-9aa1-6819fc94064c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship28locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7dcc504a-8ed6-4985-8f44-baad9d787277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0a65eea-0774-4c60-9aa1-6819fc94064c",
            "compositeImage": {
                "id": "cf6e2e20-8aac-4dbb-b19e-98156e67685b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dcc504a-8ed6-4985-8f44-baad9d787277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83682e23-4afe-4993-8fa3-26b7da4da08d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dcc504a-8ed6-4985-8f44-baad9d787277",
                    "LayerId": "92a46512-20a6-4ca0-a0db-477a26a03fd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "92a46512-20a6-4ca0-a0db-477a26a03fd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0a65eea-0774-4c60-9aa1-6819fc94064c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}