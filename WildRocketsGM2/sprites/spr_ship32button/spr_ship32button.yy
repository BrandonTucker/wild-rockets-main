{
    "id": "0d194405-e561-4fb4-b220-c80f71569445",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship32button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6e43b83-2f78-4a33-b716-68d22b2929d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d194405-e561-4fb4-b220-c80f71569445",
            "compositeImage": {
                "id": "626c10a6-0038-4669-8913-fca88b61cf71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e43b83-2f78-4a33-b716-68d22b2929d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9ca8dd9-1473-40cc-866c-e1546559d314",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e43b83-2f78-4a33-b716-68d22b2929d6",
                    "LayerId": "b2c5710c-fa77-4727-87b5-765fdd39705e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "b2c5710c-fa77-4727-87b5-765fdd39705e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d194405-e561-4fb4-b220-c80f71569445",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}