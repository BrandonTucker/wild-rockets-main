{
    "id": "dd7ce032-bf73-4fe2-9cb2-ca7f6d845b8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship40boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 47,
    "bbox_right": 72,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a556aed1-13ec-42f2-b9be-775f91701c02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd7ce032-bf73-4fe2-9cb2-ca7f6d845b8a",
            "compositeImage": {
                "id": "14ac0e22-90d6-4acc-aae7-50ecc6228efe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a556aed1-13ec-42f2-b9be-775f91701c02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "803cfee7-a27c-4c06-b131-739d2464e085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a556aed1-13ec-42f2-b9be-775f91701c02",
                    "LayerId": "7505efb0-2ec9-445a-8637-101a3be7442d"
                }
            ]
        },
        {
            "id": "8aa5ade3-66aa-4866-a7fe-7419671bdf29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd7ce032-bf73-4fe2-9cb2-ca7f6d845b8a",
            "compositeImage": {
                "id": "94ae270f-83eb-4df8-9813-ec6b85a38d15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aa5ade3-66aa-4866-a7fe-7419671bdf29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24c32247-99a0-4092-bb55-04621296ae05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aa5ade3-66aa-4866-a7fe-7419671bdf29",
                    "LayerId": "7505efb0-2ec9-445a-8637-101a3be7442d"
                }
            ]
        },
        {
            "id": "665c08df-c2cc-4768-9319-a96b7eee54ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd7ce032-bf73-4fe2-9cb2-ca7f6d845b8a",
            "compositeImage": {
                "id": "35312416-a7de-4ef0-8bb6-07536c055f7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "665c08df-c2cc-4768-9319-a96b7eee54ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9b59f8b-019c-4199-8bf6-75b942712548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "665c08df-c2cc-4768-9319-a96b7eee54ad",
                    "LayerId": "7505efb0-2ec9-445a-8637-101a3be7442d"
                }
            ]
        },
        {
            "id": "91e29fa8-6446-46dc-86e3-91ce742fe44b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd7ce032-bf73-4fe2-9cb2-ca7f6d845b8a",
            "compositeImage": {
                "id": "91765758-31e1-4bc4-9ac6-641615198863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91e29fa8-6446-46dc-86e3-91ce742fe44b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b38a23ab-1d0d-4c61-b50e-6897096b28fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91e29fa8-6446-46dc-86e3-91ce742fe44b",
                    "LayerId": "7505efb0-2ec9-445a-8637-101a3be7442d"
                }
            ]
        },
        {
            "id": "d0996198-7d9a-4721-bc9e-f3c908ebc4a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd7ce032-bf73-4fe2-9cb2-ca7f6d845b8a",
            "compositeImage": {
                "id": "676944f5-460b-4ad5-8c9f-6aa6f28469e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0996198-7d9a-4721-bc9e-f3c908ebc4a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40bdb6a7-7bfe-4783-adbd-875650b54754",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0996198-7d9a-4721-bc9e-f3c908ebc4a8",
                    "LayerId": "7505efb0-2ec9-445a-8637-101a3be7442d"
                }
            ]
        },
        {
            "id": "419c1fbd-cf87-4539-92f3-2c0bfee7b5e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd7ce032-bf73-4fe2-9cb2-ca7f6d845b8a",
            "compositeImage": {
                "id": "6e366b71-bcff-43e1-beb0-9a111f3ae51d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "419c1fbd-cf87-4539-92f3-2c0bfee7b5e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceb4b8d8-3950-4376-a648-1e0dcfd3ae84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "419c1fbd-cf87-4539-92f3-2c0bfee7b5e1",
                    "LayerId": "7505efb0-2ec9-445a-8637-101a3be7442d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 248,
    "layers": [
        {
            "id": "7505efb0-2ec9-445a-8637-101a3be7442d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd7ce032-bf73-4fe2-9cb2-ca7f6d845b8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 60,
    "yorig": 67
}