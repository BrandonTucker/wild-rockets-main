{
    "id": "fd8333a8-5572-4348-92c4-14791b710ab0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship35",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 61,
    "bbox_right": 86,
    "bbox_top": 34,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b4010d2-ab4e-4c7b-a3cd-5a5a5b7b3308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd8333a8-5572-4348-92c4-14791b710ab0",
            "compositeImage": {
                "id": "e6138718-87f9-48e2-8cc7-0c6ef550cf98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b4010d2-ab4e-4c7b-a3cd-5a5a5b7b3308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b9489cd-c3fb-43cc-8422-ba703b882131",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b4010d2-ab4e-4c7b-a3cd-5a5a5b7b3308",
                    "LayerId": "4270e52a-1a2b-4652-8bcf-87de14e11e98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 158,
    "layers": [
        {
            "id": "4270e52a-1a2b-4652-8bcf-87de14e11e98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd8333a8-5572-4348-92c4-14791b710ab0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 149,
    "xorig": 74,
    "yorig": 77
}