{
    "id": "d322e957-8c48-4740-94a5-c3e31ba3df74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship29explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 37,
    "bbox_right": 62,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "712d39de-d42b-4c87-86e7-4e296e8fae57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d322e957-8c48-4740-94a5-c3e31ba3df74",
            "compositeImage": {
                "id": "63c8a4c8-b5c6-4c2e-8c41-10f60478d3a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "712d39de-d42b-4c87-86e7-4e296e8fae57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af388d9-6596-4ff5-852b-f0d5f7ca447f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "712d39de-d42b-4c87-86e7-4e296e8fae57",
                    "LayerId": "2b72cbba-6274-4368-a7e3-c10b7e8eee16"
                }
            ]
        },
        {
            "id": "136abe1f-e88a-437c-95d6-59cf488918dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d322e957-8c48-4740-94a5-c3e31ba3df74",
            "compositeImage": {
                "id": "629850f5-710e-4066-90f0-4a25d18d2ce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136abe1f-e88a-437c-95d6-59cf488918dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3423bf7d-265b-4053-bcd4-9e400c397eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136abe1f-e88a-437c-95d6-59cf488918dd",
                    "LayerId": "2b72cbba-6274-4368-a7e3-c10b7e8eee16"
                }
            ]
        },
        {
            "id": "607a09f7-93ae-4365-9cdb-215ab420cdb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d322e957-8c48-4740-94a5-c3e31ba3df74",
            "compositeImage": {
                "id": "b4a00685-0a2a-4659-b5f6-44253107265c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "607a09f7-93ae-4365-9cdb-215ab420cdb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d9ab6a2-0b5a-4cea-a074-1abf84d7586e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "607a09f7-93ae-4365-9cdb-215ab420cdb4",
                    "LayerId": "2b72cbba-6274-4368-a7e3-c10b7e8eee16"
                }
            ]
        },
        {
            "id": "81d09975-8753-4ab6-8571-78e6fc529be9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d322e957-8c48-4740-94a5-c3e31ba3df74",
            "compositeImage": {
                "id": "e4d6c808-c850-4fdd-8e4c-3605532ecd13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81d09975-8753-4ab6-8571-78e6fc529be9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de5fb8aa-5470-42aa-8c89-9913d495c564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d09975-8753-4ab6-8571-78e6fc529be9",
                    "LayerId": "2b72cbba-6274-4368-a7e3-c10b7e8eee16"
                }
            ]
        },
        {
            "id": "6466d156-9267-4f30-b91d-4a4e4a8485db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d322e957-8c48-4740-94a5-c3e31ba3df74",
            "compositeImage": {
                "id": "9a5a68d5-a9e9-40cd-94ac-9f99dff6c7bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6466d156-9267-4f30-b91d-4a4e4a8485db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1a6c8dc-068c-4568-bfd2-27abce48cf78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6466d156-9267-4f30-b91d-4a4e4a8485db",
                    "LayerId": "2b72cbba-6274-4368-a7e3-c10b7e8eee16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "2b72cbba-6274-4368-a7e3-c10b7e8eee16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d322e957-8c48-4740-94a5-c3e31ba3df74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 50
}