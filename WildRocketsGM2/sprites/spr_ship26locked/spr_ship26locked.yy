{
    "id": "e8ff5e15-f045-43a1-a54a-6cb97a0f1dc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship26locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "885f6370-adfd-409c-b8ab-a218c1dcaee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8ff5e15-f045-43a1-a54a-6cb97a0f1dc7",
            "compositeImage": {
                "id": "8ff4f8c1-f238-4891-aed6-76535912d7a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "885f6370-adfd-409c-b8ab-a218c1dcaee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8810b9a-be77-4b27-866c-6581168099b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "885f6370-adfd-409c-b8ab-a218c1dcaee7",
                    "LayerId": "f4fb8407-3a95-49d3-9335-d628ecc1c261"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "f4fb8407-3a95-49d3-9335-d628ecc1c261",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8ff5e15-f045-43a1-a54a-6cb97a0f1dc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}