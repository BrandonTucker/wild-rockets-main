{
    "id": "91f88f7c-d662-45c4-8024-c5ccac06baaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background8",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 1311,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a38a3a3-2861-48c6-87a2-dc4339ec44bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91f88f7c-d662-45c4-8024-c5ccac06baaf",
            "compositeImage": {
                "id": "9d61fa1e-ca2b-48ba-a9bb-8165e589ce08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a38a3a3-2861-48c6-87a2-dc4339ec44bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40c84881-e889-445c-8e35-9747a8b38762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a38a3a3-2861-48c6-87a2-dc4339ec44bd",
                    "LayerId": "e3078e9b-3787-40e0-93be-2e91e117ad2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "e3078e9b-3787-40e0-93be-2e91e117ad2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91f88f7c-d662-45c4-8024-c5ccac06baaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1312,
    "xorig": 0,
    "yorig": 0
}