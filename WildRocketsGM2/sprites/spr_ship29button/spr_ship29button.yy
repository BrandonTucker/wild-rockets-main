{
    "id": "f4821cc2-8c67-40b3-b4c6-d88a94bd2f21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship29button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43fe4785-93e6-4016-95f2-da2ab5bf6061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4821cc2-8c67-40b3-b4c6-d88a94bd2f21",
            "compositeImage": {
                "id": "f7ff3256-cf47-47ab-abda-1e70c7376ca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43fe4785-93e6-4016-95f2-da2ab5bf6061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2f2b301-12d6-4047-989a-20d8dc2c8826",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43fe4785-93e6-4016-95f2-da2ab5bf6061",
                    "LayerId": "9ad5ffbd-8072-4b93-a898-e35d005fe383"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "9ad5ffbd-8072-4b93-a898-e35d005fe383",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4821cc2-8c67-40b3-b4c6-d88a94bd2f21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}