{
    "id": "dd66ba7f-a222-4ccc-b45a-2f08b04bb514",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship19locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8395b5c9-7705-40d1-8bc4-12244afe3c10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd66ba7f-a222-4ccc-b45a-2f08b04bb514",
            "compositeImage": {
                "id": "a5f56620-21ad-466b-a41a-01509bc24af1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8395b5c9-7705-40d1-8bc4-12244afe3c10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f30cd5eb-6064-4b34-9bd9-d1f2097e0e86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8395b5c9-7705-40d1-8bc4-12244afe3c10",
                    "LayerId": "6c4fe7d5-fc6e-4156-bb0d-188719845020"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6c4fe7d5-fc6e-4156-bb0d-188719845020",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd66ba7f-a222-4ccc-b45a-2f08b04bb514",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}