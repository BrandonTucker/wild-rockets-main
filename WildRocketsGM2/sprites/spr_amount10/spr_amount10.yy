{
    "id": "a4b5c361-2a55-433e-878b-58e01f9433fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_amount10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 5,
    "bbox_right": 64,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a401f9ae-67f6-42ff-b824-53559b674483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "f3707be7-574c-47f2-80e6-9258a71adfde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a401f9ae-67f6-42ff-b824-53559b674483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3ee2df3-7df3-4771-a599-68bf9eaf51ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a401f9ae-67f6-42ff-b824-53559b674483",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "60a3f367-d2ab-4cae-9d63-2092af2cf7da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "edecbc66-82fd-4f90-a03e-cd31c9879ffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a3f367-d2ab-4cae-9d63-2092af2cf7da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98f0dd9d-be93-4463-8808-6355a9d2964a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a3f367-d2ab-4cae-9d63-2092af2cf7da",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "ff64f09a-60a5-46e7-b984-186f36b8b59b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "d9e24b59-96be-41d4-8409-fe6433c98463",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff64f09a-60a5-46e7-b984-186f36b8b59b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "855f3b34-9fb2-4656-b3db-1c9a6f94a863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff64f09a-60a5-46e7-b984-186f36b8b59b",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "0a1f75f4-1215-4c2c-94c9-62c8e14dbce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "ffdbed8b-d4a5-45b6-80ed-71d1d87cdd0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a1f75f4-1215-4c2c-94c9-62c8e14dbce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9270d577-07f6-4d9a-8add-2234ce174441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a1f75f4-1215-4c2c-94c9-62c8e14dbce4",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "4c93d2b0-d2d7-44be-81e9-56d6caf5f3b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "8aaabbd1-6ac4-492f-bc9e-9fa915f3aaed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c93d2b0-d2d7-44be-81e9-56d6caf5f3b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f84bf8d7-abd0-4ee9-ab7a-fa701f439b80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c93d2b0-d2d7-44be-81e9-56d6caf5f3b3",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "405784b4-933a-433b-b6b3-4990f03d9dec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "e09586ee-ef31-4314-87f4-cb78dedb59ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "405784b4-933a-433b-b6b3-4990f03d9dec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "266b4538-3567-4109-8a74-4145f5524915",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "405784b4-933a-433b-b6b3-4990f03d9dec",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "57f08151-b981-41bf-ad78-47c1c16d11d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "ec168aba-b018-4034-889a-96715e45e004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57f08151-b981-41bf-ad78-47c1c16d11d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19c30818-2e34-4f94-a0e7-794fc94cf443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57f08151-b981-41bf-ad78-47c1c16d11d3",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "7e3b7346-5bda-45af-8405-4b11496ee0c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "0d79966e-ec56-4d12-9198-62e38a881e6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e3b7346-5bda-45af-8405-4b11496ee0c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60b36d70-52a1-4963-9cce-d7f32d59db79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e3b7346-5bda-45af-8405-4b11496ee0c4",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "f38cbfa7-c088-416e-81fd-73155b0e7075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "dd9d6890-932a-413d-ad48-46c6e421ef30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f38cbfa7-c088-416e-81fd-73155b0e7075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7560382-2b2e-4351-baf2-b20ad7470f41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f38cbfa7-c088-416e-81fd-73155b0e7075",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "d1311772-a1d2-44de-b1c6-4669489e8909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "8359eeae-4a8d-41c6-977e-c7823368cdd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1311772-a1d2-44de-b1c6-4669489e8909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dccf0a1-26bd-4520-b1a9-5d5decd5d3f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1311772-a1d2-44de-b1c6-4669489e8909",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "fc77345f-b785-401d-a7b9-49805649120f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "125ea423-afe3-4a95-b482-6a11daddb145",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc77345f-b785-401d-a7b9-49805649120f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74ee60ec-47ab-445f-882d-dad77158b1dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc77345f-b785-401d-a7b9-49805649120f",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        },
        {
            "id": "3e91889c-1ee5-497e-bd75-22e9a5cb767e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "compositeImage": {
                "id": "34373605-5703-4877-833d-ca818ccc69c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e91889c-1ee5-497e-bd75-22e9a5cb767e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8862d90a-679b-41fe-b758-4d8b01f196d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e91889c-1ee5-497e-bd75-22e9a5cb767e",
                    "LayerId": "6f3f1404-3001-4d8a-a2ae-8572cc82c395"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "6f3f1404-3001-4d8a-a2ae-8572cc82c395",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4b5c361-2a55-433e-878b-58e01f9433fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 41
}