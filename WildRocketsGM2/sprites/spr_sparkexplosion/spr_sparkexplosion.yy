{
    "id": "cc9cfa6d-858e-4bcd-90ed-57c4d505560e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sparkexplosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "771512cb-2c02-4acb-9ebd-f4290ca2269a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc9cfa6d-858e-4bcd-90ed-57c4d505560e",
            "compositeImage": {
                "id": "69bd71b3-e614-4005-8f5a-98f23eb4f811",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "771512cb-2c02-4acb-9ebd-f4290ca2269a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61f7af46-3278-4461-9e54-549c3f61b79f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "771512cb-2c02-4acb-9ebd-f4290ca2269a",
                    "LayerId": "6e60b879-71cf-477d-a11e-348b25563e8a"
                }
            ]
        },
        {
            "id": "b8040995-5272-4a7c-8a30-403bb67781bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc9cfa6d-858e-4bcd-90ed-57c4d505560e",
            "compositeImage": {
                "id": "7e761d25-4064-4827-abdb-86815b84c7d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8040995-5272-4a7c-8a30-403bb67781bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2b81caf-4e22-4734-9510-1a4094ba96bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8040995-5272-4a7c-8a30-403bb67781bd",
                    "LayerId": "6e60b879-71cf-477d-a11e-348b25563e8a"
                }
            ]
        },
        {
            "id": "f2e6fb08-22ef-4aab-a0b2-2abaf7944943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc9cfa6d-858e-4bcd-90ed-57c4d505560e",
            "compositeImage": {
                "id": "db3c1b6e-0df5-42fc-b5ed-84222a562034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2e6fb08-22ef-4aab-a0b2-2abaf7944943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67912328-3117-4657-8d63-23a2d1bc981e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2e6fb08-22ef-4aab-a0b2-2abaf7944943",
                    "LayerId": "6e60b879-71cf-477d-a11e-348b25563e8a"
                }
            ]
        },
        {
            "id": "927bdc01-02b9-4737-9fdb-7a42c75babd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc9cfa6d-858e-4bcd-90ed-57c4d505560e",
            "compositeImage": {
                "id": "a415b7a8-33e4-4017-8a5c-4740f3cf5221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "927bdc01-02b9-4737-9fdb-7a42c75babd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "453316be-80b5-4338-a4b3-d51747f17654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "927bdc01-02b9-4737-9fdb-7a42c75babd6",
                    "LayerId": "6e60b879-71cf-477d-a11e-348b25563e8a"
                }
            ]
        },
        {
            "id": "ce9b1300-ae15-4aae-bf3d-b071676f772d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc9cfa6d-858e-4bcd-90ed-57c4d505560e",
            "compositeImage": {
                "id": "88b6bc2a-6642-411c-8ddd-dad7856c8b33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce9b1300-ae15-4aae-bf3d-b071676f772d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78c64b72-1028-4fa7-9e20-85b908911e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce9b1300-ae15-4aae-bf3d-b071676f772d",
                    "LayerId": "6e60b879-71cf-477d-a11e-348b25563e8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "6e60b879-71cf-477d-a11e-348b25563e8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc9cfa6d-858e-4bcd-90ed-57c4d505560e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}