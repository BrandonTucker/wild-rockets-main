{
    "id": "999bd816-8357-46d1-b0e0-7fa64dcc2930",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship40locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "981e4171-acbb-4bf9-83c6-15e8c6a37c1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "999bd816-8357-46d1-b0e0-7fa64dcc2930",
            "compositeImage": {
                "id": "249a6d23-3f06-4fbf-b524-e6862b8d4af4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "981e4171-acbb-4bf9-83c6-15e8c6a37c1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59d553e9-9aec-4a3d-9b5f-de1abc2d1bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "981e4171-acbb-4bf9-83c6-15e8c6a37c1e",
                    "LayerId": "2d1b22ae-abd2-4ffc-b284-7fa59af135ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "2d1b22ae-abd2-4ffc-b284-7fa59af135ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "999bd816-8357-46d1-b0e0-7fa64dcc2930",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}