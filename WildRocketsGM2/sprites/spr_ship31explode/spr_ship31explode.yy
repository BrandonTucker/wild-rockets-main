{
    "id": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship31explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 326,
    "bbox_left": 0,
    "bbox_right": 302,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a07f7c1-02fb-425b-97df-395e28e5c7fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "8eb39a22-3d92-4029-a7f8-1d6865e22bc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a07f7c1-02fb-425b-97df-395e28e5c7fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e6fc41-e1d1-487f-982c-23dd1266b3ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a07f7c1-02fb-425b-97df-395e28e5c7fe",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        },
        {
            "id": "b00c3bf4-a07f-4aff-99a5-a6846f607333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "143f18d5-9457-44e0-a413-e26354ada239",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b00c3bf4-a07f-4aff-99a5-a6846f607333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d044d197-6fa8-4875-842c-70b2396ed542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b00c3bf4-a07f-4aff-99a5-a6846f607333",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        },
        {
            "id": "0ebd8021-5e19-4276-ade5-cb92b7cd8cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "e0db775e-12bd-455e-b073-fdb7bf0206a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ebd8021-5e19-4276-ade5-cb92b7cd8cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3e5b17-9495-426f-85e9-f9ac6103b7dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ebd8021-5e19-4276-ade5-cb92b7cd8cca",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        },
        {
            "id": "b4dec52e-3da0-43be-8110-417ff48a7972",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "c6897241-bf07-428a-a88e-45b4e6273cb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4dec52e-3da0-43be-8110-417ff48a7972",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9ddc26b-a3ed-4d7f-9413-db613883c24b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4dec52e-3da0-43be-8110-417ff48a7972",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        },
        {
            "id": "4a71b6bd-e4d4-451a-9026-59f4fe055de3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "a7a20370-eba5-4e9c-ad89-6cdbb1eb0a12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a71b6bd-e4d4-451a-9026-59f4fe055de3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee3ee5d8-e6d4-45fd-9f63-1e82f2fa45c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a71b6bd-e4d4-451a-9026-59f4fe055de3",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        },
        {
            "id": "d645bc8c-0282-4121-bebf-ecf02b05ac26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "6a4acdef-7169-45c8-a371-da88baaaf4ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d645bc8c-0282-4121-bebf-ecf02b05ac26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "319f1026-6ad8-478f-8779-7e7fbf2bac68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d645bc8c-0282-4121-bebf-ecf02b05ac26",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        },
        {
            "id": "524cd861-572a-4b9b-aab3-64f13a427300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "a80dee46-b0d3-4762-85b9-649ac75d60e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "524cd861-572a-4b9b-aab3-64f13a427300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d617b697-7ae0-47f0-b81e-77f59b2f82e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "524cd861-572a-4b9b-aab3-64f13a427300",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        },
        {
            "id": "227a7840-3f25-4a5d-a312-a59050a5db37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "b0ea0604-5687-4de3-a9e9-bfd0b4e4dd0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "227a7840-3f25-4a5d-a312-a59050a5db37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc4cba18-4f98-4279-a2c6-61871f3584b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "227a7840-3f25-4a5d-a312-a59050a5db37",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        },
        {
            "id": "df665d9f-6f6b-4330-be50-b715685e8dba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "65f43c4f-5694-4cbf-8477-7258ab0ed658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df665d9f-6f6b-4330-be50-b715685e8dba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ecfb3fd-e73c-4a58-9b43-a9f022ddd33f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df665d9f-6f6b-4330-be50-b715685e8dba",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        },
        {
            "id": "3536af5b-e3ce-481b-9016-8ed9ffe2378f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "58e1c432-4a48-4cc0-b267-81c4bf526917",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3536af5b-e3ce-481b-9016-8ed9ffe2378f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "965e4939-e626-4574-b84d-5c4cde71bc4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3536af5b-e3ce-481b-9016-8ed9ffe2378f",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        },
        {
            "id": "f07b1ffe-a1cd-4f1e-a068-c19e112113a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "compositeImage": {
                "id": "bc34a686-ae29-42fc-a0ad-75f9a41d294f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f07b1ffe-a1cd-4f1e-a068-c19e112113a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bf30289-0cfa-4c19-bf0a-2eb14c3ff6ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f07b1ffe-a1cd-4f1e-a068-c19e112113a5",
                    "LayerId": "c77e1ec9-556b-4a69-a12d-57d8ebe58519"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 375,
    "layers": [
        {
            "id": "c77e1ec9-556b-4a69-a12d-57d8ebe58519",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99b78516-c1c3-4ca0-b262-fb5f5c64d516",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 303,
    "xorig": 152,
    "yorig": 168
}