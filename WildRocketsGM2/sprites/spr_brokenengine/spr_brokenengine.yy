{
    "id": "823487e4-0d1e-4062-852e-41448de0e808",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_brokenengine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 5,
    "bbox_right": 39,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eacaebd0-97da-40d4-a17e-560a51b96f66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "4e67924b-60b4-468a-bfc2-fcc86f6c46a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eacaebd0-97da-40d4-a17e-560a51b96f66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c1aa49d-f267-45d1-81f1-5b68ad8ba8cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eacaebd0-97da-40d4-a17e-560a51b96f66",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "d8baff27-3d89-4b77-8269-f65ccff81b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "ebeb15f7-afcb-4fac-89a9-934905940340",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8baff27-3d89-4b77-8269-f65ccff81b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf250b40-5604-4e71-8efb-7edef8e35a5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8baff27-3d89-4b77-8269-f65ccff81b1d",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "8f3c23b0-f9c7-4b90-9824-82ef4edf5d48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "8c38d355-f30c-4e67-afb2-0d9b4bd80153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f3c23b0-f9c7-4b90-9824-82ef4edf5d48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3335f74-6822-49f5-a2fc-8b6fc6f9ae2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f3c23b0-f9c7-4b90-9824-82ef4edf5d48",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "466b0a98-5675-4a88-9a53-08946e2ded3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "21f1315b-2b0b-44cb-952a-edcc4510b33b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "466b0a98-5675-4a88-9a53-08946e2ded3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b379fd4-b730-483c-9f93-d15c3252a73f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "466b0a98-5675-4a88-9a53-08946e2ded3b",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "85bd15e3-43fa-4280-91b1-1c8809f588d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "43bbc1bc-49b9-478f-b327-63e036e60e79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85bd15e3-43fa-4280-91b1-1c8809f588d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b68653-d6ef-4ecc-8d00-f0ff2ebb4705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bd15e3-43fa-4280-91b1-1c8809f588d9",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "33ca1f3b-d24a-4b39-9688-9a099953e229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "1b6be5ea-9607-4d63-8338-8ffe4567c0cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ca1f3b-d24a-4b39-9688-9a099953e229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c2fdc27-c0a3-49d1-b239-e0b446d93ddc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ca1f3b-d24a-4b39-9688-9a099953e229",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "0ac4bb89-623f-4f45-bdfb-592622cd7624",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "dc89e422-cdbd-4785-9f46-f33a1f3c9964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ac4bb89-623f-4f45-bdfb-592622cd7624",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b633030-2345-4594-b168-4eb424f7f281",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ac4bb89-623f-4f45-bdfb-592622cd7624",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "f34e2c68-27c9-453e-9d03-095b1cc2d553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "17c39845-f95f-4a69-aadc-109c996b2f39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f34e2c68-27c9-453e-9d03-095b1cc2d553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "781fcb44-df65-45f7-9542-3a70af529567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f34e2c68-27c9-453e-9d03-095b1cc2d553",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "65dbc6bb-0f11-46e5-91d7-b565c79e574c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "fd84d086-751d-4a9f-974a-65b1cd3655ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65dbc6bb-0f11-46e5-91d7-b565c79e574c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa360594-73ac-4eeb-9404-dc8da4d9a98e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65dbc6bb-0f11-46e5-91d7-b565c79e574c",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "f76ff6fd-d067-436a-a165-f2d4b87de4e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "1b9dfaaa-8481-4be0-8ead-3d10419982fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76ff6fd-d067-436a-a165-f2d4b87de4e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ebe192d-cfdd-4ac1-a2f4-9c0fc78292c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76ff6fd-d067-436a-a165-f2d4b87de4e0",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "5fe26cb6-7f94-468a-852a-5f2819d49a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "2566b19e-5cd3-4a3e-96d6-dc92cd873cd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fe26cb6-7f94-468a-852a-5f2819d49a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40b3d56-64ed-477d-b676-931be4fc5b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fe26cb6-7f94-468a-852a-5f2819d49a0c",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "bc5d1b2a-8976-450a-9592-91756b509e76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "d6e1c552-1890-48e9-a663-39fb644db6e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc5d1b2a-8976-450a-9592-91756b509e76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f319c0d-8042-4dcd-8282-f8b77952af1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc5d1b2a-8976-450a-9592-91756b509e76",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "a4965dec-9dfa-4a2b-bce8-ff6cc2281a0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "2a6564ca-9cba-4c19-a737-d07e68f83cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4965dec-9dfa-4a2b-bce8-ff6cc2281a0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1daf780c-d307-4f7d-8fe1-a8e98aaf6f89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4965dec-9dfa-4a2b-bce8-ff6cc2281a0f",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "342b5209-3cb8-496e-8ee8-340e6a778dcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "42a78a60-a5a7-4d37-8045-5bafcee6dd24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "342b5209-3cb8-496e-8ee8-340e6a778dcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81a5408b-c2d9-4575-8f2d-383556f8c22d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "342b5209-3cb8-496e-8ee8-340e6a778dcd",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        },
        {
            "id": "e0b7e574-79f4-4a94-91a3-c7e06359e992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "compositeImage": {
                "id": "8c8086af-dd50-43b5-8376-4a3af94cc89c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0b7e574-79f4-4a94-91a3-c7e06359e992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c265186-4e22-4f00-83af-569160bea20b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0b7e574-79f4-4a94-91a3-c7e06359e992",
                    "LayerId": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "9c0adbcc-0dec-4015-9c11-7dac6eeb61f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "823487e4-0d1e-4062-852e-41448de0e808",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 12,
    "yorig": -16
}