{
    "id": "37e8a39f-9d41-45a8-8c11-e1e9f4f0bd18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship38boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 53,
    "bbox_right": 78,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff460dde-8a34-45e8-b04f-b6afdcf6693e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37e8a39f-9d41-45a8-8c11-e1e9f4f0bd18",
            "compositeImage": {
                "id": "39be5b4b-f0d1-4aba-a81b-73cea8de4124",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff460dde-8a34-45e8-b04f-b6afdcf6693e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "041c82d5-000f-4e5b-aee0-505a40515b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff460dde-8a34-45e8-b04f-b6afdcf6693e",
                    "LayerId": "389762dc-e7ba-4cb5-8316-1d63905fbcb2"
                }
            ]
        },
        {
            "id": "4511fbc5-1570-41c5-8b64-4df21f032575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37e8a39f-9d41-45a8-8c11-e1e9f4f0bd18",
            "compositeImage": {
                "id": "0b88d0f3-df8f-4e4b-a5a3-c7d4329d4a40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4511fbc5-1570-41c5-8b64-4df21f032575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f9eaef0-eba4-4dc6-b3ca-354bf25854f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4511fbc5-1570-41c5-8b64-4df21f032575",
                    "LayerId": "389762dc-e7ba-4cb5-8316-1d63905fbcb2"
                }
            ]
        },
        {
            "id": "e3032889-32c9-4e2f-8b1f-af0fb799d609",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37e8a39f-9d41-45a8-8c11-e1e9f4f0bd18",
            "compositeImage": {
                "id": "aee053e5-df69-40e9-8186-484b45137eb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3032889-32c9-4e2f-8b1f-af0fb799d609",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f482c76-0716-48c1-b852-e6e5cecb6d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3032889-32c9-4e2f-8b1f-af0fb799d609",
                    "LayerId": "389762dc-e7ba-4cb5-8316-1d63905fbcb2"
                }
            ]
        },
        {
            "id": "21e5bf4a-ab4b-4897-b99b-4c8b16b18c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37e8a39f-9d41-45a8-8c11-e1e9f4f0bd18",
            "compositeImage": {
                "id": "163aa00b-91ff-48c1-b720-c36c28080690",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21e5bf4a-ab4b-4897-b99b-4c8b16b18c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13ee8b92-218d-4dec-a22b-40628703df16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21e5bf4a-ab4b-4897-b99b-4c8b16b18c9e",
                    "LayerId": "389762dc-e7ba-4cb5-8316-1d63905fbcb2"
                }
            ]
        },
        {
            "id": "2d3c2b96-3227-43c9-bc8b-6af881719afb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37e8a39f-9d41-45a8-8c11-e1e9f4f0bd18",
            "compositeImage": {
                "id": "1457c7a7-0b5b-440e-86d3-d9af29de5165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d3c2b96-3227-43c9-bc8b-6af881719afb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "708127bf-92ee-4a96-b07f-63b00d9dfec4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d3c2b96-3227-43c9-bc8b-6af881719afb",
                    "LayerId": "389762dc-e7ba-4cb5-8316-1d63905fbcb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 195,
    "layers": [
        {
            "id": "389762dc-e7ba-4cb5-8316-1d63905fbcb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37e8a39f-9d41-45a8-8c11-e1e9f4f0bd18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 66,
    "yorig": 51
}