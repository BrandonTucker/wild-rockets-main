{
    "id": "7e01f260-7eab-495f-a0da-a95e7f564650",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship17locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72435f6a-7207-470e-a398-d806ef9e847a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e01f260-7eab-495f-a0da-a95e7f564650",
            "compositeImage": {
                "id": "e36a0ebe-c7c5-4ded-baf2-de6b96338f21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72435f6a-7207-470e-a398-d806ef9e847a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e430070-d04f-4fa4-b4c2-ed47cf1281d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72435f6a-7207-470e-a398-d806ef9e847a",
                    "LayerId": "2e61de1c-0574-484b-b6a2-c0f9ff3918b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "2e61de1c-0574-484b-b6a2-c0f9ff3918b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e01f260-7eab-495f-a0da-a95e7f564650",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}