{
    "id": "57635894-877f-40f6-be3e-cf785e3fdd62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship30explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97267688-ae24-414e-8920-39718505699c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57635894-877f-40f6-be3e-cf785e3fdd62",
            "compositeImage": {
                "id": "c467e99f-2fad-46f5-9ead-8ba747603269",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97267688-ae24-414e-8920-39718505699c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dee7bdd2-7f31-4cce-a761-111667b57157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97267688-ae24-414e-8920-39718505699c",
                    "LayerId": "beee2c92-9225-4784-a07f-cc7608302e2a"
                }
            ]
        },
        {
            "id": "8f4c04c7-1e66-4866-aeae-5a9de7273139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57635894-877f-40f6-be3e-cf785e3fdd62",
            "compositeImage": {
                "id": "5f27279a-b5c8-4151-98fb-e473deffa0c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f4c04c7-1e66-4866-aeae-5a9de7273139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b65af6f-28c3-4e35-a129-70b223a36983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f4c04c7-1e66-4866-aeae-5a9de7273139",
                    "LayerId": "beee2c92-9225-4784-a07f-cc7608302e2a"
                }
            ]
        },
        {
            "id": "c811850a-d6ef-4c97-bbed-7675aa5ee5ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57635894-877f-40f6-be3e-cf785e3fdd62",
            "compositeImage": {
                "id": "9352dc6f-3275-4e4b-845f-421602405be2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c811850a-d6ef-4c97-bbed-7675aa5ee5ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f0e333b-cbc8-4165-b6f6-7e3d678d70e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c811850a-d6ef-4c97-bbed-7675aa5ee5ce",
                    "LayerId": "beee2c92-9225-4784-a07f-cc7608302e2a"
                }
            ]
        },
        {
            "id": "330b9709-cdc6-467c-9ffb-d817ff8ecea5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57635894-877f-40f6-be3e-cf785e3fdd62",
            "compositeImage": {
                "id": "a7ea0dcd-9b55-4965-b67a-4a6e3d0794c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "330b9709-cdc6-467c-9ffb-d817ff8ecea5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "335c1097-0e92-4481-a897-d9f9dd96fc72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "330b9709-cdc6-467c-9ffb-d817ff8ecea5",
                    "LayerId": "beee2c92-9225-4784-a07f-cc7608302e2a"
                }
            ]
        },
        {
            "id": "c246a054-5168-46db-96bc-14079c0ec833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57635894-877f-40f6-be3e-cf785e3fdd62",
            "compositeImage": {
                "id": "1e6548b9-7bd4-4322-bd4d-bf65ac933a4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c246a054-5168-46db-96bc-14079c0ec833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dbd1a32-b53f-4721-ba08-a7f784477ac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c246a054-5168-46db-96bc-14079c0ec833",
                    "LayerId": "beee2c92-9225-4784-a07f-cc7608302e2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "beee2c92-9225-4784-a07f-cc7608302e2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57635894-877f-40f6-be3e-cf785e3fdd62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 50
}