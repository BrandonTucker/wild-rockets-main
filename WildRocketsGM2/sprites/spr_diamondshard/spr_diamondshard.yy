{
    "id": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_diamondshard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e58a100-644c-41b7-8a04-0673cfadd4c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "971dae57-80cd-4a33-ba76-1ab023e986ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e58a100-644c-41b7-8a04-0673cfadd4c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21a051c9-2ca0-4f0c-8a04-eeea279818f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e58a100-644c-41b7-8a04-0673cfadd4c5",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "f76de0e1-89ac-4f05-9c43-a5450218da2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "ffcc5818-0db2-4a5d-ac91-e549a0b2e125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76de0e1-89ac-4f05-9c43-a5450218da2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cdfebeb-2824-458a-833f-d25f841dc1eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76de0e1-89ac-4f05-9c43-a5450218da2e",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "ad763935-5500-4731-bc69-04c2c265fb13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "1e35281c-b078-44e4-8b1e-277411d99a22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad763935-5500-4731-bc69-04c2c265fb13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52a19ad9-d98f-4aaa-bf6f-ac5a2a047e25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad763935-5500-4731-bc69-04c2c265fb13",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "49ecf4a4-6828-49de-8036-760ec54d1262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "946cc83b-ea0b-4757-981e-ea685b9b1144",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49ecf4a4-6828-49de-8036-760ec54d1262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1afad60-b879-4bce-815f-03b917e610f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49ecf4a4-6828-49de-8036-760ec54d1262",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "b96a753b-5f68-42e8-9d6c-a7e899ad3708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "e3311d9e-d847-4d39-8603-8783d09013f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96a753b-5f68-42e8-9d6c-a7e899ad3708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "674fe816-6f9e-4993-bcaa-598574910de2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96a753b-5f68-42e8-9d6c-a7e899ad3708",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "66e578a1-bc22-41af-bfad-2218e54e27e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "37492d42-f521-4718-b974-f29b0e85eb50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66e578a1-bc22-41af-bfad-2218e54e27e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ca211a4-a06f-4e93-a558-f258e84a5f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66e578a1-bc22-41af-bfad-2218e54e27e1",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "cb9bf955-3b2f-4a2c-b5a4-450206d29420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "37cccf16-bf48-4f25-b43f-48ec6f217286",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb9bf955-3b2f-4a2c-b5a4-450206d29420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e22ca35f-43b5-4a31-8a92-f50af980251d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb9bf955-3b2f-4a2c-b5a4-450206d29420",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "57724f56-f51f-46de-9e34-b73cdacc3a27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "51394aea-0b3e-4a12-a0ff-7d9b4fbe1ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57724f56-f51f-46de-9e34-b73cdacc3a27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "773ba8f1-54f5-413e-bf2c-d8054133bcf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57724f56-f51f-46de-9e34-b73cdacc3a27",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "829d01b7-23ba-44a0-90a5-1cde25541bc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "0dec00aa-2142-454a-a4e1-955d76b19eaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "829d01b7-23ba-44a0-90a5-1cde25541bc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d32506e-58f4-435f-9969-1027cf2a93f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "829d01b7-23ba-44a0-90a5-1cde25541bc9",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "682bad9e-2eac-41f0-bbf1-6ce66e6aa5b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "5c1247c0-485a-4ec6-a1be-d36ca04ba704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "682bad9e-2eac-41f0-bbf1-6ce66e6aa5b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e7af9ff-ebfd-4af7-8363-621ba8241670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "682bad9e-2eac-41f0-bbf1-6ce66e6aa5b7",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "c23bba18-4fff-4079-a81e-07560c20ac79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "3e5fcc73-4ed1-4792-8af9-a6c92f7e49d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c23bba18-4fff-4079-a81e-07560c20ac79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9d3617-9fc2-41b9-b16a-cd5c34487c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c23bba18-4fff-4079-a81e-07560c20ac79",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "f334c9c2-f80e-4cf3-9e95-e16c78647b2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "a6dbf069-ca29-4007-86ad-0299dee92aa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f334c9c2-f80e-4cf3-9e95-e16c78647b2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b0d0d6b-9a94-420f-9d1e-c61310686e68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f334c9c2-f80e-4cf3-9e95-e16c78647b2b",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "ea1af9f4-8cf5-43fa-929b-47c6df196a3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "a4816d37-e9eb-4196-a279-efd70996c5a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea1af9f4-8cf5-43fa-929b-47c6df196a3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6975d87e-934b-4936-b5e9-0cd8a3f98eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea1af9f4-8cf5-43fa-929b-47c6df196a3e",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "eefe5f9d-b07e-43b2-8ca8-020d97fa9c19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "0cc8071f-8dc2-4124-a414-c4876eb85e62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eefe5f9d-b07e-43b2-8ca8-020d97fa9c19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbc7ba5d-87d7-4f04-baed-40a60a6a17fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eefe5f9d-b07e-43b2-8ca8-020d97fa9c19",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        },
        {
            "id": "5b3208c0-5f19-442d-a996-b9416ad293d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "compositeImage": {
                "id": "4c75c80d-6940-491b-bb70-36e874380b4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b3208c0-5f19-442d-a996-b9416ad293d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8ce2d7e-b501-45c1-aed6-710459bce72b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b3208c0-5f19-442d-a996-b9416ad293d2",
                    "LayerId": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "6b302097-c1ce-464e-bdfa-1db0d2df5d0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59d8f7f5-9b42-4bf6-8b51-66147b438587",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 19,
    "yorig": 16
}