{
    "id": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_plusLife",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 197,
    "bbox_left": 39,
    "bbox_right": 200,
    "bbox_top": 90,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77442d4f-b251-4d42-a7b5-4fa80f111d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "a0f6300b-b990-45c2-a974-184c6c8216c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77442d4f-b251-4d42-a7b5-4fa80f111d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f41c24-cde1-4c1f-ae2a-c2b35e7a5923",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77442d4f-b251-4d42-a7b5-4fa80f111d1f",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        },
        {
            "id": "8ac48d32-56bb-4dad-a06b-c44bdd0735b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "02ee1045-3328-4df4-8de0-5435d7ef121d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ac48d32-56bb-4dad-a06b-c44bdd0735b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f094806a-0d12-4401-af55-fc035d731b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ac48d32-56bb-4dad-a06b-c44bdd0735b1",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        },
        {
            "id": "c63f71fa-78b3-4a86-9f84-0f8984d9f549",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "42ed5696-c316-4ae0-816b-d2d9db2987f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c63f71fa-78b3-4a86-9f84-0f8984d9f549",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10ff27d9-9c2d-4b05-bafb-8cc6710b0efc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c63f71fa-78b3-4a86-9f84-0f8984d9f549",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        },
        {
            "id": "244fffc9-2630-46a8-97e8-4a10558705c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "98480d2e-324f-452f-a1c2-4e6410b1ab1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "244fffc9-2630-46a8-97e8-4a10558705c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26ba673b-fdce-4274-909c-977542fdc096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244fffc9-2630-46a8-97e8-4a10558705c1",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        },
        {
            "id": "4da05e95-2df9-4aba-9731-8ce69deb2b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "b578487f-ad51-4469-87d5-fa8b1e76a829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4da05e95-2df9-4aba-9731-8ce69deb2b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "978c1992-0e2d-43ee-8b92-585a98216515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4da05e95-2df9-4aba-9731-8ce69deb2b98",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        },
        {
            "id": "5db0bf01-429a-4e94-ab3d-ae314965e739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "51431ce1-c383-417b-9867-73ac5adc90fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5db0bf01-429a-4e94-ab3d-ae314965e739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc0f22d-ae12-4088-977c-3fc03b8c6ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5db0bf01-429a-4e94-ab3d-ae314965e739",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        },
        {
            "id": "c87d6f30-30d6-4e0d-b3e0-bf19ac23c1c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "4a32b519-a5f1-425d-8901-650c3bae7a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c87d6f30-30d6-4e0d-b3e0-bf19ac23c1c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f7fad10-8c09-42c2-bb4d-ed128fd1b558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c87d6f30-30d6-4e0d-b3e0-bf19ac23c1c6",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        },
        {
            "id": "3c7120cc-e2b4-4950-8ffa-9d4284616799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "64b73219-d907-4125-b9ba-7a73f380aebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7120cc-e2b4-4950-8ffa-9d4284616799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3def05a5-645d-485a-9da6-afe5cec26e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7120cc-e2b4-4950-8ffa-9d4284616799",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        },
        {
            "id": "f913ee89-cb97-4a6a-96da-41e1041714c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "9402b3a9-a9cb-4959-bf7a-4405a7f36026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f913ee89-cb97-4a6a-96da-41e1041714c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9e36e77-4860-4085-912a-6e09f7b5c4a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f913ee89-cb97-4a6a-96da-41e1041714c4",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        },
        {
            "id": "06a379df-a480-4189-ae61-0e7b7395140a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "c4bc6857-e83d-4449-8eaf-d42b6cc0d3cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06a379df-a480-4189-ae61-0e7b7395140a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe5a4d01-9618-443f-a3a8-334f45ded19d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06a379df-a480-4189-ae61-0e7b7395140a",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        },
        {
            "id": "9570794a-e086-4031-94ea-70232602700c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "compositeImage": {
                "id": "4a66773b-08d9-4451-a2e3-b6e7df345726",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9570794a-e086-4031-94ea-70232602700c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8267482f-0b64-4e15-868a-c9593b35b9df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9570794a-e086-4031-94ea-70232602700c",
                    "LayerId": "d6851d8f-1ec1-49e9-ab58-1904a44b2953"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "d6851d8f-1ec1-49e9-ab58-1904a44b2953",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1048e3df-3fe7-4fa9-9db8-82a943b75b0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 291,
    "xorig": 145,
    "yorig": 144
}