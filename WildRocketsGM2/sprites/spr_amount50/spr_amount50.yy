{
    "id": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_amount50",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 5,
    "bbox_right": 86,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94a01fdd-abe1-4c35-908d-9f7972f92767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "8eb0401e-0c96-42bd-b373-77031f4566fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94a01fdd-abe1-4c35-908d-9f7972f92767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89964a63-851c-4e69-98fb-4376010d7b24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94a01fdd-abe1-4c35-908d-9f7972f92767",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "7d8c80e7-42f9-4e6a-910f-19485f2db298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "e050fa39-a2af-426f-a1d5-8b6e083ac2e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8c80e7-42f9-4e6a-910f-19485f2db298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b961c5bf-166d-48ed-bc9e-2ddbd8fed937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8c80e7-42f9-4e6a-910f-19485f2db298",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "bad13e4e-e2cd-4482-854f-1aa9be7de829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "3d23b788-818e-4a90-b339-0845039723bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bad13e4e-e2cd-4482-854f-1aa9be7de829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0885dfd0-2f95-4cd9-a2bd-7a8d206b574e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bad13e4e-e2cd-4482-854f-1aa9be7de829",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "a4b352d8-72db-419e-8be3-77adfb004fc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "2ea7b896-1b56-4360-856f-99835de8afab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4b352d8-72db-419e-8be3-77adfb004fc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d37ca8-3830-421e-869d-3196eb1fa1cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4b352d8-72db-419e-8be3-77adfb004fc5",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "e94c8084-94a9-4ef3-a40b-623772b64aba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "d4e1a2c1-c109-4f63-8e8e-481879611580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e94c8084-94a9-4ef3-a40b-623772b64aba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a225c57c-9719-4603-bade-ffcb67d09404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e94c8084-94a9-4ef3-a40b-623772b64aba",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "cd1ada4d-00c3-4523-932f-e7c296b31f1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "abbf4478-e928-4019-b919-e33c1e6cff12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd1ada4d-00c3-4523-932f-e7c296b31f1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2030fc4-8ff9-41ee-8e1a-41dd735b2179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd1ada4d-00c3-4523-932f-e7c296b31f1e",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "2b92d814-ec08-4426-84a2-c39829d6c3c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "fcd3ea37-6451-4eb1-a494-5a166278906c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b92d814-ec08-4426-84a2-c39829d6c3c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3609fd79-f128-4346-81ff-267293e018c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b92d814-ec08-4426-84a2-c39829d6c3c3",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "fe34d2bf-ebe9-47b7-8b3e-e00a2ebc7a73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "3215fd28-a48a-4df7-809f-3a8d478f9bf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe34d2bf-ebe9-47b7-8b3e-e00a2ebc7a73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39467bbc-2625-4a59-8283-7ac20a406580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe34d2bf-ebe9-47b7-8b3e-e00a2ebc7a73",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "e1143e4d-6882-4228-b26f-7fdc3e5091c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "38d4256e-dadc-4810-b1a0-477e1f19b653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1143e4d-6882-4228-b26f-7fdc3e5091c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c4f9f1-dd21-4121-95b2-bcbdcd1b5155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1143e4d-6882-4228-b26f-7fdc3e5091c7",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "a9aaf695-39c5-40aa-b0b6-ad5506a13a78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "435805f3-d4ed-42b7-a94c-46d89a9e9647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9aaf695-39c5-40aa-b0b6-ad5506a13a78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "156adb31-b834-4ebd-b8b8-ab8013355779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9aaf695-39c5-40aa-b0b6-ad5506a13a78",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "e8775c8d-e850-409d-b3f8-94a5e527d30b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "d5fa16dd-edb6-45b2-998e-9b4117865ff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8775c8d-e850-409d-b3f8-94a5e527d30b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e5e132-b048-45ef-b1e3-5b2753627de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8775c8d-e850-409d-b3f8-94a5e527d30b",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "cd207fd0-21fe-481b-baa9-fdad00838093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "20aea798-9955-4030-8cab-78fcd1ac3aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd207fd0-21fe-481b-baa9-fdad00838093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "488e7cb7-ffcf-4abe-8d83-a40070f09c4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd207fd0-21fe-481b-baa9-fdad00838093",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        },
        {
            "id": "8c3e6da5-e153-49ed-baa1-65de8a3fb8af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "compositeImage": {
                "id": "9e214d44-a72c-4c8e-a5c5-3bd932d9490a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c3e6da5-e153-49ed-baa1-65de8a3fb8af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cf5d9da-f297-428a-86a8-a3b3138df513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c3e6da5-e153-49ed-baa1-65de8a3fb8af",
                    "LayerId": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "20e69dd8-85b7-4773-8e2d-a7cbd0c2b558",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b55a6f2-e818-4d04-868a-8a51cec01c7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 92,
    "xorig": 46,
    "yorig": 41
}