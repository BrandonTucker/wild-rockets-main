{
    "id": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rareship10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 135,
    "bbox_left": 35,
    "bbox_right": 87,
    "bbox_top": 53,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4069fb7e-af70-4c6c-9680-90cb55098ac5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "compositeImage": {
                "id": "0ef55b83-9243-4a8f-bced-de4340c1aee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4069fb7e-af70-4c6c-9680-90cb55098ac5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "760892ff-7f6b-433d-9767-c93807315fa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4069fb7e-af70-4c6c-9680-90cb55098ac5",
                    "LayerId": "f98ba2ad-8e09-4646-845b-e39f693b0d53"
                }
            ]
        },
        {
            "id": "72b9a5e6-afcb-4d89-8efa-02a647ba906e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "compositeImage": {
                "id": "a83fff67-b1ef-4732-ac5e-c7ca8261bab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72b9a5e6-afcb-4d89-8efa-02a647ba906e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32a19ef0-4a66-423c-89b3-db8320328b77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72b9a5e6-afcb-4d89-8efa-02a647ba906e",
                    "LayerId": "f98ba2ad-8e09-4646-845b-e39f693b0d53"
                }
            ]
        },
        {
            "id": "1643bc7c-eb2a-47d7-a4d0-f5c89c6320e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "compositeImage": {
                "id": "2f7a9427-0275-4462-82b2-51f768f6bacc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1643bc7c-eb2a-47d7-a4d0-f5c89c6320e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56d36608-55bc-4813-8c0d-05a28ef293c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1643bc7c-eb2a-47d7-a4d0-f5c89c6320e7",
                    "LayerId": "f98ba2ad-8e09-4646-845b-e39f693b0d53"
                }
            ]
        },
        {
            "id": "60d43528-6aa6-4213-9b07-0907fda442de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "compositeImage": {
                "id": "dd594158-106f-430f-ae82-c2f3aface837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60d43528-6aa6-4213-9b07-0907fda442de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9938f36-92b6-41b0-b57a-422742d0f220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d43528-6aa6-4213-9b07-0907fda442de",
                    "LayerId": "f98ba2ad-8e09-4646-845b-e39f693b0d53"
                }
            ]
        },
        {
            "id": "6dcc5fb8-e7f7-496b-bcef-d9eebf308f0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "compositeImage": {
                "id": "efc35d2f-7d86-4199-99c5-d81bbfc0eac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dcc5fb8-e7f7-496b-bcef-d9eebf308f0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd555139-c6f2-4652-848d-6f36c2ddd845",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dcc5fb8-e7f7-496b-bcef-d9eebf308f0a",
                    "LayerId": "f98ba2ad-8e09-4646-845b-e39f693b0d53"
                }
            ]
        },
        {
            "id": "12a08c8f-c9b7-465d-99ed-1e70e098cbf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "compositeImage": {
                "id": "fd630acf-c3dd-4547-9e94-cfbbf89fed2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12a08c8f-c9b7-465d-99ed-1e70e098cbf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c13a3ce-2742-48c0-a19c-c93cf2ae396e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12a08c8f-c9b7-465d-99ed-1e70e098cbf4",
                    "LayerId": "f98ba2ad-8e09-4646-845b-e39f693b0d53"
                }
            ]
        },
        {
            "id": "fb077f7b-cb6b-4965-b492-b586d968be56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "compositeImage": {
                "id": "c0c15b0f-a896-478d-961c-964fffa3e73b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb077f7b-cb6b-4965-b492-b586d968be56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11b173eb-0b97-402e-a369-044b88ecfe20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb077f7b-cb6b-4965-b492-b586d968be56",
                    "LayerId": "f98ba2ad-8e09-4646-845b-e39f693b0d53"
                }
            ]
        },
        {
            "id": "4a6e2a21-832d-4136-b48a-c026b9ccc588",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "compositeImage": {
                "id": "5154c308-d4ad-4f86-9a45-b633edc8f080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a6e2a21-832d-4136-b48a-c026b9ccc588",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caa6591b-29c0-4dd2-86a1-9f35c7cfe014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a6e2a21-832d-4136-b48a-c026b9ccc588",
                    "LayerId": "f98ba2ad-8e09-4646-845b-e39f693b0d53"
                }
            ]
        },
        {
            "id": "7edd8ab9-cc7f-473d-911e-c5ecfeb614ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "compositeImage": {
                "id": "6c604b6f-89cd-42ea-a318-da0d7aac38a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7edd8ab9-cc7f-473d-911e-c5ecfeb614ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38ae85aa-a06e-4398-8f7b-131fa2c5fb05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7edd8ab9-cc7f-473d-911e-c5ecfeb614ea",
                    "LayerId": "f98ba2ad-8e09-4646-845b-e39f693b0d53"
                }
            ]
        },
        {
            "id": "6d907bae-78d8-49ec-9684-5552070f79c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "compositeImage": {
                "id": "d2702c2f-8561-45c1-8f4a-ae90eda5d50c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d907bae-78d8-49ec-9684-5552070f79c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b064df-73aa-43d5-9326-f76466b1956e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d907bae-78d8-49ec-9684-5552070f79c0",
                    "LayerId": "f98ba2ad-8e09-4646-845b-e39f693b0d53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "f98ba2ad-8e09-4646-845b-e39f693b0d53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29be3de3-6f2e-4a42-90be-8bd9280fc774",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 60,
    "yorig": 87
}