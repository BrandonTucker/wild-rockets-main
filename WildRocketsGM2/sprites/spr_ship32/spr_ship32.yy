{
    "id": "07b8a1a5-0f94-4173-9ece-e91569661662",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e61ea080-921f-4a08-8755-0e3017e6b025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b8a1a5-0f94-4173-9ece-e91569661662",
            "compositeImage": {
                "id": "a494e650-da21-4848-bc8a-0a72837cc82d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e61ea080-921f-4a08-8755-0e3017e6b025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d8b5115-bd97-4e11-bd00-0e4d45041984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e61ea080-921f-4a08-8755-0e3017e6b025",
                    "LayerId": "f24f94ad-2698-4281-9df0-68a8527118e2"
                }
            ]
        },
        {
            "id": "25cfa58b-e91e-4cf3-b638-cd0079a562de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b8a1a5-0f94-4173-9ece-e91569661662",
            "compositeImage": {
                "id": "1bc4c27e-8067-44e7-a483-30f331e791b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25cfa58b-e91e-4cf3-b638-cd0079a562de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42a30979-54fa-4182-b08b-532be9d8278a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25cfa58b-e91e-4cf3-b638-cd0079a562de",
                    "LayerId": "f24f94ad-2698-4281-9df0-68a8527118e2"
                }
            ]
        },
        {
            "id": "3709df7b-2309-461d-9d1d-95a6cbf60270",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b8a1a5-0f94-4173-9ece-e91569661662",
            "compositeImage": {
                "id": "dca70bbd-c2fc-4064-83ce-59ae4f9eb557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3709df7b-2309-461d-9d1d-95a6cbf60270",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c15cbf0-a59c-4ccb-982f-835e29ba2226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3709df7b-2309-461d-9d1d-95a6cbf60270",
                    "LayerId": "f24f94ad-2698-4281-9df0-68a8527118e2"
                }
            ]
        },
        {
            "id": "cc6c9ee6-3cd2-4d4f-b464-7ad26b3bfe09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b8a1a5-0f94-4173-9ece-e91569661662",
            "compositeImage": {
                "id": "88e59dce-4dac-48e1-b439-b9d5901c4672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc6c9ee6-3cd2-4d4f-b464-7ad26b3bfe09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e58e6eae-e94d-472a-9cdc-1c8a71e68a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc6c9ee6-3cd2-4d4f-b464-7ad26b3bfe09",
                    "LayerId": "f24f94ad-2698-4281-9df0-68a8527118e2"
                }
            ]
        },
        {
            "id": "7405a9a8-9e9a-4298-a33f-59523b20364d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b8a1a5-0f94-4173-9ece-e91569661662",
            "compositeImage": {
                "id": "f1350858-f142-46fc-9551-76fe04fb827c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7405a9a8-9e9a-4298-a33f-59523b20364d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39a20a3f-5d0d-4a3b-95e3-99abe1fc4821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7405a9a8-9e9a-4298-a33f-59523b20364d",
                    "LayerId": "f24f94ad-2698-4281-9df0-68a8527118e2"
                }
            ]
        },
        {
            "id": "acd15591-2911-4bdb-b30f-06a8253f2c70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b8a1a5-0f94-4173-9ece-e91569661662",
            "compositeImage": {
                "id": "caed5ab3-abad-408b-8c69-22d725cac1b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acd15591-2911-4bdb-b30f-06a8253f2c70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fd18fcd-b722-472f-8e4d-6ed1ec4d5d86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acd15591-2911-4bdb-b30f-06a8253f2c70",
                    "LayerId": "f24f94ad-2698-4281-9df0-68a8527118e2"
                }
            ]
        },
        {
            "id": "283f9cb9-8cea-47c7-bccf-9cd30d8b227e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b8a1a5-0f94-4173-9ece-e91569661662",
            "compositeImage": {
                "id": "a99a0660-2421-4a6a-9212-ecffe78d8fb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "283f9cb9-8cea-47c7-bccf-9cd30d8b227e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7122d585-d5b1-432b-9b63-64300298a393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "283f9cb9-8cea-47c7-bccf-9cd30d8b227e",
                    "LayerId": "f24f94ad-2698-4281-9df0-68a8527118e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "f24f94ad-2698-4281-9df0-68a8527118e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07b8a1a5-0f94-4173-9ece-e91569661662",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 48
}