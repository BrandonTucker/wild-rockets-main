{
    "id": "0b298d78-4504-4f2c-9f1f-b5fba0259958",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_finnish",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 639,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4793f95f-7de4-4716-99d8-8ba76f509250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b298d78-4504-4f2c-9f1f-b5fba0259958",
            "compositeImage": {
                "id": "16fc73f3-287f-43d8-920b-9051abea2ff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4793f95f-7de4-4716-99d8-8ba76f509250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fd88e9a-3e5b-4833-bffa-82d2a5527197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4793f95f-7de4-4716-99d8-8ba76f509250",
                    "LayerId": "d823fa03-0912-4bc8-ae95-7e9c766ef9dc"
                }
            ]
        },
        {
            "id": "467c6e25-d080-4553-bd3d-6031adfb421e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b298d78-4504-4f2c-9f1f-b5fba0259958",
            "compositeImage": {
                "id": "6436f5c7-d279-42d0-bb20-ff2594d071b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "467c6e25-d080-4553-bd3d-6031adfb421e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70a1fc7e-689e-45fe-9f30-849a001abcc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "467c6e25-d080-4553-bd3d-6031adfb421e",
                    "LayerId": "d823fa03-0912-4bc8-ae95-7e9c766ef9dc"
                }
            ]
        },
        {
            "id": "28eac237-b6fa-407e-936d-ac1acd26e2cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b298d78-4504-4f2c-9f1f-b5fba0259958",
            "compositeImage": {
                "id": "aa5ebb87-b9eb-4ebc-be6b-f09400e5ff1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28eac237-b6fa-407e-936d-ac1acd26e2cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fca31a9e-a922-4220-9e9e-4eeb787c5db7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28eac237-b6fa-407e-936d-ac1acd26e2cf",
                    "LayerId": "d823fa03-0912-4bc8-ae95-7e9c766ef9dc"
                }
            ]
        },
        {
            "id": "9b776813-7214-4ecd-9603-5671c1ce34f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b298d78-4504-4f2c-9f1f-b5fba0259958",
            "compositeImage": {
                "id": "90eea1b8-38e6-4473-a3e9-82997f82a176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b776813-7214-4ecd-9603-5671c1ce34f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7703698-62e7-4849-ba46-49584e1320a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b776813-7214-4ecd-9603-5671c1ce34f6",
                    "LayerId": "d823fa03-0912-4bc8-ae95-7e9c766ef9dc"
                }
            ]
        },
        {
            "id": "84c9267e-04a3-4557-a861-afe2c65df59a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b298d78-4504-4f2c-9f1f-b5fba0259958",
            "compositeImage": {
                "id": "ab0bf403-dd01-4018-a8db-c1e40e8ba3e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84c9267e-04a3-4557-a861-afe2c65df59a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7cdaa81-e237-4f20-be4a-07d5e4117c1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84c9267e-04a3-4557-a861-afe2c65df59a",
                    "LayerId": "d823fa03-0912-4bc8-ae95-7e9c766ef9dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d823fa03-0912-4bc8-ae95-7e9c766ef9dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b298d78-4504-4f2c-9f1f-b5fba0259958",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}