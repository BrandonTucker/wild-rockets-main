{
    "id": "fff0ad50-c35d-4a0f-a9db-ca764c00d9f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon72",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 7,
    "bbox_right": 64,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e23e4de7-d402-496e-bbdf-996263b1dbca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fff0ad50-c35d-4a0f-a9db-ca764c00d9f2",
            "compositeImage": {
                "id": "1fab0236-e71f-41ac-9e09-7db8817c39c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e23e4de7-d402-496e-bbdf-996263b1dbca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "322332ab-31a6-4335-977b-630ca861fc53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e23e4de7-d402-496e-bbdf-996263b1dbca",
                    "LayerId": "747ed946-d957-404a-b701-7aafb9c3bf11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "747ed946-d957-404a-b701-7aafb9c3bf11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fff0ad50-c35d-4a0f-a9db-ca764c00d9f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 0
}