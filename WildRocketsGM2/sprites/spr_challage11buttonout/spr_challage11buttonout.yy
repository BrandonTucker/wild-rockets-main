{
    "id": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage11buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f439f760-1e8e-4bc0-85a0-0ed738ebbf54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "compositeImage": {
                "id": "3268ed57-7988-40d5-844a-18472eb14d74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f439f760-1e8e-4bc0-85a0-0ed738ebbf54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b2f1f65-c514-4c3e-a723-587e0dc98775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f439f760-1e8e-4bc0-85a0-0ed738ebbf54",
                    "LayerId": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f"
                }
            ]
        },
        {
            "id": "13874f49-99ac-4498-a763-c02debf8b798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "compositeImage": {
                "id": "be3aaf6d-31ba-4ae1-ad58-cf8c8f113cdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13874f49-99ac-4498-a763-c02debf8b798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb35bea-a38c-4130-98ff-43ffde21f10c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13874f49-99ac-4498-a763-c02debf8b798",
                    "LayerId": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f"
                }
            ]
        },
        {
            "id": "2081544c-b44f-4cbe-8e6f-37664148e3c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "compositeImage": {
                "id": "1494472b-6263-48cb-b044-a788b041ba31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2081544c-b44f-4cbe-8e6f-37664148e3c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b454070c-7d68-4db9-862a-86ac733d5621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2081544c-b44f-4cbe-8e6f-37664148e3c2",
                    "LayerId": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f"
                }
            ]
        },
        {
            "id": "a99782a3-8585-4f84-ae5f-b975813fe3c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "compositeImage": {
                "id": "ee2ab0dd-ed38-49e8-a80c-b33bda1bb68a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a99782a3-8585-4f84-ae5f-b975813fe3c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "808182aa-7794-4845-979a-fba51f760272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a99782a3-8585-4f84-ae5f-b975813fe3c1",
                    "LayerId": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f"
                }
            ]
        },
        {
            "id": "71a8975f-35db-4c3e-8419-2e2aa7829bac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "compositeImage": {
                "id": "55c5d0ec-9662-4a4c-9276-bae681909b11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71a8975f-35db-4c3e-8419-2e2aa7829bac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2259e9c9-f30b-43e7-9227-b18b601c91bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a8975f-35db-4c3e-8419-2e2aa7829bac",
                    "LayerId": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f"
                }
            ]
        },
        {
            "id": "825f85dc-d25c-4a47-bd6c-c24dd88b163f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "compositeImage": {
                "id": "43e70cde-0ed0-4e0d-aa45-f46ddad85426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "825f85dc-d25c-4a47-bd6c-c24dd88b163f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f39d987f-5ffa-4e08-abf0-3d46f83f6e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "825f85dc-d25c-4a47-bd6c-c24dd88b163f",
                    "LayerId": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f"
                }
            ]
        },
        {
            "id": "01dfed02-1826-4486-9856-44827e713a5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "compositeImage": {
                "id": "9cbc5bc9-d76f-4ce4-856a-64b4bc66b007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01dfed02-1826-4486-9856-44827e713a5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09d34977-f392-4b98-8b6d-6dbb4d91f37b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01dfed02-1826-4486-9856-44827e713a5e",
                    "LayerId": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f"
                }
            ]
        },
        {
            "id": "514aee86-731b-458b-ae4f-e0209e782a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "compositeImage": {
                "id": "14dedaeb-3988-4fca-9b87-fa882bbe2324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "514aee86-731b-458b-ae4f-e0209e782a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d88a6d59-6828-4330-b950-3840644604ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "514aee86-731b-458b-ae4f-e0209e782a1c",
                    "LayerId": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f"
                }
            ]
        },
        {
            "id": "361ad192-dff6-4b7d-9091-017ceecb4e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "compositeImage": {
                "id": "c8ea5b8c-f8e6-4ae6-a26b-4a06bcb71858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "361ad192-dff6-4b7d-9091-017ceecb4e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03255e80-9f5d-4364-9a73-775c9c982aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "361ad192-dff6-4b7d-9091-017ceecb4e3c",
                    "LayerId": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f"
                }
            ]
        },
        {
            "id": "ce27c6d1-d68e-4737-b937-f056251f7cc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "compositeImage": {
                "id": "bf36a693-71f4-4ce0-8925-c61e34432cff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce27c6d1-d68e-4737-b937-f056251f7cc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd7e9124-1ea9-4569-96a5-da5ea1676dee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce27c6d1-d68e-4737-b937-f056251f7cc0",
                    "LayerId": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "70d4ad37-6b8b-45f8-9db8-3b58e4fc9a0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2416cb77-0141-4e9f-b62b-4f2780e263b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}