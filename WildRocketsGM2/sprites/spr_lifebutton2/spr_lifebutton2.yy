{
    "id": "20c61f0f-7ec4-4b20-9b2c-b45380ab0689",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lifebutton2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 7,
    "bbox_right": 43,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90a95646-7958-41c4-a006-59475a9b3927",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20c61f0f-7ec4-4b20-9b2c-b45380ab0689",
            "compositeImage": {
                "id": "26f42777-53c4-483d-bbb7-4057e179b8a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90a95646-7958-41c4-a006-59475a9b3927",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11501895-148c-4245-9837-9d8ed67fe85d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90a95646-7958-41c4-a006-59475a9b3927",
                    "LayerId": "7e48d113-cfc3-4f5d-b99e-0079ae9fcd17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "7e48d113-cfc3-4f5d-b99e-0079ae9fcd17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20c61f0f-7ec4-4b20-9b2c-b45380ab0689",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": 28,
    "yorig": 28
}