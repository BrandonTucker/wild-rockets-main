{
    "id": "64ad79a8-31d4-42d1-841e-3e548b1676ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background15",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 959,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51aa66a3-9bb7-4bc9-83d9-bd2d6ea322cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64ad79a8-31d4-42d1-841e-3e548b1676ad",
            "compositeImage": {
                "id": "1b8eda91-ed9e-4228-b077-00def2dff445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51aa66a3-9bb7-4bc9-83d9-bd2d6ea322cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a96ad88-c68a-4e46-91ca-5b8e0377db44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51aa66a3-9bb7-4bc9-83d9-bd2d6ea322cf",
                    "LayerId": "3251af89-0f36-4bfa-b519-33978b21095d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "3251af89-0f36-4bfa-b519-33978b21095d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64ad79a8-31d4-42d1-841e-3e548b1676ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}