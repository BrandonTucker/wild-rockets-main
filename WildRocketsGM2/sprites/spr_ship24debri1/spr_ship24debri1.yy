{
    "id": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship24debri1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 7,
    "bbox_right": 70,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88bdd8a7-ab4b-4ac4-a96e-77599281ff55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "2668a033-63dc-4d40-b2b4-2ada2576ab0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88bdd8a7-ab4b-4ac4-a96e-77599281ff55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1295e671-ce1c-430d-b572-d1455acaf77b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88bdd8a7-ab4b-4ac4-a96e-77599281ff55",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "90f9a479-86b7-4934-bf32-366fced7ae1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "b55ec887-01ff-45dc-8ac0-3860993d00fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90f9a479-86b7-4934-bf32-366fced7ae1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1d2014e-a08a-4ab6-962c-fc3c5dd96ca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90f9a479-86b7-4934-bf32-366fced7ae1b",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "ee06ee14-45c7-470b-86aa-a6111ed5ee04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "2a25a1d4-a66b-4ec9-8632-7f1554b55df7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee06ee14-45c7-470b-86aa-a6111ed5ee04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0662ac02-1b5a-4cb7-8d73-f54b5168c209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee06ee14-45c7-470b-86aa-a6111ed5ee04",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "b69a5ff3-662f-412e-ad7e-512146b47850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "077820e6-5a98-4a99-a9a9-fb75948aeb04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b69a5ff3-662f-412e-ad7e-512146b47850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab32ece3-01f3-4fa0-a88e-bca87646ffd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b69a5ff3-662f-412e-ad7e-512146b47850",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "e98f94f2-b2c2-4ef0-b307-01d217a3a1df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "91899689-d9be-4807-9b83-5b1c182f1d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e98f94f2-b2c2-4ef0-b307-01d217a3a1df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c033388-9183-47c0-862e-a232838fa222",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e98f94f2-b2c2-4ef0-b307-01d217a3a1df",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "b65eac47-0420-4dc7-b93b-8cc40aca3418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "3f2ba6bb-3835-41be-9494-d6a08acac772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65eac47-0420-4dc7-b93b-8cc40aca3418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dacd79a1-0557-44fd-ad56-afe4e105e1d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65eac47-0420-4dc7-b93b-8cc40aca3418",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "a5ec1793-4b24-409c-a3a0-d6a8960c6cd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "2529fc13-22b8-4891-869f-cb707d063190",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ec1793-4b24-409c-a3a0-d6a8960c6cd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8df9ed78-aede-456a-80fe-4a968197a5d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ec1793-4b24-409c-a3a0-d6a8960c6cd8",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "2818005e-06a8-41ae-b206-51957866fc11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "f241dbf5-d2d2-4ffa-8ef1-baec93ef0931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2818005e-06a8-41ae-b206-51957866fc11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "097e8bd3-d523-4cc8-8916-50158bf44323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2818005e-06a8-41ae-b206-51957866fc11",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "270d36bf-de2a-4f21-bb37-7c82d8f98d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "1c7d844b-d4da-40c4-b431-52fdf8ce3def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "270d36bf-de2a-4f21-bb37-7c82d8f98d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3894edb0-1fbb-4c58-a32a-42f14e58b8cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "270d36bf-de2a-4f21-bb37-7c82d8f98d00",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "82758f2a-f075-488f-94a8-37a9b76f46aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "40c364d1-4e86-46ad-ad8d-756c4ff20401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82758f2a-f075-488f-94a8-37a9b76f46aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0c5e130-9209-42e7-86d6-5c021b009c08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82758f2a-f075-488f-94a8-37a9b76f46aa",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "9020a6fb-9b6b-410a-9ec6-f654669864fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "39967d53-1908-4af7-a53f-d01220423e14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9020a6fb-9b6b-410a-9ec6-f654669864fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63426544-00ab-45b4-abf6-e7aa6270fc3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9020a6fb-9b6b-410a-9ec6-f654669864fd",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "f010c826-8c32-4535-9b7e-35e3ff910abd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "18487557-e64f-4f6c-982c-13d436c52c97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f010c826-8c32-4535-9b7e-35e3ff910abd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e830d34-4ac6-441f-b5cc-9ba9e5fca0ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f010c826-8c32-4535-9b7e-35e3ff910abd",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "5cc0e5af-227c-4c50-bf13-ac34ba7df8c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "34fb9eff-dfcb-476f-a7a3-d029249fca23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cc0e5af-227c-4c50-bf13-ac34ba7df8c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4ca3940-9eff-41d0-a9e5-749744df2763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cc0e5af-227c-4c50-bf13-ac34ba7df8c1",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "fc2053ce-58e4-4345-97c0-2fe697dee17c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "4128da2e-7e9b-467d-96ec-a9939157933e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2053ce-58e4-4345-97c0-2fe697dee17c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e41fa27c-f21e-4b00-9f67-ef4086277dec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2053ce-58e4-4345-97c0-2fe697dee17c",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        },
        {
            "id": "b0307f6b-d48b-43ed-a5e3-2c46fbb5cb9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "compositeImage": {
                "id": "622915b5-2392-465c-bf12-8676340b847a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0307f6b-d48b-43ed-a5e3-2c46fbb5cb9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "389be480-11fb-4417-9c15-16d1057e9763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0307f6b-d48b-43ed-a5e3-2c46fbb5cb9e",
                    "LayerId": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 89,
    "layers": [
        {
            "id": "b4c68f94-69fe-4515-a3ff-5b1c3166acc7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42fc90ea-4c9f-41a3-a0e6-59100de31bae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 47,
    "yorig": 49
}