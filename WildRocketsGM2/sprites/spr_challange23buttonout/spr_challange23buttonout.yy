{
    "id": "a0c500f8-b649-440c-8e0f-2c9520b16347",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange23buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 0,
    "bbox_right": 104,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d31c8ed9-1dda-4dd6-990f-214f768f1935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "compositeImage": {
                "id": "0e4a123b-a950-4220-9f23-5a8ec55dd39f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d31c8ed9-1dda-4dd6-990f-214f768f1935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1979e98a-3bb8-40d8-a159-08650193c9a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d31c8ed9-1dda-4dd6-990f-214f768f1935",
                    "LayerId": "00a72935-6bd6-4138-8d2e-996315d0e272"
                }
            ]
        },
        {
            "id": "468cd18e-00f4-417f-8d38-c83c8fc34816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "compositeImage": {
                "id": "511c131a-09bd-4e63-a6d8-9f65c018e3fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "468cd18e-00f4-417f-8d38-c83c8fc34816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eb0102f-766c-47cc-a7d0-512fed1dd846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "468cd18e-00f4-417f-8d38-c83c8fc34816",
                    "LayerId": "00a72935-6bd6-4138-8d2e-996315d0e272"
                }
            ]
        },
        {
            "id": "8f104898-05bd-455b-a332-1b9f0855396c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "compositeImage": {
                "id": "fd32def3-1e72-47a4-a05d-fddee8697f95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f104898-05bd-455b-a332-1b9f0855396c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b52b0ac5-7b4c-46b8-a566-f8345d43f7a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f104898-05bd-455b-a332-1b9f0855396c",
                    "LayerId": "00a72935-6bd6-4138-8d2e-996315d0e272"
                }
            ]
        },
        {
            "id": "eade0c11-d79d-45c4-af17-3c482a76b12b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "compositeImage": {
                "id": "94b9b80c-33ae-4fb5-849b-d473147921bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eade0c11-d79d-45c4-af17-3c482a76b12b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86721443-492d-42f6-ab5a-e6a4f6e7c5c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eade0c11-d79d-45c4-af17-3c482a76b12b",
                    "LayerId": "00a72935-6bd6-4138-8d2e-996315d0e272"
                }
            ]
        },
        {
            "id": "2d3a1b02-dcf6-46d3-87d3-9ae5400309b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "compositeImage": {
                "id": "735cbaed-716d-4bc1-9658-a1e4f3087d1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d3a1b02-dcf6-46d3-87d3-9ae5400309b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a2f2fde-4100-4387-8be7-bef173f5f00e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d3a1b02-dcf6-46d3-87d3-9ae5400309b0",
                    "LayerId": "00a72935-6bd6-4138-8d2e-996315d0e272"
                }
            ]
        },
        {
            "id": "95566e6c-f30f-4577-a8f4-a116a72dbb26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "compositeImage": {
                "id": "abc92bc9-44f5-4474-a315-2f15dbb3b2c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95566e6c-f30f-4577-a8f4-a116a72dbb26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cc85347-a7d6-4047-af83-968b98698590",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95566e6c-f30f-4577-a8f4-a116a72dbb26",
                    "LayerId": "00a72935-6bd6-4138-8d2e-996315d0e272"
                }
            ]
        },
        {
            "id": "965d8f2e-2030-4c09-94ec-f67c6f3c8a68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "compositeImage": {
                "id": "1aa6fe49-d5e8-44eb-abed-7fd9085e6261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "965d8f2e-2030-4c09-94ec-f67c6f3c8a68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c28faea-df73-4942-a208-f89fed3bd179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "965d8f2e-2030-4c09-94ec-f67c6f3c8a68",
                    "LayerId": "00a72935-6bd6-4138-8d2e-996315d0e272"
                }
            ]
        },
        {
            "id": "b7df3f59-3161-4198-b05c-ac69e938db64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "compositeImage": {
                "id": "4c858eb8-855d-4ab0-9074-3ca19d7377d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7df3f59-3161-4198-b05c-ac69e938db64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d624c6-7d11-4a1c-b427-f7845d461435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7df3f59-3161-4198-b05c-ac69e938db64",
                    "LayerId": "00a72935-6bd6-4138-8d2e-996315d0e272"
                }
            ]
        },
        {
            "id": "7422144a-f83e-44d2-bfae-18989dbecf54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "compositeImage": {
                "id": "2ea3cbc4-a5f2-414a-990d-39f254e88b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7422144a-f83e-44d2-bfae-18989dbecf54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "677e167f-a8c6-4fe6-8cff-ad225434751b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7422144a-f83e-44d2-bfae-18989dbecf54",
                    "LayerId": "00a72935-6bd6-4138-8d2e-996315d0e272"
                }
            ]
        },
        {
            "id": "0156a27f-20a7-4610-bb04-0bceb6318d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "compositeImage": {
                "id": "1606f336-79a9-481c-992e-fc529527455c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0156a27f-20a7-4610-bb04-0bceb6318d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7425fbe-c5ac-4e59-82ab-80aeed309a9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0156a27f-20a7-4610-bb04-0bceb6318d50",
                    "LayerId": "00a72935-6bd6-4138-8d2e-996315d0e272"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "00a72935-6bd6-4138-8d2e-996315d0e272",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0c500f8-b649-440c-8e0f-2c9520b16347",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 53,
    "yorig": 52
}