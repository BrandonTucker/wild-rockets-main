{
    "id": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship27debri2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c630e9e5-9ed6-4c2a-9533-c28300be547a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "d0c7d167-0ccd-4793-b09e-e65c52f1b078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c630e9e5-9ed6-4c2a-9533-c28300be547a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ca91001-8656-41af-917d-55807cf8a318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c630e9e5-9ed6-4c2a-9533-c28300be547a",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "52d1c05a-93a1-424f-8cff-3a21f1b94338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "15773950-f49b-4cab-8b3d-1746c2ac5d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52d1c05a-93a1-424f-8cff-3a21f1b94338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bf93595-5175-4a7a-8429-33486805dede",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d1c05a-93a1-424f-8cff-3a21f1b94338",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "5cc22aad-d079-438d-8994-5e625b16b5c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "728888b8-406f-44e3-a460-dc3ff48c7f3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cc22aad-d079-438d-8994-5e625b16b5c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e51618a4-00ac-4b69-b1c4-660fbe10c2f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cc22aad-d079-438d-8994-5e625b16b5c5",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "89281e89-c9e3-4212-83bd-6f4c07dbfaa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "479c63f8-744d-400f-a86d-d9a2d0deeab5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89281e89-c9e3-4212-83bd-6f4c07dbfaa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66503de1-72cf-4b6e-9cc4-0054d2ef10d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89281e89-c9e3-4212-83bd-6f4c07dbfaa9",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "31ebdc94-5cdd-41cb-89b4-7c7747c0451a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "9543c16d-4863-41a0-977d-7284b2806ce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ebdc94-5cdd-41cb-89b4-7c7747c0451a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4bd04e2-9a3c-46c0-ad62-d089b0957bce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ebdc94-5cdd-41cb-89b4-7c7747c0451a",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "8c9fb0ae-8baa-423e-a9bd-52c519bbc81f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "550cdcfc-978f-4d57-995c-ea31a5f05813",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c9fb0ae-8baa-423e-a9bd-52c519bbc81f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "407466dd-8f85-41e4-a8c2-c611f43ff441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c9fb0ae-8baa-423e-a9bd-52c519bbc81f",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "b83c4b04-145b-4990-a53a-b05d73c34150",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "3546e290-b9e0-42fe-92e8-19acb0921c20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b83c4b04-145b-4990-a53a-b05d73c34150",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceebf127-5146-4b1e-af26-b0c286e08514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b83c4b04-145b-4990-a53a-b05d73c34150",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "9932056c-f1b9-45c3-8f62-e9f33b8948dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "c54d7215-9674-444e-8ab7-51a3c2f68c12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9932056c-f1b9-45c3-8f62-e9f33b8948dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2bc15f6-3455-4253-ba98-0eed325ec22a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9932056c-f1b9-45c3-8f62-e9f33b8948dc",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "c5993a61-c678-4020-abd0-3380135d770f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "0b2bbcab-994a-4781-947f-845460a7cdc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5993a61-c678-4020-abd0-3380135d770f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76c0209d-9f4e-44a7-aedc-5e76023c21f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5993a61-c678-4020-abd0-3380135d770f",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "d7e3159a-bc52-4bcc-90ab-77d625a27ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "40e31051-70ec-41f5-ab6c-6e7677ca158a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7e3159a-bc52-4bcc-90ab-77d625a27ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d04e9847-6be6-4b82-93e2-31d05af22c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7e3159a-bc52-4bcc-90ab-77d625a27ec2",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "5ba52df5-a58b-4ce6-b018-91d3394ce9f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "a9ce259a-a287-487d-837c-f78f16d5ab64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ba52df5-a58b-4ce6-b018-91d3394ce9f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28d3d8f6-6b87-4dbe-8864-90cd6e10bacc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ba52df5-a58b-4ce6-b018-91d3394ce9f8",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "23b80df5-a6c6-4ac7-810e-7db1c58aa51b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "f6d7483c-1fb2-4aba-83b8-2eec9ff5d14f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23b80df5-a6c6-4ac7-810e-7db1c58aa51b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b778a620-f329-4999-8637-24ad1d93573e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23b80df5-a6c6-4ac7-810e-7db1c58aa51b",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "e3ff1d62-3220-4d54-90ef-2d959ca43066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "5e937944-863b-49f9-a3b2-5a28bbd64902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3ff1d62-3220-4d54-90ef-2d959ca43066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b76524-ad92-4bbc-ab28-007650d71c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3ff1d62-3220-4d54-90ef-2d959ca43066",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "686c9415-798a-4e1b-8c0e-9d9dfb49e5d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "df0c1ff0-2808-4436-a680-9881bd32a3ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "686c9415-798a-4e1b-8c0e-9d9dfb49e5d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beab0cb2-a0bf-4c59-8871-48dc39b2abed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "686c9415-798a-4e1b-8c0e-9d9dfb49e5d3",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        },
        {
            "id": "b6df9e50-e8ea-4638-a6a7-fda88d483dd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "compositeImage": {
                "id": "a36897a4-052d-44df-916f-2294df181457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6df9e50-e8ea-4638-a6a7-fda88d483dd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe6013d6-5dbf-4a30-aeaa-8bda6b9dc620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6df9e50-e8ea-4638-a6a7-fda88d483dd1",
                    "LayerId": "2b98b9d7-811c-46b6-89ba-32cc76145c64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "2b98b9d7-811c-46b6-89ba-32cc76145c64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35ab50f3-769f-44d1-9a54-a1a5bc31ee6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 35,
    "yorig": 31
}