{
    "id": "b1b3162e-b1a3-4299-8b8b-0eb9be57d033",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship32boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 151,
    "bbox_left": 39,
    "bbox_right": 64,
    "bbox_top": 59,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99777678-2d15-4b43-826d-825b39b90b2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3162e-b1a3-4299-8b8b-0eb9be57d033",
            "compositeImage": {
                "id": "038340ba-5a4c-41f2-abc6-a5f806aa92b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99777678-2d15-4b43-826d-825b39b90b2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b6e8904-cdd4-4e49-b0ee-1adb5b91b46f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99777678-2d15-4b43-826d-825b39b90b2c",
                    "LayerId": "be5839ea-f97f-42bd-a2ab-c18a3cca2015"
                }
            ]
        },
        {
            "id": "9c775e64-5a7d-447b-8347-7022f21081b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3162e-b1a3-4299-8b8b-0eb9be57d033",
            "compositeImage": {
                "id": "00732eb9-1e4f-492c-83e3-cabf16d294a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c775e64-5a7d-447b-8347-7022f21081b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bddbaf4-72f0-4807-b9d4-8d1f83f490e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c775e64-5a7d-447b-8347-7022f21081b5",
                    "LayerId": "be5839ea-f97f-42bd-a2ab-c18a3cca2015"
                }
            ]
        },
        {
            "id": "46e8afe6-1fde-4304-b4ea-efc21edf23d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3162e-b1a3-4299-8b8b-0eb9be57d033",
            "compositeImage": {
                "id": "a6b9f688-68e6-44ee-a0b8-0f02a503125a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46e8afe6-1fde-4304-b4ea-efc21edf23d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc05038a-80b7-4ffe-ad9e-21ded88ad3e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46e8afe6-1fde-4304-b4ea-efc21edf23d2",
                    "LayerId": "be5839ea-f97f-42bd-a2ab-c18a3cca2015"
                }
            ]
        },
        {
            "id": "dc657ea0-8676-41a3-a99d-c20c46f86c6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3162e-b1a3-4299-8b8b-0eb9be57d033",
            "compositeImage": {
                "id": "024a2646-f0cc-4a42-8804-8f6673079534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc657ea0-8676-41a3-a99d-c20c46f86c6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fd00773-3230-43d9-b9fa-fc85ddb97477",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc657ea0-8676-41a3-a99d-c20c46f86c6d",
                    "LayerId": "be5839ea-f97f-42bd-a2ab-c18a3cca2015"
                }
            ]
        },
        {
            "id": "d415ba27-cc9b-40c8-a947-2ac914efe4dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3162e-b1a3-4299-8b8b-0eb9be57d033",
            "compositeImage": {
                "id": "74e4aaf7-e086-4c2b-bf35-d85b88fd718a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d415ba27-cc9b-40c8-a947-2ac914efe4dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59dfb56e-592d-4e7e-80af-98cbab2bbb5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d415ba27-cc9b-40c8-a947-2ac914efe4dd",
                    "LayerId": "be5839ea-f97f-42bd-a2ab-c18a3cca2015"
                }
            ]
        },
        {
            "id": "345ede9a-02cf-4957-8f1b-a7d781bac0a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3162e-b1a3-4299-8b8b-0eb9be57d033",
            "compositeImage": {
                "id": "2ac9b5be-d2a0-4e37-b2b7-54bbe01179db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "345ede9a-02cf-4957-8f1b-a7d781bac0a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "530ae041-0154-4b70-8e3a-40dbc6a75539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "345ede9a-02cf-4957-8f1b-a7d781bac0a0",
                    "LayerId": "be5839ea-f97f-42bd-a2ab-c18a3cca2015"
                }
            ]
        },
        {
            "id": "eeeb7581-8c3c-4360-8903-76aa08830aab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1b3162e-b1a3-4299-8b8b-0eb9be57d033",
            "compositeImage": {
                "id": "374d8631-8f27-45ac-8322-9fa3e48a00ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeeb7581-8c3c-4360-8903-76aa08830aab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8603d75e-d542-410e-8216-3b2593aa460c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeeb7581-8c3c-4360-8903-76aa08830aab",
                    "LayerId": "be5839ea-f97f-42bd-a2ab-c18a3cca2015"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 219,
    "layers": [
        {
            "id": "be5839ea-f97f-42bd-a2ab-c18a3cca2015",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1b3162e-b1a3-4299-8b8b-0eb9be57d033",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 95
}