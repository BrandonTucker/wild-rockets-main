{
    "id": "1adfe3b9-b5ae-44d7-8830-99c43c2af46a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage20button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 1,
    "bbox_right": 99,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e36d4f98-4e87-4f81-bc09-e36fe8c60fbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1adfe3b9-b5ae-44d7-8830-99c43c2af46a",
            "compositeImage": {
                "id": "5913a16c-4662-47d3-887e-7b4219ef8da7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e36d4f98-4e87-4f81-bc09-e36fe8c60fbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7568cfe4-fa96-4859-bea9-e367fb18217a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e36d4f98-4e87-4f81-bc09-e36fe8c60fbe",
                    "LayerId": "bd034af4-df71-4b25-a5a6-490289ea65bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "bd034af4-df71-4b25-a5a6-490289ea65bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1adfe3b9-b5ae-44d7-8830-99c43c2af46a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}