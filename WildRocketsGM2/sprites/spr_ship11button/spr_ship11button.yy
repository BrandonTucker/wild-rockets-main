{
    "id": "c20c443f-e6aa-4997-9e82-06d56d3c9881",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship11button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 12,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ec6e86d-8d11-4eee-9296-14aa2d22581a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c20c443f-e6aa-4997-9e82-06d56d3c9881",
            "compositeImage": {
                "id": "a5f8f63b-252a-4d43-b2b3-e3496bf8b7ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ec6e86d-8d11-4eee-9296-14aa2d22581a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "febdc9c6-d69e-48f8-bc47-ce00dca2b895",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ec6e86d-8d11-4eee-9296-14aa2d22581a",
                    "LayerId": "0a28ffb1-320a-4aa1-ad26-39654550024d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "0a28ffb1-320a-4aa1-ad26-39654550024d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c20c443f-e6aa-4997-9e82-06d56d3c9881",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}