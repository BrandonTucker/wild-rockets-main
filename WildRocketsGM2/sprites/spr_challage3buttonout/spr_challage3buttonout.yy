{
    "id": "b0e1b71c-6fcc-4414-b949-88718b923965",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage3buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 2,
    "bbox_right": 108,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "448aaa88-2b51-4b70-bc3e-fd66f0e319cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "compositeImage": {
                "id": "8fa9e47f-94fa-4b76-9e9f-9d0ecc550ea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "448aaa88-2b51-4b70-bc3e-fd66f0e319cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc670612-ce03-4daf-a1cb-ce69b205a069",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "448aaa88-2b51-4b70-bc3e-fd66f0e319cd",
                    "LayerId": "cec375dd-4039-4ac8-9ee1-5d71375893a5"
                }
            ]
        },
        {
            "id": "44d327f2-ea26-49fa-95ec-54807a52e118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "compositeImage": {
                "id": "f369f4b3-7616-4fec-a3ed-08c5297c5f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44d327f2-ea26-49fa-95ec-54807a52e118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7158383-08e7-4295-900f-40d89e51f21c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44d327f2-ea26-49fa-95ec-54807a52e118",
                    "LayerId": "cec375dd-4039-4ac8-9ee1-5d71375893a5"
                }
            ]
        },
        {
            "id": "eb19203b-8e4b-458e-95d6-3299c7dc201e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "compositeImage": {
                "id": "05917705-532a-48f6-bcb1-327ff5c0c2c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb19203b-8e4b-458e-95d6-3299c7dc201e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961b674c-3a9a-4de8-b187-c53f186c3074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb19203b-8e4b-458e-95d6-3299c7dc201e",
                    "LayerId": "cec375dd-4039-4ac8-9ee1-5d71375893a5"
                }
            ]
        },
        {
            "id": "e51aa8f9-cc80-4d1a-9995-3c94620cd715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "compositeImage": {
                "id": "9c0979e6-81ff-4fcd-bbc0-0ab9faba12d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e51aa8f9-cc80-4d1a-9995-3c94620cd715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c032d68a-3455-427e-97a6-5d30188ef160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e51aa8f9-cc80-4d1a-9995-3c94620cd715",
                    "LayerId": "cec375dd-4039-4ac8-9ee1-5d71375893a5"
                }
            ]
        },
        {
            "id": "e25ec105-09d1-4759-aad4-4750407acf9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "compositeImage": {
                "id": "74659f89-66aa-4716-af5a-22b697c41e73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e25ec105-09d1-4759-aad4-4750407acf9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b316aa47-2304-4ea1-a593-e40fe0e119bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e25ec105-09d1-4759-aad4-4750407acf9b",
                    "LayerId": "cec375dd-4039-4ac8-9ee1-5d71375893a5"
                }
            ]
        },
        {
            "id": "75b32db9-d229-4463-873b-357be53d651b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "compositeImage": {
                "id": "66a70ed1-20d7-4f8d-ae3c-4b6c6260e624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75b32db9-d229-4463-873b-357be53d651b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a53828aa-513e-4c44-b04a-76ca95de14d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75b32db9-d229-4463-873b-357be53d651b",
                    "LayerId": "cec375dd-4039-4ac8-9ee1-5d71375893a5"
                }
            ]
        },
        {
            "id": "e5fef32c-fe18-41c4-8445-144634455161",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "compositeImage": {
                "id": "41da9bbe-e082-46ad-9f2c-8ab5c7fdbc68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5fef32c-fe18-41c4-8445-144634455161",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ced6719-02fd-49ec-94d3-7e15faefd386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5fef32c-fe18-41c4-8445-144634455161",
                    "LayerId": "cec375dd-4039-4ac8-9ee1-5d71375893a5"
                }
            ]
        },
        {
            "id": "4261c4e1-0fae-4f84-aa99-c787c7992b28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "compositeImage": {
                "id": "09bba34d-996d-4604-9d25-e88fece84a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4261c4e1-0fae-4f84-aa99-c787c7992b28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dfaf6b9-be88-4fc6-9afa-69216dd39d1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4261c4e1-0fae-4f84-aa99-c787c7992b28",
                    "LayerId": "cec375dd-4039-4ac8-9ee1-5d71375893a5"
                }
            ]
        },
        {
            "id": "80b4e0fc-9707-4666-835e-ce071c472f7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "compositeImage": {
                "id": "8a75d5af-e23e-465b-8f01-524304b33400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b4e0fc-9707-4666-835e-ce071c472f7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2daf5ee2-0acb-4c1a-8b2e-1855f3ba3fab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b4e0fc-9707-4666-835e-ce071c472f7c",
                    "LayerId": "cec375dd-4039-4ac8-9ee1-5d71375893a5"
                }
            ]
        },
        {
            "id": "5c08f1be-6988-4ae9-a34e-36c5a9c33138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "compositeImage": {
                "id": "29304c9a-51d7-472c-982a-de11377f2087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c08f1be-6988-4ae9-a34e-36c5a9c33138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1169544-21ac-4955-8b7d-cd87b41c1d15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c08f1be-6988-4ae9-a34e-36c5a9c33138",
                    "LayerId": "cec375dd-4039-4ac8-9ee1-5d71375893a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "cec375dd-4039-4ac8-9ee1-5d71375893a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0e1b71c-6fcc-4414-b949-88718b923965",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 50,
    "yorig": 48
}