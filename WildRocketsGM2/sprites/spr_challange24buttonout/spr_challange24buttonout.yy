{
    "id": "374219c3-9777-4137-9452-4afd5f3117e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange24buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 0,
    "bbox_right": 104,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01d6f893-8654-4386-968e-f2edd76e1492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "compositeImage": {
                "id": "df841263-c061-45a4-b6c8-80e1c54c24a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01d6f893-8654-4386-968e-f2edd76e1492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d83de25-dd6d-4d62-8150-6e7e2de07723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d6f893-8654-4386-968e-f2edd76e1492",
                    "LayerId": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483"
                }
            ]
        },
        {
            "id": "a22634c5-6042-4cb1-b5ee-b3f1451c493c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "compositeImage": {
                "id": "3b137645-2413-4712-9cab-25960118e048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a22634c5-6042-4cb1-b5ee-b3f1451c493c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cbf8bc6-7626-462b-8338-5892dea0bbf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a22634c5-6042-4cb1-b5ee-b3f1451c493c",
                    "LayerId": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483"
                }
            ]
        },
        {
            "id": "4d667849-8a80-48af-abd5-d3c736329e87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "compositeImage": {
                "id": "1c215f72-00b2-4b72-8b46-f8bc0d50d695",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d667849-8a80-48af-abd5-d3c736329e87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0718a5bf-6b5e-4b52-810b-0aa72bab2f2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d667849-8a80-48af-abd5-d3c736329e87",
                    "LayerId": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483"
                }
            ]
        },
        {
            "id": "24a999a2-d53a-47fc-8304-b45a6ee4d420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "compositeImage": {
                "id": "e61a4ee3-bf1d-4eab-93b3-6823639bd886",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24a999a2-d53a-47fc-8304-b45a6ee4d420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b0ea4eb-8b15-455f-899b-a816d89df5c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24a999a2-d53a-47fc-8304-b45a6ee4d420",
                    "LayerId": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483"
                }
            ]
        },
        {
            "id": "68d6b04e-0658-4d5f-ad64-cfd98488fe00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "compositeImage": {
                "id": "79720f2f-0be2-4682-ad1b-a7f8001589e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d6b04e-0658-4d5f-ad64-cfd98488fe00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0606c629-2549-4953-b9c8-5a327e4a8fa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d6b04e-0658-4d5f-ad64-cfd98488fe00",
                    "LayerId": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483"
                }
            ]
        },
        {
            "id": "e6e7e61f-52de-4bd2-acc5-9f772ef616c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "compositeImage": {
                "id": "e1ab7831-01c7-4222-b393-0f289ab37173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6e7e61f-52de-4bd2-acc5-9f772ef616c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ed69499-0cd5-4b5a-8305-aa2f60e4d4a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6e7e61f-52de-4bd2-acc5-9f772ef616c2",
                    "LayerId": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483"
                }
            ]
        },
        {
            "id": "4c754bc7-03c0-4cb0-8d83-232c11c6af51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "compositeImage": {
                "id": "21151547-c315-4c66-a910-7f756fe4cd13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c754bc7-03c0-4cb0-8d83-232c11c6af51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d389c215-d6ba-4850-9298-270070946d6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c754bc7-03c0-4cb0-8d83-232c11c6af51",
                    "LayerId": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483"
                }
            ]
        },
        {
            "id": "089c77a4-822b-4bfc-9206-78c91ecbab44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "compositeImage": {
                "id": "ecc51331-d7aa-4b05-ad4c-b10c4c41fcce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "089c77a4-822b-4bfc-9206-78c91ecbab44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b28a8a1-2d5f-4409-9edd-3cfe18f15455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "089c77a4-822b-4bfc-9206-78c91ecbab44",
                    "LayerId": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483"
                }
            ]
        },
        {
            "id": "0f4de743-f721-407a-96ea-c9e948ea0a82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "compositeImage": {
                "id": "9c2e737c-de00-4e54-87b8-9f390d218c2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f4de743-f721-407a-96ea-c9e948ea0a82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae18e108-e126-4e30-921b-824c68d1e1bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f4de743-f721-407a-96ea-c9e948ea0a82",
                    "LayerId": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483"
                }
            ]
        },
        {
            "id": "453a4c1b-0a4d-46f7-a4d6-8505dabf0581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "compositeImage": {
                "id": "945fa84f-b16b-4749-bc42-ecc93d65ad98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "453a4c1b-0a4d-46f7-a4d6-8505dabf0581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd871340-e184-4541-81b8-360fbf0c7048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "453a4c1b-0a4d-46f7-a4d6-8505dabf0581",
                    "LayerId": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "9a40cb6c-f5f6-403b-ae73-a6855f8cb483",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "374219c3-9777-4137-9452-4afd5f3117e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 53,
    "yorig": 52
}