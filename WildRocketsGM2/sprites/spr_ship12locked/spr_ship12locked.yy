{
    "id": "fc9c153a-fe88-47cc-8544-00b4e5b680c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship12locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68b050b9-99f8-4985-b2fc-a8a08e33e7e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc9c153a-fe88-47cc-8544-00b4e5b680c0",
            "compositeImage": {
                "id": "8c1e0063-1df7-438c-9af4-06249bd6bf13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68b050b9-99f8-4985-b2fc-a8a08e33e7e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ba04a6d-ace2-4f3d-93e9-0199cc9ab7ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68b050b9-99f8-4985-b2fc-a8a08e33e7e2",
                    "LayerId": "c706e373-b069-49c6-a41b-c54daebac20f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c706e373-b069-49c6-a41b-c54daebac20f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc9c153a-fe88-47cc-8544-00b4e5b680c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}