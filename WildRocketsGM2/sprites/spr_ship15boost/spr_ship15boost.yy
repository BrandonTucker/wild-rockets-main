{
    "id": "faa6428a-f638-4112-b7e9-f70b76c5667d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship15boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 30,
    "bbox_right": 55,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b7a92dd-e3c0-4de4-ac21-2c350536097f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faa6428a-f638-4112-b7e9-f70b76c5667d",
            "compositeImage": {
                "id": "018bd840-d982-4e9c-9c20-953826b8e1c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7a92dd-e3c0-4de4-ac21-2c350536097f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "008a6db9-c404-49af-aa21-c736bc5e20cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7a92dd-e3c0-4de4-ac21-2c350536097f",
                    "LayerId": "4d059157-e7c6-4f73-99b2-10dae2c2be11"
                }
            ]
        },
        {
            "id": "1251a2a1-e368-4def-be9d-2150f6ffb3df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faa6428a-f638-4112-b7e9-f70b76c5667d",
            "compositeImage": {
                "id": "fcfc88dc-263a-4505-a62f-e56cc6769022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1251a2a1-e368-4def-be9d-2150f6ffb3df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f229c76-2945-4b65-b613-eedbf7f6fbf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1251a2a1-e368-4def-be9d-2150f6ffb3df",
                    "LayerId": "4d059157-e7c6-4f73-99b2-10dae2c2be11"
                }
            ]
        },
        {
            "id": "7f54f1a7-366a-4d5a-a25b-dbe7c85a9a4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faa6428a-f638-4112-b7e9-f70b76c5667d",
            "compositeImage": {
                "id": "f3c4644d-890f-46f6-8547-2fef71ca0c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f54f1a7-366a-4d5a-a25b-dbe7c85a9a4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f7ab4c6-6904-4a9a-bc72-d6d4411181dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f54f1a7-366a-4d5a-a25b-dbe7c85a9a4e",
                    "LayerId": "4d059157-e7c6-4f73-99b2-10dae2c2be11"
                }
            ]
        },
        {
            "id": "52e84870-acc7-41d0-a942-7907ac5123b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faa6428a-f638-4112-b7e9-f70b76c5667d",
            "compositeImage": {
                "id": "7ce96653-f1d8-4215-b5f9-311a13c89551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e84870-acc7-41d0-a942-7907ac5123b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07b46c0e-50d9-41a8-9bd8-ac7686210eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e84870-acc7-41d0-a942-7907ac5123b6",
                    "LayerId": "4d059157-e7c6-4f73-99b2-10dae2c2be11"
                }
            ]
        },
        {
            "id": "fb677855-296a-40af-9c72-1c434a5164c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faa6428a-f638-4112-b7e9-f70b76c5667d",
            "compositeImage": {
                "id": "88d06bee-eabd-4199-8108-65f876cc81fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb677855-296a-40af-9c72-1c434a5164c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f151bff-893b-427f-9700-6952e5b1699e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb677855-296a-40af-9c72-1c434a5164c5",
                    "LayerId": "4d059157-e7c6-4f73-99b2-10dae2c2be11"
                }
            ]
        },
        {
            "id": "49d14049-56bc-4929-afb0-a72d4bf39146",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faa6428a-f638-4112-b7e9-f70b76c5667d",
            "compositeImage": {
                "id": "10ad702c-b5df-43d1-ab4e-6efeaebf210d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49d14049-56bc-4929-afb0-a72d4bf39146",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e462b65-9f5f-44f2-bf7f-843cde536719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49d14049-56bc-4929-afb0-a72d4bf39146",
                    "LayerId": "4d059157-e7c6-4f73-99b2-10dae2c2be11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "4d059157-e7c6-4f73-99b2-10dae2c2be11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "faa6428a-f638-4112-b7e9-f70b76c5667d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 43,
    "yorig": 40
}