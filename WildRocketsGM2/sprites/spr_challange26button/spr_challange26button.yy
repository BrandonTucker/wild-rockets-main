{
    "id": "71042fe6-5fc5-4d6e-abbf-567e5cfa9061",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange26button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab0e10b2-b518-43a5-a41b-8ae2dbd76a74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71042fe6-5fc5-4d6e-abbf-567e5cfa9061",
            "compositeImage": {
                "id": "0e41472a-e208-4330-b444-5df4c2b49748",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab0e10b2-b518-43a5-a41b-8ae2dbd76a74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b40b4908-583e-4393-9f62-458561b55faa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab0e10b2-b518-43a5-a41b-8ae2dbd76a74",
                    "LayerId": "bccfec32-8d89-4c56-ba35-12a198102cd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "bccfec32-8d89-4c56-ba35-12a198102cd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71042fe6-5fc5-4d6e-abbf-567e5cfa9061",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}