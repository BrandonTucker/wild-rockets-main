{
    "id": "18ebbeed-2514-4e7a-b28e-03811db3df4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship22locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84c88ef1-9abc-44db-95c5-d0e3aff8dd6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18ebbeed-2514-4e7a-b28e-03811db3df4e",
            "compositeImage": {
                "id": "5f969de4-96ff-4b4c-ac5e-39acd3961098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84c88ef1-9abc-44db-95c5-d0e3aff8dd6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b673dc40-969c-4a88-92e7-5c091ca43550",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84c88ef1-9abc-44db-95c5-d0e3aff8dd6d",
                    "LayerId": "4704194a-30a4-41d9-a1b7-1f1be65be1f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4704194a-30a4-41d9-a1b7-1f1be65be1f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18ebbeed-2514-4e7a-b28e-03811db3df4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}