{
    "id": "85c085d9-3160-4f65-878e-26bbd4553701",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship30button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23181fde-eaf4-451a-925e-62ac2e4bdedf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85c085d9-3160-4f65-878e-26bbd4553701",
            "compositeImage": {
                "id": "d48befed-d379-43f1-9d7e-3cdb6cdf92b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23181fde-eaf4-451a-925e-62ac2e4bdedf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9ee8a33-510f-475c-a1ed-edf7e82706a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23181fde-eaf4-451a-925e-62ac2e4bdedf",
                    "LayerId": "3bc469a0-8d19-4bbe-b4a2-5bfec38a4f4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "3bc469a0-8d19-4bbe-b4a2-5bfec38a4f4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85c085d9-3160-4f65-878e-26bbd4553701",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}