{
    "id": "89d497a0-f79b-455f-b61d-d0a347edae32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage5button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6657135-cfb2-4c6c-a960-7c2fc6050f62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d497a0-f79b-455f-b61d-d0a347edae32",
            "compositeImage": {
                "id": "45d36a43-7019-4d75-a3a7-87c40f8c8604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6657135-cfb2-4c6c-a960-7c2fc6050f62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2f76adc-4b72-4544-a6e2-75818a3fb473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6657135-cfb2-4c6c-a960-7c2fc6050f62",
                    "LayerId": "671af01f-4593-4bdf-989b-342e6c77faea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "671af01f-4593-4bdf-989b-342e6c77faea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89d497a0-f79b-455f-b61d-d0a347edae32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}