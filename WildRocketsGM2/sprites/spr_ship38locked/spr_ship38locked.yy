{
    "id": "07e61eeb-5ad6-4413-8abf-549433adbc67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship38locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2df956f-9177-4938-9016-f96f0997ebde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07e61eeb-5ad6-4413-8abf-549433adbc67",
            "compositeImage": {
                "id": "8c6325f4-564d-4466-a320-5b0500e8368b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2df956f-9177-4938-9016-f96f0997ebde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a639fe-237e-4538-adc6-880ed18a816c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2df956f-9177-4938-9016-f96f0997ebde",
                    "LayerId": "6e5edb01-abf0-4b4a-981f-7a57e7e306a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6e5edb01-abf0-4b4a-981f-7a57e7e306a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07e61eeb-5ad6-4413-8abf-549433adbc67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}