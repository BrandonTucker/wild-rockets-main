{
    "id": "b91a55a4-5842-490e-84b6-81afcb6117c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 22,
    "bbox_right": 47,
    "bbox_top": -6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c9ef61a-b4c6-4d98-872b-86315178d40b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b91a55a4-5842-490e-84b6-81afcb6117c4",
            "compositeImage": {
                "id": "b3e69ccc-7a78-477e-acea-449393ca783a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c9ef61a-b4c6-4d98-872b-86315178d40b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed71149c-7f6c-4347-8b73-b65b9e12635f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c9ef61a-b4c6-4d98-872b-86315178d40b",
                    "LayerId": "88e4dc85-8509-4646-8f9f-a270e8fa88d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "88e4dc85-8509-4646-8f9f-a270e8fa88d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b91a55a4-5842-490e-84b6-81afcb6117c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 34,
    "yorig": 27
}