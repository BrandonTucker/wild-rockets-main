{
    "id": "c9e2604e-bc70-4d5d-836d-0c048ec87a35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship4button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 88,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e2989be-90e1-4e0a-bbee-b61812a1d733",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e2604e-bc70-4d5d-836d-0c048ec87a35",
            "compositeImage": {
                "id": "86c98641-e3a2-4b13-b579-d0ca90fa8b42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e2989be-90e1-4e0a-bbee-b61812a1d733",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a19c3405-7046-493e-a4f5-f3c8d5ed2521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e2989be-90e1-4e0a-bbee-b61812a1d733",
                    "LayerId": "0a9bf8b4-8828-4bf9-bdbf-c81ee43d7242"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "0a9bf8b4-8828-4bf9-bdbf-c81ee43d7242",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9e2604e-bc70-4d5d-836d-0c048ec87a35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}