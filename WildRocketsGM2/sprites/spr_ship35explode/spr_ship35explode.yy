{
    "id": "823dfbe8-c732-4b30-aeba-fdd4410a62aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship35explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ea2efe4-8439-4895-85ab-066a93aa1b4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823dfbe8-c732-4b30-aeba-fdd4410a62aa",
            "compositeImage": {
                "id": "2e4fdb1e-8159-44e3-a186-52709fcfcb08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea2efe4-8439-4895-85ab-066a93aa1b4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb80e551-39f2-4620-b052-fb826d715f46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea2efe4-8439-4895-85ab-066a93aa1b4f",
                    "LayerId": "620af703-0ebb-413a-8a59-820762a35f00"
                }
            ]
        },
        {
            "id": "ca4c063f-8450-4d92-a2d6-d84b9952cbdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823dfbe8-c732-4b30-aeba-fdd4410a62aa",
            "compositeImage": {
                "id": "9348c149-959f-4eca-9d0f-5502ffae5d7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca4c063f-8450-4d92-a2d6-d84b9952cbdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09a2f601-e3b8-484c-a894-5dc6f1ee5389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca4c063f-8450-4d92-a2d6-d84b9952cbdf",
                    "LayerId": "620af703-0ebb-413a-8a59-820762a35f00"
                }
            ]
        },
        {
            "id": "0d46b042-b2d6-417d-bc8d-5b00e6bc8148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823dfbe8-c732-4b30-aeba-fdd4410a62aa",
            "compositeImage": {
                "id": "5f4451aa-82d9-46f2-a48d-e9e9e86e079f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d46b042-b2d6-417d-bc8d-5b00e6bc8148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "586cb5d3-3070-4b90-9202-2f983511c07a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d46b042-b2d6-417d-bc8d-5b00e6bc8148",
                    "LayerId": "620af703-0ebb-413a-8a59-820762a35f00"
                }
            ]
        },
        {
            "id": "305ba14c-ebe9-4c00-b871-16a3ec4bb3f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823dfbe8-c732-4b30-aeba-fdd4410a62aa",
            "compositeImage": {
                "id": "aae3d264-36db-4fd5-8698-6d3ecd3f2ba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305ba14c-ebe9-4c00-b871-16a3ec4bb3f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded36a19-45d9-4b8b-bda3-bdf6d8f4fd37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305ba14c-ebe9-4c00-b871-16a3ec4bb3f4",
                    "LayerId": "620af703-0ebb-413a-8a59-820762a35f00"
                }
            ]
        },
        {
            "id": "5208dc43-beb2-4cd7-a24d-50201bd41c41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "823dfbe8-c732-4b30-aeba-fdd4410a62aa",
            "compositeImage": {
                "id": "8eec52ff-eeac-4b3e-acd1-32c731541161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5208dc43-beb2-4cd7-a24d-50201bd41c41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ade0d98-aef2-40af-85d6-b8f7cf073104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5208dc43-beb2-4cd7-a24d-50201bd41c41",
                    "LayerId": "620af703-0ebb-413a-8a59-820762a35f00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 190,
    "layers": [
        {
            "id": "620af703-0ebb-413a-8a59-820762a35f00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "823dfbe8-c732-4b30-aeba-fdd4410a62aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 149,
    "xorig": 74,
    "yorig": 90
}