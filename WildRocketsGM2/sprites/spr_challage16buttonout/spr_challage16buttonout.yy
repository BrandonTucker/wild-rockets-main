{
    "id": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage16buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1fcfbbc-628e-4d04-8c6a-b2c545648dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "compositeImage": {
                "id": "9163cf23-c1ca-4c79-893c-8c10de4d1c65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1fcfbbc-628e-4d04-8c6a-b2c545648dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "817b23b6-7aab-4e5c-a47c-5b0cccf38f85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1fcfbbc-628e-4d04-8c6a-b2c545648dfc",
                    "LayerId": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652"
                }
            ]
        },
        {
            "id": "c7fec4e6-485c-4469-a9b0-72592e2c9365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "compositeImage": {
                "id": "3dff1d60-d1bc-4ded-b705-6d49920c4bd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7fec4e6-485c-4469-a9b0-72592e2c9365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfe360d3-9d38-464c-9e94-28ab2f009d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7fec4e6-485c-4469-a9b0-72592e2c9365",
                    "LayerId": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652"
                }
            ]
        },
        {
            "id": "697f540b-211e-433a-a30e-5e5bb95b2c50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "compositeImage": {
                "id": "8ca19fae-b0c4-448d-a643-c635d5e53256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "697f540b-211e-433a-a30e-5e5bb95b2c50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3c2800e-b8dc-428a-add4-dc3374943fb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "697f540b-211e-433a-a30e-5e5bb95b2c50",
                    "LayerId": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652"
                }
            ]
        },
        {
            "id": "349c8172-d65d-4083-8ec5-ab2655f32dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "compositeImage": {
                "id": "8d0faa59-0cfc-4d7b-9516-7d1be9899ace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349c8172-d65d-4083-8ec5-ab2655f32dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "966681da-f90a-4e55-b92e-0f7ab3fa654e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349c8172-d65d-4083-8ec5-ab2655f32dfe",
                    "LayerId": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652"
                }
            ]
        },
        {
            "id": "7cd00e32-269e-4db3-a431-3a81dad35c20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "compositeImage": {
                "id": "f2786964-0e7d-40fe-afa0-df9435d88079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cd00e32-269e-4db3-a431-3a81dad35c20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc108a0e-3140-4da8-acc7-03b9617b9740",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cd00e32-269e-4db3-a431-3a81dad35c20",
                    "LayerId": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652"
                }
            ]
        },
        {
            "id": "07f91205-b6e0-4dd5-9ab5-195270e9aea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "compositeImage": {
                "id": "395e1a8c-4e09-4cdf-af23-cd5279219184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07f91205-b6e0-4dd5-9ab5-195270e9aea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1312ace4-a39a-4d35-894a-060562c0ea43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07f91205-b6e0-4dd5-9ab5-195270e9aea8",
                    "LayerId": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652"
                }
            ]
        },
        {
            "id": "39df2e3a-4171-4900-aa5c-e75f6c0647c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "compositeImage": {
                "id": "bd5e41f3-81d9-4aee-a4ac-0775c3061365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39df2e3a-4171-4900-aa5c-e75f6c0647c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23384cba-99ea-49a7-a31e-8bd4358d5162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39df2e3a-4171-4900-aa5c-e75f6c0647c7",
                    "LayerId": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652"
                }
            ]
        },
        {
            "id": "3596e953-7a2c-4bc8-9ecc-ec3af7a2049c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "compositeImage": {
                "id": "f7531fa7-d96a-4361-b085-e541c18f36a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3596e953-7a2c-4bc8-9ecc-ec3af7a2049c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7384d012-d3f4-426c-b1ca-3cc24c7926e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3596e953-7a2c-4bc8-9ecc-ec3af7a2049c",
                    "LayerId": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652"
                }
            ]
        },
        {
            "id": "ebe09d3d-bbd2-4370-ae18-a4195dd9de29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "compositeImage": {
                "id": "5471d4da-5ccf-44b3-b5f5-769351785a46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebe09d3d-bbd2-4370-ae18-a4195dd9de29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f4dd3d9-efde-4be1-9f29-3d58ac6db16a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebe09d3d-bbd2-4370-ae18-a4195dd9de29",
                    "LayerId": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652"
                }
            ]
        },
        {
            "id": "b1675410-f29b-42fb-a5cf-71aef5d134fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "compositeImage": {
                "id": "640b1e6f-dff3-415b-8529-15da09648baa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1675410-f29b-42fb-a5cf-71aef5d134fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50d9045b-c7e3-40e8-93a6-a25bdb1bedb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1675410-f29b-42fb-a5cf-71aef5d134fe",
                    "LayerId": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4bafdf69-7c99-462d-9d6d-91b3cbb4d652",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7bd64e7-eab9-40f8-83e6-a3950266fac9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}