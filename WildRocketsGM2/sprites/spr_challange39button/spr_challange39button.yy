{
    "id": "5dcd1170-fb01-437e-9727-dcf384d05d76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange39button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a087ec22-ab0d-4532-9818-38136b881556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5dcd1170-fb01-437e-9727-dcf384d05d76",
            "compositeImage": {
                "id": "6e98436b-fefc-4165-9050-c8e9a51eda81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a087ec22-ab0d-4532-9818-38136b881556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a1b84a3-6cb0-4dfd-8041-3e6772925bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a087ec22-ab0d-4532-9818-38136b881556",
                    "LayerId": "6e13ad49-3ce8-405c-a7a7-ea7bf85c6d0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6e13ad49-3ce8-405c-a7a7-ea7bf85c6d0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5dcd1170-fb01-437e-9727-dcf384d05d76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}