{
    "id": "1ae39a7e-acbb-4d87-83d4-c0d2d7a9d406",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lifepop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 5,
    "bbox_right": 76,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de121f89-b882-470d-8bcb-526bd700713c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ae39a7e-acbb-4d87-83d4-c0d2d7a9d406",
            "compositeImage": {
                "id": "23b7fac1-7f29-49a5-a9d7-26b9d214973d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de121f89-b882-470d-8bcb-526bd700713c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c942a7c1-3211-4977-b984-1f14a3fa0198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de121f89-b882-470d-8bcb-526bd700713c",
                    "LayerId": "da4171c2-f463-4020-9f17-373f95bdcaa2"
                }
            ]
        },
        {
            "id": "26a1d168-729c-4c9d-9ae5-0daa8a5eada8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ae39a7e-acbb-4d87-83d4-c0d2d7a9d406",
            "compositeImage": {
                "id": "dc5df613-cb0f-4509-8b80-95eeea28d955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26a1d168-729c-4c9d-9ae5-0daa8a5eada8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fac54ef-4bd3-44ca-b672-8a8523d5a2e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a1d168-729c-4c9d-9ae5-0daa8a5eada8",
                    "LayerId": "da4171c2-f463-4020-9f17-373f95bdcaa2"
                }
            ]
        },
        {
            "id": "a3ea8582-e219-4af7-b81f-fbb4b4fc9708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ae39a7e-acbb-4d87-83d4-c0d2d7a9d406",
            "compositeImage": {
                "id": "8d019a3e-cad6-4491-ad3c-89ef8079467b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3ea8582-e219-4af7-b81f-fbb4b4fc9708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78928851-bf9a-4a7f-84bb-d824496fb1a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3ea8582-e219-4af7-b81f-fbb4b4fc9708",
                    "LayerId": "da4171c2-f463-4020-9f17-373f95bdcaa2"
                }
            ]
        },
        {
            "id": "4c9ff573-da3a-4f6f-98e1-302ec470c03d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ae39a7e-acbb-4d87-83d4-c0d2d7a9d406",
            "compositeImage": {
                "id": "9963b560-682c-4f20-a3a4-b872180f0d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c9ff573-da3a-4f6f-98e1-302ec470c03d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd2820a6-0d61-4633-a9f9-6502ae1f09da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c9ff573-da3a-4f6f-98e1-302ec470c03d",
                    "LayerId": "da4171c2-f463-4020-9f17-373f95bdcaa2"
                }
            ]
        },
        {
            "id": "1cf55147-9e4d-41a4-8214-6e104912733f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ae39a7e-acbb-4d87-83d4-c0d2d7a9d406",
            "compositeImage": {
                "id": "38a3d89a-33c1-4ca7-8dfa-3f14e1c2ba28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cf55147-9e4d-41a4-8214-6e104912733f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09f3c8f0-c843-44cb-baa1-bfb90cfc8636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cf55147-9e4d-41a4-8214-6e104912733f",
                    "LayerId": "da4171c2-f463-4020-9f17-373f95bdcaa2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "da4171c2-f463-4020-9f17-373f95bdcaa2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ae39a7e-acbb-4d87-83d4-c0d2d7a9d406",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 38,
    "yorig": 38
}