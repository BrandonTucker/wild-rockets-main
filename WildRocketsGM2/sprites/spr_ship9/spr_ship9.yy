{
    "id": "20d59731-fed5-49a7-b546-581ebf2fcab1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69eba1eb-feea-4125-b1c6-ed4323c51dcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20d59731-fed5-49a7-b546-581ebf2fcab1",
            "compositeImage": {
                "id": "cf622bcb-6faf-4e0c-a292-d8d0c4547354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69eba1eb-feea-4125-b1c6-ed4323c51dcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14cf6730-abd7-4e0f-a61a-99ca756340f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69eba1eb-feea-4125-b1c6-ed4323c51dcc",
                    "LayerId": "1df22fd0-2e22-45b5-84c1-84bfa1562af9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "1df22fd0-2e22-45b5-84c1-84bfa1562af9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20d59731-fed5-49a7-b546-581ebf2fcab1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 43,
    "yorig": 56
}