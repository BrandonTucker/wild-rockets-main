{
    "id": "96fdeedb-3dcd-4cd0-934c-4da390ba7770",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship23locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d50a52fc-889d-4d06-8361-020db6ddf9bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96fdeedb-3dcd-4cd0-934c-4da390ba7770",
            "compositeImage": {
                "id": "5b9f44b9-c366-47ea-b2ba-9c9952a53766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d50a52fc-889d-4d06-8361-020db6ddf9bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ffc65c9-82d6-42ec-8b36-a7d1dffd1712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d50a52fc-889d-4d06-8361-020db6ddf9bc",
                    "LayerId": "dbb43390-95ae-40a6-ac3a-7eb2dc287d27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "dbb43390-95ae-40a6-ac3a-7eb2dc287d27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96fdeedb-3dcd-4cd0-934c-4da390ba7770",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}