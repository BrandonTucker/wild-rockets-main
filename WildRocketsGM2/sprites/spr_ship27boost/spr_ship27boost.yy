{
    "id": "d9d81d9d-4169-4c8d-ac2f-a1ed63179efc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship27boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86efe035-23a1-40fd-9e86-27d798e442ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9d81d9d-4169-4c8d-ac2f-a1ed63179efc",
            "compositeImage": {
                "id": "f25de526-16e0-4043-bd93-656f885caafc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86efe035-23a1-40fd-9e86-27d798e442ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1b3b826-5c8d-47cb-b690-e419e3b29643",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86efe035-23a1-40fd-9e86-27d798e442ce",
                    "LayerId": "9f779363-6183-4c52-be67-1b019d5f7420"
                }
            ]
        },
        {
            "id": "8bd2adbd-0a78-485e-add3-9be8fda3fb91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9d81d9d-4169-4c8d-ac2f-a1ed63179efc",
            "compositeImage": {
                "id": "385a0fde-6028-45ab-888a-3bae29c8c74b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bd2adbd-0a78-485e-add3-9be8fda3fb91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6feb69-8885-4a2a-9105-b9fbbfdd933b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bd2adbd-0a78-485e-add3-9be8fda3fb91",
                    "LayerId": "9f779363-6183-4c52-be67-1b019d5f7420"
                }
            ]
        },
        {
            "id": "dd5de433-42da-4702-aaa7-df01205b1139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9d81d9d-4169-4c8d-ac2f-a1ed63179efc",
            "compositeImage": {
                "id": "8f2aea81-4c19-4215-af42-66c39d53fde2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd5de433-42da-4702-aaa7-df01205b1139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1858e72-9088-4c81-bbf9-a4773be7c287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd5de433-42da-4702-aaa7-df01205b1139",
                    "LayerId": "9f779363-6183-4c52-be67-1b019d5f7420"
                }
            ]
        },
        {
            "id": "647d9073-59b4-4304-8c62-eb33e7b54c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9d81d9d-4169-4c8d-ac2f-a1ed63179efc",
            "compositeImage": {
                "id": "b627c56c-d91b-4bab-a84f-9ec4deaf7b3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647d9073-59b4-4304-8c62-eb33e7b54c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12e4fae6-1d75-4155-8c7d-f4ec11a20686",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647d9073-59b4-4304-8c62-eb33e7b54c9e",
                    "LayerId": "9f779363-6183-4c52-be67-1b019d5f7420"
                }
            ]
        },
        {
            "id": "ffea9d70-fc5d-41ca-96bc-ec10e9c5b38e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9d81d9d-4169-4c8d-ac2f-a1ed63179efc",
            "compositeImage": {
                "id": "a1d0a47c-aca3-43cf-9849-fefa43ee3d58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffea9d70-fc5d-41ca-96bc-ec10e9c5b38e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8080d433-017d-4969-b3f9-1d90aee1cfd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffea9d70-fc5d-41ca-96bc-ec10e9c5b38e",
                    "LayerId": "9f779363-6183-4c52-be67-1b019d5f7420"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "9f779363-6183-4c52-be67-1b019d5f7420",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9d81d9d-4169-4c8d-ac2f-a1ed63179efc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 50
}