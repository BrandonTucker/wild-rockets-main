{
    "id": "788bcb20-998f-4ac3-a132-00ae3fb37533",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship12explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 22,
    "bbox_right": 60,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f51a0f0a-948c-436d-9fe8-88c0a7ec5bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788bcb20-998f-4ac3-a132-00ae3fb37533",
            "compositeImage": {
                "id": "f6b6a261-8ccc-4359-bc81-4cf34cae230b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f51a0f0a-948c-436d-9fe8-88c0a7ec5bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea23372-5a34-4884-a0e4-1fe4ae76f9cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f51a0f0a-948c-436d-9fe8-88c0a7ec5bb3",
                    "LayerId": "6e53338f-0641-4e42-8abc-bc4664ad5ef0"
                }
            ]
        },
        {
            "id": "6fc08ee5-08d3-4381-8c18-bccbaea40484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788bcb20-998f-4ac3-a132-00ae3fb37533",
            "compositeImage": {
                "id": "729907b9-8f45-495e-811f-310a08b44f77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fc08ee5-08d3-4381-8c18-bccbaea40484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45679ecf-7f75-4c30-b5dd-cd444768a707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fc08ee5-08d3-4381-8c18-bccbaea40484",
                    "LayerId": "6e53338f-0641-4e42-8abc-bc4664ad5ef0"
                }
            ]
        },
        {
            "id": "c1b9f710-5e28-43f6-8b43-30eec306022d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788bcb20-998f-4ac3-a132-00ae3fb37533",
            "compositeImage": {
                "id": "4e601b7a-867a-425d-a9ab-4155ba34de92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1b9f710-5e28-43f6-8b43-30eec306022d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fa82447-ea93-445e-a823-3bad67792e0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1b9f710-5e28-43f6-8b43-30eec306022d",
                    "LayerId": "6e53338f-0641-4e42-8abc-bc4664ad5ef0"
                }
            ]
        },
        {
            "id": "3a978b70-6b7c-449e-8138-a52314bb06ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788bcb20-998f-4ac3-a132-00ae3fb37533",
            "compositeImage": {
                "id": "1604db1b-7bf2-4e8d-99b7-795b7e731e37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a978b70-6b7c-449e-8138-a52314bb06ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca239e16-c5a5-4a7b-98f7-ea6c7960f44c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a978b70-6b7c-449e-8138-a52314bb06ae",
                    "LayerId": "6e53338f-0641-4e42-8abc-bc4664ad5ef0"
                }
            ]
        },
        {
            "id": "82576809-221c-4fc2-858e-589870d6cdd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788bcb20-998f-4ac3-a132-00ae3fb37533",
            "compositeImage": {
                "id": "cad40aa2-3bce-4db4-9235-ef1a24e02ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82576809-221c-4fc2-858e-589870d6cdd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c257c17d-f05f-411e-a549-c5e1a5590036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82576809-221c-4fc2-858e-589870d6cdd7",
                    "LayerId": "6e53338f-0641-4e42-8abc-bc4664ad5ef0"
                }
            ]
        },
        {
            "id": "f2640394-c416-421d-b780-c8b168ca44c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788bcb20-998f-4ac3-a132-00ae3fb37533",
            "compositeImage": {
                "id": "fb1628b9-379b-400f-828a-fcf4b586cf59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2640394-c416-421d-b780-c8b168ca44c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "706f4feb-9a25-4421-91f7-78fc7a8bea07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2640394-c416-421d-b780-c8b168ca44c2",
                    "LayerId": "6e53338f-0641-4e42-8abc-bc4664ad5ef0"
                }
            ]
        },
        {
            "id": "00afe814-9320-43e9-b448-913cdae38023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788bcb20-998f-4ac3-a132-00ae3fb37533",
            "compositeImage": {
                "id": "751bedaa-1560-4991-8bfb-ff0329da0984",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00afe814-9320-43e9-b448-913cdae38023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f7382e6-87cd-414a-92d0-c07bc7028c44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00afe814-9320-43e9-b448-913cdae38023",
                    "LayerId": "6e53338f-0641-4e42-8abc-bc4664ad5ef0"
                }
            ]
        },
        {
            "id": "3ab73f26-2191-4482-90e9-9759ce394e10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788bcb20-998f-4ac3-a132-00ae3fb37533",
            "compositeImage": {
                "id": "5540c62e-4c37-4365-89cd-78f02c4c162e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ab73f26-2191-4482-90e9-9759ce394e10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45b9a989-8ba3-4647-949e-814f61fc91c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ab73f26-2191-4482-90e9-9759ce394e10",
                    "LayerId": "6e53338f-0641-4e42-8abc-bc4664ad5ef0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "6e53338f-0641-4e42-8abc-bc4664ad5ef0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "788bcb20-998f-4ac3-a132-00ae3fb37533",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 87,
    "xorig": 40,
    "yorig": 26
}