{
    "id": "ec400107-fa1f-4b9f-820c-a9e440b0c201",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship37explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c01ea16-3568-4e8b-b601-b69cdbb5a1d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec400107-fa1f-4b9f-820c-a9e440b0c201",
            "compositeImage": {
                "id": "79676563-2b55-401e-b997-5db97d8210dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c01ea16-3568-4e8b-b601-b69cdbb5a1d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcfe816b-e8e5-40fb-ae81-50b010f53890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c01ea16-3568-4e8b-b601-b69cdbb5a1d7",
                    "LayerId": "8dd457a6-9031-46c7-aa5d-b289700a8c5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "8dd457a6-9031-46c7-aa5d-b289700a8c5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec400107-fa1f-4b9f-820c-a9e440b0c201",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 161,
    "xorig": 80,
    "yorig": 85
}