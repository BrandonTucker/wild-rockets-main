{
    "id": "3c4a0cdf-a91d-497c-8675-144d46c5adc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec8d477f-a494-46a3-ac4a-25589c6163f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4a0cdf-a91d-497c-8675-144d46c5adc8",
            "compositeImage": {
                "id": "daeae029-8946-4090-b465-84d7ce0be8b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec8d477f-a494-46a3-ac4a-25589c6163f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a2a8962-2448-40df-b654-9816e716ed83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec8d477f-a494-46a3-ac4a-25589c6163f6",
                    "LayerId": "ebec0e4b-26e4-4b90-848a-c59b3aac1c6d"
                }
            ]
        },
        {
            "id": "9ec0117b-9c23-49b5-b5da-95484ebb81e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4a0cdf-a91d-497c-8675-144d46c5adc8",
            "compositeImage": {
                "id": "1dc624aa-793b-4427-8de0-60cff494b6a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec0117b-9c23-49b5-b5da-95484ebb81e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c204ed9-735a-4782-9cb5-f6bacddbdb66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec0117b-9c23-49b5-b5da-95484ebb81e0",
                    "LayerId": "ebec0e4b-26e4-4b90-848a-c59b3aac1c6d"
                }
            ]
        },
        {
            "id": "b0fcfbeb-0727-4b5a-8b63-5be47434e9be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4a0cdf-a91d-497c-8675-144d46c5adc8",
            "compositeImage": {
                "id": "d75b8f02-fca6-444e-a40a-44e190b0216e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0fcfbeb-0727-4b5a-8b63-5be47434e9be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40892411-56bf-4074-94bf-db07fbddae87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0fcfbeb-0727-4b5a-8b63-5be47434e9be",
                    "LayerId": "ebec0e4b-26e4-4b90-848a-c59b3aac1c6d"
                }
            ]
        },
        {
            "id": "cb7eee3a-c62f-49d1-b808-6a592e6784d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4a0cdf-a91d-497c-8675-144d46c5adc8",
            "compositeImage": {
                "id": "f992c110-18c6-494a-8e34-c0667382af33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb7eee3a-c62f-49d1-b808-6a592e6784d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f133a66f-266e-464e-9c24-2c9e01ca8521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb7eee3a-c62f-49d1-b808-6a592e6784d5",
                    "LayerId": "ebec0e4b-26e4-4b90-848a-c59b3aac1c6d"
                }
            ]
        },
        {
            "id": "cc3e4fcc-5bed-49d4-9b1c-01e29381c049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4a0cdf-a91d-497c-8675-144d46c5adc8",
            "compositeImage": {
                "id": "56f6beab-1dd6-4b8c-8af6-2511153fee3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc3e4fcc-5bed-49d4-9b1c-01e29381c049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40a6d28c-415b-4459-baf1-9889797911da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc3e4fcc-5bed-49d4-9b1c-01e29381c049",
                    "LayerId": "ebec0e4b-26e4-4b90-848a-c59b3aac1c6d"
                }
            ]
        },
        {
            "id": "519320fb-b21e-486b-a2b2-6fcdcbf54ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4a0cdf-a91d-497c-8675-144d46c5adc8",
            "compositeImage": {
                "id": "87f40cd8-16a8-4f73-9b33-c040986f1bc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "519320fb-b21e-486b-a2b2-6fcdcbf54ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4059332e-c26a-4433-a96e-d85beec3af68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "519320fb-b21e-486b-a2b2-6fcdcbf54ef4",
                    "LayerId": "ebec0e4b-26e4-4b90-848a-c59b3aac1c6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "ebec0e4b-26e4-4b90-848a-c59b3aac1c6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c4a0cdf-a91d-497c-8675-144d46c5adc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 49
}