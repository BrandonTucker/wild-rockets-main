{
    "id": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage15buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11feb636-af0e-4066-a5bf-041d5d7e1b63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "compositeImage": {
                "id": "b81b5d96-315c-427f-8990-2328978e2139",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11feb636-af0e-4066-a5bf-041d5d7e1b63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c5c03f-e256-4d19-83b4-b3b8afe80364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11feb636-af0e-4066-a5bf-041d5d7e1b63",
                    "LayerId": "c04c062a-addc-4905-94a8-478e3b8b3a60"
                }
            ]
        },
        {
            "id": "480ea27b-d2ab-448b-adb3-533d2f5ef920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "compositeImage": {
                "id": "e76abdac-84de-41c4-b359-9dd34fb80d66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "480ea27b-d2ab-448b-adb3-533d2f5ef920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fcd9210-3191-4e6e-aeb1-dc0eb07d32a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "480ea27b-d2ab-448b-adb3-533d2f5ef920",
                    "LayerId": "c04c062a-addc-4905-94a8-478e3b8b3a60"
                }
            ]
        },
        {
            "id": "30f514f0-6c5d-4736-b36a-29e0bb29bd39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "compositeImage": {
                "id": "f1d3eec4-1ef6-42e9-bb08-e46f720ac84f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30f514f0-6c5d-4736-b36a-29e0bb29bd39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c51647-f4f6-4417-be1f-4f7d8cd1c081",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30f514f0-6c5d-4736-b36a-29e0bb29bd39",
                    "LayerId": "c04c062a-addc-4905-94a8-478e3b8b3a60"
                }
            ]
        },
        {
            "id": "af63a240-2af5-4fc3-a306-2a5682c265ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "compositeImage": {
                "id": "a0714b67-737d-4b37-9dca-dc3407372cdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af63a240-2af5-4fc3-a306-2a5682c265ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc55f1b-dff8-45e0-b783-3e264dc5a721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af63a240-2af5-4fc3-a306-2a5682c265ff",
                    "LayerId": "c04c062a-addc-4905-94a8-478e3b8b3a60"
                }
            ]
        },
        {
            "id": "d3e2e40c-f052-403f-954a-a2b165904429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "compositeImage": {
                "id": "13bce3a3-4447-4831-b503-185ac365765b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3e2e40c-f052-403f-954a-a2b165904429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0dc7f5c-53e2-4299-b875-fc9a62aff7bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3e2e40c-f052-403f-954a-a2b165904429",
                    "LayerId": "c04c062a-addc-4905-94a8-478e3b8b3a60"
                }
            ]
        },
        {
            "id": "c3f4652b-2c02-494c-9b9a-499517b683ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "compositeImage": {
                "id": "c4be946a-216c-4597-ae21-751741917df1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f4652b-2c02-494c-9b9a-499517b683ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23943154-797d-4b63-aa3d-141d4508739b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f4652b-2c02-494c-9b9a-499517b683ed",
                    "LayerId": "c04c062a-addc-4905-94a8-478e3b8b3a60"
                }
            ]
        },
        {
            "id": "ab474931-2e6d-45af-bf99-f0b9beadbc75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "compositeImage": {
                "id": "a50e60fc-8bb7-4d42-a7f1-dbaf0ae94fb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab474931-2e6d-45af-bf99-f0b9beadbc75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e0935b2-1294-4138-96e4-5af740f00cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab474931-2e6d-45af-bf99-f0b9beadbc75",
                    "LayerId": "c04c062a-addc-4905-94a8-478e3b8b3a60"
                }
            ]
        },
        {
            "id": "76f0e549-92eb-475e-8a23-bccded730ff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "compositeImage": {
                "id": "09568a2e-ef15-4766-9f56-9a9b1cdad7c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76f0e549-92eb-475e-8a23-bccded730ff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04d8d3c6-840c-4a9a-be31-833ce3bc9e82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76f0e549-92eb-475e-8a23-bccded730ff0",
                    "LayerId": "c04c062a-addc-4905-94a8-478e3b8b3a60"
                }
            ]
        },
        {
            "id": "ec6e3178-77f9-4a15-8f9d-683023354d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "compositeImage": {
                "id": "5a24e125-e27a-42fb-abbf-29ec72f3be30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6e3178-77f9-4a15-8f9d-683023354d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e24bab9e-ac8f-4f1d-a891-78fddf976e3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6e3178-77f9-4a15-8f9d-683023354d74",
                    "LayerId": "c04c062a-addc-4905-94a8-478e3b8b3a60"
                }
            ]
        },
        {
            "id": "75d80287-133f-4c92-a90d-18893a675216",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "compositeImage": {
                "id": "068a5cb6-3d67-405a-b188-ff7de48be748",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75d80287-133f-4c92-a90d-18893a675216",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47d475ff-c29f-4e3c-9e32-2eb67889d325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75d80287-133f-4c92-a90d-18893a675216",
                    "LayerId": "c04c062a-addc-4905-94a8-478e3b8b3a60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c04c062a-addc-4905-94a8-478e3b8b3a60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37b8e9da-1c11-4bb2-9936-9a65a394e5c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}