{
    "id": "4d95506e-53bf-47ac-9068-bd2248ca2c07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship17boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 24,
    "bbox_right": 49,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1aef2224-3e58-49d6-86f8-49e4490a57e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d95506e-53bf-47ac-9068-bd2248ca2c07",
            "compositeImage": {
                "id": "6e89e9db-42fc-462c-a1dd-5b4519688c02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aef2224-3e58-49d6-86f8-49e4490a57e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "496941e8-3b0d-4c8f-baa6-c18c98dddc56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aef2224-3e58-49d6-86f8-49e4490a57e3",
                    "LayerId": "bb6922ad-7470-4f2a-a606-ee847a7e19b9"
                }
            ]
        },
        {
            "id": "11dfc568-79e7-446e-92a1-36356a92205c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d95506e-53bf-47ac-9068-bd2248ca2c07",
            "compositeImage": {
                "id": "9af05d80-c27f-483d-8bdf-210608939db4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11dfc568-79e7-446e-92a1-36356a92205c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "223ff3ba-b966-417b-ae4c-abaca4a028c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11dfc568-79e7-446e-92a1-36356a92205c",
                    "LayerId": "bb6922ad-7470-4f2a-a606-ee847a7e19b9"
                }
            ]
        },
        {
            "id": "a0e7cbb3-1a0c-4515-9c45-0d3fe5893a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d95506e-53bf-47ac-9068-bd2248ca2c07",
            "compositeImage": {
                "id": "c394eadf-54d1-453f-b37e-ec48ae882542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0e7cbb3-1a0c-4515-9c45-0d3fe5893a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "722ecbe3-a841-442d-9d59-4163a9333cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0e7cbb3-1a0c-4515-9c45-0d3fe5893a05",
                    "LayerId": "bb6922ad-7470-4f2a-a606-ee847a7e19b9"
                }
            ]
        },
        {
            "id": "b7de6897-3da1-49e0-a845-39de1fb6782f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d95506e-53bf-47ac-9068-bd2248ca2c07",
            "compositeImage": {
                "id": "e13f1df8-c836-46c9-bf3a-1361d8775e40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7de6897-3da1-49e0-a845-39de1fb6782f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0023b330-8b36-46ae-b85d-61142c491140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7de6897-3da1-49e0-a845-39de1fb6782f",
                    "LayerId": "bb6922ad-7470-4f2a-a606-ee847a7e19b9"
                }
            ]
        },
        {
            "id": "19ed533c-4e28-4908-b2b9-d4886732b311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d95506e-53bf-47ac-9068-bd2248ca2c07",
            "compositeImage": {
                "id": "af898d01-da46-4ef0-b4eb-2facc1c89581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ed533c-4e28-4908-b2b9-d4886732b311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c545f31b-3db3-4f4c-a5a1-a6cb1d5cdce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ed533c-4e28-4908-b2b9-d4886732b311",
                    "LayerId": "bb6922ad-7470-4f2a-a606-ee847a7e19b9"
                }
            ]
        },
        {
            "id": "d67795bf-ad2c-48c6-ad3d-51160e2e3341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d95506e-53bf-47ac-9068-bd2248ca2c07",
            "compositeImage": {
                "id": "92fc09b5-3466-4040-984d-733e5b6b6170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d67795bf-ad2c-48c6-ad3d-51160e2e3341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "478ac4b8-27eb-4d8a-887b-682535c2296e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d67795bf-ad2c-48c6-ad3d-51160e2e3341",
                    "LayerId": "bb6922ad-7470-4f2a-a606-ee847a7e19b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "bb6922ad-7470-4f2a-a606-ee847a7e19b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d95506e-53bf-47ac-9068-bd2248ca2c07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 37,
    "yorig": 45
}