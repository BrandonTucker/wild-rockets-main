{
    "id": "b2724177-762a-4901-83f0-3a6813bf42c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship27locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4080f109-42eb-46fb-abf9-4c57bf5f1f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2724177-762a-4901-83f0-3a6813bf42c6",
            "compositeImage": {
                "id": "a69bfea2-2ca3-44db-b738-aba236ffc42c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4080f109-42eb-46fb-abf9-4c57bf5f1f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55e5649c-d8a7-4843-ab57-6a1141fca36e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4080f109-42eb-46fb-abf9-4c57bf5f1f8b",
                    "LayerId": "bdcdb4ed-fb14-4caa-b8d0-8a2a83a3d7dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "bdcdb4ed-fb14-4caa-b8d0-8a2a83a3d7dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2724177-762a-4901-83f0-3a6813bf42c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}