{
    "id": "70bd9f59-0e51-43c9-a0bf-763d712a15f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship17explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 31,
    "bbox_right": 58,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a49d904-04f6-430a-8219-3fc99eaadb17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bd9f59-0e51-43c9-a0bf-763d712a15f8",
            "compositeImage": {
                "id": "46c46b9c-68a1-4dff-83fe-f842c4f8e3c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a49d904-04f6-430a-8219-3fc99eaadb17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0622db5-3ff7-414e-9dca-e034979f266b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a49d904-04f6-430a-8219-3fc99eaadb17",
                    "LayerId": "97fa0f05-a97e-4b58-b649-09b331790e3f"
                }
            ]
        },
        {
            "id": "688d6bc0-2fc3-4849-84b6-2df567bb26ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bd9f59-0e51-43c9-a0bf-763d712a15f8",
            "compositeImage": {
                "id": "0b31dc06-f80f-4020-a83d-8d58d10de4e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "688d6bc0-2fc3-4849-84b6-2df567bb26ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ad729b4-59e5-47c3-9c3f-1859b834178d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "688d6bc0-2fc3-4849-84b6-2df567bb26ee",
                    "LayerId": "97fa0f05-a97e-4b58-b649-09b331790e3f"
                }
            ]
        },
        {
            "id": "2abb52bc-5084-4e7b-a11f-aa8dd333587e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bd9f59-0e51-43c9-a0bf-763d712a15f8",
            "compositeImage": {
                "id": "05e7cd22-c4d6-4bda-8f51-f3ca991ad31a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2abb52bc-5084-4e7b-a11f-aa8dd333587e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "235ad1ec-1f02-4bd8-a9c4-fb67dbb82162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2abb52bc-5084-4e7b-a11f-aa8dd333587e",
                    "LayerId": "97fa0f05-a97e-4b58-b649-09b331790e3f"
                }
            ]
        },
        {
            "id": "aa7284e8-1652-4ccc-a5d0-ef1e0d016f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bd9f59-0e51-43c9-a0bf-763d712a15f8",
            "compositeImage": {
                "id": "3531bad9-c85d-4042-88d3-2214f2c4b56b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7284e8-1652-4ccc-a5d0-ef1e0d016f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f764ead-08b2-4719-bc6b-d608f211df42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7284e8-1652-4ccc-a5d0-ef1e0d016f77",
                    "LayerId": "97fa0f05-a97e-4b58-b649-09b331790e3f"
                }
            ]
        },
        {
            "id": "2d4ce3f7-7365-4e27-9251-4cf10031ba76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bd9f59-0e51-43c9-a0bf-763d712a15f8",
            "compositeImage": {
                "id": "886e2ec7-ccc1-41aa-af42-c53d6cd42507",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d4ce3f7-7365-4e27-9251-4cf10031ba76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa54b8b6-ca90-4295-a3c8-7961ecf55293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d4ce3f7-7365-4e27-9251-4cf10031ba76",
                    "LayerId": "97fa0f05-a97e-4b58-b649-09b331790e3f"
                }
            ]
        },
        {
            "id": "4e710a15-a593-4674-be64-1189f1ed70fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bd9f59-0e51-43c9-a0bf-763d712a15f8",
            "compositeImage": {
                "id": "5e19b03e-869a-4403-b4e2-e5a31993ee62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e710a15-a593-4674-be64-1189f1ed70fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d0f2651-70fc-4973-bd19-805069ab3815",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e710a15-a593-4674-be64-1189f1ed70fc",
                    "LayerId": "97fa0f05-a97e-4b58-b649-09b331790e3f"
                }
            ]
        },
        {
            "id": "69c49897-7719-4566-a997-44c80672b142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70bd9f59-0e51-43c9-a0bf-763d712a15f8",
            "compositeImage": {
                "id": "0e4f2973-cd34-4de8-9fad-989bd42e373e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69c49897-7719-4566-a997-44c80672b142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d677c08d-b5ed-427c-96d0-b3941b7beeaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69c49897-7719-4566-a997-44c80672b142",
                    "LayerId": "97fa0f05-a97e-4b58-b649-09b331790e3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "97fa0f05-a97e-4b58-b649-09b331790e3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70bd9f59-0e51-43c9-a0bf-763d712a15f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 87,
    "xorig": 43,
    "yorig": 44
}