{
    "id": "d09ae075-a485-40eb-80ee-e78074582cb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship13",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 30,
    "bbox_right": 55,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "045840be-2ec6-4d88-b48f-84fb3d9822e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d09ae075-a485-40eb-80ee-e78074582cb7",
            "compositeImage": {
                "id": "3fe34eae-c600-4659-997f-2eec4a365a18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "045840be-2ec6-4d88-b48f-84fb3d9822e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c7c994b-bf31-4051-8554-0d80eb28cdd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "045840be-2ec6-4d88-b48f-84fb3d9822e6",
                    "LayerId": "abc44542-3d78-436e-abdc-a8e3ccc6c425"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "abc44542-3d78-436e-abdc-a8e3ccc6c425",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d09ae075-a485-40eb-80ee-e78074582cb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 87,
    "xorig": 43,
    "yorig": 42
}