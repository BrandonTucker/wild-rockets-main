{
    "id": "b841c4e1-8070-4286-a1b2-6f2dd869d311",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange30button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1198aba0-a28d-4db3-b814-41d4a5391f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b841c4e1-8070-4286-a1b2-6f2dd869d311",
            "compositeImage": {
                "id": "58d0be3d-470f-439b-89fc-bb4e10d533e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1198aba0-a28d-4db3-b814-41d4a5391f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "673841ee-27e0-4b8e-8017-d96f1207d1e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1198aba0-a28d-4db3-b814-41d4a5391f43",
                    "LayerId": "0b71f414-6889-483c-93d6-167ba20623be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "0b71f414-6889-483c-93d6-167ba20623be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b841c4e1-8070-4286-a1b2-6f2dd869d311",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}