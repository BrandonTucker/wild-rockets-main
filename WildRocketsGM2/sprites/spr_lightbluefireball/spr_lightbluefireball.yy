{
    "id": "1470b819-1795-4256-a239-9cae84d06668",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lightbluefireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00a3fa79-8b92-493c-926f-95d473cfc69c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1470b819-1795-4256-a239-9cae84d06668",
            "compositeImage": {
                "id": "46903ce1-99b2-4b22-93ba-3105b0e27089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00a3fa79-8b92-493c-926f-95d473cfc69c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb5311cc-6fca-4419-a006-76502acafb7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00a3fa79-8b92-493c-926f-95d473cfc69c",
                    "LayerId": "29bd321d-dc42-4a68-9eda-dbd684cd6b0b"
                }
            ]
        },
        {
            "id": "ccfae920-22fa-4a02-bce0-37ea656bc650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1470b819-1795-4256-a239-9cae84d06668",
            "compositeImage": {
                "id": "f414e4d4-c8b6-45d0-8fc9-7dddedb432e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccfae920-22fa-4a02-bce0-37ea656bc650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2972797-9fde-4d10-874e-427a8a18b1e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccfae920-22fa-4a02-bce0-37ea656bc650",
                    "LayerId": "29bd321d-dc42-4a68-9eda-dbd684cd6b0b"
                }
            ]
        },
        {
            "id": "8462801c-31cb-4a52-a975-47bed29a48a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1470b819-1795-4256-a239-9cae84d06668",
            "compositeImage": {
                "id": "df248164-45d2-4ea1-9b43-2a2cd87c6ace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8462801c-31cb-4a52-a975-47bed29a48a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae35f517-c930-4271-810e-f34d91eed870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8462801c-31cb-4a52-a975-47bed29a48a5",
                    "LayerId": "29bd321d-dc42-4a68-9eda-dbd684cd6b0b"
                }
            ]
        },
        {
            "id": "033cda2d-ee00-4164-a8c4-f4b71c26d727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1470b819-1795-4256-a239-9cae84d06668",
            "compositeImage": {
                "id": "f94bdc2a-0046-4cf5-8b07-c0918e589369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "033cda2d-ee00-4164-a8c4-f4b71c26d727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1131f731-a165-4302-95f8-ad8754e68287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033cda2d-ee00-4164-a8c4-f4b71c26d727",
                    "LayerId": "29bd321d-dc42-4a68-9eda-dbd684cd6b0b"
                }
            ]
        },
        {
            "id": "e5be70d5-f280-44d1-9247-6eed07432331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1470b819-1795-4256-a239-9cae84d06668",
            "compositeImage": {
                "id": "00adeb4b-390d-4d00-be16-a14d9c2b2bf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5be70d5-f280-44d1-9247-6eed07432331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1130cec1-c1d3-4faa-8924-d3008d3ade39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5be70d5-f280-44d1-9247-6eed07432331",
                    "LayerId": "29bd321d-dc42-4a68-9eda-dbd684cd6b0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "29bd321d-dc42-4a68-9eda-dbd684cd6b0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1470b819-1795-4256-a239-9cae84d06668",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 16
}