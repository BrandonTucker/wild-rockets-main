{
    "id": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage19buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ef1d69a-d100-484b-87b2-5e997a46b9b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "compositeImage": {
                "id": "1966ad32-248c-48f6-85f3-0c83e06c51ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ef1d69a-d100-484b-87b2-5e997a46b9b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15546798-26bc-4189-8215-fb70622ea699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef1d69a-d100-484b-87b2-5e997a46b9b0",
                    "LayerId": "7db3791b-9f8e-4960-83ec-39966633cf4c"
                }
            ]
        },
        {
            "id": "29e00882-7bca-4156-9ed6-83b73c6a3666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "compositeImage": {
                "id": "aa16752c-c87c-44c2-97eb-a6d6032c9c14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29e00882-7bca-4156-9ed6-83b73c6a3666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a204123-3e49-480c-a255-4d8f9b5d1518",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29e00882-7bca-4156-9ed6-83b73c6a3666",
                    "LayerId": "7db3791b-9f8e-4960-83ec-39966633cf4c"
                }
            ]
        },
        {
            "id": "7dff1ed6-b810-4428-ae96-218c36b5b254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "compositeImage": {
                "id": "78ef3360-2f81-42a6-b564-9d883c138a3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dff1ed6-b810-4428-ae96-218c36b5b254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6c16fc0-c5a6-4795-a127-bda714e751e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dff1ed6-b810-4428-ae96-218c36b5b254",
                    "LayerId": "7db3791b-9f8e-4960-83ec-39966633cf4c"
                }
            ]
        },
        {
            "id": "7ceb12f9-7186-418a-9e25-df60d02a4f21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "compositeImage": {
                "id": "ac09a547-f674-4abc-9ffa-a6bd7f982d6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ceb12f9-7186-418a-9e25-df60d02a4f21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e1ecd31-a3af-480f-b376-7b47dafe076b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ceb12f9-7186-418a-9e25-df60d02a4f21",
                    "LayerId": "7db3791b-9f8e-4960-83ec-39966633cf4c"
                }
            ]
        },
        {
            "id": "ebfac5c0-cb3f-4549-bc65-cd75a99981e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "compositeImage": {
                "id": "71b08f98-2a24-4557-b01d-34738b516046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebfac5c0-cb3f-4549-bc65-cd75a99981e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c790d1a1-5f91-45a7-a265-f2a9bdd7a886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebfac5c0-cb3f-4549-bc65-cd75a99981e2",
                    "LayerId": "7db3791b-9f8e-4960-83ec-39966633cf4c"
                }
            ]
        },
        {
            "id": "2109b4fe-f581-42bd-a247-94bd8c8c8f87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "compositeImage": {
                "id": "a19bfa03-5f73-4674-aadf-566472d62594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2109b4fe-f581-42bd-a247-94bd8c8c8f87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71223215-b531-40de-aada-e3b3578b7c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2109b4fe-f581-42bd-a247-94bd8c8c8f87",
                    "LayerId": "7db3791b-9f8e-4960-83ec-39966633cf4c"
                }
            ]
        },
        {
            "id": "d5151e2c-114a-4aff-a54d-f45670407e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "compositeImage": {
                "id": "cbb8f471-c1f2-45b8-92f4-d62a283866b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5151e2c-114a-4aff-a54d-f45670407e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1daaf459-9e95-40a0-a5db-48e4bc970b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5151e2c-114a-4aff-a54d-f45670407e0d",
                    "LayerId": "7db3791b-9f8e-4960-83ec-39966633cf4c"
                }
            ]
        },
        {
            "id": "98c2accc-e071-4d0c-8620-4a8fb35aa003",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "compositeImage": {
                "id": "4c2b2e6f-8f84-4fef-ac0a-41dca28f7dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c2accc-e071-4d0c-8620-4a8fb35aa003",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01cb0de6-f621-44f2-bd4a-9e510029c104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c2accc-e071-4d0c-8620-4a8fb35aa003",
                    "LayerId": "7db3791b-9f8e-4960-83ec-39966633cf4c"
                }
            ]
        },
        {
            "id": "9f8817e4-9ded-4cf8-b03a-5392d4dcb2bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "compositeImage": {
                "id": "40b516f0-098f-437d-89b2-0c6a944d8892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f8817e4-9ded-4cf8-b03a-5392d4dcb2bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "457a5917-5e82-4009-b8ca-cd3a52fce5e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f8817e4-9ded-4cf8-b03a-5392d4dcb2bd",
                    "LayerId": "7db3791b-9f8e-4960-83ec-39966633cf4c"
                }
            ]
        },
        {
            "id": "36d2d942-581d-4d52-b5de-7eea4c86ed22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "compositeImage": {
                "id": "9d56bc46-13fa-4b4f-82a5-394d6aa79093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36d2d942-581d-4d52-b5de-7eea4c86ed22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58b0e336-c340-41cb-9990-3bd802774974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36d2d942-581d-4d52-b5de-7eea4c86ed22",
                    "LayerId": "7db3791b-9f8e-4960-83ec-39966633cf4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7db3791b-9f8e-4960-83ec-39966633cf4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5b6436a-3084-4d8f-86f9-ab57b43fc2f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}