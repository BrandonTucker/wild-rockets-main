{
    "id": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange31buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc628726-078c-4250-8e4f-b870c60d61eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "compositeImage": {
                "id": "a702b89f-2236-49be-b4d5-fe9400b6eb47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc628726-078c-4250-8e4f-b870c60d61eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b07a55e-b1ab-4b63-b9d9-8bf098626f92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc628726-078c-4250-8e4f-b870c60d61eb",
                    "LayerId": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4"
                }
            ]
        },
        {
            "id": "226cfbd2-0a0a-4f90-a5fb-0ed948afc298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "compositeImage": {
                "id": "118819aa-6286-402f-945f-5de9dae5cdda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "226cfbd2-0a0a-4f90-a5fb-0ed948afc298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde01e4e-c70c-43e0-9224-b08cf8c05366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "226cfbd2-0a0a-4f90-a5fb-0ed948afc298",
                    "LayerId": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4"
                }
            ]
        },
        {
            "id": "a0bf8cb1-bb2a-4f64-b2da-044c2e69a47e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "compositeImage": {
                "id": "d662a7f5-bd1f-4db1-9b5d-74ffac613d77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0bf8cb1-bb2a-4f64-b2da-044c2e69a47e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d1ab338-a388-4ec7-a804-db3e04393805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0bf8cb1-bb2a-4f64-b2da-044c2e69a47e",
                    "LayerId": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4"
                }
            ]
        },
        {
            "id": "1310a96b-70a6-46d6-b622-f90727cb6e36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "compositeImage": {
                "id": "d3e47912-901c-41f3-ba84-7e96a9c029a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1310a96b-70a6-46d6-b622-f90727cb6e36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60c372a3-6236-4785-87c0-f82847fd4db8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1310a96b-70a6-46d6-b622-f90727cb6e36",
                    "LayerId": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4"
                }
            ]
        },
        {
            "id": "982897fb-5ba1-4813-a98a-8addc23f5ad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "compositeImage": {
                "id": "084b581e-5c96-4675-ac43-795b359a5ec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "982897fb-5ba1-4813-a98a-8addc23f5ad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9af8c6b1-f393-4008-b6cd-153bd6d071bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "982897fb-5ba1-4813-a98a-8addc23f5ad5",
                    "LayerId": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4"
                }
            ]
        },
        {
            "id": "fc180976-9ff2-42bc-89a8-13423944ed3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "compositeImage": {
                "id": "b8f5781e-d1eb-48c8-b658-a4084c46d99e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc180976-9ff2-42bc-89a8-13423944ed3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5cb396f-9d98-450f-aa96-43975dd4148e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc180976-9ff2-42bc-89a8-13423944ed3e",
                    "LayerId": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4"
                }
            ]
        },
        {
            "id": "2c407aff-6eba-4087-8f2c-8e009261bfc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "compositeImage": {
                "id": "a5655960-d8c5-48d3-8f21-7ee1ef419ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c407aff-6eba-4087-8f2c-8e009261bfc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b32596f5-1758-4e60-b542-0fe181295dbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c407aff-6eba-4087-8f2c-8e009261bfc2",
                    "LayerId": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4"
                }
            ]
        },
        {
            "id": "bca2fe26-6d6b-4ef4-bd32-b4dcddb58159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "compositeImage": {
                "id": "2b46911b-dc70-4c0e-b513-ad2c4a4952d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca2fe26-6d6b-4ef4-bd32-b4dcddb58159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2266acd-cc3c-410d-bc72-22f59e2d9b98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca2fe26-6d6b-4ef4-bd32-b4dcddb58159",
                    "LayerId": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4"
                }
            ]
        },
        {
            "id": "f93dc794-6518-4f73-a8a0-18fd9555297d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "compositeImage": {
                "id": "74f1a48f-6cd6-4318-b355-66d3621b2ac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f93dc794-6518-4f73-a8a0-18fd9555297d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af6d1491-fa22-4bbb-9b7b-d91b3d2e4c75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f93dc794-6518-4f73-a8a0-18fd9555297d",
                    "LayerId": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4"
                }
            ]
        },
        {
            "id": "0ebbe878-6540-4718-8c30-ccf1ead556ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "compositeImage": {
                "id": "09206fee-59d9-4693-bdbd-3db0d056fdb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ebbe878-6540-4718-8c30-ccf1ead556ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fabda2cd-6a29-4c85-96bc-64fcf0a8ea12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ebbe878-6540-4718-8c30-ccf1ead556ab",
                    "LayerId": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "db33ddf2-7b64-441a-b61a-2b5d8b5ff9c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f45e5c56-0cf0-407a-8fc0-f65ea67c1488",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}