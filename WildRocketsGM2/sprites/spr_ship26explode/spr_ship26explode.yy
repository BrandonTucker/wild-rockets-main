{
    "id": "57073557-d2f7-4f0f-96e5-6ea10ea91774",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship26explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8eef651-51c5-4199-87b1-e06b05a2c0d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57073557-d2f7-4f0f-96e5-6ea10ea91774",
            "compositeImage": {
                "id": "62f9ba05-8263-4953-aa44-9ae290935ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8eef651-51c5-4199-87b1-e06b05a2c0d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b71b92df-c7b4-4893-9608-164f2862698d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8eef651-51c5-4199-87b1-e06b05a2c0d0",
                    "LayerId": "6491994b-bf5e-46ef-85d5-2cc9cd7d2028"
                }
            ]
        },
        {
            "id": "c28c7423-c794-4362-a522-bb66b896abe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57073557-d2f7-4f0f-96e5-6ea10ea91774",
            "compositeImage": {
                "id": "0ccc8e84-2f0d-4fb2-9ce3-1fd9191d1950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c28c7423-c794-4362-a522-bb66b896abe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b28c5624-724b-4033-a34a-e638cfa75eca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c28c7423-c794-4362-a522-bb66b896abe7",
                    "LayerId": "6491994b-bf5e-46ef-85d5-2cc9cd7d2028"
                }
            ]
        },
        {
            "id": "e80caf9e-7be4-4d13-9587-d9e68f801d92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57073557-d2f7-4f0f-96e5-6ea10ea91774",
            "compositeImage": {
                "id": "18fe6b8e-dc94-4a34-90e6-030bceb36254",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e80caf9e-7be4-4d13-9587-d9e68f801d92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12a8d1ce-c4f4-4057-88d6-582cba5774fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e80caf9e-7be4-4d13-9587-d9e68f801d92",
                    "LayerId": "6491994b-bf5e-46ef-85d5-2cc9cd7d2028"
                }
            ]
        },
        {
            "id": "42eee96e-57bd-4119-9b53-b93467156d38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57073557-d2f7-4f0f-96e5-6ea10ea91774",
            "compositeImage": {
                "id": "63fc11da-c112-493c-b7c4-dfd3c22dcfe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42eee96e-57bd-4119-9b53-b93467156d38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ef832e-6a81-426b-b28e-b40b0df42e0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42eee96e-57bd-4119-9b53-b93467156d38",
                    "LayerId": "6491994b-bf5e-46ef-85d5-2cc9cd7d2028"
                }
            ]
        },
        {
            "id": "ba1a6c6d-08fc-4de4-9d77-d69aef16affd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57073557-d2f7-4f0f-96e5-6ea10ea91774",
            "compositeImage": {
                "id": "7f5f1434-ef6f-45e3-9da3-a7647cdd900d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba1a6c6d-08fc-4de4-9d77-d69aef16affd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "920d492e-2a09-4bb3-be21-4e5986120cbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba1a6c6d-08fc-4de4-9d77-d69aef16affd",
                    "LayerId": "6491994b-bf5e-46ef-85d5-2cc9cd7d2028"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "6491994b-bf5e-46ef-85d5-2cc9cd7d2028",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57073557-d2f7-4f0f-96e5-6ea10ea91774",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 145,
    "xorig": 73,
    "yorig": 82
}