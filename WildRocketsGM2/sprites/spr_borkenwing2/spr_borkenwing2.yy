{
    "id": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_borkenwing2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 1,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a75c954-f996-4c28-a20d-9e12bd19ba75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "030192fc-1999-4c5a-9068-eb8a9ce635fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a75c954-f996-4c28-a20d-9e12bd19ba75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5a91b30-0d83-4bc0-9ec0-5086bc6288fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a75c954-f996-4c28-a20d-9e12bd19ba75",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "447486de-ad8a-4622-bc30-f56aeb8c6727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "60a219e8-02ac-429b-ae61-b6114f20b15a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "447486de-ad8a-4622-bc30-f56aeb8c6727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b5893a5-d79b-4e24-9fe0-79c5318c1cd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "447486de-ad8a-4622-bc30-f56aeb8c6727",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "87db0466-7d44-4066-afee-5cd42982e30c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "c07cc7d7-1f54-43d8-85df-ad050e030d6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87db0466-7d44-4066-afee-5cd42982e30c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b70b7bb-68ee-472b-b69a-387cddfc7da6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87db0466-7d44-4066-afee-5cd42982e30c",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "3f44077a-c801-48d6-b597-f91b313ecdee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "cbf8c037-269f-45dc-adbc-35d1ded6f261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f44077a-c801-48d6-b597-f91b313ecdee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83d2aad8-8fde-46f8-be49-d20e92ba2b88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f44077a-c801-48d6-b597-f91b313ecdee",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "6a7b7854-acf3-4fb1-bd0b-33b7ce481218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "9dfe0e3f-3e6a-4251-aa39-c8565de29c1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a7b7854-acf3-4fb1-bd0b-33b7ce481218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7afce9e1-802b-4199-bb7d-b971079c4692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a7b7854-acf3-4fb1-bd0b-33b7ce481218",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "07836a4e-dfb0-4655-9d99-0fe0a7383fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "4b448157-bc19-44f2-bb62-e5c1ca890a2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07836a4e-dfb0-4655-9d99-0fe0a7383fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "945378dc-dc5b-458d-8bbd-d8dc4b7eefd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07836a4e-dfb0-4655-9d99-0fe0a7383fe1",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "f9f2a2a7-f4e5-49d8-ace8-c490a1e0c0ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "8b1de19b-854e-4f72-ae7d-348bd91c5e24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9f2a2a7-f4e5-49d8-ace8-c490a1e0c0ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09182beb-ebd3-43b5-ad85-572bfd84d67a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9f2a2a7-f4e5-49d8-ace8-c490a1e0c0ac",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "a4955423-c330-4195-8015-1b56f751285a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "4b0ecb45-3b78-45b0-8979-ea70ceebff1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4955423-c330-4195-8015-1b56f751285a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "187a6ce1-2f2d-43b0-ac64-8ad25f3d0fb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4955423-c330-4195-8015-1b56f751285a",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "e2d74212-91d2-4710-9e2f-636ed2db04f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "ac22986f-3040-412e-893c-6b1581742cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d74212-91d2-4710-9e2f-636ed2db04f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b757a3f1-705c-416f-a0f3-1bb92b9e79a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d74212-91d2-4710-9e2f-636ed2db04f7",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "722fc6ed-1731-49ab-a84f-5d33d33d16af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "538d3f8a-f2db-4301-a945-a3b1ea062621",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "722fc6ed-1731-49ab-a84f-5d33d33d16af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e000ce7e-4f63-4ae1-ab30-0cfd99af2aee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "722fc6ed-1731-49ab-a84f-5d33d33d16af",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "fe4afa1f-dc6e-4af5-a3f7-63efab3efc5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "79f48553-cc43-47e2-bb00-5bcd2d730cf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe4afa1f-dc6e-4af5-a3f7-63efab3efc5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d619b8-e08b-4361-b205-06689391e26e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4afa1f-dc6e-4af5-a3f7-63efab3efc5a",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "73c27dbc-b51a-4092-bede-3f4e03f2a6c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "918e8571-6167-4495-9cce-48f9faae309b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73c27dbc-b51a-4092-bede-3f4e03f2a6c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "607ec250-e342-4917-a5e4-d71cd737a0c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73c27dbc-b51a-4092-bede-3f4e03f2a6c0",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "1c2c4b17-febb-467d-a3df-b6144efece16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "ca6fa96d-71e6-4d3b-9cc7-eb8ab50af0dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c2c4b17-febb-467d-a3df-b6144efece16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0dc53b0-fc6e-465e-9ec6-a3caa713673c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c2c4b17-febb-467d-a3df-b6144efece16",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "b68b4fed-ca8b-47a6-ab7e-2fa2a2f4ff83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "a76732dd-ef03-4336-a9c8-fa4e2dd28155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68b4fed-ca8b-47a6-ab7e-2fa2a2f4ff83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2b49b44-5e28-4d43-84f1-3f2259fcf311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68b4fed-ca8b-47a6-ab7e-2fa2a2f4ff83",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        },
        {
            "id": "bec0c8ad-7a06-4db0-9aed-8f235d7af34c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "compositeImage": {
                "id": "c0c8b361-8ffb-4222-a1a4-a268187ffa16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bec0c8ad-7a06-4db0-9aed-8f235d7af34c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bbbaba0-3d1c-4cba-8485-3d405b537907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bec0c8ad-7a06-4db0-9aed-8f235d7af34c",
                    "LayerId": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "ee2175fa-aab2-4d60-aed8-ee4f9bb862bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e5de1cd-091d-4183-8d29-9be8c8c9418b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 59,
    "xorig": -9,
    "yorig": -22
}