{
    "id": "d9c07256-e057-4573-b06e-a15db8aa9431",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rareship2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 23,
    "bbox_right": 79,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4fc16fa-e0ba-47d6-94a2-78edf9ecf090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "compositeImage": {
                "id": "23f46925-f82c-4699-a30b-a965bc5e843a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4fc16fa-e0ba-47d6-94a2-78edf9ecf090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a690265-2cc5-4a95-bb87-fa289cc9f7af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4fc16fa-e0ba-47d6-94a2-78edf9ecf090",
                    "LayerId": "767198b1-a267-48a5-8898-02da55395c33"
                }
            ]
        },
        {
            "id": "a2136245-684b-4ce9-9ba5-ddddcda68782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "compositeImage": {
                "id": "3ec43677-b66c-4000-8f6e-ec63bb9e2260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2136245-684b-4ce9-9ba5-ddddcda68782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7d6f299-4231-4046-a152-63bdca656838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2136245-684b-4ce9-9ba5-ddddcda68782",
                    "LayerId": "767198b1-a267-48a5-8898-02da55395c33"
                }
            ]
        },
        {
            "id": "abc9ae6c-aa57-437c-827f-9751ecae5436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "compositeImage": {
                "id": "3551c4b1-15cc-4501-a3ce-e7f31dc08176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abc9ae6c-aa57-437c-827f-9751ecae5436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2f2067d-c0e7-4585-8fd0-112a972bf6a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abc9ae6c-aa57-437c-827f-9751ecae5436",
                    "LayerId": "767198b1-a267-48a5-8898-02da55395c33"
                }
            ]
        },
        {
            "id": "1fbcce7e-f97a-4ed4-8732-0ae4c2a108d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "compositeImage": {
                "id": "a77485ea-8e0d-4f91-94f8-feb8e7df7a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fbcce7e-f97a-4ed4-8732-0ae4c2a108d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd5c4657-d211-4b8f-a0c2-eea78c35f045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fbcce7e-f97a-4ed4-8732-0ae4c2a108d8",
                    "LayerId": "767198b1-a267-48a5-8898-02da55395c33"
                }
            ]
        },
        {
            "id": "99ba54a0-48f7-4b10-84d4-9bcb08508272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "compositeImage": {
                "id": "0f948a87-ad76-476b-9f83-e8426d36a0ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99ba54a0-48f7-4b10-84d4-9bcb08508272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc96db7c-ef16-4a04-9be5-e02edf990926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ba54a0-48f7-4b10-84d4-9bcb08508272",
                    "LayerId": "767198b1-a267-48a5-8898-02da55395c33"
                }
            ]
        },
        {
            "id": "9aa3b0d4-aef7-4d41-91f9-9e9d2950dd3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "compositeImage": {
                "id": "286c711f-d414-4c28-b874-b8e076689035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa3b0d4-aef7-4d41-91f9-9e9d2950dd3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a7c2c20-2a6b-4e14-a759-2388056caf4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa3b0d4-aef7-4d41-91f9-9e9d2950dd3c",
                    "LayerId": "767198b1-a267-48a5-8898-02da55395c33"
                }
            ]
        },
        {
            "id": "935d1454-099e-4e23-a1aa-2a51e2d2423d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "compositeImage": {
                "id": "aceae3de-e9d1-4c6d-865e-a1c641cf552d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "935d1454-099e-4e23-a1aa-2a51e2d2423d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b563037-d240-4a5d-8846-162e2d8ecf74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "935d1454-099e-4e23-a1aa-2a51e2d2423d",
                    "LayerId": "767198b1-a267-48a5-8898-02da55395c33"
                }
            ]
        },
        {
            "id": "b4512e23-eecd-4bf4-8bb8-77ec7c560277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "compositeImage": {
                "id": "235feb1b-ce23-47d3-a12e-697d14b71889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4512e23-eecd-4bf4-8bb8-77ec7c560277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb08a9d9-76c2-46c3-bd4c-1221a761ca1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4512e23-eecd-4bf4-8bb8-77ec7c560277",
                    "LayerId": "767198b1-a267-48a5-8898-02da55395c33"
                }
            ]
        },
        {
            "id": "5dd6163c-d367-4a43-b5ae-e4cbd555c5e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "compositeImage": {
                "id": "c898ab99-5b70-4eaa-ab83-1f35715a2ffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd6163c-d367-4a43-b5ae-e4cbd555c5e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "023f7e59-6472-46af-87fa-f0b945bb32aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd6163c-d367-4a43-b5ae-e4cbd555c5e4",
                    "LayerId": "767198b1-a267-48a5-8898-02da55395c33"
                }
            ]
        },
        {
            "id": "f5e891d7-24a1-45e3-ba83-52e7b3d546f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "compositeImage": {
                "id": "d1d9cdad-e247-4db9-af3e-e54e969030b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5e891d7-24a1-45e3-ba83-52e7b3d546f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f35e136-2342-4533-bccb-c1d6c7e28f89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5e891d7-24a1-45e3-ba83-52e7b3d546f3",
                    "LayerId": "767198b1-a267-48a5-8898-02da55395c33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "767198b1-a267-48a5-8898-02da55395c33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9c07256-e057-4573-b06e-a15db8aa9431",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 60
}