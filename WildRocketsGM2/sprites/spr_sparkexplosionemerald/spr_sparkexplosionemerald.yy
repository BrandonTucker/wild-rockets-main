{
    "id": "5aca8933-e4fd-456c-b437-6f311388a154",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sparkexplosionemerald",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 2,
    "bbox_right": 146,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bdb2cc4-27ec-4ea9-8a2f-55eb9a5314be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aca8933-e4fd-456c-b437-6f311388a154",
            "compositeImage": {
                "id": "3c683059-83eb-4ee0-a3c6-1fb9e0f87c30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bdb2cc4-27ec-4ea9-8a2f-55eb9a5314be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8135f770-ef4c-4642-96cd-dece94862217",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bdb2cc4-27ec-4ea9-8a2f-55eb9a5314be",
                    "LayerId": "c3969ba7-2db3-4537-ac5c-abce18ce6445"
                }
            ]
        },
        {
            "id": "52848a99-5c37-4ec0-8167-42c2338506db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aca8933-e4fd-456c-b437-6f311388a154",
            "compositeImage": {
                "id": "c87af18f-3f72-4ba6-aaf9-0f6747692425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52848a99-5c37-4ec0-8167-42c2338506db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cacbb34-a1ae-425b-89d4-073ff04f09a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52848a99-5c37-4ec0-8167-42c2338506db",
                    "LayerId": "c3969ba7-2db3-4537-ac5c-abce18ce6445"
                }
            ]
        },
        {
            "id": "d318fd44-f916-4ddb-865c-329c15ff3acf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aca8933-e4fd-456c-b437-6f311388a154",
            "compositeImage": {
                "id": "3c2bd216-6e6c-4f43-a9e0-a1b8850b0b3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d318fd44-f916-4ddb-865c-329c15ff3acf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eebcb49-d4b7-40ec-88ee-a4ef67618c72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d318fd44-f916-4ddb-865c-329c15ff3acf",
                    "LayerId": "c3969ba7-2db3-4537-ac5c-abce18ce6445"
                }
            ]
        },
        {
            "id": "f20ebd74-b807-482b-b12c-e99c5f81564a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aca8933-e4fd-456c-b437-6f311388a154",
            "compositeImage": {
                "id": "57ba66c3-eb26-438f-aacf-172c8f7e231a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f20ebd74-b807-482b-b12c-e99c5f81564a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "776c32ed-268e-4e5c-bdf3-9d2d7092ef17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f20ebd74-b807-482b-b12c-e99c5f81564a",
                    "LayerId": "c3969ba7-2db3-4537-ac5c-abce18ce6445"
                }
            ]
        },
        {
            "id": "5b622b30-8c89-4400-b065-860ead989421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aca8933-e4fd-456c-b437-6f311388a154",
            "compositeImage": {
                "id": "9eb5c08d-108d-4b00-9afc-3969f948f21d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b622b30-8c89-4400-b065-860ead989421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "378e8cb5-22a5-4de2-8e03-60da4665107e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b622b30-8c89-4400-b065-860ead989421",
                    "LayerId": "c3969ba7-2db3-4537-ac5c-abce18ce6445"
                }
            ]
        },
        {
            "id": "be7e7804-6818-4a64-b560-e1dbee0a6e35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aca8933-e4fd-456c-b437-6f311388a154",
            "compositeImage": {
                "id": "5f5c69b7-44f2-47ac-af3f-733fe7273b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be7e7804-6818-4a64-b560-e1dbee0a6e35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbff0570-780d-43e9-b080-a987abffa972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be7e7804-6818-4a64-b560-e1dbee0a6e35",
                    "LayerId": "c3969ba7-2db3-4537-ac5c-abce18ce6445"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "c3969ba7-2db3-4537-ac5c-abce18ce6445",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5aca8933-e4fd-456c-b437-6f311388a154",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 148,
    "xorig": 74,
    "yorig": 74
}