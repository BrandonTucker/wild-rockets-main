{
    "id": "2a502464-c915-4218-a2a4-2d6862f9a4c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship12button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 11,
    "bbox_right": 89,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2813f0e9-b1f4-44aa-a837-728f96ba1362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a502464-c915-4218-a2a4-2d6862f9a4c7",
            "compositeImage": {
                "id": "fa2a90d1-3d23-4c4d-93a9-16b8f76f1a7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2813f0e9-b1f4-44aa-a837-728f96ba1362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b6cf4e-643e-4082-abba-92825fc75104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2813f0e9-b1f4-44aa-a837-728f96ba1362",
                    "LayerId": "6309c78a-d455-4101-a64b-7107a8eb262a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6309c78a-d455-4101-a64b-7107a8eb262a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a502464-c915-4218-a2a4-2d6862f9a4c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}