{
    "id": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smallwing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0247b018-63e2-4d0f-a35a-ad190368590e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "7bd92585-e5bc-450e-b629-c9756983a4a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0247b018-63e2-4d0f-a35a-ad190368590e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5b489cb-e5e7-462f-a611-5e8a8287c98a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0247b018-63e2-4d0f-a35a-ad190368590e",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "df8e1f46-2d11-4b08-8448-4eee80804e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "79f067a1-57cb-433a-8ddf-900cab6c8d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df8e1f46-2d11-4b08-8448-4eee80804e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6bf39d0-1310-4a63-b8da-f7f175880b82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df8e1f46-2d11-4b08-8448-4eee80804e16",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "b34e7e52-a0c5-4e7a-b28b-34a5657f46b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "6d55e453-e39b-4c93-9d79-8dfed69b8074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b34e7e52-a0c5-4e7a-b28b-34a5657f46b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76068613-06a2-4ae8-ad5a-6086c4de3911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b34e7e52-a0c5-4e7a-b28b-34a5657f46b1",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "0b04a5f8-c1ee-45c3-b75a-eb02279d9f1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "4b04b18c-bf02-4b40-81bc-ed411d0d602a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b04a5f8-c1ee-45c3-b75a-eb02279d9f1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a091edf1-7c79-48ea-a952-51332a735af5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b04a5f8-c1ee-45c3-b75a-eb02279d9f1b",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "2ce19641-aac0-4330-8aa4-3f6f91d2bf3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "48a7c969-21fd-4827-ab7e-ed69935c9485",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce19641-aac0-4330-8aa4-3f6f91d2bf3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0136c7e9-c62e-47d2-84bb-a05e636724ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce19641-aac0-4330-8aa4-3f6f91d2bf3a",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "dcdd138d-81a8-4b62-8602-03d3a33259ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "b4b414f8-f2bb-4599-abf7-c105179c3a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcdd138d-81a8-4b62-8602-03d3a33259ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7960a993-c028-4491-85e1-fe1761497138",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcdd138d-81a8-4b62-8602-03d3a33259ed",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "9381b954-37d6-49b6-9126-a04b4f942602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "504676b5-b288-4f7b-a799-502e8dc1be32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9381b954-37d6-49b6-9126-a04b4f942602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b87d8ef9-7f01-48d4-a037-1cbbcd5e45b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9381b954-37d6-49b6-9126-a04b4f942602",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "260acf26-2676-4371-ae08-ed4e462f532f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "07d6c167-96ca-40fe-b388-f369567016fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "260acf26-2676-4371-ae08-ed4e462f532f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "357faf42-7f00-4c7e-b18a-b98f0db93b26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "260acf26-2676-4371-ae08-ed4e462f532f",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "2e131449-0376-495e-af5b-a94ee7d46d0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "bd26a844-24c6-4d0d-b5a9-f643115e5a87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e131449-0376-495e-af5b-a94ee7d46d0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b0f5c9-3e64-4225-8a69-33a4b6368779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e131449-0376-495e-af5b-a94ee7d46d0a",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "39953cff-978e-46da-8b91-13c0d3feca1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "9fb4a500-aada-46cb-9aac-4258623a2bdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39953cff-978e-46da-8b91-13c0d3feca1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e56da22-43c0-4cda-8d85-0e0a306c07a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39953cff-978e-46da-8b91-13c0d3feca1d",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "189aa4e3-0001-4b15-9475-d88c40b4a337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "3a2800e9-38cf-467d-94aa-eaff2c618bc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "189aa4e3-0001-4b15-9475-d88c40b4a337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4696f6c7-f286-43a2-bcda-2ab8d1b6a43c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189aa4e3-0001-4b15-9475-d88c40b4a337",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "9c4e98ca-fa34-477e-a760-ba8b2508241f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "66dafa7d-6c0d-4d34-81f4-b68d65bee3f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c4e98ca-fa34-477e-a760-ba8b2508241f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "830aec00-b7cc-438a-9c93-bf272ba71c31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c4e98ca-fa34-477e-a760-ba8b2508241f",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "5d001876-7f0c-466c-a018-2ce99004c35f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "5d706719-6c2f-4c0e-81f5-6fc8723495bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d001876-7f0c-466c-a018-2ce99004c35f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3445397-9f2d-462c-8e37-79fda5a5585d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d001876-7f0c-466c-a018-2ce99004c35f",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "4d3f6348-faf6-42f0-80b5-8c521a6c48e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "53d87af4-52be-4d2a-a13c-0eeca0a9427b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d3f6348-faf6-42f0-80b5-8c521a6c48e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "def658a7-9a7b-47f6-b6d2-74e0d51a38cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d3f6348-faf6-42f0-80b5-8c521a6c48e9",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        },
        {
            "id": "d13ae769-f1d6-459d-af0b-5da640ff544c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "compositeImage": {
                "id": "969e2f18-420d-453b-ad65-6cd75102bf84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d13ae769-f1d6-459d-af0b-5da640ff544c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a74b71-0f4e-4cbc-a7d4-a11dc0484571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d13ae769-f1d6-459d-af0b-5da640ff544c",
                    "LayerId": "d9ded7fd-33cb-493f-af27-67fb8edb3f54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "d9ded7fd-33cb-493f-af27-67fb8edb3f54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a8c57b0-ac9c-43b7-a151-186b9dea7844",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 21,
    "yorig": -15
}