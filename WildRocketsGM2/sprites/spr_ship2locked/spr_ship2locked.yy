{
    "id": "1b3076f1-8590-4cf5-802a-e21d8cef52e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship2locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15403fd0-d3ed-4409-b7a1-d3b17b0c33a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b3076f1-8590-4cf5-802a-e21d8cef52e6",
            "compositeImage": {
                "id": "be625425-dc9f-4c8a-b6bc-a5392b99bced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15403fd0-d3ed-4409-b7a1-d3b17b0c33a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "162ed523-32ac-48b3-ae6a-89956ce32aa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15403fd0-d3ed-4409-b7a1-d3b17b0c33a6",
                    "LayerId": "beb1aa78-4389-47c3-b700-0c8a72644640"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "beb1aa78-4389-47c3-b700-0c8a72644640",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b3076f1-8590-4cf5-802a-e21d8cef52e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}