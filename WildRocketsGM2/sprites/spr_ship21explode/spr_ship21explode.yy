{
    "id": "ba974dfb-4f38-42e9-b9bf-91b45f6bda5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship21explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 25,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bea8d29-c715-496d-8657-21d614692459",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba974dfb-4f38-42e9-b9bf-91b45f6bda5b",
            "compositeImage": {
                "id": "97647516-e394-4eba-a837-fe4a9fd8034d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bea8d29-c715-496d-8657-21d614692459",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c6f19af-5180-48ad-aa30-8f5d948156dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bea8d29-c715-496d-8657-21d614692459",
                    "LayerId": "ac6c1c9d-a1d9-4ad5-9887-718f5f1fc040"
                }
            ]
        },
        {
            "id": "3663fb1a-6449-447f-b015-4a1e8da7f834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba974dfb-4f38-42e9-b9bf-91b45f6bda5b",
            "compositeImage": {
                "id": "ba163906-0c63-415c-b294-65426f0b415b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3663fb1a-6449-447f-b015-4a1e8da7f834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c76695ed-0c6d-425e-ac6e-040e62a8e55e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3663fb1a-6449-447f-b015-4a1e8da7f834",
                    "LayerId": "ac6c1c9d-a1d9-4ad5-9887-718f5f1fc040"
                }
            ]
        },
        {
            "id": "bcfeddc5-5c6a-4c7e-b771-d5dafd47c73d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba974dfb-4f38-42e9-b9bf-91b45f6bda5b",
            "compositeImage": {
                "id": "179cd002-5012-4290-a796-f5f473f6f2ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcfeddc5-5c6a-4c7e-b771-d5dafd47c73d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a563297-becd-40d7-af5b-fecc02276773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcfeddc5-5c6a-4c7e-b771-d5dafd47c73d",
                    "LayerId": "ac6c1c9d-a1d9-4ad5-9887-718f5f1fc040"
                }
            ]
        },
        {
            "id": "f4d6736f-9147-4856-ac58-d95153714dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba974dfb-4f38-42e9-b9bf-91b45f6bda5b",
            "compositeImage": {
                "id": "9dfa561c-62d3-416c-994c-304e03567dca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4d6736f-9147-4856-ac58-d95153714dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02037bff-6981-496c-8d82-4f13149657d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4d6736f-9147-4856-ac58-d95153714dfe",
                    "LayerId": "ac6c1c9d-a1d9-4ad5-9887-718f5f1fc040"
                }
            ]
        },
        {
            "id": "553b87c8-3ce0-4bf9-a5f0-8b5b0b4f09ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba974dfb-4f38-42e9-b9bf-91b45f6bda5b",
            "compositeImage": {
                "id": "26dbe8c8-3bec-4097-8fd8-5f5d51417e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553b87c8-3ce0-4bf9-a5f0-8b5b0b4f09ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e4d8bfe-7cde-45b5-ae84-a5bf6f7e4248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553b87c8-3ce0-4bf9-a5f0-8b5b0b4f09ae",
                    "LayerId": "ac6c1c9d-a1d9-4ad5-9887-718f5f1fc040"
                }
            ]
        },
        {
            "id": "a8bde9d0-0730-4376-a98a-9e82626807f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba974dfb-4f38-42e9-b9bf-91b45f6bda5b",
            "compositeImage": {
                "id": "0d08d8f3-f174-4781-ba04-d68b934c3493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8bde9d0-0730-4376-a98a-9e82626807f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1a8bb07-6e29-41f7-b86f-c5e4ba626f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8bde9d0-0730-4376-a98a-9e82626807f3",
                    "LayerId": "ac6c1c9d-a1d9-4ad5-9887-718f5f1fc040"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "ac6c1c9d-a1d9-4ad5-9887-718f5f1fc040",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba974dfb-4f38-42e9-b9bf-91b45f6bda5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 49
}