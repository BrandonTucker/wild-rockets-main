{
    "id": "fd1b0ff7-6f93-459d-9f71-dd331ede0a50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship5locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "169d3a72-58d9-418d-be76-b3157a9a0ebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd1b0ff7-6f93-459d-9f71-dd331ede0a50",
            "compositeImage": {
                "id": "362b58e4-ae93-444e-98ae-8fccfc058d25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "169d3a72-58d9-418d-be76-b3157a9a0ebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef16e483-5cee-43a7-94a6-7d88206f5d5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "169d3a72-58d9-418d-be76-b3157a9a0ebb",
                    "LayerId": "2ad2a49b-800e-4445-8c9c-2d21abfe3b8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "2ad2a49b-800e-4445-8c9c-2d21abfe3b8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd1b0ff7-6f93-459d-9f71-dd331ede0a50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}