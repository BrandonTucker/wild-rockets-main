{
    "id": "2f18f58e-83ad-414a-9d84-896b6c4182f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background3",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 639,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d38da76d-bb59-462d-bde2-9db495ab5dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f18f58e-83ad-414a-9d84-896b6c4182f1",
            "compositeImage": {
                "id": "0e0cbfe1-4c7b-43e4-8bd1-9f7e5c95be2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d38da76d-bb59-462d-bde2-9db495ab5dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5a7ab08-a4b7-48bc-b5ce-a771776ac9d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d38da76d-bb59-462d-bde2-9db495ab5dad",
                    "LayerId": "ad57cf1a-54f3-45eb-bf75-92c6f6ef59c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "ad57cf1a-54f3-45eb-bf75-92c6f6ef59c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f18f58e-83ad-414a-9d84-896b6c4182f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}