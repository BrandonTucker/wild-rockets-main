{
    "id": "11afebba-d734-44ff-8d14-220fa2d57016",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage11button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1870082-ffc8-49b8-b699-97bafb6692c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11afebba-d734-44ff-8d14-220fa2d57016",
            "compositeImage": {
                "id": "0b466f51-fb4a-4d48-a946-569cd59b74cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1870082-ffc8-49b8-b699-97bafb6692c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97920f5d-ba12-4e3d-95fa-882f0b3f07c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1870082-ffc8-49b8-b699-97bafb6692c0",
                    "LayerId": "c3c50319-81c3-4f2f-af8d-4790c5404291"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c3c50319-81c3-4f2f-af8d-4790c5404291",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11afebba-d734-44ff-8d14-220fa2d57016",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}