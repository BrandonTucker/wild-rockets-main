{
    "id": "08623dac-3075-455c-8a85-daaab8950a63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange40button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88232a3f-1095-46cf-aaf9-13043049a5c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08623dac-3075-455c-8a85-daaab8950a63",
            "compositeImage": {
                "id": "6292150f-28be-43aa-8bdf-bbb472118e2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88232a3f-1095-46cf-aaf9-13043049a5c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd636073-14e9-4cb0-bd30-0cc294c185d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88232a3f-1095-46cf-aaf9-13043049a5c7",
                    "LayerId": "ef829aaf-65fe-4a04-ad37-bd83beb459e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "ef829aaf-65fe-4a04-ad37-bd83beb459e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08623dac-3075-455c-8a85-daaab8950a63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}