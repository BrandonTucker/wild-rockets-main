{
    "id": "8834f8a1-c187-4587-9ac6-f0c17084b387",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship34",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 153,
    "bbox_left": 51,
    "bbox_right": 76,
    "bbox_top": 61,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a4ff48d-2f3b-486a-a1cc-737d6c99e145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8834f8a1-c187-4587-9ac6-f0c17084b387",
            "compositeImage": {
                "id": "2ff5cc7f-dd4d-4322-a910-3c3375eda997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a4ff48d-2f3b-486a-a1cc-737d6c99e145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65860377-4539-4349-b0f9-211fd1512618",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a4ff48d-2f3b-486a-a1cc-737d6c99e145",
                    "LayerId": "de377f88-76be-481e-8740-d9c5871530da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 209,
    "layers": [
        {
            "id": "de377f88-76be-481e-8740-d9c5871530da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8834f8a1-c187-4587-9ac6-f0c17084b387",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 129,
    "xorig": 64,
    "yorig": 86
}