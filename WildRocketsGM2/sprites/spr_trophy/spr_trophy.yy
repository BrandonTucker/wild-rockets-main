{
    "id": "f270c441-51d5-43c2-9d92-f5508657f7ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trophy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c0bb9f3-9368-4685-8bfa-752db9f3aae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f270c441-51d5-43c2-9d92-f5508657f7ab",
            "compositeImage": {
                "id": "a693e771-b923-4f10-9dcd-88be56989c30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c0bb9f3-9368-4685-8bfa-752db9f3aae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c192087-cf83-4f9b-976f-8823a37a57ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c0bb9f3-9368-4685-8bfa-752db9f3aae3",
                    "LayerId": "accda34f-164e-49e1-aabb-1bd530040007"
                }
            ]
        },
        {
            "id": "f9fe9dca-b2dd-4d32-bdc7-1655d442f99d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f270c441-51d5-43c2-9d92-f5508657f7ab",
            "compositeImage": {
                "id": "5c336220-0512-4aff-9b2c-5c2b0e237aab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9fe9dca-b2dd-4d32-bdc7-1655d442f99d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0267d203-6749-499f-be99-edfcba840e2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9fe9dca-b2dd-4d32-bdc7-1655d442f99d",
                    "LayerId": "accda34f-164e-49e1-aabb-1bd530040007"
                }
            ]
        },
        {
            "id": "2017bfd4-36a8-43cc-acea-74cb9039210a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f270c441-51d5-43c2-9d92-f5508657f7ab",
            "compositeImage": {
                "id": "e0c0fed8-22fd-4a97-9ece-57cd1859f8ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2017bfd4-36a8-43cc-acea-74cb9039210a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb459e54-6ba1-447d-b9d7-81dc1806b72e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2017bfd4-36a8-43cc-acea-74cb9039210a",
                    "LayerId": "accda34f-164e-49e1-aabb-1bd530040007"
                }
            ]
        },
        {
            "id": "d9800240-f067-4025-bcb0-ac55835c5ff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f270c441-51d5-43c2-9d92-f5508657f7ab",
            "compositeImage": {
                "id": "47f3904f-2ebf-4a05-9498-ae0fcbbd1a73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9800240-f067-4025-bcb0-ac55835c5ff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33b1565e-b897-4984-b041-9ea07acc2341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9800240-f067-4025-bcb0-ac55835c5ff4",
                    "LayerId": "accda34f-164e-49e1-aabb-1bd530040007"
                }
            ]
        },
        {
            "id": "69e9218c-770c-4bca-91f3-42ac2a8c00c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f270c441-51d5-43c2-9d92-f5508657f7ab",
            "compositeImage": {
                "id": "a5f89dd4-1009-48bd-9783-2fd973f0007c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69e9218c-770c-4bca-91f3-42ac2a8c00c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cae8294-cec9-47cf-8d09-ce979814fcc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69e9218c-770c-4bca-91f3-42ac2a8c00c9",
                    "LayerId": "accda34f-164e-49e1-aabb-1bd530040007"
                }
            ]
        },
        {
            "id": "a6ffe083-2e9a-44fd-8113-2f0a928233d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f270c441-51d5-43c2-9d92-f5508657f7ab",
            "compositeImage": {
                "id": "44b11235-e5d4-455d-9389-1fc64e454678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6ffe083-2e9a-44fd-8113-2f0a928233d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be755f17-931d-4cf2-88aa-32edc479fa38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ffe083-2e9a-44fd-8113-2f0a928233d7",
                    "LayerId": "accda34f-164e-49e1-aabb-1bd530040007"
                }
            ]
        },
        {
            "id": "96c46347-75cb-420f-bebd-8a04ae120534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f270c441-51d5-43c2-9d92-f5508657f7ab",
            "compositeImage": {
                "id": "943baaf6-d913-4ea9-980b-adb801a7280e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96c46347-75cb-420f-bebd-8a04ae120534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aad5c959-83f7-4ecc-aab4-23bf0f0f5687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96c46347-75cb-420f-bebd-8a04ae120534",
                    "LayerId": "accda34f-164e-49e1-aabb-1bd530040007"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "accda34f-164e-49e1-aabb-1bd530040007",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f270c441-51d5-43c2-9d92-f5508657f7ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 13
}