{
    "id": "5242c6ca-9f84-4f80-9cfd-59b5530f34dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship31button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bc8aa8d-1c9f-4951-8fd3-9eed4896d938",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5242c6ca-9f84-4f80-9cfd-59b5530f34dd",
            "compositeImage": {
                "id": "916c1d23-60d4-460d-a143-fdd53fc7684b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bc8aa8d-1c9f-4951-8fd3-9eed4896d938",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c75398c3-32f4-4e2e-972a-c61508eeb7ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc8aa8d-1c9f-4951-8fd3-9eed4896d938",
                    "LayerId": "1532e65d-497d-4cd8-930d-1e2aab3a9000"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1532e65d-497d-4cd8-930d-1e2aab3a9000",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5242c6ca-9f84-4f80-9cfd-59b5530f34dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}