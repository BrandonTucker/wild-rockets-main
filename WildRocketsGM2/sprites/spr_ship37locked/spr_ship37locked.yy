{
    "id": "6adae943-5ccf-47b6-8ee5-3617c6dcd78c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship37locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1a5d058-3de8-4dd4-b34e-e6d761092980",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6adae943-5ccf-47b6-8ee5-3617c6dcd78c",
            "compositeImage": {
                "id": "aa28d6fd-a855-430c-b7d1-05840d6165fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1a5d058-3de8-4dd4-b34e-e6d761092980",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0bd9c6b-414a-439a-9e5d-cba21d75b705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1a5d058-3de8-4dd4-b34e-e6d761092980",
                    "LayerId": "14209460-e691-4f9b-aacb-8fd2cc39cc7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "14209460-e691-4f9b-aacb-8fd2cc39cc7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6adae943-5ccf-47b6-8ee5-3617c6dcd78c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}