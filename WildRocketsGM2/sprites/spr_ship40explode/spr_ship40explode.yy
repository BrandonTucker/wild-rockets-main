{
    "id": "2220c11e-f557-4c36-ada8-58314cda3573",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship40explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b093e4f5-700d-49e6-99c2-8ee11109cee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2220c11e-f557-4c36-ada8-58314cda3573",
            "compositeImage": {
                "id": "9712656e-29ab-4297-b013-99e32360f3f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b093e4f5-700d-49e6-99c2-8ee11109cee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17b138e2-e3a8-4ebc-bfab-2ff07b929f33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b093e4f5-700d-49e6-99c2-8ee11109cee7",
                    "LayerId": "945a657c-6aa4-436c-a5b9-2dbbdb250e9f"
                }
            ]
        },
        {
            "id": "48038837-c72a-48df-989f-090d807ac5ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2220c11e-f557-4c36-ada8-58314cda3573",
            "compositeImage": {
                "id": "8d1839c1-2fe7-4ea3-b0ec-c55c2deefe34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48038837-c72a-48df-989f-090d807ac5ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33c231a9-347e-4cbd-b135-848698b438c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48038837-c72a-48df-989f-090d807ac5ec",
                    "LayerId": "945a657c-6aa4-436c-a5b9-2dbbdb250e9f"
                }
            ]
        },
        {
            "id": "b37b7bb9-e9db-447f-8a8a-99c2a845fd00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2220c11e-f557-4c36-ada8-58314cda3573",
            "compositeImage": {
                "id": "d65afdf4-71d8-45f4-a605-e7dd9ee0cdab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b37b7bb9-e9db-447f-8a8a-99c2a845fd00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a3564e6-4b47-4d6c-9cbe-d5325d710fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b37b7bb9-e9db-447f-8a8a-99c2a845fd00",
                    "LayerId": "945a657c-6aa4-436c-a5b9-2dbbdb250e9f"
                }
            ]
        },
        {
            "id": "06191ee2-72fd-4c7c-9b59-0bb875d862c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2220c11e-f557-4c36-ada8-58314cda3573",
            "compositeImage": {
                "id": "60e5555b-88c3-4278-976c-bffaf0480f37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06191ee2-72fd-4c7c-9b59-0bb875d862c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc28fd37-46a4-4229-88e8-414109a306a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06191ee2-72fd-4c7c-9b59-0bb875d862c8",
                    "LayerId": "945a657c-6aa4-436c-a5b9-2dbbdb250e9f"
                }
            ]
        },
        {
            "id": "334d6860-e5bf-45e0-915f-af99eb2ec008",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2220c11e-f557-4c36-ada8-58314cda3573",
            "compositeImage": {
                "id": "9a3a8d74-54eb-464f-afd2-19fa59781c8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "334d6860-e5bf-45e0-915f-af99eb2ec008",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82c8dd8b-fc2e-4499-90d6-2a1dcad38ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "334d6860-e5bf-45e0-915f-af99eb2ec008",
                    "LayerId": "945a657c-6aa4-436c-a5b9-2dbbdb250e9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 291,
    "layers": [
        {
            "id": "945a657c-6aa4-436c-a5b9-2dbbdb250e9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2220c11e-f557-4c36-ada8-58314cda3573",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 198,
    "xorig": 97,
    "yorig": 137
}