{
    "id": "d7011ac9-f958-4b4f-8c21-45e4140c40a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship34boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 169,
    "bbox_left": 36,
    "bbox_right": 61,
    "bbox_top": 77,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e7a5459-f067-44be-bae6-2f131300187d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7011ac9-f958-4b4f-8c21-45e4140c40a2",
            "compositeImage": {
                "id": "9681ec18-60a4-4cbd-98c6-a24cf6862078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e7a5459-f067-44be-bae6-2f131300187d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04c5f1f9-e604-4252-9153-d452c6c7fb17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e7a5459-f067-44be-bae6-2f131300187d",
                    "LayerId": "5f644931-aabd-42ba-a1fb-d0d88f88373a"
                }
            ]
        },
        {
            "id": "cd406ab5-d669-452b-9c2f-d93d0031a19f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7011ac9-f958-4b4f-8c21-45e4140c40a2",
            "compositeImage": {
                "id": "edc811e9-b80c-4195-806f-e9e72886b805",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd406ab5-d669-452b-9c2f-d93d0031a19f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d32c5ff0-cd08-47db-b929-ef2207920fde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd406ab5-d669-452b-9c2f-d93d0031a19f",
                    "LayerId": "5f644931-aabd-42ba-a1fb-d0d88f88373a"
                }
            ]
        },
        {
            "id": "ab6f46f5-7775-409e-a059-c890409692bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7011ac9-f958-4b4f-8c21-45e4140c40a2",
            "compositeImage": {
                "id": "ce3ed62b-93dc-479d-816a-2ebdc404f4ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab6f46f5-7775-409e-a059-c890409692bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bab2515c-357e-4bea-a5a7-6b3d50fb97cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab6f46f5-7775-409e-a059-c890409692bf",
                    "LayerId": "5f644931-aabd-42ba-a1fb-d0d88f88373a"
                }
            ]
        },
        {
            "id": "9505637b-f705-4437-8a85-979267ebcf82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7011ac9-f958-4b4f-8c21-45e4140c40a2",
            "compositeImage": {
                "id": "bda74161-118b-447b-8ffb-97046f041632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9505637b-f705-4437-8a85-979267ebcf82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd91b41b-a606-4331-b59e-3cca022aed3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9505637b-f705-4437-8a85-979267ebcf82",
                    "LayerId": "5f644931-aabd-42ba-a1fb-d0d88f88373a"
                }
            ]
        },
        {
            "id": "4b52da41-82c2-4127-91d0-5b6da54d7dfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7011ac9-f958-4b4f-8c21-45e4140c40a2",
            "compositeImage": {
                "id": "5cf3bdde-0f14-4e22-b8dd-a1339ef8ff0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b52da41-82c2-4127-91d0-5b6da54d7dfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d0a489-25a9-448d-a090-22bb69c41882",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b52da41-82c2-4127-91d0-5b6da54d7dfd",
                    "LayerId": "5f644931-aabd-42ba-a1fb-d0d88f88373a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 298,
    "layers": [
        {
            "id": "5f644931-aabd-42ba-a1fb-d0d88f88373a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7011ac9-f958-4b4f-8c21-45e4140c40a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 49,
    "yorig": 104
}