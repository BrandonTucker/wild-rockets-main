{
    "id": "f1401ffc-b4d8-444a-b5a7-533581d15076",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3878cbd2-64b7-4bc5-b9c5-912ae7b8d544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1401ffc-b4d8-444a-b5a7-533581d15076",
            "compositeImage": {
                "id": "08a75867-852d-40e0-89ef-367f5de3d6d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3878cbd2-64b7-4bc5-b9c5-912ae7b8d544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2648a45-3dff-4e94-b4ce-5f3987525ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3878cbd2-64b7-4bc5-b9c5-912ae7b8d544",
                    "LayerId": "f50e3f4d-256b-435c-b160-288fa18f5aa8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "f50e3f4d-256b-435c-b160-288fa18f5aa8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1401ffc-b4d8-444a-b5a7-533581d15076",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}