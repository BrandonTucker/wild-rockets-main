{
    "id": "3ecd907b-5d0f-400c-9451-0014c8664325",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage19button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 1,
    "bbox_right": 99,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51602d41-8c7a-4fb1-b090-1244b00f6ad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ecd907b-5d0f-400c-9451-0014c8664325",
            "compositeImage": {
                "id": "d71472d4-6997-4286-9410-fb70edf1aa9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51602d41-8c7a-4fb1-b090-1244b00f6ad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a3c8c57-d71a-46f9-846b-825f55018e0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51602d41-8c7a-4fb1-b090-1244b00f6ad8",
                    "LayerId": "868355ae-42df-44a6-9979-1f08cdaabda1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "868355ae-42df-44a6-9979-1f08cdaabda1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ecd907b-5d0f-400c-9451-0014c8664325",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}