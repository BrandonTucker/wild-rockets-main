{
    "id": "cd21c6bb-4b7c-45a7-8889-13abade9cfb5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship18explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 30,
    "bbox_right": 57,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0ad6a77-a11e-4a77-81da-09837ec23fb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd21c6bb-4b7c-45a7-8889-13abade9cfb5",
            "compositeImage": {
                "id": "6aa0a341-52d0-4253-9a19-21b326b062bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0ad6a77-a11e-4a77-81da-09837ec23fb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38581709-7399-40a3-a708-a400ac7dd6cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0ad6a77-a11e-4a77-81da-09837ec23fb8",
                    "LayerId": "1b0fedf3-df15-4051-a954-c61ae70d371b"
                }
            ]
        },
        {
            "id": "771727a4-0422-474c-9431-602a34a89a20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd21c6bb-4b7c-45a7-8889-13abade9cfb5",
            "compositeImage": {
                "id": "2e47e3f6-fe28-4e37-90cf-0c8d183f19b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "771727a4-0422-474c-9431-602a34a89a20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5768c2ea-01a2-435a-b2d7-58652cafc804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "771727a4-0422-474c-9431-602a34a89a20",
                    "LayerId": "1b0fedf3-df15-4051-a954-c61ae70d371b"
                }
            ]
        },
        {
            "id": "ffe4bbb5-b900-4e66-99d0-cfe109ac9680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd21c6bb-4b7c-45a7-8889-13abade9cfb5",
            "compositeImage": {
                "id": "6232949c-aff3-4f57-a3df-53f0486ba123",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffe4bbb5-b900-4e66-99d0-cfe109ac9680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d82365-51e0-48e1-923c-f75d4c77ef80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffe4bbb5-b900-4e66-99d0-cfe109ac9680",
                    "LayerId": "1b0fedf3-df15-4051-a954-c61ae70d371b"
                }
            ]
        },
        {
            "id": "495ba8f0-3002-4822-9b12-fe203fd9a0ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd21c6bb-4b7c-45a7-8889-13abade9cfb5",
            "compositeImage": {
                "id": "617b8677-cf73-49f3-9242-393e7f27f833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "495ba8f0-3002-4822-9b12-fe203fd9a0ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "887dfb00-5a6a-4550-8fb4-8ea9f5bf848f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "495ba8f0-3002-4822-9b12-fe203fd9a0ac",
                    "LayerId": "1b0fedf3-df15-4051-a954-c61ae70d371b"
                }
            ]
        },
        {
            "id": "3abbd3dc-4f4e-421c-a44b-db555b34de3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd21c6bb-4b7c-45a7-8889-13abade9cfb5",
            "compositeImage": {
                "id": "507e1f6f-9770-48d5-814d-d49e988cbbda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3abbd3dc-4f4e-421c-a44b-db555b34de3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e484950e-bb34-4fc7-8e2b-72ea69376dbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3abbd3dc-4f4e-421c-a44b-db555b34de3b",
                    "LayerId": "1b0fedf3-df15-4051-a954-c61ae70d371b"
                }
            ]
        },
        {
            "id": "a40f8704-1773-4157-b7b4-6e1520138c66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd21c6bb-4b7c-45a7-8889-13abade9cfb5",
            "compositeImage": {
                "id": "1e428d8a-fa8c-4431-85dc-7b0adc1cc035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40f8704-1773-4157-b7b4-6e1520138c66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e833bff-7e66-4880-aced-d29db35b5d9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40f8704-1773-4157-b7b4-6e1520138c66",
                    "LayerId": "1b0fedf3-df15-4051-a954-c61ae70d371b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 139,
    "layers": [
        {
            "id": "1b0fedf3-df15-4051-a954-c61ae70d371b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd21c6bb-4b7c-45a7-8889-13abade9cfb5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 43,
    "yorig": 49
}