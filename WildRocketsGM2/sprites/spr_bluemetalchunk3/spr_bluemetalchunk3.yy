{
    "id": "24458356-76ad-4a13-b707-3e086d94b27f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bluemetalchunk3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 40,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9d9e104-1290-4e74-9676-fd34aad89672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "c69ba162-754f-49b2-ba5a-94de74f2d2b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d9e104-1290-4e74-9676-fd34aad89672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be78f4e0-3795-4be2-88bf-08b421f30b67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d9e104-1290-4e74-9676-fd34aad89672",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "1c97b4ee-7f38-4bd0-9e73-89e959e686a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "faa8a6c7-7a92-4ab7-9376-0f79964ce97f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c97b4ee-7f38-4bd0-9e73-89e959e686a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29bf807c-d660-46be-a918-dcd254563107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c97b4ee-7f38-4bd0-9e73-89e959e686a1",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "73cbc9fa-aaf7-4e4c-babf-95e2687cee9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "775c982d-8303-4024-a891-83cc7648d3a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73cbc9fa-aaf7-4e4c-babf-95e2687cee9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab99e2c7-748b-462a-931c-4de9fbad5220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73cbc9fa-aaf7-4e4c-babf-95e2687cee9c",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "029e53e7-db26-4114-8ccc-e768194d829b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "fa0dc130-a24a-49e7-b3c0-31bd0c19c291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "029e53e7-db26-4114-8ccc-e768194d829b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0b5f367-5e68-4f98-ba2f-a04fd0e9edf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "029e53e7-db26-4114-8ccc-e768194d829b",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "450c3d83-eb46-4ef7-a76e-5e750da92780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "f7cf1cea-d58e-47e1-bfe2-0bf655b114b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "450c3d83-eb46-4ef7-a76e-5e750da92780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0bb6e48-a4ea-4960-94d8-3f43b42882a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "450c3d83-eb46-4ef7-a76e-5e750da92780",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "19d9cd8b-51fe-4133-9eba-00405317ee5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "74c0bc21-a63d-4215-a555-9585104c19ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19d9cd8b-51fe-4133-9eba-00405317ee5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dea509f-7f35-4d8a-8b9b-54cee8ccbe66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19d9cd8b-51fe-4133-9eba-00405317ee5d",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "c7fa07ce-b8f8-44bf-aff3-77b2f3c11b75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "a422db5b-842e-4394-9a55-ba25480b0015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7fa07ce-b8f8-44bf-aff3-77b2f3c11b75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb1712c8-c920-4fc4-a1ef-a234454ce2b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7fa07ce-b8f8-44bf-aff3-77b2f3c11b75",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "17686982-41b0-438b-9c73-58a5ed6f1c40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "f4d58e63-a6e5-46a8-b579-9535c594705e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17686982-41b0-438b-9c73-58a5ed6f1c40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32907e3b-e0e5-498f-af96-5d8654c9cd3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17686982-41b0-438b-9c73-58a5ed6f1c40",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "9271a295-00c9-4e14-b349-9f4e0fde8a82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "e5834aa5-4dd7-46c2-b2e2-4f45b7f355be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9271a295-00c9-4e14-b349-9f4e0fde8a82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a65dd046-01a5-4767-a722-5bd6b318c811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9271a295-00c9-4e14-b349-9f4e0fde8a82",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "e0b3e044-3132-4705-9dd9-c07885963ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "df1fa36f-cf1a-4cc7-8d6d-70c8a3f9a7e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0b3e044-3132-4705-9dd9-c07885963ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f17f2c2a-d1c3-4be7-9674-2cbafb1c8edd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0b3e044-3132-4705-9dd9-c07885963ec1",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "bbf0b8c3-c1e3-4c88-82b4-7adfe4ca7b7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "db3fa72c-de07-43eb-89d7-11551dab2bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbf0b8c3-c1e3-4c88-82b4-7adfe4ca7b7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5c76e66-6491-4ff8-9423-40d156c12e00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf0b8c3-c1e3-4c88-82b4-7adfe4ca7b7b",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "8fc22633-46a9-4712-abb9-e86381ae5e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "a46b7a5c-0fc7-4ad4-ac6a-74c79572ea81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fc22633-46a9-4712-abb9-e86381ae5e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "503793cb-5bf7-485d-ae14-78549b42a70e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fc22633-46a9-4712-abb9-e86381ae5e5d",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "fd4abbc4-8945-4d5e-917b-4e6e600630d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "b6389fd0-2b26-4242-8074-c6f96a58e96c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd4abbc4-8945-4d5e-917b-4e6e600630d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8d64134-8239-4934-bc80-1bac024f93ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd4abbc4-8945-4d5e-917b-4e6e600630d7",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "0fc8d20a-24d6-4707-8f93-29609bfcd7cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "e44afea2-7b26-4b38-8fb7-59afd9de8d2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fc8d20a-24d6-4707-8f93-29609bfcd7cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6d1b376-61ea-48c0-a64a-4079a1f84633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fc8d20a-24d6-4707-8f93-29609bfcd7cc",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        },
        {
            "id": "6024c084-d3fe-4ae1-b440-156689c98918",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "compositeImage": {
                "id": "3a64aff9-a3f3-4a34-9071-5560c7444b0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6024c084-d3fe-4ae1-b440-156689c98918",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "340c5b82-79f4-4767-bd05-b1d4c91813e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6024c084-d3fe-4ae1-b440-156689c98918",
                    "LayerId": "18eaad4a-f322-4cbb-9a2e-a3de2914864c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "18eaad4a-f322-4cbb-9a2e-a3de2914864c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24458356-76ad-4a13-b707-3e086d94b27f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 20,
    "yorig": 20
}