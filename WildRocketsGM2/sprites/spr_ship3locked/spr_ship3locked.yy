{
    "id": "26554abc-61e7-4277-a01e-e8c81f89775d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship3locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d669c32-fbb3-4301-a946-387877a5dd65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26554abc-61e7-4277-a01e-e8c81f89775d",
            "compositeImage": {
                "id": "2e534173-07a0-4f6f-8df8-12bcd80ca1ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d669c32-fbb3-4301-a946-387877a5dd65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0e22ac8-4062-4005-8f5b-986d43585d1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d669c32-fbb3-4301-a946-387877a5dd65",
                    "LayerId": "9a9d3b3f-aec2-4e73-9289-be36832a497b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "9a9d3b3f-aec2-4e73-9289-be36832a497b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26554abc-61e7-4277-a01e-e8c81f89775d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}