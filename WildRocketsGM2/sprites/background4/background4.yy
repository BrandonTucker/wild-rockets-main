{
    "id": "2a13ebbf-c6cd-41cd-9c1c-4f569ad09f5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background4",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ef16ee4-bd57-49a4-9294-b316609c052b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a13ebbf-c6cd-41cd-9c1c-4f569ad09f5c",
            "compositeImage": {
                "id": "4f177ada-8dff-48b8-b493-2241c5f2f02b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ef16ee4-bd57-49a4-9294-b316609c052b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fbfae2f-73dc-4feb-93dd-5ba42d2cf362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ef16ee4-bd57-49a4-9294-b316609c052b",
                    "LayerId": "825cfecc-438b-4de3-a881-e7cf7ad37010"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "825cfecc-438b-4de3-a881-e7cf7ad37010",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a13ebbf-c6cd-41cd-9c1c-4f569ad09f5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 0,
    "yorig": 0
}