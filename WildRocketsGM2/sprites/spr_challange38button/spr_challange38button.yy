{
    "id": "b0620c59-93e9-4c81-aeac-d15fa3c2e459",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange38button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5f8ffbd-a488-4bba-aaa5-7edd39977cc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0620c59-93e9-4c81-aeac-d15fa3c2e459",
            "compositeImage": {
                "id": "0457d0cd-02f4-4948-9b35-00eb8a0304ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5f8ffbd-a488-4bba-aaa5-7edd39977cc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90c1f53f-d6d3-4922-a36e-9335d4860769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5f8ffbd-a488-4bba-aaa5-7edd39977cc7",
                    "LayerId": "de256053-a2ce-4cf5-80c7-d5dddf4c4fe4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "de256053-a2ce-4cf5-80c7-d5dddf4c4fe4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0620c59-93e9-4c81-aeac-d15fa3c2e459",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}