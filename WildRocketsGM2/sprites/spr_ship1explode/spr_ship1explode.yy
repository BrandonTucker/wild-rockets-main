{
    "id": "d9791fc6-4d61-400a-82b9-a753a1f3cf40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship1explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 37,
    "bbox_right": 69,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8aba846-8e76-4850-91d2-f0f25cbc1708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9791fc6-4d61-400a-82b9-a753a1f3cf40",
            "compositeImage": {
                "id": "77a90ce4-82c9-4010-8925-ffddb23626bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8aba846-8e76-4850-91d2-f0f25cbc1708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e22b7963-d66e-4862-8f86-2c072186d795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8aba846-8e76-4850-91d2-f0f25cbc1708",
                    "LayerId": "cd5a7747-3fc3-450e-8f7c-8a7d89c055d3"
                }
            ]
        },
        {
            "id": "5afa4c27-77ca-41ce-9482-710460f42ee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9791fc6-4d61-400a-82b9-a753a1f3cf40",
            "compositeImage": {
                "id": "99cfd6b5-bf2a-4517-aaa3-9fc8837ce3cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5afa4c27-77ca-41ce-9482-710460f42ee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea7ccb9c-b088-4ed6-a01d-e67e3ed2d350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5afa4c27-77ca-41ce-9482-710460f42ee4",
                    "LayerId": "cd5a7747-3fc3-450e-8f7c-8a7d89c055d3"
                }
            ]
        },
        {
            "id": "24c0bc9b-7a58-433e-8f1e-e112c4968b37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9791fc6-4d61-400a-82b9-a753a1f3cf40",
            "compositeImage": {
                "id": "ecfb06d6-33b8-4eec-a01d-4aecab1c16ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24c0bc9b-7a58-433e-8f1e-e112c4968b37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99b6bfc2-0643-4632-9d30-2d1ce77538b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24c0bc9b-7a58-433e-8f1e-e112c4968b37",
                    "LayerId": "cd5a7747-3fc3-450e-8f7c-8a7d89c055d3"
                }
            ]
        },
        {
            "id": "9225be07-02bb-44c1-a94c-fc844b47e0e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9791fc6-4d61-400a-82b9-a753a1f3cf40",
            "compositeImage": {
                "id": "ed8d060c-4480-4036-a4f2-df172ab26452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9225be07-02bb-44c1-a94c-fc844b47e0e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd2e861e-8117-4284-9396-d177fe91c5cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9225be07-02bb-44c1-a94c-fc844b47e0e1",
                    "LayerId": "cd5a7747-3fc3-450e-8f7c-8a7d89c055d3"
                }
            ]
        },
        {
            "id": "7d771ecb-afce-4a60-9b64-a4b6f40d5e0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9791fc6-4d61-400a-82b9-a753a1f3cf40",
            "compositeImage": {
                "id": "dc53497b-6306-480f-b970-fbf8453830fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d771ecb-afce-4a60-9b64-a4b6f40d5e0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78208df9-6726-4d83-8c6b-f935da30bcdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d771ecb-afce-4a60-9b64-a4b6f40d5e0b",
                    "LayerId": "cd5a7747-3fc3-450e-8f7c-8a7d89c055d3"
                }
            ]
        },
        {
            "id": "3b59f7d3-84b5-4a15-943d-7827882f9bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9791fc6-4d61-400a-82b9-a753a1f3cf40",
            "compositeImage": {
                "id": "c7bc114d-ac49-4baa-b886-e987e7803ffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b59f7d3-84b5-4a15-943d-7827882f9bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73a9e616-19d9-4a12-a403-449b54e5cb7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b59f7d3-84b5-4a15-943d-7827882f9bbb",
                    "LayerId": "cd5a7747-3fc3-450e-8f7c-8a7d89c055d3"
                }
            ]
        },
        {
            "id": "130b715f-d82d-4035-92c0-ca14fb9b5891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9791fc6-4d61-400a-82b9-a753a1f3cf40",
            "compositeImage": {
                "id": "8af3e311-a810-414c-836c-451f54885560",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "130b715f-d82d-4035-92c0-ca14fb9b5891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cb070a7-3b45-4e25-a0f7-5d5af5a9c4fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "130b715f-d82d-4035-92c0-ca14fb9b5891",
                    "LayerId": "cd5a7747-3fc3-450e-8f7c-8a7d89c055d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 166,
    "layers": [
        {
            "id": "cd5a7747-3fc3-450e-8f7c-8a7d89c055d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9791fc6-4d61-400a-82b9-a753a1f3cf40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 104,
    "xorig": 51,
    "yorig": 53
}