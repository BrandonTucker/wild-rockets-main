{
    "id": "0fb9e2e2-f008-4f5d-b9f3-83ddb11c2a45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship6boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 29,
    "bbox_right": 54,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6630c502-fc91-4fef-949b-b58fb5055e19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fb9e2e2-f008-4f5d-b9f3-83ddb11c2a45",
            "compositeImage": {
                "id": "01717a79-8e04-408d-8b21-0bcca56e3d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6630c502-fc91-4fef-949b-b58fb5055e19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab8d947e-0923-4405-8af0-90790ad763d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6630c502-fc91-4fef-949b-b58fb5055e19",
                    "LayerId": "c20080da-44d3-4de4-a84f-19e2be0add78"
                }
            ]
        },
        {
            "id": "3bff8f40-55c0-4806-99bf-863dcb1109f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fb9e2e2-f008-4f5d-b9f3-83ddb11c2a45",
            "compositeImage": {
                "id": "f7c64eec-f22b-41b1-ad25-7e508b9d246a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bff8f40-55c0-4806-99bf-863dcb1109f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38b835f-49f8-4667-bd45-9967ca09cd65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bff8f40-55c0-4806-99bf-863dcb1109f1",
                    "LayerId": "c20080da-44d3-4de4-a84f-19e2be0add78"
                }
            ]
        },
        {
            "id": "7e10a1bd-4022-4f15-9567-38df7816da75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fb9e2e2-f008-4f5d-b9f3-83ddb11c2a45",
            "compositeImage": {
                "id": "b0efdd86-b157-407a-8e81-a57d8ca56670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e10a1bd-4022-4f15-9567-38df7816da75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fda34140-014c-48fd-9f8b-1642623ada15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e10a1bd-4022-4f15-9567-38df7816da75",
                    "LayerId": "c20080da-44d3-4de4-a84f-19e2be0add78"
                }
            ]
        },
        {
            "id": "08212f1e-4be0-41c7-a472-10600c0cf12e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fb9e2e2-f008-4f5d-b9f3-83ddb11c2a45",
            "compositeImage": {
                "id": "c983a6ff-dc83-4eee-b825-5ea575add334",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08212f1e-4be0-41c7-a472-10600c0cf12e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b922e1ee-5660-49cd-aed6-98d45f45a57d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08212f1e-4be0-41c7-a472-10600c0cf12e",
                    "LayerId": "c20080da-44d3-4de4-a84f-19e2be0add78"
                }
            ]
        },
        {
            "id": "fe9ba989-4b53-45a6-9ef8-2c6a84d9730a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fb9e2e2-f008-4f5d-b9f3-83ddb11c2a45",
            "compositeImage": {
                "id": "708b4b7f-85bc-465a-b4ce-eef7f6024aa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe9ba989-4b53-45a6-9ef8-2c6a84d9730a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d184940-512e-44f3-82cf-549271e4b164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe9ba989-4b53-45a6-9ef8-2c6a84d9730a",
                    "LayerId": "c20080da-44d3-4de4-a84f-19e2be0add78"
                }
            ]
        },
        {
            "id": "dc1f7847-3dc1-4308-b9a0-a11f3d393030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fb9e2e2-f008-4f5d-b9f3-83ddb11c2a45",
            "compositeImage": {
                "id": "a650d61d-7b8a-44c5-8777-73a91c48b072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc1f7847-3dc1-4308-b9a0-a11f3d393030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2ab80e-4246-4dcc-b2cd-8b7254e561d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1f7847-3dc1-4308-b9a0-a11f3d393030",
                    "LayerId": "c20080da-44d3-4de4-a84f-19e2be0add78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "c20080da-44d3-4de4-a84f-19e2be0add78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fb9e2e2-f008-4f5d-b9f3-83ddb11c2a45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 42,
    "yorig": 47
}