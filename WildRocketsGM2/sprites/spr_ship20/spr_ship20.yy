{
    "id": "6fa1c3a1-6e1d-40f4-a5a6-324680e4334d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship20",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ede33a9-13a3-430c-b278-d845bc15b8a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa1c3a1-6e1d-40f4-a5a6-324680e4334d",
            "compositeImage": {
                "id": "76de7683-3db3-4a91-943c-274ae1c45a86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ede33a9-13a3-430c-b278-d845bc15b8a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d8625f1-0ea0-4aeb-9939-0874d2858d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ede33a9-13a3-430c-b278-d845bc15b8a1",
                    "LayerId": "8bdfed14-559d-4c0c-be5d-5f7c489f0c3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "8bdfed14-559d-4c0c-be5d-5f7c489f0c3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fa1c3a1-6e1d-40f4-a5a6-324680e4334d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 49
}