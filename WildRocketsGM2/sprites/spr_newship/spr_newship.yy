{
    "id": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_newship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33ba4bca-4504-45fd-95f1-4ac1a56b7866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "ed881715-380a-4b92-9156-7284c7a025ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ba4bca-4504-45fd-95f1-4ac1a56b7866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "855af396-3037-46f9-b57b-21b1c68b97dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ba4bca-4504-45fd-95f1-4ac1a56b7866",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "97268093-2b69-48d3-b718-6e3925dd8747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "f6a600b6-fade-4d96-9f4b-31d6fe411dda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97268093-2b69-48d3-b718-6e3925dd8747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40c8471-0849-4843-81d0-a47fa89fe5f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97268093-2b69-48d3-b718-6e3925dd8747",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "879b2c3e-1e91-431a-a379-c9a0e26f4885",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "8e9f8f89-37f3-4830-b8f3-b8b0918950a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "879b2c3e-1e91-431a-a379-c9a0e26f4885",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd0f80b-fb57-4677-bb17-f7e74418276a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "879b2c3e-1e91-431a-a379-c9a0e26f4885",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "657f2cb0-29cd-4e41-bcbd-32e688b67b87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "6cfb2aeb-411a-4668-876f-d11dbc5b5d10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "657f2cb0-29cd-4e41-bcbd-32e688b67b87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64fe833c-3707-4412-9e34-5cf4bb19ea0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "657f2cb0-29cd-4e41-bcbd-32e688b67b87",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "87295990-d788-4dfc-9ff3-a783b781ed16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "4951bc6b-b74a-46cd-a299-2e2ca86406a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87295990-d788-4dfc-9ff3-a783b781ed16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0f0249e-1891-4b44-aeb2-3556982b9f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87295990-d788-4dfc-9ff3-a783b781ed16",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "eb9d9ceb-8653-46e5-99cb-dd7e1d54fe5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "c1f249c0-aef2-4b4f-bb2f-deca64609adb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb9d9ceb-8653-46e5-99cb-dd7e1d54fe5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1d63d23-5860-40a0-9e62-031e6218d0b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb9d9ceb-8653-46e5-99cb-dd7e1d54fe5c",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "03ea5fa9-2a83-4d84-9edc-7d582d69fc48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "23d2a133-0578-4dc3-baf2-312b4615e9a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ea5fa9-2a83-4d84-9edc-7d582d69fc48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45f63ff9-63a6-42cd-8b10-667cb49720dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ea5fa9-2a83-4d84-9edc-7d582d69fc48",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "c37875b0-9164-4085-9093-d047663d2735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "a5dcae44-3209-4f7f-be0b-b18762914197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37875b0-9164-4085-9093-d047663d2735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30de8e1b-db80-4603-a0d0-9e2080ee0471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37875b0-9164-4085-9093-d047663d2735",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "bac71695-ed9e-4d7c-85aa-b218c1ec92fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "26deebc5-5056-4313-a752-79441ab272e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bac71695-ed9e-4d7c-85aa-b218c1ec92fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ff52cd7-4b8c-4e83-bf8f-3c0ceffb2d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bac71695-ed9e-4d7c-85aa-b218c1ec92fd",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "d814603f-3e42-47a9-b239-8c278b6675ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "587b530d-22eb-4186-899c-3cc21f4d71e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d814603f-3e42-47a9-b239-8c278b6675ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c1b159a-e987-4d35-9210-b47e0c0afb85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d814603f-3e42-47a9-b239-8c278b6675ae",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "de23f5f6-0f8f-4eaf-a2bb-ccf4cdaf0357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "ffabeb11-3251-4f1e-867c-620063fa7437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de23f5f6-0f8f-4eaf-a2bb-ccf4cdaf0357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baebcbeb-d2f7-47c8-95ca-31e74cabc4f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de23f5f6-0f8f-4eaf-a2bb-ccf4cdaf0357",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        },
        {
            "id": "28488ce9-c021-4587-8b28-d2e119c4dede",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "compositeImage": {
                "id": "12416e46-2846-4abd-841a-8d399178c318",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28488ce9-c021-4587-8b28-d2e119c4dede",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e77914f-a97f-41da-8985-152167a568f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28488ce9-c021-4587-8b28-d2e119c4dede",
                    "LayerId": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "aa6c1ef2-111c-4e54-8a0b-6757787e7cdb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58cf5f84-f5e1-4a11-ae72-e7497529a6a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 11
}