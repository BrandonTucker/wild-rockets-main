{
    "id": "b434a762-7ca1-4b38-8c42-b037bdaddedb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship16boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 23,
    "bbox_right": 48,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14c3df9f-62a5-446c-a605-0879deff8da8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b434a762-7ca1-4b38-8c42-b037bdaddedb",
            "compositeImage": {
                "id": "cb45cc4c-b3bf-4981-bf30-fde0d4b448fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14c3df9f-62a5-446c-a605-0879deff8da8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e9a1746-e7b5-4993-8b03-acaef9c25ef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14c3df9f-62a5-446c-a605-0879deff8da8",
                    "LayerId": "e5f5b3a5-31bc-45b6-b11a-5c958c89b698"
                }
            ]
        },
        {
            "id": "634e4804-b8d8-4706-9a55-25aa07b29841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b434a762-7ca1-4b38-8c42-b037bdaddedb",
            "compositeImage": {
                "id": "ee62c2a4-4b44-4ddd-a70a-1bbbeea53c10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "634e4804-b8d8-4706-9a55-25aa07b29841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b5957a1-cf26-4d97-98ab-a19c733cbe9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "634e4804-b8d8-4706-9a55-25aa07b29841",
                    "LayerId": "e5f5b3a5-31bc-45b6-b11a-5c958c89b698"
                }
            ]
        },
        {
            "id": "a01dcb04-302e-4e7c-8941-74aac5014c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b434a762-7ca1-4b38-8c42-b037bdaddedb",
            "compositeImage": {
                "id": "5eca831b-e93d-45a8-b545-0d37b00d3a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a01dcb04-302e-4e7c-8941-74aac5014c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45a3a992-2802-4f6a-901c-5fe0706e2e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a01dcb04-302e-4e7c-8941-74aac5014c73",
                    "LayerId": "e5f5b3a5-31bc-45b6-b11a-5c958c89b698"
                }
            ]
        },
        {
            "id": "d5f884ec-b591-4410-9d1b-3ce2f1bd0531",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b434a762-7ca1-4b38-8c42-b037bdaddedb",
            "compositeImage": {
                "id": "0f213876-e27c-4027-9dfa-2f971f7cef1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5f884ec-b591-4410-9d1b-3ce2f1bd0531",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5eb2fea-4725-4e63-bd7c-1b5959a37d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5f884ec-b591-4410-9d1b-3ce2f1bd0531",
                    "LayerId": "e5f5b3a5-31bc-45b6-b11a-5c958c89b698"
                }
            ]
        },
        {
            "id": "1456dbd3-4821-46c5-a158-5512278d6d8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b434a762-7ca1-4b38-8c42-b037bdaddedb",
            "compositeImage": {
                "id": "b5e00784-c5e1-459a-982c-ddd11c2f0d2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1456dbd3-4821-46c5-a158-5512278d6d8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1af5e543-0c77-448a-a7ca-0f88f120804f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1456dbd3-4821-46c5-a158-5512278d6d8f",
                    "LayerId": "e5f5b3a5-31bc-45b6-b11a-5c958c89b698"
                }
            ]
        },
        {
            "id": "cbf6dcdb-6c1a-4cfd-a9e4-7c7ee56719c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b434a762-7ca1-4b38-8c42-b037bdaddedb",
            "compositeImage": {
                "id": "6553ac75-3649-40ba-8460-66a715140a98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf6dcdb-6c1a-4cfd-a9e4-7c7ee56719c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b89031ee-bd37-461a-a7d3-c2aabb505e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf6dcdb-6c1a-4cfd-a9e4-7c7ee56719c0",
                    "LayerId": "e5f5b3a5-31bc-45b6-b11a-5c958c89b698"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "e5f5b3a5-31bc-45b6-b11a-5c958c89b698",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b434a762-7ca1-4b38-8c42-b037bdaddedb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 36,
    "yorig": 40
}