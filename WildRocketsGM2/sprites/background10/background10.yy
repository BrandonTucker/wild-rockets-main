{
    "id": "de492048-6020-4b81-83a9-ccf78ea9cc61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background10",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 959,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "494f3679-44f2-4ed7-904a-27a2256a5fef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de492048-6020-4b81-83a9-ccf78ea9cc61",
            "compositeImage": {
                "id": "1be84a55-6386-4528-855e-c7b24783f36a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "494f3679-44f2-4ed7-904a-27a2256a5fef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c394b700-28c3-4fe2-9ef7-443440c2e51a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "494f3679-44f2-4ed7-904a-27a2256a5fef",
                    "LayerId": "c5819660-5157-47d2-b1b9-3c63a7a8db48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "c5819660-5157-47d2-b1b9-3c63a7a8db48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de492048-6020-4b81-83a9-ccf78ea9cc61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}