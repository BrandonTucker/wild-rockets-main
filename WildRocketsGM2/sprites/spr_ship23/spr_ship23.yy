{
    "id": "fc7e64d5-9da5-4931-a650-2388002cc4e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship23",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 24,
    "bbox_right": 49,
    "bbox_top": -2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5032d26-7c88-4d00-8af8-cfc04eb11679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc7e64d5-9da5-4931-a650-2388002cc4e4",
            "compositeImage": {
                "id": "a2a30885-71f8-43e5-a0ce-868516730fb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5032d26-7c88-4d00-8af8-cfc04eb11679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ab1db71-fea9-45b2-a6e2-5b623f4c5144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5032d26-7c88-4d00-8af8-cfc04eb11679",
                    "LayerId": "56cf66e1-65d7-4536-8855-ac2bb1e62341"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "56cf66e1-65d7-4536-8855-ac2bb1e62341",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc7e64d5-9da5-4931-a650-2388002cc4e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 37,
    "yorig": 32
}