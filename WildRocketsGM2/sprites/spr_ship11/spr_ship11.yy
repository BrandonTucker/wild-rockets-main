{
    "id": "cc3ebfad-d636-4642-b2a3-e7c41ad84b6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04105891-4a7f-44a2-8eef-846ee461c8eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc3ebfad-d636-4642-b2a3-e7c41ad84b6a",
            "compositeImage": {
                "id": "b44200ef-783a-43e1-8763-e5721aa9d7dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04105891-4a7f-44a2-8eef-846ee461c8eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8afffb97-52d5-427c-b28e-00b998b06ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04105891-4a7f-44a2-8eef-846ee461c8eb",
                    "LayerId": "bb87aaa8-2881-4392-aa86-7e3a8020c1f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "bb87aaa8-2881-4392-aa86-7e3a8020c1f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc3ebfad-d636-4642-b2a3-e7c41ad84b6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 42
}