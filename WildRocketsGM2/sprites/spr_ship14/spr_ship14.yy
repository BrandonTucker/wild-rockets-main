{
    "id": "b5f3f700-9456-410c-8609-f8ec5a55b668",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship14",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eff802a6-89fc-4e3b-83c9-b71f46cc68e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5f3f700-9456-410c-8609-f8ec5a55b668",
            "compositeImage": {
                "id": "1ec231cb-6420-4234-9a4f-a784ff97af58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eff802a6-89fc-4e3b-83c9-b71f46cc68e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3d2a3c8-1b6e-40ec-887d-2ad710968a9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eff802a6-89fc-4e3b-83c9-b71f46cc68e7",
                    "LayerId": "4fc1c193-a099-43c4-a55f-887698bdefcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "4fc1c193-a099-43c4-a55f-887698bdefcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5f3f700-9456-410c-8609-f8ec5a55b668",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 65
}