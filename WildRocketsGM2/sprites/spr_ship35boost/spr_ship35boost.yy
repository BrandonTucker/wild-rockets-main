{
    "id": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship35boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 61,
    "bbox_right": 86,
    "bbox_top": 49,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00728245-4fa7-4c2c-b616-06f76477e92c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
            "compositeImage": {
                "id": "ecac86a5-5199-4071-ab09-4a777c6339f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00728245-4fa7-4c2c-b616-06f76477e92c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4657149d-b7fe-49b2-81eb-95cea97065e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00728245-4fa7-4c2c-b616-06f76477e92c",
                    "LayerId": "2d265345-1a77-454a-8001-69a88e5564b5"
                }
            ]
        },
        {
            "id": "d76dc6fa-2e9a-4d52-b332-2a5b7f86f2aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
            "compositeImage": {
                "id": "e48b82e0-9fe7-48db-ad8c-350f8dec07fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d76dc6fa-2e9a-4d52-b332-2a5b7f86f2aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7177cdf-71cc-47c2-bc18-67de80161c2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d76dc6fa-2e9a-4d52-b332-2a5b7f86f2aa",
                    "LayerId": "2d265345-1a77-454a-8001-69a88e5564b5"
                }
            ]
        },
        {
            "id": "0697fa98-7c9d-44fb-9870-b63878485485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
            "compositeImage": {
                "id": "bf62a729-9b90-4658-bc43-dfb455890942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0697fa98-7c9d-44fb-9870-b63878485485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "453d01c4-4843-49c5-ba03-557798da2c89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0697fa98-7c9d-44fb-9870-b63878485485",
                    "LayerId": "2d265345-1a77-454a-8001-69a88e5564b5"
                }
            ]
        },
        {
            "id": "4f9fa099-567f-4ca1-acc5-d0b45aab5d79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
            "compositeImage": {
                "id": "7d4cfb47-2539-46bf-8014-af02ef742594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f9fa099-567f-4ca1-acc5-d0b45aab5d79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32476134-f219-4752-afa6-a37f7b77c05f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f9fa099-567f-4ca1-acc5-d0b45aab5d79",
                    "LayerId": "2d265345-1a77-454a-8001-69a88e5564b5"
                }
            ]
        },
        {
            "id": "eae4c1aa-4d8c-45ab-8888-dd269ffd4131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
            "compositeImage": {
                "id": "56be3e31-e3dc-4f8a-a5cd-be96a655a209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eae4c1aa-4d8c-45ab-8888-dd269ffd4131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c93e5cd-334f-4466-b93a-67c8761f1761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eae4c1aa-4d8c-45ab-8888-dd269ffd4131",
                    "LayerId": "2d265345-1a77-454a-8001-69a88e5564b5"
                }
            ]
        },
        {
            "id": "6d7352a2-ae1a-414b-989b-aba0931f5dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
            "compositeImage": {
                "id": "2c12b9f8-590d-4455-a2d0-5020efafa1ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d7352a2-ae1a-414b-989b-aba0931f5dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ae9d24-4bf1-412c-8a7c-1990e34d0d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d7352a2-ae1a-414b-989b-aba0931f5dc8",
                    "LayerId": "2d265345-1a77-454a-8001-69a88e5564b5"
                }
            ]
        },
        {
            "id": "38a2072e-47fd-409e-82e9-17fd8671deda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
            "compositeImage": {
                "id": "b908f64e-4d23-45d2-8a4e-e06e26984a19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38a2072e-47fd-409e-82e9-17fd8671deda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba6a3f90-77a1-4852-a199-acaf58f4f868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38a2072e-47fd-409e-82e9-17fd8671deda",
                    "LayerId": "2d265345-1a77-454a-8001-69a88e5564b5"
                }
            ]
        },
        {
            "id": "d79d4e9b-ab87-41a8-8b10-f1ceca7855aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
            "compositeImage": {
                "id": "e79d0c5e-61b4-4ac5-9b48-846bb76f87fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d79d4e9b-ab87-41a8-8b10-f1ceca7855aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbfc8c84-000e-4c08-a3f0-d7e2d17cb837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d79d4e9b-ab87-41a8-8b10-f1ceca7855aa",
                    "LayerId": "2d265345-1a77-454a-8001-69a88e5564b5"
                }
            ]
        },
        {
            "id": "f0366184-5071-4f69-bb02-15d8ed9444d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
            "compositeImage": {
                "id": "df9df87e-d859-4599-a304-5a6be940c938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0366184-5071-4f69-bb02-15d8ed9444d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb4d8a6-ceb9-4d54-b1c6-3b438d82f694",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0366184-5071-4f69-bb02-15d8ed9444d7",
                    "LayerId": "2d265345-1a77-454a-8001-69a88e5564b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 190,
    "layers": [
        {
            "id": "2d265345-1a77-454a-8001-69a88e5564b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36b92b44-29af-4d0b-b1ba-983264b86fb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 149,
    "xorig": 74,
    "yorig": 94
}