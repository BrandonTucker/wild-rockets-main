{
    "id": "61bff5f0-43c0-4226-927c-ab7a47f473c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite77",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 1,
    "bbox_right": 53,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbc3d59e-13c5-4b49-8152-5429301eb2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61bff5f0-43c0-4226-927c-ab7a47f473c3",
            "compositeImage": {
                "id": "6bd98087-9005-42bd-9ff1-410e051244fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc3d59e-13c5-4b49-8152-5429301eb2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5219f6ae-ad9d-47eb-bd97-88ee054e0887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc3d59e-13c5-4b49-8152-5429301eb2fc",
                    "LayerId": "ddca6ecd-120a-4427-aa7d-7d4d3c7b7027"
                }
            ]
        },
        {
            "id": "1fc5bc7b-e693-45f2-b9e2-9962dfdc7da9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61bff5f0-43c0-4226-927c-ab7a47f473c3",
            "compositeImage": {
                "id": "001120c6-4e8e-4e84-ad01-b99c2b37ff32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fc5bc7b-e693-45f2-b9e2-9962dfdc7da9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e10b950-b44b-4bdf-bfcc-8904a7427d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fc5bc7b-e693-45f2-b9e2-9962dfdc7da9",
                    "LayerId": "ddca6ecd-120a-4427-aa7d-7d4d3c7b7027"
                }
            ]
        },
        {
            "id": "d3b3c38a-52c4-4302-8d19-9df6490e6113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61bff5f0-43c0-4226-927c-ab7a47f473c3",
            "compositeImage": {
                "id": "575f240c-426c-45ac-9503-b8bef7dcdfe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3b3c38a-52c4-4302-8d19-9df6490e6113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c5d1e03-066b-4bd9-9e39-0ddb58b0904b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3b3c38a-52c4-4302-8d19-9df6490e6113",
                    "LayerId": "ddca6ecd-120a-4427-aa7d-7d4d3c7b7027"
                }
            ]
        },
        {
            "id": "95a0f6bb-1f77-4972-800e-1ae98df84b9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61bff5f0-43c0-4226-927c-ab7a47f473c3",
            "compositeImage": {
                "id": "0bf431f3-3c23-40ad-af17-36ff10a786b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95a0f6bb-1f77-4972-800e-1ae98df84b9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0595974d-1579-4fdd-8c20-900870d77db0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95a0f6bb-1f77-4972-800e-1ae98df84b9c",
                    "LayerId": "ddca6ecd-120a-4427-aa7d-7d4d3c7b7027"
                }
            ]
        },
        {
            "id": "7c0d3ca2-1937-4c00-a9ee-a8fd98756a61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61bff5f0-43c0-4226-927c-ab7a47f473c3",
            "compositeImage": {
                "id": "545c75b6-e0d5-454a-9b9c-bbaddf8d57ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0d3ca2-1937-4c00-a9ee-a8fd98756a61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40266426-b148-46d1-868e-5f2d5ce6a058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0d3ca2-1937-4c00-a9ee-a8fd98756a61",
                    "LayerId": "ddca6ecd-120a-4427-aa7d-7d4d3c7b7027"
                }
            ]
        },
        {
            "id": "6710e82b-e1dd-455e-aebe-34f1ca7cd940",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61bff5f0-43c0-4226-927c-ab7a47f473c3",
            "compositeImage": {
                "id": "23909140-5503-44e2-b21a-0027db76268c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6710e82b-e1dd-455e-aebe-34f1ca7cd940",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2775f716-5e4e-433f-9219-9812764494e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6710e82b-e1dd-455e-aebe-34f1ca7cd940",
                    "LayerId": "ddca6ecd-120a-4427-aa7d-7d4d3c7b7027"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "ddca6ecd-120a-4427-aa7d-7d4d3c7b7027",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61bff5f0-43c0-4226-927c-ab7a47f473c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 27,
    "yorig": 49
}