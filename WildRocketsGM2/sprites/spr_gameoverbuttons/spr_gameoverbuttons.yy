{
    "id": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gameoverbuttons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 152,
    "bbox_left": 20,
    "bbox_right": 340,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ee27466-6706-4f09-911e-0cdfa6bb2bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "compositeImage": {
                "id": "f889e699-c9e1-4383-969f-05aa4b5a987f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ee27466-6706-4f09-911e-0cdfa6bb2bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b4cc1c-d926-49b0-9dc0-b1b3840c8ed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ee27466-6706-4f09-911e-0cdfa6bb2bc7",
                    "LayerId": "65c9ee26-0dcb-479a-bdca-388a767c07f4"
                }
            ]
        },
        {
            "id": "4de37b5b-9064-40bf-bb5c-acd47e2f6ef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "compositeImage": {
                "id": "574433fb-e004-4e60-b618-afb359229212",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4de37b5b-9064-40bf-bb5c-acd47e2f6ef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b218e17-fe3a-478d-9209-bd336cee0889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4de37b5b-9064-40bf-bb5c-acd47e2f6ef0",
                    "LayerId": "65c9ee26-0dcb-479a-bdca-388a767c07f4"
                }
            ]
        },
        {
            "id": "e1293330-ff73-4d8e-9cc8-34a5e8f0a8c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "compositeImage": {
                "id": "ec2d5c8b-223c-48e4-8da1-0aab5b09e2d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1293330-ff73-4d8e-9cc8-34a5e8f0a8c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95cc8674-9e5e-4bff-bcbf-fefbbaec16ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1293330-ff73-4d8e-9cc8-34a5e8f0a8c9",
                    "LayerId": "65c9ee26-0dcb-479a-bdca-388a767c07f4"
                }
            ]
        },
        {
            "id": "c0614ba7-c389-42ad-ab1f-18aee4959b19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "compositeImage": {
                "id": "4962902b-fe73-4f03-9edb-a1360778bfe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0614ba7-c389-42ad-ab1f-18aee4959b19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51ceee23-18df-4a4e-8564-88b70fb9d8cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0614ba7-c389-42ad-ab1f-18aee4959b19",
                    "LayerId": "65c9ee26-0dcb-479a-bdca-388a767c07f4"
                }
            ]
        },
        {
            "id": "d3846795-6dff-44a1-85dc-d541f5bed248",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "compositeImage": {
                "id": "5f6c845f-b2f0-40d3-b109-8a03d0c54ade",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3846795-6dff-44a1-85dc-d541f5bed248",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ef16a87-3691-4554-abac-96838e478530",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3846795-6dff-44a1-85dc-d541f5bed248",
                    "LayerId": "65c9ee26-0dcb-479a-bdca-388a767c07f4"
                }
            ]
        },
        {
            "id": "4615bdee-ede8-4f8a-87de-600b8a60e3e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "compositeImage": {
                "id": "13d2cb34-4bbe-4f97-adb6-f260ce65bfde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4615bdee-ede8-4f8a-87de-600b8a60e3e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f02cc51f-a813-4e6e-97d5-da6a9d6f0025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4615bdee-ede8-4f8a-87de-600b8a60e3e8",
                    "LayerId": "65c9ee26-0dcb-479a-bdca-388a767c07f4"
                }
            ]
        },
        {
            "id": "fcc780c6-093d-4709-84cb-c514d211ad37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "compositeImage": {
                "id": "5d783fd2-8f38-422b-a936-4cf42f410605",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcc780c6-093d-4709-84cb-c514d211ad37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "673055d9-c13b-460f-b37b-3581fbbdf103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcc780c6-093d-4709-84cb-c514d211ad37",
                    "LayerId": "65c9ee26-0dcb-479a-bdca-388a767c07f4"
                }
            ]
        },
        {
            "id": "9a89d87d-7fb1-48e4-8e99-3316655f4d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "compositeImage": {
                "id": "c6a700e6-092b-46c8-bfc3-e6137444465a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a89d87d-7fb1-48e4-8e99-3316655f4d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33340c6b-4a9b-438d-b0b1-de3eef6d0055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a89d87d-7fb1-48e4-8e99-3316655f4d67",
                    "LayerId": "65c9ee26-0dcb-479a-bdca-388a767c07f4"
                }
            ]
        },
        {
            "id": "2fece126-5875-49c1-96f3-7e7bc92bfca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "compositeImage": {
                "id": "64f93886-bd0b-45a4-84e7-bafb95d4e8b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fece126-5875-49c1-96f3-7e7bc92bfca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2067d776-4358-4639-8927-8f855cd8589c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fece126-5875-49c1-96f3-7e7bc92bfca3",
                    "LayerId": "65c9ee26-0dcb-479a-bdca-388a767c07f4"
                }
            ]
        },
        {
            "id": "e49030e6-d040-40fe-9f5f-aeaac7b5591a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "compositeImage": {
                "id": "92384aa7-78ad-4adb-b822-12c48b4f5fce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e49030e6-d040-40fe-9f5f-aeaac7b5591a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc772e29-5592-46c3-bc92-baac5807eb8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e49030e6-d040-40fe-9f5f-aeaac7b5591a",
                    "LayerId": "65c9ee26-0dcb-479a-bdca-388a767c07f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 183,
    "layers": [
        {
            "id": "65c9ee26-0dcb-479a-bdca-388a767c07f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f68bb32c-7c5a-44b4-8464-d74b89fec967",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 352,
    "xorig": 176,
    "yorig": 91
}