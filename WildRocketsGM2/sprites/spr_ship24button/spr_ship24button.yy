{
    "id": "d74df95d-a819-48a0-92d5-c0802d5129ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship24button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ce237fb-d9af-4477-81ec-abdc4f0781ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d74df95d-a819-48a0-92d5-c0802d5129ea",
            "compositeImage": {
                "id": "bebfd73e-957c-4ed1-8795-4dae63f8654e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce237fb-d9af-4477-81ec-abdc4f0781ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eee1c2e3-7403-40aa-ad5b-fbe71c4014a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce237fb-d9af-4477-81ec-abdc4f0781ab",
                    "LayerId": "0fe4f6db-4ab5-4d9f-af81-60e7f7abd68d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "0fe4f6db-4ab5-4d9f-af81-60e7f7abd68d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d74df95d-a819-48a0-92d5-c0802d5129ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}