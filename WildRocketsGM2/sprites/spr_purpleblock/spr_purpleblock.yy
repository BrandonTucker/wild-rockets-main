{
    "id": "d9b1893a-9699-4bcd-8332-a039d9436b50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_purpleblock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73f59786-23d6-49d2-8b0f-d0e21719fad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9b1893a-9699-4bcd-8332-a039d9436b50",
            "compositeImage": {
                "id": "00040792-d114-443b-b630-09ad290e97e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73f59786-23d6-49d2-8b0f-d0e21719fad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8158d9cf-cc2f-41da-842b-0ac5239ca3af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f59786-23d6-49d2-8b0f-d0e21719fad8",
                    "LayerId": "e50199bb-46ad-473e-89c7-56302e0d7b3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e50199bb-46ad-473e-89c7-56302e0d7b3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9b1893a-9699-4bcd-8332-a039d9436b50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}