{
    "id": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grapes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac585e76-d100-4981-8e4b-811787deba08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "compositeImage": {
                "id": "d86b0c6d-769a-46e4-a12b-561233832d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac585e76-d100-4981-8e4b-811787deba08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "151000af-474d-4fc3-8280-602ae0002d75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac585e76-d100-4981-8e4b-811787deba08",
                    "LayerId": "7ca405a3-77b9-48dd-97da-83a2c10aa64d"
                }
            ]
        },
        {
            "id": "165736cf-5738-4701-9fe6-419513b69ff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "compositeImage": {
                "id": "83cebbbb-7d56-42f3-b999-1ede1e95499c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "165736cf-5738-4701-9fe6-419513b69ff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c41a4b-428f-4f6c-b826-20876d6efb49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "165736cf-5738-4701-9fe6-419513b69ff6",
                    "LayerId": "7ca405a3-77b9-48dd-97da-83a2c10aa64d"
                }
            ]
        },
        {
            "id": "052f8209-7332-4713-bf5f-152eb0d2d92b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "compositeImage": {
                "id": "99d8afbd-c7ab-423f-ad00-3d306fbc108b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "052f8209-7332-4713-bf5f-152eb0d2d92b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72e4d1d4-d4a8-4ee6-b36f-6fc901214db1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052f8209-7332-4713-bf5f-152eb0d2d92b",
                    "LayerId": "7ca405a3-77b9-48dd-97da-83a2c10aa64d"
                }
            ]
        },
        {
            "id": "5851f4df-f703-47d1-8aeb-5aae56198aa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "compositeImage": {
                "id": "2cddc173-4b71-429b-9111-aa04ace0e394",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5851f4df-f703-47d1-8aeb-5aae56198aa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ac8124d-e5d3-47ce-a61f-3e7cf60201de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5851f4df-f703-47d1-8aeb-5aae56198aa3",
                    "LayerId": "7ca405a3-77b9-48dd-97da-83a2c10aa64d"
                }
            ]
        },
        {
            "id": "d70a50f9-44f7-468b-9216-ec31bb6cbfb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "compositeImage": {
                "id": "748b45f5-901d-4a1e-bf9f-a9a7006a4346",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d70a50f9-44f7-468b-9216-ec31bb6cbfb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9142d260-9b31-487e-9831-b8df967af143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d70a50f9-44f7-468b-9216-ec31bb6cbfb8",
                    "LayerId": "7ca405a3-77b9-48dd-97da-83a2c10aa64d"
                }
            ]
        },
        {
            "id": "4236cbff-42a7-4b29-88de-18d050b78404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "compositeImage": {
                "id": "c5a73200-404b-4a49-90e1-2533d91c5a83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4236cbff-42a7-4b29-88de-18d050b78404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "842531a8-ef62-48ad-a5d5-59f58fb5f73e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4236cbff-42a7-4b29-88de-18d050b78404",
                    "LayerId": "7ca405a3-77b9-48dd-97da-83a2c10aa64d"
                }
            ]
        },
        {
            "id": "f4a2c42f-50b7-4f17-87b8-187968e4e4f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "compositeImage": {
                "id": "20bbe120-760f-48c8-93f3-a4fdcb4668b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a2c42f-50b7-4f17-87b8-187968e4e4f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cabebb2d-d54f-471b-a922-958c0ef32937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a2c42f-50b7-4f17-87b8-187968e4e4f6",
                    "LayerId": "7ca405a3-77b9-48dd-97da-83a2c10aa64d"
                }
            ]
        },
        {
            "id": "a7c4a514-e4db-4211-94d9-8471b393fd79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "compositeImage": {
                "id": "8a84cc64-56c9-4e56-97a7-f30b92039980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7c4a514-e4db-4211-94d9-8471b393fd79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff020825-b62b-48b3-8c8c-bf01e7830827",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7c4a514-e4db-4211-94d9-8471b393fd79",
                    "LayerId": "7ca405a3-77b9-48dd-97da-83a2c10aa64d"
                }
            ]
        },
        {
            "id": "98c13736-df36-44fa-bf16-bacf717c45e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "compositeImage": {
                "id": "845413a7-2b36-438b-af60-e435bf0e1072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c13736-df36-44fa-bf16-bacf717c45e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0f081af-a332-43d4-8bc5-e88bd1f016f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c13736-df36-44fa-bf16-bacf717c45e2",
                    "LayerId": "7ca405a3-77b9-48dd-97da-83a2c10aa64d"
                }
            ]
        },
        {
            "id": "33e5e5f9-5813-4864-9eab-5d0a6c00184e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "compositeImage": {
                "id": "f43f0bb8-aecf-4653-bccc-720d554ff6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e5e5f9-5813-4864-9eab-5d0a6c00184e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ecd8995-41ac-45bd-b0bd-b26fd631f8cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e5e5f9-5813-4864-9eab-5d0a6c00184e",
                    "LayerId": "7ca405a3-77b9-48dd-97da-83a2c10aa64d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "7ca405a3-77b9-48dd-97da-83a2c10aa64d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fb921c1-df3e-40c7-8d41-bef38ac7d904",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 42,
    "yorig": 38
}