{
    "id": "38248c6d-0a4a-4a6d-8298-8c272d0a54d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship1button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 13,
    "bbox_right": 88,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8062b1db-562c-41c9-af69-c58770aacc41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38248c6d-0a4a-4a6d-8298-8c272d0a54d0",
            "compositeImage": {
                "id": "56b5f079-ad5f-42af-bf44-41fe85ae0084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8062b1db-562c-41c9-af69-c58770aacc41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a4c5dcd-a26f-48e5-b47e-1342250444d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8062b1db-562c-41c9-af69-c58770aacc41",
                    "LayerId": "04d223bb-a64a-4e59-98ce-d5491b0bb4b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "04d223bb-a64a-4e59-98ce-d5491b0bb4b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38248c6d-0a4a-4a6d-8298-8c272d0a54d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}