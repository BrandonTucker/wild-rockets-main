{
    "id": "c4bea301-6b5e-4e7b-91d6-919006349300",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship4explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 24,
    "bbox_right": 63,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b1fcaaa-fc98-4c4d-8c44-4e4fa3758cf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4bea301-6b5e-4e7b-91d6-919006349300",
            "compositeImage": {
                "id": "744184c7-2a0e-4f4f-a7b2-66e72f729871",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b1fcaaa-fc98-4c4d-8c44-4e4fa3758cf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc190a4f-c006-449e-a8b5-613f630f698d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b1fcaaa-fc98-4c4d-8c44-4e4fa3758cf4",
                    "LayerId": "4502fe11-19e5-4937-a31c-fa991aa106f4"
                }
            ]
        },
        {
            "id": "9cd2b5b9-101e-4357-9d9f-7a342f904718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4bea301-6b5e-4e7b-91d6-919006349300",
            "compositeImage": {
                "id": "55cca2f6-55b2-43ce-97e1-df3cc474ae70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd2b5b9-101e-4357-9d9f-7a342f904718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a2ff35a-5527-45c4-a7c8-0fbfe9757883",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd2b5b9-101e-4357-9d9f-7a342f904718",
                    "LayerId": "4502fe11-19e5-4937-a31c-fa991aa106f4"
                }
            ]
        },
        {
            "id": "dc52cc13-cb30-4d78-93ec-c1cc7debeae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4bea301-6b5e-4e7b-91d6-919006349300",
            "compositeImage": {
                "id": "40907424-43fe-4153-85d3-e35543501fc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc52cc13-cb30-4d78-93ec-c1cc7debeae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba463b7f-fc73-4f64-ae67-6117f8ee4c62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc52cc13-cb30-4d78-93ec-c1cc7debeae3",
                    "LayerId": "4502fe11-19e5-4937-a31c-fa991aa106f4"
                }
            ]
        },
        {
            "id": "efd16084-29cc-4567-9424-8617099bee58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4bea301-6b5e-4e7b-91d6-919006349300",
            "compositeImage": {
                "id": "9a9a7052-5e10-4e3c-b837-4959d637c8c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efd16084-29cc-4567-9424-8617099bee58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1ba6dc0-735e-42e2-8e5b-72c4d22ffa69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efd16084-29cc-4567-9424-8617099bee58",
                    "LayerId": "4502fe11-19e5-4937-a31c-fa991aa106f4"
                }
            ]
        },
        {
            "id": "49c96b3e-f0b0-41fa-9b21-2336284394bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4bea301-6b5e-4e7b-91d6-919006349300",
            "compositeImage": {
                "id": "e3972eb8-d270-43ee-9dc8-f79d9bfd076a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c96b3e-f0b0-41fa-9b21-2336284394bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf92e45-f535-4a04-a2c3-303ee8bd9c94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c96b3e-f0b0-41fa-9b21-2336284394bf",
                    "LayerId": "4502fe11-19e5-4937-a31c-fa991aa106f4"
                }
            ]
        },
        {
            "id": "b0739278-bacf-42ce-8af0-c87da90fd63d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4bea301-6b5e-4e7b-91d6-919006349300",
            "compositeImage": {
                "id": "b585a114-4cb4-42f7-bb1d-9238a93b045b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0739278-bacf-42ce-8af0-c87da90fd63d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "832cad98-74ef-44f4-a97e-125fbe17c6a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0739278-bacf-42ce-8af0-c87da90fd63d",
                    "LayerId": "4502fe11-19e5-4937-a31c-fa991aa106f4"
                }
            ]
        },
        {
            "id": "71f0bd95-0b15-4623-b33b-736ebfd3eeee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4bea301-6b5e-4e7b-91d6-919006349300",
            "compositeImage": {
                "id": "671378ad-dfe5-4a07-b19a-6b5cb5fe6a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71f0bd95-0b15-4623-b33b-736ebfd3eeee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbe01100-c475-46b1-97f8-a229373329d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71f0bd95-0b15-4623-b33b-736ebfd3eeee",
                    "LayerId": "4502fe11-19e5-4937-a31c-fa991aa106f4"
                }
            ]
        },
        {
            "id": "d4623420-4a3f-4acc-b423-b213921255bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4bea301-6b5e-4e7b-91d6-919006349300",
            "compositeImage": {
                "id": "9cbc766e-ac4f-415f-a377-3971de48efe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4623420-4a3f-4acc-b423-b213921255bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e88a001-8a53-4710-81dd-dc4cd5859707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4623420-4a3f-4acc-b423-b213921255bb",
                    "LayerId": "4502fe11-19e5-4937-a31c-fa991aa106f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 186,
    "layers": [
        {
            "id": "4502fe11-19e5-4937-a31c-fa991aa106f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4bea301-6b5e-4e7b-91d6-919006349300",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 116,
    "xorig": 56,
    "yorig": 75
}