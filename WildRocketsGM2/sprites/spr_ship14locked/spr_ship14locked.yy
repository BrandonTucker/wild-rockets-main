{
    "id": "8035daf9-2885-45c5-ba7d-064165169889",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship14locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abb1fe69-5616-48c6-a33d-9e7009884f5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8035daf9-2885-45c5-ba7d-064165169889",
            "compositeImage": {
                "id": "cb5c4819-a84b-4c93-841e-0b89b3349797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abb1fe69-5616-48c6-a33d-9e7009884f5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f93b8996-3fba-476d-b4f2-9492b2453d16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abb1fe69-5616-48c6-a33d-9e7009884f5c",
                    "LayerId": "7c41e899-ba62-4478-91f4-f1f24b49af76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7c41e899-ba62-4478-91f4-f1f24b49af76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8035daf9-2885-45c5-ba7d-064165169889",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}