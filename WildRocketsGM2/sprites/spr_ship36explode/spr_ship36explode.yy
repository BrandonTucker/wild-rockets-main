{
    "id": "c66ce313-2144-4d8e-916a-01232850d0f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship36explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4a9a150-b240-4372-a9a1-9c8c117eefb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66ce313-2144-4d8e-916a-01232850d0f7",
            "compositeImage": {
                "id": "3b2cc67c-7f50-4031-8f74-9c94b3fa38c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a9a150-b240-4372-a9a1-9c8c117eefb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fd1d383-e0e8-47dc-9913-f1cf80de1a3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a9a150-b240-4372-a9a1-9c8c117eefb8",
                    "LayerId": "d41dcb8f-ef01-4e79-875b-a504121c8224"
                }
            ]
        },
        {
            "id": "a4149966-0406-4157-a4d4-23218c432f97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66ce313-2144-4d8e-916a-01232850d0f7",
            "compositeImage": {
                "id": "b07dbf8f-05b9-4220-94d4-e2ed9755245b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4149966-0406-4157-a4d4-23218c432f97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf9abf9e-c927-4594-974c-fd8811d14e89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4149966-0406-4157-a4d4-23218c432f97",
                    "LayerId": "d41dcb8f-ef01-4e79-875b-a504121c8224"
                }
            ]
        },
        {
            "id": "01d50836-9a0d-47af-a519-8048a518d0b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66ce313-2144-4d8e-916a-01232850d0f7",
            "compositeImage": {
                "id": "dfcd9a1e-22bb-4ac7-857f-f668da17e6fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01d50836-9a0d-47af-a519-8048a518d0b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e8d0c9-7f24-47d8-930c-4f802c30fa73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d50836-9a0d-47af-a519-8048a518d0b2",
                    "LayerId": "d41dcb8f-ef01-4e79-875b-a504121c8224"
                }
            ]
        },
        {
            "id": "dca9ba09-9356-4e37-8476-71db4d5a9869",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66ce313-2144-4d8e-916a-01232850d0f7",
            "compositeImage": {
                "id": "cff561e4-0a76-475a-b4e7-71da1523653d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dca9ba09-9356-4e37-8476-71db4d5a9869",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e807e84-3ee7-4c83-a1fb-a224c1b3d7b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca9ba09-9356-4e37-8476-71db4d5a9869",
                    "LayerId": "d41dcb8f-ef01-4e79-875b-a504121c8224"
                }
            ]
        },
        {
            "id": "23f8490f-6721-4d6e-b7cc-c5c6e457be70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66ce313-2144-4d8e-916a-01232850d0f7",
            "compositeImage": {
                "id": "1f176151-fe9e-4dab-a764-bafc208fc620",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23f8490f-6721-4d6e-b7cc-c5c6e457be70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdb86153-a268-43cf-bd0b-17c6bde43028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23f8490f-6721-4d6e-b7cc-c5c6e457be70",
                    "LayerId": "d41dcb8f-ef01-4e79-875b-a504121c8224"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 266,
    "layers": [
        {
            "id": "d41dcb8f-ef01-4e79-875b-a504121c8224",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c66ce313-2144-4d8e-916a-01232850d0f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 60,
    "yorig": 135
}