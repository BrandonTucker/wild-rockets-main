{
    "id": "dede4738-7320-4aec-a598-7d9528df4cdc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage9buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ffadab0-259f-437e-a393-170036a49ac5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "compositeImage": {
                "id": "0d6e740f-b728-4671-a0e7-edf533b67872",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ffadab0-259f-437e-a393-170036a49ac5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03250f4b-3729-4578-ad1e-346751cdf99c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ffadab0-259f-437e-a393-170036a49ac5",
                    "LayerId": "31849fea-7d06-4591-97d9-575cc4c20b59"
                }
            ]
        },
        {
            "id": "7ea48bbd-8b66-42ce-868b-d301e3dd862a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "compositeImage": {
                "id": "fc88e901-f2e8-4c05-82a7-9eef71b807c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ea48bbd-8b66-42ce-868b-d301e3dd862a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59f2a447-9b1a-433c-ad66-048393ee8d75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ea48bbd-8b66-42ce-868b-d301e3dd862a",
                    "LayerId": "31849fea-7d06-4591-97d9-575cc4c20b59"
                }
            ]
        },
        {
            "id": "c940b0cd-10eb-4e3c-80d5-ae140bb9a69a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "compositeImage": {
                "id": "a54dfc06-f34d-40e5-9ee7-5df73572dc1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c940b0cd-10eb-4e3c-80d5-ae140bb9a69a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3bec73b-9d68-43c3-b109-a0908b4b3b34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c940b0cd-10eb-4e3c-80d5-ae140bb9a69a",
                    "LayerId": "31849fea-7d06-4591-97d9-575cc4c20b59"
                }
            ]
        },
        {
            "id": "3f97db40-f06b-484e-bc38-61868b080253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "compositeImage": {
                "id": "53681929-c35a-4a77-91ba-29990d588620",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f97db40-f06b-484e-bc38-61868b080253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5b5eac0-5c7a-4663-aa8c-7cc7cc28c63a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f97db40-f06b-484e-bc38-61868b080253",
                    "LayerId": "31849fea-7d06-4591-97d9-575cc4c20b59"
                }
            ]
        },
        {
            "id": "31aaf73a-9ede-4720-9950-bfd020fbea18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "compositeImage": {
                "id": "e6e3a96c-9140-4fd0-a1fb-f87d1995161f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31aaf73a-9ede-4720-9950-bfd020fbea18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d091a43-866c-4633-ad91-3ceaa3803326",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31aaf73a-9ede-4720-9950-bfd020fbea18",
                    "LayerId": "31849fea-7d06-4591-97d9-575cc4c20b59"
                }
            ]
        },
        {
            "id": "b3278425-d5b0-4dda-b672-2817254d878d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "compositeImage": {
                "id": "35dddddc-78a7-48c6-9a8c-f576b79c2cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3278425-d5b0-4dda-b672-2817254d878d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b6f89cb-6d83-4c1b-9e06-49619247644b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3278425-d5b0-4dda-b672-2817254d878d",
                    "LayerId": "31849fea-7d06-4591-97d9-575cc4c20b59"
                }
            ]
        },
        {
            "id": "747debe6-265a-4f54-95b0-87f5852a0bd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "compositeImage": {
                "id": "1c4e148b-bccd-4d2c-b1b9-94e1538c7e57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "747debe6-265a-4f54-95b0-87f5852a0bd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "354e13f5-ae8e-48a4-84c5-45169f76746e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "747debe6-265a-4f54-95b0-87f5852a0bd7",
                    "LayerId": "31849fea-7d06-4591-97d9-575cc4c20b59"
                }
            ]
        },
        {
            "id": "7325e246-bc8d-4d13-aa08-e2a1765080ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "compositeImage": {
                "id": "b3c90ac5-b56a-4d27-a759-067fd7756086",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7325e246-bc8d-4d13-aa08-e2a1765080ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6dda177-03d7-4a14-8b07-3157a32e583e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7325e246-bc8d-4d13-aa08-e2a1765080ce",
                    "LayerId": "31849fea-7d06-4591-97d9-575cc4c20b59"
                }
            ]
        },
        {
            "id": "98e53854-e61b-4376-8b03-95e466e424aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "compositeImage": {
                "id": "a0e9e5a6-4179-4726-ac13-4a9a48739ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98e53854-e61b-4376-8b03-95e466e424aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd1b168f-0240-44ef-aae5-2b85b8f25056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98e53854-e61b-4376-8b03-95e466e424aa",
                    "LayerId": "31849fea-7d06-4591-97d9-575cc4c20b59"
                }
            ]
        },
        {
            "id": "303599b1-9b74-4321-9770-d3bf2a1537e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "compositeImage": {
                "id": "a8c6fe81-337e-478b-a1e7-33aa8f529a4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "303599b1-9b74-4321-9770-d3bf2a1537e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30ed025c-472a-4794-b36a-f2f3065a2a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "303599b1-9b74-4321-9770-d3bf2a1537e1",
                    "LayerId": "31849fea-7d06-4591-97d9-575cc4c20b59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "31849fea-7d06-4591-97d9-575cc4c20b59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dede4738-7320-4aec-a598-7d9528df4cdc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}