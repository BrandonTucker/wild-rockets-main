{
    "id": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challagecomplete",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 19,
    "bbox_right": 624,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6716ec76-d6e7-4a9f-b0cd-e566c99a2ec4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "b591577a-661e-407d-b94a-460838264ab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6716ec76-d6e7-4a9f-b0cd-e566c99a2ec4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c9f097e-b197-426e-bae7-2854c3bb67b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6716ec76-d6e7-4a9f-b0cd-e566c99a2ec4",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "37e040bd-5af7-4fb5-ab83-c94cc3c51b17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "bd8ffd32-40d6-4cd6-8cff-bbc61851050d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37e040bd-5af7-4fb5-ab83-c94cc3c51b17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b33483d9-936a-467a-8130-c2470404186c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37e040bd-5af7-4fb5-ab83-c94cc3c51b17",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "b834fde2-98a3-4724-9efd-83148017631c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "b38f7ff9-8305-46ab-9b9f-4abae6ca4abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b834fde2-98a3-4724-9efd-83148017631c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bc3a9f9-2820-404b-9151-3cfadf0b4e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b834fde2-98a3-4724-9efd-83148017631c",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "b03732c5-f2d4-488c-a2f1-5970b62ad5a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "a19de6cc-0bcd-43b2-88f4-0c9dc86c5465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b03732c5-f2d4-488c-a2f1-5970b62ad5a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a086853-b8a1-4b88-94e1-5dc0f476d79b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b03732c5-f2d4-488c-a2f1-5970b62ad5a7",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "ea7d80da-6acf-43e3-9c49-d4cc17092b21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "292c0df2-e144-4f61-bc84-7268d0acea76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea7d80da-6acf-43e3-9c49-d4cc17092b21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9417c4e6-78b7-4e32-80e0-2d0408b33711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea7d80da-6acf-43e3-9c49-d4cc17092b21",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "7a8e11a2-c3b4-4c87-a2f5-750c9b8030ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "4deda4b6-c05e-405a-8ceb-ff1d0b3ec9be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a8e11a2-c3b4-4c87-a2f5-750c9b8030ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c690a95-7a05-45f2-aeea-0b5e0dc66c91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a8e11a2-c3b4-4c87-a2f5-750c9b8030ed",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "72fa0475-2346-4ae1-9b66-b60c30f3f03a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "63ff3c04-25e7-4c87-8dd3-e9f5522a8275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72fa0475-2346-4ae1-9b66-b60c30f3f03a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "849fd98b-2054-4016-940b-108f46159418",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72fa0475-2346-4ae1-9b66-b60c30f3f03a",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "4dc26793-9416-4206-804e-10819a339054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "e680c4df-2dc3-4b89-82a0-b57328a258b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dc26793-9416-4206-804e-10819a339054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbb33a82-eb20-4ef2-bdb0-58d0f09cbdeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dc26793-9416-4206-804e-10819a339054",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "3bf6a209-f1e2-4ee4-9603-8bfd884972fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "d82bd779-c5fb-410e-b32e-5ca76f0b5ff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bf6a209-f1e2-4ee4-9603-8bfd884972fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "590a8c59-bacb-4ca5-8431-2d378c4d0fde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bf6a209-f1e2-4ee4-9603-8bfd884972fe",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "644d2da1-d2fa-42c2-9235-688b29f5870f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "d0581605-3c75-49c7-b5df-423d2013ab0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "644d2da1-d2fa-42c2-9235-688b29f5870f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0638086d-7861-4848-9276-bb4653ecc266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "644d2da1-d2fa-42c2-9235-688b29f5870f",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "367bdcf8-9c17-48cb-b78c-60c11455a811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "b5914faa-9877-471d-a30a-b610f165119c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "367bdcf8-9c17-48cb-b78c-60c11455a811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "831f0f8d-a6f4-47fa-837f-d6af7181a8d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "367bdcf8-9c17-48cb-b78c-60c11455a811",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "6bbfab63-c5e0-4693-82aa-1bc7db629c39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "1966cb62-c947-4ec3-acf0-626b1e638cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bbfab63-c5e0-4693-82aa-1bc7db629c39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f125bdbd-5861-4303-b930-ee182d4b5c36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bbfab63-c5e0-4693-82aa-1bc7db629c39",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "9793fb9f-380d-4530-9db1-f2458eebb705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "a92858ec-7823-401f-9770-75d30eea9fe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9793fb9f-380d-4530-9db1-f2458eebb705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93b8882b-bd84-4ef7-95fd-05b535b9ff56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9793fb9f-380d-4530-9db1-f2458eebb705",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "0e19a20c-b035-4706-9d29-65014a547b93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "3327a0a2-cdf9-41c4-a00a-a6911e19b9b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e19a20c-b035-4706-9d29-65014a547b93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14be3b55-3b35-48c6-9746-1b0704a7f78c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e19a20c-b035-4706-9d29-65014a547b93",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "393ccc24-9dfb-45ef-a3f0-5512b0e62e41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "5a7bdcef-72ed-46b7-820c-ac4297877404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "393ccc24-9dfb-45ef-a3f0-5512b0e62e41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "298a863e-568d-4dd6-93ec-992d22b1a27a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "393ccc24-9dfb-45ef-a3f0-5512b0e62e41",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "3c83f4b3-5867-4477-9758-01059dccd63a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "c673b5e1-cbe9-4458-89c4-fe190181282b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c83f4b3-5867-4477-9758-01059dccd63a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfaebcb7-571d-422a-8081-f56382b7cd16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c83f4b3-5867-4477-9758-01059dccd63a",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "168a6b8f-5c61-4013-ab2e-ceab07b25406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "dc1b40dc-bcab-457e-8a25-98fb1b8ad36e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "168a6b8f-5c61-4013-ab2e-ceab07b25406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd10006e-9484-4934-afc2-d05330491d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "168a6b8f-5c61-4013-ab2e-ceab07b25406",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        },
        {
            "id": "f8e93b42-c680-4339-8594-1f2a8a97dcf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "compositeImage": {
                "id": "52c5def4-4184-4405-8ec6-a14c7be105d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8e93b42-c680-4339-8594-1f2a8a97dcf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b56c40ca-1343-4228-abbf-0291cd989188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8e93b42-c680-4339-8594-1f2a8a97dcf4",
                    "LayerId": "977ecd68-5646-4961-9c88-0cc27bf1d653"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 63,
    "layers": [
        {
            "id": "977ecd68-5646-4961-9c88-0cc27bf1d653",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28b85c1b-79f7-4492-aa68-04dd68e194b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 31
}