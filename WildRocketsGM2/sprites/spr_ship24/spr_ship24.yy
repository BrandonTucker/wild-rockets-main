{
    "id": "c624c0e3-0b20-416a-87ab-22899a34b2f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship24",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 40,
    "bbox_right": 65,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32080469-6e30-4024-9c28-4bc7117385ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c624c0e3-0b20-416a-87ab-22899a34b2f2",
            "compositeImage": {
                "id": "5ed649ad-adfb-4106-9928-1fc6f9c66a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32080469-6e30-4024-9c28-4bc7117385ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2834dfb5-edf4-4387-a3e2-bb1f1e1087a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32080469-6e30-4024-9c28-4bc7117385ba",
                    "LayerId": "fb87ebd5-a7bc-4bfc-8103-98efddd8693f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "fb87ebd5-a7bc-4bfc-8103-98efddd8693f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c624c0e3-0b20-416a-87ab-22899a34b2f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 64
}