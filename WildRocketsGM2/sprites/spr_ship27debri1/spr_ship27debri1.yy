{
    "id": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship27debri1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 0,
    "bbox_right": 85,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fee9f91-5be2-4a59-9abe-b9fbc6c7029b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "ca8539ee-72f8-40bc-b98b-779fd3a96944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fee9f91-5be2-4a59-9abe-b9fbc6c7029b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30077edc-a492-4c80-94e8-2ab2288fada1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fee9f91-5be2-4a59-9abe-b9fbc6c7029b",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "f5125cbb-0d9a-40d2-816b-5a8a73e62d0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "b542bb5b-1047-4f43-a962-0d51d9f7cf7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5125cbb-0d9a-40d2-816b-5a8a73e62d0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5332643f-64a3-489c-aa81-8db0d1b037f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5125cbb-0d9a-40d2-816b-5a8a73e62d0f",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "e9600a5f-8b4a-47a0-bc47-567c78c4cf5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "77e89aaa-a7c1-488c-9f89-92a96df27155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9600a5f-8b4a-47a0-bc47-567c78c4cf5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4399204-623b-49d5-846f-51124096e702",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9600a5f-8b4a-47a0-bc47-567c78c4cf5f",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "caa2c282-4ae9-4928-aca3-96c80b25e385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "742d7ec6-c420-4a19-9c62-ac48e078e89d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caa2c282-4ae9-4928-aca3-96c80b25e385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ced42cc1-0891-49e7-8d82-45dd53ae9686",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caa2c282-4ae9-4928-aca3-96c80b25e385",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "81a55059-5585-4a68-8578-4be4cd4744e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "0308f7e0-716f-41fc-b466-6c621d35ec60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81a55059-5585-4a68-8578-4be4cd4744e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5d06c14-3cbf-4cce-ad87-bd9cf52aead9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81a55059-5585-4a68-8578-4be4cd4744e1",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "4ddf3e8b-02d6-4a3e-bb0c-e309ddcca3bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "d7f1a4d3-0e34-4599-8892-028ad322f62e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ddf3e8b-02d6-4a3e-bb0c-e309ddcca3bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57bb5cd5-ac71-4ee6-a45f-237386943359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ddf3e8b-02d6-4a3e-bb0c-e309ddcca3bf",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "9f63a2c5-f31c-4924-924c-ae586ca42c1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "e091be75-f06c-41ba-9091-b19547dba767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f63a2c5-f31c-4924-924c-ae586ca42c1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30d80900-2096-4b8d-86fa-3b3a5f27ce4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f63a2c5-f31c-4924-924c-ae586ca42c1a",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "69461e22-2d57-4340-b7cf-7ac44f4df890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "1748a72b-c22a-44e1-8629-b342b6b56720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69461e22-2d57-4340-b7cf-7ac44f4df890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b210cd-5cbe-466a-be6e-fc3f0004a007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69461e22-2d57-4340-b7cf-7ac44f4df890",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "575a4ea5-ee63-4332-bca6-51f5de29ba5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "08fef281-b292-4c14-9d11-01ca2f547b00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "575a4ea5-ee63-4332-bca6-51f5de29ba5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad6a40a2-1fe9-4772-b46d-83d3b09a733e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "575a4ea5-ee63-4332-bca6-51f5de29ba5f",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "95dde617-6abd-481f-b3cd-dd512952667c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "cf559f05-d2da-4c6c-b90a-a3daacf8d531",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95dde617-6abd-481f-b3cd-dd512952667c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a45a644-afbf-48b6-912b-c96a1258a908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95dde617-6abd-481f-b3cd-dd512952667c",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "5fa98c10-5e70-45d0-b0a5-e7024b94bd86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "f3b13add-b134-425e-9772-76c3626402b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa98c10-5e70-45d0-b0a5-e7024b94bd86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cacbc75-9403-4e24-8ddb-479ca4e85f08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa98c10-5e70-45d0-b0a5-e7024b94bd86",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "3c355c67-208a-4d50-bd8b-2a5d088f3fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "775499c3-b3f2-4dd8-a8ea-4f2cab8066ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c355c67-208a-4d50-bd8b-2a5d088f3fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "052a1566-bff5-45d2-8a05-5e55a660ccca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c355c67-208a-4d50-bd8b-2a5d088f3fdb",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "66f9c2c1-6899-4244-bf7c-1bfef43cfc6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "a09c2e37-9857-4bf8-86bf-d733b45c5887",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66f9c2c1-6899-4244-bf7c-1bfef43cfc6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a516f7-b3d6-4cbf-9942-166288ba9a1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66f9c2c1-6899-4244-bf7c-1bfef43cfc6c",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "0a74b867-a97f-4fd0-8f81-411ac62cab5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "dbf3ae6c-848f-4bc1-9950-afcf05ed248f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a74b867-a97f-4fd0-8f81-411ac62cab5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0940b6d-6092-4d68-8594-163497b67443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a74b867-a97f-4fd0-8f81-411ac62cab5e",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        },
        {
            "id": "eeab7145-5e09-4b4d-a765-4ba861bea398",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "compositeImage": {
                "id": "fa735bd5-2128-4801-8681-a43a0f895bd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeab7145-5e09-4b4d-a765-4ba861bea398",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96cf0a98-a9fc-45d3-a408-9378b0ec755e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeab7145-5e09-4b4d-a765-4ba861bea398",
                    "LayerId": "71ba65e5-229c-4a9a-87e2-7b0f262d9309"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 86,
    "layers": [
        {
            "id": "71ba65e5-229c-4a9a-87e2-7b0f262d9309",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7a7fbd7-8a85-4adc-9bab-21373eda0cac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 86,
    "xorig": 35,
    "yorig": 31
}