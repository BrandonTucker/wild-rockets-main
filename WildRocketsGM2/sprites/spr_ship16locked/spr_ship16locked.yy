{
    "id": "f7c35020-5903-45b8-bf07-18d381b0724a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship16locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f661e2ad-3a3c-4686-8d43-745cb8dfabdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7c35020-5903-45b8-bf07-18d381b0724a",
            "compositeImage": {
                "id": "8290b8f3-1160-479b-b536-3000cd9f2af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f661e2ad-3a3c-4686-8d43-745cb8dfabdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f3bca05-e7e9-4792-beaf-ebfc9d5bb7b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f661e2ad-3a3c-4686-8d43-745cb8dfabdf",
                    "LayerId": "fbb37081-87f9-4bbe-8552-31eb0d89b88a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "fbb37081-87f9-4bbe-8552-31eb0d89b88a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7c35020-5903-45b8-bf07-18d381b0724a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}