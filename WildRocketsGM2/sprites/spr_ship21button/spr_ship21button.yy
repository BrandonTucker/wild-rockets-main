{
    "id": "67c921f0-e72c-4677-893a-41df5d9c329d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship21button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a095c9cf-fb4f-4d08-bb1d-bb6a1856f521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67c921f0-e72c-4677-893a-41df5d9c329d",
            "compositeImage": {
                "id": "3fec8b15-381f-4573-ae89-cdcb050a882b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a095c9cf-fb4f-4d08-bb1d-bb6a1856f521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b537c93-2676-4087-86dc-e971f24b0969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a095c9cf-fb4f-4d08-bb1d-bb6a1856f521",
                    "LayerId": "a0fe1e51-671a-4158-aeeb-dfd0b5afa95f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "a0fe1e51-671a-4158-aeeb-dfd0b5afa95f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67c921f0-e72c-4677-893a-41df5d9c329d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}