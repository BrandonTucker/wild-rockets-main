{
    "id": "44a59bd5-c7d0-438a-9b59-558ea52d18c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon192",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 17,
    "bbox_right": 175,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a75ce0bd-6a78-40fc-9ae3-58291b17bf37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44a59bd5-c7d0-438a-9b59-558ea52d18c4",
            "compositeImage": {
                "id": "da80b2b9-ee17-4523-a6ef-da9fffe27482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a75ce0bd-6a78-40fc-9ae3-58291b17bf37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd6cf5fb-1e75-457b-87f9-ea7eb8c3b9bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a75ce0bd-6a78-40fc-9ae3-58291b17bf37",
                    "LayerId": "034e908c-872a-4ee8-8288-fc2512e692ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "034e908c-872a-4ee8-8288-fc2512e692ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44a59bd5-c7d0-438a-9b59-558ea52d18c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}