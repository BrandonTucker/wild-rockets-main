{
    "id": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage7buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b842f00-5ef0-4b39-9279-7147b914fca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "compositeImage": {
                "id": "097d3d04-0391-4d64-bcb7-c9a893d06381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b842f00-5ef0-4b39-9279-7147b914fca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e0dcda-4790-4734-b2e3-ae6edd631bfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b842f00-5ef0-4b39-9279-7147b914fca2",
                    "LayerId": "817db09c-03f8-4b4c-8756-11241380ec51"
                }
            ]
        },
        {
            "id": "3461c0ee-d9ef-41a5-a7b5-3b3a847f8941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "compositeImage": {
                "id": "f19dbb5b-9956-49ca-b879-d8d8e4e14063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3461c0ee-d9ef-41a5-a7b5-3b3a847f8941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf758229-66e4-4048-acd5-84b0ff820be1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3461c0ee-d9ef-41a5-a7b5-3b3a847f8941",
                    "LayerId": "817db09c-03f8-4b4c-8756-11241380ec51"
                }
            ]
        },
        {
            "id": "cb2fc7e7-18cb-469a-8b25-bfa72152228b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "compositeImage": {
                "id": "15bf476a-a1fb-4629-b9cf-9a3e760a3cec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb2fc7e7-18cb-469a-8b25-bfa72152228b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1ee3af-8205-47b0-b3dd-24f1ee6303c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb2fc7e7-18cb-469a-8b25-bfa72152228b",
                    "LayerId": "817db09c-03f8-4b4c-8756-11241380ec51"
                }
            ]
        },
        {
            "id": "3c56c8c9-cbe0-4ca8-bf9b-3fddfc9d8684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "compositeImage": {
                "id": "888552db-b866-4ce3-8643-843b1e14efbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c56c8c9-cbe0-4ca8-bf9b-3fddfc9d8684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a41cb95-859f-41df-acfb-87c9ca895468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c56c8c9-cbe0-4ca8-bf9b-3fddfc9d8684",
                    "LayerId": "817db09c-03f8-4b4c-8756-11241380ec51"
                }
            ]
        },
        {
            "id": "b6cd6fdf-de5c-4b43-8123-0e7bdebedbc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "compositeImage": {
                "id": "9aeffe19-148a-4a46-9fc4-2101e642a5ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6cd6fdf-de5c-4b43-8123-0e7bdebedbc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec9c5eb0-6389-4990-b8b1-c9d2c82d1afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6cd6fdf-de5c-4b43-8123-0e7bdebedbc6",
                    "LayerId": "817db09c-03f8-4b4c-8756-11241380ec51"
                }
            ]
        },
        {
            "id": "94b97cd6-ce7c-496d-b593-434dbd8d6ff5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "compositeImage": {
                "id": "5d20cd9f-94c1-4c1b-b1cd-d1ac9dfccc63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94b97cd6-ce7c-496d-b593-434dbd8d6ff5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af2d522-9068-49da-b4fe-4a9c9481b804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94b97cd6-ce7c-496d-b593-434dbd8d6ff5",
                    "LayerId": "817db09c-03f8-4b4c-8756-11241380ec51"
                }
            ]
        },
        {
            "id": "843150cf-d9e1-4066-bfeb-9dc6d8c001bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "compositeImage": {
                "id": "16320c76-e045-44be-aa1e-80558dbf3738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "843150cf-d9e1-4066-bfeb-9dc6d8c001bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c3b257d-92b9-4da8-a030-300ca9695c41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843150cf-d9e1-4066-bfeb-9dc6d8c001bb",
                    "LayerId": "817db09c-03f8-4b4c-8756-11241380ec51"
                }
            ]
        },
        {
            "id": "b1c965a7-4d2c-4033-84ab-a6b2f084155e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "compositeImage": {
                "id": "2803eee0-6ddc-4120-88b8-cae9ae5a8384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1c965a7-4d2c-4033-84ab-a6b2f084155e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5ec9ae9-8907-476d-998e-0ad473e9a2f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1c965a7-4d2c-4033-84ab-a6b2f084155e",
                    "LayerId": "817db09c-03f8-4b4c-8756-11241380ec51"
                }
            ]
        },
        {
            "id": "db113de3-95bb-43f8-9539-ae5f87196c2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "compositeImage": {
                "id": "f4303566-3d6a-4137-95dd-ddaee45df2ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db113de3-95bb-43f8-9539-ae5f87196c2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5cf4965-92f9-4fed-8f54-30ed34619c99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db113de3-95bb-43f8-9539-ae5f87196c2f",
                    "LayerId": "817db09c-03f8-4b4c-8756-11241380ec51"
                }
            ]
        },
        {
            "id": "5bace486-f635-4564-9513-45a350382955",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "compositeImage": {
                "id": "9032701c-8855-4827-af67-471853f9384b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bace486-f635-4564-9513-45a350382955",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7a62696-99d6-4f6e-aed7-1386afd6f17a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bace486-f635-4564-9513-45a350382955",
                    "LayerId": "817db09c-03f8-4b4c-8756-11241380ec51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "817db09c-03f8-4b4c-8756-11241380ec51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d76c4645-3449-4c83-b8c2-3c824c42e3ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}