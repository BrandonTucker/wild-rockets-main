{
    "id": "fc3606d4-3295-4f75-840e-a6f607ccbb79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship36button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a84bcd34-30fe-453c-948c-504015528851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc3606d4-3295-4f75-840e-a6f607ccbb79",
            "compositeImage": {
                "id": "adc8a4da-9eec-4cec-8bfb-2212e837c108",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a84bcd34-30fe-453c-948c-504015528851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6838e267-219d-441f-9b35-cd257641a305",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a84bcd34-30fe-453c-948c-504015528851",
                    "LayerId": "a352efc4-24f2-4e85-9b8a-2fe3c51f6b38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "a352efc4-24f2-4e85-9b8a-2fe3c51f6b38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc3606d4-3295-4f75-840e-a6f607ccbb79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}