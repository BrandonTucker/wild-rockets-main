{
    "id": "eab092fa-9b4d-4859-830e-5fd6104aa510",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite153",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 8,
    "bbox_right": 67,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebed0601-6def-493e-9439-249be9f29709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "9802af41-05bd-4a8b-84b9-58f4628504f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebed0601-6def-493e-9439-249be9f29709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bb3eae9-785f-4cf2-8ea1-d6a40e0d4fac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebed0601-6def-493e-9439-249be9f29709",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "34ebede8-1c3c-49a5-927f-be3e265fdc8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "46341a68-59c9-4026-b177-f75e49cc14c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ebede8-1c3c-49a5-927f-be3e265fdc8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6036d807-ed3e-4a91-a66a-e69a3cc9ee7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ebede8-1c3c-49a5-927f-be3e265fdc8e",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "f50fe6d3-f71e-4b7c-9ea6-79fe3c11df17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "48396246-2320-4ceb-b2fd-7764c7c6cc5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f50fe6d3-f71e-4b7c-9ea6-79fe3c11df17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35872699-b755-4176-a5a8-3f1c237b27ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f50fe6d3-f71e-4b7c-9ea6-79fe3c11df17",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "6bffe76f-38e6-4a6c-8563-35815e27c469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "7c1aa546-aac3-46d8-ae64-efadb2830f01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bffe76f-38e6-4a6c-8563-35815e27c469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f3a7adc-a009-484e-8b9c-a255dbe61279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bffe76f-38e6-4a6c-8563-35815e27c469",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "b4d47a95-c13f-4969-8000-aa6ebf6c7f97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "366c7c00-4aba-4210-a78a-5bd92cae9479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d47a95-c13f-4969-8000-aa6ebf6c7f97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf2aaa61-7144-4b15-83ae-2530901e4afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d47a95-c13f-4969-8000-aa6ebf6c7f97",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "b056bfd3-5084-4be6-8afa-f133b5b6a2f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "f5d437f9-9c64-490d-a73a-2d89486f066c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b056bfd3-5084-4be6-8afa-f133b5b6a2f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ded5287-ec3d-4913-a1b9-5c1995883e95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b056bfd3-5084-4be6-8afa-f133b5b6a2f7",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "56e5de2b-ed2b-45c7-8de9-8e9e5384b87f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "f786c381-07c6-4713-a121-5be5f68dd9c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e5de2b-ed2b-45c7-8de9-8e9e5384b87f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b1b6440-be4d-4a2a-ad3a-b93b37a7df51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e5de2b-ed2b-45c7-8de9-8e9e5384b87f",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "cced70f2-b652-4ad0-b562-6978adb1b8c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "ec9b5ed0-90e9-4ee0-822e-712fbb6f7441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cced70f2-b652-4ad0-b562-6978adb1b8c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dd41d22-05c7-40d7-be5e-7b81cec2e25c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cced70f2-b652-4ad0-b562-6978adb1b8c6",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "7109cb50-9c63-4898-849e-cc1a098f1d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "9ad964cd-fb55-485f-ab09-de1ca76021de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7109cb50-9c63-4898-849e-cc1a098f1d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e09e936-8704-44b4-8112-56b883b4dbab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7109cb50-9c63-4898-849e-cc1a098f1d5d",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "05275a64-f03a-406d-8ff6-d3cf48db4135",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "1b647488-2771-4e97-bc3b-d0e06c6eb651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05275a64-f03a-406d-8ff6-d3cf48db4135",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13229d79-c414-4550-853f-6db75d628d83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05275a64-f03a-406d-8ff6-d3cf48db4135",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "3aa5d668-8dc8-45e8-9084-8d0f22a61c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "61e30b6e-41b8-47cd-b155-ea6dc57df732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aa5d668-8dc8-45e8-9084-8d0f22a61c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58697d4b-ff5b-44d9-a8ad-678c68ed74f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aa5d668-8dc8-45e8-9084-8d0f22a61c03",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "0a133520-7ac2-4c51-9556-a75216d7d928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "b79af959-8a2a-4a1e-88e2-fbcd161b65f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a133520-7ac2-4c51-9556-a75216d7d928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa773472-98df-4477-87dd-a0fc34e9dda2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a133520-7ac2-4c51-9556-a75216d7d928",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "677f171c-4bc1-4dc4-870d-71cf407005d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "3fa61896-edaf-4aec-bde8-0e2df935911b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "677f171c-4bc1-4dc4-870d-71cf407005d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a5bf85b-de65-4856-ba24-d785f8c85b14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "677f171c-4bc1-4dc4-870d-71cf407005d6",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "59db5985-b94b-468e-bbcb-e5ae4b37bb3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "79a4abcf-c906-41d7-8c00-142c87c84835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59db5985-b94b-468e-bbcb-e5ae4b37bb3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4179fc1-ec45-4154-993c-533e29a97128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59db5985-b94b-468e-bbcb-e5ae4b37bb3d",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "9d7ca914-aa6e-4595-8089-90759f5f43e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "5a05ced6-00a6-4d94-a114-080c6a9811fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d7ca914-aa6e-4595-8089-90759f5f43e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b567c649-f19c-4add-83a5-fb6851d58f74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d7ca914-aa6e-4595-8089-90759f5f43e5",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "bbd39d31-322e-4f61-8651-acf10208eb7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "921901e6-25c8-4016-a1a7-92f9b30c04cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd39d31-322e-4f61-8651-acf10208eb7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b79070a7-b4f0-4cfb-9ad3-b9304f80c2b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd39d31-322e-4f61-8651-acf10208eb7b",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "e1d7b896-8a38-42fe-adff-7392eba48ac4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "579e7254-6e88-4503-b1d4-57389503c96b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d7b896-8a38-42fe-adff-7392eba48ac4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20327c2a-ac1a-4f1b-b688-ac0f3592cdb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d7b896-8a38-42fe-adff-7392eba48ac4",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "5be226d3-1058-44d6-9387-061434ee4c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "85aebeb1-aeac-4003-be76-3ae8cb4e7d9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5be226d3-1058-44d6-9387-061434ee4c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5423c2d-a5c2-44f6-81b6-0fb2c07f4de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5be226d3-1058-44d6-9387-061434ee4c44",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "87c342b4-1e3f-4f2d-bfab-4e4333b5b0b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "3d26cdc3-8897-40d0-ab10-6f33556ac2a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87c342b4-1e3f-4f2d-bfab-4e4333b5b0b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac97fad-9fd1-4c9e-bef1-e4aecab46e37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87c342b4-1e3f-4f2d-bfab-4e4333b5b0b5",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "059b0a5d-e8dd-40c9-b894-e1309f706c7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "09e5fd15-d70d-49c8-a43d-1ae9a228d588",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "059b0a5d-e8dd-40c9-b894-e1309f706c7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e979ab95-10d3-4d53-8aee-5f86134868d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "059b0a5d-e8dd-40c9-b894-e1309f706c7a",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "6e0ad7ad-5c59-43dc-a123-96fcf62f662e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "54206832-0e0e-478a-ba2d-e3fc5a263586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e0ad7ad-5c59-43dc-a123-96fcf62f662e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8018a60b-3fd0-4f57-97da-88e47c77e564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e0ad7ad-5c59-43dc-a123-96fcf62f662e",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "57dd7aef-8d55-4db5-9067-6a7fd11ce831",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "7edad7f4-335b-4f16-b301-287306b9e05c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57dd7aef-8d55-4db5-9067-6a7fd11ce831",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "370b2df2-f95e-40e7-850f-1b28d57a5c80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57dd7aef-8d55-4db5-9067-6a7fd11ce831",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "c0f7fa60-a954-4571-a2c3-9021bbaeffa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "e9a0155b-aa9d-4687-974b-34ea1bdc895c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0f7fa60-a954-4571-a2c3-9021bbaeffa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce1e903-1f97-4dc8-a58f-6e5ab3e69040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f7fa60-a954-4571-a2c3-9021bbaeffa9",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "2a33776c-7b22-4de3-9bb5-85a90bb04904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "7add9708-3cbd-4974-b6f5-85da3f09b82c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a33776c-7b22-4de3-9bb5-85a90bb04904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a04bee7-a790-4941-87ac-5d59cf079197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a33776c-7b22-4de3-9bb5-85a90bb04904",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "2cbe1dfa-24c7-45f3-ade8-51297afc9326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "1386a07d-5c27-45eb-8887-9ee17ebc0b91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cbe1dfa-24c7-45f3-ade8-51297afc9326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd1e8264-7bd8-4a28-896c-57a511101789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cbe1dfa-24c7-45f3-ade8-51297afc9326",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "c8987827-0d0a-4eae-aff5-028400ec3e46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "6b1b203f-ef78-48c2-bb85-0073302c1e4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8987827-0d0a-4eae-aff5-028400ec3e46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33eb7c9d-db19-452d-8bdc-50c9843e4b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8987827-0d0a-4eae-aff5-028400ec3e46",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "13ca0e82-bf77-477f-a670-eaeafbd19aa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "df12f30b-23b4-4880-a2b5-7582bca8dff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ca0e82-bf77-477f-a670-eaeafbd19aa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522d5435-aa25-44fd-9b53-890ae72ca4cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ca0e82-bf77-477f-a670-eaeafbd19aa9",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "924bc6d3-967e-435c-b288-0756483065bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "be6b73c1-0f4c-4481-b6ef-12e5fdcd63ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "924bc6d3-967e-435c-b288-0756483065bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bceb4319-b0cc-47e9-af5e-69fa2811fb22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "924bc6d3-967e-435c-b288-0756483065bb",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "845faf39-7810-4567-ae9e-747472e0058f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "6445b9de-cf4e-4864-840e-b1134e9a3fc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "845faf39-7810-4567-ae9e-747472e0058f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9387db9-69c3-4a7f-97d6-d7b4ef3f42e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "845faf39-7810-4567-ae9e-747472e0058f",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "0d684b35-5708-4ab0-90dd-ecfb1b1a333b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "bab8fc3d-fa05-46cd-ab8f-5fcd82235f74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d684b35-5708-4ab0-90dd-ecfb1b1a333b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e55daa2d-c191-4358-97bc-ac861069179f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d684b35-5708-4ab0-90dd-ecfb1b1a333b",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "711a79c5-0cfc-4770-ab06-ebf467f42343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "34e8d3b3-317d-4489-9347-834467d21fcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711a79c5-0cfc-4770-ab06-ebf467f42343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ab7698c-a9f9-4bba-b345-9fe8583ba332",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711a79c5-0cfc-4770-ab06-ebf467f42343",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "93863308-849b-4577-91e3-504ad487890a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "740aff3b-a773-4aa6-8312-5de3f670dba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93863308-849b-4577-91e3-504ad487890a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4987894-8a23-4c4b-974d-9a41a9c48a58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93863308-849b-4577-91e3-504ad487890a",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "5b03d275-b19f-4122-af40-f78f95b192cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "a18152a9-2a9a-4da1-9704-a518b35e8460",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b03d275-b19f-4122-af40-f78f95b192cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199a3248-3d59-4a77-9c47-7e9991db93df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b03d275-b19f-4122-af40-f78f95b192cf",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "2e395c97-2899-4f1e-90d4-53667883f1db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "3e72e668-5d07-4a5d-aedd-9c737c103b9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e395c97-2899-4f1e-90d4-53667883f1db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e760b5-8558-4ac6-9f7f-45eae3bad550",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e395c97-2899-4f1e-90d4-53667883f1db",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "81e6c83e-7851-4a3e-bc03-316cc7898e60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "d35d62fe-9337-48b6-81d7-826718ee0e9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e6c83e-7851-4a3e-bc03-316cc7898e60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9d19a01-eaed-4766-9b88-f08cb6fb58dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e6c83e-7851-4a3e-bc03-316cc7898e60",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "369e8c36-b261-4b71-b39a-f5d994851120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "03af8128-7caf-4867-b42b-57a0464316c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "369e8c36-b261-4b71-b39a-f5d994851120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67e3d324-a5c0-46b5-a1c0-aef2a1cd9baa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "369e8c36-b261-4b71-b39a-f5d994851120",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "2c4c48cc-447f-45f5-a0e3-2b44701497a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "bf2470b7-568c-4e59-8e92-cc806f9f104a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c4c48cc-447f-45f5-a0e3-2b44701497a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c29f7b1-7627-4a41-93d2-dcc35ad27bc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c4c48cc-447f-45f5-a0e3-2b44701497a5",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "fd3568cb-aecf-4ae5-9114-569061a25a02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "d2dce09e-4748-476d-8af2-077ed68d4ef3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd3568cb-aecf-4ae5-9114-569061a25a02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ad7a3b3-a207-4a6e-b3af-3bf8d1f26371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd3568cb-aecf-4ae5-9114-569061a25a02",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "e4990bba-5e66-45c8-b0d4-8a6ab358b10c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "3e49e3f6-11e7-43bc-843d-7f6acd136805",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4990bba-5e66-45c8-b0d4-8a6ab358b10c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051126e0-20e1-4ad4-806e-b12502a54d45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4990bba-5e66-45c8-b0d4-8a6ab358b10c",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "1b944d0c-318d-4b27-85d8-5953279f9cdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "1a0a7166-8112-4a4a-8a9c-c2f9a9fe44c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b944d0c-318d-4b27-85d8-5953279f9cdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed139588-ee14-4464-8f11-d1d2567b40ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b944d0c-318d-4b27-85d8-5953279f9cdf",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "c2cad293-6ac6-414e-b3dc-fbb0a5e7c600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "a85b98bc-f209-4b16-b399-2615cf638b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2cad293-6ac6-414e-b3dc-fbb0a5e7c600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9320d42c-ce5d-4ab3-81ee-fa47f2070ba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2cad293-6ac6-414e-b3dc-fbb0a5e7c600",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "5454323b-3c6f-4ecc-b696-91ff2f36be48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "a4cab3c0-d7c1-498c-8e9d-7b7ee45ed6d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5454323b-3c6f-4ecc-b696-91ff2f36be48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57500428-bd41-4dc3-b9d0-c4a1429299ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5454323b-3c6f-4ecc-b696-91ff2f36be48",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "b9af346c-92fb-4d62-880d-70a438928d6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "a51c24be-0048-4250-b4d7-3b2d0c9d8d3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9af346c-92fb-4d62-880d-70a438928d6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80775629-7cd3-4903-a9d6-bde845f34736",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9af346c-92fb-4d62-880d-70a438928d6e",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "c1c57f65-e9b4-4bc3-b35d-27699c20e6e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "f51838cd-fef1-4cd8-b395-8c5b5ad74284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1c57f65-e9b4-4bc3-b35d-27699c20e6e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "352825c1-87fe-4e92-808b-cff839a9c2b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1c57f65-e9b4-4bc3-b35d-27699c20e6e8",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "74d4c16e-a1ff-4c03-b157-e8bb5c3a9307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "f11edbea-637c-4aac-b2df-a04e860236f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74d4c16e-a1ff-4c03-b157-e8bb5c3a9307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74f778b1-068a-45c9-982b-ada15ff523f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74d4c16e-a1ff-4c03-b157-e8bb5c3a9307",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "b8caa241-6fc4-4032-8262-5a07d7923deb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "c5eb0741-0b72-4931-8c0b-df004a22a287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8caa241-6fc4-4032-8262-5a07d7923deb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06535965-4de7-489b-89ac-eb7e3212ae03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8caa241-6fc4-4032-8262-5a07d7923deb",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "a92faba6-33ad-4f8e-9314-19125deb01ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "b44459e0-90f0-4d7c-9525-3bc39fc0096b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a92faba6-33ad-4f8e-9314-19125deb01ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3951f15c-fd2d-4356-9c34-7ad05add18f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a92faba6-33ad-4f8e-9314-19125deb01ed",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "5a83582e-90b4-44e9-8510-fce89eb25b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "34eee239-04ad-44aa-ae89-cbc8bb41caed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a83582e-90b4-44e9-8510-fce89eb25b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df024156-81b8-4c49-bf55-6647cdc8a5ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a83582e-90b4-44e9-8510-fce89eb25b48",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "f00de1ad-7a3c-46d4-9334-b9309433634b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "c19f0b70-f644-482e-adb7-d338b5d7aa91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f00de1ad-7a3c-46d4-9334-b9309433634b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1235a19-3576-4af8-8e5f-65bc05cb9877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f00de1ad-7a3c-46d4-9334-b9309433634b",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "613dba74-865b-4a5f-9a3c-4a24fff693b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "a0f5df48-eb25-4147-ab2c-c7d24e8892f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "613dba74-865b-4a5f-9a3c-4a24fff693b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f8316f-48dd-4b5e-8128-7d9de70d379e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "613dba74-865b-4a5f-9a3c-4a24fff693b7",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "44ae4dc8-62ac-4bea-b1b3-a6097c7614c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "ab778ad1-166e-419f-9197-5c472bebccc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44ae4dc8-62ac-4bea-b1b3-a6097c7614c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b36104a-1f9b-4ff4-b087-4eb5e818871f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44ae4dc8-62ac-4bea-b1b3-a6097c7614c3",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "47acc7f2-8de9-4809-9fbf-0e29e1b50ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "8f7e208a-1ffc-48b2-867f-e0c520a054d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47acc7f2-8de9-4809-9fbf-0e29e1b50ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e4590d0-c358-4116-afaa-e4a8b605ed33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47acc7f2-8de9-4809-9fbf-0e29e1b50ffe",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "24be020c-de60-4265-8e22-157cee8a29fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "bd8448ee-cd80-40cd-8c8e-1ca01ddb0039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24be020c-de60-4265-8e22-157cee8a29fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "016c5d08-df66-4344-aa0a-649d9a767633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24be020c-de60-4265-8e22-157cee8a29fe",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "591466a9-3b1b-4d92-965b-3b89c157fa56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "45219801-2751-4c0e-92a0-29546d05b94a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "591466a9-3b1b-4d92-965b-3b89c157fa56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46cb44cf-cad4-4870-b5d2-fc2ff4513c52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "591466a9-3b1b-4d92-965b-3b89c157fa56",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "0f7c04e1-ef7c-49f6-9dae-480de847f08d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "846ce3ff-05e4-45d2-95de-8333d1f4bdc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f7c04e1-ef7c-49f6-9dae-480de847f08d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cae84e5-c1e7-4ee7-864c-37b1dde19a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f7c04e1-ef7c-49f6-9dae-480de847f08d",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "52c43966-da6b-4474-8399-9ca6dee020e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "89952ace-a101-4f4e-b69b-fc17e14695c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52c43966-da6b-4474-8399-9ca6dee020e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08ba9cb8-fd98-4fa6-9c45-c3e12749cab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52c43966-da6b-4474-8399-9ca6dee020e3",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "2de728c8-90fd-4b91-9425-277dda7fded9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "b78a029b-60a3-45d2-98a8-eaf5f1102f22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2de728c8-90fd-4b91-9425-277dda7fded9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a03a6e5-0835-4422-bd1e-79798f4a9cc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2de728c8-90fd-4b91-9425-277dda7fded9",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "1da80a57-bf90-439a-9bad-2655b13e3710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "f3208908-a0fa-4aab-bff0-5b91f72f1f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1da80a57-bf90-439a-9bad-2655b13e3710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae9a38b7-4e6b-4ca7-9827-19256d1296d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1da80a57-bf90-439a-9bad-2655b13e3710",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "1c912a7b-062e-42ba-bcbc-3a0d50f0dde7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "69a6ede2-ff3a-46b2-ae7b-32ab1531cf54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c912a7b-062e-42ba-bcbc-3a0d50f0dde7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d22c288f-67a4-453e-94ad-c93d55611d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c912a7b-062e-42ba-bcbc-3a0d50f0dde7",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "f6e7f335-e544-4274-af8c-b235a2838335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "02166741-c78d-4638-afe2-eb9356e11499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e7f335-e544-4274-af8c-b235a2838335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c246ffce-3e12-46be-90d2-0639b1f6bcbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e7f335-e544-4274-af8c-b235a2838335",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "8c41a149-93f1-410e-b9e7-347b0c5ab88c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "1be64075-c87a-4db8-bdc5-dcb35eccfa23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c41a149-93f1-410e-b9e7-347b0c5ab88c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c20e12e-921a-49bc-8825-7418a4381abb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c41a149-93f1-410e-b9e7-347b0c5ab88c",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "9972e385-a681-4a59-ab31-309f4ff1521e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "28ea848f-591e-46ef-b2ba-cc9f8ceeac17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9972e385-a681-4a59-ab31-309f4ff1521e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45274263-5bb7-4ec2-a626-c705efdbe681",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9972e385-a681-4a59-ab31-309f4ff1521e",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "bd1e5967-3cf9-428f-bb1f-303c389983b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "c5e76a65-d124-4f22-b2d1-e0fb74e68a55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd1e5967-3cf9-428f-bb1f-303c389983b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4442e3de-e3b1-4d74-87f9-beba2bc9db84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd1e5967-3cf9-428f-bb1f-303c389983b7",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "d6e48cea-8226-4ca5-8111-16807d5a6699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "7ef51ad5-9d30-40be-bbfc-1878be6db2ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e48cea-8226-4ca5-8111-16807d5a6699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3c3625e-c0d5-46b4-a8b4-f5c475734a3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e48cea-8226-4ca5-8111-16807d5a6699",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "4d86e17d-9a2c-4e11-baeb-f952e2e26310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "16fd61b4-14a8-4357-b456-af0fb160b328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d86e17d-9a2c-4e11-baeb-f952e2e26310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaa76e44-631b-4668-81ec-4de8d78ec934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d86e17d-9a2c-4e11-baeb-f952e2e26310",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "5e310bba-9914-4ef3-b181-d62cc256fbac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "81890076-2619-4fe6-b7c6-7c40b348a0dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e310bba-9914-4ef3-b181-d62cc256fbac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e274197b-2d07-4b78-9a87-beec30dfba39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e310bba-9914-4ef3-b181-d62cc256fbac",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "a42b3bdc-499b-48c1-b977-96cdfb900e09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "582f2f75-b6cd-41f0-b719-64e509232cbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a42b3bdc-499b-48c1-b977-96cdfb900e09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7745614d-3fa7-4940-a531-8eb6fdad3539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a42b3bdc-499b-48c1-b977-96cdfb900e09",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "d54f30be-21b1-49d0-a947-36544a1678ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "973a4fb0-8778-4ef0-a5c3-28746dd99b85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d54f30be-21b1-49d0-a947-36544a1678ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f142431b-ea74-4a26-a07c-e67765c0ef9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d54f30be-21b1-49d0-a947-36544a1678ee",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "cd0bbf7a-c02e-4fd3-9bae-4c3bc7e7af4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "9fc34338-e5a3-4019-a252-974b6aa79ccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd0bbf7a-c02e-4fd3-9bae-4c3bc7e7af4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df09da85-f3b8-4342-a0bb-2ab5af8a6579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd0bbf7a-c02e-4fd3-9bae-4c3bc7e7af4f",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "d2270845-65ba-4464-8f92-7d8f5784b04d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "5ac7ff3b-a655-4e61-8268-12caa6094788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2270845-65ba-4464-8f92-7d8f5784b04d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cab01724-7831-4a06-8b9d-23b5297a8ae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2270845-65ba-4464-8f92-7d8f5784b04d",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "99a3e27e-ae6d-4b47-a29f-5af78617820e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "033a3279-5bf8-4580-a7d1-c222040201d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99a3e27e-ae6d-4b47-a29f-5af78617820e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "581c87cb-9df1-4af9-8695-ddb05449fa4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99a3e27e-ae6d-4b47-a29f-5af78617820e",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "10636014-269a-4c85-9d11-39d94fc7dd34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "1b34afc0-689c-470b-8fa5-e4d7760b20d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10636014-269a-4c85-9d11-39d94fc7dd34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cf766e2-da01-4bac-9d31-09d0b05c363b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10636014-269a-4c85-9d11-39d94fc7dd34",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "aabb8cb2-2e40-4340-a591-7888bb65de6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "c8f74b9c-8e8f-46fc-80fe-00ea3aa7d615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aabb8cb2-2e40-4340-a591-7888bb65de6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97ebc870-28a5-48b8-9a99-ee2ade487e32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aabb8cb2-2e40-4340-a591-7888bb65de6d",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "bea3471c-f29b-4ed9-892a-62526ceb35c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "4287d650-4a0c-4264-acb4-f28fdffc919a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bea3471c-f29b-4ed9-892a-62526ceb35c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c30bb4ec-12b1-48a2-a5cb-24d106c1912a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bea3471c-f29b-4ed9-892a-62526ceb35c5",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "d2a7d32c-fcaf-4ca1-b3e0-5fdb86682401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "e2ddc2c5-aea3-46d1-954c-4fc3e87a7fce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2a7d32c-fcaf-4ca1-b3e0-5fdb86682401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfcad397-e241-4776-a54a-89d2435e6096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2a7d32c-fcaf-4ca1-b3e0-5fdb86682401",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "b633cfaf-ad5d-467f-b78c-4d32118491c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "30e1387b-adc1-4851-a226-2b5035a759ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b633cfaf-ad5d-467f-b78c-4d32118491c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75c42b6c-0a51-4835-86ab-81285f79506a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b633cfaf-ad5d-467f-b78c-4d32118491c7",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "6286e01b-94a4-4ce4-84aa-542308a7d9b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "b48e2099-514d-4f6a-b8f6-854411c60ed7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6286e01b-94a4-4ce4-84aa-542308a7d9b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e4c7df7-b89e-43cd-a195-f585f09a4c36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6286e01b-94a4-4ce4-84aa-542308a7d9b9",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "3d714513-f5bc-4ad2-946c-8d19eee89b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "a79b4742-d3f0-4ca0-9d23-d52b1b7fa366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d714513-f5bc-4ad2-946c-8d19eee89b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbfb6876-df81-4717-9d23-3a125ddf48f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d714513-f5bc-4ad2-946c-8d19eee89b6e",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "534704d6-f943-4af5-9e34-731a88b6ab91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "c56ccc23-c532-48fb-9047-6744fa8293fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "534704d6-f943-4af5-9e34-731a88b6ab91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1222a38d-ce46-448a-bd4d-5651c4222782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "534704d6-f943-4af5-9e34-731a88b6ab91",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "fb5525b3-4377-4354-8293-b5976644d060",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "e0fc31f6-d5c7-4ce5-84d4-54983818bb21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb5525b3-4377-4354-8293-b5976644d060",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1020391-c571-410f-9625-ab37e66cc59d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb5525b3-4377-4354-8293-b5976644d060",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "33cded30-02bb-4786-8f2a-13b4093ac975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "d7df45e1-4ff5-4d05-9a86-2855b0df8b9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33cded30-02bb-4786-8f2a-13b4093ac975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df9f9073-9a39-4b06-b61d-1d7dd62c2ac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33cded30-02bb-4786-8f2a-13b4093ac975",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "8f9d189b-06dc-4bf5-96fa-4c7250c0dd04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "429804e9-1057-4846-84e9-0c344ed5d188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f9d189b-06dc-4bf5-96fa-4c7250c0dd04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ffd80a4-5476-49b5-8be9-5e0f853bb60b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f9d189b-06dc-4bf5-96fa-4c7250c0dd04",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "665124ea-a46e-47b3-ab1b-2d436863fabe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "c2fb26f9-e80b-4815-9893-b6e16521abd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "665124ea-a46e-47b3-ab1b-2d436863fabe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a51661b-01fb-4170-b871-47ec85c60328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "665124ea-a46e-47b3-ab1b-2d436863fabe",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "7dc77b7a-648c-4fd4-8ae7-304ef2061136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "ebd5977d-623c-4bea-a74b-f703b535b95d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dc77b7a-648c-4fd4-8ae7-304ef2061136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d06f4598-131b-4d16-89fb-2b2882a373fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dc77b7a-648c-4fd4-8ae7-304ef2061136",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "758c1546-d58c-4580-be75-e0d2eff989cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "bdd15fd4-f13a-402c-9954-a434dfe0a1e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "758c1546-d58c-4580-be75-e0d2eff989cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f3ea745-468f-4cef-a6f6-5fab93bb5767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "758c1546-d58c-4580-be75-e0d2eff989cd",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "25b21ef5-1988-463d-ab50-91b31563c671",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "f1029a62-26e4-4d9c-b229-bcc35e130f0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25b21ef5-1988-463d-ab50-91b31563c671",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5fd146-6443-4821-927d-8f54d69c2403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25b21ef5-1988-463d-ab50-91b31563c671",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "d843dcff-f628-47ac-b7dd-c09262e2662d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "7c760b38-d589-48ed-a3cf-89caee4721b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d843dcff-f628-47ac-b7dd-c09262e2662d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "167654f7-ee5d-4bea-ba80-a6d3c449dd1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d843dcff-f628-47ac-b7dd-c09262e2662d",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        },
        {
            "id": "1897ddd8-8a91-4893-a7d6-1ff4dd6e5557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "compositeImage": {
                "id": "4d51544d-5e0e-4f63-9483-f6da3d1f3b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1897ddd8-8a91-4893-a7d6-1ff4dd6e5557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4d84124-499c-448d-9ce8-7662b4548e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1897ddd8-8a91-4893-a7d6-1ff4dd6e5557",
                    "LayerId": "1e38729b-d702-47e1-9c54-db2eee5c076e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1e38729b-d702-47e1-9c54-db2eee5c076e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eab092fa-9b4d-4859-830e-5fd6104aa510",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 64
}