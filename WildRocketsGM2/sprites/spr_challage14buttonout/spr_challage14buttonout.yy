{
    "id": "40201282-801b-41d1-8edf-405edcf63154",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage14buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3fcd562-d39f-4e44-895b-703aa278f01a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "compositeImage": {
                "id": "caa7d53b-c6a7-4b82-adf1-ffeef97f0bff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3fcd562-d39f-4e44-895b-703aa278f01a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "624e76a1-93fc-4633-b9c0-3aad115248e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3fcd562-d39f-4e44-895b-703aa278f01a",
                    "LayerId": "792aa352-1236-4784-91d4-1eb2cc0daf15"
                }
            ]
        },
        {
            "id": "22f971ad-7ec8-4bd7-8aca-cfda497766a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "compositeImage": {
                "id": "e1e73f25-69d2-4dc3-b8f3-5a2838901d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22f971ad-7ec8-4bd7-8aca-cfda497766a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65fc894e-972e-421a-a855-6738bae4c5c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22f971ad-7ec8-4bd7-8aca-cfda497766a1",
                    "LayerId": "792aa352-1236-4784-91d4-1eb2cc0daf15"
                }
            ]
        },
        {
            "id": "387f7295-07f5-4abf-a029-019aab624230",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "compositeImage": {
                "id": "154242d4-2195-439a-99a5-e53b82d35cd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "387f7295-07f5-4abf-a029-019aab624230",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b48a831f-428a-4694-a959-6c8f7e467100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "387f7295-07f5-4abf-a029-019aab624230",
                    "LayerId": "792aa352-1236-4784-91d4-1eb2cc0daf15"
                }
            ]
        },
        {
            "id": "956e6141-60de-4602-97f5-878a8ac3aeb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "compositeImage": {
                "id": "7d57434c-b784-42dd-a704-8769acffd395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "956e6141-60de-4602-97f5-878a8ac3aeb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8de304b-4bd2-4c84-96ca-6b85a4d549a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "956e6141-60de-4602-97f5-878a8ac3aeb6",
                    "LayerId": "792aa352-1236-4784-91d4-1eb2cc0daf15"
                }
            ]
        },
        {
            "id": "bc4e789d-6f11-499e-af29-7ccf03ea44ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "compositeImage": {
                "id": "e8fb4a0e-7952-42fc-bc96-5b3aabd3e064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc4e789d-6f11-499e-af29-7ccf03ea44ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b59eb5a-c61c-4901-ad04-3214e228c43b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc4e789d-6f11-499e-af29-7ccf03ea44ea",
                    "LayerId": "792aa352-1236-4784-91d4-1eb2cc0daf15"
                }
            ]
        },
        {
            "id": "2a38320d-5af1-4c2c-b637-f0682de662f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "compositeImage": {
                "id": "67823d76-8166-4782-978c-f549ff5455cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a38320d-5af1-4c2c-b637-f0682de662f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "063bf8c8-6f68-4e8f-84b8-c5928bfd582c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a38320d-5af1-4c2c-b637-f0682de662f8",
                    "LayerId": "792aa352-1236-4784-91d4-1eb2cc0daf15"
                }
            ]
        },
        {
            "id": "34235be0-dbc5-421f-a7d3-115ba5fef776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "compositeImage": {
                "id": "a2c5eb50-5d41-4ca8-a059-65e72ac96b1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34235be0-dbc5-421f-a7d3-115ba5fef776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f009ca07-c88e-4935-9a13-720f67590c83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34235be0-dbc5-421f-a7d3-115ba5fef776",
                    "LayerId": "792aa352-1236-4784-91d4-1eb2cc0daf15"
                }
            ]
        },
        {
            "id": "dd7e1270-9273-4f59-b9df-ecdd1d514c9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "compositeImage": {
                "id": "699cf352-6dfc-4605-991a-de603dfe80f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd7e1270-9273-4f59-b9df-ecdd1d514c9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78612ecb-f7aa-48d7-bc93-95389eb980ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd7e1270-9273-4f59-b9df-ecdd1d514c9a",
                    "LayerId": "792aa352-1236-4784-91d4-1eb2cc0daf15"
                }
            ]
        },
        {
            "id": "e8b8d74c-93fb-4e63-a7fc-03aa98fd5f9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "compositeImage": {
                "id": "741da427-0d77-4f0e-b67d-f3f7f87f537a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b8d74c-93fb-4e63-a7fc-03aa98fd5f9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecc9faa8-9729-4144-84c1-c60d1bc4e23c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b8d74c-93fb-4e63-a7fc-03aa98fd5f9c",
                    "LayerId": "792aa352-1236-4784-91d4-1eb2cc0daf15"
                }
            ]
        },
        {
            "id": "1d85c8ae-1e21-478c-85b8-4832fe109f40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "compositeImage": {
                "id": "72d8a3cb-bbe6-4cdf-b081-b4a0e42d838d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d85c8ae-1e21-478c-85b8-4832fe109f40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "819bd1f8-c5af-4e12-b3d7-5ae5c01b32e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d85c8ae-1e21-478c-85b8-4832fe109f40",
                    "LayerId": "792aa352-1236-4784-91d4-1eb2cc0daf15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "792aa352-1236-4784-91d4-1eb2cc0daf15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40201282-801b-41d1-8edf-405edcf63154",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}