{
    "id": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_amount25",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 5,
    "bbox_right": 90,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "177a1ef2-0c3c-40d6-bacd-a25935f51329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "f1aef50c-1794-459e-a393-d7e43346e19c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "177a1ef2-0c3c-40d6-bacd-a25935f51329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c46bfa8b-3830-4e29-a60b-d044c8f866c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "177a1ef2-0c3c-40d6-bacd-a25935f51329",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "8855f084-3a20-4462-9bae-5fe2c654edf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "d3b7a551-df62-4048-96d4-fb07ec521e76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8855f084-3a20-4462-9bae-5fe2c654edf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a160cb96-5f10-470a-acb2-f7c04f98b110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8855f084-3a20-4462-9bae-5fe2c654edf1",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "dcb55a63-1199-4c4b-bdf9-d737f6a7a41e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "547b9eb5-e544-49c4-8c97-59542cd34f97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcb55a63-1199-4c4b-bdf9-d737f6a7a41e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dd57e40-a624-4840-9421-669c62b3bf80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcb55a63-1199-4c4b-bdf9-d737f6a7a41e",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "8e9ae629-be72-4e47-bfc2-226203216f12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "65eb50c0-24c3-4b8b-a740-40ffe05c6048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e9ae629-be72-4e47-bfc2-226203216f12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e21cbc25-34b6-4a79-8be5-56b106785fb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e9ae629-be72-4e47-bfc2-226203216f12",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "08a283ea-cb8c-428c-8506-a95c1addcef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "d6c176df-ba79-45bf-83b2-1d41e64f9161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08a283ea-cb8c-428c-8506-a95c1addcef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "335007b3-8108-46b3-8538-77679fb551de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08a283ea-cb8c-428c-8506-a95c1addcef3",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "0b2235f8-6615-4bb9-a5c3-6ec69a81d60d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "d5444bbf-a7e8-4364-b754-274a0ec76948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b2235f8-6615-4bb9-a5c3-6ec69a81d60d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c540c5b-a2f2-430a-9c81-fcc74f245988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b2235f8-6615-4bb9-a5c3-6ec69a81d60d",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "621e5853-a583-4cb6-bab3-30b43ae6c2d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "2f532c30-39d0-4ebb-b519-78699ca698da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "621e5853-a583-4cb6-bab3-30b43ae6c2d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3712823-e9f0-45c0-81e8-8a0636e793b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "621e5853-a583-4cb6-bab3-30b43ae6c2d7",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "ff80accf-6fdc-4909-96d4-d70776aaa8a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "21a8b5fa-aa82-40f1-8b2f-5ac2503ea5cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff80accf-6fdc-4909-96d4-d70776aaa8a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5a90b32-b616-4f8e-9253-6c4ece409ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff80accf-6fdc-4909-96d4-d70776aaa8a5",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "588a1929-a539-4973-9a05-2dd710182d6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "652734dd-35c2-4ca1-9970-6156150d9af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "588a1929-a539-4973-9a05-2dd710182d6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b8d462a-120d-4cf3-96d7-dad137ac9c5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "588a1929-a539-4973-9a05-2dd710182d6a",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "ef44607c-e7eb-4c73-be34-c454ad5eef0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "e4b20d83-e85d-49fb-98eb-c6fb06a7909b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef44607c-e7eb-4c73-be34-c454ad5eef0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8abc5fd-51d3-46c9-8dc4-025c5e20f4c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef44607c-e7eb-4c73-be34-c454ad5eef0b",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "1f40bc5a-2c32-4cb0-bf99-a106908774d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "40209141-74a4-45de-b7e0-445ec01c077d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f40bc5a-2c32-4cb0-bf99-a106908774d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc020b88-ad9e-411a-88cf-81b1f04d88b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f40bc5a-2c32-4cb0-bf99-a106908774d1",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        },
        {
            "id": "c2e89c1c-2f13-466c-a47a-aab70255735e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "compositeImage": {
                "id": "15464897-36e3-4d9f-a4a0-61e13eccaede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2e89c1c-2f13-466c-a47a-aab70255735e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "786c9f77-83de-4d6c-b17b-f216f19d4795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2e89c1c-2f13-466c-a47a-aab70255735e",
                    "LayerId": "2c8d12e5-3f92-49a7-b66b-24980476ffb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "2c8d12e5-3f92-49a7-b66b-24980476ffb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0df83801-cba0-4bad-a160-2fb4c2dca6b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 49,
    "yorig": 40
}