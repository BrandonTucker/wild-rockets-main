{
    "id": "ad2912d0-68db-46e3-b7c3-69bc1327ba11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship29boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 37,
    "bbox_right": 62,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03ea0500-b598-4592-a8cd-b11c978f37f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad2912d0-68db-46e3-b7c3-69bc1327ba11",
            "compositeImage": {
                "id": "3483ff57-7682-4197-8427-1e1d940918db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ea0500-b598-4592-a8cd-b11c978f37f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34cf46d-9cbc-43a1-bb1d-a192e1404f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ea0500-b598-4592-a8cd-b11c978f37f1",
                    "LayerId": "1a838126-7264-427e-b0d1-457e98c0a90f"
                }
            ]
        },
        {
            "id": "2153a2fe-c275-47af-b01b-4843761c469c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad2912d0-68db-46e3-b7c3-69bc1327ba11",
            "compositeImage": {
                "id": "c99ab1f7-d8d4-42a5-b0a5-8d6eb547e218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2153a2fe-c275-47af-b01b-4843761c469c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1044cc17-469d-44f0-b08c-9773b1f4720e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2153a2fe-c275-47af-b01b-4843761c469c",
                    "LayerId": "1a838126-7264-427e-b0d1-457e98c0a90f"
                }
            ]
        },
        {
            "id": "51f0d35b-5cf7-4c6e-a127-ae8b02be1def",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad2912d0-68db-46e3-b7c3-69bc1327ba11",
            "compositeImage": {
                "id": "ca6255bb-c0c5-48c3-a146-1e8b8d0818b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51f0d35b-5cf7-4c6e-a127-ae8b02be1def",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "937690b1-582e-43b6-b020-c85c9fdf4e9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51f0d35b-5cf7-4c6e-a127-ae8b02be1def",
                    "LayerId": "1a838126-7264-427e-b0d1-457e98c0a90f"
                }
            ]
        },
        {
            "id": "8829d528-ffe9-41c2-afb9-7dce8f3f8e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad2912d0-68db-46e3-b7c3-69bc1327ba11",
            "compositeImage": {
                "id": "be77fec5-b32f-475a-aec5-227cbe40cc1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8829d528-ffe9-41c2-afb9-7dce8f3f8e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "528a7a74-763b-4107-b1c0-5357c1cd8580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8829d528-ffe9-41c2-afb9-7dce8f3f8e27",
                    "LayerId": "1a838126-7264-427e-b0d1-457e98c0a90f"
                }
            ]
        },
        {
            "id": "91d490d8-b54f-42c1-883a-ae8e09ebc9b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad2912d0-68db-46e3-b7c3-69bc1327ba11",
            "compositeImage": {
                "id": "8a6b1259-18f8-4f32-8cce-de78aeb58feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91d490d8-b54f-42c1-883a-ae8e09ebc9b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e09b1d1d-8560-4325-a711-8fbe68aecc69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d490d8-b54f-42c1-883a-ae8e09ebc9b6",
                    "LayerId": "1a838126-7264-427e-b0d1-457e98c0a90f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "1a838126-7264-427e-b0d1-457e98c0a90f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad2912d0-68db-46e3-b7c3-69bc1327ba11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}