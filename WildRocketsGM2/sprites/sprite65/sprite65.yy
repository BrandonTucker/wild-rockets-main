{
    "id": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite65",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 367,
    "bbox_left": 16,
    "bbox_right": 751,
    "bbox_top": 72,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c279ec4e-a7ed-4d8e-a32e-0220f8fe53b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "compositeImage": {
                "id": "363a23a3-5b32-4f31-b7c7-94303a2f28d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c279ec4e-a7ed-4d8e-a32e-0220f8fe53b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03d562bd-5e0a-4966-a520-47900fe083f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c279ec4e-a7ed-4d8e-a32e-0220f8fe53b9",
                    "LayerId": "7332b22f-0ed6-4870-9cab-e39c3c507b1b"
                }
            ]
        },
        {
            "id": "2a39d280-5ad0-4dfe-a321-877b03ae979e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "compositeImage": {
                "id": "720960d6-022d-4f62-aa20-6d81be630627",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a39d280-5ad0-4dfe-a321-877b03ae979e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08768dd4-fa6a-43cb-a205-114345c00380",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a39d280-5ad0-4dfe-a321-877b03ae979e",
                    "LayerId": "7332b22f-0ed6-4870-9cab-e39c3c507b1b"
                }
            ]
        },
        {
            "id": "9bad3aff-3073-4b4a-977d-d2c44689836c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "compositeImage": {
                "id": "6ea58d83-7938-4e13-84f4-a8e1846b56e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bad3aff-3073-4b4a-977d-d2c44689836c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4fbc560-4fd7-4d2b-bce2-2ce6ba5e96c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bad3aff-3073-4b4a-977d-d2c44689836c",
                    "LayerId": "7332b22f-0ed6-4870-9cab-e39c3c507b1b"
                }
            ]
        },
        {
            "id": "b05aa918-8c83-4ecb-95b2-efe692133cf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "compositeImage": {
                "id": "ed210871-9136-4c8b-92fa-467dc466b3e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b05aa918-8c83-4ecb-95b2-efe692133cf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f06ad1-19ce-4af2-9a5c-10ae0b37733e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b05aa918-8c83-4ecb-95b2-efe692133cf5",
                    "LayerId": "7332b22f-0ed6-4870-9cab-e39c3c507b1b"
                }
            ]
        },
        {
            "id": "e85f3f4e-c10a-497d-90bd-9393867d9efc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "compositeImage": {
                "id": "9fb425eb-c7b9-43ac-914d-b4a617dfdd24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e85f3f4e-c10a-497d-90bd-9393867d9efc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ac9f874-b9e9-47f3-b9c0-9d4b5eb93200",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e85f3f4e-c10a-497d-90bd-9393867d9efc",
                    "LayerId": "7332b22f-0ed6-4870-9cab-e39c3c507b1b"
                }
            ]
        },
        {
            "id": "47eba045-31dd-42e1-ab47-aeda6606bd1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "compositeImage": {
                "id": "4bc0929c-dd47-4915-8710-5120d4d0bbf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47eba045-31dd-42e1-ab47-aeda6606bd1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74d0aaec-2595-49c4-be67-8b039a143102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47eba045-31dd-42e1-ab47-aeda6606bd1c",
                    "LayerId": "7332b22f-0ed6-4870-9cab-e39c3c507b1b"
                }
            ]
        },
        {
            "id": "7658a873-55a9-4e90-bae5-d1755a1c479a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "compositeImage": {
                "id": "2ea82b40-7c3f-42eb-8f03-59c111028888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7658a873-55a9-4e90-bae5-d1755a1c479a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c43c693-3ea2-42f0-8f76-124f0ac49421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7658a873-55a9-4e90-bae5-d1755a1c479a",
                    "LayerId": "7332b22f-0ed6-4870-9cab-e39c3c507b1b"
                }
            ]
        },
        {
            "id": "b910e11d-a0d7-471f-b906-3000fd3915a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "compositeImage": {
                "id": "92857061-e6b4-414a-91e0-56230f802454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b910e11d-a0d7-471f-b906-3000fd3915a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb848a29-1337-457e-b88c-35144ed58f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b910e11d-a0d7-471f-b906-3000fd3915a7",
                    "LayerId": "7332b22f-0ed6-4870-9cab-e39c3c507b1b"
                }
            ]
        },
        {
            "id": "d2ac313b-95ed-43dc-8082-a13c21ff3a1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "compositeImage": {
                "id": "9b22848a-3166-48fc-8c21-02ac4321a8ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ac313b-95ed-43dc-8082-a13c21ff3a1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85bbc264-22b9-4ef3-b054-73c868286128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ac313b-95ed-43dc-8082-a13c21ff3a1f",
                    "LayerId": "7332b22f-0ed6-4870-9cab-e39c3c507b1b"
                }
            ]
        },
        {
            "id": "7e6f6d8a-6117-4ef7-a4c5-0283b7cca306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "compositeImage": {
                "id": "f8814fca-a0bd-4f21-a960-f5305a77f0a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e6f6d8a-6117-4ef7-a4c5-0283b7cca306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87de5c2-a48e-40ac-97ab-9c4c77c9d63c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e6f6d8a-6117-4ef7-a4c5-0283b7cca306",
                    "LayerId": "7332b22f-0ed6-4870-9cab-e39c3c507b1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 432,
    "layers": [
        {
            "id": "7332b22f-0ed6-4870-9cab-e39c3c507b1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54b19ecf-6ced-4452-9e2c-ad0d683f41f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 384,
    "yorig": 216
}