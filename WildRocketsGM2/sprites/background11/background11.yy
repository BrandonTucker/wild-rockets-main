{
    "id": "f3c76f19-eb79-473c-9ab2-e824021afe11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background11",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 959,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cda7a30-9c49-40a4-ba87-e5a2d8a49a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3c76f19-eb79-473c-9ab2-e824021afe11",
            "compositeImage": {
                "id": "c21b1193-5838-4a73-bcee-bf38247f5556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cda7a30-9c49-40a4-ba87-e5a2d8a49a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "151ff974-35bd-40d1-9712-c67984d0f254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cda7a30-9c49-40a4-ba87-e5a2d8a49a9b",
                    "LayerId": "68e758f1-1ccf-467b-8fca-67fb09cad508"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "68e758f1-1ccf-467b-8fca-67fb09cad508",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3c76f19-eb79-473c-9ab2-e824021afe11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}