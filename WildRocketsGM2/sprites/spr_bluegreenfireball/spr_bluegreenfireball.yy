{
    "id": "409a320a-78c9-4284-91f4-ac8c6c1636bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bluegreenfireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32a7d2ea-bddd-44d4-833e-7f16b89167e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409a320a-78c9-4284-91f4-ac8c6c1636bb",
            "compositeImage": {
                "id": "4fdb0708-de4a-47d0-826f-0155dddb76be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32a7d2ea-bddd-44d4-833e-7f16b89167e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be2995ba-0171-44b4-af41-c4c670b9fb67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32a7d2ea-bddd-44d4-833e-7f16b89167e6",
                    "LayerId": "3e428885-5f3e-4691-a284-b179912154ed"
                }
            ]
        },
        {
            "id": "291583eb-cfa3-4820-a497-835fb064863b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409a320a-78c9-4284-91f4-ac8c6c1636bb",
            "compositeImage": {
                "id": "107d05a1-f97a-4491-91f2-6b3f4db0d355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "291583eb-cfa3-4820-a497-835fb064863b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6edbaae-c877-4793-ab0c-a7856203e0cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "291583eb-cfa3-4820-a497-835fb064863b",
                    "LayerId": "3e428885-5f3e-4691-a284-b179912154ed"
                }
            ]
        },
        {
            "id": "b9a66d49-b08f-47fe-ba4d-5a361a1166e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409a320a-78c9-4284-91f4-ac8c6c1636bb",
            "compositeImage": {
                "id": "f9fae229-2c9e-46cd-adc7-556853befd8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9a66d49-b08f-47fe-ba4d-5a361a1166e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca23f40-2813-4e8f-8314-dcb46d838ae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9a66d49-b08f-47fe-ba4d-5a361a1166e4",
                    "LayerId": "3e428885-5f3e-4691-a284-b179912154ed"
                }
            ]
        },
        {
            "id": "c98857fc-00cb-440e-a163-25d8ae0c7aee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409a320a-78c9-4284-91f4-ac8c6c1636bb",
            "compositeImage": {
                "id": "d6632dc1-611c-46dd-9840-a2eef7ac162d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c98857fc-00cb-440e-a163-25d8ae0c7aee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7659def7-4cd6-47eb-8b81-428fe4293fa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c98857fc-00cb-440e-a163-25d8ae0c7aee",
                    "LayerId": "3e428885-5f3e-4691-a284-b179912154ed"
                }
            ]
        },
        {
            "id": "711404b0-8078-4ee4-beb1-46d0c275edb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "409a320a-78c9-4284-91f4-ac8c6c1636bb",
            "compositeImage": {
                "id": "7bd87082-32cf-4091-9c9e-a0d58844a278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711404b0-8078-4ee4-beb1-46d0c275edb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2243181d-ec90-42e4-9b66-e1650ae7bc00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711404b0-8078-4ee4-beb1-46d0c275edb8",
                    "LayerId": "3e428885-5f3e-4691-a284-b179912154ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3e428885-5f3e-4691-a284-b179912154ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "409a320a-78c9-4284-91f4-ac8c6c1636bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 16
}