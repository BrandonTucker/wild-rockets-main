{
    "id": "21b131d9-25b9-4634-9ebb-282825dcd604",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship20locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b859ce73-5103-47c2-b01a-025657765522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b131d9-25b9-4634-9ebb-282825dcd604",
            "compositeImage": {
                "id": "6f46ad8e-f351-4984-a97e-649cc7b33120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b859ce73-5103-47c2-b01a-025657765522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78890eed-c975-47e8-b037-7cf24cffc877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b859ce73-5103-47c2-b01a-025657765522",
                    "LayerId": "c9017f72-aed3-4820-833c-8339d503f9d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c9017f72-aed3-4820-833c-8339d503f9d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21b131d9-25b9-4634-9ebb-282825dcd604",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}