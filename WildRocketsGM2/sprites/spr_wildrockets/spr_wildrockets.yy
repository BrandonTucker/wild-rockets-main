{
    "id": "fae216a5-4a6c-4dbb-aa79-b282683b40e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wildrockets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 398,
    "bbox_left": 46,
    "bbox_right": 659,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83eda992-ecec-4357-9cb1-f8dd8b3b3a5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fae216a5-4a6c-4dbb-aa79-b282683b40e2",
            "compositeImage": {
                "id": "7793e9e5-2b3d-433f-b216-b004af56053e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83eda992-ecec-4357-9cb1-f8dd8b3b3a5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "950e26a0-a3d6-4bdf-849c-0afe21c8e97e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83eda992-ecec-4357-9cb1-f8dd8b3b3a5c",
                    "LayerId": "f0f3c9b9-0b0a-45fb-9287-1e5996b28314"
                }
            ]
        },
        {
            "id": "428bb7e0-7bcb-4872-9b28-3250211b7224",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fae216a5-4a6c-4dbb-aa79-b282683b40e2",
            "compositeImage": {
                "id": "f8ec7441-6b17-477e-a38d-22907fa2a876",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "428bb7e0-7bcb-4872-9b28-3250211b7224",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aed5894-d4c9-4c84-80c3-84c165331b04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "428bb7e0-7bcb-4872-9b28-3250211b7224",
                    "LayerId": "f0f3c9b9-0b0a-45fb-9287-1e5996b28314"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 420,
    "layers": [
        {
            "id": "f0f3c9b9-0b0a-45fb-9287-1e5996b28314",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fae216a5-4a6c-4dbb-aa79-b282683b40e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 840,
    "xorig": 332,
    "yorig": 164
}