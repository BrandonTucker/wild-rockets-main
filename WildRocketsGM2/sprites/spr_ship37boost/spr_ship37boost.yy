{
    "id": "3b5cd1f2-18df-4727-99a9-074434f11493",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship37boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 139,
    "bbox_left": 66,
    "bbox_right": 91,
    "bbox_top": 47,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c57d382a-4831-4d73-9922-d2e070d01579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5cd1f2-18df-4727-99a9-074434f11493",
            "compositeImage": {
                "id": "d42f78bf-00ea-492c-a7b3-a34b5de5ee39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c57d382a-4831-4d73-9922-d2e070d01579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66e2bb98-8d06-4da5-a416-adea399ca924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c57d382a-4831-4d73-9922-d2e070d01579",
                    "LayerId": "acab845e-b56e-49c9-9640-754cea1dbc23"
                }
            ]
        },
        {
            "id": "fce00713-97c2-4663-bea9-38e65fc0476c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5cd1f2-18df-4727-99a9-074434f11493",
            "compositeImage": {
                "id": "3376ddfd-e07f-4d56-b757-7c9b50f5d105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fce00713-97c2-4663-bea9-38e65fc0476c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87eada0-d71d-4a27-a54a-7c0ca6f94392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fce00713-97c2-4663-bea9-38e65fc0476c",
                    "LayerId": "acab845e-b56e-49c9-9640-754cea1dbc23"
                }
            ]
        },
        {
            "id": "24d55df4-24bf-4c91-9100-1c2fc12de49f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5cd1f2-18df-4727-99a9-074434f11493",
            "compositeImage": {
                "id": "417548ce-7ed2-49a9-b489-8c9cd5dfa682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24d55df4-24bf-4c91-9100-1c2fc12de49f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d69b11f-5dd5-4637-9164-2765bdfb6478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24d55df4-24bf-4c91-9100-1c2fc12de49f",
                    "LayerId": "acab845e-b56e-49c9-9640-754cea1dbc23"
                }
            ]
        },
        {
            "id": "e091946b-ed44-46ab-81c2-77d6150b6c61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5cd1f2-18df-4727-99a9-074434f11493",
            "compositeImage": {
                "id": "134faebd-3514-4755-9c16-86b6d2886f23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e091946b-ed44-46ab-81c2-77d6150b6c61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522ef8b4-cd80-4442-a314-b4bb9b16115f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e091946b-ed44-46ab-81c2-77d6150b6c61",
                    "LayerId": "acab845e-b56e-49c9-9640-754cea1dbc23"
                }
            ]
        },
        {
            "id": "bca60170-1c82-4555-83f4-49b9ebd78c2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5cd1f2-18df-4727-99a9-074434f11493",
            "compositeImage": {
                "id": "8c496562-35ef-44e1-b15c-0b658084e741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca60170-1c82-4555-83f4-49b9ebd78c2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ed5b004-91ad-427a-9fca-68b18f99e5c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca60170-1c82-4555-83f4-49b9ebd78c2f",
                    "LayerId": "acab845e-b56e-49c9-9640-754cea1dbc23"
                }
            ]
        },
        {
            "id": "fd00e5b5-3069-44c0-a2d9-5503d5988a82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5cd1f2-18df-4727-99a9-074434f11493",
            "compositeImage": {
                "id": "ef15f347-e1dd-4d7e-a66c-253b52fc96ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd00e5b5-3069-44c0-a2d9-5503d5988a82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05d83868-75db-41b7-a5bb-21dfa8d6b6da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd00e5b5-3069-44c0-a2d9-5503d5988a82",
                    "LayerId": "acab845e-b56e-49c9-9640-754cea1dbc23"
                }
            ]
        },
        {
            "id": "93db195d-8a04-4949-bd3e-d5dbbbe33dea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5cd1f2-18df-4727-99a9-074434f11493",
            "compositeImage": {
                "id": "2b110f91-009f-414b-bb54-b14d8ed27954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93db195d-8a04-4949-bd3e-d5dbbbe33dea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7297104f-253c-44db-b17c-e74d50872cf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93db195d-8a04-4949-bd3e-d5dbbbe33dea",
                    "LayerId": "acab845e-b56e-49c9-9640-754cea1dbc23"
                }
            ]
        },
        {
            "id": "d0b965e9-cf4b-4e8c-a583-87381c078985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5cd1f2-18df-4727-99a9-074434f11493",
            "compositeImage": {
                "id": "f66c43a2-9de7-41c7-ab27-eff6cb3c3215",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0b965e9-cf4b-4e8c-a583-87381c078985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43365aed-4265-4208-a67d-f65109fc8726",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0b965e9-cf4b-4e8c-a583-87381c078985",
                    "LayerId": "acab845e-b56e-49c9-9640-754cea1dbc23"
                }
            ]
        },
        {
            "id": "87b47675-2403-4334-ad6d-a2f267a60a17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5cd1f2-18df-4727-99a9-074434f11493",
            "compositeImage": {
                "id": "d770dfe4-3e2d-43c4-b6b1-9b55669b5cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b47675-2403-4334-ad6d-a2f267a60a17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a780025a-7217-4a0e-8e06-68577766bafe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b47675-2403-4334-ad6d-a2f267a60a17",
                    "LayerId": "acab845e-b56e-49c9-9640-754cea1dbc23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "acab845e-b56e-49c9-9640-754cea1dbc23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b5cd1f2-18df-4727-99a9-074434f11493",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 161,
    "xorig": 79,
    "yorig": 78
}