{
    "id": "50b35ca3-3504-4e6c-8395-581aa57ba9f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship40button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "710b774b-856c-43f0-b705-36711bb039e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50b35ca3-3504-4e6c-8395-581aa57ba9f0",
            "compositeImage": {
                "id": "6f8bddd9-1ed2-4b4c-ad7c-d3327b51fdd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "710b774b-856c-43f0-b705-36711bb039e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a84793d5-5174-483d-a7c0-6c87dc9db884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "710b774b-856c-43f0-b705-36711bb039e0",
                    "LayerId": "f6aa20cf-651d-44ca-b60b-da85a38f8269"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "f6aa20cf-651d-44ca-b60b-da85a38f8269",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50b35ca3-3504-4e6c-8395-581aa57ba9f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}