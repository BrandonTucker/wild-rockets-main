{
    "id": "e64c3597-56a3-4824-af93-46b28d79aafd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challangebutton1out",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 0,
    "bbox_right": 105,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "678f12c2-8e54-436a-bd98-5adc808a6ebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "compositeImage": {
                "id": "c6e2b2e2-615f-43e9-b603-55239b6100ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "678f12c2-8e54-436a-bd98-5adc808a6ebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7b491c9-8217-40c5-9ad7-5568dfe12b0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "678f12c2-8e54-436a-bd98-5adc808a6ebb",
                    "LayerId": "4631ad9e-d7ff-4548-9d0c-88d68e304170"
                }
            ]
        },
        {
            "id": "3216f50d-8323-4427-af59-698943eee6d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "compositeImage": {
                "id": "6e73eca8-d145-48fe-917a-9cd02a5b69c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3216f50d-8323-4427-af59-698943eee6d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31ed166f-282f-4d56-be0b-d308f03eee48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3216f50d-8323-4427-af59-698943eee6d7",
                    "LayerId": "4631ad9e-d7ff-4548-9d0c-88d68e304170"
                }
            ]
        },
        {
            "id": "d6f3fe1d-9f38-43a7-98b8-78df36953f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "compositeImage": {
                "id": "b35096b6-486c-4a11-9650-2cbabb915078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6f3fe1d-9f38-43a7-98b8-78df36953f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71de9d07-3e80-4321-9e2f-0c5c5a0c9220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6f3fe1d-9f38-43a7-98b8-78df36953f68",
                    "LayerId": "4631ad9e-d7ff-4548-9d0c-88d68e304170"
                }
            ]
        },
        {
            "id": "520c06c0-2df7-4b21-a673-4c646e3f6939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "compositeImage": {
                "id": "f9a7fa3f-8afc-43e1-b98a-344eab51b474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "520c06c0-2df7-4b21-a673-4c646e3f6939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d28daa3d-889d-412a-9db3-e131e3d7bf94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "520c06c0-2df7-4b21-a673-4c646e3f6939",
                    "LayerId": "4631ad9e-d7ff-4548-9d0c-88d68e304170"
                }
            ]
        },
        {
            "id": "141881e7-06fe-4a55-99fe-dbf4501c01f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "compositeImage": {
                "id": "095b8044-bdbf-4c2c-ae5b-2b8152eeab38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "141881e7-06fe-4a55-99fe-dbf4501c01f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bb49788-fe7d-4da6-a32a-cb4670172769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "141881e7-06fe-4a55-99fe-dbf4501c01f1",
                    "LayerId": "4631ad9e-d7ff-4548-9d0c-88d68e304170"
                }
            ]
        },
        {
            "id": "013a87a5-e369-4216-a4ef-10f14ac06ca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "compositeImage": {
                "id": "66c3db07-c56e-4ac7-9e40-abb2ab8fcde7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "013a87a5-e369-4216-a4ef-10f14ac06ca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e0bd712-2289-40cd-8e24-b54d096ab8fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "013a87a5-e369-4216-a4ef-10f14ac06ca1",
                    "LayerId": "4631ad9e-d7ff-4548-9d0c-88d68e304170"
                }
            ]
        },
        {
            "id": "b66f2128-2b4e-442a-9843-0726fa5150fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "compositeImage": {
                "id": "6200d7c2-943b-44e1-8ea4-eaf712087c98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b66f2128-2b4e-442a-9843-0726fa5150fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29cac246-761a-446e-b562-d6e418a129a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b66f2128-2b4e-442a-9843-0726fa5150fa",
                    "LayerId": "4631ad9e-d7ff-4548-9d0c-88d68e304170"
                }
            ]
        },
        {
            "id": "fcd83835-5022-4324-82d1-f6b4eaea17cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "compositeImage": {
                "id": "ea95ea31-01c5-40b2-812f-b1833372e74a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd83835-5022-4324-82d1-f6b4eaea17cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fc5f50c-4ca7-4605-b574-4ae0e922d90a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd83835-5022-4324-82d1-f6b4eaea17cb",
                    "LayerId": "4631ad9e-d7ff-4548-9d0c-88d68e304170"
                }
            ]
        },
        {
            "id": "bd020b4c-8073-4c9b-aa84-4605f1c795dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "compositeImage": {
                "id": "950bb0d9-fbfe-4178-8afe-c30f4afc276e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd020b4c-8073-4c9b-aa84-4605f1c795dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28b421fa-825d-4d72-b5d7-b0cc0779d6e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd020b4c-8073-4c9b-aa84-4605f1c795dd",
                    "LayerId": "4631ad9e-d7ff-4548-9d0c-88d68e304170"
                }
            ]
        },
        {
            "id": "929c8f10-b32a-41a2-a825-1f2b88f50f49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "compositeImage": {
                "id": "8b80e1d8-7bac-4950-be49-208d3236f2a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "929c8f10-b32a-41a2-a825-1f2b88f50f49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af88b70-105e-4f4a-b340-5d33a096e5f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "929c8f10-b32a-41a2-a825-1f2b88f50f49",
                    "LayerId": "4631ad9e-d7ff-4548-9d0c-88d68e304170"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "4631ad9e-d7ff-4548-9d0c-88d68e304170",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e64c3597-56a3-4824-af93-46b28d79aafd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 106,
    "xorig": 53,
    "yorig": 50
}