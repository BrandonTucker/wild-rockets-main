{
    "id": "de94ca94-49de-47df-809a-24334e93c83d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship30boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d031d9b-9b1b-4002-bd12-c48a4642cd53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de94ca94-49de-47df-809a-24334e93c83d",
            "compositeImage": {
                "id": "bb1a45fe-e7b1-4ce6-a98f-c5dedac98990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d031d9b-9b1b-4002-bd12-c48a4642cd53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be3062ea-c8a3-4220-ad28-81fc63935f61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d031d9b-9b1b-4002-bd12-c48a4642cd53",
                    "LayerId": "dd4cfa18-9046-4dff-89f5-19011046438f"
                }
            ]
        },
        {
            "id": "4a135450-5620-4c47-b507-df69ca143167",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de94ca94-49de-47df-809a-24334e93c83d",
            "compositeImage": {
                "id": "55fc80ce-36a6-427c-9b92-8cd2990f3162",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a135450-5620-4c47-b507-df69ca143167",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef9599cd-99b0-408e-af78-f76e2001dcfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a135450-5620-4c47-b507-df69ca143167",
                    "LayerId": "dd4cfa18-9046-4dff-89f5-19011046438f"
                }
            ]
        },
        {
            "id": "b145388f-411e-4d0d-9b78-15dfeb0b7270",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de94ca94-49de-47df-809a-24334e93c83d",
            "compositeImage": {
                "id": "2c1e5f76-4366-4072-9da3-87a0f15f7e19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b145388f-411e-4d0d-9b78-15dfeb0b7270",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47dccb75-5b48-4471-ae4b-de956a20f190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b145388f-411e-4d0d-9b78-15dfeb0b7270",
                    "LayerId": "dd4cfa18-9046-4dff-89f5-19011046438f"
                }
            ]
        },
        {
            "id": "45569217-2bdd-4358-87a3-6bdb57f13de6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de94ca94-49de-47df-809a-24334e93c83d",
            "compositeImage": {
                "id": "437b1d85-c220-4cb7-a045-c09d0d757dc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45569217-2bdd-4358-87a3-6bdb57f13de6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "249175ae-daad-4d6d-8da8-dd73676a54f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45569217-2bdd-4358-87a3-6bdb57f13de6",
                    "LayerId": "dd4cfa18-9046-4dff-89f5-19011046438f"
                }
            ]
        },
        {
            "id": "a5ff8913-c4f7-47a7-9193-1a44fc0ca34d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de94ca94-49de-47df-809a-24334e93c83d",
            "compositeImage": {
                "id": "af71f2c5-9343-4fae-a878-c8d8e7690082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ff8913-c4f7-47a7-9193-1a44fc0ca34d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7fae1c1-2bbc-4b84-813f-1c624371e9b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ff8913-c4f7-47a7-9193-1a44fc0ca34d",
                    "LayerId": "dd4cfa18-9046-4dff-89f5-19011046438f"
                }
            ]
        },
        {
            "id": "2a5ed356-0f08-4c5c-81ce-75aa605818e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de94ca94-49de-47df-809a-24334e93c83d",
            "compositeImage": {
                "id": "e2cc1deb-acf5-484c-a089-a3233ff51003",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a5ed356-0f08-4c5c-81ce-75aa605818e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16b89e8c-3e8c-4789-a945-7506140574ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a5ed356-0f08-4c5c-81ce-75aa605818e0",
                    "LayerId": "dd4cfa18-9046-4dff-89f5-19011046438f"
                }
            ]
        },
        {
            "id": "c230e272-a9d0-494f-b7cb-d13f3247364e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de94ca94-49de-47df-809a-24334e93c83d",
            "compositeImage": {
                "id": "dacd84d8-10b9-4ee1-b74e-85d523a87914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c230e272-a9d0-494f-b7cb-d13f3247364e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fc01262-e112-42df-bb1a-4618a38eb65d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c230e272-a9d0-494f-b7cb-d13f3247364e",
                    "LayerId": "dd4cfa18-9046-4dff-89f5-19011046438f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "dd4cfa18-9046-4dff-89f5-19011046438f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de94ca94-49de-47df-809a-24334e93c83d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 50
}