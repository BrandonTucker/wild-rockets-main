{
    "id": "fde030e1-8fdc-4b1c-90ef-495592f89bf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blueexplosion1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 9,
    "bbox_right": 77,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d5cfeeb-a872-47cb-bbae-a91b3171f9cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde030e1-8fdc-4b1c-90ef-495592f89bf3",
            "compositeImage": {
                "id": "4422b21d-a475-4d51-8e35-5af9fae05c8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d5cfeeb-a872-47cb-bbae-a91b3171f9cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c8a5b8c-ea8f-44ef-9efd-54e9c1a7e20a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d5cfeeb-a872-47cb-bbae-a91b3171f9cf",
                    "LayerId": "3728bbcb-846a-4556-a43a-453be3eebd25"
                }
            ]
        },
        {
            "id": "d1cb6853-9b16-48ed-8fd5-6f18a9870d32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde030e1-8fdc-4b1c-90ef-495592f89bf3",
            "compositeImage": {
                "id": "42d8a361-5dbf-4526-94b4-984e5571bfb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1cb6853-9b16-48ed-8fd5-6f18a9870d32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77fd33c1-9d1a-43da-94cb-1b73987d1708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1cb6853-9b16-48ed-8fd5-6f18a9870d32",
                    "LayerId": "3728bbcb-846a-4556-a43a-453be3eebd25"
                }
            ]
        },
        {
            "id": "c4b22b88-0dd7-4c6a-ae72-7a99a4b8b586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde030e1-8fdc-4b1c-90ef-495592f89bf3",
            "compositeImage": {
                "id": "51e22435-c7e7-4d8f-aeaa-8f139959b01c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b22b88-0dd7-4c6a-ae72-7a99a4b8b586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cbb9a1e-483f-45a3-874e-684077c7d8f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b22b88-0dd7-4c6a-ae72-7a99a4b8b586",
                    "LayerId": "3728bbcb-846a-4556-a43a-453be3eebd25"
                }
            ]
        },
        {
            "id": "6eaf967b-2c24-4fe5-acf6-fb25310350d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde030e1-8fdc-4b1c-90ef-495592f89bf3",
            "compositeImage": {
                "id": "b4d5bb40-6c6c-4f0b-a302-86f7fa79adf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eaf967b-2c24-4fe5-acf6-fb25310350d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7611ce3-5e03-41d6-9313-2adb336b781b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eaf967b-2c24-4fe5-acf6-fb25310350d5",
                    "LayerId": "3728bbcb-846a-4556-a43a-453be3eebd25"
                }
            ]
        },
        {
            "id": "d4612952-e813-418d-b4f0-ab62ba6f9367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde030e1-8fdc-4b1c-90ef-495592f89bf3",
            "compositeImage": {
                "id": "ec5630b5-00d5-4c83-9ada-ec43c5e226b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4612952-e813-418d-b4f0-ab62ba6f9367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a880460-c8d2-4363-8602-b61cda44e600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4612952-e813-418d-b4f0-ab62ba6f9367",
                    "LayerId": "3728bbcb-846a-4556-a43a-453be3eebd25"
                }
            ]
        },
        {
            "id": "be5a4692-4998-4e76-83cd-ce219222fad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde030e1-8fdc-4b1c-90ef-495592f89bf3",
            "compositeImage": {
                "id": "8561a834-6db8-42a2-9e65-0b6bfa41d701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be5a4692-4998-4e76-83cd-ce219222fad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "722e74f3-3604-4e98-ac80-414938e66d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be5a4692-4998-4e76-83cd-ce219222fad6",
                    "LayerId": "3728bbcb-846a-4556-a43a-453be3eebd25"
                }
            ]
        },
        {
            "id": "7d45ed86-5d1b-4df7-99fc-53e193626a1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde030e1-8fdc-4b1c-90ef-495592f89bf3",
            "compositeImage": {
                "id": "a2e8d447-e0d3-49c4-8cfa-04651ee5f642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d45ed86-5d1b-4df7-99fc-53e193626a1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b094f333-de92-410f-9efb-f691fcdfaa6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d45ed86-5d1b-4df7-99fc-53e193626a1b",
                    "LayerId": "3728bbcb-846a-4556-a43a-453be3eebd25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "3728bbcb-846a-4556-a43a-453be3eebd25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fde030e1-8fdc-4b1c-90ef-495592f89bf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}