{
    "id": "207aa4f7-a084-4c8b-87a9-542cf143e753",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background9",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10064ba2-2ec3-4e4e-9167-8d6bb8a9ee45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "207aa4f7-a084-4c8b-87a9-542cf143e753",
            "compositeImage": {
                "id": "9ce4de58-7d80-4108-9820-2a7b1e0e2039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10064ba2-2ec3-4e4e-9167-8d6bb8a9ee45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8264d05f-3e1e-4e4b-8942-9b2e8a2315fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10064ba2-2ec3-4e4e-9167-8d6bb8a9ee45",
                    "LayerId": "ecb1bc41-3c84-444b-83b6-562754e9d668"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "ecb1bc41-3c84-444b-83b6-562754e9d668",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "207aa4f7-a084-4c8b-87a9-542cf143e753",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 0,
    "yorig": 0
}