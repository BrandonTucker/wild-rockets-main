{
    "id": "3348fcd8-18ff-4c92-87ed-ecba25f7e144",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startbutton2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 23,
    "bbox_right": 106,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99c57f62-d511-44f2-82e2-7230a8471450",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3348fcd8-18ff-4c92-87ed-ecba25f7e144",
            "compositeImage": {
                "id": "a2cff19c-dbe3-4f3b-961c-558346522d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99c57f62-d511-44f2-82e2-7230a8471450",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2df8223-a142-41c7-a993-41513f0c821f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99c57f62-d511-44f2-82e2-7230a8471450",
                    "LayerId": "0d8e7eda-89ef-4e4e-817d-dd33b8e1abc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 124,
    "layers": [
        {
            "id": "0d8e7eda-89ef-4e4e-817d-dd33b8e1abc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3348fcd8-18ff-4c92-87ed-ecba25f7e144",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 127,
    "xorig": 63,
    "yorig": 62
}