{
    "id": "d859ace7-b20b-467c-b759-5730db396f98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship26debri2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e191866-5cdc-494b-896f-5849d7bbefaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "41e5eda3-6e01-4dc4-9638-94486c4b8ae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e191866-5cdc-494b-896f-5849d7bbefaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a89e36cc-0d59-4149-9c40-4c7aa66387f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e191866-5cdc-494b-896f-5849d7bbefaa",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "8bb1fa96-e588-460c-8a60-361a30798176",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "afe72567-06d3-43fc-bc1e-60a312876c30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb1fa96-e588-460c-8a60-361a30798176",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16c52167-76cd-4431-9845-313c12e6ae2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb1fa96-e588-460c-8a60-361a30798176",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "4c71c966-7930-431e-99d8-5c36297e5a33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "d6cc42b9-aad9-47d4-92a1-4c324f315867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c71c966-7930-431e-99d8-5c36297e5a33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d37ba27f-1ceb-479a-9e15-9d61b0bba642",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c71c966-7930-431e-99d8-5c36297e5a33",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "cd52e4c5-95cf-42a9-a581-b18711e33110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "beea10f6-6fde-4db6-bee0-79d44e86d2c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd52e4c5-95cf-42a9-a581-b18711e33110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b40fad37-bdcc-45a8-8352-010528bf97dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd52e4c5-95cf-42a9-a581-b18711e33110",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "2aed94a7-97f1-49ef-8853-d1f61af9891e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "109d5f50-f797-4ac6-9df5-116c47708dfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aed94a7-97f1-49ef-8853-d1f61af9891e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "008196a0-0ddf-46d8-be16-6a3580f0b811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aed94a7-97f1-49ef-8853-d1f61af9891e",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "431ccc9c-bec8-445a-b501-0064de215ba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "95c78806-ee2a-4ee8-9155-c424a9f0fd9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "431ccc9c-bec8-445a-b501-0064de215ba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3fb76f0-812c-44c4-8aac-7df9a4efa5b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "431ccc9c-bec8-445a-b501-0064de215ba2",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "2aa09a1d-faab-4d0b-afd4-ea47c65a8b4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "02173f9e-982b-4ea8-893d-060b07236760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa09a1d-faab-4d0b-afd4-ea47c65a8b4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbf59807-7211-4ed5-879e-c65dd82d04ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa09a1d-faab-4d0b-afd4-ea47c65a8b4f",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "8fb4274e-107c-490a-8763-7eab7647b340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "94aef1f5-5332-4cb4-bce9-b574b911b639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fb4274e-107c-490a-8763-7eab7647b340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3271deb0-a8a7-4cbf-bd36-2e28e6f23498",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fb4274e-107c-490a-8763-7eab7647b340",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "82d67d59-4a93-475b-8552-b6a2291421e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "9411098c-e0bf-4b2c-a28c-7a27b238995b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82d67d59-4a93-475b-8552-b6a2291421e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e067b141-6399-4f62-9156-4ef1f93aca87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82d67d59-4a93-475b-8552-b6a2291421e1",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "52f3478e-08a9-4cc7-a3ed-3ccab7ca76cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "2436257e-d353-4dc8-84af-97cd2b82d758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52f3478e-08a9-4cc7-a3ed-3ccab7ca76cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae93cefa-cc99-4cba-8ce8-ecef1b3c0cef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52f3478e-08a9-4cc7-a3ed-3ccab7ca76cb",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "b31c855f-5e62-42b3-a00b-00870143d90c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "6537fb8c-1c69-4469-8c6d-df337903993c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b31c855f-5e62-42b3-a00b-00870143d90c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfa323f2-5c68-49cc-ae2d-2646c96576b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b31c855f-5e62-42b3-a00b-00870143d90c",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "35defc6e-5001-44ed-9b5a-d7468d937dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "7eed5a2e-fe45-42bc-9f83-9dbed5aa1ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35defc6e-5001-44ed-9b5a-d7468d937dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b142f70f-1b8b-4b81-b4a8-eeee46db4d1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35defc6e-5001-44ed-9b5a-d7468d937dc7",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "ebcf0d63-72a4-4c22-97af-3e3d0b839a3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "8a003328-d557-4887-b276-c7a0cab4564d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebcf0d63-72a4-4c22-97af-3e3d0b839a3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e0fa468-2fdf-48e4-8385-736767ac3e8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebcf0d63-72a4-4c22-97af-3e3d0b839a3b",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "673e2d6a-184e-471d-b03e-d73cd2b53dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "cb815750-26dd-4554-92f6-774c6199d568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "673e2d6a-184e-471d-b03e-d73cd2b53dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73ab695-696d-47a6-b610-c2e7cd7f3a62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "673e2d6a-184e-471d-b03e-d73cd2b53dc2",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        },
        {
            "id": "f2820dcf-74f7-4fe3-9dea-f84c18ba7058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "compositeImage": {
                "id": "ee45e8a0-4615-4470-8bcf-37fef8a14e8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2820dcf-74f7-4fe3-9dea-f84c18ba7058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92c0f677-bb91-44d0-a914-d0e428d6cc26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2820dcf-74f7-4fe3-9dea-f84c18ba7058",
                    "LayerId": "325e4ad4-da33-4d75-b1a8-33a66f37802e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "325e4ad4-da33-4d75-b1a8-33a66f37802e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d859ace7-b20b-467c-b759-5730db396f98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 53,
    "yorig": 40
}