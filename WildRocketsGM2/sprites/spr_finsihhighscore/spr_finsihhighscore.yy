{
    "id": "87f61f77-134a-44f5-9819-bad6e67c5abd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_finsihhighscore",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1924c44-9999-4e65-99da-bc1f3da5d566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87f61f77-134a-44f5-9819-bad6e67c5abd",
            "compositeImage": {
                "id": "cd67128f-96ba-474b-b69b-91725e924cae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1924c44-9999-4e65-99da-bc1f3da5d566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b39cbc30-3e2a-48cd-8d36-d1173177f671",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1924c44-9999-4e65-99da-bc1f3da5d566",
                    "LayerId": "0768670c-5502-47ed-870d-e613ee4f30a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0768670c-5502-47ed-870d-e613ee4f30a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87f61f77-134a-44f5-9819-bad6e67c5abd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}