{
    "id": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lifebutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 68,
    "bbox_left": -15,
    "bbox_right": 71,
    "bbox_top": -11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9be6b3e0-999c-4412-9b34-c516dee11d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "compositeImage": {
                "id": "38998e11-7953-49ef-94cf-0a3c27a2aeb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9be6b3e0-999c-4412-9b34-c516dee11d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e130684-96be-4fbc-93c9-7a9ceaa84994",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9be6b3e0-999c-4412-9b34-c516dee11d5d",
                    "LayerId": "aa9477da-b24a-48c5-aec3-bb07502098db"
                }
            ]
        },
        {
            "id": "5f0dc7fb-636f-41f9-9d32-273bcff6d209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "compositeImage": {
                "id": "cb4b09ac-5899-405d-894c-629668faddc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f0dc7fb-636f-41f9-9d32-273bcff6d209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb5c15ff-08cb-4f45-b3cb-02be5f8bc4e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f0dc7fb-636f-41f9-9d32-273bcff6d209",
                    "LayerId": "aa9477da-b24a-48c5-aec3-bb07502098db"
                }
            ]
        },
        {
            "id": "6f91e7de-80d7-42c7-b290-7cd1d9645300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "compositeImage": {
                "id": "b612e6ea-f1ab-4942-8382-5fcdb8749290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f91e7de-80d7-42c7-b290-7cd1d9645300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc03967f-60ab-4ba3-abbf-90ecaacd7b60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f91e7de-80d7-42c7-b290-7cd1d9645300",
                    "LayerId": "aa9477da-b24a-48c5-aec3-bb07502098db"
                }
            ]
        },
        {
            "id": "ef3e6602-6928-41fe-81c4-ea0a590aee6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "compositeImage": {
                "id": "0c2f09fc-fb6b-45e0-8534-7c4fcf6086ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef3e6602-6928-41fe-81c4-ea0a590aee6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31602f24-dcf4-4ef5-a91a-9e5ae167f2da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef3e6602-6928-41fe-81c4-ea0a590aee6f",
                    "LayerId": "aa9477da-b24a-48c5-aec3-bb07502098db"
                }
            ]
        },
        {
            "id": "7aec6c90-1ae7-4459-956c-3e5e7b9c31fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "compositeImage": {
                "id": "a12cd78b-5c90-46fc-ad1f-e516bb837cfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aec6c90-1ae7-4459-956c-3e5e7b9c31fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35a5932b-ec8b-478d-834c-02c010cbb2a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aec6c90-1ae7-4459-956c-3e5e7b9c31fd",
                    "LayerId": "aa9477da-b24a-48c5-aec3-bb07502098db"
                }
            ]
        },
        {
            "id": "10b6540d-5984-4b4a-ae4a-3e0133510166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "compositeImage": {
                "id": "f8147af0-a7a0-4c92-8414-1c025acef046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10b6540d-5984-4b4a-ae4a-3e0133510166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be53129a-3854-40e6-9d89-bd5ca5eadacd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10b6540d-5984-4b4a-ae4a-3e0133510166",
                    "LayerId": "aa9477da-b24a-48c5-aec3-bb07502098db"
                }
            ]
        },
        {
            "id": "707a6780-b5f7-4f12-b93b-ae857b2fbae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "compositeImage": {
                "id": "eedfd8e7-1ad4-4f95-b899-21538e8287c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "707a6780-b5f7-4f12-b93b-ae857b2fbae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31cea5c9-a51b-4f40-a6e0-900ecf108811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "707a6780-b5f7-4f12-b93b-ae857b2fbae3",
                    "LayerId": "aa9477da-b24a-48c5-aec3-bb07502098db"
                }
            ]
        },
        {
            "id": "8bd04ebd-8172-4bf5-bebf-6919d5df4c7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "compositeImage": {
                "id": "70bba306-9a97-42e4-9752-9e538e1a5f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bd04ebd-8172-4bf5-bebf-6919d5df4c7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b114729-57e4-4968-bdcb-5d39051d9e0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bd04ebd-8172-4bf5-bebf-6919d5df4c7c",
                    "LayerId": "aa9477da-b24a-48c5-aec3-bb07502098db"
                }
            ]
        },
        {
            "id": "1cd366e5-7c78-42a5-84c8-e6904efe558b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "compositeImage": {
                "id": "c9efab52-8fd2-4065-b52c-cf463d7f2d2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd366e5-7c78-42a5-84c8-e6904efe558b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb1c0f9-912f-42a7-8e89-29bc4c69cd3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd366e5-7c78-42a5-84c8-e6904efe558b",
                    "LayerId": "aa9477da-b24a-48c5-aec3-bb07502098db"
                }
            ]
        },
        {
            "id": "73442d92-7ab8-408b-8a08-3eeb152a94e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "compositeImage": {
                "id": "664f963f-5613-48a4-aa5c-51bb9512e335",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73442d92-7ab8-408b-8a08-3eeb152a94e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bec55421-51b8-4907-8f4a-9c617c329bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73442d92-7ab8-408b-8a08-3eeb152a94e6",
                    "LayerId": "aa9477da-b24a-48c5-aec3-bb07502098db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "aa9477da-b24a-48c5-aec3-bb07502098db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9cab2bb-683c-4762-aa70-761ad1ce8e2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": 28,
    "yorig": 28
}