{
    "id": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange30buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08141d6d-da93-478f-8bcd-aaa6762ca672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "compositeImage": {
                "id": "07d241dd-9f2d-4ee2-818d-b6c8fa847735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08141d6d-da93-478f-8bcd-aaa6762ca672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e26dfca0-a6c2-4363-abd2-0e31d47c1458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08141d6d-da93-478f-8bcd-aaa6762ca672",
                    "LayerId": "27d0f2aa-77a6-4a76-8320-22c719606f38"
                }
            ]
        },
        {
            "id": "c15821e8-e619-4cc8-81ca-2f049c1ea0e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "compositeImage": {
                "id": "b30960b7-9fb5-4570-826a-63639b10803f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c15821e8-e619-4cc8-81ca-2f049c1ea0e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b77f042-194d-4bc1-8511-587d08aed708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c15821e8-e619-4cc8-81ca-2f049c1ea0e7",
                    "LayerId": "27d0f2aa-77a6-4a76-8320-22c719606f38"
                }
            ]
        },
        {
            "id": "17e993cb-f405-4853-b399-19be4055b262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "compositeImage": {
                "id": "47b7522d-14dc-4bee-8412-36b33c9d14f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e993cb-f405-4853-b399-19be4055b262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92ccf0c8-73b6-4788-996f-83a95e5f8216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e993cb-f405-4853-b399-19be4055b262",
                    "LayerId": "27d0f2aa-77a6-4a76-8320-22c719606f38"
                }
            ]
        },
        {
            "id": "bfe1dc9d-8fbe-4f3e-b458-5ca85f5c7c2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "compositeImage": {
                "id": "83c2fc79-239f-48ad-9fbb-07b5646bb767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe1dc9d-8fbe-4f3e-b458-5ca85f5c7c2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a101609-4d35-401f-8504-ade3c8770f61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe1dc9d-8fbe-4f3e-b458-5ca85f5c7c2f",
                    "LayerId": "27d0f2aa-77a6-4a76-8320-22c719606f38"
                }
            ]
        },
        {
            "id": "ea5ccc6c-d712-4ab0-9eb2-863fa71fd5df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "compositeImage": {
                "id": "67d2b4b9-78ca-4565-ac95-6fac36d425dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea5ccc6c-d712-4ab0-9eb2-863fa71fd5df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14bf46c2-3a26-4cc1-ad65-24491cb11969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea5ccc6c-d712-4ab0-9eb2-863fa71fd5df",
                    "LayerId": "27d0f2aa-77a6-4a76-8320-22c719606f38"
                }
            ]
        },
        {
            "id": "a46ca6fd-928d-4375-b9cc-a75ba9762d19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "compositeImage": {
                "id": "918270aa-6c81-45fd-a026-c57ef14a0c79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a46ca6fd-928d-4375-b9cc-a75ba9762d19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e87354d-10fc-43e8-8e17-838169c9cd07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a46ca6fd-928d-4375-b9cc-a75ba9762d19",
                    "LayerId": "27d0f2aa-77a6-4a76-8320-22c719606f38"
                }
            ]
        },
        {
            "id": "d2197764-c6e8-48a6-9244-fa354ee51474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "compositeImage": {
                "id": "64b2d22b-f186-408c-aaed-571a7a584b9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2197764-c6e8-48a6-9244-fa354ee51474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9497c782-38b1-4774-b8a0-b692b24745ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2197764-c6e8-48a6-9244-fa354ee51474",
                    "LayerId": "27d0f2aa-77a6-4a76-8320-22c719606f38"
                }
            ]
        },
        {
            "id": "88a61246-12b9-4b4c-bbcb-ecfff119ac37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "compositeImage": {
                "id": "8114006d-ff04-4004-a944-4e388caf4246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88a61246-12b9-4b4c-bbcb-ecfff119ac37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8674a949-38ef-49e3-bb5c-8323fed6b853",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88a61246-12b9-4b4c-bbcb-ecfff119ac37",
                    "LayerId": "27d0f2aa-77a6-4a76-8320-22c719606f38"
                }
            ]
        },
        {
            "id": "8cd12ab1-dfcd-42e6-addf-df08519dd6f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "compositeImage": {
                "id": "39699745-5a6c-43dc-b0b5-a16ef9288b5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd12ab1-dfcd-42e6-addf-df08519dd6f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f666b68-53df-43de-ae91-6a3725751e65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd12ab1-dfcd-42e6-addf-df08519dd6f0",
                    "LayerId": "27d0f2aa-77a6-4a76-8320-22c719606f38"
                }
            ]
        },
        {
            "id": "8c491383-21c0-40ca-ad00-d4b0ac7231d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "compositeImage": {
                "id": "a9575212-d9aa-4f60-aa6d-1a270ed3596c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c491383-21c0-40ca-ad00-d4b0ac7231d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "766e98be-af94-43d2-a6e8-f6f5a5c35b38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c491383-21c0-40ca-ad00-d4b0ac7231d3",
                    "LayerId": "27d0f2aa-77a6-4a76-8320-22c719606f38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "27d0f2aa-77a6-4a76-8320-22c719606f38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a2e8f6e-5d3d-47f6-a247-21802a918cc1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}