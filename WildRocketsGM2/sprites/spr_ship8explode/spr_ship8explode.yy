{
    "id": "e153e280-135a-41b1-bca0-4efa4f1f1bf7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship8explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 140,
    "bbox_left": 1,
    "bbox_right": 130,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df15210a-61b5-4197-8863-b0f1db05b1b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e153e280-135a-41b1-bca0-4efa4f1f1bf7",
            "compositeImage": {
                "id": "66f36d19-5dee-41f2-9e26-dd659c9a108a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df15210a-61b5-4197-8863-b0f1db05b1b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "691d4b25-be16-4014-9236-e5e4df7e3b65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df15210a-61b5-4197-8863-b0f1db05b1b6",
                    "LayerId": "fd8c3f7f-afeb-4c0b-83b2-d9004bdeb476"
                }
            ]
        },
        {
            "id": "9b22f1e2-8b93-4fe2-8ca1-c2fa73249ba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e153e280-135a-41b1-bca0-4efa4f1f1bf7",
            "compositeImage": {
                "id": "b02704a2-1623-43ea-b3e8-e6e9adefb395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b22f1e2-8b93-4fe2-8ca1-c2fa73249ba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e2ff804-9787-46b6-b18c-ab97304fb7ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b22f1e2-8b93-4fe2-8ca1-c2fa73249ba5",
                    "LayerId": "fd8c3f7f-afeb-4c0b-83b2-d9004bdeb476"
                }
            ]
        },
        {
            "id": "27cae019-d362-4553-84cd-2996a08d06b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e153e280-135a-41b1-bca0-4efa4f1f1bf7",
            "compositeImage": {
                "id": "d26a2d28-2864-42c7-b041-a7e6746065c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27cae019-d362-4553-84cd-2996a08d06b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f930e861-ffbd-47ef-87b6-50c9e07189b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27cae019-d362-4553-84cd-2996a08d06b0",
                    "LayerId": "fd8c3f7f-afeb-4c0b-83b2-d9004bdeb476"
                }
            ]
        },
        {
            "id": "66daecdb-b145-4f57-8e0b-0c5395da2bc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e153e280-135a-41b1-bca0-4efa4f1f1bf7",
            "compositeImage": {
                "id": "f933e23e-842f-4236-bd70-fc4f621bb687",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66daecdb-b145-4f57-8e0b-0c5395da2bc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "370357dc-d045-4ee8-95bd-61f863221a85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66daecdb-b145-4f57-8e0b-0c5395da2bc1",
                    "LayerId": "fd8c3f7f-afeb-4c0b-83b2-d9004bdeb476"
                }
            ]
        },
        {
            "id": "ff24bed0-b4af-427f-b256-7d56c8d26690",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e153e280-135a-41b1-bca0-4efa4f1f1bf7",
            "compositeImage": {
                "id": "b881ca51-eab0-4749-857e-91e3b8c45b2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff24bed0-b4af-427f-b256-7d56c8d26690",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0c947ef-041e-4212-a93c-b614fd3c11bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff24bed0-b4af-427f-b256-7d56c8d26690",
                    "LayerId": "fd8c3f7f-afeb-4c0b-83b2-d9004bdeb476"
                }
            ]
        },
        {
            "id": "5963f3aa-89e5-4423-8e37-e4721f17dca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e153e280-135a-41b1-bca0-4efa4f1f1bf7",
            "compositeImage": {
                "id": "a6e70382-4319-47e6-9b39-f636b268c59b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5963f3aa-89e5-4423-8e37-e4721f17dca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eaaa84f-2767-4c62-b8c5-41f3ebb83933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5963f3aa-89e5-4423-8e37-e4721f17dca3",
                    "LayerId": "fd8c3f7f-afeb-4c0b-83b2-d9004bdeb476"
                }
            ]
        },
        {
            "id": "c6d9b2f7-96a5-40fa-bd78-6534e8095518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e153e280-135a-41b1-bca0-4efa4f1f1bf7",
            "compositeImage": {
                "id": "46be8e89-023d-4131-9085-51e01e567e52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d9b2f7-96a5-40fa-bd78-6534e8095518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47315e6f-f361-40a7-89cd-c982607e8b56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d9b2f7-96a5-40fa-bd78-6534e8095518",
                    "LayerId": "fd8c3f7f-afeb-4c0b-83b2-d9004bdeb476"
                }
            ]
        },
        {
            "id": "d5fca9fd-baa5-42ad-8b35-68e7dc6861cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e153e280-135a-41b1-bca0-4efa4f1f1bf7",
            "compositeImage": {
                "id": "0db2c727-e29f-43f8-850f-5bae6be897e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5fca9fd-baa5-42ad-8b35-68e7dc6861cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f728ee1-a44d-44c4-aa1c-46337bb4f8ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5fca9fd-baa5-42ad-8b35-68e7dc6861cf",
                    "LayerId": "fd8c3f7f-afeb-4c0b-83b2-d9004bdeb476"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "fd8c3f7f-afeb-4c0b-83b2-d9004bdeb476",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e153e280-135a-41b1-bca0-4efa4f1f1bf7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 67,
    "yorig": 48
}