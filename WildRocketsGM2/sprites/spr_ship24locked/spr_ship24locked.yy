{
    "id": "67e6f697-50d3-4ea9-9bc8-fad547f0c206",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship24locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5df96530-1b52-44a3-b589-e05af8f68cc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67e6f697-50d3-4ea9-9bc8-fad547f0c206",
            "compositeImage": {
                "id": "f5bf6975-c109-4664-a6f3-89bd9b788680",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5df96530-1b52-44a3-b589-e05af8f68cc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f20b905c-bbe4-4708-8eb7-636d8ad04ec0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5df96530-1b52-44a3-b589-e05af8f68cc7",
                    "LayerId": "2a2ac87f-1abc-4539-aedb-b7dfd6b7c417"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "2a2ac87f-1abc-4539-aedb-b7dfd6b7c417",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67e6f697-50d3-4ea9-9bc8-fad547f0c206",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}