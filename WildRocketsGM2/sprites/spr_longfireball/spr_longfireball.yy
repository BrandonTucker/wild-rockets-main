{
    "id": "bd986112-5c21-4a62-8d47-0ffaa8a9543c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_longfireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 34,
    "bbox_right": 64,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38bb7a70-d648-4058-9b20-402583214ad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd986112-5c21-4a62-8d47-0ffaa8a9543c",
            "compositeImage": {
                "id": "ffc65312-9547-453e-9fcb-d1363a8a9b8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38bb7a70-d648-4058-9b20-402583214ad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eecd17f9-0934-4440-8974-cd10abda45f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38bb7a70-d648-4058-9b20-402583214ad5",
                    "LayerId": "622ff741-17f0-4ead-8afa-59862933fa00"
                }
            ]
        },
        {
            "id": "95baca71-4f87-4a02-9b0a-8fc4cf123943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd986112-5c21-4a62-8d47-0ffaa8a9543c",
            "compositeImage": {
                "id": "1661a730-c872-4e4c-906c-155a594ed06d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95baca71-4f87-4a02-9b0a-8fc4cf123943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6f4140-87c6-4174-bc05-c42e49c51d78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95baca71-4f87-4a02-9b0a-8fc4cf123943",
                    "LayerId": "622ff741-17f0-4ead-8afa-59862933fa00"
                }
            ]
        },
        {
            "id": "aa5e35a5-caf2-4ec0-9fba-44566ef53b9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd986112-5c21-4a62-8d47-0ffaa8a9543c",
            "compositeImage": {
                "id": "7751e636-1657-487d-8b5b-02360c09d421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa5e35a5-caf2-4ec0-9fba-44566ef53b9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e871354-fdd2-4871-9b14-40821e2bbda9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa5e35a5-caf2-4ec0-9fba-44566ef53b9d",
                    "LayerId": "622ff741-17f0-4ead-8afa-59862933fa00"
                }
            ]
        },
        {
            "id": "8ea35528-6334-470f-aa09-8586197c0701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd986112-5c21-4a62-8d47-0ffaa8a9543c",
            "compositeImage": {
                "id": "b5716a14-5202-4c84-abee-09c58b9cbbf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ea35528-6334-470f-aa09-8586197c0701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc06f0bf-6590-4acc-9d1e-d56a1dec4deb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ea35528-6334-470f-aa09-8586197c0701",
                    "LayerId": "622ff741-17f0-4ead-8afa-59862933fa00"
                }
            ]
        },
        {
            "id": "d239d43a-85f3-4482-9eba-850c5813ae71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd986112-5c21-4a62-8d47-0ffaa8a9543c",
            "compositeImage": {
                "id": "a33c9fe4-f95d-4079-aa05-2c94c9a91b48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d239d43a-85f3-4482-9eba-850c5813ae71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5663d854-ba87-42f1-9b1a-16469959f097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d239d43a-85f3-4482-9eba-850c5813ae71",
                    "LayerId": "622ff741-17f0-4ead-8afa-59862933fa00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "622ff741-17f0-4ead-8afa-59862933fa00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd986112-5c21-4a62-8d47-0ffaa8a9543c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 47,
    "yorig": 22
}