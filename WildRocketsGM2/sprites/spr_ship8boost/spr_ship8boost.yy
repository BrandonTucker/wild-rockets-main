{
    "id": "e1914a70-9dea-439c-ad6b-9ba5656bda46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship8boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 55,
    "bbox_right": 80,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a33902f1-9d46-4ae7-a6f3-c1d744a41150",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1914a70-9dea-439c-ad6b-9ba5656bda46",
            "compositeImage": {
                "id": "a504b42b-f95f-459f-b261-0fa778d165c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a33902f1-9d46-4ae7-a6f3-c1d744a41150",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "261a8963-8daa-4678-8500-2a9e76d9ea27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a33902f1-9d46-4ae7-a6f3-c1d744a41150",
                    "LayerId": "d002fe80-a56e-4107-929e-41645663694a"
                }
            ]
        },
        {
            "id": "f5047cbf-4a6f-4e64-8e48-63123a1a1bd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1914a70-9dea-439c-ad6b-9ba5656bda46",
            "compositeImage": {
                "id": "7bd56758-435f-4a55-bd2a-375b67dd2edc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5047cbf-4a6f-4e64-8e48-63123a1a1bd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff932b1-3e7c-4521-b329-4a9a8330e2e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5047cbf-4a6f-4e64-8e48-63123a1a1bd9",
                    "LayerId": "d002fe80-a56e-4107-929e-41645663694a"
                }
            ]
        },
        {
            "id": "46d02a48-612c-43ef-8391-1849b6790e46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1914a70-9dea-439c-ad6b-9ba5656bda46",
            "compositeImage": {
                "id": "30b5f1e5-943d-4421-b4f3-e57339534d0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d02a48-612c-43ef-8391-1849b6790e46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb25bdce-369f-4776-b3de-e288f224b166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d02a48-612c-43ef-8391-1849b6790e46",
                    "LayerId": "d002fe80-a56e-4107-929e-41645663694a"
                }
            ]
        },
        {
            "id": "c6a91082-7ab1-4156-a34f-955c06b72cd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1914a70-9dea-439c-ad6b-9ba5656bda46",
            "compositeImage": {
                "id": "a41794d7-8e9b-4f0f-8bf3-50ebbd0db20e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6a91082-7ab1-4156-a34f-955c06b72cd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e7a524d-06fe-4e6d-b62f-603c1adbdfd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6a91082-7ab1-4156-a34f-955c06b72cd4",
                    "LayerId": "d002fe80-a56e-4107-929e-41645663694a"
                }
            ]
        },
        {
            "id": "eb9690a7-4972-4fc7-9add-92292737a095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1914a70-9dea-439c-ad6b-9ba5656bda46",
            "compositeImage": {
                "id": "c9e1600f-7d04-4c46-bc7a-459c1c4063da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb9690a7-4972-4fc7-9add-92292737a095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85530ee0-aae2-4776-9ae3-bee219c8c1c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb9690a7-4972-4fc7-9add-92292737a095",
                    "LayerId": "d002fe80-a56e-4107-929e-41645663694a"
                }
            ]
        },
        {
            "id": "2c811c6c-7561-481f-9217-052eb1a71030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1914a70-9dea-439c-ad6b-9ba5656bda46",
            "compositeImage": {
                "id": "703a2819-63b3-409b-912c-0d39d0a1e57e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c811c6c-7561-481f-9217-052eb1a71030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd613ea-76ed-480d-a1e8-10d211c70333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c811c6c-7561-481f-9217-052eb1a71030",
                    "LayerId": "d002fe80-a56e-4107-929e-41645663694a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "d002fe80-a56e-4107-929e-41645663694a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1914a70-9dea-439c-ad6b-9ba5656bda46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 67,
    "yorig": 48
}