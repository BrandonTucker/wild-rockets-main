{
    "id": "162591cd-c4f4-455a-83fe-e8023d278525",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship4locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68656bce-b204-468e-8f70-92d79592d9d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "162591cd-c4f4-455a-83fe-e8023d278525",
            "compositeImage": {
                "id": "737697ed-843a-49b2-a7a6-3f051149b759",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68656bce-b204-468e-8f70-92d79592d9d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d58681d3-1190-48fa-955e-90d8964ff298",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68656bce-b204-468e-8f70-92d79592d9d6",
                    "LayerId": "39e43e74-dfbb-47c8-8975-a218337b1acb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "39e43e74-dfbb-47c8-8975-a218337b1acb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "162591cd-c4f4-455a-83fe-e8023d278525",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}