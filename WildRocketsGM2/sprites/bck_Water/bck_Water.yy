{
    "id": "3c59527e-f4e3-4162-814e-56e08cfb04f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bck_Water",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c183dae-1213-4ab0-8f72-8fc0172a1dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c59527e-f4e3-4162-814e-56e08cfb04f2",
            "compositeImage": {
                "id": "eba0bc81-7df0-466f-920b-8613feb9fa2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c183dae-1213-4ab0-8f72-8fc0172a1dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df8dc08f-70af-44d0-8e3f-8df201a6367b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c183dae-1213-4ab0-8f72-8fc0172a1dd5",
                    "LayerId": "6dd71787-c695-48a7-b550-f4973ad4d4ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6dd71787-c695-48a7-b550-f4973ad4d4ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c59527e-f4e3-4162-814e-56e08cfb04f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}