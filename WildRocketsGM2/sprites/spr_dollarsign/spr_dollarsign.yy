{
    "id": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dollarsign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 10,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90d1a117-8e14-436d-a56e-2e5de1a50862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "87f2f9c4-5e05-4678-a7a9-93ff4d028c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90d1a117-8e14-436d-a56e-2e5de1a50862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d53c582-b311-4097-af93-f7fe00bbeb8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90d1a117-8e14-436d-a56e-2e5de1a50862",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "d74faaf0-c5d5-4723-bc10-efb68ce17007",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "3e5dd4d4-1347-4a29-8f28-895890eddeb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d74faaf0-c5d5-4723-bc10-efb68ce17007",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eacb8c92-53f1-4711-9a7f-f991c1f999a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d74faaf0-c5d5-4723-bc10-efb68ce17007",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "e6cb6a6a-a632-4210-b73f-ae43654a97cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "c2ae25f1-5cd7-4d42-9360-2d3ce1abb8e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6cb6a6a-a632-4210-b73f-ae43654a97cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "194f3c57-d855-433b-a1b2-5677c2ed8bff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6cb6a6a-a632-4210-b73f-ae43654a97cb",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "6ff6238e-45de-433a-a575-ce9b6ad45d34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "f32e5ec0-0aae-4ee6-9df6-b11c661fac0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff6238e-45de-433a-a575-ce9b6ad45d34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "241e7755-2818-4c01-918f-8ce8d1f7b2d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff6238e-45de-433a-a575-ce9b6ad45d34",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "7f7b7249-64b6-412a-b02a-e6d57dcb79db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "410831e9-0f6b-433c-8790-e0cd97f2c5f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f7b7249-64b6-412a-b02a-e6d57dcb79db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "158c2736-61af-41f4-987f-56826782e4d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f7b7249-64b6-412a-b02a-e6d57dcb79db",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "cc982ab9-3acf-401a-93ff-a73f44a4382a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "4481f81a-b04e-4937-87e8-0dcba9387f3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc982ab9-3acf-401a-93ff-a73f44a4382a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93d4c6d3-b054-46bd-90b6-64c3df4d3367",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc982ab9-3acf-401a-93ff-a73f44a4382a",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "75759473-3b76-41d8-8c2b-d90aa9175047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "21804058-86a3-4523-89f5-636d348394ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75759473-3b76-41d8-8c2b-d90aa9175047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2929962d-772a-4082-abf5-29b08575e713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75759473-3b76-41d8-8c2b-d90aa9175047",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "766a40a1-9d12-426d-ad53-cf086bc8e6b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "bc812218-b18a-4afa-afa9-870b4aaa9f7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "766a40a1-9d12-426d-ad53-cf086bc8e6b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16c3292b-b9be-4092-af91-32e44337a7d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "766a40a1-9d12-426d-ad53-cf086bc8e6b4",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "a33bd830-c42d-4ac4-bebd-3b679a8ce0c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "a235a4f9-b04b-45e5-bf56-b6c80db12797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a33bd830-c42d-4ac4-bebd-3b679a8ce0c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79bfc29a-e0c6-4367-bbfe-15194c99c08b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a33bd830-c42d-4ac4-bebd-3b679a8ce0c6",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "11075832-57cd-4b4e-a970-223ea8db93a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "0e476aa8-8650-4ef3-af26-13a567032e7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11075832-57cd-4b4e-a970-223ea8db93a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "addb9b0d-aa8f-43d6-8aad-c0d16cfca834",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11075832-57cd-4b4e-a970-223ea8db93a3",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "735244e7-f5b1-4048-b18f-24e645cb83af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "9a2072c7-a2bd-4555-b750-b02256c731ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "735244e7-f5b1-4048-b18f-24e645cb83af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "850fe54a-293f-40cd-a21a-5d09aeb52984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "735244e7-f5b1-4048-b18f-24e645cb83af",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "206bec46-6df4-46a3-96f4-06d7751e144f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "9f841c0b-bd6c-49db-8af1-ed27ad00b9c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "206bec46-6df4-46a3-96f4-06d7751e144f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6c4a207-332f-40ee-a979-edd889d58e95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "206bec46-6df4-46a3-96f4-06d7751e144f",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "02558a70-3244-423e-bbe9-6393e700a62f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "0a809cdd-ae4a-428f-ba50-3c406916220d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02558a70-3244-423e-bbe9-6393e700a62f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a86b21da-bcf9-4df2-b5ca-359727a18f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02558a70-3244-423e-bbe9-6393e700a62f",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "9f8f2e7f-dc86-4d2d-875f-cc728f1e6782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "3a0e8938-d466-4dda-b8d0-5004ea73f32c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f8f2e7f-dc86-4d2d-875f-cc728f1e6782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54ab3697-5489-4dcd-aa0a-4e2bd009099a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f8f2e7f-dc86-4d2d-875f-cc728f1e6782",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "dabd7f62-0c53-47cc-abd0-88a50b303e8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "22155ac2-7935-476a-a0fb-0cee27485045",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dabd7f62-0c53-47cc-abd0-88a50b303e8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1975765d-5beb-4fb3-9d97-13f5bd6af262",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dabd7f62-0c53-47cc-abd0-88a50b303e8d",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "90361f97-60cd-4c8d-99a9-7a21da2f2075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "0414393b-6947-40c3-a6f7-81308ddcc71d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90361f97-60cd-4c8d-99a9-7a21da2f2075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "787e7d76-545a-4187-ab92-b5d232fc3eb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90361f97-60cd-4c8d-99a9-7a21da2f2075",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "06969fe2-623c-42da-8164-d57700f95466",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "2614e572-eb9e-48e1-81df-a60f2f976af3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06969fe2-623c-42da-8164-d57700f95466",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c53fd6f-3fcb-4b7b-8d31-180bf30b2302",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06969fe2-623c-42da-8164-d57700f95466",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "c0252ad5-ba11-404c-926f-dfbbff9e963f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "7cb06b91-7b02-4d25-94d9-c4b9c9ddc2b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0252ad5-ba11-404c-926f-dfbbff9e963f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6037b72e-3eb7-4475-94b7-5c6a52611e00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0252ad5-ba11-404c-926f-dfbbff9e963f",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "9ade3dfc-21d1-4011-9e2b-5d91edea458f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "03773027-c23a-47fc-8eb2-01f24aaf6ee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ade3dfc-21d1-4011-9e2b-5d91edea458f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b78fb8b1-fab0-41ab-a903-81164b9fe70f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ade3dfc-21d1-4011-9e2b-5d91edea458f",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        },
        {
            "id": "a34e2d7d-ff19-4a88-925d-1f8e6e93c982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "compositeImage": {
                "id": "0e4cf3f1-3cf9-44e0-bda3-e535be67659a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a34e2d7d-ff19-4a88-925d-1f8e6e93c982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75f908e0-f69a-426f-a02b-567f46932ec2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a34e2d7d-ff19-4a88-925d-1f8e6e93c982",
                    "LayerId": "27b180e6-d667-4662-aced-63d4e4cd3e8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "27b180e6-d667-4662-aced-63d4e4cd3e8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db3cb0eb-b301-4f81-9599-6044ba5c0fca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}