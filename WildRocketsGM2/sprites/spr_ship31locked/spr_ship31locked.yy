{
    "id": "1f801a27-4613-4779-9e69-0b066d1ace37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship31locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f1ba366-9a37-4248-93cc-daeda9de9dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f801a27-4613-4779-9e69-0b066d1ace37",
            "compositeImage": {
                "id": "b44607b7-c9b6-4868-ac17-93fb1b691e9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f1ba366-9a37-4248-93cc-daeda9de9dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ad5839-b3e9-4c9e-912c-0bd2ac93d649",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f1ba366-9a37-4248-93cc-daeda9de9dbc",
                    "LayerId": "4e9338fd-3620-48c2-946f-e7dc0b6a7ece"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4e9338fd-3620-48c2-946f-e7dc0b6a7ece",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f801a27-4613-4779-9e69-0b066d1ace37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}