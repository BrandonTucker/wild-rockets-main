{
    "id": "49b31bd6-2729-4efd-8b76-22172f79fb4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship5explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 28,
    "bbox_right": 54,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9843adb-d128-4369-a95e-a5bc3ded3aeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49b31bd6-2729-4efd-8b76-22172f79fb4f",
            "compositeImage": {
                "id": "b3411f52-3e54-437d-867c-d94b8b82a682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9843adb-d128-4369-a95e-a5bc3ded3aeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaff0ea4-728d-4937-b3f0-0ea1039e39f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9843adb-d128-4369-a95e-a5bc3ded3aeb",
                    "LayerId": "8db0a5bf-a10b-4b71-bfd5-be7230c8c30b"
                }
            ]
        },
        {
            "id": "72e98bf6-fe2a-43ef-9279-06f22d250281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49b31bd6-2729-4efd-8b76-22172f79fb4f",
            "compositeImage": {
                "id": "2dd68a5b-9c6a-4f1a-95de-9482092990f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72e98bf6-fe2a-43ef-9279-06f22d250281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1322620-998f-410e-aa8a-57115b10e97f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72e98bf6-fe2a-43ef-9279-06f22d250281",
                    "LayerId": "8db0a5bf-a10b-4b71-bfd5-be7230c8c30b"
                }
            ]
        },
        {
            "id": "c5615ea8-28a6-4702-9db6-ca500f7c6c88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49b31bd6-2729-4efd-8b76-22172f79fb4f",
            "compositeImage": {
                "id": "07b11c7e-879a-464e-a8a6-550d739fc728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5615ea8-28a6-4702-9db6-ca500f7c6c88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1dee402-a408-4c0b-8c0d-23e13d0dccaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5615ea8-28a6-4702-9db6-ca500f7c6c88",
                    "LayerId": "8db0a5bf-a10b-4b71-bfd5-be7230c8c30b"
                }
            ]
        },
        {
            "id": "2cee4ab2-3a30-45f0-875e-13858ec4fb59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49b31bd6-2729-4efd-8b76-22172f79fb4f",
            "compositeImage": {
                "id": "503d376d-ce11-4f6e-9fd8-061b858f9f6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cee4ab2-3a30-45f0-875e-13858ec4fb59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d17f9cff-6766-440f-8577-61745a7295f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cee4ab2-3a30-45f0-875e-13858ec4fb59",
                    "LayerId": "8db0a5bf-a10b-4b71-bfd5-be7230c8c30b"
                }
            ]
        },
        {
            "id": "7baedbbf-766e-4fd4-9830-5a72ff8963fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49b31bd6-2729-4efd-8b76-22172f79fb4f",
            "compositeImage": {
                "id": "48058a96-36ad-4951-aa7e-9ba11b464c28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7baedbbf-766e-4fd4-9830-5a72ff8963fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38604b1f-adba-4a8e-a1c4-6c0e9788e4a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7baedbbf-766e-4fd4-9830-5a72ff8963fd",
                    "LayerId": "8db0a5bf-a10b-4b71-bfd5-be7230c8c30b"
                }
            ]
        },
        {
            "id": "4fe910fd-c399-4614-92b1-50a1ec8ba132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49b31bd6-2729-4efd-8b76-22172f79fb4f",
            "compositeImage": {
                "id": "b33db648-fe07-4819-b71c-05d90ec6416f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fe910fd-c399-4614-92b1-50a1ec8ba132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cda8c4a3-b173-45db-bc02-4e100e141922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fe910fd-c399-4614-92b1-50a1ec8ba132",
                    "LayerId": "8db0a5bf-a10b-4b71-bfd5-be7230c8c30b"
                }
            ]
        },
        {
            "id": "69adf6ea-39c9-46aa-bd0f-92988d0d0e22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49b31bd6-2729-4efd-8b76-22172f79fb4f",
            "compositeImage": {
                "id": "5c6f7e91-4ee1-4744-983c-9c814f90c3b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69adf6ea-39c9-46aa-bd0f-92988d0d0e22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "823dcfe9-29cc-40e9-b218-76622de9cbe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69adf6ea-39c9-46aa-bd0f-92988d0d0e22",
                    "LayerId": "8db0a5bf-a10b-4b71-bfd5-be7230c8c30b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8db0a5bf-a10b-4b71-bfd5-be7230c8c30b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49b31bd6-2729-4efd-8b76-22172f79fb4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 46
}