{
    "id": "a209d819-1986-46aa-95e2-cc7ec35f6965",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage13buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b33ba723-2086-43de-8a00-e9f161494945",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "compositeImage": {
                "id": "707f28b8-fb59-421a-bc88-72361c5e907e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b33ba723-2086-43de-8a00-e9f161494945",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d6d0633-4707-443a-8b58-2be14f80ae13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b33ba723-2086-43de-8a00-e9f161494945",
                    "LayerId": "858add93-e523-456f-9e7d-cc63729e14f4"
                }
            ]
        },
        {
            "id": "96826f83-b51f-465d-bd48-92c414d7e95b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "compositeImage": {
                "id": "51419475-be12-42af-9366-392b89860370",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96826f83-b51f-465d-bd48-92c414d7e95b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f1fdfa7-cc2b-48bd-aca6-35edc5f08454",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96826f83-b51f-465d-bd48-92c414d7e95b",
                    "LayerId": "858add93-e523-456f-9e7d-cc63729e14f4"
                }
            ]
        },
        {
            "id": "b6b19168-2805-429c-b408-8db485aacca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "compositeImage": {
                "id": "83fc2fd6-4c61-470f-ba9c-31d6699c46af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6b19168-2805-429c-b408-8db485aacca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17a2c13a-4e18-4e51-8be3-37cdda29c727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6b19168-2805-429c-b408-8db485aacca8",
                    "LayerId": "858add93-e523-456f-9e7d-cc63729e14f4"
                }
            ]
        },
        {
            "id": "143c1d01-b889-4588-9fdf-acb65dd7b001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "compositeImage": {
                "id": "d4f8a975-33d6-4857-8f67-ee4e54cac109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "143c1d01-b889-4588-9fdf-acb65dd7b001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fd4db80-762d-4d40-a7af-be12c25df065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "143c1d01-b889-4588-9fdf-acb65dd7b001",
                    "LayerId": "858add93-e523-456f-9e7d-cc63729e14f4"
                }
            ]
        },
        {
            "id": "f63ff497-dc48-4973-b4f0-2d5a357846f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "compositeImage": {
                "id": "fd00770e-265e-4170-be6e-a2e7b1823473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f63ff497-dc48-4973-b4f0-2d5a357846f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb972426-7e9c-463b-9342-70c39dd9ed14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63ff497-dc48-4973-b4f0-2d5a357846f7",
                    "LayerId": "858add93-e523-456f-9e7d-cc63729e14f4"
                }
            ]
        },
        {
            "id": "6a3004c6-4335-4cbe-b267-7e4f37b11064",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "compositeImage": {
                "id": "a09794c9-4cf0-4a38-a35e-939d9d24f030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3004c6-4335-4cbe-b267-7e4f37b11064",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c6cea26-688f-46fd-978b-e81c612e418d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3004c6-4335-4cbe-b267-7e4f37b11064",
                    "LayerId": "858add93-e523-456f-9e7d-cc63729e14f4"
                }
            ]
        },
        {
            "id": "73dbce5e-bbbc-42cf-a74e-a9adb31066d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "compositeImage": {
                "id": "d835e3e9-7ac6-4f69-9c55-159547b27c9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73dbce5e-bbbc-42cf-a74e-a9adb31066d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f4ec6bf-aa03-4085-87ee-307153630b2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73dbce5e-bbbc-42cf-a74e-a9adb31066d4",
                    "LayerId": "858add93-e523-456f-9e7d-cc63729e14f4"
                }
            ]
        },
        {
            "id": "febf24bd-c5f8-4cbe-91c0-f2ab7f434527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "compositeImage": {
                "id": "f1d5cb3f-5953-42db-bce6-08981b8f4407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "febf24bd-c5f8-4cbe-91c0-f2ab7f434527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70d36b29-29ce-460b-b7c7-664d06f49677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "febf24bd-c5f8-4cbe-91c0-f2ab7f434527",
                    "LayerId": "858add93-e523-456f-9e7d-cc63729e14f4"
                }
            ]
        },
        {
            "id": "28e1af37-fdef-4fee-9b87-ec7acefe39f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "compositeImage": {
                "id": "c55e975f-baca-4052-8361-ac85d7f4ae8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28e1af37-fdef-4fee-9b87-ec7acefe39f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d06c1aa4-5dab-4352-a583-320a0c3d9e21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28e1af37-fdef-4fee-9b87-ec7acefe39f0",
                    "LayerId": "858add93-e523-456f-9e7d-cc63729e14f4"
                }
            ]
        },
        {
            "id": "d934372d-7bab-45dd-8e06-a37e5ef3a0f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "compositeImage": {
                "id": "2692db55-8775-44c0-a331-5d63be79f039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d934372d-7bab-45dd-8e06-a37e5ef3a0f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c010cb86-9ba0-4a00-82d6-0a119ec74cc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d934372d-7bab-45dd-8e06-a37e5ef3a0f1",
                    "LayerId": "858add93-e523-456f-9e7d-cc63729e14f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "858add93-e523-456f-9e7d-cc63729e14f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a209d819-1986-46aa-95e2-cc7ec35f6965",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}