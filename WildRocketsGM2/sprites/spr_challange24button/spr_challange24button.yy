{
    "id": "e2865308-32b5-40d4-aceb-14ece7f29277",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange24button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 0,
    "bbox_right": 98,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cc1fa2e-75fa-43b1-b3e4-d2db74568b23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2865308-32b5-40d4-aceb-14ece7f29277",
            "compositeImage": {
                "id": "2b9d062a-a13a-4c95-a886-47b869e872f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cc1fa2e-75fa-43b1-b3e4-d2db74568b23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50d96233-bc33-4ddd-9855-b22ca5564498",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cc1fa2e-75fa-43b1-b3e4-d2db74568b23",
                    "LayerId": "accb56c0-074a-467c-badd-3f7f37a7458f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "accb56c0-074a-467c-badd-3f7f37a7458f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2865308-32b5-40d4-aceb-14ece7f29277",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}