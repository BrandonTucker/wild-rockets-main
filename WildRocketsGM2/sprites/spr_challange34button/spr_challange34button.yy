{
    "id": "c260ea39-2dcb-44be-8318-12e7f92414de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange34button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cae65fcb-c01e-46f5-a887-491e79372ae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c260ea39-2dcb-44be-8318-12e7f92414de",
            "compositeImage": {
                "id": "2004b505-c781-4db8-a0b5-063590e8905d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cae65fcb-c01e-46f5-a887-491e79372ae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e772aec5-07d7-4a53-af49-962991104d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cae65fcb-c01e-46f5-a887-491e79372ae7",
                    "LayerId": "bb51bd2a-ebb1-4553-860c-dcc4f2882610"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "bb51bd2a-ebb1-4553-860c-dcc4f2882610",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c260ea39-2dcb-44be-8318-12e7f92414de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}