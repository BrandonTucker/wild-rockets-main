{
    "id": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rareship8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 38,
    "bbox_right": 92,
    "bbox_top": 29,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48575c1d-ba2e-485e-a8c7-35cc11575e53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "compositeImage": {
                "id": "1d8bd65e-6783-49b7-bac0-e95c22b94dd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48575c1d-ba2e-485e-a8c7-35cc11575e53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c797f853-540d-46a7-b951-d990afcde2e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48575c1d-ba2e-485e-a8c7-35cc11575e53",
                    "LayerId": "adcab970-e55a-4d2a-86af-212e9a1b0587"
                }
            ]
        },
        {
            "id": "2ba45723-6cfa-4d40-add6-53b8a230b164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "compositeImage": {
                "id": "68318e6f-5e9c-490a-96a2-d2bb1c9b0fba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ba45723-6cfa-4d40-add6-53b8a230b164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a213cd8a-9fc4-49eb-9a51-8b530a567b76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ba45723-6cfa-4d40-add6-53b8a230b164",
                    "LayerId": "adcab970-e55a-4d2a-86af-212e9a1b0587"
                }
            ]
        },
        {
            "id": "c063f093-8810-42ef-94a9-219f50ec90ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "compositeImage": {
                "id": "fc436c5d-ee7f-40b4-8da0-85a5fc61a085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c063f093-8810-42ef-94a9-219f50ec90ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e4fc9bf-3e42-43fe-8380-0f4d41094303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c063f093-8810-42ef-94a9-219f50ec90ed",
                    "LayerId": "adcab970-e55a-4d2a-86af-212e9a1b0587"
                }
            ]
        },
        {
            "id": "5896c302-33de-4727-be79-fb613aa5c251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "compositeImage": {
                "id": "feaf18f2-efe3-4942-9b01-de9f224ac67b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5896c302-33de-4727-be79-fb613aa5c251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6db11d0-73ea-45b3-9f30-4de2a0073371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5896c302-33de-4727-be79-fb613aa5c251",
                    "LayerId": "adcab970-e55a-4d2a-86af-212e9a1b0587"
                }
            ]
        },
        {
            "id": "7264d177-39b9-4b34-a1ec-949f49676d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "compositeImage": {
                "id": "364c0e41-2392-43d3-8db4-cd8610b16958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7264d177-39b9-4b34-a1ec-949f49676d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9798be56-3955-41c2-806f-14a05e61d8ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7264d177-39b9-4b34-a1ec-949f49676d56",
                    "LayerId": "adcab970-e55a-4d2a-86af-212e9a1b0587"
                }
            ]
        },
        {
            "id": "ab1ee8c7-10cc-4267-b709-ed407576f6e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "compositeImage": {
                "id": "e8e5972c-fb75-49da-8a87-b0bd17c4d7a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab1ee8c7-10cc-4267-b709-ed407576f6e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "707983c1-a541-4a5b-9c91-85365c02e599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab1ee8c7-10cc-4267-b709-ed407576f6e0",
                    "LayerId": "adcab970-e55a-4d2a-86af-212e9a1b0587"
                }
            ]
        },
        {
            "id": "8078ec8c-cd18-4033-8d25-5ce656047eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "compositeImage": {
                "id": "707d76e7-cb15-45cc-8ebc-35bdc2f364c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8078ec8c-cd18-4033-8d25-5ce656047eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1edd8d52-72ff-43d6-b6c0-96f45a2aba8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8078ec8c-cd18-4033-8d25-5ce656047eae",
                    "LayerId": "adcab970-e55a-4d2a-86af-212e9a1b0587"
                }
            ]
        },
        {
            "id": "293a4602-6831-4795-bc57-9c3d85d794d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "compositeImage": {
                "id": "21b80e9f-b3f8-4b25-a4cb-cb4edada7f44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "293a4602-6831-4795-bc57-9c3d85d794d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4987dd90-7664-4f3c-939f-d394b5486289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "293a4602-6831-4795-bc57-9c3d85d794d0",
                    "LayerId": "adcab970-e55a-4d2a-86af-212e9a1b0587"
                }
            ]
        },
        {
            "id": "c8b3998f-ba2f-4e2a-b597-30050cab6f94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "compositeImage": {
                "id": "c821269f-58ee-40de-a096-339107c86430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b3998f-ba2f-4e2a-b597-30050cab6f94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b7411e5-16d3-4030-b472-32cb9b44282f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b3998f-ba2f-4e2a-b597-30050cab6f94",
                    "LayerId": "adcab970-e55a-4d2a-86af-212e9a1b0587"
                }
            ]
        },
        {
            "id": "d47a9b61-48fb-4d1d-a73d-608a895bee61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "compositeImage": {
                "id": "eadbfb0e-ebd1-437b-afe7-822fb02ceb00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d47a9b61-48fb-4d1d-a73d-608a895bee61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a663547-85bb-46cf-8e5f-56cff2bf1187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d47a9b61-48fb-4d1d-a73d-608a895bee61",
                    "LayerId": "adcab970-e55a-4d2a-86af-212e9a1b0587"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 130,
    "layers": [
        {
            "id": "adcab970-e55a-4d2a-86af-212e9a1b0587",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eef3850f-59e6-486f-ac4b-b3829bec8f91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 65,
    "yorig": 67
}