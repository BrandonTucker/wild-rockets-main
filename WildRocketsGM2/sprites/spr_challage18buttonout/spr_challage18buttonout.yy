{
    "id": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage18buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9030af80-155d-406b-870f-45b3b05143a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "compositeImage": {
                "id": "1782210b-b03c-4ef6-85a7-17883b27fdb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9030af80-155d-406b-870f-45b3b05143a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef3c4191-2035-4737-b58d-1d9a8df5cea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9030af80-155d-406b-870f-45b3b05143a6",
                    "LayerId": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4"
                }
            ]
        },
        {
            "id": "27bc58aa-9597-41c1-80c2-4c71cf850d90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "compositeImage": {
                "id": "97a4e27c-a754-422e-a84f-1610bbbd528b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27bc58aa-9597-41c1-80c2-4c71cf850d90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a307945-5063-4bf7-90d7-0a3a2769930a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27bc58aa-9597-41c1-80c2-4c71cf850d90",
                    "LayerId": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4"
                }
            ]
        },
        {
            "id": "caa9bc81-6e91-4a31-8476-fbe73e408cb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "compositeImage": {
                "id": "b01bf2d7-2eba-4183-bec3-6acccd281080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caa9bc81-6e91-4a31-8476-fbe73e408cb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "559b81ef-918c-468d-8085-64cfd88025bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caa9bc81-6e91-4a31-8476-fbe73e408cb0",
                    "LayerId": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4"
                }
            ]
        },
        {
            "id": "b28a3345-2e4f-4714-baab-3807a11e8f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "compositeImage": {
                "id": "85017624-0a12-4279-939a-fafc205ccea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b28a3345-2e4f-4714-baab-3807a11e8f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76da988b-a935-4edb-9b5f-41d5948a4599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b28a3345-2e4f-4714-baab-3807a11e8f32",
                    "LayerId": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4"
                }
            ]
        },
        {
            "id": "69d97663-2f53-4c07-9727-42e37cd8ea69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "compositeImage": {
                "id": "a169c545-4d96-4d1d-9fb0-2a7e4f3855ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69d97663-2f53-4c07-9727-42e37cd8ea69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54dd3634-e852-4e5e-b888-5721a9f17dbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69d97663-2f53-4c07-9727-42e37cd8ea69",
                    "LayerId": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4"
                }
            ]
        },
        {
            "id": "89a553f5-e6b6-4188-8bd2-df863ebf39cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "compositeImage": {
                "id": "b6dd9e8c-ef30-41a1-b580-87d47d316171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89a553f5-e6b6-4188-8bd2-df863ebf39cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876f9ea4-c5b4-4ac2-9f2d-020f382dfc6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89a553f5-e6b6-4188-8bd2-df863ebf39cb",
                    "LayerId": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4"
                }
            ]
        },
        {
            "id": "63afbad9-bf69-456c-bcd2-3843c98c4862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "compositeImage": {
                "id": "682177c3-0237-4b8c-b538-53f748f86ed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63afbad9-bf69-456c-bcd2-3843c98c4862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da685259-805e-4139-a842-a4fc56c2d547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63afbad9-bf69-456c-bcd2-3843c98c4862",
                    "LayerId": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4"
                }
            ]
        },
        {
            "id": "acd950a6-6c3f-48fd-b373-515c089f57ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "compositeImage": {
                "id": "0d8f2cea-f573-40e2-b9e6-f9da515cd33e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acd950a6-6c3f-48fd-b373-515c089f57ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65fd426d-2bad-4757-bdcc-a1adb3aed144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acd950a6-6c3f-48fd-b373-515c089f57ef",
                    "LayerId": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4"
                }
            ]
        },
        {
            "id": "16865b06-ff03-4007-88f2-912e6beaa0ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "compositeImage": {
                "id": "a4f31898-50dc-4104-bf24-7a7c672fc2e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16865b06-ff03-4007-88f2-912e6beaa0ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e81075ee-0eb3-4277-919d-79ec09e1123e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16865b06-ff03-4007-88f2-912e6beaa0ce",
                    "LayerId": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4"
                }
            ]
        },
        {
            "id": "6897ca24-6099-42a3-92ee-732673caefb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "compositeImage": {
                "id": "761e7746-f029-4265-b147-9aad1e8c345e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6897ca24-6099-42a3-92ee-732673caefb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4514b3c-a367-4a94-b50b-36d595bb3df6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6897ca24-6099-42a3-92ee-732673caefb0",
                    "LayerId": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "3efa47ff-956e-4fdb-9eb2-f33f8420c9e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7cdf42c2-b565-4573-8156-b4e720e6b85d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}