{
    "id": "ced0b82f-19d0-4332-a7a0-77f3856c55ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship17",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 24,
    "bbox_right": 49,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8fbf39b-728a-4404-a220-302b56555b77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ced0b82f-19d0-4332-a7a0-77f3856c55ab",
            "compositeImage": {
                "id": "2d95cb31-97f1-46e6-a12f-bc1a33bdcef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8fbf39b-728a-4404-a220-302b56555b77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c85a1b3f-706a-47f9-a75a-c8e6ad82513f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8fbf39b-728a-4404-a220-302b56555b77",
                    "LayerId": "b02b0899-4347-48db-b036-e1d32f3fca3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "b02b0899-4347-48db-b036-e1d32f3fca3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ced0b82f-19d0-4332-a7a0-77f3856c55ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 36,
    "yorig": 41
}