{
    "id": "3c60cd49-2f3c-429f-bb1f-cf7114bf5d3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite181",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1aec761-1d88-4dfa-b035-42fe5818755a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c60cd49-2f3c-429f-bb1f-cf7114bf5d3b",
            "compositeImage": {
                "id": "dd949196-3a74-4515-a453-f04a51020d9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1aec761-1d88-4dfa-b035-42fe5818755a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c71069be-b176-4103-af55-5c6dfd078e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1aec761-1d88-4dfa-b035-42fe5818755a",
                    "LayerId": "40c236f2-ed36-49e0-9488-73c1889fe3db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "40c236f2-ed36-49e0-9488-73c1889fe3db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c60cd49-2f3c-429f-bb1f-cf7114bf5d3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}