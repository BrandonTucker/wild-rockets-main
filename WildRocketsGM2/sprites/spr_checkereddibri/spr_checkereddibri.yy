{
    "id": "94cba514-6696-47e7-8ff7-b179374bc4c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_checkereddibri",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f7a79a5-d703-41c7-86f2-46738c41af34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "b7ad298f-bf5b-476d-ae29-34ea808c8100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f7a79a5-d703-41c7-86f2-46738c41af34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5395769e-c9ad-4b30-b56c-89202098556f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f7a79a5-d703-41c7-86f2-46738c41af34",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "f1da1af8-ff05-4cb5-b440-23dfc5d60c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "b0b6bd47-5025-469b-b971-af3e83a43d87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1da1af8-ff05-4cb5-b440-23dfc5d60c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1813a738-6249-434a-be57-2442d9334ffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1da1af8-ff05-4cb5-b440-23dfc5d60c60",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "b69d9e6c-5af3-4c1c-8563-e38d85515df6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "8b36a8cc-220c-42b0-9ae6-1ead45b4a799",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b69d9e6c-5af3-4c1c-8563-e38d85515df6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a33bdfa3-ba29-4685-b99b-df3c7b470e03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b69d9e6c-5af3-4c1c-8563-e38d85515df6",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "5894bfde-8a86-4fc1-9b80-8c971d670eb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "5398a071-a3fd-4488-9557-5c73e16692ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5894bfde-8a86-4fc1-9b80-8c971d670eb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8d90f06-b13c-44f1-b7d1-347ac66618fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5894bfde-8a86-4fc1-9b80-8c971d670eb8",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "10d8b4d3-1a38-4498-bab6-67c9d3b180a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "be66bbac-9187-47ab-a90a-a83fa59a6ea6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d8b4d3-1a38-4498-bab6-67c9d3b180a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79eb89e9-5f53-4beb-8ed3-6e12c853f780",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d8b4d3-1a38-4498-bab6-67c9d3b180a4",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "acd3caba-f080-4c5f-b403-88a9b70bbecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "fea0d6ca-6eda-4c67-afaf-9a09aeea816c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acd3caba-f080-4c5f-b403-88a9b70bbecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32f9171d-3065-41c7-b805-a1029faa154b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acd3caba-f080-4c5f-b403-88a9b70bbecf",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "90fcab51-1ed9-402b-bd2b-c0d94fa68030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "3e477dc1-37ae-4770-8b6b-8b0f55931798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90fcab51-1ed9-402b-bd2b-c0d94fa68030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f121c997-dd2c-46a9-9c06-00d05e22a2e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90fcab51-1ed9-402b-bd2b-c0d94fa68030",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "558cfea2-198e-4d7b-85bd-23c43ce4ab2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "2eecbaad-113c-4f7d-94ae-2d75b807c30e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "558cfea2-198e-4d7b-85bd-23c43ce4ab2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "668c54c7-e514-41aa-920e-34b3bc888ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "558cfea2-198e-4d7b-85bd-23c43ce4ab2f",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "19ba5e41-a3cf-44d6-90b3-653a5ebb99f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "33df034f-c045-4452-a254-29539ce3d844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ba5e41-a3cf-44d6-90b3-653a5ebb99f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f5e7297-6547-46ea-8b67-806b20f13657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ba5e41-a3cf-44d6-90b3-653a5ebb99f9",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "5783eada-024e-4b1d-b7cd-2f8cf38250b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "fcd935fa-4b5d-4df3-b5ef-bb298a72f29c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5783eada-024e-4b1d-b7cd-2f8cf38250b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd01df39-c83c-44f9-85b5-c50b71984d3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5783eada-024e-4b1d-b7cd-2f8cf38250b1",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "a237693d-bf7d-43a3-99a5-33a38e476df9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "142d2cb1-b890-441e-b170-a0686675d15a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a237693d-bf7d-43a3-99a5-33a38e476df9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2802fb-9a8c-4636-a748-aa42a67bcb77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a237693d-bf7d-43a3-99a5-33a38e476df9",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "7960afdf-f157-4ad0-af2e-b773fdd99145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "0ac243a0-ef3c-461c-bf12-5a3e52c2a9b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7960afdf-f157-4ad0-af2e-b773fdd99145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6b13f64-ae50-48ea-a07a-f903dba90478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7960afdf-f157-4ad0-af2e-b773fdd99145",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "7067f5a7-a78f-4cc2-9394-c1ede99764bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "afe3132b-5502-49cf-8409-f5fb2a79e2c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7067f5a7-a78f-4cc2-9394-c1ede99764bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06bd8c2d-10aa-4a3a-ad38-b84d1716c4f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7067f5a7-a78f-4cc2-9394-c1ede99764bf",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "f7b397c9-47d8-49d7-a0d3-ddb2638ceced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "533ada33-3012-45aa-837b-97f6a31724cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7b397c9-47d8-49d7-a0d3-ddb2638ceced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a919a07-7014-4d08-ad86-d01c5c4255ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7b397c9-47d8-49d7-a0d3-ddb2638ceced",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        },
        {
            "id": "2cef60ff-b627-4b50-9fc7-08f0355d5caf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "compositeImage": {
                "id": "712971ec-6efc-4607-91ae-772b44abb36c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cef60ff-b627-4b50-9fc7-08f0355d5caf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2af8aeb-2e96-406b-851f-b4798b86a561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cef60ff-b627-4b50-9fc7-08f0355d5caf",
                    "LayerId": "a1af2586-ef76-415d-bb62-a33df922d57e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a1af2586-ef76-415d-bb62-a33df922d57e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94cba514-6696-47e7-8ff7-b179374bc4c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -4,
    "yorig": -4
}