{
    "id": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange5buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 2,
    "bbox_right": 108,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10156e5f-ddd5-4923-a657-ba771f08557b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "compositeImage": {
                "id": "32017c2f-171c-4c85-9704-9f590a8b5079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10156e5f-ddd5-4923-a657-ba771f08557b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ac91304-9730-4519-9d72-cab01a357e51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10156e5f-ddd5-4923-a657-ba771f08557b",
                    "LayerId": "87eac268-4d67-433d-8a6b-56842886a85a"
                }
            ]
        },
        {
            "id": "4ac66597-c47b-421d-bca0-6b9ffef10277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "compositeImage": {
                "id": "bdc401df-345a-4312-a919-497d1b30e0f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ac66597-c47b-421d-bca0-6b9ffef10277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9fd68f9-b7e9-4c1a-97c6-a8e616b5cf21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ac66597-c47b-421d-bca0-6b9ffef10277",
                    "LayerId": "87eac268-4d67-433d-8a6b-56842886a85a"
                }
            ]
        },
        {
            "id": "6ada5572-4e03-49fd-9508-3cdc9ddc3be6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "compositeImage": {
                "id": "8b440b1e-4f6b-4758-8f38-966e82cfbde8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ada5572-4e03-49fd-9508-3cdc9ddc3be6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d6ab96a-df68-4e07-953b-8a923a00abde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ada5572-4e03-49fd-9508-3cdc9ddc3be6",
                    "LayerId": "87eac268-4d67-433d-8a6b-56842886a85a"
                }
            ]
        },
        {
            "id": "96c63b4f-ebd1-424d-8dd7-9a52c2bcdf43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "compositeImage": {
                "id": "b0464666-cc3b-45f2-919b-bcc2e8217a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96c63b4f-ebd1-424d-8dd7-9a52c2bcdf43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3908726-eeae-4be8-86c1-1beb26d9ee96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96c63b4f-ebd1-424d-8dd7-9a52c2bcdf43",
                    "LayerId": "87eac268-4d67-433d-8a6b-56842886a85a"
                }
            ]
        },
        {
            "id": "841ce4f9-a7a4-469b-9a76-eda8bc37f657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "compositeImage": {
                "id": "00db4147-0379-4b68-91ac-3af575fd83d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "841ce4f9-a7a4-469b-9a76-eda8bc37f657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94df2354-f13b-43ad-b4e6-b8d954bdd5aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "841ce4f9-a7a4-469b-9a76-eda8bc37f657",
                    "LayerId": "87eac268-4d67-433d-8a6b-56842886a85a"
                }
            ]
        },
        {
            "id": "64d6b532-fce3-4372-ab98-d9d8ebb305fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "compositeImage": {
                "id": "16d3ef75-4d05-4dbb-a939-bb4446a89c40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64d6b532-fce3-4372-ab98-d9d8ebb305fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a94360cc-ff73-4f47-8ad6-f1e9393f4a3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64d6b532-fce3-4372-ab98-d9d8ebb305fb",
                    "LayerId": "87eac268-4d67-433d-8a6b-56842886a85a"
                }
            ]
        },
        {
            "id": "30ab2b55-2d56-4b56-b9a0-e6b4a9abe36e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "compositeImage": {
                "id": "1ae4c9eb-2402-4644-beae-e4509d39ad8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30ab2b55-2d56-4b56-b9a0-e6b4a9abe36e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7640a1a-660d-417b-a49c-abed2a593ea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30ab2b55-2d56-4b56-b9a0-e6b4a9abe36e",
                    "LayerId": "87eac268-4d67-433d-8a6b-56842886a85a"
                }
            ]
        },
        {
            "id": "ba046f41-b4ba-426c-ba42-a26737ebc702",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "compositeImage": {
                "id": "f2116c08-1c3d-4e86-8de7-ae460ec57194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba046f41-b4ba-426c-ba42-a26737ebc702",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2038222e-93b6-4bb0-9f16-3769223c5c58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba046f41-b4ba-426c-ba42-a26737ebc702",
                    "LayerId": "87eac268-4d67-433d-8a6b-56842886a85a"
                }
            ]
        },
        {
            "id": "0fd5f174-27e4-4d1f-af9d-45148b5cdfda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "compositeImage": {
                "id": "82ab49e0-8996-4a8e-94d6-359c6dbc3a0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fd5f174-27e4-4d1f-af9d-45148b5cdfda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2cacd65-ebfa-4d0d-a067-e58ca4b760b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fd5f174-27e4-4d1f-af9d-45148b5cdfda",
                    "LayerId": "87eac268-4d67-433d-8a6b-56842886a85a"
                }
            ]
        },
        {
            "id": "9ca05d57-3efc-4a94-a580-a7876c548a23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "compositeImage": {
                "id": "59d76c71-8e2f-498c-964d-abcd1c5b8263",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ca05d57-3efc-4a94-a580-a7876c548a23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc1ae533-003f-4013-8403-882a3ab1a0fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ca05d57-3efc-4a94-a580-a7876c548a23",
                    "LayerId": "87eac268-4d67-433d-8a6b-56842886a85a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "87eac268-4d67-433d-8a6b-56842886a85a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3cd0913-9c68-45b0-ab00-14c30c276fe7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 50,
    "yorig": 48
}