{
    "id": "761c817e-3048-4377-9e6d-eb73aea214fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shiplockedtemplate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ba85004-c40a-4998-896a-364d18a87bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761c817e-3048-4377-9e6d-eb73aea214fe",
            "compositeImage": {
                "id": "5160564f-a8da-4812-aab9-cb1381736f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ba85004-c40a-4998-896a-364d18a87bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e93a0e2-4bdb-4774-bf23-7430935ae9f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ba85004-c40a-4998-896a-364d18a87bcf",
                    "LayerId": "5c60c8fb-7774-4c99-9084-009365d5d38a"
                }
            ]
        },
        {
            "id": "5d22373e-eb9d-43ed-8f8b-fd6b7b6ad3d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761c817e-3048-4377-9e6d-eb73aea214fe",
            "compositeImage": {
                "id": "c1f87644-5a34-4daa-8e82-cc9a04768f87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d22373e-eb9d-43ed-8f8b-fd6b7b6ad3d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5a0c6de-d002-4681-9044-b71f723791fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d22373e-eb9d-43ed-8f8b-fd6b7b6ad3d3",
                    "LayerId": "5c60c8fb-7774-4c99-9084-009365d5d38a"
                }
            ]
        },
        {
            "id": "9085bb9c-ffe4-47f7-b0d2-d5c527e721c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761c817e-3048-4377-9e6d-eb73aea214fe",
            "compositeImage": {
                "id": "c92eb91a-2a09-48ce-89d6-a623386b5b05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9085bb9c-ffe4-47f7-b0d2-d5c527e721c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f93ac85e-4446-4b95-8fc4-11998bd53f85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9085bb9c-ffe4-47f7-b0d2-d5c527e721c1",
                    "LayerId": "5c60c8fb-7774-4c99-9084-009365d5d38a"
                }
            ]
        },
        {
            "id": "a1e4f651-ad6f-4b07-9b04-13eb96fd707f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "761c817e-3048-4377-9e6d-eb73aea214fe",
            "compositeImage": {
                "id": "c50beb63-5a5b-40bb-838e-ab33a5fc0ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1e4f651-ad6f-4b07-9b04-13eb96fd707f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "148489aa-30b7-4a61-9b89-78efa4188311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1e4f651-ad6f-4b07-9b04-13eb96fd707f",
                    "LayerId": "5c60c8fb-7774-4c99-9084-009365d5d38a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "5c60c8fb-7774-4c99-9084-009365d5d38a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "761c817e-3048-4377-9e6d-eb73aea214fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}