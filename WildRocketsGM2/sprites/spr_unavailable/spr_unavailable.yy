{
    "id": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_unavailable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f65efbb8-c821-4e39-babe-2690f2854b34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
            "compositeImage": {
                "id": "d1e33888-9d51-4ab9-9aae-d6ceda5f87d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f65efbb8-c821-4e39-babe-2690f2854b34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a70ecf1-ee99-438d-b555-88cac9e41915",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f65efbb8-c821-4e39-babe-2690f2854b34",
                    "LayerId": "8aa52a14-6111-41d6-bc99-153143a3887a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "8aa52a14-6111-41d6-bc99-153143a3887a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcb25dd5-49c0-4cff-9539-61d70cdf5486",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}