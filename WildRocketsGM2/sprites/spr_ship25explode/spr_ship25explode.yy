{
    "id": "dcc520c8-72ce-4606-ad15-b17078e0dd81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship25explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 25,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "630dfc34-3166-4b42-a75b-f6bd03686504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc520c8-72ce-4606-ad15-b17078e0dd81",
            "compositeImage": {
                "id": "180d6d38-ed56-40cc-9d31-ee434222da4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "630dfc34-3166-4b42-a75b-f6bd03686504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd3d4c7f-e926-4f42-aff0-d2f6069496a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "630dfc34-3166-4b42-a75b-f6bd03686504",
                    "LayerId": "1d51c0d3-d78d-4526-ba00-d6764ef895eb"
                }
            ]
        },
        {
            "id": "71053e57-dd19-477f-9e3c-abfc75759919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc520c8-72ce-4606-ad15-b17078e0dd81",
            "compositeImage": {
                "id": "17f1705e-484b-484a-bdfe-f8a31f78fda9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71053e57-dd19-477f-9e3c-abfc75759919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1989bc04-87a3-4406-9010-0328a449808e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71053e57-dd19-477f-9e3c-abfc75759919",
                    "LayerId": "1d51c0d3-d78d-4526-ba00-d6764ef895eb"
                }
            ]
        },
        {
            "id": "dc77ef77-a0e3-4bd6-a5fb-370dca5e8542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc520c8-72ce-4606-ad15-b17078e0dd81",
            "compositeImage": {
                "id": "7e1d1431-e679-4bc9-9e24-79e0abb4d2f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc77ef77-a0e3-4bd6-a5fb-370dca5e8542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "040cb665-4153-4277-b967-d393d940dc27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc77ef77-a0e3-4bd6-a5fb-370dca5e8542",
                    "LayerId": "1d51c0d3-d78d-4526-ba00-d6764ef895eb"
                }
            ]
        },
        {
            "id": "62eb6687-f074-4925-a83a-f81db87b47d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc520c8-72ce-4606-ad15-b17078e0dd81",
            "compositeImage": {
                "id": "5ba99aee-07ec-428f-b35a-6491d35341ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62eb6687-f074-4925-a83a-f81db87b47d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e6799da-1418-4e21-8745-196a6873d15a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62eb6687-f074-4925-a83a-f81db87b47d2",
                    "LayerId": "1d51c0d3-d78d-4526-ba00-d6764ef895eb"
                }
            ]
        },
        {
            "id": "bd71a580-74a0-48e9-8849-ff9c96166d83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc520c8-72ce-4606-ad15-b17078e0dd81",
            "compositeImage": {
                "id": "c7aca907-1836-4093-8b36-f3b58cf6cbc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd71a580-74a0-48e9-8849-ff9c96166d83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6f32e4-c67f-4ef2-b46d-6245435266a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd71a580-74a0-48e9-8849-ff9c96166d83",
                    "LayerId": "1d51c0d3-d78d-4526-ba00-d6764ef895eb"
                }
            ]
        },
        {
            "id": "640b3148-a031-4155-a188-fc2d79eef078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc520c8-72ce-4606-ad15-b17078e0dd81",
            "compositeImage": {
                "id": "adc3d376-333f-41b7-8210-4d68a3ecd30d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "640b3148-a031-4155-a188-fc2d79eef078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1e3ee05-5f66-48d2-813b-febb65333fec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "640b3148-a031-4155-a188-fc2d79eef078",
                    "LayerId": "1d51c0d3-d78d-4526-ba00-d6764ef895eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 172,
    "layers": [
        {
            "id": "1d51c0d3-d78d-4526-ba00-d6764ef895eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcc520c8-72ce-4606-ad15-b17078e0dd81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        285212671,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 62
}