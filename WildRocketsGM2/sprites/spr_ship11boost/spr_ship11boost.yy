{
    "id": "36943602-cbd4-4509-926f-18e5998a17a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship11boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f80982a8-6082-43bd-a61d-c52da8241049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36943602-cbd4-4509-926f-18e5998a17a8",
            "compositeImage": {
                "id": "59d7dd63-de8c-4a28-90a8-6ba7cc1dda85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f80982a8-6082-43bd-a61d-c52da8241049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c11a5b9c-cd62-45d1-b38e-27d8b77ff891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f80982a8-6082-43bd-a61d-c52da8241049",
                    "LayerId": "f683abad-711b-4237-8be7-20043ca037db"
                }
            ]
        },
        {
            "id": "19375259-5357-4e64-8439-aed6b0f97644",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36943602-cbd4-4509-926f-18e5998a17a8",
            "compositeImage": {
                "id": "9c063681-a047-49c3-a108-45dccf531d10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19375259-5357-4e64-8439-aed6b0f97644",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f830e7e-61c1-4ef8-8b68-3bd21d47d8c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19375259-5357-4e64-8439-aed6b0f97644",
                    "LayerId": "f683abad-711b-4237-8be7-20043ca037db"
                }
            ]
        },
        {
            "id": "02eb9d95-5da4-41fe-962b-8b594210b5c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36943602-cbd4-4509-926f-18e5998a17a8",
            "compositeImage": {
                "id": "3a6489c7-9c5a-4204-bd51-3e42ef648ad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02eb9d95-5da4-41fe-962b-8b594210b5c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdd95479-37ed-490b-a303-f57ff9f34c4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02eb9d95-5da4-41fe-962b-8b594210b5c4",
                    "LayerId": "f683abad-711b-4237-8be7-20043ca037db"
                }
            ]
        },
        {
            "id": "6a8feb5c-1846-41c8-adfb-c62c93b171bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36943602-cbd4-4509-926f-18e5998a17a8",
            "compositeImage": {
                "id": "3e962bc0-5a55-48fe-b0f5-284f63385cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a8feb5c-1846-41c8-adfb-c62c93b171bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c59b6127-da57-4a0d-8cb3-1d1bb89d0206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a8feb5c-1846-41c8-adfb-c62c93b171bd",
                    "LayerId": "f683abad-711b-4237-8be7-20043ca037db"
                }
            ]
        },
        {
            "id": "ec1e1f6a-b829-47e5-a691-367f6249516f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36943602-cbd4-4509-926f-18e5998a17a8",
            "compositeImage": {
                "id": "553bef65-b09a-49ed-ba70-e750fc88132d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec1e1f6a-b829-47e5-a691-367f6249516f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "665f532b-2403-46cb-ab58-0991be8cb7d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec1e1f6a-b829-47e5-a691-367f6249516f",
                    "LayerId": "f683abad-711b-4237-8be7-20043ca037db"
                }
            ]
        },
        {
            "id": "17426cca-1b11-4454-96b8-88e7439733d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36943602-cbd4-4509-926f-18e5998a17a8",
            "compositeImage": {
                "id": "82123e74-b83f-4618-86a1-9b758c604379",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17426cca-1b11-4454-96b8-88e7439733d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ce9e33f-91b5-4071-96e6-a9ef64d78be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17426cca-1b11-4454-96b8-88e7439733d7",
                    "LayerId": "f683abad-711b-4237-8be7-20043ca037db"
                }
            ]
        },
        {
            "id": "fb6c661d-b1d0-493d-9b10-33c6e5f9b794",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36943602-cbd4-4509-926f-18e5998a17a8",
            "compositeImage": {
                "id": "e3a682be-9494-4596-8012-b4a29f47c581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb6c661d-b1d0-493d-9b10-33c6e5f9b794",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb2e1c47-e91c-4c3d-8a6a-91edbcff0ec7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb6c661d-b1d0-493d-9b10-33c6e5f9b794",
                    "LayerId": "f683abad-711b-4237-8be7-20043ca037db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 155,
    "layers": [
        {
            "id": "f683abad-711b-4237-8be7-20043ca037db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36943602-cbd4-4509-926f-18e5998a17a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 42
}