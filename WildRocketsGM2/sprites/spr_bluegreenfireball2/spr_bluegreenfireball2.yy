{
    "id": "6007a803-2d8f-453d-a159-6b68d8f00100",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bluegreenfireball2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54fc0e2a-314c-4e5b-99b8-ffa840a6d080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6007a803-2d8f-453d-a159-6b68d8f00100",
            "compositeImage": {
                "id": "374fb7f2-c11f-4782-af3b-f8904df21551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54fc0e2a-314c-4e5b-99b8-ffa840a6d080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e787b27-f1b6-44f2-a7af-189edb6a283b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54fc0e2a-314c-4e5b-99b8-ffa840a6d080",
                    "LayerId": "3937bdef-08a2-4199-99a6-7dc67d9dd1f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3937bdef-08a2-4199-99a6-7dc67d9dd1f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6007a803-2d8f-453d-a159-6b68d8f00100",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 16
}