{
    "id": "0d361f46-e4e5-4ac6-9728-a58d0b350847",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship13explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 25,
    "bbox_right": 55,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d74a9114-bd00-4650-a09e-53cb1ebd88e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d361f46-e4e5-4ac6-9728-a58d0b350847",
            "compositeImage": {
                "id": "f1597a03-a369-45fa-bbc4-869ad079a02d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d74a9114-bd00-4650-a09e-53cb1ebd88e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01285cc0-c064-40e2-8540-ee61ea4dc2ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d74a9114-bd00-4650-a09e-53cb1ebd88e9",
                    "LayerId": "d6bb62f5-b9b9-4686-a077-9fef4dadd40a"
                }
            ]
        },
        {
            "id": "e1b498dd-1582-48a9-a0cf-8cf539c02744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d361f46-e4e5-4ac6-9728-a58d0b350847",
            "compositeImage": {
                "id": "6d17d072-2c36-4d33-a140-0afe4cd195a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b498dd-1582-48a9-a0cf-8cf539c02744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "246346f3-8b61-4643-8821-a12c6668c662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b498dd-1582-48a9-a0cf-8cf539c02744",
                    "LayerId": "d6bb62f5-b9b9-4686-a077-9fef4dadd40a"
                }
            ]
        },
        {
            "id": "a1fccc0a-2f1e-4923-8911-a1631699751b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d361f46-e4e5-4ac6-9728-a58d0b350847",
            "compositeImage": {
                "id": "a7e9c59a-7fb6-4329-bbf7-1dd3f16daa75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1fccc0a-2f1e-4923-8911-a1631699751b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eee8f1c-d044-466f-aa86-4222b5717fd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1fccc0a-2f1e-4923-8911-a1631699751b",
                    "LayerId": "d6bb62f5-b9b9-4686-a077-9fef4dadd40a"
                }
            ]
        },
        {
            "id": "4b36cef3-2b2d-4502-bf8f-fb670fe2458e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d361f46-e4e5-4ac6-9728-a58d0b350847",
            "compositeImage": {
                "id": "d2c15279-5579-4bab-b041-f16735c61b7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b36cef3-2b2d-4502-bf8f-fb670fe2458e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dc25aed-24bd-43c3-b8ee-1fdcbaf4382f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b36cef3-2b2d-4502-bf8f-fb670fe2458e",
                    "LayerId": "d6bb62f5-b9b9-4686-a077-9fef4dadd40a"
                }
            ]
        },
        {
            "id": "7aa312fb-01f3-4d53-a36b-4654cfd474ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d361f46-e4e5-4ac6-9728-a58d0b350847",
            "compositeImage": {
                "id": "3b15e2be-9651-4751-a2f3-f931e2b3583d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa312fb-01f3-4d53-a36b-4654cfd474ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93d33c8c-2d2c-41c4-b311-214e31e4c0ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa312fb-01f3-4d53-a36b-4654cfd474ff",
                    "LayerId": "d6bb62f5-b9b9-4686-a077-9fef4dadd40a"
                }
            ]
        },
        {
            "id": "ef902a67-766d-41ab-a5d5-148b3362ab9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d361f46-e4e5-4ac6-9728-a58d0b350847",
            "compositeImage": {
                "id": "6a5e4022-60f1-48eb-9022-55baefaedb4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef902a67-766d-41ab-a5d5-148b3362ab9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5debcab-d777-4b17-91ca-a221bfd3f47a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef902a67-766d-41ab-a5d5-148b3362ab9d",
                    "LayerId": "d6bb62f5-b9b9-4686-a077-9fef4dadd40a"
                }
            ]
        },
        {
            "id": "cb6cf895-6c94-4eb8-a09f-3a5d553e7a32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d361f46-e4e5-4ac6-9728-a58d0b350847",
            "compositeImage": {
                "id": "88ca59dd-3d40-4278-b093-728bd9e7ce0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb6cf895-6c94-4eb8-a09f-3a5d553e7a32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d488911-bffe-471a-b0a7-7a64a25c5cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6cf895-6c94-4eb8-a09f-3a5d553e7a32",
                    "LayerId": "d6bb62f5-b9b9-4686-a077-9fef4dadd40a"
                }
            ]
        },
        {
            "id": "99b697d0-03d5-4d51-bec2-a826f378124c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d361f46-e4e5-4ac6-9728-a58d0b350847",
            "compositeImage": {
                "id": "d788108d-deb7-4112-970b-b21b64a00908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99b697d0-03d5-4d51-bec2-a826f378124c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14bb769c-f124-4b64-bb3c-5fbc5b4e09dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99b697d0-03d5-4d51-bec2-a826f378124c",
                    "LayerId": "d6bb62f5-b9b9-4686-a077-9fef4dadd40a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "d6bb62f5-b9b9-4686-a077-9fef4dadd40a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d361f46-e4e5-4ac6-9728-a58d0b350847",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 42
}