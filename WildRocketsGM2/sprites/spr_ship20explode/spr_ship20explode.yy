{
    "id": "8ff7ce9d-73d9-4101-9bc4-dcd5ed06b219",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship20explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 29,
    "bbox_right": 59,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4931b84b-6e35-4baa-807d-98cb6012b93e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff7ce9d-73d9-4101-9bc4-dcd5ed06b219",
            "compositeImage": {
                "id": "60fefb0a-0e18-47bd-91a2-d1a0f6de3550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4931b84b-6e35-4baa-807d-98cb6012b93e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebfc8265-32c5-45e6-a1b2-bf537602e012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4931b84b-6e35-4baa-807d-98cb6012b93e",
                    "LayerId": "a1f4e9d8-175f-49d4-99cb-b1e9ee3516b6"
                }
            ]
        },
        {
            "id": "ef922b82-25b1-49f3-ac07-6c1b88477b00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff7ce9d-73d9-4101-9bc4-dcd5ed06b219",
            "compositeImage": {
                "id": "7605a68f-51a4-4a16-a440-44acf3935ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef922b82-25b1-49f3-ac07-6c1b88477b00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d11faf6-6724-48c2-ac79-0421bdf1db4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef922b82-25b1-49f3-ac07-6c1b88477b00",
                    "LayerId": "a1f4e9d8-175f-49d4-99cb-b1e9ee3516b6"
                }
            ]
        },
        {
            "id": "456bda6a-c74f-49dc-a71f-15f7eab0ea0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff7ce9d-73d9-4101-9bc4-dcd5ed06b219",
            "compositeImage": {
                "id": "8f7c14ba-6a9e-40fa-a11b-9ac9dfa1bb83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "456bda6a-c74f-49dc-a71f-15f7eab0ea0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1cfcb34-371f-4dc0-87df-ae44040887bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "456bda6a-c74f-49dc-a71f-15f7eab0ea0f",
                    "LayerId": "a1f4e9d8-175f-49d4-99cb-b1e9ee3516b6"
                }
            ]
        },
        {
            "id": "68375c93-7344-48b6-8406-c4a968bd4ba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff7ce9d-73d9-4101-9bc4-dcd5ed06b219",
            "compositeImage": {
                "id": "cc8f0ea1-0cc2-4a25-9339-eef1f3168fea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68375c93-7344-48b6-8406-c4a968bd4ba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d9d40f-85e4-4637-baf3-034c3c60c2a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68375c93-7344-48b6-8406-c4a968bd4ba0",
                    "LayerId": "a1f4e9d8-175f-49d4-99cb-b1e9ee3516b6"
                }
            ]
        },
        {
            "id": "8687354d-bde5-4b9f-bbf4-b608f3d8862e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff7ce9d-73d9-4101-9bc4-dcd5ed06b219",
            "compositeImage": {
                "id": "252f4e4c-ae9d-443b-83bf-c683de707ccd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8687354d-bde5-4b9f-bbf4-b608f3d8862e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051b8157-9d67-4925-bb4e-c6f66d0cf176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8687354d-bde5-4b9f-bbf4-b608f3d8862e",
                    "LayerId": "a1f4e9d8-175f-49d4-99cb-b1e9ee3516b6"
                }
            ]
        },
        {
            "id": "ac650d7e-e454-4cb1-9d9d-5ef9b75e29ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff7ce9d-73d9-4101-9bc4-dcd5ed06b219",
            "compositeImage": {
                "id": "2cd1fa43-7bab-44ea-9e52-6f1e6a6567e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac650d7e-e454-4cb1-9d9d-5ef9b75e29ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "570c5b7d-7036-4042-b021-cb01e8730353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac650d7e-e454-4cb1-9d9d-5ef9b75e29ee",
                    "LayerId": "a1f4e9d8-175f-49d4-99cb-b1e9ee3516b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "a1f4e9d8-175f-49d4-99cb-b1e9ee3516b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ff7ce9d-73d9-4101-9bc4-dcd5ed06b219",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 55
}