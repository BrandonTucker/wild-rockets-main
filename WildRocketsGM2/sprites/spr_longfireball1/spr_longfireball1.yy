{
    "id": "944f2300-2d6f-468c-aaf7-2166efb19f81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_longfireball1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 34,
    "bbox_right": 64,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4976cb4c-dc92-470e-bf40-89807cd79dac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "944f2300-2d6f-468c-aaf7-2166efb19f81",
            "compositeImage": {
                "id": "914b1c40-3158-4ade-a121-e3fd1472b545",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4976cb4c-dc92-470e-bf40-89807cd79dac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a26c9d-0bd8-459c-a767-adeeb876e5a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4976cb4c-dc92-470e-bf40-89807cd79dac",
                    "LayerId": "b72a2cec-93cd-4678-94ec-87dc92972c93"
                }
            ]
        },
        {
            "id": "7dbd9698-ff90-4bb7-8b43-5c162584335b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "944f2300-2d6f-468c-aaf7-2166efb19f81",
            "compositeImage": {
                "id": "a0a4c498-53c1-466b-a355-84abbe77c9e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dbd9698-ff90-4bb7-8b43-5c162584335b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7a6c11c-1400-48fa-aa47-317fbbb60764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dbd9698-ff90-4bb7-8b43-5c162584335b",
                    "LayerId": "b72a2cec-93cd-4678-94ec-87dc92972c93"
                }
            ]
        },
        {
            "id": "90d02fb0-281c-4181-a568-0b908d88fb51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "944f2300-2d6f-468c-aaf7-2166efb19f81",
            "compositeImage": {
                "id": "9e103499-221d-4606-90da-05b985b18313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90d02fb0-281c-4181-a568-0b908d88fb51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f9a7820-1a52-4d2f-a104-6afc6c64481f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90d02fb0-281c-4181-a568-0b908d88fb51",
                    "LayerId": "b72a2cec-93cd-4678-94ec-87dc92972c93"
                }
            ]
        },
        {
            "id": "0e6c6a53-f72f-4862-8cb7-2fce25271d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "944f2300-2d6f-468c-aaf7-2166efb19f81",
            "compositeImage": {
                "id": "f8b27f9a-1805-46ae-9f56-6c09de3fc72f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e6c6a53-f72f-4862-8cb7-2fce25271d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1df247ac-c10d-41a4-8170-e27e6f3e7415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e6c6a53-f72f-4862-8cb7-2fce25271d65",
                    "LayerId": "b72a2cec-93cd-4678-94ec-87dc92972c93"
                }
            ]
        },
        {
            "id": "df1feded-14b7-4870-9fd2-2f9dcbbf5be3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "944f2300-2d6f-468c-aaf7-2166efb19f81",
            "compositeImage": {
                "id": "c71be1d3-53dc-498a-96dc-45c362bd27bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df1feded-14b7-4870-9fd2-2f9dcbbf5be3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04c68025-56ef-42dc-a523-90f15844e787",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df1feded-14b7-4870-9fd2-2f9dcbbf5be3",
                    "LayerId": "b72a2cec-93cd-4678-94ec-87dc92972c93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "b72a2cec-93cd-4678-94ec-87dc92972c93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "944f2300-2d6f-468c-aaf7-2166efb19f81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 47,
    "yorig": 22
}