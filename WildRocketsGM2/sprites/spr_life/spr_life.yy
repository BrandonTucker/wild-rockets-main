{
    "id": "3df27bd2-a74f-4f5d-b3a1-84c2a722a913",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_life",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": -10,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "545c8b08-6d64-407d-9cb4-1fe33fbab433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3df27bd2-a74f-4f5d-b3a1-84c2a722a913",
            "compositeImage": {
                "id": "ff9c2aed-df66-495d-9ba9-99d0785dd479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "545c8b08-6d64-407d-9cb4-1fe33fbab433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99e3720f-6400-4d0d-8828-9dd77b8a0b77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "545c8b08-6d64-407d-9cb4-1fe33fbab433",
                    "LayerId": "731a5ad2-ad1a-4ec5-8b55-d3e9415ba8e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "731a5ad2-ad1a-4ec5-8b55-d3e9415ba8e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3df27bd2-a74f-4f5d-b3a1-84c2a722a913",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 18,
    "yorig": 18
}