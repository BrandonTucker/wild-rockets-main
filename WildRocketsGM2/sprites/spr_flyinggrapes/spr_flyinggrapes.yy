{
    "id": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flyinggrapes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78202915-f175-444a-bdca-cb6cb7cfe5bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "706b1b52-007f-4509-b65f-efaccd09af09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78202915-f175-444a-bdca-cb6cb7cfe5bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e83fb07-04fb-484c-aa36-96d2d41be911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78202915-f175-444a-bdca-cb6cb7cfe5bc",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "8921004e-34df-403a-9db9-76c06b941587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "1cd5d4af-3930-47be-9347-e453af053ee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8921004e-34df-403a-9db9-76c06b941587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18bbe217-d4ab-419e-9a69-b073acc6da11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8921004e-34df-403a-9db9-76c06b941587",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "08c56b7c-4145-4911-bf17-0f071c8af859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "d58db0a9-f6fc-4230-bbdb-98f773c307b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08c56b7c-4145-4911-bf17-0f071c8af859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e9c628b-63a8-4b48-93a1-0351b8c7ac66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08c56b7c-4145-4911-bf17-0f071c8af859",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "1565934e-421d-4135-b70f-46b95e0a73bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "1d7d6c70-47fb-43b7-82c6-1237f330ee95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1565934e-421d-4135-b70f-46b95e0a73bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeac2bab-d142-44fc-bbde-fe96d6e1b551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1565934e-421d-4135-b70f-46b95e0a73bd",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "77422e4f-c37c-436a-b440-3d3e6a0c34fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "0be31c62-e7fb-4d6e-8a00-165a8886135b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77422e4f-c37c-436a-b440-3d3e6a0c34fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86675fb6-1c41-4411-a5b8-f4f312ead5a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77422e4f-c37c-436a-b440-3d3e6a0c34fe",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "8db04936-a3c3-4a31-9686-9d327d6da7cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "ae9d75a8-fefd-4903-8d67-b672bf4dca68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8db04936-a3c3-4a31-9686-9d327d6da7cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "740c6591-d228-4d53-af90-eedcb2666e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8db04936-a3c3-4a31-9686-9d327d6da7cb",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "b3b0573d-bf43-4d43-8820-454767c7764d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "9ef0b785-7a88-4643-9f22-7f50c49bea33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b0573d-bf43-4d43-8820-454767c7764d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3e4daa-3d8c-45a1-adba-13fc667d990d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b0573d-bf43-4d43-8820-454767c7764d",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "fc019da7-eb23-4a7d-8eda-e2e5e9a4987e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "215318e9-fcba-4b0c-97f8-0078499da6bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc019da7-eb23-4a7d-8eda-e2e5e9a4987e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "017e1378-fe6c-4e1f-9a0d-71dbed6c5843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc019da7-eb23-4a7d-8eda-e2e5e9a4987e",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "f2558274-080f-4d32-ab21-4312b29b7545",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "d36644f2-5f65-4f6a-8fd5-faf8d9d8da64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2558274-080f-4d32-ab21-4312b29b7545",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9c7be82-f3c0-4d2a-9cb5-43000bab89f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2558274-080f-4d32-ab21-4312b29b7545",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "85a548f9-2b0f-42aa-b7af-65d6feaea328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "b9671521-5c92-4c5f-b966-5d77c0a0890f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85a548f9-2b0f-42aa-b7af-65d6feaea328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e548cdb-5614-40f1-8454-a7d1c7aa1daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85a548f9-2b0f-42aa-b7af-65d6feaea328",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "9723f6f2-32ba-46ef-9885-6e4c7be548f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "d913a097-ef78-4e92-9f22-0fe08cc04e4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9723f6f2-32ba-46ef-9885-6e4c7be548f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4135d440-777a-40ad-932d-47652027aa19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9723f6f2-32ba-46ef-9885-6e4c7be548f3",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "1e51b553-5ae4-467c-87d2-faefe7078891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "1068a7fc-a4d1-4004-87c6-17bb3bd5e79d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e51b553-5ae4-467c-87d2-faefe7078891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5876e920-e6be-4afe-b4ff-6479e9ecd2db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e51b553-5ae4-467c-87d2-faefe7078891",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "001ea112-784d-40e7-84cc-ff68f84c9699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "456ad4ea-b10c-41d9-af3f-c8bbf434cf30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "001ea112-784d-40e7-84cc-ff68f84c9699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7eeec6a-75bf-4755-aee9-970fc09a56a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "001ea112-784d-40e7-84cc-ff68f84c9699",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "0db757b9-d7f7-413c-b863-9e815440f0b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "cacc777e-18db-483d-ab12-ff4711789c93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0db757b9-d7f7-413c-b863-9e815440f0b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cc4de2b-ba0d-46c4-9d69-785804d0e6f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0db757b9-d7f7-413c-b863-9e815440f0b1",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        },
        {
            "id": "a2592a98-caab-4a46-a2b7-51e9d05bcbf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "compositeImage": {
                "id": "36d128bb-c45a-4082-a322-5918563b7212",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2592a98-caab-4a46-a2b7-51e9d05bcbf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f95132b-dd67-4b98-90de-7cb7c3a98d98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2592a98-caab-4a46-a2b7-51e9d05bcbf9",
                    "LayerId": "f0881fc2-e00d-455a-9c73-d570c0a284e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "f0881fc2-e00d-455a-9c73-d570c0a284e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1d08dcc-a058-4d0c-abfa-996d145b4d78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}