{
    "id": "19693492-cd41-4a0f-a653-8c4753951dc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship19button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 12,
    "bbox_right": 88,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4edb5d80-b472-4c4e-b48a-96ddc2f87020",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19693492-cd41-4a0f-a653-8c4753951dc5",
            "compositeImage": {
                "id": "8c1edf31-2aef-40d2-8933-e9c5ce63ab1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4edb5d80-b472-4c4e-b48a-96ddc2f87020",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6141d20-feed-4522-9af8-ca7538947707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4edb5d80-b472-4c4e-b48a-96ddc2f87020",
                    "LayerId": "d49d9898-d32b-421b-8c7e-92a84034801e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "d49d9898-d32b-421b-8c7e-92a84034801e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19693492-cd41-4a0f-a653-8c4753951dc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}