{
    "id": "e9ef46c4-79be-4962-ad73-297f81e4fbe3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yesno",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 3,
    "bbox_right": 119,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbaed430-c1b7-4936-a154-cc8e1f66780c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ef46c4-79be-4962-ad73-297f81e4fbe3",
            "compositeImage": {
                "id": "0d64fd0c-1d1b-4f38-8d50-c35d8ee856c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbaed430-c1b7-4936-a154-cc8e1f66780c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e77a15b3-8695-4de6-8b76-38cf1abe129a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbaed430-c1b7-4936-a154-cc8e1f66780c",
                    "LayerId": "0abb5e76-bfe1-4b0c-844d-1dd05d15a53d"
                }
            ]
        },
        {
            "id": "2db989dc-3cd1-4a6d-982a-dfe15b39fcaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ef46c4-79be-4962-ad73-297f81e4fbe3",
            "compositeImage": {
                "id": "2e7a3859-30c3-4954-9e1a-efe97b733f14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2db989dc-3cd1-4a6d-982a-dfe15b39fcaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16be5d34-0e6e-481d-afa5-2199df936284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2db989dc-3cd1-4a6d-982a-dfe15b39fcaa",
                    "LayerId": "0abb5e76-bfe1-4b0c-844d-1dd05d15a53d"
                }
            ]
        },
        {
            "id": "95394afa-294f-4b7f-ac9f-77b82725c0f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ef46c4-79be-4962-ad73-297f81e4fbe3",
            "compositeImage": {
                "id": "d993a1f8-ed57-415a-a17b-c2bb617ddf42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95394afa-294f-4b7f-ac9f-77b82725c0f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c86d096-dcfd-4d26-80db-d50af0163328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95394afa-294f-4b7f-ac9f-77b82725c0f2",
                    "LayerId": "0abb5e76-bfe1-4b0c-844d-1dd05d15a53d"
                }
            ]
        },
        {
            "id": "1a62de64-5cea-41b2-9ee3-3f3c3e44e468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ef46c4-79be-4962-ad73-297f81e4fbe3",
            "compositeImage": {
                "id": "96f8d959-6bc9-476f-9dd5-7e45f6afe4ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a62de64-5cea-41b2-9ee3-3f3c3e44e468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "224df82b-de55-4267-bec0-a2dd64c202fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a62de64-5cea-41b2-9ee3-3f3c3e44e468",
                    "LayerId": "0abb5e76-bfe1-4b0c-844d-1dd05d15a53d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 89,
    "layers": [
        {
            "id": "0abb5e76-bfe1-4b0c-844d-1dd05d15a53d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9ef46c4-79be-4962-ad73-297f81e4fbe3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 130,
    "xorig": 65,
    "yorig": 44
}