{
    "id": "6476bb16-5afd-4227-8908-31f69d6c65a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange32buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "221ec912-8954-4e76-b5a8-eeca32408606",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "compositeImage": {
                "id": "1dd86add-322c-44bb-81c1-e7aa45d53641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "221ec912-8954-4e76-b5a8-eeca32408606",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "873e91e9-8134-4ecd-b351-69076c100547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "221ec912-8954-4e76-b5a8-eeca32408606",
                    "LayerId": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d"
                }
            ]
        },
        {
            "id": "98cc0136-2526-4a53-9735-68434b78ebd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "compositeImage": {
                "id": "5e698266-eadd-462e-b9b3-e954a1dd7b0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98cc0136-2526-4a53-9735-68434b78ebd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a01c0518-62bf-4e6c-a3c6-d73cf4a11515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98cc0136-2526-4a53-9735-68434b78ebd4",
                    "LayerId": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d"
                }
            ]
        },
        {
            "id": "e6bf742e-4de3-4ea9-8b11-71677da204e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "compositeImage": {
                "id": "5d038f81-fe89-42d0-9365-866e057eedce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6bf742e-4de3-4ea9-8b11-71677da204e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "076efb4d-9131-41f5-a062-00ea6361f4b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6bf742e-4de3-4ea9-8b11-71677da204e0",
                    "LayerId": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d"
                }
            ]
        },
        {
            "id": "7212bbbd-84dd-40ce-9174-5b9e35aeba78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "compositeImage": {
                "id": "ba07ad3f-87e0-47ff-94d8-1bb47b2e87e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7212bbbd-84dd-40ce-9174-5b9e35aeba78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc0752ec-0e78-4573-bdc4-61bf8d8e832f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7212bbbd-84dd-40ce-9174-5b9e35aeba78",
                    "LayerId": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d"
                }
            ]
        },
        {
            "id": "c819c770-a94e-4741-8b21-ff64217521a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "compositeImage": {
                "id": "2e0aa1bd-b221-4e54-b350-d4ba0286a3de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c819c770-a94e-4741-8b21-ff64217521a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04f77629-6423-4bcc-bc98-bd61ef7918d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c819c770-a94e-4741-8b21-ff64217521a6",
                    "LayerId": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d"
                }
            ]
        },
        {
            "id": "8b614781-5f7a-4c06-a12a-b0d32e44f9b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "compositeImage": {
                "id": "afd3c050-91a7-4f9a-b8ef-fd683f917942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b614781-5f7a-4c06-a12a-b0d32e44f9b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd84995-ae1e-4a9a-b3c5-4cd74450c35c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b614781-5f7a-4c06-a12a-b0d32e44f9b1",
                    "LayerId": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d"
                }
            ]
        },
        {
            "id": "c70641d0-6242-483d-a065-40d3fa4f89af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "compositeImage": {
                "id": "fb9291d6-0c5a-4502-adc2-2bcb156a1f3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70641d0-6242-483d-a065-40d3fa4f89af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "820829d1-2e77-4aea-afd8-02c7c7fe6803",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70641d0-6242-483d-a065-40d3fa4f89af",
                    "LayerId": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d"
                }
            ]
        },
        {
            "id": "72fb9617-2c08-40d8-9488-732ebf31f45c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "compositeImage": {
                "id": "34abdc08-5bea-4ce2-b3ce-b39d288c0f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72fb9617-2c08-40d8-9488-732ebf31f45c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53e9e457-b052-48df-9040-592aa123be70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72fb9617-2c08-40d8-9488-732ebf31f45c",
                    "LayerId": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d"
                }
            ]
        },
        {
            "id": "cefb5fcf-02ae-4b75-b620-a29900a71cd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "compositeImage": {
                "id": "0ed820b2-b79b-458b-81c7-6891ac8590b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cefb5fcf-02ae-4b75-b620-a29900a71cd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c390777-3d6f-4c36-9799-d402cce01130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cefb5fcf-02ae-4b75-b620-a29900a71cd4",
                    "LayerId": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d"
                }
            ]
        },
        {
            "id": "bf5bd742-bfdf-49bd-9e82-81bf89e960ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "compositeImage": {
                "id": "7af7c109-a175-4be5-a192-e627bb010cd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf5bd742-bfdf-49bd-9e82-81bf89e960ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d33aabf3-e7ff-4e08-aa39-b29d70a6afdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf5bd742-bfdf-49bd-9e82-81bf89e960ed",
                    "LayerId": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "fd8a63a5-9005-4fb8-8db0-ea9e7a25047d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6476bb16-5afd-4227-8908-31f69d6c65a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}