{
    "id": "9afe316f-7702-40c4-8d68-ead213c1cc4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pearl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 7,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9f3bc4b-8a50-42db-971a-e3bd1f3ab24c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afe316f-7702-40c4-8d68-ead213c1cc4a",
            "compositeImage": {
                "id": "5d75d2d5-acc7-473e-b490-bc7dfee54c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9f3bc4b-8a50-42db-971a-e3bd1f3ab24c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3161844c-b63c-4a4f-88c4-a32b8bebe34e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f3bc4b-8a50-42db-971a-e3bd1f3ab24c",
                    "LayerId": "52b5f5e9-4704-4e83-bafb-7db869629bb2"
                }
            ]
        },
        {
            "id": "49f87b53-99ed-4d17-9a08-0687ef57a094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afe316f-7702-40c4-8d68-ead213c1cc4a",
            "compositeImage": {
                "id": "19c0ca02-399e-444e-b23b-060a3faab5ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49f87b53-99ed-4d17-9a08-0687ef57a094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c755b1-aad5-4d2d-8ce2-70534db51bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49f87b53-99ed-4d17-9a08-0687ef57a094",
                    "LayerId": "52b5f5e9-4704-4e83-bafb-7db869629bb2"
                }
            ]
        },
        {
            "id": "ce76e868-f847-4ff1-8b33-74f1ff89f40b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afe316f-7702-40c4-8d68-ead213c1cc4a",
            "compositeImage": {
                "id": "4ff6b5b7-7f88-4b15-a9ce-07e075633b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce76e868-f847-4ff1-8b33-74f1ff89f40b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36881f95-5c46-4415-bcb5-7decbd24bea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce76e868-f847-4ff1-8b33-74f1ff89f40b",
                    "LayerId": "52b5f5e9-4704-4e83-bafb-7db869629bb2"
                }
            ]
        },
        {
            "id": "591249dc-7f5f-4dfd-be91-1e4f3255bf5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afe316f-7702-40c4-8d68-ead213c1cc4a",
            "compositeImage": {
                "id": "60652b00-fa15-49f0-9186-49cf2b777b63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "591249dc-7f5f-4dfd-be91-1e4f3255bf5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d56ae11-a84b-47d2-8d76-6607946cfebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "591249dc-7f5f-4dfd-be91-1e4f3255bf5c",
                    "LayerId": "52b5f5e9-4704-4e83-bafb-7db869629bb2"
                }
            ]
        },
        {
            "id": "19ec50bf-ab70-4532-9b53-6115ef40a3a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afe316f-7702-40c4-8d68-ead213c1cc4a",
            "compositeImage": {
                "id": "df8cf7a5-bfdd-4333-beca-959d2b48386d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ec50bf-ab70-4532-9b53-6115ef40a3a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f218254-da1d-421d-b143-714bbf1136ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ec50bf-ab70-4532-9b53-6115ef40a3a0",
                    "LayerId": "52b5f5e9-4704-4e83-bafb-7db869629bb2"
                }
            ]
        },
        {
            "id": "790b3caa-367b-46f4-b91e-fcf13ba91bdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afe316f-7702-40c4-8d68-ead213c1cc4a",
            "compositeImage": {
                "id": "3dff175a-0ca6-4b3b-8629-be1292501001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "790b3caa-367b-46f4-b91e-fcf13ba91bdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99e1b21c-e2f4-4f02-99c5-34da98aa57f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "790b3caa-367b-46f4-b91e-fcf13ba91bdb",
                    "LayerId": "52b5f5e9-4704-4e83-bafb-7db869629bb2"
                }
            ]
        },
        {
            "id": "02b8ae3a-a644-4eca-854a-5d693e9997f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afe316f-7702-40c4-8d68-ead213c1cc4a",
            "compositeImage": {
                "id": "a826352e-de46-4b3c-8b9b-0976960163fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02b8ae3a-a644-4eca-854a-5d693e9997f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58190fa1-cf77-4997-b518-f43531f9f194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02b8ae3a-a644-4eca-854a-5d693e9997f9",
                    "LayerId": "52b5f5e9-4704-4e83-bafb-7db869629bb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "52b5f5e9-4704-4e83-bafb-7db869629bb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9afe316f-7702-40c4-8d68-ead213c1cc4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 26
}