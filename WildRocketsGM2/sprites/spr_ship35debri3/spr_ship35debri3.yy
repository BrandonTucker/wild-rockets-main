{
    "id": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship35debri3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e6f9592-21ce-4a06-b23e-d79629805b9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "9aef9c2d-d471-457f-a9f6-1d86238387c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e6f9592-21ce-4a06-b23e-d79629805b9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f72e682-66b0-4753-ab81-2120531d3c77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e6f9592-21ce-4a06-b23e-d79629805b9b",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "bacc27bc-0409-4e09-b033-c3770afafd5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "35f3596d-22fc-4603-849a-06727ece227c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bacc27bc-0409-4e09-b033-c3770afafd5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c97b5e15-3fa6-4e52-94be-d212fb676528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bacc27bc-0409-4e09-b033-c3770afafd5f",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "8fe63200-be16-4d9e-b7a6-ec3015d4f2fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "091db64f-d80b-495d-89b6-5cafda169926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fe63200-be16-4d9e-b7a6-ec3015d4f2fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd60794e-31e7-4416-a680-a46b91b48efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fe63200-be16-4d9e-b7a6-ec3015d4f2fd",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "b6d4d2d6-e2e6-4b0c-9755-2ad584ba355f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "d75aece5-a746-4ffe-afe9-423e786c0947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d4d2d6-e2e6-4b0c-9755-2ad584ba355f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "016779a5-d054-49e0-8c0d-cd54316ee295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d4d2d6-e2e6-4b0c-9755-2ad584ba355f",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "82d5e8c8-ebff-4171-874f-aea44af0a32f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "b3e3924f-84c4-4170-9251-4d2962c9858a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82d5e8c8-ebff-4171-874f-aea44af0a32f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc71cd4-70e0-41c0-ae7b-9ab1b47eba8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82d5e8c8-ebff-4171-874f-aea44af0a32f",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "27127da7-9e96-4f28-b77d-40f9cbd33b5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "4ef3bab8-93ff-4952-9b0a-e92958b39b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27127da7-9e96-4f28-b77d-40f9cbd33b5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4f3c7cb-ef03-4e98-b8ec-64f5e33f0b55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27127da7-9e96-4f28-b77d-40f9cbd33b5b",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "06e24d50-425a-4e7b-b527-97795d64269b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "fb74f930-140b-4588-8980-a3d49ab6ca5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06e24d50-425a-4e7b-b527-97795d64269b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1297bdd4-7454-4db6-a71f-a5179dfed295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06e24d50-425a-4e7b-b527-97795d64269b",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "d8c16320-f590-4d83-9571-50742f941363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "65880d50-3aaf-42b0-8cec-9211cba56161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8c16320-f590-4d83-9571-50742f941363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6358d0f2-2a23-420a-ba2b-06200d0559fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8c16320-f590-4d83-9571-50742f941363",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "6e6fdff5-8fae-44b8-b1b1-3ae396041ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "50b951d6-fa92-4e54-9c34-cd5ff9e7f8f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e6fdff5-8fae-44b8-b1b1-3ae396041ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c658a11-0efa-46a4-aec5-8089ce774ede",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e6fdff5-8fae-44b8-b1b1-3ae396041ed4",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "c4313840-6f0a-43e4-9b94-68b4e26f11d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "a7dbddef-b3f9-499c-b325-2c605b023d9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4313840-6f0a-43e4-9b94-68b4e26f11d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37169221-ba24-45c7-ab23-d684a125f078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4313840-6f0a-43e4-9b94-68b4e26f11d9",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "5fcef7e2-69d5-44b2-94c0-7675de38f051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "0e2d6939-53fc-4c94-b13d-017523a7edbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fcef7e2-69d5-44b2-94c0-7675de38f051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a7096b9-cc1a-4388-a5c8-6b46e2757530",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fcef7e2-69d5-44b2-94c0-7675de38f051",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "b5ba82e5-f0ea-45bc-a6dd-c675eec81304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "2f139fb9-5f95-4656-b24b-fb19620e5028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5ba82e5-f0ea-45bc-a6dd-c675eec81304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6746af0f-0941-4e08-a7ef-a0ffa8bef4eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5ba82e5-f0ea-45bc-a6dd-c675eec81304",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "e951d6a9-0b04-47c1-9fa1-bd920979353a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "4ab7d1bb-6a8d-4239-9a99-f62e685119c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e951d6a9-0b04-47c1-9fa1-bd920979353a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "960e616f-2111-4948-a4ff-f5b70808dc01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e951d6a9-0b04-47c1-9fa1-bd920979353a",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "4503e66f-4c2a-49b3-80b3-e8788c6eea71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "67856288-e668-4811-894d-2cb8996ce7f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4503e66f-4c2a-49b3-80b3-e8788c6eea71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a893099-8ee1-404a-8b40-486b02d15df0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4503e66f-4c2a-49b3-80b3-e8788c6eea71",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        },
        {
            "id": "0973c7c2-d464-4a10-8816-389ca6263c2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "compositeImage": {
                "id": "db9d8116-b0f2-4018-8528-8e4b2629c8c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0973c7c2-d464-4a10-8816-389ca6263c2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d6e0c50-6f6c-4dbc-9562-c45f6de94d1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0973c7c2-d464-4a10-8816-389ca6263c2a",
                    "LayerId": "ab039787-c0cf-4d12-aa30-c688dcd97fcd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "ab039787-c0cf-4d12-aa30-c688dcd97fcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4314e90d-70f9-4d54-8923-e42b680cd5c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 41,
    "yorig": 37
}