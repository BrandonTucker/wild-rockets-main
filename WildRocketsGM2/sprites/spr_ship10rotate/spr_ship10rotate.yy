{
    "id": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship10rotate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3975312c-50bc-4be1-a0fc-7a8b0e280ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "64f13661-742e-458a-aada-f1d3be2f6d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3975312c-50bc-4be1-a0fc-7a8b0e280ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc543109-6c42-4322-94f1-86c9e1704d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3975312c-50bc-4be1-a0fc-7a8b0e280ce3",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "0b4bb87b-1da0-4e5f-b94d-74e5929aa658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "1ce285f6-76d6-4ef5-ada0-51483f685982",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b4bb87b-1da0-4e5f-b94d-74e5929aa658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dd30ea1-7fe3-45ba-a4e0-d30aba400213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b4bb87b-1da0-4e5f-b94d-74e5929aa658",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "8748d0e9-2f9f-43d5-96c2-1856539ece14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "45f96937-9c86-4607-921e-4676250ff9a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8748d0e9-2f9f-43d5-96c2-1856539ece14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fc621f0-c628-4f2e-9601-afd4c7bcd358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8748d0e9-2f9f-43d5-96c2-1856539ece14",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "181bed0e-caf2-4216-a78d-c41fcc4feaab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "4dceab69-acb2-4256-bd06-803b7c85904c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "181bed0e-caf2-4216-a78d-c41fcc4feaab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5366dcd-ac4c-4531-bc45-1bafd37c3345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "181bed0e-caf2-4216-a78d-c41fcc4feaab",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "e4317fa0-4bc0-4ce6-a31f-f9b303c3f058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "38dae27b-5e3c-418a-b623-21199bf3f452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4317fa0-4bc0-4ce6-a31f-f9b303c3f058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5881a91-e9b5-4ef5-b8d7-2a8765d24b05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4317fa0-4bc0-4ce6-a31f-f9b303c3f058",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "cd083a7d-57e4-4535-8f7e-71a75f8aa941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "055ab8ec-a2c6-46fe-8319-52d9b29cc00c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd083a7d-57e4-4535-8f7e-71a75f8aa941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "112bcd4d-7d1f-4916-86a9-4cdc88c60c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd083a7d-57e4-4535-8f7e-71a75f8aa941",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "ae06b69a-1bf9-462c-9292-ec227b9eda8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "e927cca6-38d8-49d4-937a-b93e316d9fcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae06b69a-1bf9-462c-9292-ec227b9eda8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc215705-32a3-48d1-a1a2-c82949476dfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae06b69a-1bf9-462c-9292-ec227b9eda8e",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "8baad46d-7aae-4e24-95de-6328cec30fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "337d05fc-1720-43cb-8eee-2599b4209ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8baad46d-7aae-4e24-95de-6328cec30fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f985a1-27aa-4d4f-8d70-0a62ab0ae433",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8baad46d-7aae-4e24-95de-6328cec30fac",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "09a5e1f3-6979-42dc-8b7b-0ee252dafb75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "16c63155-6fb0-4e00-903b-4e4a185c7431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09a5e1f3-6979-42dc-8b7b-0ee252dafb75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "426d429d-472a-404a-9a15-860fd72e2806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09a5e1f3-6979-42dc-8b7b-0ee252dafb75",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "cffcd663-4c04-491c-82ec-cdd3f5f51f41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "c56353d0-4563-4230-b1cb-05c4282138f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cffcd663-4c04-491c-82ec-cdd3f5f51f41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e25e7ce-6d75-4cc8-8488-266a62ffd007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffcd663-4c04-491c-82ec-cdd3f5f51f41",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "1f69fdc1-bc21-4051-9e82-ff4aa2e660f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "75c3b0e6-4bf7-4aa5-9ffe-5ebb0eaecf92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f69fdc1-bc21-4051-9e82-ff4aa2e660f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3779784d-5dde-4c98-96e1-a22a6fc0661f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f69fdc1-bc21-4051-9e82-ff4aa2e660f4",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "420ed1eb-9d9c-46a0-bd44-700685684e4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "961c6ef0-8692-4d96-ba00-ffd022a68232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "420ed1eb-9d9c-46a0-bd44-700685684e4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49c973b1-1203-4608-a1cb-d36f944c453d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "420ed1eb-9d9c-46a0-bd44-700685684e4b",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "48fc2dec-dac0-419d-a966-9c8665e5a4fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "95040a73-a291-40f9-9e3f-f1712ed803b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48fc2dec-dac0-419d-a966-9c8665e5a4fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0cc159-43cd-4c4b-bb6f-c4ac7b0d43aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48fc2dec-dac0-419d-a966-9c8665e5a4fc",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "11e201b5-9c3b-488c-9e62-5f5e95b0a24b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "b45db8ac-eee7-4403-ac12-8ecbd55c254f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11e201b5-9c3b-488c-9e62-5f5e95b0a24b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a7902cc-25d4-444f-af62-f9ecac4d6239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11e201b5-9c3b-488c-9e62-5f5e95b0a24b",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "34d32ee8-8a62-4b4b-836e-ed8d5810a5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "8dcd3a14-812f-4503-9266-863427fe1524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34d32ee8-8a62-4b4b-836e-ed8d5810a5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d973638-6738-4d1b-9f8d-5dc57138dde7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34d32ee8-8a62-4b4b-836e-ed8d5810a5b0",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "153812aa-990a-476d-8a44-68de5d760cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "d0bfb9af-702e-4cec-8390-f7a7d36e006b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "153812aa-990a-476d-8a44-68de5d760cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2dce97e-92db-4bcb-a627-6e413b0bd7f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "153812aa-990a-476d-8a44-68de5d760cde",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "8a723dea-6abf-4ddf-bbb1-414a1f5e88f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "8afa8d28-bbc8-4f11-b436-4f2fb6f4edfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a723dea-6abf-4ddf-bbb1-414a1f5e88f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cfc8365-394c-4ede-b549-2f1553c6ea08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a723dea-6abf-4ddf-bbb1-414a1f5e88f3",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "8d4d0ecf-2fe8-4e4d-8b85-780e92311e17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "bcb67492-7253-45c3-955d-4a28aa21da34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d4d0ecf-2fe8-4e4d-8b85-780e92311e17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63708156-ae04-4782-942a-1d0db002bfa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d4d0ecf-2fe8-4e4d-8b85-780e92311e17",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "f42529ec-ac0a-4520-bf8b-3b6a078e8462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "aa31e417-a9c1-4663-8641-0aa1892247f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f42529ec-ac0a-4520-bf8b-3b6a078e8462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a07b2bd-55e5-4935-8ee2-98964452016a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f42529ec-ac0a-4520-bf8b-3b6a078e8462",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "c84e7982-aaae-4638-91a1-113c5ba36a3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "0971363f-679c-4ed7-9fc9-a5bc329bb2a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c84e7982-aaae-4638-91a1-113c5ba36a3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acbf2939-f028-47db-9023-c71e3c4e4324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c84e7982-aaae-4638-91a1-113c5ba36a3f",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "4177ae54-a804-457b-a16b-48ccea5cd251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "f4df2424-5bbb-4871-a81a-f528f0d6c410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4177ae54-a804-457b-a16b-48ccea5cd251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05d30473-720d-429f-b659-fd8bad7f01fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4177ae54-a804-457b-a16b-48ccea5cd251",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "7e114076-b03c-4863-9d94-072dd15fa1f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "3916aab2-eb2d-4a1a-a609-2fe0408e3044",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e114076-b03c-4863-9d94-072dd15fa1f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5f9d472-f801-4214-b0dc-2781587f3cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e114076-b03c-4863-9d94-072dd15fa1f9",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "8cf1be59-f953-4f80-8045-667b9799ae12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "de43c035-d7cf-48b4-ab3d-4a7439c2c244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cf1be59-f953-4f80-8045-667b9799ae12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b85f5098-7131-4827-acb2-1393ae281a0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cf1be59-f953-4f80-8045-667b9799ae12",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "7b05d5a3-8050-4931-bc93-e1d6a147810c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "499448b5-b586-411f-a571-91f9b1c96465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b05d5a3-8050-4931-bc93-e1d6a147810c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd2ce487-72f5-4b5e-8caa-16d978a0e6de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b05d5a3-8050-4931-bc93-e1d6a147810c",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "ec92a015-491f-402e-b5b9-24135864b212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "1733e706-39d3-41b2-8e6f-f26ca0d24a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec92a015-491f-402e-b5b9-24135864b212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cb6ae3e-c370-470b-a99f-dd3858fcbbf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec92a015-491f-402e-b5b9-24135864b212",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "def837d2-8257-4b3c-add1-1bbbd3c639f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "615c5277-74e5-4ff8-9876-5da45b044305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "def837d2-8257-4b3c-add1-1bbbd3c639f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fddb210e-94ac-43c6-bc84-f28ded4e7457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "def837d2-8257-4b3c-add1-1bbbd3c639f1",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "b60b1863-a9e2-43dc-9566-ec2d13637791",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "838e72c4-938c-404e-b6c7-f03821f627f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b60b1863-a9e2-43dc-9566-ec2d13637791",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18ddce69-917a-4a1c-b703-42d5f3bc4dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b60b1863-a9e2-43dc-9566-ec2d13637791",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "e0513af1-5b60-4f7e-919b-bbc81000b921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "e943e542-c4b3-4b59-8f9a-928d823d7718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0513af1-5b60-4f7e-919b-bbc81000b921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f369d25f-d9a9-4ab5-a0e3-d602e85e3383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0513af1-5b60-4f7e-919b-bbc81000b921",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "ad73dbed-5a45-47ee-a3ac-6057b3431283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "0fe81000-5385-4df8-ab10-df76feb40b60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad73dbed-5a45-47ee-a3ac-6057b3431283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efa7a403-b77a-4c8e-b14d-89fc1c4b87ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad73dbed-5a45-47ee-a3ac-6057b3431283",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        },
        {
            "id": "81b48d33-3da4-4901-a752-3efca5457168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "compositeImage": {
                "id": "4e97edec-7247-4b73-a84e-54acda265feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b48d33-3da4-4901-a752-3efca5457168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3b385c7-75c8-4da8-b5dc-b51ddd2a1a21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b48d33-3da4-4901-a752-3efca5457168",
                    "LayerId": "69f296a6-982f-4eca-945f-140dbe4d7e18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "69f296a6-982f-4eca-945f-140dbe4d7e18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47cba3e9-d00d-4821-b43d-11aa2e824dcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 32,
    "yorig": 75
}