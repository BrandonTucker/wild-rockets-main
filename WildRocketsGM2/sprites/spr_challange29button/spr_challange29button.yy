{
    "id": "92388c1d-bb6d-456b-9906-e9556f5c24be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange29button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bde8b671-f3f4-45c3-92c5-8df373a95946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92388c1d-bb6d-456b-9906-e9556f5c24be",
            "compositeImage": {
                "id": "dfe3ecbd-26f9-47ba-89fb-bdd635269e71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde8b671-f3f4-45c3-92c5-8df373a95946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688e9910-e5f0-41d5-88a3-6ee4add7e7fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde8b671-f3f4-45c3-92c5-8df373a95946",
                    "LayerId": "7de29cf6-0450-4120-93e3-f042c3b9de64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7de29cf6-0450-4120-93e3-f042c3b9de64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92388c1d-bb6d-456b-9906-e9556f5c24be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}