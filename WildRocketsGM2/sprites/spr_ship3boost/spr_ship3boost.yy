{
    "id": "ece48550-589e-4f3f-87da-cafcb75cb9e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship3boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 28,
    "bbox_right": 53,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa966e55-dd8e-4e22-a163-c038d7832732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece48550-589e-4f3f-87da-cafcb75cb9e8",
            "compositeImage": {
                "id": "a33d7951-dd82-42a0-aa91-1d87463e4d9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa966e55-dd8e-4e22-a163-c038d7832732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff8d7d3-f46c-4a78-a939-bc44ba7140ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa966e55-dd8e-4e22-a163-c038d7832732",
                    "LayerId": "b9ee2bf3-8ed5-40d9-a095-466360c1dc0e"
                }
            ]
        },
        {
            "id": "94cbb50b-d3c1-4428-b58e-609f9da20be2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece48550-589e-4f3f-87da-cafcb75cb9e8",
            "compositeImage": {
                "id": "38ab6cd9-3b61-4d8f-a9d1-9bddfcd68569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94cbb50b-d3c1-4428-b58e-609f9da20be2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "264a9a0f-e2ed-44fa-9e15-923427dba649",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94cbb50b-d3c1-4428-b58e-609f9da20be2",
                    "LayerId": "b9ee2bf3-8ed5-40d9-a095-466360c1dc0e"
                }
            ]
        },
        {
            "id": "7cf77115-d21c-4e49-8874-18b223e2f442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece48550-589e-4f3f-87da-cafcb75cb9e8",
            "compositeImage": {
                "id": "99faf5c6-667e-428a-ab3d-ef051061b0e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf77115-d21c-4e49-8874-18b223e2f442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "567845df-c765-4512-a2f9-6ad889df0a0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf77115-d21c-4e49-8874-18b223e2f442",
                    "LayerId": "b9ee2bf3-8ed5-40d9-a095-466360c1dc0e"
                }
            ]
        },
        {
            "id": "ca6626d5-1d12-4494-8312-d7cada18decc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece48550-589e-4f3f-87da-cafcb75cb9e8",
            "compositeImage": {
                "id": "3dda4d0b-267a-4413-97b2-a1bec869ec01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6626d5-1d12-4494-8312-d7cada18decc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a587a9b-546c-4a2a-9c2a-a4bee71744c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6626d5-1d12-4494-8312-d7cada18decc",
                    "LayerId": "b9ee2bf3-8ed5-40d9-a095-466360c1dc0e"
                }
            ]
        },
        {
            "id": "7eefdce7-7d9e-425b-ab2c-f20c48250cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece48550-589e-4f3f-87da-cafcb75cb9e8",
            "compositeImage": {
                "id": "94c3e4e0-8726-4f39-9810-0e2e7948f75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eefdce7-7d9e-425b-ab2c-f20c48250cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d10fd695-b1e1-46b1-93af-863ca52bd171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eefdce7-7d9e-425b-ab2c-f20c48250cc4",
                    "LayerId": "b9ee2bf3-8ed5-40d9-a095-466360c1dc0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 166,
    "layers": [
        {
            "id": "b9ee2bf3-8ed5-40d9-a095-466360c1dc0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ece48550-589e-4f3f-87da-cafcb75cb9e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 42
}