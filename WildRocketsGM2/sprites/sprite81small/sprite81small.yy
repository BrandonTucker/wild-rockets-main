{
    "id": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite81small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 11,
    "bbox_right": 22,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3737ddf0-e347-4cb8-9434-2e0488db0047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "compositeImage": {
                "id": "0b533f3a-38cb-4ad7-ba90-10734709a723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3737ddf0-e347-4cb8-9434-2e0488db0047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c145f48-8376-4bc2-903a-0aec4aba1276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3737ddf0-e347-4cb8-9434-2e0488db0047",
                    "LayerId": "867e5014-e2cf-4f58-a4dc-d45010ccd09c"
                }
            ]
        },
        {
            "id": "e95e1457-24c4-482b-ba97-1533bcc763de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "compositeImage": {
                "id": "615b0c48-fcfb-4f01-ab0e-b3934b4389b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e95e1457-24c4-482b-ba97-1533bcc763de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56fd7d9b-8df8-453a-82ca-ff1959c9f63b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e95e1457-24c4-482b-ba97-1533bcc763de",
                    "LayerId": "867e5014-e2cf-4f58-a4dc-d45010ccd09c"
                }
            ]
        },
        {
            "id": "11fcc5b7-ffeb-4255-8efc-70569ce7fb36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "compositeImage": {
                "id": "f2e60d92-548a-4674-b280-a4924e65439b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11fcc5b7-ffeb-4255-8efc-70569ce7fb36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56354d34-ee9a-442c-9151-d8b83ef2b5c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11fcc5b7-ffeb-4255-8efc-70569ce7fb36",
                    "LayerId": "867e5014-e2cf-4f58-a4dc-d45010ccd09c"
                }
            ]
        },
        {
            "id": "1dcab111-b88f-46b3-91df-5631a582787b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "compositeImage": {
                "id": "120fa62e-5f42-44d0-9e6a-e7b8f4b8f55f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dcab111-b88f-46b3-91df-5631a582787b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3087ead5-0602-48e6-bed6-26b5eb645a6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dcab111-b88f-46b3-91df-5631a582787b",
                    "LayerId": "867e5014-e2cf-4f58-a4dc-d45010ccd09c"
                }
            ]
        },
        {
            "id": "e2bbbff9-72bd-4bcc-b662-b2481d17b45e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "compositeImage": {
                "id": "a249a73c-2399-4b59-aa9f-d2865d8a7ef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2bbbff9-72bd-4bcc-b662-b2481d17b45e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3752f35-714a-42c0-b65c-0b61505e0f42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2bbbff9-72bd-4bcc-b662-b2481d17b45e",
                    "LayerId": "867e5014-e2cf-4f58-a4dc-d45010ccd09c"
                }
            ]
        },
        {
            "id": "6a33c5d1-a05e-440c-9feb-4fe315a35927",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "compositeImage": {
                "id": "839cf9fd-4511-43dd-b785-fc392d1073bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a33c5d1-a05e-440c-9feb-4fe315a35927",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a642c1dd-7ad5-4481-9b85-8dab87daa5ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a33c5d1-a05e-440c-9feb-4fe315a35927",
                    "LayerId": "867e5014-e2cf-4f58-a4dc-d45010ccd09c"
                }
            ]
        },
        {
            "id": "1d805afd-f0c6-403e-a8a7-83a6ea4fc2fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "compositeImage": {
                "id": "629a87f5-4b61-4f9b-bac6-6463dbcce0a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d805afd-f0c6-403e-a8a7-83a6ea4fc2fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ee899a9-2af2-4c80-9caf-34ca9a7d6ea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d805afd-f0c6-403e-a8a7-83a6ea4fc2fb",
                    "LayerId": "867e5014-e2cf-4f58-a4dc-d45010ccd09c"
                }
            ]
        },
        {
            "id": "11a0449f-4d41-451b-a8cf-fa07a6996a4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "compositeImage": {
                "id": "6c988bcc-06ff-4f91-abd9-a7da886fd306",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11a0449f-4d41-451b-a8cf-fa07a6996a4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5f4eccc-e168-4b06-a7ed-6c6018750802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11a0449f-4d41-451b-a8cf-fa07a6996a4d",
                    "LayerId": "867e5014-e2cf-4f58-a4dc-d45010ccd09c"
                }
            ]
        },
        {
            "id": "6ee698bd-653f-4e15-9a4b-a91433ad560a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "compositeImage": {
                "id": "5c940f9d-17f1-43ea-837f-98cbc0c74977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee698bd-653f-4e15-9a4b-a91433ad560a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2ac753e-6900-46f2-b60a-42ecf844aa04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee698bd-653f-4e15-9a4b-a91433ad560a",
                    "LayerId": "867e5014-e2cf-4f58-a4dc-d45010ccd09c"
                }
            ]
        },
        {
            "id": "3ad4a0bd-796e-4415-8abc-1c6c884bfdc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "compositeImage": {
                "id": "bd4b3bb3-ef39-41e2-a47c-a02d29a3ecc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ad4a0bd-796e-4415-8abc-1c6c884bfdc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98c47b6-3f39-4151-bf5e-0e847b8b56cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ad4a0bd-796e-4415-8abc-1c6c884bfdc3",
                    "LayerId": "867e5014-e2cf-4f58-a4dc-d45010ccd09c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "867e5014-e2cf-4f58-a4dc-d45010ccd09c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f601b07c-c5d8-43a1-b299-7d730ac32eed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 18,
    "yorig": 18
}