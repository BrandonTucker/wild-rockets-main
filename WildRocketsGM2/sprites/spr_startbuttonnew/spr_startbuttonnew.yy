{
    "id": "0294c4de-1353-4639-be1d-7336257d3444",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startbuttonnew",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 12,
    "bbox_right": 127,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87e7d711-d61e-4d0d-a583-83e9ef733f3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "daf48aad-d40b-424d-b0b5-39f49e535333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87e7d711-d61e-4d0d-a583-83e9ef733f3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b5fbc05-0ba3-412c-96a2-b9385ad34b0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87e7d711-d61e-4d0d-a583-83e9ef733f3f",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "92c07bda-e595-41f3-9de0-8dbc9d6e3a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "91d992f4-dfa1-4304-b84b-ed48b343d714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92c07bda-e595-41f3-9de0-8dbc9d6e3a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2742279-76f6-4a38-8eaa-c3f55fda62e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92c07bda-e595-41f3-9de0-8dbc9d6e3a51",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "a8c49568-cee4-438d-9591-2cdd36e422e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "15e530ae-dd05-42d9-8481-407d9e9274e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8c49568-cee4-438d-9591-2cdd36e422e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a7a87f2-9c99-40fb-801a-4e98d2dfd03e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8c49568-cee4-438d-9591-2cdd36e422e0",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "635f6e85-a3da-4810-8214-70b25cb0b3b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "3e68df4f-e9db-4aa5-b2ba-19d446821dfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "635f6e85-a3da-4810-8214-70b25cb0b3b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e7db40e-1bf0-4fa1-be6a-4f1138347411",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "635f6e85-a3da-4810-8214-70b25cb0b3b7",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "2bd738ad-27fa-4333-8695-2504714d9b0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "224cea2f-49c0-4704-b441-c875139fe9ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bd738ad-27fa-4333-8695-2504714d9b0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd5e8151-069e-49f7-b6a5-3047fa1f38c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bd738ad-27fa-4333-8695-2504714d9b0f",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "2de224f5-46bc-4b30-afe1-45c7ccc27fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "68832a15-1eea-4e74-a0cd-df27e2661d71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2de224f5-46bc-4b30-afe1-45c7ccc27fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acaf8b3a-4489-4acc-b7f8-f8a9a691be74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2de224f5-46bc-4b30-afe1-45c7ccc27fce",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "9a1b590c-b470-4d97-abb3-46664c95fe76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "e1412d41-9faa-4951-8ea8-553045ac5abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a1b590c-b470-4d97-abb3-46664c95fe76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc41270a-93ba-4258-9221-099d4c020eaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a1b590c-b470-4d97-abb3-46664c95fe76",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "0eeefc1c-4a3e-4107-a549-7060fc4b6c58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "fdc32b91-4c69-4190-bf15-e9079f8ddf6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eeefc1c-4a3e-4107-a549-7060fc4b6c58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a2c414b-1af3-4344-8ceb-6b8dd9403f59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eeefc1c-4a3e-4107-a549-7060fc4b6c58",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "8615b478-2894-4b4f-9278-0a91a1f44172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "4f7b1498-d433-4d08-ae79-cfe8f4182919",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8615b478-2894-4b4f-9278-0a91a1f44172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a98b1b0c-f036-4da6-9b83-703d84a244ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8615b478-2894-4b4f-9278-0a91a1f44172",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "7a363e84-a3a0-437d-8edd-7f644592189a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "8ad3f2c3-6067-4676-841e-de2e0ee63e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a363e84-a3a0-437d-8edd-7f644592189a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5f9562d-5e8b-4dae-9e49-d2ab57349479",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a363e84-a3a0-437d-8edd-7f644592189a",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "3f535b0c-7245-4bdd-836b-1a91c6d5b493",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "49aa6ec5-cad7-43b8-91ef-17a6f1cbb550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f535b0c-7245-4bdd-836b-1a91c6d5b493",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6bb085-a0fa-4b85-bcef-717bd27e5d29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f535b0c-7245-4bdd-836b-1a91c6d5b493",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "e8a4c737-d650-4788-8124-2339c493246a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "7518ed4c-472a-4c4a-a125-61da72063ba7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8a4c737-d650-4788-8124-2339c493246a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c105f8-4973-411b-922c-7d5510c8462d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8a4c737-d650-4788-8124-2339c493246a",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "f53675a6-6f43-4c25-bd8b-11bd28e53ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "5e917d3f-3493-40fb-b215-fe9cdff3d4e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f53675a6-6f43-4c25-bd8b-11bd28e53ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a32e8e8-1597-43f4-988a-75120fb2bf01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f53675a6-6f43-4c25-bd8b-11bd28e53ef4",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "81d0da78-dd08-4884-8cf3-4485b831a84c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "c3935c99-bf3a-4361-84ee-3c720a9350cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81d0da78-dd08-4884-8cf3-4485b831a84c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56242f4d-8987-4bdb-8338-67a48e5955cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d0da78-dd08-4884-8cf3-4485b831a84c",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "ad9ea81b-3c81-4fe3-9fb9-b07d50f33a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "bc8407cb-59ce-4ad2-95cd-48a6f1d8d6ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad9ea81b-3c81-4fe3-9fb9-b07d50f33a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e15b3e-b242-4343-8052-a016c8be3104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad9ea81b-3c81-4fe3-9fb9-b07d50f33a12",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "62c2f902-280b-44cf-a50c-73d0084b37e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "35e19bac-f1a2-4a94-a320-808da6706ff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62c2f902-280b-44cf-a50c-73d0084b37e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47065a2c-f037-4a9f-95be-eac8973fbbfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62c2f902-280b-44cf-a50c-73d0084b37e3",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "80656329-7850-444f-97d2-c7a8b6c631aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "6bc00803-6186-4566-8f49-d85eaa6b07a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80656329-7850-444f-97d2-c7a8b6c631aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e9e49f0-af23-4f12-82c9-499f15731b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80656329-7850-444f-97d2-c7a8b6c631aa",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "19ece4df-c254-45ce-bda9-ab295acd0655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "1ad7cf1b-a32a-4141-b110-bca69d80d471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ece4df-c254-45ce-bda9-ab295acd0655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b583a958-754b-45f6-9b54-8038868cd690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ece4df-c254-45ce-bda9-ab295acd0655",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "bef0f820-3b37-4d67-b18f-58492037c12c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "ce4ec8f1-395a-4166-b8a3-d1ebd3cda036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bef0f820-3b37-4d67-b18f-58492037c12c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aba09893-58a3-44f1-a282-5410fbf45483",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bef0f820-3b37-4d67-b18f-58492037c12c",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        },
        {
            "id": "83219645-221b-4913-a17e-bdef67c28c49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "compositeImage": {
                "id": "5f98ef35-335f-48ae-aff3-31c1fb27b3d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83219645-221b-4913-a17e-bdef67c28c49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10e83cbe-92aa-4b52-9f25-8206d7bfd44d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83219645-221b-4913-a17e-bdef67c28c49",
                    "LayerId": "3025abd6-e832-4200-b695-4800a503901d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "3025abd6-e832-4200-b695-4800a503901d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0294c4de-1353-4639-be1d-7336257d3444",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 129,
    "xorig": 63,
    "yorig": 60
}