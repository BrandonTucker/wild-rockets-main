{
    "id": "ebf0280d-e80c-44e2-92b2-68f5a56ec69a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 14,
    "bbox_right": 39,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98dad5f6-dee6-431c-894f-a124577af1a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebf0280d-e80c-44e2-92b2-68f5a56ec69a",
            "compositeImage": {
                "id": "b11af6f6-7174-44a0-b4a4-d17064aacd35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98dad5f6-dee6-431c-894f-a124577af1a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c88623d-c235-4d72-a640-c56bb031006b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98dad5f6-dee6-431c-894f-a124577af1a5",
                    "LayerId": "4652ab85-b28d-48c9-9ad7-c6a1230d428f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 116,
    "layers": [
        {
            "id": "4652ab85-b28d-48c9-9ad7-c6a1230d428f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebf0280d-e80c-44e2-92b2-68f5a56ec69a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 27,
    "yorig": 49
}