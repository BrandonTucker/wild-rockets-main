{
    "id": "5e691a78-cad7-4f05-bddc-6d4a4036428b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship31",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 40,
    "bbox_right": 65,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3de1453-80ec-4cf7-8015-b45e6d3b9923",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e691a78-cad7-4f05-bddc-6d4a4036428b",
            "compositeImage": {
                "id": "45433757-3557-4372-ba2d-53aab5593c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3de1453-80ec-4cf7-8015-b45e6d3b9923",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06b9fa47-66fa-4228-bac8-08a17b495ba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3de1453-80ec-4cf7-8015-b45e6d3b9923",
                    "LayerId": "6e453bd3-eb8f-4a07-afc7-96f20e1f2a30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "6e453bd3-eb8f-4a07-afc7-96f20e1f2a30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e691a78-cad7-4f05-bddc-6d4a4036428b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 53,
    "yorig": 49
}