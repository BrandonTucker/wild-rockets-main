{
    "id": "6aeec2d8-c23b-4e28-a5ae-22a2814c4e79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 3,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "086e3b88-d731-4dd4-9e26-5179898ffe53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aeec2d8-c23b-4e28-a5ae-22a2814c4e79",
            "compositeImage": {
                "id": "f8122504-29dc-48d1-be02-6e84e98445e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "086e3b88-d731-4dd4-9e26-5179898ffe53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df964dd9-d1d1-4fb2-85a8-7bd637a7db31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "086e3b88-d731-4dd4-9e26-5179898ffe53",
                    "LayerId": "ea9e5099-58e3-4371-a1d7-3707b91a24dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ea9e5099-58e3-4371-a1d7-3707b91a24dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6aeec2d8-c23b-4e28-a5ae-22a2814c4e79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}