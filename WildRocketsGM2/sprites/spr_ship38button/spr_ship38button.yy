{
    "id": "10d046c9-c01d-458b-92bf-7e457f7c64cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship38button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41f0dbd9-b1ce-40ea-a78a-8cd074bb17f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10d046c9-c01d-458b-92bf-7e457f7c64cc",
            "compositeImage": {
                "id": "3baff9a3-60cd-48a8-8d41-ca0004749eff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41f0dbd9-b1ce-40ea-a78a-8cd074bb17f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05917d1f-6d86-41cc-ac19-a978150703c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41f0dbd9-b1ce-40ea-a78a-8cd074bb17f6",
                    "LayerId": "ff3cdfbf-39d4-4117-b3bd-7b09c77d8d4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "ff3cdfbf-39d4-4117-b3bd-7b09c77d8d4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10d046c9-c01d-458b-92bf-7e457f7c64cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}