{
    "id": "239fcfbf-5e1e-4e17-b4c3-e032cee56e4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange32button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ccca81b-4826-40d5-8df4-440225767f1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "239fcfbf-5e1e-4e17-b4c3-e032cee56e4a",
            "compositeImage": {
                "id": "e33b8466-00bb-4870-b48f-bf4945024ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ccca81b-4826-40d5-8df4-440225767f1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5ce22d3-3899-452f-80b6-9f08fd325806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ccca81b-4826-40d5-8df4-440225767f1d",
                    "LayerId": "73b97534-996a-4297-aa06-ce22e80f2e72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "73b97534-996a-4297-aa06-ce22e80f2e72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "239fcfbf-5e1e-4e17-b4c3-e032cee56e4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}