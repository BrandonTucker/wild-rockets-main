{
    "id": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challangebutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 11,
    "bbox_right": 518,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27863420-c690-4204-b520-a57007016d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "f73149fb-80f5-4c57-99d6-bcc3e5faa797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27863420-c690-4204-b520-a57007016d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84eb8a3d-03f7-46aa-99e8-79132504d934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27863420-c690-4204-b520-a57007016d1f",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "6e91098b-b691-4f47-a751-f4e638c0e4b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "4ed1901a-709e-47fb-9460-e3a01de6815d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e91098b-b691-4f47-a751-f4e638c0e4b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d93340b-a239-4eec-a9d9-ef035b222fa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e91098b-b691-4f47-a751-f4e638c0e4b9",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "6dadb5ab-1c9b-4268-8117-aab373586218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "10d40489-40b0-4bcf-8b27-de4319f85c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dadb5ab-1c9b-4268-8117-aab373586218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4408cab-b77c-4cd9-bf28-c86628f41975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dadb5ab-1c9b-4268-8117-aab373586218",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "b667e8dc-0b94-4fd6-b86f-ad375d6b0869",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "e06f8f79-1876-4cfe-9cd3-a18d21b83d24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b667e8dc-0b94-4fd6-b86f-ad375d6b0869",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac02dd13-fb32-43c3-958d-7ebc2d437ee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b667e8dc-0b94-4fd6-b86f-ad375d6b0869",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "82cdab9f-ec91-445b-bdaa-04ff68fe53aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "dacaeadf-97cf-45fd-9843-b543844d89e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82cdab9f-ec91-445b-bdaa-04ff68fe53aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3ccfa7-ced1-4c0d-9c46-1610eb900bfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82cdab9f-ec91-445b-bdaa-04ff68fe53aa",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "96ce4755-738d-4443-afc8-d34463bc95e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "c4e99f06-6855-45f0-b9e0-f759846b7bc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ce4755-738d-4443-afc8-d34463bc95e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2846e9a-d614-4508-9cbf-b7cb28c4e2b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ce4755-738d-4443-afc8-d34463bc95e0",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "0c8a155d-49de-48cf-a83b-e1028faa8fc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "12606e6a-cd2b-49da-874c-97a481b64e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c8a155d-49de-48cf-a83b-e1028faa8fc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a5c050b-6754-4dcd-a617-573f7ff1c58c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c8a155d-49de-48cf-a83b-e1028faa8fc3",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "272e962d-1853-4309-8c07-3e1d8754392f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "77de21a1-6408-4c5d-820a-62f766d28973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "272e962d-1853-4309-8c07-3e1d8754392f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d41367ff-7982-471a-8d82-d0e43a288a83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "272e962d-1853-4309-8c07-3e1d8754392f",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "333a4fcb-c93b-4e18-833c-8d0bf422dea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "56ef9b5a-efce-465b-a6e0-822ae9305728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "333a4fcb-c93b-4e18-833c-8d0bf422dea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d9d8055-3f70-47dd-ace5-31f439b90418",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "333a4fcb-c93b-4e18-833c-8d0bf422dea9",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "6b9e73aa-90e5-40fa-9ec4-b36c37cc06b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "464e95bc-d2c0-48a4-b31a-4a1fafd09adc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b9e73aa-90e5-40fa-9ec4-b36c37cc06b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07338e00-aed4-4d06-a955-26e508cbb612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b9e73aa-90e5-40fa-9ec4-b36c37cc06b5",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "ca0faff6-cc6c-4f9d-a2d1-f98c2660e36c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "cda05ea2-11c1-48d1-ac9e-92db8904adca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca0faff6-cc6c-4f9d-a2d1-f98c2660e36c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c10a4191-3d78-4518-858d-456ea75ec188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca0faff6-cc6c-4f9d-a2d1-f98c2660e36c",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "4e63643b-b6d6-485f-9c18-c68de4d9dc36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "ecdf4db8-89f5-4112-ae9f-fffe39aa13f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e63643b-b6d6-485f-9c18-c68de4d9dc36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a832cb1a-46a6-423a-bb34-64d5a85cdcfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e63643b-b6d6-485f-9c18-c68de4d9dc36",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "7db5057d-793f-4d0a-b45e-ed3d33ef102e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "1d28eeb5-769c-4c3e-9592-4014ebfd555e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db5057d-793f-4d0a-b45e-ed3d33ef102e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9323071f-4ac1-4f5d-bd64-2e047f49bcf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db5057d-793f-4d0a-b45e-ed3d33ef102e",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "f9b75fde-e494-481d-9183-f0c78e8ccd40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "cf4c9673-c3ce-482a-a705-7f8749f71e6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9b75fde-e494-481d-9183-f0c78e8ccd40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15372be3-c1b5-4327-866a-f393de7dfc14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9b75fde-e494-481d-9183-f0c78e8ccd40",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "b4e6cbc7-7815-4fb2-855d-2e0931639584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "b601256d-3432-427d-a7cd-470bc5ad1353",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4e6cbc7-7815-4fb2-855d-2e0931639584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0befc75b-441c-4560-a766-66682b14acd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4e6cbc7-7815-4fb2-855d-2e0931639584",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "f8ae692a-d751-487a-bbf7-40f5c9ae00f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "214f2d65-ebec-4d95-a0e5-ebe4c9154387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8ae692a-d751-487a-bbf7-40f5c9ae00f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0688c525-7a7a-499d-9b62-0a9eec968ecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8ae692a-d751-487a-bbf7-40f5c9ae00f6",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "c3101458-3ba5-4fe6-a9df-e8057cc7cb1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "b36ccda6-6c0e-4072-b6d1-41422513bf14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3101458-3ba5-4fe6-a9df-e8057cc7cb1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84b98ce7-34e3-4fe9-97b5-f82afa9833e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3101458-3ba5-4fe6-a9df-e8057cc7cb1b",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "75b891f8-4473-4522-b785-4d9b7a3db0e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "ee431699-2497-4a38-90fd-ab8d0cb7d7e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75b891f8-4473-4522-b785-4d9b7a3db0e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50190a19-bb8c-49bd-b3d2-54ea96c7ed64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75b891f8-4473-4522-b785-4d9b7a3db0e6",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "1246007c-886c-4a3c-ba58-3bb0b5c9ae40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "87515fe8-9f1a-4679-8a27-47d0e567fe87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1246007c-886c-4a3c-ba58-3bb0b5c9ae40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85892d21-cb70-403c-88f2-6d05d5444fe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1246007c-886c-4a3c-ba58-3bb0b5c9ae40",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "6061b0a5-8015-43e5-b7b9-cee5a3b677d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "a363d305-96de-4e47-a9df-02da84c6aac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6061b0a5-8015-43e5-b7b9-cee5a3b677d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "965dad7c-eb35-4d7a-a813-a326b31e678d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6061b0a5-8015-43e5-b7b9-cee5a3b677d1",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "b0cad360-35e7-4286-8afc-af68f778ecaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "3a2d24be-724d-43a4-9f9d-0d139b051946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0cad360-35e7-4286-8afc-af68f778ecaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3493fe1a-8411-4a28-9826-dc54f1c96703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0cad360-35e7-4286-8afc-af68f778ecaa",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "c37bdbca-166f-4340-a732-48eab114f69b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "92f4a192-26d1-4aa1-bce8-6cfd7f4223ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37bdbca-166f-4340-a732-48eab114f69b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e98a7b2c-d7a0-4374-94ca-39aad27e30bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37bdbca-166f-4340-a732-48eab114f69b",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "10bf6302-25a8-4b47-8abb-49fba63c07ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "c6e6f496-6491-4e00-a436-ed43d59babf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10bf6302-25a8-4b47-8abb-49fba63c07ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb39234a-8dc8-4caa-a2f6-f655bb8ad312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10bf6302-25a8-4b47-8abb-49fba63c07ee",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        },
        {
            "id": "34de0be4-3502-47ed-8d7c-58f49fe591f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "compositeImage": {
                "id": "dcf5508e-8e2d-4506-8d9f-b0731f1861a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34de0be4-3502-47ed-8d7c-58f49fe591f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d60d5b6-7fdd-46cf-80ab-bffd0da20938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34de0be4-3502-47ed-8d7c-58f49fe591f1",
                    "LayerId": "c5e323db-2ab7-4984-a352-fcfa41538f05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "c5e323db-2ab7-4984-a352-fcfa41538f05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b8fa64c-47fe-4cce-8591-1c492c7aa2d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 540,
    "xorig": 33,
    "yorig": 22
}