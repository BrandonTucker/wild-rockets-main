{
    "id": "f41a2465-edd2-4b69-96a5-45298f4fa637",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship27explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f71935c9-22fe-43ee-ac16-db4c46feef58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f41a2465-edd2-4b69-96a5-45298f4fa637",
            "compositeImage": {
                "id": "ad3db767-e1cb-458b-82e8-2471ff3a480f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f71935c9-22fe-43ee-ac16-db4c46feef58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf34a1a-bdeb-4650-8c74-69345d231798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f71935c9-22fe-43ee-ac16-db4c46feef58",
                    "LayerId": "fadb8c24-08c5-4459-a385-9916308530bc"
                }
            ]
        },
        {
            "id": "0adbd64d-bafa-4802-912f-c994b981e77d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f41a2465-edd2-4b69-96a5-45298f4fa637",
            "compositeImage": {
                "id": "0ce3be39-7128-44a1-b906-228c8e6690c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0adbd64d-bafa-4802-912f-c994b981e77d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b0f63de-d355-4385-b867-338ec24a155b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0adbd64d-bafa-4802-912f-c994b981e77d",
                    "LayerId": "fadb8c24-08c5-4459-a385-9916308530bc"
                }
            ]
        },
        {
            "id": "bc628c93-8c3b-41fc-bf18-f1bdad19b769",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f41a2465-edd2-4b69-96a5-45298f4fa637",
            "compositeImage": {
                "id": "61841adf-4498-46ce-8bbd-13de899afa94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc628c93-8c3b-41fc-bf18-f1bdad19b769",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c0ce8aa-da71-45a8-adfc-6f2f65f421fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc628c93-8c3b-41fc-bf18-f1bdad19b769",
                    "LayerId": "fadb8c24-08c5-4459-a385-9916308530bc"
                }
            ]
        },
        {
            "id": "8368ced0-1881-4bad-9413-9a55dffb2cc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f41a2465-edd2-4b69-96a5-45298f4fa637",
            "compositeImage": {
                "id": "36b83f56-b777-46b8-a1d8-4c65924a9b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8368ced0-1881-4bad-9413-9a55dffb2cc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6913c6b6-b952-47c4-9d78-e02f6d28c91a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8368ced0-1881-4bad-9413-9a55dffb2cc8",
                    "LayerId": "fadb8c24-08c5-4459-a385-9916308530bc"
                }
            ]
        },
        {
            "id": "16c0ad95-d017-401f-a6ab-9179446c080e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f41a2465-edd2-4b69-96a5-45298f4fa637",
            "compositeImage": {
                "id": "831389b6-82f9-4a13-8c1d-ee3c3eb94160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16c0ad95-d017-401f-a6ab-9179446c080e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ebc68fc-ab03-446b-abef-0d2d9f5907d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16c0ad95-d017-401f-a6ab-9179446c080e",
                    "LayerId": "fadb8c24-08c5-4459-a385-9916308530bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "fadb8c24-08c5-4459-a385-9916308530bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f41a2465-edd2-4b69-96a5-45298f4fa637",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 50
}