{
    "id": "ac090849-bf48-4a5d-8e5c-8d076bed7c84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon36",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 2,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0394d1f6-d2bf-4c21-9ee8-d459039f7b88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac090849-bf48-4a5d-8e5c-8d076bed7c84",
            "compositeImage": {
                "id": "452ff065-0a8f-4eb0-a1a2-e6faa966214a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0394d1f6-d2bf-4c21-9ee8-d459039f7b88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35625039-ec99-4b5d-8969-6e759bb47f0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0394d1f6-d2bf-4c21-9ee8-d459039f7b88",
                    "LayerId": "119d19b3-eaff-49c7-aa68-b282e923b83f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "119d19b3-eaff-49c7-aa68-b282e923b83f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac090849-bf48-4a5d-8e5c-8d076bed7c84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 0,
    "yorig": 0
}