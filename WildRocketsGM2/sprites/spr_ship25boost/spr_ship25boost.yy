{
    "id": "f727e2e0-ce43-43a8-8c17-02d35312cb3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship25boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fba9a2e0-fcb4-4ace-ad64-307d50532f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f727e2e0-ce43-43a8-8c17-02d35312cb3e",
            "compositeImage": {
                "id": "4839b049-b4ef-4b0d-91a1-9f6252d74d2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba9a2e0-fcb4-4ace-ad64-307d50532f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c409db69-f63b-42b5-8078-d94d563d2c38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba9a2e0-fcb4-4ace-ad64-307d50532f8d",
                    "LayerId": "ac8c1c6d-4f16-4343-80a1-507fdd9f6dec"
                }
            ]
        },
        {
            "id": "2f3a8c19-6d9e-485f-a122-15db9e43f4aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f727e2e0-ce43-43a8-8c17-02d35312cb3e",
            "compositeImage": {
                "id": "d9494f34-c534-4056-a374-2497062d816a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f3a8c19-6d9e-485f-a122-15db9e43f4aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50a37fb2-cf69-44db-a641-250ae4236ef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f3a8c19-6d9e-485f-a122-15db9e43f4aa",
                    "LayerId": "ac8c1c6d-4f16-4343-80a1-507fdd9f6dec"
                }
            ]
        },
        {
            "id": "dff5e405-1771-4792-9717-63c340767b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f727e2e0-ce43-43a8-8c17-02d35312cb3e",
            "compositeImage": {
                "id": "4b1f1afe-1034-453d-a314-d826194a0957",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dff5e405-1771-4792-9717-63c340767b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b03d5a5c-98e6-4915-948e-b8c314d8af87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dff5e405-1771-4792-9717-63c340767b57",
                    "LayerId": "ac8c1c6d-4f16-4343-80a1-507fdd9f6dec"
                }
            ]
        },
        {
            "id": "c8b47706-9fd2-4be0-9c77-55e7c4bfbe09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f727e2e0-ce43-43a8-8c17-02d35312cb3e",
            "compositeImage": {
                "id": "4b7d2caa-a9e9-404f-a67a-7693e5b41014",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b47706-9fd2-4be0-9c77-55e7c4bfbe09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fe3cf7c-7da0-4013-9e17-4bff1da92886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b47706-9fd2-4be0-9c77-55e7c4bfbe09",
                    "LayerId": "ac8c1c6d-4f16-4343-80a1-507fdd9f6dec"
                }
            ]
        },
        {
            "id": "32e56921-bd35-41ea-a511-3734dbd2e673",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f727e2e0-ce43-43a8-8c17-02d35312cb3e",
            "compositeImage": {
                "id": "0739a2bd-0e04-48c6-b47a-e91c949494b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32e56921-bd35-41ea-a511-3734dbd2e673",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5449e5f4-4a09-4cbc-bbcb-2de2b9d31e0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32e56921-bd35-41ea-a511-3734dbd2e673",
                    "LayerId": "ac8c1c6d-4f16-4343-80a1-507fdd9f6dec"
                }
            ]
        },
        {
            "id": "55ddb59e-e5c9-4a66-b577-ed51aaec81fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f727e2e0-ce43-43a8-8c17-02d35312cb3e",
            "compositeImage": {
                "id": "757ec59d-f72c-44ba-aa4d-63e18c34115f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55ddb59e-e5c9-4a66-b577-ed51aaec81fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "476883f9-ab89-4463-94b6-d3f1c6dde358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ddb59e-e5c9-4a66-b577-ed51aaec81fb",
                    "LayerId": "ac8c1c6d-4f16-4343-80a1-507fdd9f6dec"
                }
            ]
        },
        {
            "id": "8e42b957-3cd4-44f6-9c3b-c4b5ab57611e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f727e2e0-ce43-43a8-8c17-02d35312cb3e",
            "compositeImage": {
                "id": "c1199a66-e039-4609-9a6c-91dd896f55e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e42b957-3cd4-44f6-9c3b-c4b5ab57611e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "825b9e0d-bb87-49a5-8619-cba9b6787985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e42b957-3cd4-44f6-9c3b-c4b5ab57611e",
                    "LayerId": "ac8c1c6d-4f16-4343-80a1-507fdd9f6dec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 172,
    "layers": [
        {
            "id": "ac8c1c6d-4f16-4343-80a1-507fdd9f6dec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f727e2e0-ce43-43a8-8c17-02d35312cb3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 62
}