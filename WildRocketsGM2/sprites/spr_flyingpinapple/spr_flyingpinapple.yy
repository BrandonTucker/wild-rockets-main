{
    "id": "d1529be5-c8c3-4858-937c-3ab3500611f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flyingpinapple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "648155a2-91f8-4c50-9d48-c0828a803ea2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "b349c0bb-eac0-42f6-bbc9-467f9d643388",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "648155a2-91f8-4c50-9d48-c0828a803ea2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11408c18-cb2b-4e3d-9932-42ef34245956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "648155a2-91f8-4c50-9d48-c0828a803ea2",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "d08fa6fd-5197-4ad3-85b4-ac19a78c94a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "05801392-60a0-48e7-9a3d-d6fa0d913fd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d08fa6fd-5197-4ad3-85b4-ac19a78c94a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd436905-d731-465c-9265-d8058c5ef830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d08fa6fd-5197-4ad3-85b4-ac19a78c94a3",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "921aba09-de8f-48ed-a46a-ff7fb9bd16dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "6521456e-372b-487d-9522-7fb46591c2f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "921aba09-de8f-48ed-a46a-ff7fb9bd16dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cfe3ef4-7479-408b-9b1d-e3b8b1948717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "921aba09-de8f-48ed-a46a-ff7fb9bd16dd",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "70979664-a48e-454b-9990-2a7a27aa4631",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "9035b7ea-4a70-4d9c-a363-7729393c51a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70979664-a48e-454b-9990-2a7a27aa4631",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55622d21-f869-44b7-9a22-d6dfc1de5360",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70979664-a48e-454b-9990-2a7a27aa4631",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "2b61684e-eba2-454c-b9f9-1f575e917e04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "49896da1-f79e-4bea-9449-841119738e6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b61684e-eba2-454c-b9f9-1f575e917e04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "167e5813-16e4-4177-96f8-a2da4a941fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b61684e-eba2-454c-b9f9-1f575e917e04",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "b8ab921b-8a3c-4e01-b7da-e2cf2ee6d27e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "d59b82e5-b7ed-4f7a-b052-5ef43c8e6ccd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8ab921b-8a3c-4e01-b7da-e2cf2ee6d27e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ec317a5-4c66-4412-b33b-946e1bfa4056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8ab921b-8a3c-4e01-b7da-e2cf2ee6d27e",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "dd12c571-324f-4186-b1c2-c9fb48ae2af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "a7fee600-a1db-4235-98ad-095f62c94595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd12c571-324f-4186-b1c2-c9fb48ae2af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98bca209-bb06-4179-854b-8785097a348f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd12c571-324f-4186-b1c2-c9fb48ae2af7",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "d1e1c912-6bd3-44a6-80da-d36e0e43ee56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "bd504b0f-9475-4928-8ca3-7ee622ca5f33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1e1c912-6bd3-44a6-80da-d36e0e43ee56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba2cca1a-abce-42f4-b17c-104d36834842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e1c912-6bd3-44a6-80da-d36e0e43ee56",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "dbc58134-2b8a-4f5e-bf67-d20b986555bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "cfbe0044-0526-45d5-b248-dea388c982ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbc58134-2b8a-4f5e-bf67-d20b986555bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6055a4e5-c1a9-4be5-a68c-3a3923216b5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbc58134-2b8a-4f5e-bf67-d20b986555bb",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "20dd76fa-0d07-43b4-8432-215dda1ea58f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "b34b40fb-2d5f-4e36-a32c-5d0b3b8f4091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20dd76fa-0d07-43b4-8432-215dda1ea58f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1e8f882-4abe-4973-b9ed-52343bbdefbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20dd76fa-0d07-43b4-8432-215dda1ea58f",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "a7097369-f718-4dcf-9e25-893555e9968d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "b27c2e06-22ab-494b-898b-79f00b67fcb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7097369-f718-4dcf-9e25-893555e9968d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "836e4c0e-3282-47c6-b32d-4785f8ed9e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7097369-f718-4dcf-9e25-893555e9968d",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "2270a1e1-01a2-42bd-b1ec-5477f7369e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "18964faa-6db9-4f8b-ba6e-3183bfedf259",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2270a1e1-01a2-42bd-b1ec-5477f7369e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0fabb2f-eb2b-4197-a95c-4141b671eacc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2270a1e1-01a2-42bd-b1ec-5477f7369e69",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "b7aee8b5-7549-4c4a-94ff-1552df7a2d8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "0773284b-95cb-42b8-a228-76b2c53a6d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7aee8b5-7549-4c4a-94ff-1552df7a2d8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bc04558-e1bf-4bcc-aa9d-cea0d9df5edd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7aee8b5-7549-4c4a-94ff-1552df7a2d8f",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "b10358f1-1e7a-400a-a4a5-2785ef659c8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "e6f56efd-cc4d-493e-99b5-c70ec170f7c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b10358f1-1e7a-400a-a4a5-2785ef659c8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbe73864-fc38-4a34-9bc0-f7b1bd46ad18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b10358f1-1e7a-400a-a4a5-2785ef659c8c",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        },
        {
            "id": "587f6ca4-aa8e-4fff-970f-dfadcb120846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "compositeImage": {
                "id": "3eeff4af-7c1e-4a72-9815-1ed831256ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "587f6ca4-aa8e-4fff-970f-dfadcb120846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9559eb0c-b5e0-4fcd-a62e-93bb28c7e2da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "587f6ca4-aa8e-4fff-970f-dfadcb120846",
                    "LayerId": "d5373489-16d0-4a1d-8c41-484af1905b20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "d5373489-16d0-4a1d-8c41-484af1905b20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1529be5-c8c3-4858-937c-3ab3500611f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 16
}