{
    "id": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite223",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 12,
    "bbox_right": 127,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7da2ae11-845c-45a3-bcc6-1abe20be5bb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "a8630ea9-73a2-4d37-add7-07ed34acd85a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da2ae11-845c-45a3-bcc6-1abe20be5bb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6d3d857-db64-44ea-8237-30d247887d89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da2ae11-845c-45a3-bcc6-1abe20be5bb4",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "094d43f8-ce23-4090-8d5a-e0ba6b78be4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "84214050-d8f3-4b52-8cda-2b71836f405f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "094d43f8-ce23-4090-8d5a-e0ba6b78be4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c1a7384-59ac-4363-a788-948d04bf1498",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "094d43f8-ce23-4090-8d5a-e0ba6b78be4f",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "2622f2af-d8af-420e-a961-a4506b8fb28d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "71c8f4a4-2e8f-4227-b504-39276ecdb4ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2622f2af-d8af-420e-a961-a4506b8fb28d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d686c078-55e8-4036-807b-437c8cf09018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2622f2af-d8af-420e-a961-a4506b8fb28d",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "9353ed4e-e490-4f4b-aad3-8bf14fb13e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "b5f8e50b-13fd-4a0c-8ce5-6ccca97d6840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9353ed4e-e490-4f4b-aad3-8bf14fb13e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8beac83-a297-4eb1-b4f7-4528c6422501",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9353ed4e-e490-4f4b-aad3-8bf14fb13e2d",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "e2f529cb-b1b1-418c-9aea-1d8b99e0080e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "eaf27468-fd58-4100-9d48-cb20a3b97fe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2f529cb-b1b1-418c-9aea-1d8b99e0080e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27fb01b0-c0b4-4dc1-8d49-dcbacfe64789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2f529cb-b1b1-418c-9aea-1d8b99e0080e",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "d89c684d-c4b1-47b7-88db-3a012b4cc261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "ab576ec1-b36b-4352-971d-5bcf12f8a854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d89c684d-c4b1-47b7-88db-3a012b4cc261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "889fa652-339d-4953-a9d0-2223fa95ec64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d89c684d-c4b1-47b7-88db-3a012b4cc261",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "bdcaafaa-8a5b-4731-aad4-7100729aa363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "d082bacb-23f1-4b68-bcb7-fde0078af5d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdcaafaa-8a5b-4731-aad4-7100729aa363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d0b843a-a2c3-460c-995c-421c027009a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdcaafaa-8a5b-4731-aad4-7100729aa363",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "39e0bfc8-00cc-4160-9a2d-4783b3da6f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "8e9fd887-9c94-46a4-b9ea-e435682ba47f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39e0bfc8-00cc-4160-9a2d-4783b3da6f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20e6a08c-09ee-448d-99c3-708e375e0ef1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39e0bfc8-00cc-4160-9a2d-4783b3da6f16",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "2da3f6b9-3ee8-4d38-a5bd-da6706fee841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "3285b1cb-427b-4652-8131-c327d076c320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2da3f6b9-3ee8-4d38-a5bd-da6706fee841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baebfd03-007e-47cc-933f-53a312c0de5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2da3f6b9-3ee8-4d38-a5bd-da6706fee841",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "5d10a90c-33c4-4f32-aa69-45d263118408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "2366babf-b588-4124-825d-7b98a6c11b3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d10a90c-33c4-4f32-aa69-45d263118408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac053827-08fb-450c-9d2d-34b5077e4b86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d10a90c-33c4-4f32-aa69-45d263118408",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "a98137c0-12a2-4a81-ba30-152d0da27a28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "179578ed-c88a-4ebe-9ef5-553f3bf21acf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a98137c0-12a2-4a81-ba30-152d0da27a28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "104be012-575f-4d6a-82c6-17885d50c0aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a98137c0-12a2-4a81-ba30-152d0da27a28",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "7582fe29-323c-436a-8ae6-cec25acb5da4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "5d642dd7-4e61-4f2e-8056-f8faef06aae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7582fe29-323c-436a-8ae6-cec25acb5da4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5da520c0-77fe-4d64-a919-5f81afcfe1e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7582fe29-323c-436a-8ae6-cec25acb5da4",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "d01ea8a1-5b77-4f7d-a03a-dc3ba09488fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "6df078ad-ca88-43cf-a106-8bd13fd7a8bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d01ea8a1-5b77-4f7d-a03a-dc3ba09488fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb1f3166-3e8e-48a3-a172-af75e3450de7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01ea8a1-5b77-4f7d-a03a-dc3ba09488fe",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "8ff0cd17-5d7e-4883-97ef-c0e8e8d278ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "f3e5933a-529c-41f6-887f-547eab00a11e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff0cd17-5d7e-4883-97ef-c0e8e8d278ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3935d789-2fbc-4e9c-9c24-29505a236d66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff0cd17-5d7e-4883-97ef-c0e8e8d278ee",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "d38cd415-436d-40a8-b40c-df1df4d999f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "ddca5d51-0c2d-4868-9a0b-b3502d27a1fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d38cd415-436d-40a8-b40c-df1df4d999f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26332cdd-e08d-4c83-a930-cb711ed95057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d38cd415-436d-40a8-b40c-df1df4d999f2",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "d8dab57f-c35c-4830-9db3-26073886affa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "7b098ae8-34bf-4a9e-a177-e11959ec6d18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8dab57f-c35c-4830-9db3-26073886affa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f73e97d-effc-48ea-96a6-2d1b3bc09160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8dab57f-c35c-4830-9db3-26073886affa",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "e0fdf9c7-1875-4cb1-bb48-112ea1b7db90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "c8d74cc1-f177-4e9c-920e-1aca428bf656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0fdf9c7-1875-4cb1-bb48-112ea1b7db90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0b6e2fd-3b86-4adf-85e8-51af32bddebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0fdf9c7-1875-4cb1-bb48-112ea1b7db90",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "eb89785b-f371-45ea-91b5-a5ba9a77be41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "44c58bdc-048b-400f-a624-6e814428f081",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb89785b-f371-45ea-91b5-a5ba9a77be41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12902ed4-7ac9-43fa-9caf-91598d26aa96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb89785b-f371-45ea-91b5-a5ba9a77be41",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "0fc8b3ef-42c8-4616-b4f5-199e29c285d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "9b16c6ac-0168-4b74-8342-0b0f90664a54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fc8b3ef-42c8-4616-b4f5-199e29c285d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85267ac9-76dc-4ec5-93e5-625a5f5fe93b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fc8b3ef-42c8-4616-b4f5-199e29c285d2",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        },
        {
            "id": "cdbddec5-1a3f-46d8-a05f-149332da5919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "compositeImage": {
                "id": "6d60c9e3-34d5-4d38-81ea-b5858219a917",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdbddec5-1a3f-46d8-a05f-149332da5919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea8f705a-1fde-440a-a063-dd7158026c23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdbddec5-1a3f-46d8-a05f-149332da5919",
                    "LayerId": "5c390c7b-2822-40db-a25e-02b7d692a3b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "5c390c7b-2822-40db-a25e-02b7d692a3b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3596d0f-a00e-441f-b2bb-1f1b3709aef3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 55,
    "yorig": 50
}