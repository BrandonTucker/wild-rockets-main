{
    "id": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pineapple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a55cba56-1d41-43a1-b649-5124e8e711f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "compositeImage": {
                "id": "bd45dd68-fe9f-4654-aef3-0411eba82126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a55cba56-1d41-43a1-b649-5124e8e711f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e868950a-7d6c-4fb5-83bc-a60bbc51a955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a55cba56-1d41-43a1-b649-5124e8e711f7",
                    "LayerId": "0d703fd9-8a03-4358-af6e-738a500b6ef9"
                }
            ]
        },
        {
            "id": "90963b58-95ac-43ee-a015-20274f2dcde6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "compositeImage": {
                "id": "757ee941-44be-45e6-adfc-362377c47c28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90963b58-95ac-43ee-a015-20274f2dcde6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fccc6ca7-2591-4d94-8d7c-8dfe8f316c04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90963b58-95ac-43ee-a015-20274f2dcde6",
                    "LayerId": "0d703fd9-8a03-4358-af6e-738a500b6ef9"
                }
            ]
        },
        {
            "id": "9d863df1-7194-4467-9ea1-fc8fe42262b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "compositeImage": {
                "id": "71ccfc01-5468-4254-bbed-156699eee23d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d863df1-7194-4467-9ea1-fc8fe42262b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9963a79b-ecef-4ecf-8a61-04ee1877fac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d863df1-7194-4467-9ea1-fc8fe42262b5",
                    "LayerId": "0d703fd9-8a03-4358-af6e-738a500b6ef9"
                }
            ]
        },
        {
            "id": "03e4cad8-72af-4d26-8ef1-ec8641575f78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "compositeImage": {
                "id": "8b70fdcb-f219-4eb1-9ec9-80a5bb0ef5bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e4cad8-72af-4d26-8ef1-ec8641575f78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4fbec45-ed24-4fde-8cdc-16c3d990f40f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e4cad8-72af-4d26-8ef1-ec8641575f78",
                    "LayerId": "0d703fd9-8a03-4358-af6e-738a500b6ef9"
                }
            ]
        },
        {
            "id": "d676a33d-005f-4c34-90e2-eb5aa8b34063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "compositeImage": {
                "id": "710f3b08-d41b-4493-8461-462f0acfa28a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d676a33d-005f-4c34-90e2-eb5aa8b34063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b17965-3d94-4582-893d-8db8f8417069",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d676a33d-005f-4c34-90e2-eb5aa8b34063",
                    "LayerId": "0d703fd9-8a03-4358-af6e-738a500b6ef9"
                }
            ]
        },
        {
            "id": "f4c1e97a-436d-4190-a5cd-d6f8b0f837c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "compositeImage": {
                "id": "958c73d3-6262-4d52-bda4-bc74a595aa99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4c1e97a-436d-4190-a5cd-d6f8b0f837c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "444faece-6616-4adf-a40f-049d90d175f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4c1e97a-436d-4190-a5cd-d6f8b0f837c1",
                    "LayerId": "0d703fd9-8a03-4358-af6e-738a500b6ef9"
                }
            ]
        },
        {
            "id": "b2ae8503-8d07-4cfa-a7a8-0a5c7787eedd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "compositeImage": {
                "id": "b7888e5a-1eb6-495e-85fb-aa6923f71e46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ae8503-8d07-4cfa-a7a8-0a5c7787eedd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e115cdd5-409d-498e-9273-77f1308954d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ae8503-8d07-4cfa-a7a8-0a5c7787eedd",
                    "LayerId": "0d703fd9-8a03-4358-af6e-738a500b6ef9"
                }
            ]
        },
        {
            "id": "9d7d2bd8-7b08-4c84-a581-fe3b7ba613fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "compositeImage": {
                "id": "a0c799b4-1357-4ee0-b06b-51ac32353baa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d7d2bd8-7b08-4c84-a581-fe3b7ba613fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54cd8f15-afee-43d9-9749-0d4c24ad5a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d7d2bd8-7b08-4c84-a581-fe3b7ba613fd",
                    "LayerId": "0d703fd9-8a03-4358-af6e-738a500b6ef9"
                }
            ]
        },
        {
            "id": "1bc6c036-f649-463d-a09a-a5336e8dbd54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "compositeImage": {
                "id": "c7af55a3-3211-4f70-9edb-9d95fc86aa79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bc6c036-f649-463d-a09a-a5336e8dbd54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f255c3c-6a9c-45f7-be4c-22dccdf39cbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bc6c036-f649-463d-a09a-a5336e8dbd54",
                    "LayerId": "0d703fd9-8a03-4358-af6e-738a500b6ef9"
                }
            ]
        },
        {
            "id": "e0433dbb-57e5-4645-9c03-7f6376f6ea15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "compositeImage": {
                "id": "468495f6-f3ef-441b-a029-b74683902098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0433dbb-57e5-4645-9c03-7f6376f6ea15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e5ad11-704d-4ace-a9d9-8f195c127d19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0433dbb-57e5-4645-9c03-7f6376f6ea15",
                    "LayerId": "0d703fd9-8a03-4358-af6e-738a500b6ef9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "0d703fd9-8a03-4358-af6e-738a500b6ef9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be917f8a-efd8-4d3a-b7a9-aea37e965585",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 42,
    "yorig": 38
}