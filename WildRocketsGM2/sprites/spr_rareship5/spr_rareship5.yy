{
    "id": "4914c640-901c-40ad-b1ac-3877f6ad5309",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rareship5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 35,
    "bbox_right": 114,
    "bbox_top": 47,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b72fba4-3326-4ad1-b458-d74e3b41c015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "compositeImage": {
                "id": "e0655f50-d36c-4d77-977a-57baa99b3827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b72fba4-3326-4ad1-b458-d74e3b41c015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5202ece-b42d-4ba6-acd0-17c86c9b4058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b72fba4-3326-4ad1-b458-d74e3b41c015",
                    "LayerId": "04900853-040b-4435-9101-e9b47c646464"
                }
            ]
        },
        {
            "id": "0758909c-7d14-452e-9ffd-0d253813bbe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "compositeImage": {
                "id": "bf6216a3-f52a-4cd1-8d53-ceeea68cce42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0758909c-7d14-452e-9ffd-0d253813bbe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89993acf-8185-42c2-a3c5-81270513d03b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0758909c-7d14-452e-9ffd-0d253813bbe7",
                    "LayerId": "04900853-040b-4435-9101-e9b47c646464"
                }
            ]
        },
        {
            "id": "f871284b-23fc-4620-afb8-ffe20e45d47f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "compositeImage": {
                "id": "f3659979-673f-4325-a2d9-d01210d45038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f871284b-23fc-4620-afb8-ffe20e45d47f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95be9859-fe1e-4765-b1c4-04a584a9af8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f871284b-23fc-4620-afb8-ffe20e45d47f",
                    "LayerId": "04900853-040b-4435-9101-e9b47c646464"
                }
            ]
        },
        {
            "id": "a865fe99-f39c-4084-93ce-194863fc4cb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "compositeImage": {
                "id": "84772eb2-462a-4d82-b26f-2b10728ecd05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a865fe99-f39c-4084-93ce-194863fc4cb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b30b5ae4-5b6e-4e5a-95a6-86496ef214f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a865fe99-f39c-4084-93ce-194863fc4cb7",
                    "LayerId": "04900853-040b-4435-9101-e9b47c646464"
                }
            ]
        },
        {
            "id": "56c27de8-43da-4386-b4f9-b74023425202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "compositeImage": {
                "id": "1295376f-28d6-4594-9c69-dcd51dcbad8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c27de8-43da-4386-b4f9-b74023425202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f431bdda-c002-49a4-a03e-ab3d06397033",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c27de8-43da-4386-b4f9-b74023425202",
                    "LayerId": "04900853-040b-4435-9101-e9b47c646464"
                }
            ]
        },
        {
            "id": "db9741ac-23a4-43ac-bd93-6df190367c54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "compositeImage": {
                "id": "ed55c322-b4c6-4ae1-b1e8-d14bb31c380a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db9741ac-23a4-43ac-bd93-6df190367c54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3bb15e3-aff3-4d1e-9f2a-bc7455ddaab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db9741ac-23a4-43ac-bd93-6df190367c54",
                    "LayerId": "04900853-040b-4435-9101-e9b47c646464"
                }
            ]
        },
        {
            "id": "f0af70be-a4a8-4a96-9637-d91819600d75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "compositeImage": {
                "id": "1339c541-581c-4539-9a62-801c98470e47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0af70be-a4a8-4a96-9637-d91819600d75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaa3e877-7393-4d66-bec7-d707aeb49527",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0af70be-a4a8-4a96-9637-d91819600d75",
                    "LayerId": "04900853-040b-4435-9101-e9b47c646464"
                }
            ]
        },
        {
            "id": "70c7582e-4309-48ea-a9a8-2003c0cf3fba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "compositeImage": {
                "id": "06bdcfc5-87c1-4131-9194-86e0fe2930c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70c7582e-4309-48ea-a9a8-2003c0cf3fba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "960c3b50-52a6-4f54-bd9d-d9b879b49eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70c7582e-4309-48ea-a9a8-2003c0cf3fba",
                    "LayerId": "04900853-040b-4435-9101-e9b47c646464"
                }
            ]
        },
        {
            "id": "3161266c-edd8-4d61-bcf4-565d50da2362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "compositeImage": {
                "id": "b3998f5b-17e7-4c3b-ae9b-fd729168c1d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3161266c-edd8-4d61-bcf4-565d50da2362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "020a8d31-4c54-48cd-8c52-009c5152ba55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3161266c-edd8-4d61-bcf4-565d50da2362",
                    "LayerId": "04900853-040b-4435-9101-e9b47c646464"
                }
            ]
        },
        {
            "id": "35d6118e-85a9-4e53-bf22-eac0c129c926",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "compositeImage": {
                "id": "9129577e-4721-4292-afaf-03f68df6f55d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d6118e-85a9-4e53-bf22-eac0c129c926",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8bca529-c3ce-43eb-8825-1ad7c937240d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d6118e-85a9-4e53-bf22-eac0c129c926",
                    "LayerId": "04900853-040b-4435-9101-e9b47c646464"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 158,
    "layers": [
        {
            "id": "04900853-040b-4435-9101-e9b47c646464",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4914c640-901c-40ad-b1ac-3877f6ad5309",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 149,
    "xorig": 74,
    "yorig": 75
}