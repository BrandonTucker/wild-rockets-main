{
    "id": "2203b363-5072-47b4-b2dd-0dd36be655d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange33buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbd695b8-c6e0-43e9-abe4-02eb465ef056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "compositeImage": {
                "id": "f1deaf03-aa27-4b2a-9454-2f4ec2daa155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbd695b8-c6e0-43e9-abe4-02eb465ef056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57bba4d0-1752-46cc-a07f-858c3251efa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbd695b8-c6e0-43e9-abe4-02eb465ef056",
                    "LayerId": "70b01d7c-f02f-4ece-923b-dc0db16ba313"
                }
            ]
        },
        {
            "id": "b120503d-e375-433f-94f5-1d80d9a64f62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "compositeImage": {
                "id": "c5c5a6a6-1e7b-417c-84ee-97db7264ed31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b120503d-e375-433f-94f5-1d80d9a64f62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "502e4c5d-ace6-43a4-b1c8-ca949122c9f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b120503d-e375-433f-94f5-1d80d9a64f62",
                    "LayerId": "70b01d7c-f02f-4ece-923b-dc0db16ba313"
                }
            ]
        },
        {
            "id": "01e69a4d-5362-4533-9aee-89839226a400",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "compositeImage": {
                "id": "8883771d-74cb-445e-89dc-c197e98119bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01e69a4d-5362-4533-9aee-89839226a400",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c5432ef-919e-4ab2-9d63-379896561e38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01e69a4d-5362-4533-9aee-89839226a400",
                    "LayerId": "70b01d7c-f02f-4ece-923b-dc0db16ba313"
                }
            ]
        },
        {
            "id": "2602f0c7-5c5e-4715-befd-6a2315107b73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "compositeImage": {
                "id": "963ccddc-cfa1-46ca-9b64-02d6091c4315",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2602f0c7-5c5e-4715-befd-6a2315107b73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b8cfc3e-8389-4531-8a26-c2142eb89315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2602f0c7-5c5e-4715-befd-6a2315107b73",
                    "LayerId": "70b01d7c-f02f-4ece-923b-dc0db16ba313"
                }
            ]
        },
        {
            "id": "b9d24a54-478b-42fb-aaac-9f94bd7ab36c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "compositeImage": {
                "id": "cf4555cb-ab42-4934-9f09-6796b4e4cace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d24a54-478b-42fb-aaac-9f94bd7ab36c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a09dc0ed-f537-4505-977d-c4569c200465",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d24a54-478b-42fb-aaac-9f94bd7ab36c",
                    "LayerId": "70b01d7c-f02f-4ece-923b-dc0db16ba313"
                }
            ]
        },
        {
            "id": "d58171c4-614c-48e5-a7f4-597ee1cdf821",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "compositeImage": {
                "id": "e8e7c761-6d12-4427-89df-cc82c97cb0ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d58171c4-614c-48e5-a7f4-597ee1cdf821",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d47085f0-97b6-41d5-b8df-27f28f001ba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d58171c4-614c-48e5-a7f4-597ee1cdf821",
                    "LayerId": "70b01d7c-f02f-4ece-923b-dc0db16ba313"
                }
            ]
        },
        {
            "id": "8bc9a6a2-4a42-467f-969d-1f534b39080e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "compositeImage": {
                "id": "94542479-2f83-4fcc-8ac0-5b0aef9d11e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bc9a6a2-4a42-467f-969d-1f534b39080e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657f7e40-2ca6-40fa-a3f5-fd85c0735049",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bc9a6a2-4a42-467f-969d-1f534b39080e",
                    "LayerId": "70b01d7c-f02f-4ece-923b-dc0db16ba313"
                }
            ]
        },
        {
            "id": "61d553ab-bdb0-466f-8711-34334daed4d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "compositeImage": {
                "id": "6a82e790-8c7c-4e1f-b8a0-d1d7647aa2dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61d553ab-bdb0-466f-8711-34334daed4d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1593552d-8142-4118-92a1-7ce9abc6cf11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61d553ab-bdb0-466f-8711-34334daed4d4",
                    "LayerId": "70b01d7c-f02f-4ece-923b-dc0db16ba313"
                }
            ]
        },
        {
            "id": "1d2a8b25-443e-4772-8a4b-d680f3affbe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "compositeImage": {
                "id": "3761b36d-5297-4db8-bced-a87b787516f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d2a8b25-443e-4772-8a4b-d680f3affbe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c96e7cc-15a6-4a09-90e3-2664c6bbe162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d2a8b25-443e-4772-8a4b-d680f3affbe0",
                    "LayerId": "70b01d7c-f02f-4ece-923b-dc0db16ba313"
                }
            ]
        },
        {
            "id": "55689351-e286-4806-a8d7-75f494e18b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "compositeImage": {
                "id": "ae058224-ef31-43f7-a1f7-fecd534d7cc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55689351-e286-4806-a8d7-75f494e18b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b10572-ace8-401d-bee5-d40b94f54d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55689351-e286-4806-a8d7-75f494e18b78",
                    "LayerId": "70b01d7c-f02f-4ece-923b-dc0db16ba313"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "70b01d7c-f02f-4ece-923b-dc0db16ba313",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2203b363-5072-47b4-b2dd-0dd36be655d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}