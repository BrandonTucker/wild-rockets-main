{
    "id": "c06d9ccc-a710-4000-a065-af9c2639be0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship3explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 138,
    "bbox_left": 31,
    "bbox_right": 65,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b01ffb4a-765c-446b-a2b9-4c9ac75c88d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c06d9ccc-a710-4000-a065-af9c2639be0c",
            "compositeImage": {
                "id": "4bf19a76-eb5a-476d-b6ee-f5cee62439d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b01ffb4a-765c-446b-a2b9-4c9ac75c88d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f87e761-a58c-492f-a0f7-9b646fd9c940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b01ffb4a-765c-446b-a2b9-4c9ac75c88d4",
                    "LayerId": "caad744b-51df-46b4-bf8c-a391f98c3d50"
                }
            ]
        },
        {
            "id": "7a4fdad9-06c5-44e5-b962-2dc21cb0c9e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c06d9ccc-a710-4000-a065-af9c2639be0c",
            "compositeImage": {
                "id": "f321f8cb-fe8f-4920-9c37-30355d4aef5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a4fdad9-06c5-44e5-b962-2dc21cb0c9e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a89ca4d-c4ee-4571-8f34-bf07b2b3d11f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a4fdad9-06c5-44e5-b962-2dc21cb0c9e5",
                    "LayerId": "caad744b-51df-46b4-bf8c-a391f98c3d50"
                }
            ]
        },
        {
            "id": "e7100173-f29c-452c-8eb8-090514881b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c06d9ccc-a710-4000-a065-af9c2639be0c",
            "compositeImage": {
                "id": "99b04acd-cab7-428c-8c6f-9e594da22684",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7100173-f29c-452c-8eb8-090514881b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0bb34e0-ce54-428c-b0da-032f837d25ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7100173-f29c-452c-8eb8-090514881b09",
                    "LayerId": "caad744b-51df-46b4-bf8c-a391f98c3d50"
                }
            ]
        },
        {
            "id": "469f72d3-b82c-4ceb-8945-35507e221468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c06d9ccc-a710-4000-a065-af9c2639be0c",
            "compositeImage": {
                "id": "de5cfcd9-45d7-4b94-8580-cfe24a1cf34f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "469f72d3-b82c-4ceb-8945-35507e221468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17822770-eb30-449e-8e95-038ab33e81f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "469f72d3-b82c-4ceb-8945-35507e221468",
                    "LayerId": "caad744b-51df-46b4-bf8c-a391f98c3d50"
                }
            ]
        },
        {
            "id": "cb309853-5850-46c8-a2d3-fc5f2e2e1100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c06d9ccc-a710-4000-a065-af9c2639be0c",
            "compositeImage": {
                "id": "9af1e98a-1731-43ce-a2a6-50e142859423",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb309853-5850-46c8-a2d3-fc5f2e2e1100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b62486db-d54d-4ea6-9156-2c0b52c94d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb309853-5850-46c8-a2d3-fc5f2e2e1100",
                    "LayerId": "caad744b-51df-46b4-bf8c-a391f98c3d50"
                }
            ]
        },
        {
            "id": "d7130f5f-8ca8-4adb-bdfe-29c5265b374c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c06d9ccc-a710-4000-a065-af9c2639be0c",
            "compositeImage": {
                "id": "bc7fcbd2-450e-4e03-b46f-8b5b69e584c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7130f5f-8ca8-4adb-bdfe-29c5265b374c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e1da800-ad81-4f0d-aa7e-c6811667c7d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7130f5f-8ca8-4adb-bdfe-29c5265b374c",
                    "LayerId": "caad744b-51df-46b4-bf8c-a391f98c3d50"
                }
            ]
        },
        {
            "id": "4cee61ff-f5f0-4eba-9979-66509a11707d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c06d9ccc-a710-4000-a065-af9c2639be0c",
            "compositeImage": {
                "id": "26202a20-ed9d-4d39-8e58-1480899ca676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cee61ff-f5f0-4eba-9979-66509a11707d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca800978-033b-4551-b91f-036e17c42247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cee61ff-f5f0-4eba-9979-66509a11707d",
                    "LayerId": "caad744b-51df-46b4-bf8c-a391f98c3d50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 154,
    "layers": [
        {
            "id": "caad744b-51df-46b4-bf8c-a391f98c3d50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c06d9ccc-a710-4000-a065-af9c2639be0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 53
}