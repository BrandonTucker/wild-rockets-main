{
    "id": "65426212-b449-4ba2-a32a-aa04ac75ffda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship14boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51abaa36-1e1c-422c-98af-010c6cdd7c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65426212-b449-4ba2-a32a-aa04ac75ffda",
            "compositeImage": {
                "id": "06002ec9-8a17-49f4-a3fb-2552206896ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51abaa36-1e1c-422c-98af-010c6cdd7c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "615acf4c-6904-4ef8-b667-5201fec23073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51abaa36-1e1c-422c-98af-010c6cdd7c5e",
                    "LayerId": "0b622771-8123-4cc6-8bbe-79e5520c7f70"
                }
            ]
        },
        {
            "id": "99a1931e-1ea2-471a-83e9-be41c3c8e53a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65426212-b449-4ba2-a32a-aa04ac75ffda",
            "compositeImage": {
                "id": "7e68f8fd-7dff-46ca-b01b-20cb0a6fe03c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99a1931e-1ea2-471a-83e9-be41c3c8e53a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b80ef6-49b7-47c8-a3b3-391b9eeb86b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99a1931e-1ea2-471a-83e9-be41c3c8e53a",
                    "LayerId": "0b622771-8123-4cc6-8bbe-79e5520c7f70"
                }
            ]
        },
        {
            "id": "4926d75a-2ae2-439c-b320-fd5626439660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65426212-b449-4ba2-a32a-aa04ac75ffda",
            "compositeImage": {
                "id": "04691312-925f-42ff-9bd1-04ad61e2ffbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4926d75a-2ae2-439c-b320-fd5626439660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b610c3d-cfed-45fa-85ee-c4fa896dc3ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4926d75a-2ae2-439c-b320-fd5626439660",
                    "LayerId": "0b622771-8123-4cc6-8bbe-79e5520c7f70"
                }
            ]
        },
        {
            "id": "7170e471-02d1-46d3-8b95-56c41bfd2f21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65426212-b449-4ba2-a32a-aa04ac75ffda",
            "compositeImage": {
                "id": "87aebe30-0ae3-40c2-ba01-382520a37140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7170e471-02d1-46d3-8b95-56c41bfd2f21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca502564-a778-4330-b219-e4c2e251403b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7170e471-02d1-46d3-8b95-56c41bfd2f21",
                    "LayerId": "0b622771-8123-4cc6-8bbe-79e5520c7f70"
                }
            ]
        },
        {
            "id": "91a37560-afc6-4510-b29b-f99bc77a34a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65426212-b449-4ba2-a32a-aa04ac75ffda",
            "compositeImage": {
                "id": "0237f001-ff66-42ea-8c15-93a0a27636ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91a37560-afc6-4510-b29b-f99bc77a34a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72e222d8-59d4-4151-98cf-5cbc00d0995e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91a37560-afc6-4510-b29b-f99bc77a34a3",
                    "LayerId": "0b622771-8123-4cc6-8bbe-79e5520c7f70"
                }
            ]
        },
        {
            "id": "9b9fd2c4-df96-43fd-b7b2-4363bdad713b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65426212-b449-4ba2-a32a-aa04ac75ffda",
            "compositeImage": {
                "id": "3bc96182-808d-4ae5-91d1-0d5847c41c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b9fd2c4-df96-43fd-b7b2-4363bdad713b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cafab7d9-ed77-4cb3-8c5c-b1dcca244098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b9fd2c4-df96-43fd-b7b2-4363bdad713b",
                    "LayerId": "0b622771-8123-4cc6-8bbe-79e5520c7f70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 162,
    "layers": [
        {
            "id": "0b622771-8123-4cc6-8bbe-79e5520c7f70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65426212-b449-4ba2-a32a-aa04ac75ffda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 65
}