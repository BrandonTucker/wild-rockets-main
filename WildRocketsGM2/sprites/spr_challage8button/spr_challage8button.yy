{
    "id": "e3ac22b7-fc1c-4b24-b0ec-3ab6e27beff4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage8button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c0a8503-39a7-48fc-9b70-a7af028c53aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ac22b7-fc1c-4b24-b0ec-3ab6e27beff4",
            "compositeImage": {
                "id": "895ca1b4-ff08-4934-be5d-1b606f144813",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c0a8503-39a7-48fc-9b70-a7af028c53aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4cacf63-553c-4312-9287-2f4fa5dbf692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c0a8503-39a7-48fc-9b70-a7af028c53aa",
                    "LayerId": "0e4d76f6-d277-4771-8c66-ad49136a2721"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "0e4d76f6-d277-4771-8c66-ad49136a2721",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3ac22b7-fc1c-4b24-b0ec-3ab6e27beff4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}