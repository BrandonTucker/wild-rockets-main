{
    "id": "53162758-122a-47e4-9819-5bd8c5132503",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bananas",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f72ac682-600b-44a7-8714-f6efa633f30e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "compositeImage": {
                "id": "99160807-6bb2-4e25-a3cb-d3d1836d0fe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f72ac682-600b-44a7-8714-f6efa633f30e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bd71f82-7836-40ea-8b23-7c0f6ff4885a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f72ac682-600b-44a7-8714-f6efa633f30e",
                    "LayerId": "2610a686-df58-4491-9c08-35fbdb24c572"
                }
            ]
        },
        {
            "id": "ab018398-5f56-4fe3-878c-36dacc0c4ac8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "compositeImage": {
                "id": "74af14ad-721b-4b5a-8d65-a0c1605bf0f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab018398-5f56-4fe3-878c-36dacc0c4ac8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70712004-f8a3-4585-b3ed-6cf1c92e0505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab018398-5f56-4fe3-878c-36dacc0c4ac8",
                    "LayerId": "2610a686-df58-4491-9c08-35fbdb24c572"
                }
            ]
        },
        {
            "id": "f203e8e6-8f56-4396-8351-654828179906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "compositeImage": {
                "id": "7524fcd5-dcf7-44db-84f9-d6ce1686db94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f203e8e6-8f56-4396-8351-654828179906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c39884f1-a903-4660-a7b7-e56104ddcbf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f203e8e6-8f56-4396-8351-654828179906",
                    "LayerId": "2610a686-df58-4491-9c08-35fbdb24c572"
                }
            ]
        },
        {
            "id": "d51aa971-9068-411a-bbbb-6889e9d1e853",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "compositeImage": {
                "id": "6a1150e4-7c7b-49c1-a2e2-db1f01c6dcd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d51aa971-9068-411a-bbbb-6889e9d1e853",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40f0ba6e-b4f4-4ee9-9830-5bc79db4e186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d51aa971-9068-411a-bbbb-6889e9d1e853",
                    "LayerId": "2610a686-df58-4491-9c08-35fbdb24c572"
                }
            ]
        },
        {
            "id": "afd948fc-8c00-4f35-9764-ef41a4c7966c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "compositeImage": {
                "id": "b886eba9-d736-4f89-9e6f-cd411f2e8454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afd948fc-8c00-4f35-9764-ef41a4c7966c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc9af1f4-e840-4f7d-b0d8-f4129cb2f141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afd948fc-8c00-4f35-9764-ef41a4c7966c",
                    "LayerId": "2610a686-df58-4491-9c08-35fbdb24c572"
                }
            ]
        },
        {
            "id": "fc105c24-e032-49ed-8a4c-628111474ca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "compositeImage": {
                "id": "38a65f3f-fde1-472a-8d4f-a46bed90e21c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc105c24-e032-49ed-8a4c-628111474ca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9ec758a-f9ad-4130-bce1-88f3fc703e4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc105c24-e032-49ed-8a4c-628111474ca8",
                    "LayerId": "2610a686-df58-4491-9c08-35fbdb24c572"
                }
            ]
        },
        {
            "id": "c3e9ba0f-731a-46d2-aaf1-1eb1a80ca494",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "compositeImage": {
                "id": "616a64d7-f580-4dc0-9525-b735bad9a843",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3e9ba0f-731a-46d2-aaf1-1eb1a80ca494",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7584b46-fd57-4450-8b38-f36cfcb220bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3e9ba0f-731a-46d2-aaf1-1eb1a80ca494",
                    "LayerId": "2610a686-df58-4491-9c08-35fbdb24c572"
                }
            ]
        },
        {
            "id": "cdb300e3-96bc-45dc-9ac5-32f703382f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "compositeImage": {
                "id": "83a65938-4b53-400f-940e-6ff3218767e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdb300e3-96bc-45dc-9ac5-32f703382f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d57a5513-4485-4d15-9ce0-7f4004ba3a79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdb300e3-96bc-45dc-9ac5-32f703382f32",
                    "LayerId": "2610a686-df58-4491-9c08-35fbdb24c572"
                }
            ]
        },
        {
            "id": "e5c253f5-959c-4e87-bc37-41f58ba33af6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "compositeImage": {
                "id": "1b40c672-dbf1-413a-b8e3-534db253529f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c253f5-959c-4e87-bc37-41f58ba33af6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a70446-b237-4120-9d2c-589c5c153664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c253f5-959c-4e87-bc37-41f58ba33af6",
                    "LayerId": "2610a686-df58-4491-9c08-35fbdb24c572"
                }
            ]
        },
        {
            "id": "d41903ec-74a1-4cf8-8e61-d701b6f14098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "compositeImage": {
                "id": "e800140d-4e11-44d3-a9e7-e48e0c4ec4a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d41903ec-74a1-4cf8-8e61-d701b6f14098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "544d0b49-c2ce-47f5-846c-776d28e8b06a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d41903ec-74a1-4cf8-8e61-d701b6f14098",
                    "LayerId": "2610a686-df58-4491-9c08-35fbdb24c572"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "2610a686-df58-4491-9c08-35fbdb24c572",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53162758-122a-47e4-9819-5bd8c5132503",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 42,
    "yorig": 38
}