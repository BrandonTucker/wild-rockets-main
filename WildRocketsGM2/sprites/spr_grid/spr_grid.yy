{
    "id": "46ac9a6f-d48a-4d48-9755-860814abd4a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 343,
    "bbox_left": 0,
    "bbox_right": 450,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7852ec22-5030-4800-b986-2dff847fa4a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46ac9a6f-d48a-4d48-9755-860814abd4a1",
            "compositeImage": {
                "id": "e767178a-d99a-468d-9cbb-8311c0f863c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7852ec22-5030-4800-b986-2dff847fa4a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "973bc6db-7792-4be9-9c62-320ed090650a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7852ec22-5030-4800-b986-2dff847fa4a8",
                    "LayerId": "55e27de6-ef4d-41c8-aea8-d08ad1add587"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 344,
    "layers": [
        {
            "id": "55e27de6-ef4d-41c8-aea8-d08ad1add587",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46ac9a6f-d48a-4d48-9755-860814abd4a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 451,
    "xorig": 225,
    "yorig": 172
}