{
    "id": "e89fb926-0108-4e00-a506-becd918d39f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bluemetalchunk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 0,
    "bbox_right": 45,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82e0bb37-865f-4b41-b752-1297f9441dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "e0f9f655-a6ac-4976-978b-3ec37105058c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e0bb37-865f-4b41-b752-1297f9441dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d95615d9-5871-4261-bd27-ef72026f2117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e0bb37-865f-4b41-b752-1297f9441dbc",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "d0ddba34-ca9f-4205-985b-4e4794720f11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "85e4f54a-6b8f-45ef-9ddb-979ca3de5034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0ddba34-ca9f-4205-985b-4e4794720f11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56ec8850-11f9-407a-91cc-d2d91b619307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0ddba34-ca9f-4205-985b-4e4794720f11",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "99e7caef-e57e-4aa9-9aab-70a20b2a9845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "ec79a4f5-b722-4d98-a959-291c054b814a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99e7caef-e57e-4aa9-9aab-70a20b2a9845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce0dee4d-011f-4559-9a9e-de01434e2533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99e7caef-e57e-4aa9-9aab-70a20b2a9845",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "4bd09e8b-041a-4cae-ab7d-79dd4dd1c288",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "90b22afb-a7be-4ed9-ba64-c668d6e2a9da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd09e8b-041a-4cae-ab7d-79dd4dd1c288",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef93a1e1-359a-426b-ba52-0c3b66618d2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd09e8b-041a-4cae-ab7d-79dd4dd1c288",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "422d5efb-9cbf-4717-8511-188f4b0268fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "fc5fc79d-78a5-498d-9d04-e27d65885972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "422d5efb-9cbf-4717-8511-188f4b0268fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f693e976-65f3-451d-8b1c-824ab3670174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "422d5efb-9cbf-4717-8511-188f4b0268fd",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "059932cd-a4c2-4548-bbe8-ccc0b09b8521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "ba4eae99-c4e6-476c-bb2f-32e37bf3ae02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "059932cd-a4c2-4548-bbe8-ccc0b09b8521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45554715-48c9-49a6-b13b-2074201586fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "059932cd-a4c2-4548-bbe8-ccc0b09b8521",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "13948fdb-8c5a-47cd-95bc-c6c7fd847357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "73288871-08d1-48f1-a63c-58538e115cb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13948fdb-8c5a-47cd-95bc-c6c7fd847357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a766d34e-cce0-4b92-bf99-9043bcc1e2ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13948fdb-8c5a-47cd-95bc-c6c7fd847357",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "3cce1ff2-717f-4227-9e71-9a2c078d724a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "520e0ba9-4cec-4767-9170-bfde3c8ab801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cce1ff2-717f-4227-9e71-9a2c078d724a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc5c0299-aac1-40f9-aae9-e51bbb5ee121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cce1ff2-717f-4227-9e71-9a2c078d724a",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "7a662d12-d534-49dc-a23c-c3b9794fa651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "30ea7478-6fe2-4147-a18d-2ed5f81801dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a662d12-d534-49dc-a23c-c3b9794fa651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a62e52df-1055-4c03-afaf-99c5fdf0e0a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a662d12-d534-49dc-a23c-c3b9794fa651",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "68f529bb-b59a-4f96-82ba-4900840f87e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "9cd8f583-0f8d-47ca-bcbe-afd0da7ff538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68f529bb-b59a-4f96-82ba-4900840f87e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e85cb5d-47e7-43c2-913c-64342968e2b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68f529bb-b59a-4f96-82ba-4900840f87e2",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "029d1c8c-41bf-4c8e-a5c0-2f6d9f825cc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "f0c86bd4-e230-4cf7-89e5-628d8a23ea08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "029d1c8c-41bf-4c8e-a5c0-2f6d9f825cc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "947f61b6-4119-4a88-8f75-d3138ab47683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "029d1c8c-41bf-4c8e-a5c0-2f6d9f825cc5",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "8f5896f9-7256-447b-bd4c-7369110b4335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "4b2e13c7-3fc5-456e-aca0-cde1d8295549",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f5896f9-7256-447b-bd4c-7369110b4335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a041c4eb-885b-4e71-be42-be6170e310ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f5896f9-7256-447b-bd4c-7369110b4335",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "32b56d97-7dcc-4bea-9876-ff6eec4c988e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "07fecb3e-1dd7-40bb-947c-eec8d5cf4593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32b56d97-7dcc-4bea-9876-ff6eec4c988e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871dc47c-4a01-45a4-883b-dc4d7763a4f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32b56d97-7dcc-4bea-9876-ff6eec4c988e",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "f7640cfe-94b1-4fef-9b3f-429a140b94aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "20200bc5-b79d-43fd-8bbb-0c6aa947c3fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7640cfe-94b1-4fef-9b3f-429a140b94aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecf1dc36-dba3-4027-80c8-ef54830fecd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7640cfe-94b1-4fef-9b3f-429a140b94aa",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        },
        {
            "id": "762f52a9-da93-484a-b960-04e9e78feeae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "compositeImage": {
                "id": "1fba2bfb-1b92-4c5e-93d7-07fefa644776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "762f52a9-da93-484a-b960-04e9e78feeae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cddcaf9c-b9c4-484c-9747-c8ba63ac8eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "762f52a9-da93-484a-b960-04e9e78feeae",
                    "LayerId": "1f680449-9d12-4441-b05f-5786aa73ba67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "1f680449-9d12-4441-b05f-5786aa73ba67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e89fb926-0108-4e00-a506-becd918d39f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 23,
    "yorig": 23
}