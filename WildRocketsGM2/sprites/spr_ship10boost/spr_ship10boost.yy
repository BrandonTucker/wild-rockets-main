{
    "id": "47a74024-5503-488c-84ca-2a0f3cc8d381",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship10boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 15,
    "bbox_right": 40,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e1e1154-8062-42cc-86a4-26dbcc5442f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47a74024-5503-488c-84ca-2a0f3cc8d381",
            "compositeImage": {
                "id": "260c0322-50dc-4af3-a090-d6bb5e614973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e1e1154-8062-42cc-86a4-26dbcc5442f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7be55868-0627-4581-b2ee-d2b1aff466b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e1e1154-8062-42cc-86a4-26dbcc5442f0",
                    "LayerId": "cb2f1138-b0e5-46bf-89e2-7684b28b4341"
                }
            ]
        },
        {
            "id": "3610eef5-33e5-4e46-b242-91f490e4d6a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47a74024-5503-488c-84ca-2a0f3cc8d381",
            "compositeImage": {
                "id": "0e0ddfdf-9174-49e2-ad0c-b9187b24c6b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3610eef5-33e5-4e46-b242-91f490e4d6a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86bb9df5-7fab-4983-9c3b-af67399e20d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3610eef5-33e5-4e46-b242-91f490e4d6a1",
                    "LayerId": "cb2f1138-b0e5-46bf-89e2-7684b28b4341"
                }
            ]
        },
        {
            "id": "23f70245-c468-417d-9c78-c463584eb0d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47a74024-5503-488c-84ca-2a0f3cc8d381",
            "compositeImage": {
                "id": "60e239c3-31f3-4d2c-a514-1918e7d26d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23f70245-c468-417d-9c78-c463584eb0d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4861575d-36a4-45c2-87a1-e686d78eb66f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23f70245-c468-417d-9c78-c463584eb0d7",
                    "LayerId": "cb2f1138-b0e5-46bf-89e2-7684b28b4341"
                }
            ]
        },
        {
            "id": "c3edfdd8-19ef-4cc3-bd42-f0bb8b4a2dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47a74024-5503-488c-84ca-2a0f3cc8d381",
            "compositeImage": {
                "id": "d409f037-9ab1-48aa-829a-ec74791b07ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3edfdd8-19ef-4cc3-bd42-f0bb8b4a2dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05a9cd97-420f-4473-9221-ab49a9583793",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3edfdd8-19ef-4cc3-bd42-f0bb8b4a2dc8",
                    "LayerId": "cb2f1138-b0e5-46bf-89e2-7684b28b4341"
                }
            ]
        },
        {
            "id": "8a6cc1d6-c3a0-4465-917b-16c8b4160556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47a74024-5503-488c-84ca-2a0f3cc8d381",
            "compositeImage": {
                "id": "905145c8-21d1-40e3-a3a6-6b3af2299ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a6cc1d6-c3a0-4465-917b-16c8b4160556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0cb6558-cf1c-445f-93fa-8b7433e9ee2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a6cc1d6-c3a0-4465-917b-16c8b4160556",
                    "LayerId": "cb2f1138-b0e5-46bf-89e2-7684b28b4341"
                }
            ]
        },
        {
            "id": "a44db20f-f1bc-4be3-954f-5fe433c519d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47a74024-5503-488c-84ca-2a0f3cc8d381",
            "compositeImage": {
                "id": "458b76bc-7742-4fc6-bfa0-523905851b3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a44db20f-f1bc-4be3-954f-5fe433c519d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3982f225-bd45-4f1b-83fb-3bbd195fe9d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a44db20f-f1bc-4be3-954f-5fe433c519d0",
                    "LayerId": "cb2f1138-b0e5-46bf-89e2-7684b28b4341"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "cb2f1138-b0e5-46bf-89e2-7684b28b4341",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47a74024-5503-488c-84ca-2a0f3cc8d381",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 27,
    "yorig": 49
}