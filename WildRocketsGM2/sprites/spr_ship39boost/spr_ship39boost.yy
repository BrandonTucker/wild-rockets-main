{
    "id": "251f4843-05ee-461b-8c38-fe1530b93baf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship39boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 49,
    "bbox_right": 74,
    "bbox_top": 63,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99cc898b-a365-4d36-9580-60da77ac82ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251f4843-05ee-461b-8c38-fe1530b93baf",
            "compositeImage": {
                "id": "1f4f46a2-7807-4b83-9d38-e128cc251916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99cc898b-a365-4d36-9580-60da77ac82ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48bd738-26f5-43a5-96b4-828b79c081a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99cc898b-a365-4d36-9580-60da77ac82ab",
                    "LayerId": "fe62e7d7-80ca-4587-9fd9-7dfaa016af9e"
                }
            ]
        },
        {
            "id": "15265b9b-0c13-4c71-92b7-e0d8204d1d39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251f4843-05ee-461b-8c38-fe1530b93baf",
            "compositeImage": {
                "id": "445ab8bd-ae96-4817-ad9a-33342775e7d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15265b9b-0c13-4c71-92b7-e0d8204d1d39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b7a6646-46ff-4cc8-b067-abb668b0839a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15265b9b-0c13-4c71-92b7-e0d8204d1d39",
                    "LayerId": "fe62e7d7-80ca-4587-9fd9-7dfaa016af9e"
                }
            ]
        },
        {
            "id": "829274ff-3ad1-4943-a082-62aa3a48177f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251f4843-05ee-461b-8c38-fe1530b93baf",
            "compositeImage": {
                "id": "95252b27-2e86-458a-8ed2-7e3126ff8a94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "829274ff-3ad1-4943-a082-62aa3a48177f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5512009e-0df3-43ff-9143-da905cbae3d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "829274ff-3ad1-4943-a082-62aa3a48177f",
                    "LayerId": "fe62e7d7-80ca-4587-9fd9-7dfaa016af9e"
                }
            ]
        },
        {
            "id": "bab33555-0ef3-4408-8828-d607616ee5a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251f4843-05ee-461b-8c38-fe1530b93baf",
            "compositeImage": {
                "id": "322e0bb1-34e6-4b85-9f0e-cf2ffe9c7f1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bab33555-0ef3-4408-8828-d607616ee5a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "598f0f8c-d994-4b1a-913e-586b51aec57d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bab33555-0ef3-4408-8828-d607616ee5a7",
                    "LayerId": "fe62e7d7-80ca-4587-9fd9-7dfaa016af9e"
                }
            ]
        },
        {
            "id": "65a06666-7220-48c1-92c7-0ca0895a9e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251f4843-05ee-461b-8c38-fe1530b93baf",
            "compositeImage": {
                "id": "46bd6dba-81e0-49ae-b06c-dbaee2c53253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65a06666-7220-48c1-92c7-0ca0895a9e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e2eb87-fa23-4444-824d-04ef8a9d2479",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a06666-7220-48c1-92c7-0ca0895a9e2b",
                    "LayerId": "fe62e7d7-80ca-4587-9fd9-7dfaa016af9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 301,
    "layers": [
        {
            "id": "fe62e7d7-80ca-4587-9fd9-7dfaa016af9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "251f4843-05ee-461b-8c38-fe1530b93baf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 62,
    "yorig": 88
}