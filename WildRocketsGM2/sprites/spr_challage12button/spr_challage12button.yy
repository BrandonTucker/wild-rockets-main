{
    "id": "29651981-1a85-4e1b-ac24-5e8a4f7e07a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage12button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb438b08-c121-46b8-bfb6-42af23aa21b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29651981-1a85-4e1b-ac24-5e8a4f7e07a4",
            "compositeImage": {
                "id": "001f533e-1cd4-48b3-bae7-7eeb637ce147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb438b08-c121-46b8-bfb6-42af23aa21b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d111fb93-89a5-4eb8-b411-47ed8ff2750e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb438b08-c121-46b8-bfb6-42af23aa21b7",
                    "LayerId": "1a7ac801-fe95-4fe7-8b33-194b5ebfb364"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1a7ac801-fe95-4fe7-8b33-194b5ebfb364",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29651981-1a85-4e1b-ac24-5e8a4f7e07a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}