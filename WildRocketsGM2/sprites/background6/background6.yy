{
    "id": "d705b7d6-52b8-4987-9ee3-2c5d63a54c1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background6",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b34113c-4d33-43d9-84a7-7365c5e0758c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d705b7d6-52b8-4987-9ee3-2c5d63a54c1b",
            "compositeImage": {
                "id": "0555cbe6-49a8-4e20-9a7c-3b6a6826017b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b34113c-4d33-43d9-84a7-7365c5e0758c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c375c058-c385-40ae-b49b-43ece4df726d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b34113c-4d33-43d9-84a7-7365c5e0758c",
                    "LayerId": "15eab71d-1995-4946-b04f-2a679380568b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "15eab71d-1995-4946-b04f-2a679380568b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d705b7d6-52b8-4987-9ee3-2c5d63a54c1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}