{
    "id": "277b2104-174c-48df-b9dc-ee3eae7789af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship6explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 29,
    "bbox_right": 54,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aea1fa8f-7503-49ed-a043-d50ac03d1804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "277b2104-174c-48df-b9dc-ee3eae7789af",
            "compositeImage": {
                "id": "34211924-f291-40a5-92a7-f11e82061d3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea1fa8f-7503-49ed-a043-d50ac03d1804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc2c805b-3add-4e44-a6f2-cc67d002f990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea1fa8f-7503-49ed-a043-d50ac03d1804",
                    "LayerId": "7c5180f0-3545-4eb8-a0ad-5d2b2bdddd20"
                }
            ]
        },
        {
            "id": "ecf5eece-241d-4114-a7c5-2dfffe1b71a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "277b2104-174c-48df-b9dc-ee3eae7789af",
            "compositeImage": {
                "id": "aab4e40d-a050-4205-807d-9f899cf39ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf5eece-241d-4114-a7c5-2dfffe1b71a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead3bd39-c9cb-4d76-96c6-1a7e9cb7ad62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf5eece-241d-4114-a7c5-2dfffe1b71a6",
                    "LayerId": "7c5180f0-3545-4eb8-a0ad-5d2b2bdddd20"
                }
            ]
        },
        {
            "id": "16cbd358-7a49-479c-91d7-d5fda7707051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "277b2104-174c-48df-b9dc-ee3eae7789af",
            "compositeImage": {
                "id": "a6ffaee0-4c60-45d0-9822-396f4a9224a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16cbd358-7a49-479c-91d7-d5fda7707051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe3cd042-4dc1-45fa-87ef-71e4369ca87e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16cbd358-7a49-479c-91d7-d5fda7707051",
                    "LayerId": "7c5180f0-3545-4eb8-a0ad-5d2b2bdddd20"
                }
            ]
        },
        {
            "id": "879ba4f7-21b6-4b59-bd8c-f54567bdd242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "277b2104-174c-48df-b9dc-ee3eae7789af",
            "compositeImage": {
                "id": "1bff8ea4-12e3-44c4-80f3-6dcbc8a355d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "879ba4f7-21b6-4b59-bd8c-f54567bdd242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7feccab-2941-461b-8758-b3773c0a814f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "879ba4f7-21b6-4b59-bd8c-f54567bdd242",
                    "LayerId": "7c5180f0-3545-4eb8-a0ad-5d2b2bdddd20"
                }
            ]
        },
        {
            "id": "377eb701-5278-4ca7-8bcc-e18f771c7143",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "277b2104-174c-48df-b9dc-ee3eae7789af",
            "compositeImage": {
                "id": "d7568204-c2e7-48d6-8feb-291a4ff50e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "377eb701-5278-4ca7-8bcc-e18f771c7143",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "892ed3e0-1ec9-42f2-94a9-66815978143a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "377eb701-5278-4ca7-8bcc-e18f771c7143",
                    "LayerId": "7c5180f0-3545-4eb8-a0ad-5d2b2bdddd20"
                }
            ]
        },
        {
            "id": "a9e92d30-7206-4408-9a38-ace805eae593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "277b2104-174c-48df-b9dc-ee3eae7789af",
            "compositeImage": {
                "id": "e1c3d8f9-4d0c-47c5-85d0-61d1f94d6a80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9e92d30-7206-4408-9a38-ace805eae593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d99541db-b8a8-42ea-aa1d-7cf26a651419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9e92d30-7206-4408-9a38-ace805eae593",
                    "LayerId": "7c5180f0-3545-4eb8-a0ad-5d2b2bdddd20"
                }
            ]
        },
        {
            "id": "51ada6c5-03d8-420b-b77c-f6b2b4f903a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "277b2104-174c-48df-b9dc-ee3eae7789af",
            "compositeImage": {
                "id": "23d66f62-ebcd-4ac4-81a0-58129e793eef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ada6c5-03d8-420b-b77c-f6b2b4f903a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c036275-d607-4c88-b531-cb8430b35679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ada6c5-03d8-420b-b77c-f6b2b4f903a5",
                    "LayerId": "7c5180f0-3545-4eb8-a0ad-5d2b2bdddd20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "7c5180f0-3545-4eb8-a0ad-5d2b2bdddd20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "277b2104-174c-48df-b9dc-ee3eae7789af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 42,
    "yorig": 47
}