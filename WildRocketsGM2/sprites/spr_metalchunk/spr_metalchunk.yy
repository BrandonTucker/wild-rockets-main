{
    "id": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_metalchunk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97a1c03b-96e7-4e09-887c-07c9fd8803f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "397170f9-57c8-4be7-b2d1-164f10f10eb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a1c03b-96e7-4e09-887c-07c9fd8803f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3490ea04-e779-43dc-af92-91e0a28dc5f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a1c03b-96e7-4e09-887c-07c9fd8803f2",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "cffe94f3-aa33-43b5-b4d2-70426a2f4cf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "9e791c89-2446-4ec3-87c0-fad2c4566aa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cffe94f3-aa33-43b5-b4d2-70426a2f4cf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06f08231-f0e6-4121-a7b1-e5b5ce58c02b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffe94f3-aa33-43b5-b4d2-70426a2f4cf7",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "e5be2b1a-651e-4922-9cb9-96570e58c3ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "9710fc7b-dbbe-4199-8b4a-d5c83091eddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5be2b1a-651e-4922-9cb9-96570e58c3ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7681d4da-ed68-4dfb-bcf7-9b82750d03ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5be2b1a-651e-4922-9cb9-96570e58c3ff",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "4b51ab1d-56b7-42f6-87bb-2dc64ea3b5eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "4ee8457b-076d-48dd-9075-90c144d5aebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b51ab1d-56b7-42f6-87bb-2dc64ea3b5eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29031c56-e2cc-4039-881e-e917d42fcdc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b51ab1d-56b7-42f6-87bb-2dc64ea3b5eb",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "0b166830-c427-48d5-9624-21f05d042a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "aebb5541-6fea-4b6f-b0c3-3d62f4ac309e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b166830-c427-48d5-9624-21f05d042a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e29fac53-626d-4a83-857a-89b333a54786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b166830-c427-48d5-9624-21f05d042a8d",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "1bc9531b-4754-48bd-8ada-de6aa10fc137",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "ddd4fe43-fd27-4eff-9d6d-6c99fc470a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bc9531b-4754-48bd-8ada-de6aa10fc137",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a30a6e0-a05c-4caa-8457-9d98f2bfa2a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bc9531b-4754-48bd-8ada-de6aa10fc137",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "8f20c766-d571-4cee-afe0-056e9ed8862e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "04bcbbaa-d457-4451-a7b0-00b151e2309b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f20c766-d571-4cee-afe0-056e9ed8862e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6391c6aa-665a-4e63-aa54-30e99552b455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f20c766-d571-4cee-afe0-056e9ed8862e",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "008c4272-db21-4f87-8f11-fc9e627e2916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "e8a0b9bc-3c2f-44c7-a851-7e1834b31040",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "008c4272-db21-4f87-8f11-fc9e627e2916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a915eeb-cc2a-4878-845c-ecfc5d26e22f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "008c4272-db21-4f87-8f11-fc9e627e2916",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "2f97a7bd-92ef-4253-a8b6-0c237733c0c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "7ab481c4-a44c-4a6a-a46e-80b01f11893f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f97a7bd-92ef-4253-a8b6-0c237733c0c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2accbdd1-073b-4892-92d5-ad0ab974c10c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f97a7bd-92ef-4253-a8b6-0c237733c0c9",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "eb810744-c884-46da-a954-8c9759f3fd47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "1d998634-4b99-4835-a77e-b51026e677b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb810744-c884-46da-a954-8c9759f3fd47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "599df49f-81dc-4c41-8d0c-b1f80ebbc184",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb810744-c884-46da-a954-8c9759f3fd47",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "0adc36fc-12d3-4091-95ff-99253a20bf2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "8e8589b1-f662-438d-b565-40b15affcd00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0adc36fc-12d3-4091-95ff-99253a20bf2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98faecff-b290-434e-990e-984c8e1344b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0adc36fc-12d3-4091-95ff-99253a20bf2f",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "2a99299b-2b6a-46f7-9265-a6404a43c332",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "afadcb3e-f8bf-4743-8fea-674a00d3bfa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a99299b-2b6a-46f7-9265-a6404a43c332",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4933a6af-920b-417c-a20e-e9355cda7296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a99299b-2b6a-46f7-9265-a6404a43c332",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "e927e765-aa5d-4eb5-894a-e20ac8146c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "4b1affc3-9469-423f-8536-2316b38fe740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e927e765-aa5d-4eb5-894a-e20ac8146c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7d374ca-e551-4db8-959a-111efdd36239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e927e765-aa5d-4eb5-894a-e20ac8146c76",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "70bf8664-70c0-4f1e-9fe8-77a8728121ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "378dd1fd-aafe-4742-a0cd-c8812e273000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70bf8664-70c0-4f1e-9fe8-77a8728121ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a1211c6-0436-4526-9718-bf1e46bbe19f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70bf8664-70c0-4f1e-9fe8-77a8728121ef",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        },
        {
            "id": "7f637a12-acef-49dc-b114-8fe53ce7f860",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "compositeImage": {
                "id": "1c65b698-1334-4ae2-88ab-485d085230f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f637a12-acef-49dc-b114-8fe53ce7f860",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50855e32-46df-4d32-bb3e-9020f010665c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f637a12-acef-49dc-b114-8fe53ce7f860",
                    "LayerId": "253a98ad-dca8-44bd-8ce8-f582e226e90d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 61,
    "layers": [
        {
            "id": "253a98ad-dca8-44bd-8ce8-f582e226e90d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da592ef5-a796-4bd1-a9f7-5cccd388503b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}