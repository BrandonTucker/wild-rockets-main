{
    "id": "675243f1-9100-4117-98b9-5aa7514d8388",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship28explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40f700c8-ddca-4b7b-a4fe-14de0318a627",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "675243f1-9100-4117-98b9-5aa7514d8388",
            "compositeImage": {
                "id": "13471634-c8cb-4f0e-a0ad-132cbfb139eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40f700c8-ddca-4b7b-a4fe-14de0318a627",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dfb2104-9a27-475d-b1b2-49c564e0b793",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40f700c8-ddca-4b7b-a4fe-14de0318a627",
                    "LayerId": "3adb7222-8321-436e-beda-7b8d55d17dc8"
                }
            ]
        },
        {
            "id": "c67de454-5a6a-417d-9018-940c5336f9e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "675243f1-9100-4117-98b9-5aa7514d8388",
            "compositeImage": {
                "id": "a370ddd2-b6c7-4674-94ba-9526cfe0f1c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c67de454-5a6a-417d-9018-940c5336f9e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df7acd44-863f-42ca-b304-e093551df413",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c67de454-5a6a-417d-9018-940c5336f9e6",
                    "LayerId": "3adb7222-8321-436e-beda-7b8d55d17dc8"
                }
            ]
        },
        {
            "id": "33051825-9464-4d0b-87f8-52c13854765d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "675243f1-9100-4117-98b9-5aa7514d8388",
            "compositeImage": {
                "id": "c64e8cc6-095a-48ad-a9be-8ac7c6ae4de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33051825-9464-4d0b-87f8-52c13854765d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13433d2a-91b3-4db4-ad54-1fdc274e9237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33051825-9464-4d0b-87f8-52c13854765d",
                    "LayerId": "3adb7222-8321-436e-beda-7b8d55d17dc8"
                }
            ]
        },
        {
            "id": "ec12afac-0acb-40d3-8858-74253809d297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "675243f1-9100-4117-98b9-5aa7514d8388",
            "compositeImage": {
                "id": "e37043ef-6fea-44a1-aa45-6426cb4f5a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec12afac-0acb-40d3-8858-74253809d297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b3d1dbe-b326-468d-a207-5f8a7bf9d0cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec12afac-0acb-40d3-8858-74253809d297",
                    "LayerId": "3adb7222-8321-436e-beda-7b8d55d17dc8"
                }
            ]
        },
        {
            "id": "5d7736d4-8505-4ed7-8f40-d38c061036e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "675243f1-9100-4117-98b9-5aa7514d8388",
            "compositeImage": {
                "id": "744a84ad-db56-42f3-b721-35e1c46ef8c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d7736d4-8505-4ed7-8f40-d38c061036e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "145c1176-a79c-443b-a288-2013bd5c3a1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d7736d4-8505-4ed7-8f40-d38c061036e0",
                    "LayerId": "3adb7222-8321-436e-beda-7b8d55d17dc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 188,
    "layers": [
        {
            "id": "3adb7222-8321-436e-beda-7b8d55d17dc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "675243f1-9100-4117-98b9-5aa7514d8388",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 152,
    "xorig": 76,
    "yorig": 78
}