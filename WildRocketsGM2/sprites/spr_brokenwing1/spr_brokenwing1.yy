{
    "id": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_brokenwing1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 0,
    "bbox_right": 56,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "994b9be9-7825-4010-9089-00b02e4d77d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "70811ccd-93da-48f0-be77-df73d37f3c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "994b9be9-7825-4010-9089-00b02e4d77d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f671aefc-4a7d-44b1-9dc5-89bd0aa98df0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "994b9be9-7825-4010-9089-00b02e4d77d5",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "e12458ba-3718-44ce-9f17-91974f1a91bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "6d96f793-1432-446d-8544-f6ab2edcbbbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e12458ba-3718-44ce-9f17-91974f1a91bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8efd292f-2748-4cce-94e2-b6c41765313d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e12458ba-3718-44ce-9f17-91974f1a91bd",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "b5e14741-8a96-4ebf-8739-149c381fffcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "d05e25ea-b3f5-47f6-930d-a0b2107d3182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e14741-8a96-4ebf-8739-149c381fffcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ae77c58-8f3a-440f-b011-c00ed4068da0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e14741-8a96-4ebf-8739-149c381fffcf",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "82e39762-ab92-4b7d-a01e-304580bee16c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "ba5aa694-6a26-48bf-8d32-1463ac4f0797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e39762-ab92-4b7d-a01e-304580bee16c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eec40fde-ddbf-4dee-b23e-03ae4723c91d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e39762-ab92-4b7d-a01e-304580bee16c",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "09b1cb58-20cb-414b-8619-a99df4cc3fc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "f08a476e-2c59-4a0d-8a1f-c3467871d8ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09b1cb58-20cb-414b-8619-a99df4cc3fc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb1aee6-369a-46d0-a711-48c4e109f307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09b1cb58-20cb-414b-8619-a99df4cc3fc8",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "ec02df6c-1959-45f3-8936-565203c93292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "07b3b171-5fbc-4922-93d8-11ed404dfa68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec02df6c-1959-45f3-8936-565203c93292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e91f8bf-4733-4729-b139-bab47223aaea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec02df6c-1959-45f3-8936-565203c93292",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "833b53fb-0103-4341-b9c3-9f8b5088c1a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "ec4a15a7-c292-4704-8bf4-a0ab5707c0bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "833b53fb-0103-4341-b9c3-9f8b5088c1a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec65d219-e860-4816-bbf0-79020366ada0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "833b53fb-0103-4341-b9c3-9f8b5088c1a3",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "a3bd66d7-6c4d-457a-81fd-697271a9d170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "336cfe92-8c49-44a7-919c-89f1f7988303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3bd66d7-6c4d-457a-81fd-697271a9d170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c173bcf3-c001-48f9-8781-cf7ddf26fd54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3bd66d7-6c4d-457a-81fd-697271a9d170",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "d69c81a3-7d15-40b7-8467-87167a5f7bbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "ead99f46-f61c-4f9c-90de-1fbf729eaf6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d69c81a3-7d15-40b7-8467-87167a5f7bbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8ef996a-4f93-4f52-9226-ba82179959cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d69c81a3-7d15-40b7-8467-87167a5f7bbf",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "03c04eca-c3c3-4477-b047-c5b15b00dc42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "a32f1d09-cf3a-4126-83b9-70bfb131979e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03c04eca-c3c3-4477-b047-c5b15b00dc42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b69ad06-20ef-42b2-90aa-f785213d4ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c04eca-c3c3-4477-b047-c5b15b00dc42",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "d14c6a3c-f6ac-4c69-b68e-f78ab152a5b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "702eb68f-4183-4636-820a-6103c9520856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d14c6a3c-f6ac-4c69-b68e-f78ab152a5b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b7ae9d-14f0-442e-a9cb-666becdfe5d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d14c6a3c-f6ac-4c69-b68e-f78ab152a5b5",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "49225819-9151-4eaf-aeb5-a45bfe242d2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "64fd269a-a9a7-4fba-8382-13473a1fe5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49225819-9151-4eaf-aeb5-a45bfe242d2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea3503e4-659a-4916-b057-3360372ffad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49225819-9151-4eaf-aeb5-a45bfe242d2c",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "421be9ee-8d04-4c6a-91a1-1ecc777187c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "57d642f2-9edc-4abd-8d9c-f8ac513a3c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "421be9ee-8d04-4c6a-91a1-1ecc777187c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34cfc93c-7474-479b-b2f0-0e3883dc3856",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "421be9ee-8d04-4c6a-91a1-1ecc777187c1",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "398387ee-bc63-4fbd-b612-b454f315890c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "eb087d01-18f2-4c04-9d06-7503f7c2508e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "398387ee-bc63-4fbd-b612-b454f315890c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1288b5ee-3cd4-4fe7-996e-4e2e294d508c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "398387ee-bc63-4fbd-b612-b454f315890c",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        },
        {
            "id": "60b5ad66-45a3-414d-ad06-7cc93a1589b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "compositeImage": {
                "id": "c6bc9776-0bcd-4bba-b49a-fb770f67ce5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b5ad66-45a3-414d-ad06-7cc93a1589b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f28465d2-71d1-45f1-9901-d2b16ddc94ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b5ad66-45a3-414d-ad06-7cc93a1589b8",
                    "LayerId": "82c03adb-ae32-4237-8971-137dcedb94a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 57,
    "layers": [
        {
            "id": "82c03adb-ae32-4237-8971-137dcedb94a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9797450f-fb16-46f9-bdcc-4e0fe76fa5c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": -10,
    "yorig": -23
}