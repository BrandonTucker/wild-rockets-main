{
    "id": "9cecbeba-0da7-4c41-91a2-848b29d1c2f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship14explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 34,
    "bbox_right": 58,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d35f6df2-050a-481a-bfc8-4a1a89457577",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cecbeba-0da7-4c41-91a2-848b29d1c2f4",
            "compositeImage": {
                "id": "2296e08c-60d0-4bd7-85e7-d34c9f1fadc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d35f6df2-050a-481a-bfc8-4a1a89457577",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcfda3c0-ac93-4400-a3f5-4ab4823aaaa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d35f6df2-050a-481a-bfc8-4a1a89457577",
                    "LayerId": "3b1d9593-704f-4dee-b56e-9a6958426381"
                }
            ]
        },
        {
            "id": "16c4e2fc-7880-48d7-b1a5-ddbeee28a476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cecbeba-0da7-4c41-91a2-848b29d1c2f4",
            "compositeImage": {
                "id": "53ddf180-f40e-4276-a5d9-2c3d3d524f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16c4e2fc-7880-48d7-b1a5-ddbeee28a476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35774058-34fa-41e6-8460-2b86b55affe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16c4e2fc-7880-48d7-b1a5-ddbeee28a476",
                    "LayerId": "3b1d9593-704f-4dee-b56e-9a6958426381"
                }
            ]
        },
        {
            "id": "9e94e51f-b8a6-41fd-9114-18ad12eeaba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cecbeba-0da7-4c41-91a2-848b29d1c2f4",
            "compositeImage": {
                "id": "90578bac-1acc-439c-a11f-7873bed94131",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e94e51f-b8a6-41fd-9114-18ad12eeaba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74ad919b-e914-46d6-9fa1-5ce93d5a301e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e94e51f-b8a6-41fd-9114-18ad12eeaba0",
                    "LayerId": "3b1d9593-704f-4dee-b56e-9a6958426381"
                }
            ]
        },
        {
            "id": "a65c6042-019a-4485-b43a-b6ff7e4fb0f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cecbeba-0da7-4c41-91a2-848b29d1c2f4",
            "compositeImage": {
                "id": "20a149da-bdb8-4ff4-b748-ce47b0582a7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a65c6042-019a-4485-b43a-b6ff7e4fb0f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b021605-ca78-4572-bf4b-839137591de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a65c6042-019a-4485-b43a-b6ff7e4fb0f6",
                    "LayerId": "3b1d9593-704f-4dee-b56e-9a6958426381"
                }
            ]
        },
        {
            "id": "afcd7730-0b9c-4a9d-befd-6493429d6b07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cecbeba-0da7-4c41-91a2-848b29d1c2f4",
            "compositeImage": {
                "id": "55685776-22df-474e-b7a6-0d1e3048e6b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afcd7730-0b9c-4a9d-befd-6493429d6b07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c49af6f3-cbd9-46e5-815b-a635b6c21595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afcd7730-0b9c-4a9d-befd-6493429d6b07",
                    "LayerId": "3b1d9593-704f-4dee-b56e-9a6958426381"
                }
            ]
        },
        {
            "id": "0c467c6f-8706-45ce-b2ad-1303e25934b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cecbeba-0da7-4c41-91a2-848b29d1c2f4",
            "compositeImage": {
                "id": "726e609b-51c2-4ad4-a692-afbdf5783587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c467c6f-8706-45ce-b2ad-1303e25934b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5267c392-0907-4630-8248-5c129bba8141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c467c6f-8706-45ce-b2ad-1303e25934b6",
                    "LayerId": "3b1d9593-704f-4dee-b56e-9a6958426381"
                }
            ]
        },
        {
            "id": "6b2d7c84-df0c-4ac7-8f5a-1de541b7f476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cecbeba-0da7-4c41-91a2-848b29d1c2f4",
            "compositeImage": {
                "id": "bc092b46-ced5-48d8-a4e4-38f7173f9bab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b2d7c84-df0c-4ac7-8f5a-1de541b7f476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8da4d412-d609-408a-804f-c0726c70be9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b2d7c84-df0c-4ac7-8f5a-1de541b7f476",
                    "LayerId": "3b1d9593-704f-4dee-b56e-9a6958426381"
                }
            ]
        },
        {
            "id": "d6416a7a-c31c-4797-aa3c-7cf4a35d21c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cecbeba-0da7-4c41-91a2-848b29d1c2f4",
            "compositeImage": {
                "id": "ee188066-79cf-4d59-b9e1-fa538cc683f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6416a7a-c31c-4797-aa3c-7cf4a35d21c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a262b1a-63bd-4b25-b32c-76586912cfdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6416a7a-c31c-4797-aa3c-7cf4a35d21c3",
                    "LayerId": "3b1d9593-704f-4dee-b56e-9a6958426381"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 126,
    "layers": [
        {
            "id": "3b1d9593-704f-4dee-b56e-9a6958426381",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cecbeba-0da7-4c41-91a2-848b29d1c2f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 87,
    "xorig": 43,
    "yorig": 42
}