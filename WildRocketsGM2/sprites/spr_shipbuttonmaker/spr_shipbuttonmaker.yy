{
    "id": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shipbuttonmaker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a324062-aa6e-48bf-b3a1-1513d56d1de6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "a0d0d224-1f5c-49d5-9cb6-d2e9a0f17139",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a324062-aa6e-48bf-b3a1-1513d56d1de6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "082b1f51-ab5a-4c34-b47d-afe6eaceda7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a324062-aa6e-48bf-b3a1-1513d56d1de6",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "eefd1056-49fe-4400-84aa-2365913a9bf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "4405e9be-f1cf-4f57-b716-55215ae4e656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eefd1056-49fe-4400-84aa-2365913a9bf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37920927-7c7a-4440-8dd2-928cacf4e34a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eefd1056-49fe-4400-84aa-2365913a9bf5",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "90f17231-3bd9-4d48-884d-497858b5b207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "413ede72-d850-4a5b-8dcf-ad1cf7468f66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90f17231-3bd9-4d48-884d-497858b5b207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3167155-9f6d-418e-a7a2-b3172c6d6ef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90f17231-3bd9-4d48-884d-497858b5b207",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "6a8ce62a-dce9-4b0c-9641-2a95edf056a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "a57ba984-15f9-4092-8742-58593be28e12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a8ce62a-dce9-4b0c-9641-2a95edf056a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ecb1581-d6b1-4ec7-bf97-461b63f77fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a8ce62a-dce9-4b0c-9641-2a95edf056a0",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "6d8c10f8-dfaa-4bed-baf2-8b4d18764f6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "e56238b2-c7b9-44d6-be52-5676574374ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d8c10f8-dfaa-4bed-baf2-8b4d18764f6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a53f308d-69d0-45f7-9bd2-ea81452f7c5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d8c10f8-dfaa-4bed-baf2-8b4d18764f6d",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "66bf78da-0d4b-43b4-8752-6523239595d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "bc309f1c-5914-49eb-8ec7-ce5940f49001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66bf78da-0d4b-43b4-8752-6523239595d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aa0cb7f-1523-444e-907d-5c1a7bfda113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66bf78da-0d4b-43b4-8752-6523239595d3",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "a44cae58-e614-4fc2-a0ae-f5e06ead8c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "d74eac88-9543-4587-85bb-accfb31294f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a44cae58-e614-4fc2-a0ae-f5e06ead8c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe9e368-ff8e-410a-93f8-6191ed368e96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a44cae58-e614-4fc2-a0ae-f5e06ead8c2e",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "eda48166-d884-4c04-a5d1-85dee20007d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "afc4b0c2-cd03-40e0-a9ab-d7e60727b82d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda48166-d884-4c04-a5d1-85dee20007d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1035a9f6-eae8-4cf3-b289-ea6340d28fde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda48166-d884-4c04-a5d1-85dee20007d2",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "677f73d0-2c8d-4ff7-a23d-360cb570293e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "2ae31ae8-89b8-4383-b1e4-1075dbeb41b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "677f73d0-2c8d-4ff7-a23d-360cb570293e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5959a7b-a4ef-4283-9676-6d04332dfb1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "677f73d0-2c8d-4ff7-a23d-360cb570293e",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "3336b47f-7308-41d6-9933-2ee21dfda552",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "a22a317d-1606-425e-a0f8-6dc8e0d0f4a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3336b47f-7308-41d6-9933-2ee21dfda552",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56d9d80b-237c-4e12-9a9a-d791903895a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3336b47f-7308-41d6-9933-2ee21dfda552",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "2b42b9d8-d341-49a3-a7b7-e3c900067c8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "06008103-9964-4c3a-9cc2-99f2876c518e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b42b9d8-d341-49a3-a7b7-e3c900067c8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1e66ca7-d7c6-4580-8cf5-8633a4aaec6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b42b9d8-d341-49a3-a7b7-e3c900067c8f",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "c0ecb607-fd93-4225-b7a9-1817ff74cb56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "609af61d-63a8-41a2-8560-957d777320a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0ecb607-fd93-4225-b7a9-1817ff74cb56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c89ecbf1-2a7f-4a17-b2bd-b1e0c06e2b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0ecb607-fd93-4225-b7a9-1817ff74cb56",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "d811607a-ccb5-4c38-8e68-bbdf36cd2641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "1715c32b-e101-4f14-8da8-fc22cb341cdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d811607a-ccb5-4c38-8e68-bbdf36cd2641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcd74eb6-2598-4fed-892a-6c43d5b21c2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d811607a-ccb5-4c38-8e68-bbdf36cd2641",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "d2f00241-63c6-4e28-84ee-44a19b68e2f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "6ba46c05-bcff-485e-97dd-00bd6d934a3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2f00241-63c6-4e28-84ee-44a19b68e2f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12baa541-a361-446b-87ce-dfd7cc2c3838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2f00241-63c6-4e28-84ee-44a19b68e2f7",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "485a8a4f-5971-420e-a7db-217c5f1e4f9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "a163f306-df47-4b41-8762-491e432e698b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "485a8a4f-5971-420e-a7db-217c5f1e4f9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00cdd01a-dfbb-4ff5-871b-fe30b310ebdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "485a8a4f-5971-420e-a7db-217c5f1e4f9f",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "ff48a832-b157-42d6-80e1-89778bacdeb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "dc119650-f4a1-430d-9dcf-79a19cee4305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff48a832-b157-42d6-80e1-89778bacdeb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36dfc83b-e453-4bba-8ee4-b32d0be01f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff48a832-b157-42d6-80e1-89778bacdeb8",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "556baf93-2c26-48bf-8a01-b97755bc9cbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "dfd9309c-3cd0-4e47-940f-c6243e631797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "556baf93-2c26-48bf-8a01-b97755bc9cbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e286ebb0-2acf-4ead-84b6-0ad14b8d7067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "556baf93-2c26-48bf-8a01-b97755bc9cbc",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "f78ecf83-a643-4e67-9271-76d9410e77be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "3a8575e3-e3e2-44ba-99eb-313efb2a4f20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f78ecf83-a643-4e67-9271-76d9410e77be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e68df3ac-9491-49e0-b77f-075b384ef36e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f78ecf83-a643-4e67-9271-76d9410e77be",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "d5689925-6eac-4678-b9c9-720ae4381997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "ba88396a-32ef-428e-b56f-1a2c20e10fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5689925-6eac-4678-b9c9-720ae4381997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5518e99-bdab-49b2-8c7b-2e3099636aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5689925-6eac-4678-b9c9-720ae4381997",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        },
        {
            "id": "8020abeb-7e16-4fd5-8ddc-0fb807833f53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "compositeImage": {
                "id": "38f42140-63a7-4407-b671-255c73b5b206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8020abeb-7e16-4fd5-8ddc-0fb807833f53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba0acc8b-2eb9-4849-a50d-3543b1b01344",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8020abeb-7e16-4fd5-8ddc-0fb807833f53",
                    "LayerId": "02f2e42c-a4b0-4e05-a222-fda016db01f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "02f2e42c-a4b0-4e05-a222-fda016db01f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9956c159-35ae-4fe2-b884-4f5cf6e65285",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 50,
    "yorig": 48
}