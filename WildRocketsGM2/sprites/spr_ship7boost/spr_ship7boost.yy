{
    "id": "532c310b-0592-4cd0-b658-b22277d88da8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship7boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 27,
    "bbox_right": 52,
    "bbox_top": -8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6507e19d-1af7-4c76-bebf-4e00eecd017d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "532c310b-0592-4cd0-b658-b22277d88da8",
            "compositeImage": {
                "id": "61887139-a09e-40b9-8a79-77c1c286e46a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6507e19d-1af7-4c76-bebf-4e00eecd017d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fd979f6-6c76-42ea-960d-54c40db09fb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6507e19d-1af7-4c76-bebf-4e00eecd017d",
                    "LayerId": "6e67d4a0-9015-475c-967b-e1d39d83ee29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 79,
    "layers": [
        {
            "id": "6e67d4a0-9015-475c-967b-e1d39d83ee29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "532c310b-0592-4cd0-b658-b22277d88da8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 41,
    "yorig": 26
}