{
    "id": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_finnishlinedis",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03e9e1eb-ec6d-4ddb-b625-c65900a347c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "dd8b6744-5a3b-4e51-8a3a-23146ddff22b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e9e1eb-ec6d-4ddb-b625-c65900a347c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83a05ae6-ecf9-469a-adf8-02d01761d8ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e9e1eb-ec6d-4ddb-b625-c65900a347c4",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "37d82cd4-cf1a-41bd-87a2-1e6b02608396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "5b4804bc-7755-4764-8985-2ff1bdcf2a10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37d82cd4-cf1a-41bd-87a2-1e6b02608396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfcc5eed-9d09-45fe-87cd-028dfcc7f31a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37d82cd4-cf1a-41bd-87a2-1e6b02608396",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "9ab72a6c-168c-42f0-98a7-05b0396c98f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "3415d1cd-f89b-4edc-bb53-e27f81e1be30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ab72a6c-168c-42f0-98a7-05b0396c98f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ecaad82-6272-4630-82a1-faa61b4b4d6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ab72a6c-168c-42f0-98a7-05b0396c98f4",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "0441a8a0-557b-4b3c-9c8e-c6f43e3296b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "bc9a67e6-0582-4fcf-8ee2-4f8a984845f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0441a8a0-557b-4b3c-9c8e-c6f43e3296b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "585831db-4b3d-48d6-babc-d6dfaac6f51c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0441a8a0-557b-4b3c-9c8e-c6f43e3296b6",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "3611b380-be19-46fc-8b65-0aa59b1684ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "5c80e030-de9b-4117-9a99-ecf7a4647e75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3611b380-be19-46fc-8b65-0aa59b1684ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e303bb1a-107d-4c1e-9b6e-6aac2f7bb57a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3611b380-be19-46fc-8b65-0aa59b1684ee",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "a321ad90-6562-431c-8cf3-9a773aba1d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "51757f8d-7eed-4453-a642-f2bf1274d93f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a321ad90-6562-431c-8cf3-9a773aba1d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b88b9d2-616f-49db-8d01-f941b772bf86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a321ad90-6562-431c-8cf3-9a773aba1d1a",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "6ae5db1c-26e5-42e2-a3e5-b5bd12c33c0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "76c661ac-cd07-4481-8698-dfe69ffb423c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ae5db1c-26e5-42e2-a3e5-b5bd12c33c0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe25f51-3cb5-4e67-a659-11e776180648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ae5db1c-26e5-42e2-a3e5-b5bd12c33c0e",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "4936cb66-f9a3-455f-8350-ca4f56dcdc55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "4efba200-da8d-4086-b849-00394f815c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4936cb66-f9a3-455f-8350-ca4f56dcdc55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff455531-44ad-4345-b07a-8317d7848183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4936cb66-f9a3-455f-8350-ca4f56dcdc55",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "88a88785-228d-489f-9122-4d37fc6b994b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "64e9dfdd-b2a3-417d-8f7f-358032a85b20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88a88785-228d-489f-9122-4d37fc6b994b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd1b1a11-9237-4cae-b36b-0169df270ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88a88785-228d-489f-9122-4d37fc6b994b",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "d602a380-d795-4db9-b166-5a4fbe8eea17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "60c07b8c-79ee-492c-8541-af8b033be542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d602a380-d795-4db9-b166-5a4fbe8eea17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "827e110a-6451-4db8-9d36-67e085ad1cff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d602a380-d795-4db9-b166-5a4fbe8eea17",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "d08badaa-f82a-484f-8613-76593a439a72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "768730a3-07cf-463b-a3a1-1aeecb282bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d08badaa-f82a-484f-8613-76593a439a72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5c97c2b-e706-408a-b35a-6ea4dead6d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d08badaa-f82a-484f-8613-76593a439a72",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "81000005-db0c-474b-97d9-b2ff10d0706c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "4c1cfca6-fa5c-4dbd-8646-4190ef3e0b92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81000005-db0c-474b-97d9-b2ff10d0706c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "472d9908-f987-4488-8aa4-4e3e96deef10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81000005-db0c-474b-97d9-b2ff10d0706c",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "8e74eca1-6455-4ff6-89e7-dee53c8268a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "26f86dfb-3133-4fee-8c02-90b759af54c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e74eca1-6455-4ff6-89e7-dee53c8268a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "853a7bee-035c-4fd0-95b7-18fa637bcf8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e74eca1-6455-4ff6-89e7-dee53c8268a9",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "f965b8f0-1150-4d51-9d6a-37f61e779f3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "9e077bd2-0cb8-445c-ae80-38b7886ed35e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f965b8f0-1150-4d51-9d6a-37f61e779f3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b6ec99-1b93-4727-9573-7a080750041a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f965b8f0-1150-4d51-9d6a-37f61e779f3f",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        },
        {
            "id": "a44f76ae-08d5-46ac-9138-4c255ccd9e87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "compositeImage": {
                "id": "b7b2b5f2-902f-47d3-9667-68b98957ff25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a44f76ae-08d5-46ac-9138-4c255ccd9e87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445809cd-b03d-437b-8c38-6e6ecf4a17b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a44f76ae-08d5-46ac-9138-4c255ccd9e87",
                    "LayerId": "7119f7c4-d206-469c-8a80-886c98451537"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7119f7c4-d206-469c-8a80-886c98451537",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "505b97fd-a7d0-48b7-bbfe-a993a74e9abd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}