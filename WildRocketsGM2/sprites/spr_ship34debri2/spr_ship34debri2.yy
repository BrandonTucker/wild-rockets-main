{
    "id": "be769c75-d8a2-4b7a-87c4-c594dca12767",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship34debri2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 0,
    "bbox_right": 88,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a18d31e-9a9b-4691-9d14-c8325feac7f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "69aab783-78bf-477d-ade2-6ed4b5c20f97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a18d31e-9a9b-4691-9d14-c8325feac7f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9506dbf9-c1c7-4809-8018-b5470a856b1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a18d31e-9a9b-4691-9d14-c8325feac7f9",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "8cd6926d-ddc2-4b3d-8ba4-cac4815e65a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "72a684f4-9c36-41e7-bb49-119d814049be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd6926d-ddc2-4b3d-8ba4-cac4815e65a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7c30b76-172a-4b5f-af7e-c8d310c6fdea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd6926d-ddc2-4b3d-8ba4-cac4815e65a6",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "9dd82c00-4fc4-4b13-b385-d6c22dc32051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "95e13c8d-bec4-42fd-b88c-0b8762d84a3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dd82c00-4fc4-4b13-b385-d6c22dc32051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad8329c-69fc-4f42-b849-deafa951eb2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dd82c00-4fc4-4b13-b385-d6c22dc32051",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "a8a1ef93-3792-4de2-930a-bcbea09f7b2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "4e0ac9ce-e518-45f7-bd2b-050da18c09eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8a1ef93-3792-4de2-930a-bcbea09f7b2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c07851c-f7f5-4418-bae7-68b27319b6a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8a1ef93-3792-4de2-930a-bcbea09f7b2a",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "38915b4e-e18c-4417-8a2c-299eff941b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "937969ef-87d2-464f-b0e3-ba55b9fd449d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38915b4e-e18c-4417-8a2c-299eff941b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d605bdcd-42e0-4898-8ca7-ff908ac46da7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38915b4e-e18c-4417-8a2c-299eff941b30",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "ebab8fbd-2691-436b-a587-9f817f320231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "578d1d1b-cd92-4473-adc0-73e79bc7951c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebab8fbd-2691-436b-a587-9f817f320231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b04542-764c-48bd-a38f-367310ed3d24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebab8fbd-2691-436b-a587-9f817f320231",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "4ed2d063-eb73-4d99-b847-1651a324b73e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "7e08cf98-f8ed-4765-baf3-19230bee9ab5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ed2d063-eb73-4d99-b847-1651a324b73e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7127fdd7-8c8b-46cc-b2a7-2b389f5c6670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ed2d063-eb73-4d99-b847-1651a324b73e",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "e1e30f72-ab61-4ac8-ab30-644eeda0b100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "b0257760-b102-45bb-9676-fab8790cbd86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1e30f72-ab61-4ac8-ab30-644eeda0b100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f77073-a102-487a-9cfa-eae74c82ed37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1e30f72-ab61-4ac8-ab30-644eeda0b100",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "5195cf04-394d-4a0d-9608-a00f35241380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "b4e66b4c-e2ad-45c8-a064-6e4f35a8f4dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5195cf04-394d-4a0d-9608-a00f35241380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4673ae3f-2199-4ae3-8d88-888b05b588e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5195cf04-394d-4a0d-9608-a00f35241380",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "ad708bdc-d2c3-4eeb-b3a2-ab71e50e6548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "1b1689eb-bf94-48f7-b2e2-f926aef8663e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad708bdc-d2c3-4eeb-b3a2-ab71e50e6548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "536a18f0-45b0-43d4-885e-ed57cbaf8442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad708bdc-d2c3-4eeb-b3a2-ab71e50e6548",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "5d2c1845-0460-4537-bcc1-bf25f57fc074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "95855b52-5126-457b-adf9-088ef87eaf71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d2c1845-0460-4537-bcc1-bf25f57fc074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1bdcc0-6b27-4508-b74a-eb0c76abf70b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d2c1845-0460-4537-bcc1-bf25f57fc074",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "4d15f0d1-74cd-4d61-93f7-f4178720db0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "cd27fa49-cbac-4ec6-a801-33043c260fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d15f0d1-74cd-4d61-93f7-f4178720db0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7634c075-ee59-4ace-a168-f4249879f8bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d15f0d1-74cd-4d61-93f7-f4178720db0e",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "6827f2b0-5d0a-46e7-8fd8-5b75d0f9294b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "9a86328b-56d3-4818-83b5-549612ae7186",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6827f2b0-5d0a-46e7-8fd8-5b75d0f9294b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c092a01-036c-4c49-beff-c930ed53e8c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6827f2b0-5d0a-46e7-8fd8-5b75d0f9294b",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "44fc1c58-1cc2-471c-940f-129f2be6e672",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "9c8695cf-8b49-49e8-875a-3db36c56ea4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44fc1c58-1cc2-471c-940f-129f2be6e672",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "808ad932-c287-427a-9284-6e6b475439ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44fc1c58-1cc2-471c-940f-129f2be6e672",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        },
        {
            "id": "7b4ae178-4933-4999-af8a-692b018ec03a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "compositeImage": {
                "id": "7669d524-54c2-4618-ba03-44ec1f53c106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b4ae178-4933-4999-af8a-692b018ec03a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e73d1622-0147-48ba-9672-5bfaf082d1bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b4ae178-4933-4999-af8a-692b018ec03a",
                    "LayerId": "b13a3c98-193a-4717-83f2-83dbff0d4268"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "b13a3c98-193a-4717-83f2-83dbff0d4268",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be769c75-d8a2-4b7a-87c4-c594dca12767",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 89,
    "xorig": 41,
    "yorig": 37
}