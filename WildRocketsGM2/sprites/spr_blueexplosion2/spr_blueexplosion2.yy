{
    "id": "1547e11e-dae4-47b2-ab52-e3400ace6404",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blueexplosion2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 97,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8dbe563a-5642-445e-8dd1-1b56d1073f91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1547e11e-dae4-47b2-ab52-e3400ace6404",
            "compositeImage": {
                "id": "c5883442-1f9c-448b-a009-8fb7a0bd0094",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dbe563a-5642-445e-8dd1-1b56d1073f91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e914db45-c5c0-44a8-86d6-9168b8ab6cd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dbe563a-5642-445e-8dd1-1b56d1073f91",
                    "LayerId": "e41f4591-bb76-43a5-98b7-65c3c87df87c"
                }
            ]
        },
        {
            "id": "5d7088e7-921b-452d-91c0-be1015fd1464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1547e11e-dae4-47b2-ab52-e3400ace6404",
            "compositeImage": {
                "id": "38563fd5-d9a1-471d-82ec-0242768a5fad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d7088e7-921b-452d-91c0-be1015fd1464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a263ab9-f259-4b28-b5ce-4a0c2f179cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d7088e7-921b-452d-91c0-be1015fd1464",
                    "LayerId": "e41f4591-bb76-43a5-98b7-65c3c87df87c"
                }
            ]
        },
        {
            "id": "be4687f8-0360-49b1-b1b3-43d9bfb48bc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1547e11e-dae4-47b2-ab52-e3400ace6404",
            "compositeImage": {
                "id": "89b11247-cb84-4050-bf14-2ce7cd08db78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be4687f8-0360-49b1-b1b3-43d9bfb48bc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1952dd5-9189-436e-bf51-aad9eff05f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4687f8-0360-49b1-b1b3-43d9bfb48bc9",
                    "LayerId": "e41f4591-bb76-43a5-98b7-65c3c87df87c"
                }
            ]
        },
        {
            "id": "968f7c8a-2782-4c87-bfd2-738178ff0874",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1547e11e-dae4-47b2-ab52-e3400ace6404",
            "compositeImage": {
                "id": "961fd2a2-69ab-4a8c-a759-830ea6c5f23f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "968f7c8a-2782-4c87-bfd2-738178ff0874",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d70e7a3b-9f62-4b88-a592-ba0e9318f18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "968f7c8a-2782-4c87-bfd2-738178ff0874",
                    "LayerId": "e41f4591-bb76-43a5-98b7-65c3c87df87c"
                }
            ]
        },
        {
            "id": "d85422a6-7f12-4eb8-a4cf-204a6feb5aef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1547e11e-dae4-47b2-ab52-e3400ace6404",
            "compositeImage": {
                "id": "6ba5ca8b-a3e2-488c-964c-9285af1d6647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d85422a6-7f12-4eb8-a4cf-204a6feb5aef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24565dcc-3e30-4663-8385-42b49d4a8752",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d85422a6-7f12-4eb8-a4cf-204a6feb5aef",
                    "LayerId": "e41f4591-bb76-43a5-98b7-65c3c87df87c"
                }
            ]
        },
        {
            "id": "640a589a-ac23-42e7-b166-0fa6267a0609",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1547e11e-dae4-47b2-ab52-e3400ace6404",
            "compositeImage": {
                "id": "ff36e34b-afea-4ad0-82e2-eef5bc73d1d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "640a589a-ac23-42e7-b166-0fa6267a0609",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "823d591b-290d-47af-9dd7-d50010cb23c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "640a589a-ac23-42e7-b166-0fa6267a0609",
                    "LayerId": "e41f4591-bb76-43a5-98b7-65c3c87df87c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "e41f4591-bb76-43a5-98b7-65c3c87df87c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1547e11e-dae4-47b2-ab52-e3400ace6404",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}