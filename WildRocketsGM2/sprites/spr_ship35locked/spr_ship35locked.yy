{
    "id": "0f4b8b02-1df3-4214-8491-088923296577",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship35locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86b83958-9052-480a-bc8d-2c0a0a75c9f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f4b8b02-1df3-4214-8491-088923296577",
            "compositeImage": {
                "id": "886b37a2-a622-4d58-ba52-40e81727f4fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86b83958-9052-480a-bc8d-2c0a0a75c9f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa6cedb-5fb6-43b8-8978-8e30ec6effde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86b83958-9052-480a-bc8d-2c0a0a75c9f2",
                    "LayerId": "a4396692-b171-4cee-a487-703f09233ff1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "a4396692-b171-4cee-a487-703f09233ff1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f4b8b02-1df3-4214-8491-088923296577",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}