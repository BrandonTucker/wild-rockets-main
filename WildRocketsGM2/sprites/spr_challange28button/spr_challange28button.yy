{
    "id": "6ced8832-de65-458f-b099-93dae60a627d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange28button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d692ba9c-2811-41ea-b093-ffa73c4b0554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ced8832-de65-458f-b099-93dae60a627d",
            "compositeImage": {
                "id": "1645ddad-fee0-4ade-8601-282a70263380",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d692ba9c-2811-41ea-b093-ffa73c4b0554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e5c3903-d9ed-425b-9e0f-5600cbb63eb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d692ba9c-2811-41ea-b093-ffa73c4b0554",
                    "LayerId": "0c5d12d4-9e4a-44cf-b756-a4df71795996"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "0c5d12d4-9e4a-44cf-b756-a4df71795996",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ced8832-de65-458f-b099-93dae60a627d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}