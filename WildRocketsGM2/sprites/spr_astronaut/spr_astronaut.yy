{
    "id": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_astronaut",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "471636af-d691-4755-908f-61bccbd74aa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "f1ab4243-8b55-488c-b0ff-cfd1ff7e5852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "471636af-d691-4755-908f-61bccbd74aa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db5b600e-7acc-4fd7-ad4b-ca0bc6d15157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "471636af-d691-4755-908f-61bccbd74aa4",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "74b88b9c-b01a-4823-a533-25255edf3867",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "0eab805d-b7ba-4095-8993-f96c814744eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74b88b9c-b01a-4823-a533-25255edf3867",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8220ea80-e0c4-44b3-9279-2925f6be37d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74b88b9c-b01a-4823-a533-25255edf3867",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "e255d1b1-01c4-4b22-962d-0b1d2edc26ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "a1b861f5-fcb6-4d61-9ece-8c90bdb8d615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e255d1b1-01c4-4b22-962d-0b1d2edc26ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea0d0cc5-5915-4115-846c-4553599472be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e255d1b1-01c4-4b22-962d-0b1d2edc26ec",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "5dce2e19-5e6e-4cc8-881b-9123035f178f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "d8241152-af50-458b-b303-254dd646b69b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dce2e19-5e6e-4cc8-881b-9123035f178f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10b15f93-e743-47c2-a721-d3b7fb9c48b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dce2e19-5e6e-4cc8-881b-9123035f178f",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "11590fe6-922c-4e26-85c7-6f51c28a16b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "a1c2af1a-b31d-48b4-8ab2-a10d9bdf9753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11590fe6-922c-4e26-85c7-6f51c28a16b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57af236c-c906-4d98-8ce0-db4f75fd053f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11590fe6-922c-4e26-85c7-6f51c28a16b2",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "05a25ce3-673f-403e-af01-38cbd8080d38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "3ef3143a-877d-4aa8-8725-02ce19ebe559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a25ce3-673f-403e-af01-38cbd8080d38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f158109-0d17-4ec6-877b-637952a157dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a25ce3-673f-403e-af01-38cbd8080d38",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "c346c2a9-02ae-4740-b439-affddb42149d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "425d9885-534c-4003-93d0-f477e8cd12ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c346c2a9-02ae-4740-b439-affddb42149d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b597644d-8ebe-4b89-82bb-c60cb825e615",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c346c2a9-02ae-4740-b439-affddb42149d",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "d97dbd3a-2d95-4ea3-b8d8-0b93b820a590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "5898c61f-e9db-42b2-90cc-bf9959e7be9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d97dbd3a-2d95-4ea3-b8d8-0b93b820a590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86ac5d6e-5f1d-4062-91a4-8f142a3722d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d97dbd3a-2d95-4ea3-b8d8-0b93b820a590",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "2743f60d-3ca6-4aa1-8644-09087fb59f2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "e80eecfc-1bf9-4d10-9051-82281ba10296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2743f60d-3ca6-4aa1-8644-09087fb59f2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69ed3948-b303-4ea4-923d-56344f1d48a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2743f60d-3ca6-4aa1-8644-09087fb59f2d",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "c87bac37-95a5-4eca-8a58-d949a991a88f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "b515a1ee-85b3-420a-8822-d877f8a79cfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c87bac37-95a5-4eca-8a58-d949a991a88f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0561a63f-9841-45bc-ba91-a6cee2e62a15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c87bac37-95a5-4eca-8a58-d949a991a88f",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "d851854f-1fa1-4a6d-a0ad-284dd80b9162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "e14845a5-19b5-4898-976c-86139806e431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d851854f-1fa1-4a6d-a0ad-284dd80b9162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "209d403c-cd62-48a6-8793-360137f8004e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d851854f-1fa1-4a6d-a0ad-284dd80b9162",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "523b5455-ec84-4db3-b75f-7d494fafeb44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "ec19ca86-2a1a-4eff-83cd-bfa06e724aac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "523b5455-ec84-4db3-b75f-7d494fafeb44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98cc344-c31f-4c06-9aa8-f109eb1a97bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "523b5455-ec84-4db3-b75f-7d494fafeb44",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "5de90115-c003-4b6f-a316-33591a7a9738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "86dbaa5f-a0f8-44a2-8a83-c8903e715b49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de90115-c003-4b6f-a316-33591a7a9738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b3f752-72ff-448a-b740-32d4ced203c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de90115-c003-4b6f-a316-33591a7a9738",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "0dbf43c1-53f9-496a-a7fc-f747eb3b5111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "cc73cbf8-0c2c-43a8-8efa-267c657b122c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dbf43c1-53f9-496a-a7fc-f747eb3b5111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff5610b5-6995-47b0-a629-b10223724f05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dbf43c1-53f9-496a-a7fc-f747eb3b5111",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        },
        {
            "id": "230b52e7-dfaa-43d1-bf4b-9aad24784396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "compositeImage": {
                "id": "499d8db5-d7f1-4a02-b80f-46fd317520da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "230b52e7-dfaa-43d1-bf4b-9aad24784396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0de6491-6cb7-434c-87f9-83d09e35e648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "230b52e7-dfaa-43d1-bf4b-9aad24784396",
                    "LayerId": "2073bf93-7175-43e4-9647-573dba638ddd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "2073bf93-7175-43e4-9647-573dba638ddd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddd25a12-4f95-4ba7-81cb-645a181eab76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}