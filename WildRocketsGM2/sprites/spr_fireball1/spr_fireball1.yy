{
    "id": "1bad23a7-f376-4890-83ed-c4b35a34036b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireball1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 44,
    "bbox_right": 55,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b28ec56b-08c4-4663-8caf-db596711d83f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bad23a7-f376-4890-83ed-c4b35a34036b",
            "compositeImage": {
                "id": "3d8701bf-27c7-4744-a9a5-7214c6aeae00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b28ec56b-08c4-4663-8caf-db596711d83f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ebf802-de80-446a-b073-8dc3ea2d21f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b28ec56b-08c4-4663-8caf-db596711d83f",
                    "LayerId": "e6c20879-b876-47bc-b774-36766b143f2a"
                }
            ]
        },
        {
            "id": "bbd8b8d1-804a-489c-bf8e-6c28513313d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bad23a7-f376-4890-83ed-c4b35a34036b",
            "compositeImage": {
                "id": "99bbc520-6dde-4729-ab3f-67d0fcb3a346",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd8b8d1-804a-489c-bf8e-6c28513313d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "848957d5-6ac2-4917-b710-e16a174a00de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd8b8d1-804a-489c-bf8e-6c28513313d6",
                    "LayerId": "e6c20879-b876-47bc-b774-36766b143f2a"
                }
            ]
        },
        {
            "id": "62af44b7-b594-421d-ae11-9a83bfbe2a9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bad23a7-f376-4890-83ed-c4b35a34036b",
            "compositeImage": {
                "id": "2d6c820a-9cb9-45b2-9f6c-cfbdb07ca972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62af44b7-b594-421d-ae11-9a83bfbe2a9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68f8c68c-d2d8-4e60-8d29-4bf66dd14c46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62af44b7-b594-421d-ae11-9a83bfbe2a9e",
                    "LayerId": "e6c20879-b876-47bc-b774-36766b143f2a"
                }
            ]
        },
        {
            "id": "c45aad81-21ec-42f6-bc64-50b561782ba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bad23a7-f376-4890-83ed-c4b35a34036b",
            "compositeImage": {
                "id": "0764de7d-88bf-49db-9f4c-3765b42dc3a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c45aad81-21ec-42f6-bc64-50b561782ba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051bc0d8-b9d5-4cd1-b5af-e6b4f18f6dc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c45aad81-21ec-42f6-bc64-50b561782ba2",
                    "LayerId": "e6c20879-b876-47bc-b774-36766b143f2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "e6c20879-b876-47bc-b774-36766b143f2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bad23a7-f376-4890-83ed-c4b35a34036b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 56
}