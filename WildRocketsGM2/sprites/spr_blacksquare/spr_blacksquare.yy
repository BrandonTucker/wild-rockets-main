{
    "id": "a3338c74-40d3-497e-9b1a-088d17a78374",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blacksquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 11,
    "bbox_right": 49,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4b4f07d-242c-4028-8b68-7e1aa8e43923",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "compositeImage": {
                "id": "502595c3-1d1c-460b-905b-3f8359b1beb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4b4f07d-242c-4028-8b68-7e1aa8e43923",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a291cbb9-ee6e-4700-8161-72ac030db7d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4b4f07d-242c-4028-8b68-7e1aa8e43923",
                    "LayerId": "fbb64b45-67a8-4c75-a47a-24f6f2109322"
                }
            ]
        },
        {
            "id": "d097b6c3-2a9a-41f7-9261-b39443dea35b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "compositeImage": {
                "id": "18d8724b-4073-4cf5-b976-844609fa06d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d097b6c3-2a9a-41f7-9261-b39443dea35b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f70d714-a08a-4bd9-8df3-6ca80fed5679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d097b6c3-2a9a-41f7-9261-b39443dea35b",
                    "LayerId": "fbb64b45-67a8-4c75-a47a-24f6f2109322"
                }
            ]
        },
        {
            "id": "378530fe-85c8-49ef-8f91-4eea22aba934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "compositeImage": {
                "id": "a855c34b-a2a8-49c2-935c-b9db3fd7a499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "378530fe-85c8-49ef-8f91-4eea22aba934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3bd106-d02e-488f-84cb-e1753caa5ee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "378530fe-85c8-49ef-8f91-4eea22aba934",
                    "LayerId": "fbb64b45-67a8-4c75-a47a-24f6f2109322"
                }
            ]
        },
        {
            "id": "0504d97f-e390-41a4-8489-ad6440f9b5a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "compositeImage": {
                "id": "772245cd-6a74-44e1-bf3d-9943a4e438c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0504d97f-e390-41a4-8489-ad6440f9b5a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "276155d1-f7fb-4073-8789-c34448f3d1b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0504d97f-e390-41a4-8489-ad6440f9b5a1",
                    "LayerId": "fbb64b45-67a8-4c75-a47a-24f6f2109322"
                }
            ]
        },
        {
            "id": "830bb31e-4142-4058-8ee6-db367694554d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "compositeImage": {
                "id": "85542666-c334-4091-b7b5-3b7692f50cf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "830bb31e-4142-4058-8ee6-db367694554d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f316290-57d0-49c0-8bed-e506fc8dbeda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "830bb31e-4142-4058-8ee6-db367694554d",
                    "LayerId": "fbb64b45-67a8-4c75-a47a-24f6f2109322"
                }
            ]
        },
        {
            "id": "b622269a-f0ee-44c7-bfd5-a7ac9afbe0f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "compositeImage": {
                "id": "57ea12d7-5741-407a-bf5b-63c790485189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b622269a-f0ee-44c7-bfd5-a7ac9afbe0f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73dd42b2-b420-4ebd-8374-360e4f074d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b622269a-f0ee-44c7-bfd5-a7ac9afbe0f9",
                    "LayerId": "fbb64b45-67a8-4c75-a47a-24f6f2109322"
                }
            ]
        },
        {
            "id": "821d3cfa-59c7-4843-b4a8-bce0164a73b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "compositeImage": {
                "id": "48315676-f3ed-4783-9fee-54f2f03a8676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "821d3cfa-59c7-4843-b4a8-bce0164a73b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e919a1ee-7070-47a7-a1c2-83acebeacfb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "821d3cfa-59c7-4843-b4a8-bce0164a73b6",
                    "LayerId": "fbb64b45-67a8-4c75-a47a-24f6f2109322"
                }
            ]
        },
        {
            "id": "4ad01e73-c175-4012-b44f-ea08cf013bbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "compositeImage": {
                "id": "c1614e29-064e-4a01-8f10-e459ed10e42b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ad01e73-c175-4012-b44f-ea08cf013bbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11489d32-6450-4445-9674-cc7a094dac87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ad01e73-c175-4012-b44f-ea08cf013bbe",
                    "LayerId": "fbb64b45-67a8-4c75-a47a-24f6f2109322"
                }
            ]
        },
        {
            "id": "adc671cc-9766-4e90-a188-c264d3b0fd27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "compositeImage": {
                "id": "4e920520-52de-4319-b508-a54bff51ab9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adc671cc-9766-4e90-a188-c264d3b0fd27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46923b41-bcd6-40fe-80af-173677733e42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adc671cc-9766-4e90-a188-c264d3b0fd27",
                    "LayerId": "fbb64b45-67a8-4c75-a47a-24f6f2109322"
                }
            ]
        },
        {
            "id": "f1b08e63-bdc1-4c9c-813f-b65d1f013e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "compositeImage": {
                "id": "fbbdae47-59e0-44c8-93eb-5bb899ac826c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1b08e63-bdc1-4c9c-813f-b65d1f013e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a766690-383c-4703-abd0-067e9ea1f6be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1b08e63-bdc1-4c9c-813f-b65d1f013e2b",
                    "LayerId": "fbb64b45-67a8-4c75-a47a-24f6f2109322"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fbb64b45-67a8-4c75-a47a-24f6f2109322",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3338c74-40d3-497e-9b1a-088d17a78374",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 21,
    "yorig": 20
}