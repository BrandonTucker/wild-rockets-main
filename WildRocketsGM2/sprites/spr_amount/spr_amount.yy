{
    "id": "2d607792-27b4-4f49-af97-b04356bd9867",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_amount",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 5,
    "bbox_right": 107,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f923c63b-ae7f-4c4b-846d-eb132b4407e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "0ab5114b-7210-48b8-af95-8e39df8d2392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f923c63b-ae7f-4c4b-846d-eb132b4407e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "396ec000-4ab1-4069-9ec2-aa7029574d00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f923c63b-ae7f-4c4b-846d-eb132b4407e7",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "79b37d8b-10dc-402e-aa99-0108b4f6f094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "62870b5f-bc30-4dda-b6f1-2269caddac28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79b37d8b-10dc-402e-aa99-0108b4f6f094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bea73e9-fa4e-422f-bddf-061011da7809",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79b37d8b-10dc-402e-aa99-0108b4f6f094",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "d9a0fc9e-f972-46ad-8b93-cceaf7683a83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "1d960603-25d4-47e6-a33e-63ac8337e9f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9a0fc9e-f972-46ad-8b93-cceaf7683a83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8279ee0-4ff7-4381-bc64-e5088d47dc88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9a0fc9e-f972-46ad-8b93-cceaf7683a83",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "030100f0-82a4-45f6-860f-d6aa191feb7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "469dd349-8a98-42c3-8f1e-a103d7a59446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "030100f0-82a4-45f6-860f-d6aa191feb7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c01e3007-00ff-4253-9c68-479bf168cc75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "030100f0-82a4-45f6-860f-d6aa191feb7f",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "8ba76321-b3d5-4172-9d5a-ae86c3079bbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "accd6f91-7303-40b3-92d2-8b1061c89af9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba76321-b3d5-4172-9d5a-ae86c3079bbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969706d2-4d69-4586-8b90-7cbc4307f5dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba76321-b3d5-4172-9d5a-ae86c3079bbf",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "85e7994b-13de-4024-8541-1e284ece53db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "191c16b8-928c-46de-b03e-a460f039d709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85e7994b-13de-4024-8541-1e284ece53db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80ec7376-5cd9-4b18-a08b-b3e45e1d949a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85e7994b-13de-4024-8541-1e284ece53db",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "61a8a36c-aa46-420c-b87e-1a8ed203611a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "4a8c0183-6055-4f20-83e6-8873c5e75de6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61a8a36c-aa46-420c-b87e-1a8ed203611a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b091df6-0de8-4e8c-aae8-bf62886b2b8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61a8a36c-aa46-420c-b87e-1a8ed203611a",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "15bfa44f-456f-4e5f-bf0a-31301e12441d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "f993f79f-becc-4178-a0c9-187a3a26eb9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15bfa44f-456f-4e5f-bf0a-31301e12441d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01634192-4fe5-4be0-b392-ad6b9b2468c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15bfa44f-456f-4e5f-bf0a-31301e12441d",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "c1a5b310-9a8e-4c0b-b2b0-1c1a9712c76a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "1e24553b-fa9b-4af3-a855-6b39f6baf6c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1a5b310-9a8e-4c0b-b2b0-1c1a9712c76a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b011c880-d622-4c02-be95-39bb80f5c47f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1a5b310-9a8e-4c0b-b2b0-1c1a9712c76a",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "6ea0ec04-771e-4509-b48c-fac74ed87179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "7fdbb1eb-dbd8-435d-b944-59b591f04698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ea0ec04-771e-4509-b48c-fac74ed87179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6278b333-4526-4902-85f0-0415a96cab00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ea0ec04-771e-4509-b48c-fac74ed87179",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "c1e65a63-416a-409b-8402-2fd76f7da36a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "1aacb700-74a6-4d66-9cb6-65802b83c645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1e65a63-416a-409b-8402-2fd76f7da36a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a013f1a2-3192-41f8-8892-d3c93ed60fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1e65a63-416a-409b-8402-2fd76f7da36a",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "84843627-95c4-4e61-afae-979662d40f30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "6b643d95-06d0-48dd-839f-f4ebbb5b5ce6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84843627-95c4-4e61-afae-979662d40f30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6dfa35c-d1b5-4513-a8c4-15f02e395008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84843627-95c4-4e61-afae-979662d40f30",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        },
        {
            "id": "5a952ab8-efa1-490d-8473-970aac435c5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "compositeImage": {
                "id": "9adfc546-1c57-4955-9961-3e5114c18484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a952ab8-efa1-490d-8473-970aac435c5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d298555f-fe60-4c71-b221-019deef42359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a952ab8-efa1-490d-8473-970aac435c5c",
                    "LayerId": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "5fa6f53a-7e6e-45af-8e58-9ca7832e023c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d607792-27b4-4f49-af97-b04356bd9867",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 113,
    "xorig": 58,
    "yorig": 40
}