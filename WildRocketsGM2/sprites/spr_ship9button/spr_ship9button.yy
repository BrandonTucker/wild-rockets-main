{
    "id": "773ae2ec-391e-4a4b-8e5e-7b09052e0f98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship9button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 12,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33696616-d02b-4da7-b4af-c39f037c3d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "773ae2ec-391e-4a4b-8e5e-7b09052e0f98",
            "compositeImage": {
                "id": "4c0bd174-3629-4b65-8464-95ba4e111488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33696616-d02b-4da7-b4af-c39f037c3d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a8bea1a-3c0e-4866-ad9b-05691625e981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33696616-d02b-4da7-b4af-c39f037c3d31",
                    "LayerId": "bfa1f026-2c6e-4624-9933-fce5023ad9f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "bfa1f026-2c6e-4624-9933-fce5023ad9f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "773ae2ec-391e-4a4b-8e5e-7b09052e0f98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}