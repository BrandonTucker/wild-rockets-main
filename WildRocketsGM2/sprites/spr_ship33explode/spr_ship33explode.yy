{
    "id": "1af91a87-1a7a-4dfd-8dac-a72abccf65d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship33explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f8a2a04-9be6-4b68-a2cc-75185dad26df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1af91a87-1a7a-4dfd-8dac-a72abccf65d5",
            "compositeImage": {
                "id": "32915fee-7993-4fb5-a641-9a7db01bcd21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f8a2a04-9be6-4b68-a2cc-75185dad26df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6f8c647-9382-4d6a-8178-127ba61e35f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f8a2a04-9be6-4b68-a2cc-75185dad26df",
                    "LayerId": "662c620c-94c8-4902-b651-10d0601a996b"
                }
            ]
        },
        {
            "id": "7fa50be2-41ea-4514-b6df-bc1f1fa02dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1af91a87-1a7a-4dfd-8dac-a72abccf65d5",
            "compositeImage": {
                "id": "7e0dc235-0dac-44ad-8a5d-07afb7981ae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa50be2-41ea-4514-b6df-bc1f1fa02dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c5873e-bf8b-4171-8679-67ad275781fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa50be2-41ea-4514-b6df-bc1f1fa02dc2",
                    "LayerId": "662c620c-94c8-4902-b651-10d0601a996b"
                }
            ]
        },
        {
            "id": "14c63e24-9d41-44da-8cfc-5250d0b42a2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1af91a87-1a7a-4dfd-8dac-a72abccf65d5",
            "compositeImage": {
                "id": "f6e4ca63-bd88-424f-b8ee-7060b9b72849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14c63e24-9d41-44da-8cfc-5250d0b42a2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdaf084f-ea6f-4de9-bdb8-fc117ce85e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14c63e24-9d41-44da-8cfc-5250d0b42a2f",
                    "LayerId": "662c620c-94c8-4902-b651-10d0601a996b"
                }
            ]
        },
        {
            "id": "7d679bac-18de-4c53-9df3-65757fbaa14a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1af91a87-1a7a-4dfd-8dac-a72abccf65d5",
            "compositeImage": {
                "id": "a18ff0ca-25fa-41df-a7bd-d8306ec5d8cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d679bac-18de-4c53-9df3-65757fbaa14a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "944ceb40-bd09-42da-b62b-d32c1a323274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d679bac-18de-4c53-9df3-65757fbaa14a",
                    "LayerId": "662c620c-94c8-4902-b651-10d0601a996b"
                }
            ]
        },
        {
            "id": "70b4261d-079e-417c-a9f0-dfab821128e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1af91a87-1a7a-4dfd-8dac-a72abccf65d5",
            "compositeImage": {
                "id": "b31232ed-bbc1-4b9f-b89c-cd9a59586623",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70b4261d-079e-417c-a9f0-dfab821128e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f00083d-ee30-4823-95fe-4904cc3bd9f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b4261d-079e-417c-a9f0-dfab821128e1",
                    "LayerId": "662c620c-94c8-4902-b651-10d0601a996b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 207,
    "layers": [
        {
            "id": "662c620c-94c8-4902-b651-10d0601a996b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1af91a87-1a7a-4dfd-8dac-a72abccf65d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 67,
    "yorig": 99
}