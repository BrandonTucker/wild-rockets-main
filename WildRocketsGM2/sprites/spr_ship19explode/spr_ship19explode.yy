{
    "id": "d783f2bb-784d-4583-9ddb-cee0bf4f1879",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship19explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 28,
    "bbox_right": 60,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8a04ff2-b651-4110-8a7c-0044086df9ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d783f2bb-784d-4583-9ddb-cee0bf4f1879",
            "compositeImage": {
                "id": "5ac30217-6c28-41ff-aa06-153b7eb32457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8a04ff2-b651-4110-8a7c-0044086df9ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "660e457f-f81b-4629-8697-d830a74a3885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8a04ff2-b651-4110-8a7c-0044086df9ef",
                    "LayerId": "aac6cea7-737d-43d2-92cf-5a7b8171b09a"
                }
            ]
        },
        {
            "id": "6f6781e9-4137-46bf-b39c-99283c273c3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d783f2bb-784d-4583-9ddb-cee0bf4f1879",
            "compositeImage": {
                "id": "f0b586ab-830b-4881-89b6-0029323e0eba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f6781e9-4137-46bf-b39c-99283c273c3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4955c47-0c2d-44c0-8b6c-1a409cd402a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f6781e9-4137-46bf-b39c-99283c273c3e",
                    "LayerId": "aac6cea7-737d-43d2-92cf-5a7b8171b09a"
                }
            ]
        },
        {
            "id": "f901d379-45d0-415b-bc1d-604f54854d99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d783f2bb-784d-4583-9ddb-cee0bf4f1879",
            "compositeImage": {
                "id": "849f8864-a191-48f9-be41-4523e6ad9a82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f901d379-45d0-415b-bc1d-604f54854d99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc351311-b01c-43ef-baad-42ccbba254d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f901d379-45d0-415b-bc1d-604f54854d99",
                    "LayerId": "aac6cea7-737d-43d2-92cf-5a7b8171b09a"
                }
            ]
        },
        {
            "id": "d8288d03-ddc8-465c-b51a-3869706995a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d783f2bb-784d-4583-9ddb-cee0bf4f1879",
            "compositeImage": {
                "id": "1c173422-20d4-4727-8feb-631ec7781ed2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8288d03-ddc8-465c-b51a-3869706995a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a2ecb3-cff8-4adc-b86b-d0c074036125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8288d03-ddc8-465c-b51a-3869706995a5",
                    "LayerId": "aac6cea7-737d-43d2-92cf-5a7b8171b09a"
                }
            ]
        },
        {
            "id": "e4ded468-02ec-48b9-aa9e-497e1b404449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d783f2bb-784d-4583-9ddb-cee0bf4f1879",
            "compositeImage": {
                "id": "edf1c588-92f7-4009-9cc4-07178604d066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4ded468-02ec-48b9-aa9e-497e1b404449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ec5c811-1c9e-4a11-85a3-a880af30710d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4ded468-02ec-48b9-aa9e-497e1b404449",
                    "LayerId": "aac6cea7-737d-43d2-92cf-5a7b8171b09a"
                }
            ]
        },
        {
            "id": "59f5090f-0104-472f-8751-3f130ad793aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d783f2bb-784d-4583-9ddb-cee0bf4f1879",
            "compositeImage": {
                "id": "8c4085da-05f1-4509-8466-9ccb14f90774",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59f5090f-0104-472f-8751-3f130ad793aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c600326-c410-4ebd-8b0d-748d4b4719a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f5090f-0104-472f-8751-3f130ad793aa",
                    "LayerId": "aac6cea7-737d-43d2-92cf-5a7b8171b09a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "aac6cea7-737d-43d2-92cf-5a7b8171b09a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d783f2bb-784d-4583-9ddb-cee0bf4f1879",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 47
}