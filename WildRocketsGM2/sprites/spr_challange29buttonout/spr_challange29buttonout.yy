{
    "id": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange29buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d8ee48c-eb10-45ae-8e63-c3ea9bd4d750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "compositeImage": {
                "id": "16e93776-930b-4510-aa15-18d1c29edab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d8ee48c-eb10-45ae-8e63-c3ea9bd4d750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31461feb-649f-4fa6-bf47-563177eb51a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d8ee48c-eb10-45ae-8e63-c3ea9bd4d750",
                    "LayerId": "3284271a-d392-477f-97ad-7c9f478cecf2"
                }
            ]
        },
        {
            "id": "8474b09f-894b-4907-95ad-54e845a5db8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "compositeImage": {
                "id": "f5644f96-bc2b-4be4-a73c-959286cd2551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8474b09f-894b-4907-95ad-54e845a5db8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2628842-f576-48c3-b2be-a6f66a2158e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8474b09f-894b-4907-95ad-54e845a5db8d",
                    "LayerId": "3284271a-d392-477f-97ad-7c9f478cecf2"
                }
            ]
        },
        {
            "id": "25d8c720-e6af-43a2-ba2f-9c6b4653daa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "compositeImage": {
                "id": "58f5e91f-b8f8-49e3-a770-4d0d60b0622b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25d8c720-e6af-43a2-ba2f-9c6b4653daa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3feb7d38-6978-4f74-ac4b-4ec9881f331d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25d8c720-e6af-43a2-ba2f-9c6b4653daa1",
                    "LayerId": "3284271a-d392-477f-97ad-7c9f478cecf2"
                }
            ]
        },
        {
            "id": "f2b53bf8-4a6f-4d3f-8492-47577aa683df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "compositeImage": {
                "id": "a61f3bf2-54cc-4b11-a684-9ca7b3871b95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2b53bf8-4a6f-4d3f-8492-47577aa683df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2852891d-b86a-4ad9-9aa6-6665bf1cc611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2b53bf8-4a6f-4d3f-8492-47577aa683df",
                    "LayerId": "3284271a-d392-477f-97ad-7c9f478cecf2"
                }
            ]
        },
        {
            "id": "72457f18-bc7e-4926-af6e-0d927f69e981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "compositeImage": {
                "id": "ff26ba3a-4d86-40ca-b76d-81dbb196b7f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72457f18-bc7e-4926-af6e-0d927f69e981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f88dee1e-2365-4a27-a38a-4d58e90109ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72457f18-bc7e-4926-af6e-0d927f69e981",
                    "LayerId": "3284271a-d392-477f-97ad-7c9f478cecf2"
                }
            ]
        },
        {
            "id": "311e0026-a520-497d-9c52-180b1f2c51f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "compositeImage": {
                "id": "3b367873-910e-4d05-a090-450d2ae0dad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "311e0026-a520-497d-9c52-180b1f2c51f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ee40f8f-487c-4442-9338-7f60a981fdff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "311e0026-a520-497d-9c52-180b1f2c51f3",
                    "LayerId": "3284271a-d392-477f-97ad-7c9f478cecf2"
                }
            ]
        },
        {
            "id": "8da269bf-475e-470b-9583-3fa3de85b2f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "compositeImage": {
                "id": "3c4cf4fb-b4b8-49b4-b487-7f8fe8fafede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da269bf-475e-470b-9583-3fa3de85b2f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0850a1e1-29c3-4932-8ab2-2b3ae63b0614",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da269bf-475e-470b-9583-3fa3de85b2f0",
                    "LayerId": "3284271a-d392-477f-97ad-7c9f478cecf2"
                }
            ]
        },
        {
            "id": "fe8db1a3-7a17-4e35-92e8-71cc9c8cf9c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "compositeImage": {
                "id": "ed6ffcc9-5ae5-417b-8fd1-8ddecd30fe56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe8db1a3-7a17-4e35-92e8-71cc9c8cf9c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bd26166-35c3-4650-85d9-4e5e019ea817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe8db1a3-7a17-4e35-92e8-71cc9c8cf9c8",
                    "LayerId": "3284271a-d392-477f-97ad-7c9f478cecf2"
                }
            ]
        },
        {
            "id": "80a3caca-8509-4cb5-903d-eaaf79cc13fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "compositeImage": {
                "id": "c665285b-831a-480e-846b-9e78d6c5d553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80a3caca-8509-4cb5-903d-eaaf79cc13fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e042a82-6404-422e-bcf6-52cb525f3c4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80a3caca-8509-4cb5-903d-eaaf79cc13fe",
                    "LayerId": "3284271a-d392-477f-97ad-7c9f478cecf2"
                }
            ]
        },
        {
            "id": "c29ae4e5-e4a5-414c-9c86-00225bdeb1c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "compositeImage": {
                "id": "33cd459a-95ad-4fcc-8947-a9a5bd91ed79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c29ae4e5-e4a5-414c-9c86-00225bdeb1c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b0b7db2-a6ea-453d-8c54-6de7785dd5b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c29ae4e5-e4a5-414c-9c86-00225bdeb1c8",
                    "LayerId": "3284271a-d392-477f-97ad-7c9f478cecf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "3284271a-d392-477f-97ad-7c9f478cecf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7e6ecdc-0414-454c-a814-9bbc6da198c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}