{
    "id": "354a4ded-4bb2-44bf-8779-1fb90259a218",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage1button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2aa57a94-f3a2-44b0-80f9-77cbd7c26f4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "354a4ded-4bb2-44bf-8779-1fb90259a218",
            "compositeImage": {
                "id": "b24a0dfd-775b-4cb3-af3d-3e68c8d4906f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa57a94-f3a2-44b0-80f9-77cbd7c26f4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12ae544b-d419-4c0e-86b4-2379313c21c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa57a94-f3a2-44b0-80f9-77cbd7c26f4c",
                    "LayerId": "1a09b23a-c531-49ef-8228-123dbe23eac9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1a09b23a-c531-49ef-8228-123dbe23eac9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "354a4ded-4bb2-44bf-8779-1fb90259a218",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}