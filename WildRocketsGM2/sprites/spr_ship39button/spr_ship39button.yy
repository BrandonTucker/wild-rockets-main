{
    "id": "b52b60ea-c57e-47bb-b199-b154b7ab2583",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship39button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6089f1e6-4370-45fa-b2e6-c439edec5c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b52b60ea-c57e-47bb-b199-b154b7ab2583",
            "compositeImage": {
                "id": "dafa1cae-a4bb-4cc3-a9f2-6e08a34174c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6089f1e6-4370-45fa-b2e6-c439edec5c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2c3e649-9cb9-4d89-b23f-85dc1a53888f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6089f1e6-4370-45fa-b2e6-c439edec5c01",
                    "LayerId": "6c4fe057-70d8-4d9b-a643-b8b938aa77a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6c4fe057-70d8-4d9b-a643-b8b938aa77a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b52b60ea-c57e-47bb-b199-b154b7ab2583",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}