{
    "id": "723a13ab-35a2-4bf1-81be-45b3eb420164",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_block3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 255,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7049ba4a-1fad-4f35-8afe-3438df844350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "723a13ab-35a2-4bf1-81be-45b3eb420164",
            "compositeImage": {
                "id": "bab9411f-1310-4bf7-afeb-aeddd75edb70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7049ba4a-1fad-4f35-8afe-3438df844350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b187953-0445-473f-b2c9-2170fa3fe979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7049ba4a-1fad-4f35-8afe-3438df844350",
                    "LayerId": "fcecef23-b839-4b3e-b3d9-628a8f20176f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fcecef23-b839-4b3e-b3d9-628a8f20176f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "723a13ab-35a2-4bf1-81be-45b3eb420164",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 16,
    "yorig": 16
}