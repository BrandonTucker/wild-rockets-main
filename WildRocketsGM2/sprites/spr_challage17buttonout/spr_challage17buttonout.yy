{
    "id": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage17buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "026a3747-92f6-474c-92c3-5e42ec8e5ce5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "compositeImage": {
                "id": "de6368ab-94ae-46c0-99ca-0eb02008f6d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "026a3747-92f6-474c-92c3-5e42ec8e5ce5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f1f5bcc-5fe8-407e-a903-ef559e654a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "026a3747-92f6-474c-92c3-5e42ec8e5ce5",
                    "LayerId": "f3e08a94-8731-4538-af1d-626c7cd3501a"
                }
            ]
        },
        {
            "id": "29942268-cd05-49e6-9832-44916782fdae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "compositeImage": {
                "id": "fcde81ea-4bca-4946-858b-2920782b3753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29942268-cd05-49e6-9832-44916782fdae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90c230a9-7d9f-4601-b265-a81c3709262e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29942268-cd05-49e6-9832-44916782fdae",
                    "LayerId": "f3e08a94-8731-4538-af1d-626c7cd3501a"
                }
            ]
        },
        {
            "id": "53ccb0b4-040b-41e0-bb55-440c3d1029d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "compositeImage": {
                "id": "8c5754f9-cf20-4424-815e-61887f9f2704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53ccb0b4-040b-41e0-bb55-440c3d1029d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd49caec-3a00-4bd4-8f24-7cee6a328318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53ccb0b4-040b-41e0-bb55-440c3d1029d5",
                    "LayerId": "f3e08a94-8731-4538-af1d-626c7cd3501a"
                }
            ]
        },
        {
            "id": "9201db9f-378e-4d3c-8e57-05c4815e563e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "compositeImage": {
                "id": "17a7045a-29c9-4112-97be-21f773de5a6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9201db9f-378e-4d3c-8e57-05c4815e563e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4abeb27c-cf4a-4e19-a970-529ea0393300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9201db9f-378e-4d3c-8e57-05c4815e563e",
                    "LayerId": "f3e08a94-8731-4538-af1d-626c7cd3501a"
                }
            ]
        },
        {
            "id": "da227116-e13f-44e3-a90f-ca6b432c169b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "compositeImage": {
                "id": "e730ea28-f4cf-4bbf-a6a0-96e3c4fd018e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da227116-e13f-44e3-a90f-ca6b432c169b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "613f08df-caec-4d96-a466-35136a419228",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da227116-e13f-44e3-a90f-ca6b432c169b",
                    "LayerId": "f3e08a94-8731-4538-af1d-626c7cd3501a"
                }
            ]
        },
        {
            "id": "1028329b-787f-40fb-ace9-4494aa129b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "compositeImage": {
                "id": "76bf8357-2cb6-4877-baa3-a318074890a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1028329b-787f-40fb-ace9-4494aa129b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dec86c3c-41df-4938-b2f9-0b599a46f505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1028329b-787f-40fb-ace9-4494aa129b48",
                    "LayerId": "f3e08a94-8731-4538-af1d-626c7cd3501a"
                }
            ]
        },
        {
            "id": "b3ee22f4-2443-4d91-a3d2-156df10d0329",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "compositeImage": {
                "id": "c3ce7935-e1c3-4528-b3d3-9ebffada900f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3ee22f4-2443-4d91-a3d2-156df10d0329",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f669c09-d4ab-4ab6-9bec-d351423489d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3ee22f4-2443-4d91-a3d2-156df10d0329",
                    "LayerId": "f3e08a94-8731-4538-af1d-626c7cd3501a"
                }
            ]
        },
        {
            "id": "c0d4c7e1-5df5-453d-9620-57a57303bc3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "compositeImage": {
                "id": "e9b0b834-23ca-49f5-bf02-b9ad3c5024d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0d4c7e1-5df5-453d-9620-57a57303bc3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8c7bf60-1190-46d7-b97a-c3a6389f0636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d4c7e1-5df5-453d-9620-57a57303bc3f",
                    "LayerId": "f3e08a94-8731-4538-af1d-626c7cd3501a"
                }
            ]
        },
        {
            "id": "58e3a4cd-090a-49b3-a5a6-e41883eb83f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "compositeImage": {
                "id": "21c84cbd-25ce-490e-bd73-8b30c578c942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e3a4cd-090a-49b3-a5a6-e41883eb83f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a183887-acd6-4aa1-b62f-f4e2290754d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e3a4cd-090a-49b3-a5a6-e41883eb83f7",
                    "LayerId": "f3e08a94-8731-4538-af1d-626c7cd3501a"
                }
            ]
        },
        {
            "id": "52c0baee-9964-4c43-b582-2d1cd17558c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "compositeImage": {
                "id": "96452bfc-72c1-4c73-a7e0-38d7987935a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52c0baee-9964-4c43-b582-2d1cd17558c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "328aff80-2b26-4799-824f-1e7c7f3ea56b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52c0baee-9964-4c43-b582-2d1cd17558c9",
                    "LayerId": "f3e08a94-8731-4538-af1d-626c7cd3501a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "f3e08a94-8731-4538-af1d-626c7cd3501a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca5a3c58-9a23-4491-bea2-2124b51c8d4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}