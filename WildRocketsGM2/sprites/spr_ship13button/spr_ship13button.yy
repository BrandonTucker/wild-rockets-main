{
    "id": "a40f695e-11b8-4942-8d62-f8858ad9f9f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship13button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 13,
    "bbox_right": 88,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72749fca-7f9e-4979-8429-f0f6af55b5da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a40f695e-11b8-4942-8d62-f8858ad9f9f2",
            "compositeImage": {
                "id": "8f3e8878-b61a-44b3-b25d-783aa46da00e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72749fca-7f9e-4979-8429-f0f6af55b5da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf700c8c-a4b4-4d0e-9bb4-c5a9716476d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72749fca-7f9e-4979-8429-f0f6af55b5da",
                    "LayerId": "2a10ab88-932f-4a98-a0d1-025c007aeee7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "2a10ab88-932f-4a98-a0d1-025c007aeee7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a40f695e-11b8-4942-8d62-f8858ad9f9f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}