{
    "id": "3e401d18-6701-4c6c-a1e7-6776c672d455",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship34explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f0f049a-d1e8-4613-9135-2f99d21936d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e401d18-6701-4c6c-a1e7-6776c672d455",
            "compositeImage": {
                "id": "3c241b77-82e5-44d5-a96d-4819182ea503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f0f049a-d1e8-4613-9135-2f99d21936d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a52f3bdf-bebe-47e5-8877-ab1cf62f6679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f0f049a-d1e8-4613-9135-2f99d21936d4",
                    "LayerId": "cc907455-a31b-4e4b-b62b-3bf9280d2aec"
                }
            ]
        },
        {
            "id": "1a936d5c-b134-4e78-9e32-d5b3b1bd923a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e401d18-6701-4c6c-a1e7-6776c672d455",
            "compositeImage": {
                "id": "036ca4ea-4214-4888-a4f8-dd30ebfeccde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a936d5c-b134-4e78-9e32-d5b3b1bd923a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aea45984-da82-4b54-b14d-5c906e0e3d17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a936d5c-b134-4e78-9e32-d5b3b1bd923a",
                    "LayerId": "cc907455-a31b-4e4b-b62b-3bf9280d2aec"
                }
            ]
        },
        {
            "id": "5f39247d-7ebf-4f1e-95bb-fdab342c8e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e401d18-6701-4c6c-a1e7-6776c672d455",
            "compositeImage": {
                "id": "27341af9-195c-446d-8ad3-4a3e3a125a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f39247d-7ebf-4f1e-95bb-fdab342c8e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77f2ad5d-9893-4cef-bc00-fe24cdc3d458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f39247d-7ebf-4f1e-95bb-fdab342c8e40",
                    "LayerId": "cc907455-a31b-4e4b-b62b-3bf9280d2aec"
                }
            ]
        },
        {
            "id": "4a024f7e-aba5-427d-85e8-fe87fa6d73e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e401d18-6701-4c6c-a1e7-6776c672d455",
            "compositeImage": {
                "id": "f8bdb81b-b7b6-4dd2-8bf3-15072794ae49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a024f7e-aba5-427d-85e8-fe87fa6d73e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "258f1912-bded-416c-a907-a3010372ff6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a024f7e-aba5-427d-85e8-fe87fa6d73e1",
                    "LayerId": "cc907455-a31b-4e4b-b62b-3bf9280d2aec"
                }
            ]
        },
        {
            "id": "1d0a7b6d-8215-4322-8c76-44e35ff6c1fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e401d18-6701-4c6c-a1e7-6776c672d455",
            "compositeImage": {
                "id": "2411ea82-3c6a-4986-bad6-5eab075e8364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d0a7b6d-8215-4322-8c76-44e35ff6c1fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4253ec27-a7cf-45c2-ac39-759246265482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d0a7b6d-8215-4322-8c76-44e35ff6c1fa",
                    "LayerId": "cc907455-a31b-4e4b-b62b-3bf9280d2aec"
                }
            ]
        },
        {
            "id": "e6dbaebc-0080-4389-90e5-7dd93e3da247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e401d18-6701-4c6c-a1e7-6776c672d455",
            "compositeImage": {
                "id": "64c76585-6582-4a04-b263-01b47c4d6d25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6dbaebc-0080-4389-90e5-7dd93e3da247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "087eb3a0-0549-4378-8fb1-033add72e027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6dbaebc-0080-4389-90e5-7dd93e3da247",
                    "LayerId": "cc907455-a31b-4e4b-b62b-3bf9280d2aec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 298,
    "layers": [
        {
            "id": "cc907455-a31b-4e4b-b62b-3bf9280d2aec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e401d18-6701-4c6c-a1e7-6776c672d455",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 130,
    "xorig": 64,
    "yorig": 112
}