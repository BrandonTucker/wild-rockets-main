{
    "id": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_alien",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 4,
    "bbox_right": 35,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d2e703f-d8b3-43d2-8bbc-97577fcc8a95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "cf1a97a0-8245-448d-a311-fcf0390cd469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d2e703f-d8b3-43d2-8bbc-97577fcc8a95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd7bf8e-165c-464e-8c86-4b2c82b644a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d2e703f-d8b3-43d2-8bbc-97577fcc8a95",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "75e26f02-498e-42c1-9d5e-1dae22a277bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "4bc3acea-aac8-4412-8dc4-c2f23681a2ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75e26f02-498e-42c1-9d5e-1dae22a277bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac6a7013-968b-4e21-9192-31eb41fe10e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75e26f02-498e-42c1-9d5e-1dae22a277bb",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "f45480fb-1180-4e1e-bf5f-9f38aba740fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "709cef0d-66c8-44ed-a7ed-fa8ac14aa9cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f45480fb-1180-4e1e-bf5f-9f38aba740fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a483618-f976-4046-84e7-bc44493af364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f45480fb-1180-4e1e-bf5f-9f38aba740fe",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "772f0d9d-91a0-45ab-80e4-a9dead81242a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "65d8f303-53a4-44e6-845b-fe007f086a2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "772f0d9d-91a0-45ab-80e4-a9dead81242a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9234e4a-e885-4ad1-a749-997ea8ab7b3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772f0d9d-91a0-45ab-80e4-a9dead81242a",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "01fbfeb1-dd54-49cf-b761-b2335c1dde39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "f25a08f0-1d83-4010-a2a9-c1d82245227b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01fbfeb1-dd54-49cf-b761-b2335c1dde39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "035e8ba6-3797-4ae8-8fe4-c446cb3dbd7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01fbfeb1-dd54-49cf-b761-b2335c1dde39",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "f031faa4-2cec-4932-ac46-d551a24fa316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "6fee41f6-7c65-4db7-985c-60f242e36127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f031faa4-2cec-4932-ac46-d551a24fa316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60013561-82bb-4aeb-842a-ac2baf4c9330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f031faa4-2cec-4932-ac46-d551a24fa316",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "46334efa-a853-477d-b086-9430a55ad286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "ea8de955-fafc-44c5-8357-b9f5c23b6837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46334efa-a853-477d-b086-9430a55ad286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "324608b3-7c9d-4247-98ce-402924c9de6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46334efa-a853-477d-b086-9430a55ad286",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "c6d8f8a2-953e-444e-ae5f-a57200430cd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "74c592ac-ff48-4207-b998-b72740be4e8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d8f8a2-953e-444e-ae5f-a57200430cd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efad8dcc-a92c-4f05-a06e-8fca05cb02b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d8f8a2-953e-444e-ae5f-a57200430cd3",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "5427ef20-0994-4e3d-8c1a-a4d2d8f178b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "327c2767-9fe5-4763-af95-1d0d616958c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5427ef20-0994-4e3d-8c1a-a4d2d8f178b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "080f5761-326f-4758-96e0-bfd078b9fd61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5427ef20-0994-4e3d-8c1a-a4d2d8f178b2",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "1684472a-fecd-480d-86a5-a83109799de4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "c811f93e-1abe-4613-b509-e95feca75364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1684472a-fecd-480d-86a5-a83109799de4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f830fa8a-b502-4086-804a-d2a3dd8e8786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1684472a-fecd-480d-86a5-a83109799de4",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "9e59395a-cdfc-4ea4-a172-aab879a6ca9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "1f9013d6-64ec-42f0-8c7b-1d8d4e155bee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e59395a-cdfc-4ea4-a172-aab879a6ca9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3c42d8-5330-41d1-a7cd-51075850dbce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e59395a-cdfc-4ea4-a172-aab879a6ca9c",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "dcbb6098-67fa-4ef0-9c31-938b8debde87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "6ba775b7-f908-487e-8125-8c3c52a7097a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcbb6098-67fa-4ef0-9c31-938b8debde87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "112eded3-6689-4fd5-a09c-48f76f1a9f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcbb6098-67fa-4ef0-9c31-938b8debde87",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "4d9fbbdf-78b6-47a4-aea2-4ac99fd62725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "3f82d12a-a51e-41e8-9974-6a59726898d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d9fbbdf-78b6-47a4-aea2-4ac99fd62725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f6939f1-c601-4c78-b334-20d84fa9376f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d9fbbdf-78b6-47a4-aea2-4ac99fd62725",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "fe449cca-cf91-49d9-9d42-9fcd90700331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "805de551-932f-462c-9a19-725219540d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe449cca-cf91-49d9-9d42-9fcd90700331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9282c63f-a604-4ef2-a5f7-9fb3195d6e42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe449cca-cf91-49d9-9d42-9fcd90700331",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "7f5cc7c7-f74e-4560-a227-65f8b2aaa6a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "dd811833-137f-4358-abb3-995089e44b53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f5cc7c7-f74e-4560-a227-65f8b2aaa6a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25453d45-4761-4a8e-a3d3-23d4fc90af6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f5cc7c7-f74e-4560-a227-65f8b2aaa6a4",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        },
        {
            "id": "57fab354-ddd4-4149-aae6-e1cdcc2fa2fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "compositeImage": {
                "id": "f898dd49-0494-40e7-be10-fa9acc2dcd86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57fab354-ddd4-4149-aae6-e1cdcc2fa2fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7218ca0b-a588-4d55-b991-2dfc674e50e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57fab354-ddd4-4149-aae6-e1cdcc2fa2fe",
                    "LayerId": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "2eb5b563-b761-4b8a-95ee-2d2fa3daa323",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0b1fa68-2446-4173-90b5-2c0c267bc69c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}