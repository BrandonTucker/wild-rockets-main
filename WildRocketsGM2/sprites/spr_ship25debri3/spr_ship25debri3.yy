{
    "id": "124b9719-7f32-4004-835c-63b82037b2b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship25debri3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 102,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cf9cb0b-4b3e-49cd-9c36-c318fa25cc04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "d795228b-9eca-47a3-86ae-24c853f15122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf9cb0b-4b3e-49cd-9c36-c318fa25cc04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3e386d9-9643-4a90-b257-1790644aba4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf9cb0b-4b3e-49cd-9c36-c318fa25cc04",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "09bfa5be-b9f4-4a69-9042-96d8caf25cb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "82bf6138-e468-4cc1-889b-1e54b35c1aef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09bfa5be-b9f4-4a69-9042-96d8caf25cb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844a06f0-12e8-4990-9588-4bb426806155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09bfa5be-b9f4-4a69-9042-96d8caf25cb1",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "5e517932-e3e7-4b4a-9feb-63853c846273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "efb88886-9194-4a79-bc02-725866d29af0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e517932-e3e7-4b4a-9feb-63853c846273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e036afe7-b2d8-409f-a0a1-863a818fcb01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e517932-e3e7-4b4a-9feb-63853c846273",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "970b7027-558b-48e3-8dc3-7cd4b8552ba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "88c8f804-2896-4428-a4e9-a0b645c3e966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "970b7027-558b-48e3-8dc3-7cd4b8552ba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b2a06cd-7f5b-4078-b591-9cc0444ce2cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "970b7027-558b-48e3-8dc3-7cd4b8552ba4",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "20780ac9-5695-4aef-b67a-1362a8b272e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "23daeb9a-18d8-43c2-881f-dddc0fefb594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20780ac9-5695-4aef-b67a-1362a8b272e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbb39788-8be4-4309-8b46-320f76cf891b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20780ac9-5695-4aef-b67a-1362a8b272e9",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "c5f7ba2e-3817-4e7f-bc18-3ec502a18d0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "2789d8b7-0bfe-4b1d-aa3e-51f59b519e1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5f7ba2e-3817-4e7f-bc18-3ec502a18d0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84724436-7106-4c4a-8f17-7826cede9c02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f7ba2e-3817-4e7f-bc18-3ec502a18d0a",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "91044619-b609-4392-9685-228e7993954f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "53d928b4-8c99-4b9e-b03d-be14bdd4d0c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91044619-b609-4392-9685-228e7993954f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "994e4717-789f-49b6-aabe-c75d40a87ea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91044619-b609-4392-9685-228e7993954f",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "b0df17ad-3d2e-4751-8df1-019b99161dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "cce2f605-5fd9-43a7-872f-8249a75395f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0df17ad-3d2e-4751-8df1-019b99161dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed778350-c2b6-4aeb-bb4b-58ddbcfc322d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0df17ad-3d2e-4751-8df1-019b99161dfc",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "b32d3d3f-4aea-4512-bcc5-81d0a00bd3a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "92fc65d9-9aaf-42dd-8286-76643aef13fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b32d3d3f-4aea-4512-bcc5-81d0a00bd3a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeec99cc-fe03-4e54-ac6a-90c714a54708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b32d3d3f-4aea-4512-bcc5-81d0a00bd3a1",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "251ee9cc-b6b1-4c28-8e95-d909b6d49a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "66925170-c33a-4fee-ba6a-82da24409480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "251ee9cc-b6b1-4c28-8e95-d909b6d49a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d889bdfa-1608-4e58-9d42-344237068a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "251ee9cc-b6b1-4c28-8e95-d909b6d49a47",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "cace862f-86b1-4f05-aa1e-57a400d6c39b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "17c66269-4c36-4c00-b9a4-a18ae556e81f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cace862f-86b1-4f05-aa1e-57a400d6c39b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80362154-3d7b-4ab1-a0da-7130b68dfe41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cace862f-86b1-4f05-aa1e-57a400d6c39b",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "9beefa9f-a363-44ed-9749-fc7f3e4e26ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "b179e8c8-5020-4a06-a970-d5a88d69dd1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9beefa9f-a363-44ed-9749-fc7f3e4e26ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f621e4e-f118-495a-b04c-af2c8c45bd3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9beefa9f-a363-44ed-9749-fc7f3e4e26ba",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "666e88b8-e402-40f2-93d0-b26bf75f587b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "4e30df4f-ccd5-4833-8011-2f6e67b4cdc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "666e88b8-e402-40f2-93d0-b26bf75f587b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eac98adf-580e-4f9a-a000-1063a4b47fcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "666e88b8-e402-40f2-93d0-b26bf75f587b",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "567c0051-097e-4417-982a-32a4bd678c86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "fbd2ac88-bd75-4ab9-b83f-c98dfb54d48a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "567c0051-097e-4417-982a-32a4bd678c86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "928aee13-c310-4d42-9f69-872a2ca75fd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567c0051-097e-4417-982a-32a4bd678c86",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "c64499d8-52bd-41fb-8276-27af25cfadb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "8c244c4d-3478-4d87-ab3b-43cc084239c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c64499d8-52bd-41fb-8276-27af25cfadb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0de18a0-2116-42a2-a770-3dc3a0eccb29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c64499d8-52bd-41fb-8276-27af25cfadb6",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "d3c00e6b-9ba4-4aa8-a77e-a6171de54e6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "915b48ee-ea14-4c96-b02c-9f720b120db5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3c00e6b-9ba4-4aa8-a77e-a6171de54e6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6f66fe6-8d8f-486e-bcce-1f661bdcf248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3c00e6b-9ba4-4aa8-a77e-a6171de54e6b",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "31aa5ba0-f36d-49c5-8674-bed17984bca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "f825513f-d2cc-4f3c-bce8-41f1cd30d3e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31aa5ba0-f36d-49c5-8674-bed17984bca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b441631-7502-4593-96f7-020439674111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31aa5ba0-f36d-49c5-8674-bed17984bca5",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "bd35f407-8594-419f-a5cb-56a6e511bd94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "0c37cb66-bfb9-4365-9ad3-1e83e5f02757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd35f407-8594-419f-a5cb-56a6e511bd94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00920f23-3cad-4e75-9a67-641f119c0d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd35f407-8594-419f-a5cb-56a6e511bd94",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "e96149ee-4aa1-47c4-97ee-a5205599e163",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "5d62b1c8-122b-40c4-8739-2a1369c6dc52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e96149ee-4aa1-47c4-97ee-a5205599e163",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f76f434-a9a1-4879-acb4-61ebd87003cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e96149ee-4aa1-47c4-97ee-a5205599e163",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "bbc9ff72-4e78-481b-87b3-8c10554c1d76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "d0b9ac9a-3f13-429d-9b54-66733eaa20cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc9ff72-4e78-481b-87b3-8c10554c1d76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54907176-0cec-4461-9813-475a6f617b1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc9ff72-4e78-481b-87b3-8c10554c1d76",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "8ecd2d30-1856-49f1-8d40-a7c700c44108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "1875d048-f4d6-4c56-8929-34526d8cb5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ecd2d30-1856-49f1-8d40-a7c700c44108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05ccbd7f-a61f-443c-ad9f-933c11544366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ecd2d30-1856-49f1-8d40-a7c700c44108",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "fd082856-68eb-468e-a353-abf399096775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "c552fdb4-9969-457d-8cfb-589d076386ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd082856-68eb-468e-a353-abf399096775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fd2b5d3-6666-4e03-87c5-90d4814e19c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd082856-68eb-468e-a353-abf399096775",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "21651b9f-e15b-4121-87d3-d5f84c841326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "1c6f53d0-8a59-4a08-8ca7-a756ca54edcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21651b9f-e15b-4121-87d3-d5f84c841326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea00e79-069e-4648-a6dc-608478850e8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21651b9f-e15b-4121-87d3-d5f84c841326",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "3a8bcecb-29bb-4992-90f4-bcf96010eb52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "f0104fe9-d9cc-48dc-96ea-b705dcd709bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a8bcecb-29bb-4992-90f4-bcf96010eb52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16604904-9343-4d7d-a803-fc38f023bd02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a8bcecb-29bb-4992-90f4-bcf96010eb52",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        },
        {
            "id": "d362e62a-c7b2-49b9-9e5d-163354abf83a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "compositeImage": {
                "id": "29c012ad-5f43-4b65-ab13-398979ec3549",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d362e62a-c7b2-49b9-9e5d-163354abf83a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6045b27-c9a0-408a-baf3-8f74354a3ab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d362e62a-c7b2-49b9-9e5d-163354abf83a",
                    "LayerId": "8059709f-1848-4e2d-ba1c-54befa03ce64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "8059709f-1848-4e2d-ba1c-54befa03ce64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "124b9719-7f32-4004-835c-63b82037b2b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 103,
    "xorig": 51,
    "yorig": 51
}