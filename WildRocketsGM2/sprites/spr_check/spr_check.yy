{
    "id": "6e128cc8-f264-4299-9c91-4c540862bf5d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_check",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5fc6ae1-d865-43a4-9f72-1e24a9bd5e62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e128cc8-f264-4299-9c91-4c540862bf5d",
            "compositeImage": {
                "id": "c2f98b27-d103-4661-b1e6-5ed3e751c4b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5fc6ae1-d865-43a4-9f72-1e24a9bd5e62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd2d0c3-3d81-4d75-8533-01c23ea9cf3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5fc6ae1-d865-43a4-9f72-1e24a9bd5e62",
                    "LayerId": "0b5af977-a6c5-4a44-b249-4a5829400557"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0b5af977-a6c5-4a44-b249-4a5829400557",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e128cc8-f264-4299-9c91-4c540862bf5d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 0,
    "yorig": 0
}