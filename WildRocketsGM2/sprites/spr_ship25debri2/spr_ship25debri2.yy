{
    "id": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship25debri2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 87,
    "bbox_left": 0,
    "bbox_right": 87,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cbe931c-60ab-4ed1-aa6d-e573665741e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "e543dd7b-e7a5-4d10-b9bb-1090730c8dcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cbe931c-60ab-4ed1-aa6d-e573665741e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e3c6ff4-f910-4d67-a127-f5c80d5ac719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cbe931c-60ab-4ed1-aa6d-e573665741e5",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "e53f0ba6-1cac-4e9e-b2a6-8ae71059dfb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "2bd83e62-f3ac-4f34-b3d9-f7208dec0b00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e53f0ba6-1cac-4e9e-b2a6-8ae71059dfb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26f3a589-1032-42d5-91f6-6658119e79db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e53f0ba6-1cac-4e9e-b2a6-8ae71059dfb0",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "885b6099-0f7b-4883-a516-0ca94f827e7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "d00c86f5-d8e1-45ad-9044-2fa4ced48786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "885b6099-0f7b-4883-a516-0ca94f827e7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b65e46-3741-40eb-bb52-661e44eb035b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "885b6099-0f7b-4883-a516-0ca94f827e7d",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "6a86681b-66cf-486c-96ce-92c6a6d04494",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "fc37e8be-43b7-4ccc-abf9-531e1ff1e057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a86681b-66cf-486c-96ce-92c6a6d04494",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a10fadeb-ecf8-4956-ad0c-cbddc4372bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a86681b-66cf-486c-96ce-92c6a6d04494",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "219f590a-706b-4be8-ab77-5b3d6b22c3e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "190b1999-186c-4d81-a26f-4fa6cff12af5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "219f590a-706b-4be8-ab77-5b3d6b22c3e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d57abfad-2f44-4a9f-9fc5-e801b4d8ff8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "219f590a-706b-4be8-ab77-5b3d6b22c3e7",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "6785ddbc-13a0-4a8e-84cd-a0fca8e53f0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "240dc8cd-bda0-49c9-b5cb-6da1b690aa05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6785ddbc-13a0-4a8e-84cd-a0fca8e53f0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c93b700e-e4c9-441a-a3f2-e9062f5be843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6785ddbc-13a0-4a8e-84cd-a0fca8e53f0a",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "d34d7242-4f78-42d7-bb77-ae0ae4fb6f33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "86d48d2e-c5f6-48f7-b6e7-d74835c71370",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d34d7242-4f78-42d7-bb77-ae0ae4fb6f33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "128c1be5-a783-4569-bf97-625c1182e447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d34d7242-4f78-42d7-bb77-ae0ae4fb6f33",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "1098885a-0186-44fe-a330-afb38054bb20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "371b191d-386d-48cd-a3b3-d86ce89bd50d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1098885a-0186-44fe-a330-afb38054bb20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "705e27c1-8ae0-4641-97ba-3c154cbe1646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1098885a-0186-44fe-a330-afb38054bb20",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "4dde4b40-5563-41b2-8ccb-325c145ee03a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "d5f00cbd-1ec5-43bd-a63e-9d4f374d3c1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dde4b40-5563-41b2-8ccb-325c145ee03a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b6428c2-13d8-4bf1-934d-4de6bcb47743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dde4b40-5563-41b2-8ccb-325c145ee03a",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "54b35bb0-794c-4ef3-863b-9471dbd014c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "06a8d1b5-c619-46c1-9b3f-cc1673849188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54b35bb0-794c-4ef3-863b-9471dbd014c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a993d4-66e3-4e02-ae2a-44b5bcd5e71d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b35bb0-794c-4ef3-863b-9471dbd014c5",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "4afaff24-e908-4613-9387-50b557b18146",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "156e240f-2d36-4ee8-b3e1-a84c32d256e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4afaff24-e908-4613-9387-50b557b18146",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e20feb0a-a820-4421-aa5b-866497581196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4afaff24-e908-4613-9387-50b557b18146",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "0674fd2e-0e8c-4819-81e3-fcb796050454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "6c60cc30-d96a-4828-97db-98c58738d66e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0674fd2e-0e8c-4819-81e3-fcb796050454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2472aa1f-521a-434d-9f49-88e1e97efd9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0674fd2e-0e8c-4819-81e3-fcb796050454",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "ce84765f-8248-42f7-a3b1-97a20c81ce94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "20cfab7b-b209-4aa6-9727-75cc7e8e4e50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce84765f-8248-42f7-a3b1-97a20c81ce94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60e67a4f-70f7-4a5b-96ab-85d7267b8f2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce84765f-8248-42f7-a3b1-97a20c81ce94",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "6d15aff2-0055-40c8-84f4-49d75c8bf7c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "c51c4823-7f0e-4004-b246-9fcf4ecaba8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d15aff2-0055-40c8-84f4-49d75c8bf7c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6f936f2-4e47-4a11-9697-d757a2df5f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d15aff2-0055-40c8-84f4-49d75c8bf7c4",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "1ae22552-8751-46ff-974a-245c1953eb2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "1c444a6c-72c8-4d4b-898e-bd27777ef437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ae22552-8751-46ff-974a-245c1953eb2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2094aa1-4386-43f9-922e-4bd4393444b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ae22552-8751-46ff-974a-245c1953eb2e",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "23000b28-6196-4e84-bf35-967cb012c60c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "1da911de-58d7-40e3-bd50-4130b685498a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23000b28-6196-4e84-bf35-967cb012c60c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc763936-f115-4a54-93b8-c09bc00e3dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23000b28-6196-4e84-bf35-967cb012c60c",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "52e7a1fb-d4fc-45ed-a121-7729526b3dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "038fa535-da0f-4545-9469-dabf0700b3de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e7a1fb-d4fc-45ed-a121-7729526b3dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfccd5cd-2f21-4631-992c-f552c40e31c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e7a1fb-d4fc-45ed-a121-7729526b3dbd",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "5560f86d-5039-4945-9217-f4fd788a5d70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "440b429f-b57c-4e4d-943e-9fbdf7373427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5560f86d-5039-4945-9217-f4fd788a5d70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8895892-b78d-4be7-9e8f-ff3e6f9fa4c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5560f86d-5039-4945-9217-f4fd788a5d70",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "5285747a-978a-490a-a585-8964a841244c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "2ca84eb0-4178-4b73-9ed4-8c11d66de2f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5285747a-978a-490a-a585-8964a841244c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1551dc09-57f6-4a89-96bb-3f069614e489",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5285747a-978a-490a-a585-8964a841244c",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "8b20d3d2-204e-4dd0-9f57-99673e46fe34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "63549bf7-37e0-49cf-9873-4e1fbbf09f39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b20d3d2-204e-4dd0-9f57-99673e46fe34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37e9c7e1-10a4-479a-80f4-9d783ac98651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b20d3d2-204e-4dd0-9f57-99673e46fe34",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "4dca7282-09c1-4b19-8b03-ab3d525b62d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "99ce469f-a135-454c-ad30-70c18f145e08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dca7282-09c1-4b19-8b03-ab3d525b62d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cbefdaa-d57f-423e-bb1e-5b2f69ad1b13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dca7282-09c1-4b19-8b03-ab3d525b62d1",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "f4075f48-2594-4da4-8a59-261a4bf93fe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "3d1f7c6e-5ac8-40ad-b90d-c7f482142881",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4075f48-2594-4da4-8a59-261a4bf93fe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdfb38e9-f055-48c2-96e0-666f1f342bd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4075f48-2594-4da4-8a59-261a4bf93fe4",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "29aaae54-c329-4033-b20f-9ab045a4ff5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "5bfe2cef-2153-46a7-8c87-0188e1894a06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29aaae54-c329-4033-b20f-9ab045a4ff5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06c3100f-3d01-4ee1-abd8-a602839d1482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29aaae54-c329-4033-b20f-9ab045a4ff5b",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "6f8f2fa6-3197-462c-8736-a6143f27acd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "2965de91-b061-4458-a754-86dfb14d44d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f8f2fa6-3197-462c-8736-a6143f27acd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30fc697f-0302-4dc6-83cf-e1aec82c946a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f8f2fa6-3197-462c-8736-a6143f27acd3",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        },
        {
            "id": "8c0b6628-33ea-4bab-966e-7a63d25ccb75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "compositeImage": {
                "id": "e936a747-9a3d-4720-9ddf-7391b488f591",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c0b6628-33ea-4bab-966e-7a63d25ccb75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bba9c19-ff74-42d8-bfe5-3ba8cffd3497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c0b6628-33ea-4bab-966e-7a63d25ccb75",
                    "LayerId": "72e76e7e-1a7c-4c51-9c4d-50ff64765484"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 88,
    "layers": [
        {
            "id": "72e76e7e-1a7c-4c51-9c4d-50ff64765484",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9ac67bb-1306-41bf-b3b0-dff5397ed80a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 44
}