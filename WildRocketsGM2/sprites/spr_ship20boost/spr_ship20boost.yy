{
    "id": "fd40ac58-163b-4f65-86d2-639a0be230f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship20boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ac88e43-3424-4aaf-9728-2b3bcfffb667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd40ac58-163b-4f65-86d2-639a0be230f7",
            "compositeImage": {
                "id": "2918459d-531f-4953-8e44-470d69559f36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ac88e43-3424-4aaf-9728-2b3bcfffb667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93cd2cab-3150-4afd-84e0-3d7b8317d3e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ac88e43-3424-4aaf-9728-2b3bcfffb667",
                    "LayerId": "f57d9160-b824-4e60-928e-c542de694c5b"
                }
            ]
        },
        {
            "id": "a498b504-74f3-4535-96fb-7a24babeb392",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd40ac58-163b-4f65-86d2-639a0be230f7",
            "compositeImage": {
                "id": "eafa4bdc-6168-4276-8eb3-f3e03eb6eb08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a498b504-74f3-4535-96fb-7a24babeb392",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bbed43a-7bc9-48ce-aa8e-b8e2be411ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a498b504-74f3-4535-96fb-7a24babeb392",
                    "LayerId": "f57d9160-b824-4e60-928e-c542de694c5b"
                }
            ]
        },
        {
            "id": "08ecc779-ba27-46f2-9b0e-63da1098cb5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd40ac58-163b-4f65-86d2-639a0be230f7",
            "compositeImage": {
                "id": "a94b1838-36bf-4b1f-a56d-659f1387eebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ecc779-ba27-46f2-9b0e-63da1098cb5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6224c858-80ee-4a2c-876d-26b7e3717bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ecc779-ba27-46f2-9b0e-63da1098cb5e",
                    "LayerId": "f57d9160-b824-4e60-928e-c542de694c5b"
                }
            ]
        },
        {
            "id": "84def7fc-8767-4cc9-a484-c03f86b443a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd40ac58-163b-4f65-86d2-639a0be230f7",
            "compositeImage": {
                "id": "f9e1ea71-bd09-4fab-9d4d-3e3eefd2654e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84def7fc-8767-4cc9-a484-c03f86b443a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeee36f2-c570-4eb7-b879-7e82b8dc7fa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84def7fc-8767-4cc9-a484-c03f86b443a4",
                    "LayerId": "f57d9160-b824-4e60-928e-c542de694c5b"
                }
            ]
        },
        {
            "id": "073b6496-5502-4ee6-a0ac-780da413a432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd40ac58-163b-4f65-86d2-639a0be230f7",
            "compositeImage": {
                "id": "6d8940ac-0d0f-45b0-b1a3-a4f0c832386e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "073b6496-5502-4ee6-a0ac-780da413a432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b91c925-c257-4701-a599-3a986910fd29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "073b6496-5502-4ee6-a0ac-780da413a432",
                    "LayerId": "f57d9160-b824-4e60-928e-c542de694c5b"
                }
            ]
        },
        {
            "id": "1fd947fb-26d3-4afa-a39c-8990346cc235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd40ac58-163b-4f65-86d2-639a0be230f7",
            "compositeImage": {
                "id": "7964786b-73d1-4077-a361-8311f6ee32ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fd947fb-26d3-4afa-a39c-8990346cc235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "152878c0-083e-4cce-9354-b9be9194b7cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fd947fb-26d3-4afa-a39c-8990346cc235",
                    "LayerId": "f57d9160-b824-4e60-928e-c542de694c5b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "f57d9160-b824-4e60-928e-c542de694c5b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd40ac58-163b-4f65-86d2-639a0be230f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 50
}