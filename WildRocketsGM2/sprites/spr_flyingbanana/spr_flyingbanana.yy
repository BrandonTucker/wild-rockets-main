{
    "id": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flyingbanana",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3ea087f-1e1c-4528-be6d-dfc2e54a41a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "f44831f2-08a3-48ab-ad4e-789c1ade7d02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3ea087f-1e1c-4528-be6d-dfc2e54a41a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84bee174-b250-4236-a083-3bdf0903b2ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3ea087f-1e1c-4528-be6d-dfc2e54a41a6",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "cebefa14-60cf-4c0c-b045-f08552f2b045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "e1c39367-1c4f-43d9-aa20-86303faaf1de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cebefa14-60cf-4c0c-b045-f08552f2b045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b51512-b3f4-47e6-983e-e219e615ce8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cebefa14-60cf-4c0c-b045-f08552f2b045",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "e70fca79-dcbe-433d-aae1-062cbf3b8db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "2e3b2ee6-8c6f-4e25-a0c4-1cbe4391391d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e70fca79-dcbe-433d-aae1-062cbf3b8db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbee99c0-61e8-4e46-ba67-26235a918943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e70fca79-dcbe-433d-aae1-062cbf3b8db5",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "dfc5bde1-0324-4bb9-9b98-626672c1bcdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "6b86a425-7d14-4c90-88e0-08641ba19ffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfc5bde1-0324-4bb9-9b98-626672c1bcdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5623e58a-ca8d-4144-a5cc-9e10f803401b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfc5bde1-0324-4bb9-9b98-626672c1bcdb",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "c9cdee58-e063-460a-804f-2cccabe61c14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "e37cbd76-9e91-49bf-af94-b41cd76d3e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9cdee58-e063-460a-804f-2cccabe61c14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd3d2dd7-9eac-4c93-86d2-508fad9cefcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9cdee58-e063-460a-804f-2cccabe61c14",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "fb51805c-bd0d-4f03-be06-abf416b719cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "3db71ffd-a186-4851-a860-f32407a40f7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb51805c-bd0d-4f03-be06-abf416b719cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6ac1e3d-01a4-4902-9382-f67540893ac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb51805c-bd0d-4f03-be06-abf416b719cf",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "9604fe49-40fe-4bb4-b8f9-1009ee41a859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "f6dfe9dd-bf89-46d2-9c9f-7027fb8571e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9604fe49-40fe-4bb4-b8f9-1009ee41a859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a9673b7-f697-47d9-92c2-588067198da2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9604fe49-40fe-4bb4-b8f9-1009ee41a859",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "cae1974c-f9d3-4cb3-a379-4c20cbec198d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "90669502-492f-4412-82e7-cc4331e39337",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cae1974c-f9d3-4cb3-a379-4c20cbec198d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1185948-8e48-4156-95a5-7b5f76304176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cae1974c-f9d3-4cb3-a379-4c20cbec198d",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "1dad2814-92ed-44d5-af8c-aaa683221cb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "49e6df0b-95f9-47b7-828f-954a4f4dea1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dad2814-92ed-44d5-af8c-aaa683221cb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f20cdc-08b2-4f21-8aa5-65c87471e805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dad2814-92ed-44d5-af8c-aaa683221cb9",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "c0498c9f-3c80-4322-aade-e8170d0b382a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "1954b1f6-d1b8-4367-b13a-b73ab97d565b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0498c9f-3c80-4322-aade-e8170d0b382a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "164d9f25-7c12-4696-9d0c-c305690a905a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0498c9f-3c80-4322-aade-e8170d0b382a",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "35adae84-69c1-4e7f-8c2e-6eff794db3d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "1401a822-0e66-489a-8e1d-37e00c9da5e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35adae84-69c1-4e7f-8c2e-6eff794db3d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d41c5677-b8e6-4aca-a9a1-df0487ef1f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35adae84-69c1-4e7f-8c2e-6eff794db3d5",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "d2fa402d-a287-4cde-854a-2fb2f9684ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "21ab9364-3282-4525-b9b9-cf8d2d07f44d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2fa402d-a287-4cde-854a-2fb2f9684ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dbdc4ba-7246-4300-a456-3d51cb5ed3d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2fa402d-a287-4cde-854a-2fb2f9684ed4",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "1d77f7b9-2745-4d1c-b2a8-bb7ea2052044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "424b14ad-ec34-4dbf-973d-a26a738600b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d77f7b9-2745-4d1c-b2a8-bb7ea2052044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf214b5f-2c6d-417c-a951-3f87a21ccced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d77f7b9-2745-4d1c-b2a8-bb7ea2052044",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "42774f27-77d1-404d-bae2-27e85055740d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "eafa8647-f0df-4d07-b215-95cafccd82d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42774f27-77d1-404d-bae2-27e85055740d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c370b8ea-f7fd-4ca5-a6a7-03e9bb34b9d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42774f27-77d1-404d-bae2-27e85055740d",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        },
        {
            "id": "6441fb4b-87d2-4c36-b199-509a2d2c9ac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "compositeImage": {
                "id": "9d1442ec-b75a-4d78-95d4-aeb23b19002b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6441fb4b-87d2-4c36-b199-509a2d2c9ac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb38cc84-6e4e-4146-ad1c-2ba2a2636e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6441fb4b-87d2-4c36-b199-509a2d2c9ac3",
                    "LayerId": "aac62e01-ea01-4084-97f3-efe6b5b942ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "aac62e01-ea01-4084-97f3-efe6b5b942ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51f36bfc-7d1f-41d1-93e5-789dd63a5750",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}