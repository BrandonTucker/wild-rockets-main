{
    "id": "79cf564d-a89b-445f-8d8e-6cd7a76a97bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 447,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59a93502-06c4-4a16-b2a9-68b8f6b0cacd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79cf564d-a89b-445f-8d8e-6cd7a76a97bb",
            "compositeImage": {
                "id": "6000a14c-9b76-415f-b78b-0a236e23b5a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a93502-06c4-4a16-b2a9-68b8f6b0cacd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5776571-5387-414c-9cd9-058655abe40b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a93502-06c4-4a16-b2a9-68b8f6b0cacd",
                    "LayerId": "07d3028a-f701-4de9-9c4e-fab8c1378bf5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 448,
    "layers": [
        {
            "id": "07d3028a-f701-4de9-9c4e-fab8c1378bf5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79cf564d-a89b-445f-8d8e-6cd7a76a97bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}