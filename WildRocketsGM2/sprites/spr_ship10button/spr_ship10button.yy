{
    "id": "8fe687e1-f8e4-4819-9b45-4dc617f8835e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship10button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 12,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e97ee62f-f32d-4738-b77c-f89b2b6d0e88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fe687e1-f8e4-4819-9b45-4dc617f8835e",
            "compositeImage": {
                "id": "443040cc-1019-4dbd-a8d3-3449b8a1d0a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97ee62f-f32d-4738-b77c-f89b2b6d0e88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fdb65e2-9047-4771-9216-a18f69785ec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97ee62f-f32d-4738-b77c-f89b2b6d0e88",
                    "LayerId": "355d25ec-82d4-4ec1-87b2-04ab03e2b862"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "355d25ec-82d4-4ec1-87b2-04ab03e2b862",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fe687e1-f8e4-4819-9b45-4dc617f8835e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}