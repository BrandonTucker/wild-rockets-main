{
    "id": "dde1ea8f-2a97-4133-a261-586feff60a1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship32locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd7f6a87-9ce8-4c0d-bb26-f330becf88c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dde1ea8f-2a97-4133-a261-586feff60a1e",
            "compositeImage": {
                "id": "dac39443-ab80-4638-a18f-587b077d7645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd7f6a87-9ce8-4c0d-bb26-f330becf88c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44fbac00-8237-4b5a-89a5-72bd685b5018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd7f6a87-9ce8-4c0d-bb26-f330becf88c0",
                    "LayerId": "1631df66-a689-4033-971b-0d75f8979138"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1631df66-a689-4033-971b-0d75f8979138",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dde1ea8f-2a97-4133-a261-586feff60a1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}