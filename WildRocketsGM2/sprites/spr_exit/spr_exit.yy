{
    "id": "102617c5-cacf-4458-8d4a-9756e75ef5d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 21,
    "bbox_right": 70,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "767c8358-694f-4c38-9d66-c3e882ca63e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "102617c5-cacf-4458-8d4a-9756e75ef5d8",
            "compositeImage": {
                "id": "31c29d79-5271-491b-8221-656304202ad1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "767c8358-694f-4c38-9d66-c3e882ca63e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffbd91ce-4d88-4d59-beef-51598c14773b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "767c8358-694f-4c38-9d66-c3e882ca63e7",
                    "LayerId": "1fde35ee-4af3-4192-b7f1-757197959987"
                }
            ]
        },
        {
            "id": "be9c65c0-4533-4b5a-aa89-62643a70cdd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "102617c5-cacf-4458-8d4a-9756e75ef5d8",
            "compositeImage": {
                "id": "6581475d-4634-49c2-af99-5dd4fe2d67ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be9c65c0-4533-4b5a-aa89-62643a70cdd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd8bf6de-80d0-4207-a758-44a2010400d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be9c65c0-4533-4b5a-aa89-62643a70cdd3",
                    "LayerId": "1fde35ee-4af3-4192-b7f1-757197959987"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1fde35ee-4af3-4192-b7f1-757197959987",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "102617c5-cacf-4458-8d4a-9756e75ef5d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}