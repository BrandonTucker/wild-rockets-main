{
    "id": "0c221841-b97b-4eb3-9bb6-424b1cc17110",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship26boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a269263c-6d7c-4f0f-a6e3-b967e78fab3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c221841-b97b-4eb3-9bb6-424b1cc17110",
            "compositeImage": {
                "id": "ca859586-bea6-4cc7-8183-2cad991e1dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a269263c-6d7c-4f0f-a6e3-b967e78fab3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eb3062e-01bd-428f-ba9b-8be36ba8407a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a269263c-6d7c-4f0f-a6e3-b967e78fab3c",
                    "LayerId": "c18b327b-501a-4234-a634-04160f778764"
                }
            ]
        },
        {
            "id": "b4d74f16-5ec2-49d0-9332-3b8d9bd59417",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c221841-b97b-4eb3-9bb6-424b1cc17110",
            "compositeImage": {
                "id": "f8917eca-6dfc-480f-8159-e387287accf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d74f16-5ec2-49d0-9332-3b8d9bd59417",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63bfae98-23b7-410d-859c-c138dcb2f02d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d74f16-5ec2-49d0-9332-3b8d9bd59417",
                    "LayerId": "c18b327b-501a-4234-a634-04160f778764"
                }
            ]
        },
        {
            "id": "aa3d2f13-6eae-4696-9b6c-a040555df326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c221841-b97b-4eb3-9bb6-424b1cc17110",
            "compositeImage": {
                "id": "aeb7c47e-ba09-4d82-a44e-ee80152006bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa3d2f13-6eae-4696-9b6c-a040555df326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e97705d3-2f2e-4fd2-bb59-f7d6398ef1d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa3d2f13-6eae-4696-9b6c-a040555df326",
                    "LayerId": "c18b327b-501a-4234-a634-04160f778764"
                }
            ]
        },
        {
            "id": "cec8f24a-07cb-4b26-85ef-44404a29b5cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c221841-b97b-4eb3-9bb6-424b1cc17110",
            "compositeImage": {
                "id": "f10e4fd2-7db7-444b-a14e-d6f2408161c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cec8f24a-07cb-4b26-85ef-44404a29b5cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a016d766-6c7d-4084-9ff2-d428b061a6f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cec8f24a-07cb-4b26-85ef-44404a29b5cc",
                    "LayerId": "c18b327b-501a-4234-a634-04160f778764"
                }
            ]
        },
        {
            "id": "02402f5d-b751-4c95-a124-c1cc68417a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c221841-b97b-4eb3-9bb6-424b1cc17110",
            "compositeImage": {
                "id": "d01c66f5-7c1c-4030-a0c4-6bcd69c6fe03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02402f5d-b751-4c95-a124-c1cc68417a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eed57da-90f7-4391-ab28-6934d9b3d9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02402f5d-b751-4c95-a124-c1cc68417a5f",
                    "LayerId": "c18b327b-501a-4234-a634-04160f778764"
                }
            ]
        },
        {
            "id": "68868b75-0586-40b9-87c5-c540d4cb1c33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c221841-b97b-4eb3-9bb6-424b1cc17110",
            "compositeImage": {
                "id": "29712e03-f9e7-4fec-861e-f84147592d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68868b75-0586-40b9-87c5-c540d4cb1c33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f900fb2-2156-4648-906a-67ee0803cabb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68868b75-0586-40b9-87c5-c540d4cb1c33",
                    "LayerId": "c18b327b-501a-4234-a634-04160f778764"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 188,
    "layers": [
        {
            "id": "c18b327b-501a-4234-a634-04160f778764",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c221841-b97b-4eb3-9bb6-424b1cc17110",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 50
}