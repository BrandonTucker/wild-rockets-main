{
    "id": "dd06240c-cefd-488b-b588-b18b0e336e18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange38buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4ac8a89-c8ae-41b8-8278-1c0b7b7a4c1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "compositeImage": {
                "id": "dee9ad9d-08d5-47fc-8402-9d8661b47252",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4ac8a89-c8ae-41b8-8278-1c0b7b7a4c1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12067b8c-d156-4821-855c-c474d127c978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4ac8a89-c8ae-41b8-8278-1c0b7b7a4c1c",
                    "LayerId": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b"
                }
            ]
        },
        {
            "id": "0471f06c-86eb-4bfd-ae9e-2383f1a3398c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "compositeImage": {
                "id": "e699fbfa-72de-4f30-9c65-5878468cabe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0471f06c-86eb-4bfd-ae9e-2383f1a3398c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96d0c4b1-e524-41d1-9c41-73f78abfdddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0471f06c-86eb-4bfd-ae9e-2383f1a3398c",
                    "LayerId": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b"
                }
            ]
        },
        {
            "id": "282cdd1a-358a-424a-bbf8-dc30f4540b67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "compositeImage": {
                "id": "35c755f1-5177-4595-9c4e-5f12e2048a2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "282cdd1a-358a-424a-bbf8-dc30f4540b67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d255b0ba-c9de-4ac1-8d82-67e00ea9306a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "282cdd1a-358a-424a-bbf8-dc30f4540b67",
                    "LayerId": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b"
                }
            ]
        },
        {
            "id": "282344cb-dcca-432b-aa5c-a4d9c733079e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "compositeImage": {
                "id": "1e143450-2400-4a3d-a00d-e8c10f88e84f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "282344cb-dcca-432b-aa5c-a4d9c733079e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6abbcbe1-a36e-4c99-abf6-98413fdccf89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "282344cb-dcca-432b-aa5c-a4d9c733079e",
                    "LayerId": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b"
                }
            ]
        },
        {
            "id": "a84dc2a4-db94-4984-8572-d5927dfb638b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "compositeImage": {
                "id": "d6d3fcbb-f036-4d83-9a8a-009e9536a481",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a84dc2a4-db94-4984-8572-d5927dfb638b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e27890f3-d2c8-4c1b-9275-c1c71470b330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a84dc2a4-db94-4984-8572-d5927dfb638b",
                    "LayerId": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b"
                }
            ]
        },
        {
            "id": "ec185783-aca1-4beb-80e2-bd57f31f51d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "compositeImage": {
                "id": "e478e2ad-16cf-4ccd-a13a-b64280e01b72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec185783-aca1-4beb-80e2-bd57f31f51d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e9bdbcb-0671-405e-979a-03d1d00a4034",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec185783-aca1-4beb-80e2-bd57f31f51d2",
                    "LayerId": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b"
                }
            ]
        },
        {
            "id": "c93e951a-fdbe-44bc-aa36-e3693a86a58a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "compositeImage": {
                "id": "60b730e5-44a7-40f7-8d69-2843e7216cd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c93e951a-fdbe-44bc-aa36-e3693a86a58a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69b14b74-ff15-4f1d-9dcb-4ffbffd94521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c93e951a-fdbe-44bc-aa36-e3693a86a58a",
                    "LayerId": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b"
                }
            ]
        },
        {
            "id": "82f8514b-c77b-4eab-b87e-b6b847c730c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "compositeImage": {
                "id": "784879f2-28f8-4f5e-8ebe-e839cda8f0d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f8514b-c77b-4eab-b87e-b6b847c730c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd15d8ee-b656-4fa2-a959-ae4ae4845e9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f8514b-c77b-4eab-b87e-b6b847c730c7",
                    "LayerId": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b"
                }
            ]
        },
        {
            "id": "a6c1f6f6-7707-47ec-b2c6-24cd5114d5a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "compositeImage": {
                "id": "465d4ced-3ccf-42d9-b5a2-22c9aeda1e20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6c1f6f6-7707-47ec-b2c6-24cd5114d5a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbb02d09-0178-44a6-a2dd-4e1af75d7f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6c1f6f6-7707-47ec-b2c6-24cd5114d5a1",
                    "LayerId": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b"
                }
            ]
        },
        {
            "id": "ab77dd8c-538f-4bf7-8073-ead951fe225f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "compositeImage": {
                "id": "4b58437f-6e4e-4001-9837-e3986904c96c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab77dd8c-538f-4bf7-8073-ead951fe225f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b297d47-80ce-4550-a7d9-75e4147fcad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab77dd8c-538f-4bf7-8073-ead951fe225f",
                    "LayerId": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "f6d33cfd-f0c8-4ad6-b82c-676a9f18c32b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd06240c-cefd-488b-b588-b18b0e336e18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 52
}