{
    "id": "33ebd159-59e3-4a84-9bd3-391caf878ff2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship38",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 53,
    "bbox_right": 78,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1eee74c-533e-40fa-8e03-7438c2561053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ebd159-59e3-4a84-9bd3-391caf878ff2",
            "compositeImage": {
                "id": "e73f7d61-e3fb-423e-9a05-b377b3ed6312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1eee74c-533e-40fa-8e03-7438c2561053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86f496ea-f1c3-42f6-80dd-b6a7c152139d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1eee74c-533e-40fa-8e03-7438c2561053",
                    "LayerId": "be6bdd3e-6c62-4715-a765-0e1a94d65517"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 130,
    "layers": [
        {
            "id": "be6bdd3e-6c62-4715-a765-0e1a94d65517",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33ebd159-59e3-4a84-9bd3-391caf878ff2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 66,
    "yorig": 40
}