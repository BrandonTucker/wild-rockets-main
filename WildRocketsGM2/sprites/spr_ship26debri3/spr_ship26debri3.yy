{
    "id": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship26debri3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6deac3c4-bf81-4ade-85fd-6708a1276e75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "659b2336-e2ca-4080-a3ac-6c3cc7993551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6deac3c4-bf81-4ade-85fd-6708a1276e75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "012dee5c-c952-40d9-896f-d8a0721ada99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6deac3c4-bf81-4ade-85fd-6708a1276e75",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "cab294aa-74a7-4faa-8a42-3efa012f3206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "9b8d699b-c7c4-42eb-93e9-b2a35a43bff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cab294aa-74a7-4faa-8a42-3efa012f3206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f455526b-3c4a-4c24-8cd1-bae6522efc54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cab294aa-74a7-4faa-8a42-3efa012f3206",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "0dbbf18d-17f7-49ef-ac00-a02de8cb6f4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "4339f3fc-a284-40b9-9132-fe83dfc2c91f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dbbf18d-17f7-49ef-ac00-a02de8cb6f4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec73b7d9-7dd1-4fde-b97b-337038640e3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dbbf18d-17f7-49ef-ac00-a02de8cb6f4d",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "ccf29a0b-501b-4045-97a2-51f38f157fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "4c134f74-7103-45f7-9ff6-c1d57b2e148c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf29a0b-501b-4045-97a2-51f38f157fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d97b85b5-ec38-495b-a24a-7359ad34b8bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf29a0b-501b-4045-97a2-51f38f157fb6",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "5b78f538-9445-45d7-83a9-2041bbaf96c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "f5a4bdae-0904-4464-80d7-f8f08dc9a271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b78f538-9445-45d7-83a9-2041bbaf96c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b03617-cd7b-444d-bc6d-f6c2f80e04ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b78f538-9445-45d7-83a9-2041bbaf96c9",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "c72c2365-9a31-49a6-bcc5-5e8a9a2dee97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "59aacad8-88cc-42d2-b06f-27bf31b8119b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c72c2365-9a31-49a6-bcc5-5e8a9a2dee97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d08266db-d825-4ae4-a7e6-baf3a7ccaeaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c72c2365-9a31-49a6-bcc5-5e8a9a2dee97",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "c039e734-5b25-4548-b438-cce1feae285f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "4748f947-52cf-4b03-a951-f79578404740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c039e734-5b25-4548-b438-cce1feae285f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff47e5db-f2cb-42ba-9a9a-7a83815f9984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c039e734-5b25-4548-b438-cce1feae285f",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "61cda177-9fef-477b-956d-53f6961ad454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "3c116ded-9e61-4648-8f8d-f39589cc96ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61cda177-9fef-477b-956d-53f6961ad454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "310a0142-8a21-4fb7-a962-7d430e367494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61cda177-9fef-477b-956d-53f6961ad454",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "6c10a712-13fd-43fa-b7ab-3258c7a061e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "e7170b4d-48ac-41f9-afd7-df84dd59a211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c10a712-13fd-43fa-b7ab-3258c7a061e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40c4841d-2ea1-4e58-8393-85647135bac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c10a712-13fd-43fa-b7ab-3258c7a061e1",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "61ea5335-6b59-4b39-90ff-3e79803ab379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "6cd87772-a172-4bbd-a2e1-288bb3d7bc7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61ea5335-6b59-4b39-90ff-3e79803ab379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a039137-89a8-4128-a0fa-0c1771f9ae6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61ea5335-6b59-4b39-90ff-3e79803ab379",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "8712a0a9-b1e0-41df-9ae8-63e94edf3389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "ca1b97c0-abf4-4e0d-a857-9b59f982194b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8712a0a9-b1e0-41df-9ae8-63e94edf3389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0de8b2f-99a7-4fc0-8a7a-223fc82d4838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8712a0a9-b1e0-41df-9ae8-63e94edf3389",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "81d5234a-7214-4c60-bf87-fa5213d54ea7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "feb1ad5d-a2cf-4ea5-a861-7b7f22708f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81d5234a-7214-4c60-bf87-fa5213d54ea7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f25c60cf-bc42-44a4-bbde-d6b78750c404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d5234a-7214-4c60-bf87-fa5213d54ea7",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "1e882f6f-7a5a-4784-b485-2fbb03d0b160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "1672aa99-6e0c-4e4c-81e9-5f232fd7c757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e882f6f-7a5a-4784-b485-2fbb03d0b160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f85077c5-13c3-4aa9-b6b1-a7aa16e2129b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e882f6f-7a5a-4784-b485-2fbb03d0b160",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "54ecbf6a-430f-4872-b01f-a7f2df561ee0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "836148d8-bc33-44a7-872d-a4495fe37c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54ecbf6a-430f-4872-b01f-a7f2df561ee0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d23a2a-02dd-4848-966a-58d50024c863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54ecbf6a-430f-4872-b01f-a7f2df561ee0",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        },
        {
            "id": "63dc8ae0-348a-4956-84cf-d01feff1268c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "compositeImage": {
                "id": "d6ebac38-6fd9-4add-beb9-aec28531e8c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63dc8ae0-348a-4956-84cf-d01feff1268c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89cede04-7171-4713-a592-a8d2ec6a3f2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63dc8ae0-348a-4956-84cf-d01feff1268c",
                    "LayerId": "fc43e113-9190-4da6-8b70-82217fbf3f14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "fc43e113-9190-4da6-8b70-82217fbf3f14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d07a6663-8f75-4b6c-8481-ffa75ddbe62c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 43,
    "xorig": 22,
    "yorig": 14
}