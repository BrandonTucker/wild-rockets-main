{
    "id": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite55small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 14,
    "bbox_right": 25,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5663afc-88dc-4f98-9c9a-b888fc5ac098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "68b25d67-9320-46cc-9ecb-35da547cf5da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5663afc-88dc-4f98-9c9a-b888fc5ac098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1cc42ab-2916-4557-a398-7ee2d0c18c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5663afc-88dc-4f98-9c9a-b888fc5ac098",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "71464e4b-ffd2-488d-bd51-1ec08db78d38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "e5c2c789-fca6-4fb7-ae32-ce59e5c22a88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71464e4b-ffd2-488d-bd51-1ec08db78d38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9e4aff0-9cd2-441d-9e2e-860501094cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71464e4b-ffd2-488d-bd51-1ec08db78d38",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "6d5dea2d-500f-4810-ab3b-974439f120b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "188fb418-a5d5-4296-9356-bc093607dbe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d5dea2d-500f-4810-ab3b-974439f120b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e69fb5b-ce18-4382-b548-5b26f9e8b486",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d5dea2d-500f-4810-ab3b-974439f120b4",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "61d17bc9-94e2-472e-983c-b8497a428a7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "8fd7f7ef-dad0-426c-bd76-67e96001b061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61d17bc9-94e2-472e-983c-b8497a428a7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d89600ad-1a84-4064-8bce-dbb8be12d5fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61d17bc9-94e2-472e-983c-b8497a428a7a",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "006e0d8b-4e53-4e89-81e8-9ea531536460",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "1f461a6a-0a27-40ee-aad2-5480a8fda90f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "006e0d8b-4e53-4e89-81e8-9ea531536460",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "910cc550-95bd-4af0-ada2-16fd270eacea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "006e0d8b-4e53-4e89-81e8-9ea531536460",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "36af992e-a745-4973-8299-83fcaaacd557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "9632cad5-1fb9-4cd1-81f6-be76ec698a71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36af992e-a745-4973-8299-83fcaaacd557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e4654a6-cbfb-4da7-ae9d-af9b5bec6748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36af992e-a745-4973-8299-83fcaaacd557",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "aabdc4e2-7fcc-47af-bb72-e0ce7da7d2b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "0f3e4711-99f3-49df-943e-991082885788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aabdc4e2-7fcc-47af-bb72-e0ce7da7d2b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81927d4d-0bfc-4a24-ab15-ba0e6c86d511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aabdc4e2-7fcc-47af-bb72-e0ce7da7d2b3",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "dae08281-9062-41f6-811e-21f250440e14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "328316a0-b62d-4666-84d1-dab3ffa156c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dae08281-9062-41f6-811e-21f250440e14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78543f5d-f664-4d0f-898c-d6d5125c2fee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dae08281-9062-41f6-811e-21f250440e14",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "58dcd2a6-13e0-494a-a767-e7a80089aea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "20fb6179-5c24-46a0-8eb8-63ad590a8585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58dcd2a6-13e0-494a-a767-e7a80089aea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "505ab19c-c77a-462d-b2fa-7240e83fac64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58dcd2a6-13e0-494a-a767-e7a80089aea9",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "a4fe48ec-453b-4643-b5c5-1e0cd484c5c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "fd74bf58-bfd5-4898-a668-65daa9d609eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4fe48ec-453b-4643-b5c5-1e0cd484c5c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df23c2f5-a5fd-4720-80d4-b5633ba4c005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4fe48ec-453b-4643-b5c5-1e0cd484c5c2",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "a410728a-382d-4b4c-b9fe-44858dd07fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "ea4ca999-608a-489d-8262-5d2e386cbbad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a410728a-382d-4b4c-b9fe-44858dd07fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8806dea8-ad0e-4cb7-b6eb-8393b041f0ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a410728a-382d-4b4c-b9fe-44858dd07fc9",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "6338cfb5-56d5-4ec2-b38b-b2052c23c26c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "6429fbfe-b767-4cc2-b9ef-048e6a7e6bf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6338cfb5-56d5-4ec2-b38b-b2052c23c26c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1db7b00c-ec5e-4aba-b379-04f0a59b21e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6338cfb5-56d5-4ec2-b38b-b2052c23c26c",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "84b98f0d-8cfd-4688-bf22-03a2cd6b97c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "aa462f5a-b877-4ce1-9080-139d3da6c325",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b98f0d-8cfd-4688-bf22-03a2cd6b97c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9029f47-9259-4d2c-a16c-e636daca7b70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b98f0d-8cfd-4688-bf22-03a2cd6b97c7",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "9548c386-d364-4be2-8f2f-91e1ea59c258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "df864b8f-e123-4cf9-b311-661b8b67c62a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9548c386-d364-4be2-8f2f-91e1ea59c258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0345b43-8819-4560-ae41-7c6cbdfd2443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9548c386-d364-4be2-8f2f-91e1ea59c258",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "47de93cb-0d56-45a8-b4d2-ab58b9769b0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "c61fd099-0f69-4324-b2e5-9c3245b3a73f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47de93cb-0d56-45a8-b4d2-ab58b9769b0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c636a340-ac1a-410b-a834-33ee86d8cf2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47de93cb-0d56-45a8-b4d2-ab58b9769b0a",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "b456e863-00d7-4d6a-a4fb-4d84e1cb2ff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "ba222a37-45e4-4aac-8eb8-f29878ee78a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b456e863-00d7-4d6a-a4fb-4d84e1cb2ff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14fc0187-5c58-4c72-9c73-365997aa7ea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b456e863-00d7-4d6a-a4fb-4d84e1cb2ff6",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "2ad9a649-7943-47db-8531-24ee084765d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "bc562e65-6b8d-43dd-9fa7-b74795b22248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ad9a649-7943-47db-8531-24ee084765d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea318af0-2a1a-47a9-ba12-4875cc73fe32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ad9a649-7943-47db-8531-24ee084765d0",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "f7e0bc13-6976-45e3-b6a2-27f56b091db1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "e0b1af92-2500-49fc-9f40-2751b51cb42b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7e0bc13-6976-45e3-b6a2-27f56b091db1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13cea973-f87a-4ed8-882e-87ee7362f9c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7e0bc13-6976-45e3-b6a2-27f56b091db1",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "43edbbb9-9577-4d8a-b15b-095b947c7b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "810dcc30-d1c3-4b3c-a5db-1662171874d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43edbbb9-9577-4d8a-b15b-095b947c7b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1579664e-c168-44d9-b8d9-77ed4e329558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43edbbb9-9577-4d8a-b15b-095b947c7b83",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        },
        {
            "id": "785690a8-096b-4a71-9242-5774515eada8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "compositeImage": {
                "id": "2d6a3f50-21ef-4e7a-9a24-83a5317a8518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "785690a8-096b-4a71-9242-5774515eada8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb20e0fe-6b53-496c-9ba8-291143f7bed7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "785690a8-096b-4a71-9242-5774515eada8",
                    "LayerId": "341e273d-478c-4f36-84a0-bf3b5517c802"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "341e273d-478c-4f36-84a0-bf3b5517c802",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25a3eefe-e4f7-4b74-8ee0-b383ad80f860",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 18
}