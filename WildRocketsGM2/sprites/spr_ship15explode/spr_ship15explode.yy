{
    "id": "5f95dd23-70ca-42d4-9345-4e8cab9531dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship15explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 34,
    "bbox_right": 59,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9de4a115-51e1-4e79-97de-1c41baaa8b9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f95dd23-70ca-42d4-9345-4e8cab9531dc",
            "compositeImage": {
                "id": "30d6fca3-1a89-49c4-a7b1-93d72519e5e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9de4a115-51e1-4e79-97de-1c41baaa8b9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b917d45-ac0e-4b37-83bf-c06f4d3805f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9de4a115-51e1-4e79-97de-1c41baaa8b9c",
                    "LayerId": "383f2bd7-7a00-4872-8df3-af8b776a8cb7"
                }
            ]
        },
        {
            "id": "0a2f0a18-e475-4aca-ac10-759a73632795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f95dd23-70ca-42d4-9345-4e8cab9531dc",
            "compositeImage": {
                "id": "f8fcf577-6ccd-44f4-a0d3-ab6e68afe01e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a2f0a18-e475-4aca-ac10-759a73632795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e31e3b9c-5474-4af1-923b-9bd44a7271c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a2f0a18-e475-4aca-ac10-759a73632795",
                    "LayerId": "383f2bd7-7a00-4872-8df3-af8b776a8cb7"
                }
            ]
        },
        {
            "id": "ea54292e-c442-4265-9909-f0593f81c58c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f95dd23-70ca-42d4-9345-4e8cab9531dc",
            "compositeImage": {
                "id": "f4c3af5e-16e2-4405-90d1-493019511ca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea54292e-c442-4265-9909-f0593f81c58c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "852d3ea5-3005-49eb-984c-25390e1c8b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea54292e-c442-4265-9909-f0593f81c58c",
                    "LayerId": "383f2bd7-7a00-4872-8df3-af8b776a8cb7"
                }
            ]
        },
        {
            "id": "56ecb4b3-df8e-47ca-98dd-5cd01b0e7a59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f95dd23-70ca-42d4-9345-4e8cab9531dc",
            "compositeImage": {
                "id": "ef1bcd30-3410-4cc4-bb5c-a561bfc9d803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56ecb4b3-df8e-47ca-98dd-5cd01b0e7a59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abbe25f8-913b-4493-9707-7cb03c130945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56ecb4b3-df8e-47ca-98dd-5cd01b0e7a59",
                    "LayerId": "383f2bd7-7a00-4872-8df3-af8b776a8cb7"
                }
            ]
        },
        {
            "id": "3a6422b6-3e55-478a-807c-868aea1edf86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f95dd23-70ca-42d4-9345-4e8cab9531dc",
            "compositeImage": {
                "id": "99f56bfa-7577-4d3b-8575-a92ff2fa3f50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a6422b6-3e55-478a-807c-868aea1edf86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f637075c-cc7b-4c9b-9a2c-0604f4b0bd8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a6422b6-3e55-478a-807c-868aea1edf86",
                    "LayerId": "383f2bd7-7a00-4872-8df3-af8b776a8cb7"
                }
            ]
        },
        {
            "id": "ffb22178-f79c-490e-ab4c-d50058fc7387",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f95dd23-70ca-42d4-9345-4e8cab9531dc",
            "compositeImage": {
                "id": "a70ee3b4-c5b9-4aae-83b6-eaf87e7b6bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb22178-f79c-490e-ab4c-d50058fc7387",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa0e9983-bb57-41a2-b15b-0c90b8fd661a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb22178-f79c-490e-ab4c-d50058fc7387",
                    "LayerId": "383f2bd7-7a00-4872-8df3-af8b776a8cb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "383f2bd7-7a00-4872-8df3-af8b776a8cb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f95dd23-70ca-42d4-9345-4e8cab9531dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 46,
    "yorig": 41
}