{
    "id": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rareship6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 133,
    "bbox_left": 34,
    "bbox_right": 90,
    "bbox_top": 50,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f9f3221-a973-4759-bdcc-ba15b3da4353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "compositeImage": {
                "id": "1836d224-5467-4881-b859-96ec7b424305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f9f3221-a973-4759-bdcc-ba15b3da4353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0206d962-4aa0-4500-bc89-b21717c389b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f9f3221-a973-4759-bdcc-ba15b3da4353",
                    "LayerId": "5a546109-09d8-4d4d-8dca-ccb5acd78d70"
                }
            ]
        },
        {
            "id": "fa72ef19-3abc-4065-a644-0c1bf9209c19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "compositeImage": {
                "id": "0b99cf82-6c55-4a31-8655-a3e91c78318d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa72ef19-3abc-4065-a644-0c1bf9209c19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef63ca2c-e6aa-4ca5-97c4-0932fd344898",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa72ef19-3abc-4065-a644-0c1bf9209c19",
                    "LayerId": "5a546109-09d8-4d4d-8dca-ccb5acd78d70"
                }
            ]
        },
        {
            "id": "6e705274-4546-4ea7-9098-03881fbe0c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "compositeImage": {
                "id": "f4f82bc2-e444-4d64-b704-cc8e95373d6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e705274-4546-4ea7-9098-03881fbe0c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c2fe10c-2c57-4fe7-805b-520ab20d84c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e705274-4546-4ea7-9098-03881fbe0c2e",
                    "LayerId": "5a546109-09d8-4d4d-8dca-ccb5acd78d70"
                }
            ]
        },
        {
            "id": "7651a574-00e1-405f-94ce-61d609c1299c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "compositeImage": {
                "id": "d1087082-3e2c-4c48-8a15-429617495bdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7651a574-00e1-405f-94ce-61d609c1299c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70f4bbe4-2f39-4385-a2d7-faa52266154b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7651a574-00e1-405f-94ce-61d609c1299c",
                    "LayerId": "5a546109-09d8-4d4d-8dca-ccb5acd78d70"
                }
            ]
        },
        {
            "id": "e63a3719-0354-4525-aeee-f8bbd83c823d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "compositeImage": {
                "id": "0a0ed394-ef09-4197-ab9b-ad3f76bc8c38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e63a3719-0354-4525-aeee-f8bbd83c823d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3375b9ca-c79f-459c-abc4-4b6b6c1b1b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e63a3719-0354-4525-aeee-f8bbd83c823d",
                    "LayerId": "5a546109-09d8-4d4d-8dca-ccb5acd78d70"
                }
            ]
        },
        {
            "id": "2362ae34-28ab-40f4-add1-3060dc203f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "compositeImage": {
                "id": "ad993a55-87c7-4b85-a5ea-7ddade1f9370",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2362ae34-28ab-40f4-add1-3060dc203f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09f47bee-58d6-4b2c-8bd5-d521735fc720",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2362ae34-28ab-40f4-add1-3060dc203f04",
                    "LayerId": "5a546109-09d8-4d4d-8dca-ccb5acd78d70"
                }
            ]
        },
        {
            "id": "9721a722-b2a3-413e-9ad4-1a5230cf66d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "compositeImage": {
                "id": "0571d774-f682-42f7-b071-892faed105e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9721a722-b2a3-413e-9ad4-1a5230cf66d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bedb6beb-d027-4aea-b955-14d512e1c17a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9721a722-b2a3-413e-9ad4-1a5230cf66d3",
                    "LayerId": "5a546109-09d8-4d4d-8dca-ccb5acd78d70"
                }
            ]
        },
        {
            "id": "2e502418-4ae4-4bce-9a03-1ccaeb8ad68f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "compositeImage": {
                "id": "11d11d45-9895-4b12-a9dc-fd1527dbc9a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e502418-4ae4-4bce-9a03-1ccaeb8ad68f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e4299a-d9c5-4dd9-9f9d-28088301a3e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e502418-4ae4-4bce-9a03-1ccaeb8ad68f",
                    "LayerId": "5a546109-09d8-4d4d-8dca-ccb5acd78d70"
                }
            ]
        },
        {
            "id": "a4810732-68b3-49ca-882d-28b3e41a0d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "compositeImage": {
                "id": "660c5767-7a23-4909-86b2-35e95aed713f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4810732-68b3-49ca-882d-28b3e41a0d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccbbe411-0a23-49ca-8218-8f870c248f23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4810732-68b3-49ca-882d-28b3e41a0d50",
                    "LayerId": "5a546109-09d8-4d4d-8dca-ccb5acd78d70"
                }
            ]
        },
        {
            "id": "35719974-5498-4bcb-b221-6dd8e57c4996",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "compositeImage": {
                "id": "5f0f74a3-497d-4ed1-9160-86712fff8ed8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35719974-5498-4bcb-b221-6dd8e57c4996",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a451bd4-cb3d-466c-bc44-078f09fe5b15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35719974-5498-4bcb-b221-6dd8e57c4996",
                    "LayerId": "5a546109-09d8-4d4d-8dca-ccb5acd78d70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "5a546109-09d8-4d4d-8dca-ccb5acd78d70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83b0ee1e-9f84-49eb-be0e-ecf30b4497fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 62,
    "yorig": 91
}