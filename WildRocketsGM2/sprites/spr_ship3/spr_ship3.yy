{
    "id": "cc91abe9-c938-4a22-9cfa-d9178ab181a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 26,
    "bbox_right": 51,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1c3b057-8391-403b-a1ea-266af1a7e30b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc91abe9-c938-4a22-9cfa-d9178ab181a4",
            "compositeImage": {
                "id": "859b063b-af86-460a-bb8c-baa95c2cab89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1c3b057-8391-403b-a1ea-266af1a7e30b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e80c4f3-6a66-4c18-b343-c95710486a7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1c3b057-8391-403b-a1ea-266af1a7e30b",
                    "LayerId": "cb068bb7-ee8c-4165-8adc-f2fe744ce2c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 118,
    "layers": [
        {
            "id": "cb068bb7-ee8c-4165-8adc-f2fe744ce2c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc91abe9-c938-4a22-9cfa-d9178ab181a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 38,
    "yorig": 34
}