{
    "id": "85cc9422-9db9-4be6-a11b-abceed2cce7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship24explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 25,
    "bbox_right": 50,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92b4836b-df6d-4e74-885c-932b93cb24e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85cc9422-9db9-4be6-a11b-abceed2cce7d",
            "compositeImage": {
                "id": "e3f06432-b40a-4a0b-81a8-7faaab67f71c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92b4836b-df6d-4e74-885c-932b93cb24e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a997f0d-a7b2-4fbe-94bb-e2a46a7dc408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92b4836b-df6d-4e74-885c-932b93cb24e9",
                    "LayerId": "136e7e62-80e8-4953-958e-267eb89f01ea"
                }
            ]
        },
        {
            "id": "40d01651-5129-46c0-887b-7e8100980bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85cc9422-9db9-4be6-a11b-abceed2cce7d",
            "compositeImage": {
                "id": "ac8020fd-8ec3-4de6-921f-e79531c8ee87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40d01651-5129-46c0-887b-7e8100980bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62cc5d44-36e4-4418-a450-e8a82fb753f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40d01651-5129-46c0-887b-7e8100980bd8",
                    "LayerId": "136e7e62-80e8-4953-958e-267eb89f01ea"
                }
            ]
        },
        {
            "id": "f9fb2295-3f23-4c0a-b8b7-573c267829d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85cc9422-9db9-4be6-a11b-abceed2cce7d",
            "compositeImage": {
                "id": "6c95311c-5053-4274-9604-b28831d58396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9fb2295-3f23-4c0a-b8b7-573c267829d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39e05803-0e9b-4824-a962-fd8137564cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9fb2295-3f23-4c0a-b8b7-573c267829d2",
                    "LayerId": "136e7e62-80e8-4953-958e-267eb89f01ea"
                }
            ]
        },
        {
            "id": "956c6142-c2ab-4387-b616-e64facf16fa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85cc9422-9db9-4be6-a11b-abceed2cce7d",
            "compositeImage": {
                "id": "4a492813-ca76-460f-af7e-41f1eb26d3c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "956c6142-c2ab-4387-b616-e64facf16fa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeea4501-9479-433a-a093-0b7057328ec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "956c6142-c2ab-4387-b616-e64facf16fa7",
                    "LayerId": "136e7e62-80e8-4953-958e-267eb89f01ea"
                }
            ]
        },
        {
            "id": "af4ab1da-ac64-4568-9f0e-2ceb7d76ba91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85cc9422-9db9-4be6-a11b-abceed2cce7d",
            "compositeImage": {
                "id": "ba3a8afa-ea6a-4c19-8c74-09a0f009514e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af4ab1da-ac64-4568-9f0e-2ceb7d76ba91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2cf234f-e324-4b99-8fbf-087e11e59f69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af4ab1da-ac64-4568-9f0e-2ceb7d76ba91",
                    "LayerId": "136e7e62-80e8-4953-958e-267eb89f01ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 187,
    "layers": [
        {
            "id": "136e7e62-80e8-4953-958e-267eb89f01ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85cc9422-9db9-4be6-a11b-abceed2cce7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 64
}