{
    "id": "6f66b6c3-303a-4139-9858-dfe2434a1aa4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buttonglow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 0,
    "bbox_right": 120,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f49f3986-bbbe-4b99-a9ee-f1efd19725d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f66b6c3-303a-4139-9858-dfe2434a1aa4",
            "compositeImage": {
                "id": "bfa4d6c8-75e2-409b-ad68-95be238bd7ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f49f3986-bbbe-4b99-a9ee-f1efd19725d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "612dce1b-bdab-4a6b-a120-1bb39ca55ac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f49f3986-bbbe-4b99-a9ee-f1efd19725d8",
                    "LayerId": "c56ed528-2043-4a7b-85d8-886ec25d98e6"
                }
            ]
        },
        {
            "id": "780b41f3-fac8-4cce-bce8-1a37aa8f4720",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f66b6c3-303a-4139-9858-dfe2434a1aa4",
            "compositeImage": {
                "id": "b94166ea-d910-4c1b-aa3b-7f5e330bcba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "780b41f3-fac8-4cce-bce8-1a37aa8f4720",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9711633f-38ac-42c4-aba2-c4d2b551e642",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "780b41f3-fac8-4cce-bce8-1a37aa8f4720",
                    "LayerId": "c56ed528-2043-4a7b-85d8-886ec25d98e6"
                }
            ]
        },
        {
            "id": "a69546bf-d462-49bc-bf1c-3acb4c6e8979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f66b6c3-303a-4139-9858-dfe2434a1aa4",
            "compositeImage": {
                "id": "e52677d9-bf06-4dfc-b690-0deaf9ba6d7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a69546bf-d462-49bc-bf1c-3acb4c6e8979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccb90745-bf99-42bd-b09b-20cde72fdfc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a69546bf-d462-49bc-bf1c-3acb4c6e8979",
                    "LayerId": "c56ed528-2043-4a7b-85d8-886ec25d98e6"
                }
            ]
        },
        {
            "id": "7c7c2f65-6923-457c-8311-d2173003536a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f66b6c3-303a-4139-9858-dfe2434a1aa4",
            "compositeImage": {
                "id": "17f4aaf7-dfee-4b2d-ab64-e641cab3cd39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c7c2f65-6923-457c-8311-d2173003536a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3916ecd0-8b55-4db5-bf83-87f6bfbcfe96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c7c2f65-6923-457c-8311-d2173003536a",
                    "LayerId": "c56ed528-2043-4a7b-85d8-886ec25d98e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "c56ed528-2043-4a7b-85d8-886ec25d98e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f66b6c3-303a-4139-9858-dfe2434a1aa4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 121,
    "xorig": 60,
    "yorig": 57
}