{
    "id": "ec95b60b-45c2-449f-80e2-1020a594fdab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage2button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f79fea0-221b-4e04-9cb0-7c2c77dd2649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec95b60b-45c2-449f-80e2-1020a594fdab",
            "compositeImage": {
                "id": "5f6b697d-a406-4407-92a2-be4f54b7cd4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f79fea0-221b-4e04-9cb0-7c2c77dd2649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9b04479-de25-4919-94cb-94d38cd0b3ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f79fea0-221b-4e04-9cb0-7c2c77dd2649",
                    "LayerId": "428a231f-4a09-412c-abec-3fe683faf34d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "428a231f-4a09-412c-abec-3fe683faf34d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec95b60b-45c2-449f-80e2-1020a594fdab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}