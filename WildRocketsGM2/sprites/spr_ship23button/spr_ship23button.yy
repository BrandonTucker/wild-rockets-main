{
    "id": "baebe8ff-9328-44f5-b69a-6b49c16e3409",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship23button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b481ad73-0744-482d-8acc-96d3e5d0f8c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baebe8ff-9328-44f5-b69a-6b49c16e3409",
            "compositeImage": {
                "id": "e99cda0a-4874-4c37-bcd8-e0b2bf83b309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b481ad73-0744-482d-8acc-96d3e5d0f8c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8e62c8f-2d83-43f9-b12a-d694c9776dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b481ad73-0744-482d-8acc-96d3e5d0f8c8",
                    "LayerId": "1272d32b-2b88-478a-8794-6076ee76ad4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1272d32b-2b88-478a-8794-6076ee76ad4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "baebe8ff-9328-44f5-b69a-6b49c16e3409",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}