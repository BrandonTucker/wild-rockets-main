{
    "id": "746f0ca6-62b0-43e3-af8c-80762a087846",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite104",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 0,
    "bbox_right": 323,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8675bc9a-91eb-4453-bdd5-22d125e20d27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "746f0ca6-62b0-43e3-af8c-80762a087846",
            "compositeImage": {
                "id": "92bb6807-c7af-4a4f-93b3-dc072452e415",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8675bc9a-91eb-4453-bdd5-22d125e20d27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09a19b2b-0c05-4b5c-a6da-725f81d572b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8675bc9a-91eb-4453-bdd5-22d125e20d27",
                    "LayerId": "ddb4b72e-5c20-43e7-b5ab-93778645acc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 157,
    "layers": [
        {
            "id": "ddb4b72e-5c20-43e7-b5ab-93778645acc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "746f0ca6-62b0-43e3-af8c-80762a087846",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 324,
    "xorig": 127,
    "yorig": 79
}