{
    "id": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_brokencockpit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 6,
    "bbox_right": 47,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6782a16c-3a10-46c1-a4ad-79ff39f6c2d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "715537c2-8ffa-4b26-ab3f-446509e37560",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6782a16c-3a10-46c1-a4ad-79ff39f6c2d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97c553be-380e-4e95-ba60-f2cead3df08b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6782a16c-3a10-46c1-a4ad-79ff39f6c2d1",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "a7fc426f-c15b-494c-929f-b6ca6ec1ecc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "c3167c43-c085-46e9-b6e2-5a57a61e24fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7fc426f-c15b-494c-929f-b6ca6ec1ecc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c63bfcac-7696-4c0f-b139-dba78d7592ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7fc426f-c15b-494c-929f-b6ca6ec1ecc6",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "474601b3-17b3-4b7a-8839-5f05bbac2f9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "8de59cd2-b2e8-41c9-8e73-04e7a1f4a5d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "474601b3-17b3-4b7a-8839-5f05bbac2f9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a70bc2c7-2fe2-4163-a1b0-2d6c6914dc1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "474601b3-17b3-4b7a-8839-5f05bbac2f9c",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "fda0abc8-93da-43ec-a7e4-183bb524b052",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "4512d30a-04cf-4205-90fc-415bfd1c8868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fda0abc8-93da-43ec-a7e4-183bb524b052",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04dbfbf7-9a2f-474b-abd9-2292b44a8b17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fda0abc8-93da-43ec-a7e4-183bb524b052",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "2daf8304-cb95-483d-b813-b99a13ec38e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "3854be79-fb3c-44b0-b065-5127fd54b506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2daf8304-cb95-483d-b813-b99a13ec38e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83669e2a-2af4-4094-9552-25042000603e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2daf8304-cb95-483d-b813-b99a13ec38e1",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "1ba70567-4a7f-4395-9fb9-c5936cfa5e90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "def122af-5025-4bed-89fd-df0a8e0d0379",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ba70567-4a7f-4395-9fb9-c5936cfa5e90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c770e78-722e-467c-bacc-53dc9aaa42f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ba70567-4a7f-4395-9fb9-c5936cfa5e90",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "3ed01dfa-8375-4fae-b0c9-adeb3cdd09f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "93fd327b-6798-4c17-9776-c6e14702eaf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ed01dfa-8375-4fae-b0c9-adeb3cdd09f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc11ba67-be8c-4dad-80d3-8cbe9251496c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ed01dfa-8375-4fae-b0c9-adeb3cdd09f9",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "31b07eae-02a8-4f56-a758-d46ed1951a06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "1353d90a-2ee2-490f-94e3-d58c528b1009",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31b07eae-02a8-4f56-a758-d46ed1951a06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "065da41f-c676-4838-8dc2-582d7f92e030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31b07eae-02a8-4f56-a758-d46ed1951a06",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "a60da178-6d1b-470b-8c3e-788216d98951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "fd0cf8da-6b07-4375-9f12-924c06c51d6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a60da178-6d1b-470b-8c3e-788216d98951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a01167fc-986e-4d03-a019-2b662e7b9cab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a60da178-6d1b-470b-8c3e-788216d98951",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "681740fc-4409-48f0-a94a-d0a650eeb8ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "f69f0a60-4e04-4b3d-aa08-9cbafe92cca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "681740fc-4409-48f0-a94a-d0a650eeb8ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7167260-448d-47d9-bea7-57960f8164ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "681740fc-4409-48f0-a94a-d0a650eeb8ac",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "4c52900b-d6e0-46c4-91cd-6c7e11eb0f0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "c9063432-8ec5-4a2b-97d4-ac9e7e41b622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c52900b-d6e0-46c4-91cd-6c7e11eb0f0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "768912c7-8570-46e0-ae5d-8ba35b936ae4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c52900b-d6e0-46c4-91cd-6c7e11eb0f0c",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "09dd6444-c11f-4cae-8ead-6d1537cf0763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "02c2affc-466b-4dcf-9c5e-ef51c4f7c185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09dd6444-c11f-4cae-8ead-6d1537cf0763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b190dca8-fd0a-41f5-9541-452ba3fc167a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09dd6444-c11f-4cae-8ead-6d1537cf0763",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "19fedb5f-2448-481f-af7d-4fe22ba3d7c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "7631c6c7-20ac-456b-a025-7042b3054eae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19fedb5f-2448-481f-af7d-4fe22ba3d7c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d16ecebe-8807-400e-8b9e-7776cdec0e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19fedb5f-2448-481f-af7d-4fe22ba3d7c7",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "c4e3eee0-bc08-4e68-86d6-3ea9a3a04234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "2200b20b-b381-4e80-ad4c-e97f6035ec33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4e3eee0-bc08-4e68-86d6-3ea9a3a04234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "352906d7-984c-4e68-ae15-17828069f4cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4e3eee0-bc08-4e68-86d6-3ea9a3a04234",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        },
        {
            "id": "adb3e8c3-a869-4e37-a7ea-29dad5f80490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "compositeImage": {
                "id": "cf140ee6-9e6d-4632-be96-cc37604f1d9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adb3e8c3-a869-4e37-a7ea-29dad5f80490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f13dd397-c104-4acc-8b0c-06021b9c46a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adb3e8c3-a869-4e37-a7ea-29dad5f80490",
                    "LayerId": "438e796a-7a3c-4a25-a642-23aad425fd9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "438e796a-7a3c-4a25-a642-23aad425fd9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "050faa2f-3837-4de6-8f15-52d956f9aaf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 0,
    "yorig": -20
}