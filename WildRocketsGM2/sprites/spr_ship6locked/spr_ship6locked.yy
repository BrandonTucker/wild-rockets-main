{
    "id": "6ebb570a-ab2f-48de-82a9-9ee54b6ac0e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship6locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f91314e-355c-4fa8-9a83-87e926d881ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ebb570a-ab2f-48de-82a9-9ee54b6ac0e0",
            "compositeImage": {
                "id": "bf3dff1b-8fe7-4d59-b91e-6ec315562356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f91314e-355c-4fa8-9a83-87e926d881ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bebb5c88-35ed-40a4-9774-186011de4d15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f91314e-355c-4fa8-9a83-87e926d881ec",
                    "LayerId": "16c69906-537a-47f7-b90b-ad4bb34fa96a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "16c69906-537a-47f7-b90b-ad4bb34fa96a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ebb570a-ab2f-48de-82a9-9ee54b6ac0e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}