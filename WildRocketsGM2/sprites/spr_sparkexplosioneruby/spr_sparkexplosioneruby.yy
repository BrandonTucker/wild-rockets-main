{
    "id": "f39edcec-d026-41cc-9ced-6f337c83100b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sparkexplosioneruby",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 272,
    "bbox_left": 3,
    "bbox_right": 210,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9fae3880-66d7-4971-a256-b42c981864c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39edcec-d026-41cc-9ced-6f337c83100b",
            "compositeImage": {
                "id": "daac2681-eb76-4652-b5b6-53c3eccfe5e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fae3880-66d7-4971-a256-b42c981864c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45ebd9a-d898-47dd-86e8-23acc0c22bff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fae3880-66d7-4971-a256-b42c981864c4",
                    "LayerId": "fbfc95ba-3690-448c-bf8c-1d7b52b53f1a"
                }
            ]
        },
        {
            "id": "afb9a787-4228-4ed6-8b86-a7e03d1cfddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39edcec-d026-41cc-9ced-6f337c83100b",
            "compositeImage": {
                "id": "4fc8a978-79f6-484b-a31f-c2fd25207ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb9a787-4228-4ed6-8b86-a7e03d1cfddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "110f6633-c049-4423-90c4-a0d41de02570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb9a787-4228-4ed6-8b86-a7e03d1cfddd",
                    "LayerId": "fbfc95ba-3690-448c-bf8c-1d7b52b53f1a"
                }
            ]
        },
        {
            "id": "3977e044-4da4-4863-a5a5-a4ad026d7ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39edcec-d026-41cc-9ced-6f337c83100b",
            "compositeImage": {
                "id": "b6116775-3ba3-439f-813d-cd11166e2c0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3977e044-4da4-4863-a5a5-a4ad026d7ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda9ee2b-6075-4715-a157-8af6240065cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3977e044-4da4-4863-a5a5-a4ad026d7ade",
                    "LayerId": "fbfc95ba-3690-448c-bf8c-1d7b52b53f1a"
                }
            ]
        },
        {
            "id": "4281418e-e05e-44e8-aed5-073e059af61f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39edcec-d026-41cc-9ced-6f337c83100b",
            "compositeImage": {
                "id": "e85b6097-fb45-470e-a74f-e7d2a957843c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4281418e-e05e-44e8-aed5-073e059af61f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb61dce1-154c-4b3c-bb98-09cf9b0b9455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4281418e-e05e-44e8-aed5-073e059af61f",
                    "LayerId": "fbfc95ba-3690-448c-bf8c-1d7b52b53f1a"
                }
            ]
        },
        {
            "id": "ce5dbc9c-cc9d-42a2-a81c-0920d7a8fee0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f39edcec-d026-41cc-9ced-6f337c83100b",
            "compositeImage": {
                "id": "bbb84133-791e-40f0-91dc-fcf160324d53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce5dbc9c-cc9d-42a2-a81c-0920d7a8fee0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13797812-2066-49f0-84d7-692f040fab87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce5dbc9c-cc9d-42a2-a81c-0920d7a8fee0",
                    "LayerId": "fbfc95ba-3690-448c-bf8c-1d7b52b53f1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "fbfc95ba-3690-448c-bf8c-1d7b52b53f1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f39edcec-d026-41cc-9ced-6f337c83100b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 222,
    "xorig": 111,
    "yorig": 145
}