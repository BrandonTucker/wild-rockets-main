{
    "id": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage10buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dab72cd2-8c52-4ce3-ab7c-f0f43db0752b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "compositeImage": {
                "id": "b6ba8c56-7644-41bd-92f5-f40bd8da3595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dab72cd2-8c52-4ce3-ab7c-f0f43db0752b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44e33694-3beb-4924-a6ad-092273096a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dab72cd2-8c52-4ce3-ab7c-f0f43db0752b",
                    "LayerId": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8"
                }
            ]
        },
        {
            "id": "60a2b8c4-2c13-4f77-be9f-c5de3b3b4db7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "compositeImage": {
                "id": "5ff90a3f-2ce0-4899-bbab-a772ad70d195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a2b8c4-2c13-4f77-be9f-c5de3b3b4db7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c994b56-0b58-4ae2-8d96-a2fd3d51e216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a2b8c4-2c13-4f77-be9f-c5de3b3b4db7",
                    "LayerId": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8"
                }
            ]
        },
        {
            "id": "d0104848-412c-49f4-ab95-a3e3b0589841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "compositeImage": {
                "id": "2e07feff-26c8-46f4-a734-af2f4df580f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0104848-412c-49f4-ab95-a3e3b0589841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21f5ddac-f4d2-4c86-b0ee-12140b697d83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0104848-412c-49f4-ab95-a3e3b0589841",
                    "LayerId": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8"
                }
            ]
        },
        {
            "id": "23d5f6af-14a1-402f-86ba-3b84be6e2f30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "compositeImage": {
                "id": "654b3f89-66df-4f31-8606-ead14d60fc6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23d5f6af-14a1-402f-86ba-3b84be6e2f30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60004931-c522-4bc7-9697-86949aa98041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23d5f6af-14a1-402f-86ba-3b84be6e2f30",
                    "LayerId": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8"
                }
            ]
        },
        {
            "id": "074e2d6a-f7d0-42ea-b045-1e43b17cc271",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "compositeImage": {
                "id": "9ce20867-412e-4e63-a658-6507cc5efd6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "074e2d6a-f7d0-42ea-b045-1e43b17cc271",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfc3066f-3458-4961-92ed-6e15ea95eec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "074e2d6a-f7d0-42ea-b045-1e43b17cc271",
                    "LayerId": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8"
                }
            ]
        },
        {
            "id": "a6760e07-651c-404c-90be-1c0db277b25b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "compositeImage": {
                "id": "57c3e2b9-2425-4f13-9889-5baf7fc89758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6760e07-651c-404c-90be-1c0db277b25b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7cc70a8-f929-4917-9842-b5f7a460d2f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6760e07-651c-404c-90be-1c0db277b25b",
                    "LayerId": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8"
                }
            ]
        },
        {
            "id": "de3b3e28-dc4a-4733-8c11-17cf20b732a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "compositeImage": {
                "id": "c8766a95-4192-4a57-add1-6134c1610ef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de3b3e28-dc4a-4733-8c11-17cf20b732a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfac6401-0519-4a14-b4f9-14491be5f46d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de3b3e28-dc4a-4733-8c11-17cf20b732a1",
                    "LayerId": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8"
                }
            ]
        },
        {
            "id": "cba82cb2-fcfa-4168-b359-f093a98b489b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "compositeImage": {
                "id": "fda34d89-7d09-46bc-a21a-a6b5620b34ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cba82cb2-fcfa-4168-b359-f093a98b489b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4d67ace-a67c-4dcf-a6be-9905324daff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cba82cb2-fcfa-4168-b359-f093a98b489b",
                    "LayerId": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8"
                }
            ]
        },
        {
            "id": "b2923b67-9b05-4909-a064-976981233f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "compositeImage": {
                "id": "c1e6e633-115f-4496-ae43-828d47f1fc65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2923b67-9b05-4909-a064-976981233f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ea5da16-c3c8-4826-82bf-26fc9693e34d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2923b67-9b05-4909-a064-976981233f9b",
                    "LayerId": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8"
                }
            ]
        },
        {
            "id": "31499f33-21d2-46f0-bead-368e9259607d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "compositeImage": {
                "id": "826cf24d-c52e-432e-b670-0ef2033ca3fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31499f33-21d2-46f0-bead-368e9259607d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b15f0c0a-a4c8-4b97-85c5-b1baf78aa157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31499f33-21d2-46f0-bead-368e9259607d",
                    "LayerId": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4b448bbf-e9ec-4ff9-93de-a61d51e747a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98310d9a-a08d-4a28-a7b4-dee917db97a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}