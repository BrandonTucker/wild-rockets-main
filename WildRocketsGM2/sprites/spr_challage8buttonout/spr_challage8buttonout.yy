{
    "id": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage8buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b4c5d80-6707-43b4-9eeb-80f771d4c6ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "compositeImage": {
                "id": "4d298b16-4e61-4a3b-bf78-6f1e09aaaecb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b4c5d80-6707-43b4-9eeb-80f771d4c6ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d4b281-6dbd-4b7e-a56b-244374362ff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b4c5d80-6707-43b4-9eeb-80f771d4c6ad",
                    "LayerId": "07d8b376-620c-4a89-88cb-4c1c767ba712"
                }
            ]
        },
        {
            "id": "b9c14830-a1c0-443f-b8b1-cd8dda92f335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "compositeImage": {
                "id": "0554060e-ab30-4276-b6ed-9e0c7a3bb003",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9c14830-a1c0-443f-b8b1-cd8dda92f335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e332d7b-3181-425f-9d64-10fa88363c63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9c14830-a1c0-443f-b8b1-cd8dda92f335",
                    "LayerId": "07d8b376-620c-4a89-88cb-4c1c767ba712"
                }
            ]
        },
        {
            "id": "e85a0a9f-c4b4-4c9c-9658-833efeda2f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "compositeImage": {
                "id": "f46a7f8e-ade8-4889-aa7d-d70e177ca57b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e85a0a9f-c4b4-4c9c-9658-833efeda2f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a85be050-10f1-4db9-b5f7-20aaab195e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e85a0a9f-c4b4-4c9c-9658-833efeda2f6e",
                    "LayerId": "07d8b376-620c-4a89-88cb-4c1c767ba712"
                }
            ]
        },
        {
            "id": "72605618-8cd8-4953-a716-1d3c737edf80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "compositeImage": {
                "id": "908b5cd2-cb11-4bbe-a6a2-6f1706f39320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72605618-8cd8-4953-a716-1d3c737edf80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09aab1e7-b4c3-47ae-96ab-9ede7bdd99fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72605618-8cd8-4953-a716-1d3c737edf80",
                    "LayerId": "07d8b376-620c-4a89-88cb-4c1c767ba712"
                }
            ]
        },
        {
            "id": "ddf464cf-48b1-456d-ad63-bf1e4690c3b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "compositeImage": {
                "id": "051e3036-9b67-4cf7-9e78-51d197c8712a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddf464cf-48b1-456d-ad63-bf1e4690c3b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13bf37dd-293f-457e-ac1e-2933a3694e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddf464cf-48b1-456d-ad63-bf1e4690c3b2",
                    "LayerId": "07d8b376-620c-4a89-88cb-4c1c767ba712"
                }
            ]
        },
        {
            "id": "bdf831cd-4585-4668-9f6b-2392700ffaa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "compositeImage": {
                "id": "aede5809-92ba-4040-bf27-fafcc11c7848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdf831cd-4585-4668-9f6b-2392700ffaa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8035cf7-7854-47a8-ad10-6784af4d2d5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdf831cd-4585-4668-9f6b-2392700ffaa3",
                    "LayerId": "07d8b376-620c-4a89-88cb-4c1c767ba712"
                }
            ]
        },
        {
            "id": "e8b8a9ce-9b1e-442b-a199-d81778a6b13b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "compositeImage": {
                "id": "5803d427-4486-4e7f-94e4-0d7f5dd8a9bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b8a9ce-9b1e-442b-a199-d81778a6b13b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "660b0262-87e9-4975-8bb5-d1da22d3a473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b8a9ce-9b1e-442b-a199-d81778a6b13b",
                    "LayerId": "07d8b376-620c-4a89-88cb-4c1c767ba712"
                }
            ]
        },
        {
            "id": "41d864f2-3241-4ea9-a521-cebffe58caa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "compositeImage": {
                "id": "fa39f8f5-1219-4733-bc54-7bcabbff33cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41d864f2-3241-4ea9-a521-cebffe58caa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0fd1e61-fc75-434c-b931-2a5ddef7fc1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41d864f2-3241-4ea9-a521-cebffe58caa6",
                    "LayerId": "07d8b376-620c-4a89-88cb-4c1c767ba712"
                }
            ]
        },
        {
            "id": "3f4e1932-e9e2-4953-b2c9-a17d2fac0d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "compositeImage": {
                "id": "d8424b2c-9ef6-46c8-8001-ab7cf92a0b08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f4e1932-e9e2-4953-b2c9-a17d2fac0d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23f03495-f9dd-428a-9a47-2e16921faf41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f4e1932-e9e2-4953-b2c9-a17d2fac0d4d",
                    "LayerId": "07d8b376-620c-4a89-88cb-4c1c767ba712"
                }
            ]
        },
        {
            "id": "34bf4dae-8bfe-49c9-b3ef-1d2c11adfb48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "compositeImage": {
                "id": "6755168f-083b-40f9-a1ce-fbcea1f7b4e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34bf4dae-8bfe-49c9-b3ef-1d2c11adfb48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "693bd009-396e-4a6e-b300-00d997880eb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34bf4dae-8bfe-49c9-b3ef-1d2c11adfb48",
                    "LayerId": "07d8b376-620c-4a89-88cb-4c1c767ba712"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "07d8b376-620c-4a89-88cb-4c1c767ba712",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf8b63b4-00ee-4b33-9da6-011c00d819bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}