{
    "id": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bluemetalchunk2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29d2abfc-6cee-4ff4-a861-285f17e192ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "715766a7-f263-446f-b603-2354c53b7755",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29d2abfc-6cee-4ff4-a861-285f17e192ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f177df7c-5c9f-4c6c-b452-2c9222640606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29d2abfc-6cee-4ff4-a861-285f17e192ff",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "5c514b0a-aed7-4d39-9603-88f28b8ad512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "1083dd91-ef01-4cf2-8053-c19e0e601e3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c514b0a-aed7-4d39-9603-88f28b8ad512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "243ecac9-d936-4e8c-b8cb-bb3cc81521c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c514b0a-aed7-4d39-9603-88f28b8ad512",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "19f8f176-46e4-4751-a038-fb7979034944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "c803db9c-375c-4619-8b99-82de773a98e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19f8f176-46e4-4751-a038-fb7979034944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab450604-d609-47af-a71c-a50de47cb066",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19f8f176-46e4-4751-a038-fb7979034944",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "c07b4a88-567d-4910-960b-488259b15d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "192a499b-955c-4ca4-abd6-d547723a01fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c07b4a88-567d-4910-960b-488259b15d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d02d87b5-48e6-453f-81d9-a5da347ff202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c07b4a88-567d-4910-960b-488259b15d56",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "256b06ec-3c4f-4da0-8f3f-45ac608f4602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "b7cfba6c-6ca8-4309-a17b-6d07f9234387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "256b06ec-3c4f-4da0-8f3f-45ac608f4602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e3d3ba7-5b7b-4f1d-bdad-88c9cd70bc84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "256b06ec-3c4f-4da0-8f3f-45ac608f4602",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "28d2b7c5-232f-4c28-8372-c1710691438a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "24f273a2-05ec-4044-b306-d37e5d16fafa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28d2b7c5-232f-4c28-8372-c1710691438a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5a9dea9-19b6-4beb-83ac-d38440cc6d00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28d2b7c5-232f-4c28-8372-c1710691438a",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "70849be3-4dff-419a-ba8f-c128e8c52cfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "72592956-463f-482a-8c8e-af78b56eccb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70849be3-4dff-419a-ba8f-c128e8c52cfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97fd0bf7-36d5-4bb6-9bd7-93bb9e6c31b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70849be3-4dff-419a-ba8f-c128e8c52cfa",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "c0fbdf88-a6a1-4e98-9484-af0c748bcccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "1c26a32c-d1c5-45e7-814b-80cf83c9da7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0fbdf88-a6a1-4e98-9484-af0c748bcccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9706ea0-1df4-425f-8389-e6d3606638fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0fbdf88-a6a1-4e98-9484-af0c748bcccd",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "5c89b80b-96f5-4155-8d7c-2eaff9d2de18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "7e40a10a-216f-4687-82ec-fd516217f51c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c89b80b-96f5-4155-8d7c-2eaff9d2de18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "168540aa-e89e-49a5-b5ad-8991a320028f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c89b80b-96f5-4155-8d7c-2eaff9d2de18",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "fbaaa679-51d3-4ef0-8e16-5fe84747532c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "5d748963-1b0a-4d2a-b97f-068c3781d65a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbaaa679-51d3-4ef0-8e16-5fe84747532c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7873a8f1-e1b4-4199-bb6d-7e62a86610d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbaaa679-51d3-4ef0-8e16-5fe84747532c",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "aa511c6d-0250-4d92-abff-5580ca356a78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "a53cba62-3b77-409f-8d22-57b893e6932e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa511c6d-0250-4d92-abff-5580ca356a78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ccad19f-699c-44af-9331-66bf7c8935e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa511c6d-0250-4d92-abff-5580ca356a78",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "f1bc8cc6-13b0-442d-a8ed-d5a336494315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "33c8c094-51a1-49e7-8ec0-5609395ea7f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1bc8cc6-13b0-442d-a8ed-d5a336494315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0ef355b-9b36-44d5-ae5d-0c491b5f8ceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1bc8cc6-13b0-442d-a8ed-d5a336494315",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "2fb7b94d-f6bd-45d1-9542-043af5788129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "5382518b-8d34-4650-94d9-c21809543320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb7b94d-f6bd-45d1-9542-043af5788129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74925bd-a1db-4a10-b958-317e9b7c5639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb7b94d-f6bd-45d1-9542-043af5788129",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "3253a508-13fc-4aca-b142-af5dfb78f753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "c26053fb-5f71-49e5-8f02-7a1c11310998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3253a508-13fc-4aca-b142-af5dfb78f753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ab47875-e664-4cc0-90de-55df3ca2419c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3253a508-13fc-4aca-b142-af5dfb78f753",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        },
        {
            "id": "ddf3ae7a-9cbe-4ca6-9f57-a25f7d940846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "compositeImage": {
                "id": "9a950a6a-e5a6-4996-b61e-01c229ec6833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddf3ae7a-9cbe-4ca6-9f57-a25f7d940846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bf07021-9919-43db-9ae1-8117d31d9278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddf3ae7a-9cbe-4ca6-9f57-a25f7d940846",
                    "LayerId": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "d6dc8172-95e1-4fce-a354-6a98a9ad6aee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14a9ac5b-6640-4a6b-aabc-79ec1bc9de90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 22
}