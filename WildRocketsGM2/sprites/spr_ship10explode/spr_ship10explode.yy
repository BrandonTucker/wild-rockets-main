{
    "id": "325061a9-46ea-4035-a815-2b3a3f7a6407",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship10explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 0,
    "bbox_right": 54,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "363b5924-f92c-4bd2-86d3-716863f2aa6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325061a9-46ea-4035-a815-2b3a3f7a6407",
            "compositeImage": {
                "id": "a8657036-44e3-455a-b28d-2166ec0f9196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "363b5924-f92c-4bd2-86d3-716863f2aa6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "080d8d87-ee5a-4f2a-8b0f-ac8095fc7898",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "363b5924-f92c-4bd2-86d3-716863f2aa6f",
                    "LayerId": "3e9cab6e-e609-4908-967d-2461d2fdfdc3"
                }
            ]
        },
        {
            "id": "ce2b0709-2fe8-48e5-91e5-665c03024817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325061a9-46ea-4035-a815-2b3a3f7a6407",
            "compositeImage": {
                "id": "696c3b9d-f0b9-42aa-9886-ca1381784b85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce2b0709-2fe8-48e5-91e5-665c03024817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59e09bae-3d1f-47e0-9fe4-4f799c1de5b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce2b0709-2fe8-48e5-91e5-665c03024817",
                    "LayerId": "3e9cab6e-e609-4908-967d-2461d2fdfdc3"
                }
            ]
        },
        {
            "id": "0fd70d79-5ec0-4b05-9f32-8d80030bb544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325061a9-46ea-4035-a815-2b3a3f7a6407",
            "compositeImage": {
                "id": "09ea4c7d-f361-47e5-90d4-ab6a1c2c4f74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fd70d79-5ec0-4b05-9f32-8d80030bb544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48f3149e-b911-4759-927b-3cacb079ba5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fd70d79-5ec0-4b05-9f32-8d80030bb544",
                    "LayerId": "3e9cab6e-e609-4908-967d-2461d2fdfdc3"
                }
            ]
        },
        {
            "id": "4b367bbc-d59e-4633-9e06-9fd8a84bd0ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325061a9-46ea-4035-a815-2b3a3f7a6407",
            "compositeImage": {
                "id": "dba2a5c0-7e72-46f2-85c7-f24d1e8d64ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b367bbc-d59e-4633-9e06-9fd8a84bd0ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18b53e17-5509-4af5-80d1-c56dc1e6c017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b367bbc-d59e-4633-9e06-9fd8a84bd0ad",
                    "LayerId": "3e9cab6e-e609-4908-967d-2461d2fdfdc3"
                }
            ]
        },
        {
            "id": "579723e8-904d-49bb-85ec-fc6f24df424e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325061a9-46ea-4035-a815-2b3a3f7a6407",
            "compositeImage": {
                "id": "1dd24a48-4aa8-4968-afe7-588b57676d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "579723e8-904d-49bb-85ec-fc6f24df424e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ff18f93-9fef-4e3e-9704-33ed0db0b254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "579723e8-904d-49bb-85ec-fc6f24df424e",
                    "LayerId": "3e9cab6e-e609-4908-967d-2461d2fdfdc3"
                }
            ]
        },
        {
            "id": "9c0f7f24-edeb-476e-af08-c95a8ec41dd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325061a9-46ea-4035-a815-2b3a3f7a6407",
            "compositeImage": {
                "id": "af04f4f1-b321-4933-b31e-72b986ad784f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c0f7f24-edeb-476e-af08-c95a8ec41dd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d378c367-4cb2-4f85-89dd-eac0f33fc48d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c0f7f24-edeb-476e-af08-c95a8ec41dd1",
                    "LayerId": "3e9cab6e-e609-4908-967d-2461d2fdfdc3"
                }
            ]
        },
        {
            "id": "d7124477-c235-49a8-ad09-1daf15736411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325061a9-46ea-4035-a815-2b3a3f7a6407",
            "compositeImage": {
                "id": "3c6d5068-ab4b-4491-9126-6efa9c30ee7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7124477-c235-49a8-ad09-1daf15736411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ec83120-c9dd-441c-abda-888918620c31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7124477-c235-49a8-ad09-1daf15736411",
                    "LayerId": "3e9cab6e-e609-4908-967d-2461d2fdfdc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "3e9cab6e-e609-4908-967d-2461d2fdfdc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "325061a9-46ea-4035-a815-2b3a3f7a6407",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 27,
    "yorig": 49
}