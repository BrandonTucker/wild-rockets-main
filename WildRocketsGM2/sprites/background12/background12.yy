{
    "id": "683f27e7-c327-494b-ac78-4af0803d60c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background12",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 959,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e987e939-44cd-404f-a382-aad4b83a879f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "683f27e7-c327-494b-ac78-4af0803d60c5",
            "compositeImage": {
                "id": "ab632c56-77a9-42a4-933f-8b48c859bbcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e987e939-44cd-404f-a382-aad4b83a879f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fc0cd4a-ce65-44ce-98ee-55cad86392a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e987e939-44cd-404f-a382-aad4b83a879f",
                    "LayerId": "02f5469b-80c2-443f-b5ba-55bfa2caf21b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "02f5469b-80c2-443f-b5ba-55bfa2caf21b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "683f27e7-c327-494b-ac78-4af0803d60c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}