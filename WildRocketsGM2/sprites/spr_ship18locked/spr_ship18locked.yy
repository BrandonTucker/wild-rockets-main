{
    "id": "a406a008-4cc4-40e5-8edf-fd229bb50e25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship18locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb5c7b50-6015-4515-8260-6c2e101f15d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a406a008-4cc4-40e5-8edf-fd229bb50e25",
            "compositeImage": {
                "id": "7b5cbe64-f18a-416b-b728-4c41a800261b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb5c7b50-6015-4515-8260-6c2e101f15d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca830b56-ef9b-4dc6-9790-68a9337d40cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb5c7b50-6015-4515-8260-6c2e101f15d8",
                    "LayerId": "a31922df-0d58-449b-bb07-d60e42588c9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "a31922df-0d58-449b-bb07-d60e42588c9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a406a008-4cc4-40e5-8edf-fd229bb50e25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}