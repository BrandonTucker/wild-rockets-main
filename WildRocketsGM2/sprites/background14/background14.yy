{
    "id": "a31601e4-5f53-474d-9641-864f4c28f2e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background14",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 959,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46838699-9a31-4d95-9af8-4ae2110ca5b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a31601e4-5f53-474d-9641-864f4c28f2e5",
            "compositeImage": {
                "id": "ded6cba7-f357-4983-8dae-721ac7834b1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46838699-9a31-4d95-9af8-4ae2110ca5b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86eace5f-e1b6-488f-aff2-ccff84b734af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46838699-9a31-4d95-9af8-4ae2110ca5b2",
                    "LayerId": "0832d282-c962-451d-aa64-1328452cf1c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "0832d282-c962-451d-aa64-1328452cf1c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a31601e4-5f53-474d-9641-864f4c28f2e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}