{
    "id": "0efbc10f-856c-45b2-b1f1-9d8df146915d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shipbuttontemplate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad8014ec-32e7-4ca4-b80a-63cae7ff82ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efbc10f-856c-45b2-b1f1-9d8df146915d",
            "compositeImage": {
                "id": "7c83d5ea-f9b3-4832-924a-388fd7e8d6cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad8014ec-32e7-4ca4-b80a-63cae7ff82ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54aba54f-390d-41ce-8c96-e3232a29ec52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad8014ec-32e7-4ca4-b80a-63cae7ff82ce",
                    "LayerId": "e142de4a-9522-4e59-8daf-fc7d434e1e58"
                }
            ]
        },
        {
            "id": "9059271f-58b3-49f2-8b56-17ceae807a91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efbc10f-856c-45b2-b1f1-9d8df146915d",
            "compositeImage": {
                "id": "4dea8e73-2423-43da-9220-ce782eaba79a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9059271f-58b3-49f2-8b56-17ceae807a91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abfe89d2-39d2-4bf8-8f77-614e8a78071b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9059271f-58b3-49f2-8b56-17ceae807a91",
                    "LayerId": "e142de4a-9522-4e59-8daf-fc7d434e1e58"
                }
            ]
        },
        {
            "id": "2a79bd8b-9d4c-4347-adc6-131b201cb458",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efbc10f-856c-45b2-b1f1-9d8df146915d",
            "compositeImage": {
                "id": "ab9c8c60-99b4-4da4-ac3a-f795fb4459bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a79bd8b-9d4c-4347-adc6-131b201cb458",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9c331b-e16a-4ddd-8e2e-0fad3630ab9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a79bd8b-9d4c-4347-adc6-131b201cb458",
                    "LayerId": "e142de4a-9522-4e59-8daf-fc7d434e1e58"
                }
            ]
        },
        {
            "id": "433f90cd-2556-4302-b67e-c2fe29d555b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efbc10f-856c-45b2-b1f1-9d8df146915d",
            "compositeImage": {
                "id": "9a8a842b-da33-46ea-885c-cd30209562c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "433f90cd-2556-4302-b67e-c2fe29d555b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb73a479-cdb4-413a-a7ca-8f6ba0f86fcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "433f90cd-2556-4302-b67e-c2fe29d555b2",
                    "LayerId": "e142de4a-9522-4e59-8daf-fc7d434e1e58"
                }
            ]
        },
        {
            "id": "6cce8fcb-1a81-4149-ac43-3ee8aa3c60ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efbc10f-856c-45b2-b1f1-9d8df146915d",
            "compositeImage": {
                "id": "a9848447-ce80-4eae-9a2c-23a203e5894b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cce8fcb-1a81-4149-ac43-3ee8aa3c60ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93b36cb1-b9e8-4d3e-b511-f8686f5ccf97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cce8fcb-1a81-4149-ac43-3ee8aa3c60ac",
                    "LayerId": "e142de4a-9522-4e59-8daf-fc7d434e1e58"
                }
            ]
        },
        {
            "id": "458eb2fb-1f94-4466-b80a-130a3e5dbfc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0efbc10f-856c-45b2-b1f1-9d8df146915d",
            "compositeImage": {
                "id": "b6fdca48-7d53-4f85-8ee4-69ee99feba1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "458eb2fb-1f94-4466-b80a-130a3e5dbfc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2c411f1-610a-4612-a1ec-3e9bc02e5b7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "458eb2fb-1f94-4466-b80a-130a3e5dbfc8",
                    "LayerId": "e142de4a-9522-4e59-8daf-fc7d434e1e58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "e142de4a-9522-4e59-8daf-fc7d434e1e58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0efbc10f-856c-45b2-b1f1-9d8df146915d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}