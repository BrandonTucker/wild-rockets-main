{
    "id": "ef786960-5354-4640-8ff8-1a99e26c74ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 87,
    "bbox_left": 2,
    "bbox_right": 87,
    "bbox_top": 41,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2d0bb95-4257-46fd-820f-bf291325a1e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef786960-5354-4640-8ff8-1a99e26c74ac",
            "compositeImage": {
                "id": "9bcdd56a-3f9f-45e3-972e-27b6c71f2065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2d0bb95-4257-46fd-820f-bf291325a1e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e48b79a9-c05e-4ff6-8405-d9fb8c2a93d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2d0bb95-4257-46fd-820f-bf291325a1e6",
                    "LayerId": "9c03df7d-a64d-488b-9b40-60e5700e9d84"
                }
            ]
        },
        {
            "id": "3d2061d0-f8d2-45e1-8aa6-8b4a64139376",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef786960-5354-4640-8ff8-1a99e26c74ac",
            "compositeImage": {
                "id": "68f9359f-9cd3-44ca-888e-ab0482cdf2c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d2061d0-f8d2-45e1-8aa6-8b4a64139376",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab0df54-c42c-4fb5-87e2-537affcf5212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d2061d0-f8d2-45e1-8aa6-8b4a64139376",
                    "LayerId": "9c03df7d-a64d-488b-9b40-60e5700e9d84"
                }
            ]
        },
        {
            "id": "016a4fd6-cd86-4f66-be57-aeb884655dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef786960-5354-4640-8ff8-1a99e26c74ac",
            "compositeImage": {
                "id": "6cbc5f13-8bf2-4f6f-9846-b8fac9393ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016a4fd6-cd86-4f66-be57-aeb884655dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c83fbb82-df85-462d-b1b1-1e68b06c155d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016a4fd6-cd86-4f66-be57-aeb884655dce",
                    "LayerId": "9c03df7d-a64d-488b-9b40-60e5700e9d84"
                }
            ]
        },
        {
            "id": "7d3e407e-0ed2-4f6b-a714-9cbfa8786b00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef786960-5354-4640-8ff8-1a99e26c74ac",
            "compositeImage": {
                "id": "78dad3a9-c738-4275-a79b-7af1805d8537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d3e407e-0ed2-4f6b-a714-9cbfa8786b00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd5f785-4dae-406b-81eb-1a8553b8bc7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d3e407e-0ed2-4f6b-a714-9cbfa8786b00",
                    "LayerId": "9c03df7d-a64d-488b-9b40-60e5700e9d84"
                }
            ]
        },
        {
            "id": "696e01c7-515a-45eb-84c8-3b734de58025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef786960-5354-4640-8ff8-1a99e26c74ac",
            "compositeImage": {
                "id": "5788bc0b-b634-45c3-8f98-7a7579e08ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "696e01c7-515a-45eb-84c8-3b734de58025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e4ebd57-9112-4967-a2ae-63ebf0cf47ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "696e01c7-515a-45eb-84c8-3b734de58025",
                    "LayerId": "9c03df7d-a64d-488b-9b40-60e5700e9d84"
                }
            ]
        },
        {
            "id": "616424fc-86ac-4442-a598-70aaafa11e5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef786960-5354-4640-8ff8-1a99e26c74ac",
            "compositeImage": {
                "id": "60bfcb2b-a911-43cc-8cff-2c5661acb27d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "616424fc-86ac-4442-a598-70aaafa11e5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41bde73a-0ffe-4946-957f-6682f69f2e76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "616424fc-86ac-4442-a598-70aaafa11e5f",
                    "LayerId": "9c03df7d-a64d-488b-9b40-60e5700e9d84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "9c03df7d-a64d-488b-9b40-60e5700e9d84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef786960-5354-4640-8ff8-1a99e26c74ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 43,
    "yorig": 68
}