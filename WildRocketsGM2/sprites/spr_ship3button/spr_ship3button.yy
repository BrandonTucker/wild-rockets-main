{
    "id": "82d47bdb-1129-492c-a4be-6d0d25a2d589",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship3button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 14,
    "bbox_right": 88,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e00f418a-5990-4df4-a8c5-15e8d8837ac0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d47bdb-1129-492c-a4be-6d0d25a2d589",
            "compositeImage": {
                "id": "1aa40818-77d6-4119-a9ed-588caf09b1cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e00f418a-5990-4df4-a8c5-15e8d8837ac0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9449331-32d8-41f1-8c31-3e76c598e2cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e00f418a-5990-4df4-a8c5-15e8d8837ac0",
                    "LayerId": "3e2c7b39-a8b3-46a1-ba67-6ef30955ddbc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "3e2c7b39-a8b3-46a1-ba67-6ef30955ddbc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82d47bdb-1129-492c-a4be-6d0d25a2d589",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}