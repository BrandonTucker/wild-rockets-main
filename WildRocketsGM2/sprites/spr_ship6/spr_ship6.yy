{
    "id": "9ab4c2e7-3870-4221-9e8c-9b27a4305908",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 29,
    "bbox_right": 54,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "309accba-858f-4819-a7a3-b6ee8cba3f03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ab4c2e7-3870-4221-9e8c-9b27a4305908",
            "compositeImage": {
                "id": "a090b5e7-b3b6-435d-b37f-416fe1eb2a32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "309accba-858f-4819-a7a3-b6ee8cba3f03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "378d4eeb-66b3-4940-ad78-0af01efe29d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "309accba-858f-4819-a7a3-b6ee8cba3f03",
                    "LayerId": "dd0261fa-9d2d-42e0-b9a3-ac49428b4dc4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "dd0261fa-9d2d-42e0-b9a3-ac49428b4dc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ab4c2e7-3870-4221-9e8c-9b27a4305908",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 42,
    "yorig": 47
}