{
    "id": "16392c5f-6abb-4f3c-84e4-e629b27b3c35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite24",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 236,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00a59826-4e27-43dc-ad86-7ac743976597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16392c5f-6abb-4f3c-84e4-e629b27b3c35",
            "compositeImage": {
                "id": "bfb5b75c-75c6-4ea2-aaf5-71e3d594a615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00a59826-4e27-43dc-ad86-7ac743976597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f9d791b-dc2f-4604-b3e7-1a3e1e388451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00a59826-4e27-43dc-ad86-7ac743976597",
                    "LayerId": "1a840fb7-5f7f-4d50-9e55-ac330a0c346b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 237,
    "layers": [
        {
            "id": "1a840fb7-5f7f-4d50-9e55-ac330a0c346b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16392c5f-6abb-4f3c-84e4-e629b27b3c35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}