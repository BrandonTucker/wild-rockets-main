{
    "id": "24d65620-c4b3-4b34-99bb-77c389504d05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship8locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9355ddd4-f74f-4cdd-8c6d-64d2f821c4ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24d65620-c4b3-4b34-99bb-77c389504d05",
            "compositeImage": {
                "id": "e8830937-403e-4a88-99ad-f75ec18bbd3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9355ddd4-f74f-4cdd-8c6d-64d2f821c4ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ca1aef-2047-499a-8a03-78ae4917a63f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9355ddd4-f74f-4cdd-8c6d-64d2f821c4ea",
                    "LayerId": "088155fc-2ce4-4344-b569-a6d11477a13e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "088155fc-2ce4-4344-b569-a6d11477a13e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24d65620-c4b3-4b34-99bb-77c389504d05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}