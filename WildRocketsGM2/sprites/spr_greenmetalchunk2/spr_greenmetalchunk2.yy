{
    "id": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_greenmetalchunk2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24e04ba9-9b88-49c9-a0ae-65c4cd0acc47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "f95387e8-24cf-40a1-91ab-a9d0f46232d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24e04ba9-9b88-49c9-a0ae-65c4cd0acc47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d7e5cb5-bf4a-48fa-83dd-07894342aae9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24e04ba9-9b88-49c9-a0ae-65c4cd0acc47",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "d39a6ad0-4e0a-4bf0-a6a8-20e892d57f0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "012c0e41-c762-4b5b-a25d-fac6065a3a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d39a6ad0-4e0a-4bf0-a6a8-20e892d57f0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66bd2628-fe0b-4b09-bb1c-a63d69932690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d39a6ad0-4e0a-4bf0-a6a8-20e892d57f0b",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "5419e501-b5f9-4041-b38d-461d57aa1a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "9bf324e6-7a8a-4cf1-8668-43311ed579f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5419e501-b5f9-4041-b38d-461d57aa1a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "356fa814-fb47-490e-ad0f-3cf136a6613a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5419e501-b5f9-4041-b38d-461d57aa1a7c",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "01c1aae3-539d-47c9-9aa1-d7ae97df778c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "a186ac1d-3ce1-487a-afa7-4d29b5f51dd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c1aae3-539d-47c9-9aa1-d7ae97df778c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f669f280-6671-4b1c-991d-36d9bf0ad9fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c1aae3-539d-47c9-9aa1-d7ae97df778c",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "2f4b7dc8-592d-4959-a180-15c3cb24202b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "b9db58c0-6817-46bc-83f9-551bcd356456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f4b7dc8-592d-4959-a180-15c3cb24202b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a9731d0-bf8e-441a-b50c-503d304a01b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f4b7dc8-592d-4959-a180-15c3cb24202b",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "679243fb-d9c3-4a80-a03f-c7b731fcc05d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "523e61c8-4d07-4230-9c59-855d15cb995f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "679243fb-d9c3-4a80-a03f-c7b731fcc05d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0093d182-5266-4f1a-a9ec-6c64df27a993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "679243fb-d9c3-4a80-a03f-c7b731fcc05d",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "42801a32-173a-4105-9a95-b471669d28e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "aaaec4cf-c26f-444b-94a9-830c3b4591cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42801a32-173a-4105-9a95-b471669d28e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33e869eb-6616-4aa6-8d05-2c2aaa7801af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42801a32-173a-4105-9a95-b471669d28e4",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "ca05eb80-408b-4b7b-b1b0-983365853489",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "c096a2f1-4c58-4f3b-af99-60a6d28f70b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca05eb80-408b-4b7b-b1b0-983365853489",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36c59619-4b5d-4eeb-b1b0-d1462761cfab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca05eb80-408b-4b7b-b1b0-983365853489",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "75c2dbc8-bc99-401e-8267-2103c94e61e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "176fb1b2-e1a4-4613-9b5f-64a4d784caa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75c2dbc8-bc99-401e-8267-2103c94e61e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efc4d04d-0f50-433d-8c68-20043761cdcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75c2dbc8-bc99-401e-8267-2103c94e61e1",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "f0df5f76-652f-4012-b957-dd903bd24752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "6df6a05c-fab4-4a95-b9b6-f3afc08edd40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0df5f76-652f-4012-b957-dd903bd24752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c972c0b-d104-4b72-b625-0e16c0d168db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0df5f76-652f-4012-b957-dd903bd24752",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "0d7b8470-fb5a-4ea5-a041-724ee10b891e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "72130104-8e87-41b3-b7b2-e110cde9c8f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d7b8470-fb5a-4ea5-a041-724ee10b891e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca015fc-496b-4e28-b39a-0034607609fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d7b8470-fb5a-4ea5-a041-724ee10b891e",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "7dd31de5-5f0a-42c3-91dd-735373a3d1b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "0fd01317-d984-402a-9c4f-a8a954fd19ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dd31de5-5f0a-42c3-91dd-735373a3d1b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c106fed-4adf-4363-89bd-7f291d84c00c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dd31de5-5f0a-42c3-91dd-735373a3d1b3",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "be3a3eac-b50b-493d-9ed1-5efc9d18b2e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "f6fb742b-d497-47c1-8d5a-a2b3cbd30183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3a3eac-b50b-493d-9ed1-5efc9d18b2e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "352ddb57-e484-49a8-98db-364ee5a8bfba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3a3eac-b50b-493d-9ed1-5efc9d18b2e3",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "8f90c99b-d7f1-4d8c-9861-c2f008c1015e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "fe982eb2-e15b-4a12-940e-ebc8c2e032f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f90c99b-d7f1-4d8c-9861-c2f008c1015e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f0f85d-4cd5-48f2-8e52-c9ced62f9662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f90c99b-d7f1-4d8c-9861-c2f008c1015e",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        },
        {
            "id": "0fa0719b-069b-4985-bf32-aeee0a36bfac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "compositeImage": {
                "id": "e23a3edf-ce03-420b-a320-40aec17e01ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fa0719b-069b-4985-bf32-aeee0a36bfac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21fdd18e-d70d-4686-9252-93b6c26fa2d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa0719b-069b-4985-bf32-aeee0a36bfac",
                    "LayerId": "e41b979f-53ab-4f2c-aa65-36bd656edfe1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 78,
    "layers": [
        {
            "id": "e41b979f-53ab-4f2c-aa65-36bd656edfe1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2ff62cb-a206-4db4-b63e-c5c073fe35a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 39
}