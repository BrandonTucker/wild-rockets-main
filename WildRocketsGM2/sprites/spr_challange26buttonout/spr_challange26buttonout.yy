{
    "id": "205e363b-544b-4248-a358-4e75abad22f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange26buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "beecd2ee-2ff0-4b2d-b17c-d054051bc260",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "compositeImage": {
                "id": "f26a3509-4453-4e99-accd-fd45ebfba103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beecd2ee-2ff0-4b2d-b17c-d054051bc260",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03fac1cc-a1d8-498e-a289-5e7f61ac6da3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beecd2ee-2ff0-4b2d-b17c-d054051bc260",
                    "LayerId": "f42309b5-7837-460b-9e9d-b8652030c1d2"
                }
            ]
        },
        {
            "id": "fb617efb-3b48-4e56-aa76-d8f9e812aa42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "compositeImage": {
                "id": "13532a07-8eb1-4e64-b3ae-3d5bce126600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb617efb-3b48-4e56-aa76-d8f9e812aa42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dee1a34-893e-451e-9765-868be279256e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb617efb-3b48-4e56-aa76-d8f9e812aa42",
                    "LayerId": "f42309b5-7837-460b-9e9d-b8652030c1d2"
                }
            ]
        },
        {
            "id": "e914bc9b-4a4a-4519-ae73-f2c708830696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "compositeImage": {
                "id": "5045ebed-7833-45c0-9f03-6d0e7d8fb1ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e914bc9b-4a4a-4519-ae73-f2c708830696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a982bd8-3df0-4530-8906-b247b78bf2bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e914bc9b-4a4a-4519-ae73-f2c708830696",
                    "LayerId": "f42309b5-7837-460b-9e9d-b8652030c1d2"
                }
            ]
        },
        {
            "id": "5707ee45-490e-4578-b168-7bcadcfc9ed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "compositeImage": {
                "id": "43f6470a-d414-4dc3-bc79-d7be0368c5d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5707ee45-490e-4578-b168-7bcadcfc9ed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5483b4af-e04f-413f-a314-1ac0e70ace4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5707ee45-490e-4578-b168-7bcadcfc9ed3",
                    "LayerId": "f42309b5-7837-460b-9e9d-b8652030c1d2"
                }
            ]
        },
        {
            "id": "a8b8f94f-e512-4761-96cd-97b0dcba6687",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "compositeImage": {
                "id": "dc4c9001-c531-404c-b540-9d7a3a39ef8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8b8f94f-e512-4761-96cd-97b0dcba6687",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26bf3c5e-ac27-459d-aab5-a98eb6b2fdd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8b8f94f-e512-4761-96cd-97b0dcba6687",
                    "LayerId": "f42309b5-7837-460b-9e9d-b8652030c1d2"
                }
            ]
        },
        {
            "id": "e54ad1cf-e01d-4407-b353-77662120071c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "compositeImage": {
                "id": "4d797e55-aa84-4e81-9693-8914b852e047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e54ad1cf-e01d-4407-b353-77662120071c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfde8942-dc42-4269-9411-c75cb2d5c363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e54ad1cf-e01d-4407-b353-77662120071c",
                    "LayerId": "f42309b5-7837-460b-9e9d-b8652030c1d2"
                }
            ]
        },
        {
            "id": "c1fc817a-42b8-4a74-8841-a79315af673e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "compositeImage": {
                "id": "cff8f414-4308-4d82-ba10-5aa8ce378e2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1fc817a-42b8-4a74-8841-a79315af673e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b917a59-de8c-4aca-b2c7-a5387fc8962c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1fc817a-42b8-4a74-8841-a79315af673e",
                    "LayerId": "f42309b5-7837-460b-9e9d-b8652030c1d2"
                }
            ]
        },
        {
            "id": "2ef7c48f-3596-458c-8515-5b18873c34b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "compositeImage": {
                "id": "7a1ebb39-5847-408b-8b41-81a78d41f125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ef7c48f-3596-458c-8515-5b18873c34b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb256736-e265-44c4-ad73-f865b201d008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ef7c48f-3596-458c-8515-5b18873c34b1",
                    "LayerId": "f42309b5-7837-460b-9e9d-b8652030c1d2"
                }
            ]
        },
        {
            "id": "9588b9ae-999b-488e-a3f4-d34d8c9c5fdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "compositeImage": {
                "id": "7f7b6303-a00b-4626-ac92-d9011ee7a8b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9588b9ae-999b-488e-a3f4-d34d8c9c5fdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1203d660-991b-414f-868a-8db4c813bde5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9588b9ae-999b-488e-a3f4-d34d8c9c5fdf",
                    "LayerId": "f42309b5-7837-460b-9e9d-b8652030c1d2"
                }
            ]
        },
        {
            "id": "8ab39d07-d7ff-4e16-95c9-ea4cc010691d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "compositeImage": {
                "id": "c7a2d811-9680-4fbf-baab-d44002f85d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ab39d07-d7ff-4e16-95c9-ea4cc010691d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1195e4a2-c102-4aa6-b837-a379640737ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ab39d07-d7ff-4e16-95c9-ea4cc010691d",
                    "LayerId": "f42309b5-7837-460b-9e9d-b8652030c1d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "f42309b5-7837-460b-9e9d-b8652030c1d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "205e363b-544b-4248-a358-4e75abad22f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}