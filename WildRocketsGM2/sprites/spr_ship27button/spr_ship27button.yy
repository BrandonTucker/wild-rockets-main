{
    "id": "18150916-85a3-4111-bf1c-22a473d05262",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship27button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e132c79-0033-4725-823d-e1b137422d5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18150916-85a3-4111-bf1c-22a473d05262",
            "compositeImage": {
                "id": "93700862-577a-418c-86ff-046e634ad430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e132c79-0033-4725-823d-e1b137422d5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "683ba544-6dd4-4039-9fa8-4eca9e1d72a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e132c79-0033-4725-823d-e1b137422d5f",
                    "LayerId": "d4c0acfb-1f85-4451-966d-1d9bf1499f8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "d4c0acfb-1f85-4451-966d-1d9bf1499f8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18150916-85a3-4111-bf1c-22a473d05262",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}