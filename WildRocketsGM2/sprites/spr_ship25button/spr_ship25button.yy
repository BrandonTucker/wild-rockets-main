{
    "id": "e948847d-a441-49fb-988d-0e5916ffe9e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship25button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05bc97af-32ce-4c2e-add3-5010b5194b90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e948847d-a441-49fb-988d-0e5916ffe9e6",
            "compositeImage": {
                "id": "6682bbce-f337-46f9-bba5-aab7649cebdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05bc97af-32ce-4c2e-add3-5010b5194b90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1605db9f-b63b-4ef2-9149-3281ce9260c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05bc97af-32ce-4c2e-add3-5010b5194b90",
                    "LayerId": "77a627c6-f688-4d1d-8e3a-ea6a89933848"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "77a627c6-f688-4d1d-8e3a-ea6a89933848",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e948847d-a441-49fb-988d-0e5916ffe9e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}