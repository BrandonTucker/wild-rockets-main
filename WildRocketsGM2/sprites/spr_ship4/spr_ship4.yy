{
    "id": "6a0c0881-721d-4e9e-9c2e-61268ce3c894",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "383c01b3-f704-4e24-89d3-d6b5d472314c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a0c0881-721d-4e9e-9c2e-61268ce3c894",
            "compositeImage": {
                "id": "b09b2a46-a51a-4e0b-9dab-79b49a4d955c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "383c01b3-f704-4e24-89d3-d6b5d472314c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbb5d636-08b1-4e72-a366-1874ac6adcb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "383c01b3-f704-4e24-89d3-d6b5d472314c",
                    "LayerId": "24da0b6a-8c9b-4732-9e1a-55db8a193b8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "24da0b6a-8c9b-4732-9e1a-55db8a193b8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a0c0881-721d-4e9e-9c2e-61268ce3c894",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 53
}