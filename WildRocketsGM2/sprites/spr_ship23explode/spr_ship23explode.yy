{
    "id": "9b8c7037-4ecf-4b0b-89f2-e22c046a8d5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship23explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 25,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42d1a995-4cdc-4be5-8faf-16af47699ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b8c7037-4ecf-4b0b-89f2-e22c046a8d5b",
            "compositeImage": {
                "id": "63fcc087-6248-4698-b02e-016ca828a069",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42d1a995-4cdc-4be5-8faf-16af47699ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39799b79-17bd-4fa5-8d7a-e64afafa06a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42d1a995-4cdc-4be5-8faf-16af47699ee2",
                    "LayerId": "e53e65e3-1131-4ff3-95c4-2b6b55240039"
                }
            ]
        },
        {
            "id": "67dec202-ca2d-47c7-861e-6e76272b80a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b8c7037-4ecf-4b0b-89f2-e22c046a8d5b",
            "compositeImage": {
                "id": "55368f0f-13d6-4476-8662-f6b0b9a28206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67dec202-ca2d-47c7-861e-6e76272b80a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69a14327-f00d-4984-8b44-7c6185250587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67dec202-ca2d-47c7-861e-6e76272b80a0",
                    "LayerId": "e53e65e3-1131-4ff3-95c4-2b6b55240039"
                }
            ]
        },
        {
            "id": "6cc12108-bf02-4e7d-b675-a1608a7c2293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b8c7037-4ecf-4b0b-89f2-e22c046a8d5b",
            "compositeImage": {
                "id": "22c5d165-15f5-4337-92a7-5acad82bce0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cc12108-bf02-4e7d-b675-a1608a7c2293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2311c468-6e2e-4ee1-a325-711a6c740696",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cc12108-bf02-4e7d-b675-a1608a7c2293",
                    "LayerId": "e53e65e3-1131-4ff3-95c4-2b6b55240039"
                }
            ]
        },
        {
            "id": "7f992188-32d5-42ca-ae05-faf4f22ff4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b8c7037-4ecf-4b0b-89f2-e22c046a8d5b",
            "compositeImage": {
                "id": "444226a1-aa83-4f97-956b-4ef9f9fdbebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f992188-32d5-42ca-ae05-faf4f22ff4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d668f68a-a689-4640-a2d7-0d2c919a98fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f992188-32d5-42ca-ae05-faf4f22ff4c4",
                    "LayerId": "e53e65e3-1131-4ff3-95c4-2b6b55240039"
                }
            ]
        },
        {
            "id": "7542a85e-3e4e-4480-abd3-dfb64971b8c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b8c7037-4ecf-4b0b-89f2-e22c046a8d5b",
            "compositeImage": {
                "id": "fa40aef4-5226-46e8-88bd-5bd1f7f64f2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7542a85e-3e4e-4480-abd3-dfb64971b8c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc2902d6-613b-4656-a728-b8bd7af4e9cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7542a85e-3e4e-4480-abd3-dfb64971b8c7",
                    "LayerId": "e53e65e3-1131-4ff3-95c4-2b6b55240039"
                }
            ]
        },
        {
            "id": "e49e53d4-1409-48dc-b5a4-1fcc328d44f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b8c7037-4ecf-4b0b-89f2-e22c046a8d5b",
            "compositeImage": {
                "id": "f8efc9ff-abd3-4b19-890e-189a15895f64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e49e53d4-1409-48dc-b5a4-1fcc328d44f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29679d96-3721-475d-b0d2-1f86018d03b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e49e53d4-1409-48dc-b5a4-1fcc328d44f2",
                    "LayerId": "e53e65e3-1131-4ff3-95c4-2b6b55240039"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 119,
    "layers": [
        {
            "id": "e53e65e3-1131-4ff3-95c4-2b6b55240039",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b8c7037-4ecf-4b0b-89f2-e22c046a8d5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 103,
    "xorig": 52,
    "yorig": 47
}