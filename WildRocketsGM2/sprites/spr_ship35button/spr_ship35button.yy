{
    "id": "f8851adc-8783-479f-9de3-f0f8771910ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship35button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cb7aa17-3c98-470c-9f07-9a46647406b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8851adc-8783-479f-9de3-f0f8771910ea",
            "compositeImage": {
                "id": "ee580ce1-ccf5-4f38-9722-ca7c0176672e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cb7aa17-3c98-470c-9f07-9a46647406b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c77203f4-605d-4cb9-a305-a0f55fb3ceac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cb7aa17-3c98-470c-9f07-9a46647406b4",
                    "LayerId": "95a95d49-bdf6-4cb4-885e-5114fe261c52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "95a95d49-bdf6-4cb4-885e-5114fe261c52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8851adc-8783-479f-9de3-f0f8771910ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}