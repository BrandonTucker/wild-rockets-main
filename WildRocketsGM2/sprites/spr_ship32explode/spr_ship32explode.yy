{
    "id": "a5bc9d49-c00a-467c-8c73-34a47c0e80ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship32explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d76071ba-cd04-4ab3-977e-cf81669d3f12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5bc9d49-c00a-467c-8c73-34a47c0e80ae",
            "compositeImage": {
                "id": "d33319f3-2eba-4bd7-98d1-d793fee3bfd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d76071ba-cd04-4ab3-977e-cf81669d3f12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f1db92b-8cf2-4d80-a489-a8b4e3c999f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d76071ba-cd04-4ab3-977e-cf81669d3f12",
                    "LayerId": "5bb3930b-87aa-4883-99d1-0832d73d92ae"
                }
            ]
        },
        {
            "id": "8cb2c757-f14d-439c-a0fa-21f08efcbd57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5bc9d49-c00a-467c-8c73-34a47c0e80ae",
            "compositeImage": {
                "id": "edde310f-6bcc-4581-924c-69afbb8cf0db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb2c757-f14d-439c-a0fa-21f08efcbd57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a28450b1-8962-4c13-b240-5488fcfb5446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb2c757-f14d-439c-a0fa-21f08efcbd57",
                    "LayerId": "5bb3930b-87aa-4883-99d1-0832d73d92ae"
                }
            ]
        },
        {
            "id": "7b3d08ec-6610-4a4f-8172-7be1d820caf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5bc9d49-c00a-467c-8c73-34a47c0e80ae",
            "compositeImage": {
                "id": "10e017d2-814d-4761-8b4f-05277bee48e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b3d08ec-6610-4a4f-8172-7be1d820caf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41c3ffbc-753d-4ded-a552-9f4af5ee2ee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b3d08ec-6610-4a4f-8172-7be1d820caf7",
                    "LayerId": "5bb3930b-87aa-4883-99d1-0832d73d92ae"
                }
            ]
        },
        {
            "id": "e84c321e-aa54-4c9c-aa68-fbc617f2ab97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5bc9d49-c00a-467c-8c73-34a47c0e80ae",
            "compositeImage": {
                "id": "b45970b7-f716-4894-bac5-ca363aabc2dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84c321e-aa54-4c9c-aa68-fbc617f2ab97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdd5d945-c2dd-4051-9a4d-1fff10cb9805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84c321e-aa54-4c9c-aa68-fbc617f2ab97",
                    "LayerId": "5bb3930b-87aa-4883-99d1-0832d73d92ae"
                }
            ]
        },
        {
            "id": "9a7409ad-c577-4eb4-98b0-5a8afe49ea54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5bc9d49-c00a-467c-8c73-34a47c0e80ae",
            "compositeImage": {
                "id": "0ab99220-c771-4243-b234-d70a0c66e9b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a7409ad-c577-4eb4-98b0-5a8afe49ea54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b1809d2-9eb2-4623-b526-cce73cad4f79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a7409ad-c577-4eb4-98b0-5a8afe49ea54",
                    "LayerId": "5bb3930b-87aa-4883-99d1-0832d73d92ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "5bb3930b-87aa-4883-99d1-0832d73d92ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5bc9d49-c00a-467c-8c73-34a47c0e80ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 47
}