{
    "id": "3faa7578-be8a-4cf8-a152-62b5bb4cfc24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage14button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dadc49e-1c17-4559-b811-3adef0d4c336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3faa7578-be8a-4cf8-a152-62b5bb4cfc24",
            "compositeImage": {
                "id": "bc4eb731-a23f-4738-b5fc-441f3423c438",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dadc49e-1c17-4559-b811-3adef0d4c336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea0a743b-9a08-4b3a-ad0f-a3e9b5fd051b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dadc49e-1c17-4559-b811-3adef0d4c336",
                    "LayerId": "39c9e38c-785f-4dce-993e-08d4fec53a33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "39c9e38c-785f-4dce-993e-08d4fec53a33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3faa7578-be8a-4cf8-a152-62b5bb4cfc24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}