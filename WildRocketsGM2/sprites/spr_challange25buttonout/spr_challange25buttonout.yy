{
    "id": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange25buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10c84392-5be1-404e-922f-db0e38782431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "compositeImage": {
                "id": "978411d5-f990-47b7-898e-afefdea225e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10c84392-5be1-404e-922f-db0e38782431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25ac45c3-c7fe-4be4-a57f-204e630ee6bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10c84392-5be1-404e-922f-db0e38782431",
                    "LayerId": "789b5821-4bca-4793-99cc-6cce31dad8d3"
                }
            ]
        },
        {
            "id": "ba9e472d-e164-4d50-ac02-8a679e0a93dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "compositeImage": {
                "id": "2f386de4-560c-48f5-b9ab-55baa9dabfa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba9e472d-e164-4d50-ac02-8a679e0a93dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e4c00f5-36ba-4a8c-884e-16d4d8c0f85e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba9e472d-e164-4d50-ac02-8a679e0a93dc",
                    "LayerId": "789b5821-4bca-4793-99cc-6cce31dad8d3"
                }
            ]
        },
        {
            "id": "1226a6cd-5c21-4ce2-9dd1-18c3f20ec505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "compositeImage": {
                "id": "758b7ec5-7a28-41cc-98cc-e4a4ed177fd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1226a6cd-5c21-4ce2-9dd1-18c3f20ec505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "579f3179-53e4-4405-bf65-d4bdbe6f978f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1226a6cd-5c21-4ce2-9dd1-18c3f20ec505",
                    "LayerId": "789b5821-4bca-4793-99cc-6cce31dad8d3"
                }
            ]
        },
        {
            "id": "a56abf51-1f05-4c6e-b394-f2b5b1beba16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "compositeImage": {
                "id": "25cadaaa-563b-4e56-bedd-cb4665bc44a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a56abf51-1f05-4c6e-b394-f2b5b1beba16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fac3389-16de-4b4b-8063-195e28c43d32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a56abf51-1f05-4c6e-b394-f2b5b1beba16",
                    "LayerId": "789b5821-4bca-4793-99cc-6cce31dad8d3"
                }
            ]
        },
        {
            "id": "3131ba29-5e0d-4690-bcca-c5302e26215f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "compositeImage": {
                "id": "78e8faa5-6589-46bb-aed4-f6037de8a62a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3131ba29-5e0d-4690-bcca-c5302e26215f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60563310-37b7-4259-a7e3-3e8228516778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3131ba29-5e0d-4690-bcca-c5302e26215f",
                    "LayerId": "789b5821-4bca-4793-99cc-6cce31dad8d3"
                }
            ]
        },
        {
            "id": "5a5bacc7-e595-43a4-a0a4-2811194686ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "compositeImage": {
                "id": "1ade09eb-8f86-4423-855c-84bd0504c5f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a5bacc7-e595-43a4-a0a4-2811194686ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e5f25ba-a073-409e-a30d-2ef9a82690d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a5bacc7-e595-43a4-a0a4-2811194686ea",
                    "LayerId": "789b5821-4bca-4793-99cc-6cce31dad8d3"
                }
            ]
        },
        {
            "id": "6c29f340-dc28-40e0-9ea5-8601d9a976ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "compositeImage": {
                "id": "f310b65a-ff9e-4124-a780-da1e01dabcac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c29f340-dc28-40e0-9ea5-8601d9a976ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "045cc1a0-177d-45c0-a08a-dc10842b5431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c29f340-dc28-40e0-9ea5-8601d9a976ad",
                    "LayerId": "789b5821-4bca-4793-99cc-6cce31dad8d3"
                }
            ]
        },
        {
            "id": "a91d458b-2084-440e-8c3e-9841b593c136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "compositeImage": {
                "id": "94db1125-7f61-4f86-92fa-bc75f3333b00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a91d458b-2084-440e-8c3e-9841b593c136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98cf5034-19cf-4480-a90b-d945dbcc04dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a91d458b-2084-440e-8c3e-9841b593c136",
                    "LayerId": "789b5821-4bca-4793-99cc-6cce31dad8d3"
                }
            ]
        },
        {
            "id": "c5e8585e-6d43-4d83-84ae-d60f47fd5718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "compositeImage": {
                "id": "d00dd3eb-8183-473e-bb62-887251cc3b91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5e8585e-6d43-4d83-84ae-d60f47fd5718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c882f7f0-ebe4-4247-ac13-b69ff277b8d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5e8585e-6d43-4d83-84ae-d60f47fd5718",
                    "LayerId": "789b5821-4bca-4793-99cc-6cce31dad8d3"
                }
            ]
        },
        {
            "id": "bce6757e-2c3c-485d-b8be-7961edfb9dca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "compositeImage": {
                "id": "bfd2e11e-93bc-4185-a9bd-2b23a405625b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bce6757e-2c3c-485d-b8be-7961edfb9dca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79c94aaa-c452-4d05-a31e-c7796f2aaebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bce6757e-2c3c-485d-b8be-7961edfb9dca",
                    "LayerId": "789b5821-4bca-4793-99cc-6cce31dad8d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "789b5821-4bca-4793-99cc-6cce31dad8d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f0bd6cc-7aba-43bc-ab13-16675268164e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}