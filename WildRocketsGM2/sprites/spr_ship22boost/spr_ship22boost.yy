{
    "id": "2253cfdd-007e-462d-a043-8b3e043edb1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship22boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7da8e0d-ccee-40ec-a7b3-566d15caf4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2253cfdd-007e-462d-a043-8b3e043edb1e",
            "compositeImage": {
                "id": "270a7a41-fbb3-4099-9576-4b5c5527a94c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7da8e0d-ccee-40ec-a7b3-566d15caf4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5f67512-69e7-4042-975c-0652d79e8aa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7da8e0d-ccee-40ec-a7b3-566d15caf4c9",
                    "LayerId": "a0c4df16-befb-4994-9fac-a2b510b65b47"
                }
            ]
        },
        {
            "id": "80d304f9-e56f-45df-9b60-f7c2a5e20a53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2253cfdd-007e-462d-a043-8b3e043edb1e",
            "compositeImage": {
                "id": "89b333e7-1341-41c4-bf13-1adcd9e0f435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80d304f9-e56f-45df-9b60-f7c2a5e20a53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a086d09e-9f76-45fd-8967-a3ecf5842533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80d304f9-e56f-45df-9b60-f7c2a5e20a53",
                    "LayerId": "a0c4df16-befb-4994-9fac-a2b510b65b47"
                }
            ]
        },
        {
            "id": "1382de58-bbbc-411a-948e-c70ccbd7eebd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2253cfdd-007e-462d-a043-8b3e043edb1e",
            "compositeImage": {
                "id": "334077b6-721e-4665-99eb-a198ba6387e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1382de58-bbbc-411a-948e-c70ccbd7eebd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1ef538e-4099-410d-8a90-6f565bf03a0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1382de58-bbbc-411a-948e-c70ccbd7eebd",
                    "LayerId": "a0c4df16-befb-4994-9fac-a2b510b65b47"
                }
            ]
        },
        {
            "id": "c7e71abd-5bea-4605-9cfa-f35fc5782c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2253cfdd-007e-462d-a043-8b3e043edb1e",
            "compositeImage": {
                "id": "671fb4f3-d24f-487f-8d4d-9643311002d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7e71abd-5bea-4605-9cfa-f35fc5782c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c662b1b-4d56-4ebf-b3c9-119102bd9c98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7e71abd-5bea-4605-9cfa-f35fc5782c5e",
                    "LayerId": "a0c4df16-befb-4994-9fac-a2b510b65b47"
                }
            ]
        },
        {
            "id": "035e1a39-c7fc-4199-b73b-c7ecceff7af4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2253cfdd-007e-462d-a043-8b3e043edb1e",
            "compositeImage": {
                "id": "2b66caab-d76a-4a8d-998c-42ffa65d0ce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "035e1a39-c7fc-4199-b73b-c7ecceff7af4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4387aad-4ce6-4174-950d-8a7c3b572d04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "035e1a39-c7fc-4199-b73b-c7ecceff7af4",
                    "LayerId": "a0c4df16-befb-4994-9fac-a2b510b65b47"
                }
            ]
        },
        {
            "id": "11e9ac57-efb8-43ca-a7a6-3bb9ff775205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2253cfdd-007e-462d-a043-8b3e043edb1e",
            "compositeImage": {
                "id": "51d68318-aace-4c5f-a8e7-6bfeac9124ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11e9ac57-efb8-43ca-a7a6-3bb9ff775205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f92432fd-345d-48dc-a3d1-edf106190a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11e9ac57-efb8-43ca-a7a6-3bb9ff775205",
                    "LayerId": "a0c4df16-befb-4994-9fac-a2b510b65b47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 138,
    "layers": [
        {
            "id": "a0c4df16-befb-4994-9fac-a2b510b65b47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2253cfdd-007e-462d-a043-8b3e043edb1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 49
}