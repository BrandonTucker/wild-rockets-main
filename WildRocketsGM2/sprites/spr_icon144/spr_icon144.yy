{
    "id": "0b864d28-4ea0-4b8b-9f51-cfa918b5d7d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon144",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 10,
    "bbox_right": 133,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb7eb78c-8489-41ad-8325-de256a4d82ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b864d28-4ea0-4b8b-9f51-cfa918b5d7d6",
            "compositeImage": {
                "id": "f3e4a69c-0619-417e-998a-ec789db49a12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb7eb78c-8489-41ad-8325-de256a4d82ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "949c4a79-8fa6-4cc9-a373-a61f6c5f9460",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb7eb78c-8489-41ad-8325-de256a4d82ef",
                    "LayerId": "169607c2-da85-4d83-b143-1764e9575923"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 144,
    "layers": [
        {
            "id": "169607c2-da85-4d83-b143-1764e9575923",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b864d28-4ea0-4b8b-9f51-cfa918b5d7d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 0,
    "yorig": 0
}