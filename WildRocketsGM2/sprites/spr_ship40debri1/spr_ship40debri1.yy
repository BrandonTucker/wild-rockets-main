{
    "id": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship40debri1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 0,
    "bbox_right": 73,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25f4d7c8-42a9-4f9e-8e3e-80c0ebb3b123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "f1d73d58-bb9c-474b-8259-9c9f0ec1c31d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25f4d7c8-42a9-4f9e-8e3e-80c0ebb3b123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e01b4b45-7b8e-4c4e-8852-69d40ed2d1ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25f4d7c8-42a9-4f9e-8e3e-80c0ebb3b123",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "7b462193-2d07-4c7a-9d99-8c143d12c838",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "a51ff95d-9d57-4e54-9cdc-ed4f4cd0296a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b462193-2d07-4c7a-9d99-8c143d12c838",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5a7fd6d-4342-4d87-9a5b-c504819e2f2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b462193-2d07-4c7a-9d99-8c143d12c838",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "db7a50bc-f2ce-4009-b35c-e77d59f2082d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "d12ba0af-f9b7-462c-bb75-9276cdc0c98b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db7a50bc-f2ce-4009-b35c-e77d59f2082d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "510f7f26-6acf-4faf-a977-1a9de60cc36b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db7a50bc-f2ce-4009-b35c-e77d59f2082d",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "af1bfb6d-f4c9-43cc-bf7a-9812dd688afc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "55780466-c6ee-4328-add5-0dfaa0c9dcb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af1bfb6d-f4c9-43cc-bf7a-9812dd688afc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fd1ac15-0e4b-41b3-8e7f-9f5519155cbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af1bfb6d-f4c9-43cc-bf7a-9812dd688afc",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "2c783e03-e907-4913-9ba1-b860e39cf053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "bca147c9-0b89-4015-ae1f-150dc6251db6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c783e03-e907-4913-9ba1-b860e39cf053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a371ccf-b099-4180-bb51-80f989d08ef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c783e03-e907-4913-9ba1-b860e39cf053",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "6f072138-0308-4c25-a1ae-a73968935428",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "1776e5c7-7987-457a-b70a-974a017ada89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f072138-0308-4c25-a1ae-a73968935428",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f937439-c379-4261-9f71-8f380e852912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f072138-0308-4c25-a1ae-a73968935428",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "3d331c7c-2cc4-4af9-8403-a7ff6d7800d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "071a37b8-d994-41bc-b970-a57a9ffd826e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d331c7c-2cc4-4af9-8403-a7ff6d7800d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa1c14dd-7494-4f75-a89b-8070772f4a4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d331c7c-2cc4-4af9-8403-a7ff6d7800d7",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "22cdd9e1-7d28-4b1b-835d-12e1679784d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "f47f0fc5-dd1b-4250-a04c-5fd073a33d76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22cdd9e1-7d28-4b1b-835d-12e1679784d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "554af0a5-0449-4bb3-a30b-32119649cfc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22cdd9e1-7d28-4b1b-835d-12e1679784d5",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "f59bc2c5-c373-467d-98ca-5b8f93cbface",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "ae9a14d7-900e-4299-a366-239adac00792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f59bc2c5-c373-467d-98ca-5b8f93cbface",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f013c3be-42bd-4d7d-b5d4-7741294ddac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f59bc2c5-c373-467d-98ca-5b8f93cbface",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "6b1242a4-f9c7-410e-97ce-257b71e4cf54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "dbf00dc3-7478-4349-8193-e87ca2e556a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b1242a4-f9c7-410e-97ce-257b71e4cf54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "963e08b8-c69b-4fbb-afe2-bdaac0fabafd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b1242a4-f9c7-410e-97ce-257b71e4cf54",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "d39bdddd-cd26-4118-be38-c113b7691b03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "7104c566-6618-42c9-8d48-abcc333ea9c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d39bdddd-cd26-4118-be38-c113b7691b03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb65dfc8-8b21-4d57-94fe-0d2d0d0e21d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d39bdddd-cd26-4118-be38-c113b7691b03",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "b50b00ec-6234-48d7-a7c7-a1a5604e0e91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "ad0eaeaa-0bcf-438c-a914-5677a7d49203",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b50b00ec-6234-48d7-a7c7-a1a5604e0e91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47c85295-22cf-489b-bd2b-f332d5819f14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50b00ec-6234-48d7-a7c7-a1a5604e0e91",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "f4392ed3-8bc0-45d7-b59d-46159ccbb38e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "025780b3-601e-4360-9d2b-7a45bb700afc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4392ed3-8bc0-45d7-b59d-46159ccbb38e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d419f8b-bbd4-4513-b4a5-dabe12cff5c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4392ed3-8bc0-45d7-b59d-46159ccbb38e",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "74059c38-9e5f-4a7c-b8a5-169e69e9c197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "184cf93c-b9b5-424c-8c3d-b007c94ebaf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74059c38-9e5f-4a7c-b8a5-169e69e9c197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8903a6c6-619d-4d3f-9dd0-9da77437c40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74059c38-9e5f-4a7c-b8a5-169e69e9c197",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        },
        {
            "id": "1c006ea3-b322-4027-b4d5-e6795c235eeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "compositeImage": {
                "id": "ecab0d4d-0bfd-482d-a963-ff00e0ae2c25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c006ea3-b322-4027-b4d5-e6795c235eeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7426958-71e1-445b-a09b-3455be94ad0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c006ea3-b322-4027-b4d5-e6795c235eeb",
                    "LayerId": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 73,
    "layers": [
        {
            "id": "11d22edb-f8a5-4760-bf44-e1a2c8e4f5aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e88baf0-09e3-40a8-87cf-f3aca26434e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 35,
    "yorig": 31
}