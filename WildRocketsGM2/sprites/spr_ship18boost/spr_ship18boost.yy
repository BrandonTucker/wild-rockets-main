{
    "id": "057b5d45-13c8-4d83-ad80-7d338262519e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship18boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 30,
    "bbox_right": 55,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afaed99a-30c1-4816-8047-eb213613b3cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057b5d45-13c8-4d83-ad80-7d338262519e",
            "compositeImage": {
                "id": "9a5ac663-872e-470e-9b88-59e0818f9c78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afaed99a-30c1-4816-8047-eb213613b3cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d41693ab-646f-48cb-a861-7fd7806ed3ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afaed99a-30c1-4816-8047-eb213613b3cd",
                    "LayerId": "81584763-386d-4dda-86ee-014c2f8371d7"
                }
            ]
        },
        {
            "id": "875cd7bb-3f54-477a-89b9-615b593f850b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057b5d45-13c8-4d83-ad80-7d338262519e",
            "compositeImage": {
                "id": "e4c21bfa-703a-4179-b826-4bd3b1e3d9d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875cd7bb-3f54-477a-89b9-615b593f850b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4205c8b5-94e0-47d5-aa24-61be7a3b46e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875cd7bb-3f54-477a-89b9-615b593f850b",
                    "LayerId": "81584763-386d-4dda-86ee-014c2f8371d7"
                }
            ]
        },
        {
            "id": "c13e0e61-1ea0-434e-a5f3-87b7ec5ece4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057b5d45-13c8-4d83-ad80-7d338262519e",
            "compositeImage": {
                "id": "ebcfc61f-8cda-476b-a614-f6a685fc2177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c13e0e61-1ea0-434e-a5f3-87b7ec5ece4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c197f4-ee0f-4c67-a264-ee5c551cb4fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c13e0e61-1ea0-434e-a5f3-87b7ec5ece4d",
                    "LayerId": "81584763-386d-4dda-86ee-014c2f8371d7"
                }
            ]
        },
        {
            "id": "68c18c0c-0fc2-45bc-bcb7-ee3293499d52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057b5d45-13c8-4d83-ad80-7d338262519e",
            "compositeImage": {
                "id": "33fa2eeb-97e9-4ff7-81e7-78f3d9886d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68c18c0c-0fc2-45bc-bcb7-ee3293499d52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f442acf2-173d-412c-accf-50a8786334a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68c18c0c-0fc2-45bc-bcb7-ee3293499d52",
                    "LayerId": "81584763-386d-4dda-86ee-014c2f8371d7"
                }
            ]
        },
        {
            "id": "73c13b50-d8a9-4567-a05e-e47619f5e6da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057b5d45-13c8-4d83-ad80-7d338262519e",
            "compositeImage": {
                "id": "5fdab0b9-f947-4f2a-9655-269ae8f5bb69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73c13b50-d8a9-4567-a05e-e47619f5e6da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fa96ffb-87d2-4b3d-b96b-9691a9aebef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73c13b50-d8a9-4567-a05e-e47619f5e6da",
                    "LayerId": "81584763-386d-4dda-86ee-014c2f8371d7"
                }
            ]
        },
        {
            "id": "da842e06-38ed-4de9-9516-2300d4aa612f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057b5d45-13c8-4d83-ad80-7d338262519e",
            "compositeImage": {
                "id": "31c04c50-9885-4066-b851-918e93bee46b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da842e06-38ed-4de9-9516-2300d4aa612f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29400127-edad-4a14-b3b9-c5d5ff153fa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da842e06-38ed-4de9-9516-2300d4aa612f",
                    "LayerId": "81584763-386d-4dda-86ee-014c2f8371d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "81584763-386d-4dda-86ee-014c2f8371d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "057b5d45-13c8-4d83-ad80-7d338262519e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 86,
    "xorig": 42,
    "yorig": 50
}