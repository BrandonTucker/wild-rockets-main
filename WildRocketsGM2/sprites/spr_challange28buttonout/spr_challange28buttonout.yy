{
    "id": "9793730c-7050-478f-b36b-2d7af576b410",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange28buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d035b0ee-38ee-4099-8fdb-22850a9fca2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "compositeImage": {
                "id": "0936d382-bd80-4e35-9fea-1364896400b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d035b0ee-38ee-4099-8fdb-22850a9fca2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f81c490-e20a-444b-ad68-3e8b03b90259",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d035b0ee-38ee-4099-8fdb-22850a9fca2d",
                    "LayerId": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed"
                }
            ]
        },
        {
            "id": "21fe2507-627a-4d54-91ec-0d3916d53a19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "compositeImage": {
                "id": "a99093df-96b7-4888-a072-019ae88364e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21fe2507-627a-4d54-91ec-0d3916d53a19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2b381a8-2bc6-4db2-81ad-b4dfa99295da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21fe2507-627a-4d54-91ec-0d3916d53a19",
                    "LayerId": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed"
                }
            ]
        },
        {
            "id": "aabd7c6e-c975-48ff-87ec-57a32fcae470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "compositeImage": {
                "id": "d3beec14-3201-4506-aebd-5c67e5abb97b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aabd7c6e-c975-48ff-87ec-57a32fcae470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11241168-ed0e-451e-a35a-cbe91e70850b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aabd7c6e-c975-48ff-87ec-57a32fcae470",
                    "LayerId": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed"
                }
            ]
        },
        {
            "id": "70066382-d416-4580-b3eb-ce31a7db47bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "compositeImage": {
                "id": "7c85575f-7010-47d0-904a-fce6726ef05c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70066382-d416-4580-b3eb-ce31a7db47bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8b50019-cb86-4b53-94da-66ba2cc0c7d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70066382-d416-4580-b3eb-ce31a7db47bf",
                    "LayerId": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed"
                }
            ]
        },
        {
            "id": "5a606063-bb45-4952-b563-ed675d58449d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "compositeImage": {
                "id": "7da90e91-94c3-4d6d-bd78-f23580c9cba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a606063-bb45-4952-b563-ed675d58449d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "630f6773-cf32-4d4d-8847-6bd06dfa6c39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a606063-bb45-4952-b563-ed675d58449d",
                    "LayerId": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed"
                }
            ]
        },
        {
            "id": "12f60052-7b56-49a0-92b7-9d824e69d4e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "compositeImage": {
                "id": "046b02b0-96dc-4b5a-b215-2944e9f392ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f60052-7b56-49a0-92b7-9d824e69d4e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71205208-b7e3-48de-a832-a8d28bc991fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f60052-7b56-49a0-92b7-9d824e69d4e6",
                    "LayerId": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed"
                }
            ]
        },
        {
            "id": "59e7235c-11b1-45fc-a07a-c166fa95e121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "compositeImage": {
                "id": "5e18eb9b-35ec-451a-a727-964fca1b7df4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59e7235c-11b1-45fc-a07a-c166fa95e121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf92df36-89c4-4595-838f-e30ce8a61364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59e7235c-11b1-45fc-a07a-c166fa95e121",
                    "LayerId": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed"
                }
            ]
        },
        {
            "id": "04f4e247-f3a4-4c33-aa9f-ebc73d9d0546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "compositeImage": {
                "id": "79b126eb-5434-4324-a001-59f46726b31c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f4e247-f3a4-4c33-aa9f-ebc73d9d0546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638fa7bb-305b-45af-87f2-d2df19d4a00a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f4e247-f3a4-4c33-aa9f-ebc73d9d0546",
                    "LayerId": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed"
                }
            ]
        },
        {
            "id": "650918be-5dfb-4062-a46d-14d25427bb9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "compositeImage": {
                "id": "67523ab8-c53f-461b-ae02-de7e89acde8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "650918be-5dfb-4062-a46d-14d25427bb9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9263611-a1b5-47a0-9aa4-a912c18fd149",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "650918be-5dfb-4062-a46d-14d25427bb9d",
                    "LayerId": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed"
                }
            ]
        },
        {
            "id": "71815cdf-7e96-4f00-bb9c-2532ea7f9d9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "compositeImage": {
                "id": "c6c8d430-ee09-4c9d-b7f6-475ff17a9e7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71815cdf-7e96-4f00-bb9c-2532ea7f9d9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3fcb840-a71b-4549-9376-7cee526d3620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71815cdf-7e96-4f00-bb9c-2532ea7f9d9a",
                    "LayerId": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "9ddb1a65-b34b-45f0-9dd8-6b98e3ee9eed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9793730c-7050-478f-b36b-2d7af576b410",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}