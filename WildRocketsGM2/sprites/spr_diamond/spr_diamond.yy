{
    "id": "6909f790-96cf-49aa-8130-95802e0c665e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_diamond",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 66,
    "bbox_left": 1,
    "bbox_right": 85,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7738307-f9c8-4a67-a10f-1726e14c26b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "f9288356-a1d6-48a4-a733-fa80425e0840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7738307-f9c8-4a67-a10f-1726e14c26b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c18b2aa-03aa-4609-84e2-1a91ba18d221",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7738307-f9c8-4a67-a10f-1726e14c26b2",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        },
        {
            "id": "63230691-5c9f-4983-b1f8-244a1fd90054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "25c4a3da-66a5-45b4-b886-64f26fbdd2c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63230691-5c9f-4983-b1f8-244a1fd90054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca9477db-5d41-42b1-89f4-a168c98810fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63230691-5c9f-4983-b1f8-244a1fd90054",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        },
        {
            "id": "b38c6c5d-8d54-4825-998c-c5eb809df1b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "32bea41a-99e7-4f44-aa84-fc78f1aea54f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b38c6c5d-8d54-4825-998c-c5eb809df1b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69cedab3-f8bd-4038-b7c3-cb2a00e6715f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b38c6c5d-8d54-4825-998c-c5eb809df1b1",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        },
        {
            "id": "a2c3a592-3c0a-4c22-a217-2ec75efed09c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "eb43a429-4198-437d-a78a-51ea347c1c5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2c3a592-3c0a-4c22-a217-2ec75efed09c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66cb1d82-0e01-495f-a980-9ff155b97fb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2c3a592-3c0a-4c22-a217-2ec75efed09c",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        },
        {
            "id": "73f6c2f7-d095-4535-97e4-4fc21ddf7bd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "cd7de26b-9989-4924-b47f-2f046e5975c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73f6c2f7-d095-4535-97e4-4fc21ddf7bd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae32a29a-3062-4a3f-8e78-b1c033675347",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f6c2f7-d095-4535-97e4-4fc21ddf7bd0",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        },
        {
            "id": "6ee77976-b246-4fd4-9749-4ad48f9fac22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "6b400b67-5872-4307-86d9-caf7556a941e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee77976-b246-4fd4-9749-4ad48f9fac22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df3d802c-766b-4cd9-9b43-bce684bfb40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee77976-b246-4fd4-9749-4ad48f9fac22",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        },
        {
            "id": "08d622bf-8f7c-4309-9d0d-1dbd54b89666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "d5ec9724-921d-4914-bba8-5a52d0831c87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08d622bf-8f7c-4309-9d0d-1dbd54b89666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cef18616-fdcb-4e65-8765-740b1c1e1593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08d622bf-8f7c-4309-9d0d-1dbd54b89666",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        },
        {
            "id": "76fd9444-6036-4a5a-8c38-6e324f9a04c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "3cc829be-37de-445f-8c2c-1f91713505e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76fd9444-6036-4a5a-8c38-6e324f9a04c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22ff037d-f720-41e0-be64-b316410d70f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76fd9444-6036-4a5a-8c38-6e324f9a04c3",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        },
        {
            "id": "d4558bda-0523-4a3c-a338-844af3f8c66a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "31db3d9b-dd6d-4bb8-99a0-3a45c6641065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4558bda-0523-4a3c-a338-844af3f8c66a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "167b59a2-efdf-44d7-b026-a7dc4f184f8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4558bda-0523-4a3c-a338-844af3f8c66a",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        },
        {
            "id": "d17d670e-c514-49d3-9dca-bda30bac2511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "2f16fe42-c887-4050-817f-3d0bd701e723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d17d670e-c514-49d3-9dca-bda30bac2511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d651d05-514d-404d-a1b4-cb11b01a8510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d17d670e-c514-49d3-9dca-bda30bac2511",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        },
        {
            "id": "1931638d-507b-4113-b354-fe0165d1b467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "compositeImage": {
                "id": "3641d171-6c1b-4158-9373-6bba911f424a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1931638d-507b-4113-b354-fe0165d1b467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab6d2db4-ed40-43d5-aa16-c6fdaf51a7a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1931638d-507b-4113-b354-fe0165d1b467",
                    "LayerId": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 69,
    "layers": [
        {
            "id": "7177c3dd-3c57-46b5-ac35-aa3cf273c3a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6909f790-96cf-49aa-8130-95802e0c665e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 34
}