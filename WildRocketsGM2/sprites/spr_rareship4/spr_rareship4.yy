{
    "id": "afe73659-6f85-4099-acb2-abd50cc97367",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rareship4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 153,
    "bbox_left": 37,
    "bbox_right": 89,
    "bbox_top": 57,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb64b442-3ad7-428b-b651-f79a5bbae30f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "compositeImage": {
                "id": "72b9d8c9-63d4-414a-8f13-0c3a55359ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb64b442-3ad7-428b-b651-f79a5bbae30f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4fe9825-c717-49a1-9aaf-f6722c761592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb64b442-3ad7-428b-b651-f79a5bbae30f",
                    "LayerId": "63a286ab-632d-4a73-a1ad-eceb4f57e803"
                }
            ]
        },
        {
            "id": "42b9f380-4de7-499a-acb3-5ae9695fab75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "compositeImage": {
                "id": "c5862244-d799-45d8-91c4-1e1845b3a88f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42b9f380-4de7-499a-acb3-5ae9695fab75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1c62840-3290-4cf1-a3a2-349ce2863945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42b9f380-4de7-499a-acb3-5ae9695fab75",
                    "LayerId": "63a286ab-632d-4a73-a1ad-eceb4f57e803"
                }
            ]
        },
        {
            "id": "fa548147-e960-4d47-b54e-df44ec23dae5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "compositeImage": {
                "id": "11a9d23e-7a00-449e-9ad5-35e81f7816f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa548147-e960-4d47-b54e-df44ec23dae5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60bae717-d570-434e-9b8b-3bf38aee16b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa548147-e960-4d47-b54e-df44ec23dae5",
                    "LayerId": "63a286ab-632d-4a73-a1ad-eceb4f57e803"
                }
            ]
        },
        {
            "id": "7dc5a498-4188-4593-9a86-b4713c153776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "compositeImage": {
                "id": "dffd5d88-2438-4919-a143-6dc325016051",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dc5a498-4188-4593-9a86-b4713c153776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eba8881-70a4-4f98-bc08-71c1ec140675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dc5a498-4188-4593-9a86-b4713c153776",
                    "LayerId": "63a286ab-632d-4a73-a1ad-eceb4f57e803"
                }
            ]
        },
        {
            "id": "72433ba5-e8be-4de8-b0a8-a5a841fa3df7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "compositeImage": {
                "id": "af0d20e0-6d0f-41b5-8595-52de4cef45ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72433ba5-e8be-4de8-b0a8-a5a841fa3df7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77090875-b680-4a6c-a71f-438c9aa9581c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72433ba5-e8be-4de8-b0a8-a5a841fa3df7",
                    "LayerId": "63a286ab-632d-4a73-a1ad-eceb4f57e803"
                }
            ]
        },
        {
            "id": "8ff28e78-c779-4890-89fd-672859267503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "compositeImage": {
                "id": "eb93c268-b3e2-4f43-9b80-c233ac5c641b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff28e78-c779-4890-89fd-672859267503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d8b4904-2af7-4ab0-b700-12aa30136a08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff28e78-c779-4890-89fd-672859267503",
                    "LayerId": "63a286ab-632d-4a73-a1ad-eceb4f57e803"
                }
            ]
        },
        {
            "id": "19b0bfcd-af8b-4030-a20e-0f77402d0ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "compositeImage": {
                "id": "d22b7684-fd76-445f-b45d-7779423703c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b0bfcd-af8b-4030-a20e-0f77402d0ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7867cab-2416-41f6-ac47-5e0e84484cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b0bfcd-af8b-4030-a20e-0f77402d0ee1",
                    "LayerId": "63a286ab-632d-4a73-a1ad-eceb4f57e803"
                }
            ]
        },
        {
            "id": "dc2443e0-5f8c-48f7-9aab-5a7cf88a86bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "compositeImage": {
                "id": "2cf40040-5319-4f19-bf37-3f35ce840354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc2443e0-5f8c-48f7-9aab-5a7cf88a86bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92238c96-e568-4780-bc01-69dad9d68d89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc2443e0-5f8c-48f7-9aab-5a7cf88a86bb",
                    "LayerId": "63a286ab-632d-4a73-a1ad-eceb4f57e803"
                }
            ]
        },
        {
            "id": "5214aa00-e106-4b0b-b292-f2760e3fa92f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "compositeImage": {
                "id": "b03bac5b-4d83-488b-bd06-9c773a027a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5214aa00-e106-4b0b-b292-f2760e3fa92f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7a943d-102d-4ee0-b44d-9d7411ee5619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5214aa00-e106-4b0b-b292-f2760e3fa92f",
                    "LayerId": "63a286ab-632d-4a73-a1ad-eceb4f57e803"
                }
            ]
        },
        {
            "id": "d2b4b391-1115-4be3-a665-79d0d64e9ca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "compositeImage": {
                "id": "dbb0b56a-fa62-4e47-8ae3-881f6e22d6c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2b4b391-1115-4be3-a665-79d0d64e9ca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "247dab2d-6286-4875-9307-56bee8e6c2f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2b4b391-1115-4be3-a665-79d0d64e9ca5",
                    "LayerId": "63a286ab-632d-4a73-a1ad-eceb4f57e803"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 209,
    "layers": [
        {
            "id": "63a286ab-632d-4a73-a1ad-eceb4f57e803",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afe73659-6f85-4099-acb2-abd50cc97367",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 129,
    "xorig": 64,
    "yorig": 102
}