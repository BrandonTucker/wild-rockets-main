{
    "id": "8c1a5bdf-a1a7-4962-8d63-53c67b340bd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship36locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db46c473-dd54-4459-8794-b330499a49c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c1a5bdf-a1a7-4962-8d63-53c67b340bd8",
            "compositeImage": {
                "id": "93b39aca-18a3-420a-996f-74fbd4140d81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db46c473-dd54-4459-8794-b330499a49c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e289100f-50d3-4074-95cb-b08483499a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db46c473-dd54-4459-8794-b330499a49c2",
                    "LayerId": "4244a8f1-f4e8-4d0f-8ac0-9210cb03333d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4244a8f1-f4e8-4d0f-8ac0-9210cb03333d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c1a5bdf-a1a7-4962-8d63-53c67b340bd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}