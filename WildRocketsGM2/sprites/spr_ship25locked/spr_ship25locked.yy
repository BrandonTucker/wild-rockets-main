{
    "id": "06c9feeb-f1ca-443c-8805-5edb76000f37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship25locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5e4e1e7-23ad-4a4a-b466-6db476adfd5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c9feeb-f1ca-443c-8805-5edb76000f37",
            "compositeImage": {
                "id": "4730bc5c-832b-4bfa-af1e-8045db66b4e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5e4e1e7-23ad-4a4a-b466-6db476adfd5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c22e0fd-0155-44ef-84d7-7e631bed36e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5e4e1e7-23ad-4a4a-b466-6db476adfd5a",
                    "LayerId": "8a4df52e-0807-4df9-ad3d-4ae5e5c4ae95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "8a4df52e-0807-4df9-ad3d-4ae5e5c4ae95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06c9feeb-f1ca-443c-8805-5edb76000f37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}