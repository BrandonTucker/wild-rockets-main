{
    "id": "44c49b54-266e-4552-a6ef-025930319461",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 55,
    "bbox_right": 80,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43461e65-3331-42b5-96db-04955b0c064c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44c49b54-266e-4552-a6ef-025930319461",
            "compositeImage": {
                "id": "42f5ef0a-f4a8-4dfc-b6a3-463300b66d99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43461e65-3331-42b5-96db-04955b0c064c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5341a96c-119a-47c6-8218-c3f53543870f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43461e65-3331-42b5-96db-04955b0c064c",
                    "LayerId": "116de223-7d0f-4933-86ed-3fbe339c52f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "116de223-7d0f-4933-86ed-3fbe339c52f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44c49b54-266e-4552-a6ef-025930319461",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 67,
    "yorig": 48
}