{
    "id": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship35debri2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5201aa51-e4f4-446a-bbc2-c1120d210c10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "b56cd63b-13bf-432d-9731-d5ff9d4ef87b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5201aa51-e4f4-446a-bbc2-c1120d210c10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ce960bc-100f-42ed-8ed9-a96b97d18d9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5201aa51-e4f4-446a-bbc2-c1120d210c10",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "8dc73e6f-9c93-494a-81da-aff4b28b8f9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "6ad9c9ca-5740-42f5-89df-0ff9d350c89c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dc73e6f-9c93-494a-81da-aff4b28b8f9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f76ac092-99b9-4709-ad9e-4d190473371d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc73e6f-9c93-494a-81da-aff4b28b8f9c",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "bcab2bd4-b423-4658-9c64-b6864d8cc5f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "8c617f87-6a79-47af-8baf-1f547b791bb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcab2bd4-b423-4658-9c64-b6864d8cc5f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5b99102-b897-45a1-9471-2420f8c8062b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcab2bd4-b423-4658-9c64-b6864d8cc5f7",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "e6aa764e-884e-4a5b-a636-c83b3bd93251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "f5149829-6532-4957-a44e-d1730a7860aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6aa764e-884e-4a5b-a636-c83b3bd93251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b58380b-26b1-41d4-84a3-52907449961d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6aa764e-884e-4a5b-a636-c83b3bd93251",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "d6e66f62-52eb-457f-bbac-b7aa966f8357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "6b49ec8c-b2b7-4e53-ab38-1a2c4f4cbb84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e66f62-52eb-457f-bbac-b7aa966f8357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c4fc4ed-73a1-47d7-917a-373ad8ca26fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e66f62-52eb-457f-bbac-b7aa966f8357",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "2ed6cd9f-39a8-440a-9f51-087e5f020183",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "f8a18eb9-4f1a-4fc5-b64e-2f1bbbc8fd8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ed6cd9f-39a8-440a-9f51-087e5f020183",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46e7fe7c-8133-4b97-af12-93af556d2d3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ed6cd9f-39a8-440a-9f51-087e5f020183",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "41562a41-cda5-4c7a-8d7e-676911669c02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "f309ed13-537d-4c8a-9bca-708b88e57153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41562a41-cda5-4c7a-8d7e-676911669c02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef867e27-b9e8-482a-b811-d83e1b930a29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41562a41-cda5-4c7a-8d7e-676911669c02",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "e88ed57b-7842-4bf5-bc17-5da5c4454222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "43ca5eca-21af-4041-b019-f16d295bfdcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e88ed57b-7842-4bf5-bc17-5da5c4454222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc9c75ef-4f97-4b46-a895-76724e70c97a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e88ed57b-7842-4bf5-bc17-5da5c4454222",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "3b52ce05-2c62-4a92-bd12-b4a6eab18695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "02a39522-7e0c-4ae1-ad69-9502dc2c349b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b52ce05-2c62-4a92-bd12-b4a6eab18695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5194b54-991e-4a50-9b97-7ac1bd9c3d69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b52ce05-2c62-4a92-bd12-b4a6eab18695",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "df413e48-84ef-4f65-8781-cdc7fb054532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "75b52d40-0631-4c76-9442-ac5365863d36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df413e48-84ef-4f65-8781-cdc7fb054532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "689fedf6-1ca6-482e-bfa6-72f1b85a20e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df413e48-84ef-4f65-8781-cdc7fb054532",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "4af51f4a-a2ee-45be-8688-a1f65827233a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "ac7e8599-b7c9-476a-a877-1eacb35403cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4af51f4a-a2ee-45be-8688-a1f65827233a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52924aad-faba-4c6b-93f4-e2533f7f065f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4af51f4a-a2ee-45be-8688-a1f65827233a",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "55379b4a-ea7f-4775-a350-65aaa2372765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "dcf6b6c4-730b-438c-9b60-9b47ac0060c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55379b4a-ea7f-4775-a350-65aaa2372765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9b5f847-6331-42e9-97c1-19fdd2540161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55379b4a-ea7f-4775-a350-65aaa2372765",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "1eefd96e-5920-4cab-91ee-0982527b64a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "cd288bc3-01aa-4e25-8e35-bcfcf99f0ffd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eefd96e-5920-4cab-91ee-0982527b64a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f1b2f1-4ba2-4e11-9586-89e8443cec43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eefd96e-5920-4cab-91ee-0982527b64a7",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "96eff449-6278-47f3-8dbc-c782c384bab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "cf15aa14-de5d-4071-aa40-a241ab890c8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96eff449-6278-47f3-8dbc-c782c384bab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fda5e5cd-e912-4947-bbd1-7859f0f5478e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96eff449-6278-47f3-8dbc-c782c384bab9",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "3fd1dba3-0013-422a-aa0a-56667458b2aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "42c5c811-a3d8-4937-8761-1289823cd049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fd1dba3-0013-422a-aa0a-56667458b2aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a42f0eab-58ed-45c0-a7da-9a0437604b0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fd1dba3-0013-422a-aa0a-56667458b2aa",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        },
        {
            "id": "dc4ecacc-325f-400f-8b44-e218fd7b395b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "compositeImage": {
                "id": "dc4faf86-92ca-41f3-bd91-13a4c9779fa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc4ecacc-325f-400f-8b44-e218fd7b395b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d3cf082-8a61-478a-b81f-c4b0ebfa77e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc4ecacc-325f-400f-8b44-e218fd7b395b",
                    "LayerId": "8dcd30b8-901c-4d3c-b693-3c81149c521b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "8dcd30b8-901c-4d3c-b693-3c81149c521b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29ec9356-7c7f-4fbe-967b-efc11d4a296e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 41,
    "yorig": 37
}