{
    "id": "453733d3-8a04-4ba6-a037-e9d9d943128d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage3button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1f08515-9bff-4346-9236-8fcde68c5f40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "453733d3-8a04-4ba6-a037-e9d9d943128d",
            "compositeImage": {
                "id": "9cd9c902-8cec-4ce4-82c9-f10aa600c1c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1f08515-9bff-4346-9236-8fcde68c5f40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27c0eb23-770f-468c-844d-02fd23350daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1f08515-9bff-4346-9236-8fcde68c5f40",
                    "LayerId": "56a23ed8-6194-4c8c-a61e-b5abba57ae39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "56a23ed8-6194-4c8c-a61e-b5abba57ae39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "453733d3-8a04-4ba6-a037-e9d9d943128d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}