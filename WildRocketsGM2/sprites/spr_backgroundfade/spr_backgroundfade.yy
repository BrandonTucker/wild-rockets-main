{
    "id": "885940b9-737a-42e7-8745-614be2fb04d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backgroundfade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4151bc7b-5f3f-49b8-a2c3-2505085d5c9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "885940b9-737a-42e7-8745-614be2fb04d4",
            "compositeImage": {
                "id": "3348413a-7f29-4b44-b651-f3e5635bdfe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4151bc7b-5f3f-49b8-a2c3-2505085d5c9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be8aaa4-ac02-4ccd-83d7-67afb1357c86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4151bc7b-5f3f-49b8-a2c3-2505085d5c9b",
                    "LayerId": "6deb1895-d040-48f3-8a13-0a5755ef0bc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6deb1895-d040-48f3-8a13-0a5755ef0bc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "885940b9-737a-42e7-8745-614be2fb04d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}