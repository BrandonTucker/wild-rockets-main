{
    "id": "0daca597-6829-48e0-87a2-62a0dd78e1b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship39locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1283586b-e7a1-46dc-b73f-a6f1a3a1a307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0daca597-6829-48e0-87a2-62a0dd78e1b4",
            "compositeImage": {
                "id": "1e0d4ca7-f492-4a37-a322-126e85f98267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1283586b-e7a1-46dc-b73f-a6f1a3a1a307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "691d9dc5-747d-487f-a7e0-d3f75e3c340c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1283586b-e7a1-46dc-b73f-a6f1a3a1a307",
                    "LayerId": "c17656c3-85aa-4c81-aa75-c4c91260f876"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c17656c3-85aa-4c81-aa75-c4c91260f876",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0daca597-6829-48e0-87a2-62a0dd78e1b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}