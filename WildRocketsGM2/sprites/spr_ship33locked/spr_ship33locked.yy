{
    "id": "40d9f999-d74b-4dcb-8298-879ee561d209",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship33locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f73f2c93-8187-4763-9fc6-97cb97f9c5ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40d9f999-d74b-4dcb-8298-879ee561d209",
            "compositeImage": {
                "id": "3b14fd06-e365-42ae-8d62-5e4f2c5aaa0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f73f2c93-8187-4763-9fc6-97cb97f9c5ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "561d749d-d610-45cc-82c2-4db9607c8701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f73f2c93-8187-4763-9fc6-97cb97f9c5ba",
                    "LayerId": "79257dfb-a4c1-480a-98c8-a323f2020bd5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "79257dfb-a4c1-480a-98c8-a323f2020bd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40d9f999-d74b-4dcb-8298-879ee561d209",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}