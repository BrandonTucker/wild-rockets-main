{
    "id": "28499dae-81da-4c9b-9b88-75e0cc1e7e2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship30locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a110a555-399b-4172-a3c3-ae5ac60e963a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28499dae-81da-4c9b-9b88-75e0cc1e7e2f",
            "compositeImage": {
                "id": "529178ac-ca28-4fda-a3ef-26eb1acdeeab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a110a555-399b-4172-a3c3-ae5ac60e963a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "837dd242-d4b9-4f74-bc85-c0a21f15c177",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a110a555-399b-4172-a3c3-ae5ac60e963a",
                    "LayerId": "07eb8ebb-183e-4da9-b82c-5aa71a46cf18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "07eb8ebb-183e-4da9-b82c-5aa71a46cf18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28499dae-81da-4c9b-9b88-75e0cc1e7e2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}