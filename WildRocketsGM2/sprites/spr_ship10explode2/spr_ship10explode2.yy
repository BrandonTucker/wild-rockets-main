{
    "id": "8ab21622-4f93-4497-80f6-0a5408289433",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship10explode2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 7,
    "bbox_right": 102,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3956ad7b-4bba-428f-88dc-161a99a405e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ab21622-4f93-4497-80f6-0a5408289433",
            "compositeImage": {
                "id": "998412ad-5907-4648-b8e8-60ec55d76a9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3956ad7b-4bba-428f-88dc-161a99a405e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbb7038f-64e0-49a7-adcb-f0e5514d6fb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3956ad7b-4bba-428f-88dc-161a99a405e9",
                    "LayerId": "b17ffa9b-6e48-47f4-a593-dd189e7218c2"
                }
            ]
        },
        {
            "id": "15c0b4b1-b44d-44c2-a14a-07d6bdcf95a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ab21622-4f93-4497-80f6-0a5408289433",
            "compositeImage": {
                "id": "dbfd6dc1-1224-4d39-ade4-2de841d679d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15c0b4b1-b44d-44c2-a14a-07d6bdcf95a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56ef58ea-106a-4993-9a85-d5bafc56821d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15c0b4b1-b44d-44c2-a14a-07d6bdcf95a0",
                    "LayerId": "b17ffa9b-6e48-47f4-a593-dd189e7218c2"
                }
            ]
        },
        {
            "id": "e1693540-420e-4c8f-aab5-0f4cc901e5e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ab21622-4f93-4497-80f6-0a5408289433",
            "compositeImage": {
                "id": "20994335-777f-43ef-ae2f-909e3a884ac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1693540-420e-4c8f-aab5-0f4cc901e5e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3dee39f-bec3-49bf-9746-51424cadd4b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1693540-420e-4c8f-aab5-0f4cc901e5e6",
                    "LayerId": "b17ffa9b-6e48-47f4-a593-dd189e7218c2"
                }
            ]
        },
        {
            "id": "1599c5ab-6cd8-4b82-a956-f61cb4ca82fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ab21622-4f93-4497-80f6-0a5408289433",
            "compositeImage": {
                "id": "a5424248-a9ce-4fb5-98ff-90410a9f03e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1599c5ab-6cd8-4b82-a956-f61cb4ca82fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e749f298-3b43-45e9-aa17-206c82753d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1599c5ab-6cd8-4b82-a956-f61cb4ca82fa",
                    "LayerId": "b17ffa9b-6e48-47f4-a593-dd189e7218c2"
                }
            ]
        },
        {
            "id": "4e0eb894-1e18-4f84-b774-f1c08bb169d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ab21622-4f93-4497-80f6-0a5408289433",
            "compositeImage": {
                "id": "a0080c34-d3a0-4dd3-a9e7-e7ed3bc77a80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e0eb894-1e18-4f84-b774-f1c08bb169d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28be15c0-4e7a-48fd-8552-a5b91e57c9e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e0eb894-1e18-4f84-b774-f1c08bb169d4",
                    "LayerId": "b17ffa9b-6e48-47f4-a593-dd189e7218c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 136,
    "layers": [
        {
            "id": "b17ffa9b-6e48-47f4-a593-dd189e7218c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ab21622-4f93-4497-80f6-0a5408289433",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 37,
    "yorig": 58
}