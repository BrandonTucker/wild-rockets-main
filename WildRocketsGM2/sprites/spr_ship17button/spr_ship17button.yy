{
    "id": "c6d7f37f-e10e-4354-9ed6-bc619cfe79a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship17button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0cb3144-0752-4c97-9604-237109ef1953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6d7f37f-e10e-4354-9ed6-bc619cfe79a4",
            "compositeImage": {
                "id": "556a6834-642a-4974-900e-e17b52a79575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0cb3144-0752-4c97-9604-237109ef1953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6a68f11-dac3-4aa8-83f1-65a8260f61d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0cb3144-0752-4c97-9604-237109ef1953",
                    "LayerId": "f5b9a530-3dfb-456d-b275-045e2b2de9fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "f5b9a530-3dfb-456d-b275-045e2b2de9fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6d7f37f-e10e-4354-9ed6-bc619cfe79a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}