{
    "id": "cb589006-0c61-4e2b-8986-d282c2e9e39f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship16button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d8b2f22-3a97-4f09-881e-0bc2dbeeb560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb589006-0c61-4e2b-8986-d282c2e9e39f",
            "compositeImage": {
                "id": "83568ef0-2604-4766-82a3-811f19309f5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d8b2f22-3a97-4f09-881e-0bc2dbeeb560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78f4ad48-20ef-4517-b6b3-dcb5bd0180d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d8b2f22-3a97-4f09-881e-0bc2dbeeb560",
                    "LayerId": "bce12cd1-451a-4bef-a496-2314bc3e8c34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "bce12cd1-451a-4bef-a496-2314bc3e8c34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb589006-0c61-4e2b-8986-d282c2e9e39f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}