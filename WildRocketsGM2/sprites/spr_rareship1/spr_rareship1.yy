{
    "id": "1332a035-f772-437c-addd-a65068078d7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rareship1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0e23e60-bf5a-48d8-9b13-2b4406261de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "compositeImage": {
                "id": "42abb5d0-a724-48e5-8bc5-3591bbc19740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0e23e60-bf5a-48d8-9b13-2b4406261de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e02b07eb-92ee-48b5-b497-2c5fa707d7e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0e23e60-bf5a-48d8-9b13-2b4406261de9",
                    "LayerId": "eea68155-fd84-49d2-a659-20c08f10e3a8"
                }
            ]
        },
        {
            "id": "fd150043-2afb-4300-ba05-2dbc91d2d11d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "compositeImage": {
                "id": "15aad746-5167-43a8-9d8d-8572a6b97aa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd150043-2afb-4300-ba05-2dbc91d2d11d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a7e8413-7662-4c1a-b846-e1f798d4df45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd150043-2afb-4300-ba05-2dbc91d2d11d",
                    "LayerId": "eea68155-fd84-49d2-a659-20c08f10e3a8"
                }
            ]
        },
        {
            "id": "51122ea5-4f94-4209-b7d2-5a8bb41da500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "compositeImage": {
                "id": "d6752ea4-fdd6-428f-9473-4c9dbc096cca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51122ea5-4f94-4209-b7d2-5a8bb41da500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ae41a5e-1ee3-4582-80fa-c811d7efc798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51122ea5-4f94-4209-b7d2-5a8bb41da500",
                    "LayerId": "eea68155-fd84-49d2-a659-20c08f10e3a8"
                }
            ]
        },
        {
            "id": "260b69b4-7aff-40c8-a559-acf1c6331e83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "compositeImage": {
                "id": "6be29dfd-b412-4c79-91d3-41f314df1a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "260b69b4-7aff-40c8-a559-acf1c6331e83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05fc87f9-2f28-479d-8765-ed8e2b6be4b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "260b69b4-7aff-40c8-a559-acf1c6331e83",
                    "LayerId": "eea68155-fd84-49d2-a659-20c08f10e3a8"
                }
            ]
        },
        {
            "id": "696615d3-55cc-46c6-aeca-580e010da17c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "compositeImage": {
                "id": "7132e9d8-22c8-439e-8fc6-e38d570d997a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "696615d3-55cc-46c6-aeca-580e010da17c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b7cb32b-09cf-4dad-b197-a04379a80196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "696615d3-55cc-46c6-aeca-580e010da17c",
                    "LayerId": "eea68155-fd84-49d2-a659-20c08f10e3a8"
                }
            ]
        },
        {
            "id": "a62a279b-a0b3-4ee7-9409-72389a0702fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "compositeImage": {
                "id": "7f405f2e-4ade-4bfb-92f0-d9d4147569af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a62a279b-a0b3-4ee7-9409-72389a0702fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01b6e3cd-45df-4463-9ba2-04d95b1e0756",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a62a279b-a0b3-4ee7-9409-72389a0702fb",
                    "LayerId": "eea68155-fd84-49d2-a659-20c08f10e3a8"
                }
            ]
        },
        {
            "id": "843a8b67-c29b-4b96-b03c-be9c9f78274f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "compositeImage": {
                "id": "6c5d0d64-49df-4c5e-a06e-7e8acd290dab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "843a8b67-c29b-4b96-b03c-be9c9f78274f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f458494-0635-4c92-997d-d4330627aa88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843a8b67-c29b-4b96-b03c-be9c9f78274f",
                    "LayerId": "eea68155-fd84-49d2-a659-20c08f10e3a8"
                }
            ]
        },
        {
            "id": "1537329a-b456-4091-8ca7-8ecbe3cff335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "compositeImage": {
                "id": "b7ea795b-c95b-45b1-bcef-5f7492a5b680",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1537329a-b456-4091-8ca7-8ecbe3cff335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f148a9bd-e2f8-40f9-915c-f722bec8e37b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1537329a-b456-4091-8ca7-8ecbe3cff335",
                    "LayerId": "eea68155-fd84-49d2-a659-20c08f10e3a8"
                }
            ]
        },
        {
            "id": "7940d6d9-4d1b-46ee-94e8-2aba38ac213d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "compositeImage": {
                "id": "eabc89e5-3a01-43e9-8c2b-201e02885a66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7940d6d9-4d1b-46ee-94e8-2aba38ac213d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd22f89f-5e42-4e5d-adb2-2a83cfd9bc76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7940d6d9-4d1b-46ee-94e8-2aba38ac213d",
                    "LayerId": "eea68155-fd84-49d2-a659-20c08f10e3a8"
                }
            ]
        },
        {
            "id": "b0f868a9-ec65-4105-aebe-228a944d1cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "compositeImage": {
                "id": "d7dedda5-1a91-4a52-923c-da900cca5472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0f868a9-ec65-4105-aebe-228a944d1cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "316de914-fe3b-4cfe-b29c-e7bf05be6415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0f868a9-ec65-4105-aebe-228a944d1cfd",
                    "LayerId": "eea68155-fd84-49d2-a659-20c08f10e3a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 112,
    "layers": [
        {
            "id": "eea68155-fd84-49d2-a659-20c08f10e3a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1332a035-f772-437c-addd-a65068078d7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 19,
    "yorig": 48
}