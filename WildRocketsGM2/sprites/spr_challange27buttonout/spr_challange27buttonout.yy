{
    "id": "d30676be-6c36-4287-b55f-08fc74351897",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange27buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8e68ebf-14e7-49ea-be68-b0e115939fcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "compositeImage": {
                "id": "4d2d41aa-760b-48d3-ba48-172b3c4fc101",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e68ebf-14e7-49ea-be68-b0e115939fcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "510a5d41-6bb4-4636-b2d1-2c8adec06c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e68ebf-14e7-49ea-be68-b0e115939fcf",
                    "LayerId": "da81f09b-21b1-404e-86a9-97c4c997b4e8"
                }
            ]
        },
        {
            "id": "d9adbdf7-50df-415d-8950-049c6bac55af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "compositeImage": {
                "id": "b7967173-8133-4e3c-9d40-e06971f50d9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9adbdf7-50df-415d-8950-049c6bac55af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df88c32d-5eb3-4e64-bff8-991367abe42a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9adbdf7-50df-415d-8950-049c6bac55af",
                    "LayerId": "da81f09b-21b1-404e-86a9-97c4c997b4e8"
                }
            ]
        },
        {
            "id": "22b13fa9-c5a5-4629-8b74-bac0d542a839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "compositeImage": {
                "id": "9268a6ff-dc92-46e8-b1ea-9a3c21fb7905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22b13fa9-c5a5-4629-8b74-bac0d542a839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3770e1e-98bf-475c-9ea8-91fc4097dcde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22b13fa9-c5a5-4629-8b74-bac0d542a839",
                    "LayerId": "da81f09b-21b1-404e-86a9-97c4c997b4e8"
                }
            ]
        },
        {
            "id": "80331add-d38b-4e70-9ee9-bb30d4c7e8c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "compositeImage": {
                "id": "e97cfa87-91cd-4a3e-a666-307c39cd71fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80331add-d38b-4e70-9ee9-bb30d4c7e8c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d18ed1-32f9-4f96-b30c-4ec802ed8045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80331add-d38b-4e70-9ee9-bb30d4c7e8c7",
                    "LayerId": "da81f09b-21b1-404e-86a9-97c4c997b4e8"
                }
            ]
        },
        {
            "id": "1b811041-013a-4b3d-86f9-8a8ce6eade0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "compositeImage": {
                "id": "8a33da18-0a08-479e-af98-2d280772a7eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b811041-013a-4b3d-86f9-8a8ce6eade0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1e02e8e-e082-4c6a-9228-a82e6718da2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b811041-013a-4b3d-86f9-8a8ce6eade0c",
                    "LayerId": "da81f09b-21b1-404e-86a9-97c4c997b4e8"
                }
            ]
        },
        {
            "id": "ccc4f888-8f65-431b-9f54-22bc90dbf68f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "compositeImage": {
                "id": "be572b4d-0564-42ac-8c98-79db027d5b9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccc4f888-8f65-431b-9f54-22bc90dbf68f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "780bdb09-e81d-4529-b480-962dfa7dc376",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccc4f888-8f65-431b-9f54-22bc90dbf68f",
                    "LayerId": "da81f09b-21b1-404e-86a9-97c4c997b4e8"
                }
            ]
        },
        {
            "id": "6b88b088-0f64-4f85-9913-07fd58ed9278",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "compositeImage": {
                "id": "f1c08ad1-ba38-494d-8fde-84808bee6cc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b88b088-0f64-4f85-9913-07fd58ed9278",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b84fe72-1a88-4e3a-8993-bdb87c06ad39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b88b088-0f64-4f85-9913-07fd58ed9278",
                    "LayerId": "da81f09b-21b1-404e-86a9-97c4c997b4e8"
                }
            ]
        },
        {
            "id": "8440ab63-7bbc-4d2b-b36c-74b7cf8a52e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "compositeImage": {
                "id": "aec35eda-856b-444d-8d2c-8191354e69a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8440ab63-7bbc-4d2b-b36c-74b7cf8a52e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94dcd0a0-6f1c-42c2-b10f-32bbd93e9096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8440ab63-7bbc-4d2b-b36c-74b7cf8a52e2",
                    "LayerId": "da81f09b-21b1-404e-86a9-97c4c997b4e8"
                }
            ]
        },
        {
            "id": "ba4db131-448f-4241-909e-02e704209ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "compositeImage": {
                "id": "7d736bed-ff4d-46d0-9eb1-855c7a400f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba4db131-448f-4241-909e-02e704209ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19d2226c-7364-45eb-9a4a-b01e29076ac7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba4db131-448f-4241-909e-02e704209ef4",
                    "LayerId": "da81f09b-21b1-404e-86a9-97c4c997b4e8"
                }
            ]
        },
        {
            "id": "4d513cfd-19f4-457c-a172-d64bd23488a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "compositeImage": {
                "id": "b96fa0d3-99ce-48b7-96c5-6c59012c7099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d513cfd-19f4-457c-a172-d64bd23488a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d7d4407-3379-4d14-9bdd-a7154876be2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d513cfd-19f4-457c-a172-d64bd23488a7",
                    "LayerId": "da81f09b-21b1-404e-86a9-97c4c997b4e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "da81f09b-21b1-404e-86a9-97c4c997b4e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d30676be-6c36-4287-b55f-08fc74351897",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}