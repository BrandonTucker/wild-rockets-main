{
    "id": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship30",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1687a26f-e4ee-4c72-ae23-1a41801a9591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "378b8712-ec9d-4252-96f9-5b23238b4337",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1687a26f-e4ee-4c72-ae23-1a41801a9591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be58dc7f-87d9-4985-ac25-525921ac397e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1687a26f-e4ee-4c72-ae23-1a41801a9591",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "80d34795-44e3-46dc-94d9-45b75941298a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "43de6a0a-940b-4825-9c2f-0b7d9d1283f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80d34795-44e3-46dc-94d9-45b75941298a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0f802af-ffb6-4952-bd81-151b327dba29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80d34795-44e3-46dc-94d9-45b75941298a",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "70ff8e46-361c-4246-a5db-87dd958457c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "b6a8dc86-a082-44bf-bc06-6e0d38e47ddb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70ff8e46-361c-4246-a5db-87dd958457c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f17ef52-4485-44d4-b4ce-3826e8e1353e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70ff8e46-361c-4246-a5db-87dd958457c4",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "9d337c31-8c3e-487b-af44-7a58d4d5461c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "326c196f-b31d-4ce7-85c4-1a25fdab341e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d337c31-8c3e-487b-af44-7a58d4d5461c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79730090-00da-44c7-b6bf-aff552c9995e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d337c31-8c3e-487b-af44-7a58d4d5461c",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "02684f87-6766-4045-8c9a-85d49f00de0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "843fc58c-2db9-48db-9145-e925007b4486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02684f87-6766-4045-8c9a-85d49f00de0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47f0d5a8-8243-4d49-b935-d6790fee570e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02684f87-6766-4045-8c9a-85d49f00de0b",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "b3a15ee0-dd90-4844-a511-842e3fdd01c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "329501aa-18c5-464e-8475-1d1abcb4aa2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3a15ee0-dd90-4844-a511-842e3fdd01c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25d20ed5-3145-4a0d-bc15-a9a4781de010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a15ee0-dd90-4844-a511-842e3fdd01c2",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "cc8f708e-a9d7-4b9f-b752-221a633d5b06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "8a627acc-ddab-4c6d-81a0-f9c84a059507",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc8f708e-a9d7-4b9f-b752-221a633d5b06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49e9d74e-3793-44f4-a631-2cdf0f803ea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc8f708e-a9d7-4b9f-b752-221a633d5b06",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "79012475-4181-47a9-837e-62c673f1add0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "5f968be9-8c33-4ba0-b961-ee339da075db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79012475-4181-47a9-837e-62c673f1add0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb346d5a-0ed8-4e82-aefd-fd6e6c9792ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79012475-4181-47a9-837e-62c673f1add0",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "00c572d3-28d7-48d6-9175-fc9df07a0ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "17b42ffc-5f88-4f6b-a45b-ba5942444ce1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c572d3-28d7-48d6-9175-fc9df07a0ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0656d4cd-b2a1-46ce-92ca-db891fbc753a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c572d3-28d7-48d6-9175-fc9df07a0ee3",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "e82209db-8fd5-46c5-bcda-c33491fa7a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "35eb80e7-4358-4afd-a272-153f79f7eb45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e82209db-8fd5-46c5-bcda-c33491fa7a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "505210c5-501f-4354-b6f4-255ccd4ee45b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e82209db-8fd5-46c5-bcda-c33491fa7a9b",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "d9dd277a-1a04-4dc8-932d-518f948c4e72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "85ed4ee9-ffb3-4e46-af1f-a3de8ae7a5c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9dd277a-1a04-4dc8-932d-518f948c4e72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bff7063a-d437-46fe-95b0-8ea3fc845e46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9dd277a-1a04-4dc8-932d-518f948c4e72",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "02497a8f-b23c-46b5-b049-06cc620e97ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "dcf1a0e1-128d-4d4a-9a2d-965a6e4563a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02497a8f-b23c-46b5-b049-06cc620e97ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ef2bc74-8b2a-405b-bff5-ec5131530320",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02497a8f-b23c-46b5-b049-06cc620e97ab",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "307e6f24-377d-4516-b2fc-3908e9fa4273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "336e98ad-78f2-4f55-a270-9a00c06ec7d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "307e6f24-377d-4516-b2fc-3908e9fa4273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6779cfb1-7cca-48b1-93ae-74d3a66cf188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "307e6f24-377d-4516-b2fc-3908e9fa4273",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        },
        {
            "id": "fd058f3e-e6c5-4262-b74e-fd6a06c904e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "compositeImage": {
                "id": "875730bd-a3d1-465a-847f-f97991084140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd058f3e-e6c5-4262-b74e-fd6a06c904e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2208da93-e3c4-4ca3-b09e-a824f217aad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd058f3e-e6c5-4262-b74e-fd6a06c904e7",
                    "LayerId": "35317332-8484-44f3-9ade-5fd0101175f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "35317332-8484-44f3-9ade-5fd0101175f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54c34f18-0612-4e2a-8e55-3fdae60bf026",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 51,
    "yorig": 50
}