{
    "id": "419b688d-4e67-453f-8bf4-690615a18ec4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange27button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d876c59-787a-41b6-a27c-c52866a7f26d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419b688d-4e67-453f-8bf4-690615a18ec4",
            "compositeImage": {
                "id": "fc74f65e-b6c3-4b71-aa7b-bba5a2fc277f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d876c59-787a-41b6-a27c-c52866a7f26d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17b290be-73a4-45c2-9c9c-d31bdd2c1c92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d876c59-787a-41b6-a27c-c52866a7f26d",
                    "LayerId": "cb6ed502-b6f2-435d-b3d6-fac87770cdad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "cb6ed502-b6f2-435d-b3d6-fac87770cdad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "419b688d-4e67-453f-8bf4-690615a18ec4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}