{
    "id": "d5b99ed3-952f-4bd3-81c0-94b5f0ad2203",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage7button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2d3c0e6-4dbe-45d0-8cd2-bbefd4e826ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5b99ed3-952f-4bd3-81c0-94b5f0ad2203",
            "compositeImage": {
                "id": "294c8f21-b7d9-412e-93bc-65bc77de79c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2d3c0e6-4dbe-45d0-8cd2-bbefd4e826ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f84d62c-fc0e-4411-b3a2-18dafba13d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2d3c0e6-4dbe-45d0-8cd2-bbefd4e826ab",
                    "LayerId": "be756b8d-3c0f-47e2-bf4b-c7ecafd55335"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "be756b8d-3c0f-47e2-bf4b-c7ecafd55335",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5b99ed3-952f-4bd3-81c0-94b5f0ad2203",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}