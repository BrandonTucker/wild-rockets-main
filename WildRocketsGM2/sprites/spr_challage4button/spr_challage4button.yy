{
    "id": "086d938d-62cf-42b0-8904-a954553e1613",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage4button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 3,
    "bbox_right": 106,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fe5c539-cf5d-4fc9-8426-ba955a9333cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "086d938d-62cf-42b0-8904-a954553e1613",
            "compositeImage": {
                "id": "159019c4-1bc4-4ac1-b4e6-d7ca6d7c8fc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fe5c539-cf5d-4fc9-8426-ba955a9333cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f60fbf4-208e-4440-b9d0-f9d889d099c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fe5c539-cf5d-4fc9-8426-ba955a9333cb",
                    "LayerId": "8e7e2565-107f-4fcb-90d1-43c0b4962a13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "8e7e2565-107f-4fcb-90d1-43c0b4962a13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "086d938d-62cf-42b0-8904-a954553e1613",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 55,
    "yorig": 53
}