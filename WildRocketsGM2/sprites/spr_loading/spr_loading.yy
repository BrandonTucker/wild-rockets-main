{
    "id": "b42c5f79-1826-474d-9a3f-3cc6fc9caf43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_loading",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 235,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39b35785-2bb6-4bfc-92e9-753de4617488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b42c5f79-1826-474d-9a3f-3cc6fc9caf43",
            "compositeImage": {
                "id": "3c3ceb74-9089-4488-a71b-70eb67f47bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39b35785-2bb6-4bfc-92e9-753de4617488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5d9aa5f-e850-444a-a293-51b0315d7111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39b35785-2bb6-4bfc-92e9-753de4617488",
                    "LayerId": "cfc093db-6204-4891-beee-d79e7f1f201b"
                }
            ]
        },
        {
            "id": "9f28f043-001b-412b-9830-b830c903c7c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b42c5f79-1826-474d-9a3f-3cc6fc9caf43",
            "compositeImage": {
                "id": "511543cf-e2a6-427c-8bb2-ead47fc29642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f28f043-001b-412b-9830-b830c903c7c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8957d731-f6bb-4490-b444-e49f80066a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f28f043-001b-412b-9830-b830c903c7c8",
                    "LayerId": "cfc093db-6204-4891-beee-d79e7f1f201b"
                }
            ]
        },
        {
            "id": "ac2a9035-88a5-4eff-9b79-bba5452aeca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b42c5f79-1826-474d-9a3f-3cc6fc9caf43",
            "compositeImage": {
                "id": "59ced29d-697b-4a82-8b72-42896c24f3b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac2a9035-88a5-4eff-9b79-bba5452aeca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62844982-080e-409c-a344-92cd1938c585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac2a9035-88a5-4eff-9b79-bba5452aeca1",
                    "LayerId": "cfc093db-6204-4891-beee-d79e7f1f201b"
                }
            ]
        },
        {
            "id": "246a8a79-d0f3-43c2-86ec-8fe9df8e955e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b42c5f79-1826-474d-9a3f-3cc6fc9caf43",
            "compositeImage": {
                "id": "f3e7ed48-504a-4db5-9f54-e94a297126ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "246a8a79-d0f3-43c2-86ec-8fe9df8e955e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c7ecd2-f7ee-4955-a633-13e62bd6dd50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "246a8a79-d0f3-43c2-86ec-8fe9df8e955e",
                    "LayerId": "cfc093db-6204-4891-beee-d79e7f1f201b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "cfc093db-6204-4891-beee-d79e7f1f201b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b42c5f79-1826-474d-9a3f-3cc6fc9caf43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 236,
    "xorig": 118,
    "yorig": 24
}