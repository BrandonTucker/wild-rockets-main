{
    "id": "8a124835-4f29-40d3-9a6c-675ab67e0468",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_block5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "375aa82f-8789-4ef6-a07f-70fb9956dcf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a124835-4f29-40d3-9a6c-675ab67e0468",
            "compositeImage": {
                "id": "f1aa15d6-5e05-4562-8121-4a1c9df66842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "375aa82f-8789-4ef6-a07f-70fb9956dcf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d7529e5-4255-4084-a57a-8a5449b3c4a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "375aa82f-8789-4ef6-a07f-70fb9956dcf2",
                    "LayerId": "694106a2-863d-458b-b243-4d0a9eef9054"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "694106a2-863d-458b-b243-4d0a9eef9054",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a124835-4f29-40d3-9a6c-675ab67e0468",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 464,
    "yorig": 16
}