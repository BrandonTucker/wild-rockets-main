{
    "id": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startbutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 12,
    "bbox_right": 127,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ddcf3a6-2b8a-4174-95eb-b6de94d4a582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "a25721a5-b427-4706-b4c0-cccc8a16ee8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ddcf3a6-2b8a-4174-95eb-b6de94d4a582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788dde01-7293-44b6-af97-18fa7bcea35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ddcf3a6-2b8a-4174-95eb-b6de94d4a582",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        },
        {
            "id": "e975be7c-6dc2-4490-8580-1e412286de71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "8128648d-1ee8-4234-9a68-5e90e781e697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e975be7c-6dc2-4490-8580-1e412286de71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ee4935a-c5b5-4d2a-9e1f-ec88efccb85d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e975be7c-6dc2-4490-8580-1e412286de71",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        },
        {
            "id": "a10ad9d0-1029-474d-b403-0d1bcba2711b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "de1c088d-894f-49ac-8bce-d9dbe85f0947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a10ad9d0-1029-474d-b403-0d1bcba2711b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cf33e8a-41b1-4ad7-ad13-db029a2705be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a10ad9d0-1029-474d-b403-0d1bcba2711b",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        },
        {
            "id": "8d2c8675-c4b3-4fd5-b907-0e8761cbfbff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "8245c571-2b1c-48a8-82a3-1fa834305131",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d2c8675-c4b3-4fd5-b907-0e8761cbfbff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "416c6135-6f19-4cb3-afb2-987ba98e1799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d2c8675-c4b3-4fd5-b907-0e8761cbfbff",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        },
        {
            "id": "471d0ffc-32be-4530-a4fa-682348e57d5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "7ee4e37c-62d9-456e-90f1-3fc599da2400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "471d0ffc-32be-4530-a4fa-682348e57d5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccd3ea9a-9b61-4eca-9438-4c1be4913c48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "471d0ffc-32be-4530-a4fa-682348e57d5b",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        },
        {
            "id": "8b7a8fde-acd9-4382-9e79-430f7139cff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "2e39f130-23a4-4968-a43b-277212bbb128",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b7a8fde-acd9-4382-9e79-430f7139cff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bfb4d7f-77be-4812-ba47-ba214a85c6d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b7a8fde-acd9-4382-9e79-430f7139cff6",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        },
        {
            "id": "9f8a4c10-585d-43ff-8990-7f54f375bc53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "48ef8210-199a-4c92-b11a-2be29f7d2ce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f8a4c10-585d-43ff-8990-7f54f375bc53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119cb51e-5458-4396-8885-c3a7a3fd44bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f8a4c10-585d-43ff-8990-7f54f375bc53",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        },
        {
            "id": "b2c226e4-b857-4993-8c31-58248429dd0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "9b178c23-016b-4e7e-b726-6a07500beaa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2c226e4-b857-4993-8c31-58248429dd0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f16bf7b3-254c-4e81-86a3-98a188e9e136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2c226e4-b857-4993-8c31-58248429dd0a",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        },
        {
            "id": "62793bdc-8c4a-4a4f-b66c-41dad261015a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "1c6f0a27-7a0e-4d41-9e27-dc4fed4dd376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62793bdc-8c4a-4a4f-b66c-41dad261015a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5570482d-9522-48a8-99ff-f54309d840b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62793bdc-8c4a-4a4f-b66c-41dad261015a",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        },
        {
            "id": "d3cb79b4-0631-4406-9c1c-3617771dce09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "3a565e30-7ed1-4355-8fc5-d4943be4651d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3cb79b4-0631-4406-9c1c-3617771dce09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea6067c0-9227-4297-9a4f-46deb6a0c7b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3cb79b4-0631-4406-9c1c-3617771dce09",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        },
        {
            "id": "06c0adda-7e91-4956-8eda-b53f66258679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "compositeImage": {
                "id": "87e0274b-0906-4629-830d-77c72cf746fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06c0adda-7e91-4956-8eda-b53f66258679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70f6b271-a7fb-42c4-af18-20b5b599305a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06c0adda-7e91-4956-8eda-b53f66258679",
                    "LayerId": "a9867eb6-5bd1-47ed-a1af-a0253e666638"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 136,
    "layers": [
        {
            "id": "a9867eb6-5bd1-47ed-a1af-a0253e666638",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fc7eb73-66c3-48fa-a8fa-c7a7f09d974a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 140,
    "xorig": 70,
    "yorig": 68
}