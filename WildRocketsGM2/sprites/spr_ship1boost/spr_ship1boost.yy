{
    "id": "5e1cbf23-b66c-4af6-b360-ea8206de6a12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship1boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 49,
    "bbox_right": 74,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "528ff471-479a-4e8e-beeb-f0b3a83a0e4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e1cbf23-b66c-4af6-b360-ea8206de6a12",
            "compositeImage": {
                "id": "bef6074f-ec97-4878-83dc-805132a2726f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "528ff471-479a-4e8e-beeb-f0b3a83a0e4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6659b117-f59e-466c-a540-6fc2b844d041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "528ff471-479a-4e8e-beeb-f0b3a83a0e4a",
                    "LayerId": "f445120e-1c6a-44e6-9004-2f8f88adcdcc"
                }
            ]
        },
        {
            "id": "bb69a3e6-e12e-4367-aac3-fbb536dbdfdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e1cbf23-b66c-4af6-b360-ea8206de6a12",
            "compositeImage": {
                "id": "b0fa0fe3-e11e-4b5d-ab62-dc6076415d2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb69a3e6-e12e-4367-aac3-fbb536dbdfdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75b16fcd-3d6b-4254-962b-4c8c8473aacf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb69a3e6-e12e-4367-aac3-fbb536dbdfdd",
                    "LayerId": "f445120e-1c6a-44e6-9004-2f8f88adcdcc"
                }
            ]
        },
        {
            "id": "b659d280-25e9-4de1-8ef7-6496855dba66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e1cbf23-b66c-4af6-b360-ea8206de6a12",
            "compositeImage": {
                "id": "bf202521-a2b5-4675-a12b-125883321bab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b659d280-25e9-4de1-8ef7-6496855dba66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ef3962-847f-49b3-bd7a-2439cd22ec68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b659d280-25e9-4de1-8ef7-6496855dba66",
                    "LayerId": "f445120e-1c6a-44e6-9004-2f8f88adcdcc"
                }
            ]
        },
        {
            "id": "37a69cd4-751f-49b0-9039-94e76c315e96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e1cbf23-b66c-4af6-b360-ea8206de6a12",
            "compositeImage": {
                "id": "1ddc10df-73b2-47e5-af90-da8e2ff76db7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a69cd4-751f-49b0-9039-94e76c315e96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e2be14e-2d47-44e3-8aac-b0c51b3cc8fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a69cd4-751f-49b0-9039-94e76c315e96",
                    "LayerId": "f445120e-1c6a-44e6-9004-2f8f88adcdcc"
                }
            ]
        },
        {
            "id": "ab915690-38e5-49a3-9d1a-59bcaeeaf99f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e1cbf23-b66c-4af6-b360-ea8206de6a12",
            "compositeImage": {
                "id": "fbdacd4c-7bb4-4cb5-b533-39c0c32ef2cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab915690-38e5-49a3-9d1a-59bcaeeaf99f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5378371-8252-4d00-aa49-682b1a68a3a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab915690-38e5-49a3-9d1a-59bcaeeaf99f",
                    "LayerId": "f445120e-1c6a-44e6-9004-2f8f88adcdcc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "f445120e-1c6a-44e6-9004-2f8f88adcdcc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e1cbf23-b66c-4af6-b360-ea8206de6a12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 52
}