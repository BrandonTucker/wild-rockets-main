{
    "id": "fa2bc2c5-8540-4ed5-b396-b51f860c63f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_testy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3719fcc3-65b1-4821-9230-62478e293457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa2bc2c5-8540-4ed5-b396-b51f860c63f7",
            "compositeImage": {
                "id": "0e5b1741-64f9-4513-85a0-79983cc73df6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3719fcc3-65b1-4821-9230-62478e293457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d4a6bd3-b040-4c56-af6f-97fcaf4032d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3719fcc3-65b1-4821-9230-62478e293457",
                    "LayerId": "f3c9a7b5-8a8c-48c6-ab21-5d26b6c41c07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f3c9a7b5-8a8c-48c6-ab21-5d26b6c41c07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa2bc2c5-8540-4ed5-b396-b51f860c63f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}