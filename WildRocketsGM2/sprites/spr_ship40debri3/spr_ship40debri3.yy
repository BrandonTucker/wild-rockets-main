{
    "id": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship40debri3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 86,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9766b857-9a61-419f-8327-be3b3f14f348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "c75f8dbd-fc9a-4860-a06e-3e2552dd9749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9766b857-9a61-419f-8327-be3b3f14f348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef849285-cb07-4f7d-96d0-ab0c465b5991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9766b857-9a61-419f-8327-be3b3f14f348",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "ead38468-1732-4328-a44d-99d4332d3a8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "c8c01eef-f06b-4cfb-872e-2d77402b42ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ead38468-1732-4328-a44d-99d4332d3a8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36c61343-d4c0-4552-9456-89f9e332b21a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ead38468-1732-4328-a44d-99d4332d3a8f",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "04a8f9aa-b08e-4863-9d33-a3c265a6387e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "7a192a61-5971-4e34-9074-62f32bcd4fdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04a8f9aa-b08e-4863-9d33-a3c265a6387e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a82c21-36ae-48be-8cbb-ef9cba4265c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04a8f9aa-b08e-4863-9d33-a3c265a6387e",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "63455fc9-4c8b-4c01-8adb-782035cdbd14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "7bf4fbc9-5cb7-4610-9c24-40d38e69cb2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63455fc9-4c8b-4c01-8adb-782035cdbd14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba2ba943-b3f0-40a2-9277-48d5ccfad757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63455fc9-4c8b-4c01-8adb-782035cdbd14",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "67ab1012-f2c9-4485-82a9-ee0887f0820b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "3765d4e2-40b4-41c0-a0b6-415a770af293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67ab1012-f2c9-4485-82a9-ee0887f0820b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ee5d07f-ee86-4b05-996e-f7a1dbc36a77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67ab1012-f2c9-4485-82a9-ee0887f0820b",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "a9aa303a-009b-4c9f-b342-4cc21f8b632a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "64fb9575-857b-44d9-8038-80c3d0d7ba89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9aa303a-009b-4c9f-b342-4cc21f8b632a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f43efba-5a50-44e2-93b0-47257817222a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9aa303a-009b-4c9f-b342-4cc21f8b632a",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "57bb695f-456b-48d3-b066-114f80acda25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "0dd4fc1b-9868-45e9-9e41-3ce8867e330d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57bb695f-456b-48d3-b066-114f80acda25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42bf810e-285d-4aed-afed-2e6c936b8653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57bb695f-456b-48d3-b066-114f80acda25",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "d90ca5f0-3c8c-45b8-9885-da47b0b91b6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "9c09c78c-bb69-4664-bd5d-26e92dd01805",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d90ca5f0-3c8c-45b8-9885-da47b0b91b6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea1f3108-9acf-4fea-ba3b-8b860e08d997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d90ca5f0-3c8c-45b8-9885-da47b0b91b6c",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "1566a562-f069-41c2-9767-c86e7cde0277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "6f3327ac-d868-4fa5-8d62-42a50909f1fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1566a562-f069-41c2-9767-c86e7cde0277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969afc37-8c46-40e4-ad75-d1ed41c16afe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1566a562-f069-41c2-9767-c86e7cde0277",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "9ca859a3-16d8-4f06-a1c6-ccae0163b159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "4bdbc7e6-7b3f-4594-aa86-6f2e73612447",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ca859a3-16d8-4f06-a1c6-ccae0163b159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38721af3-6a8a-4b3c-9b03-335eb521ec8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ca859a3-16d8-4f06-a1c6-ccae0163b159",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "28f2707e-7d87-460b-851a-3000acace952",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "a05990ce-40d3-43e2-883b-03446699554f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28f2707e-7d87-460b-851a-3000acace952",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7607751-1a80-4247-8073-d0ee2a679b0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28f2707e-7d87-460b-851a-3000acace952",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "8cbb76bf-1eb6-41c9-8325-a1020b9d76a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "8e46d9af-724d-4095-aaa1-ce6d90bb0a3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cbb76bf-1eb6-41c9-8325-a1020b9d76a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a6a668-0555-4c1c-95d0-2a010e9ea025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cbb76bf-1eb6-41c9-8325-a1020b9d76a8",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "47772e63-0875-4244-b944-4ac8a439627e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "ad953000-2dd8-4888-9dfe-172ef89e730f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47772e63-0875-4244-b944-4ac8a439627e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3a27c26-277c-44e8-b842-ee3cfd3f7847",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47772e63-0875-4244-b944-4ac8a439627e",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "337274e0-b01e-42a9-808f-fbb7b76c2051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "d9da7950-f6e1-4d99-ab2d-67222039801c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "337274e0-b01e-42a9-808f-fbb7b76c2051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbc254cb-55e0-4a27-9c19-166e8a2abbc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "337274e0-b01e-42a9-808f-fbb7b76c2051",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        },
        {
            "id": "50678aa4-e371-40a7-9f02-a2b0af1ed11f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "compositeImage": {
                "id": "ee0aa3e6-da72-4ac6-82a4-b770b942e089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50678aa4-e371-40a7-9f02-a2b0af1ed11f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db029ac-025c-44a3-8f92-44d64ea45ac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50678aa4-e371-40a7-9f02-a2b0af1ed11f",
                    "LayerId": "364660e0-0e59-4dc9-884b-b034c6168be6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "364660e0-0e59-4dc9-884b-b034c6168be6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2dd1b9f9-3116-4860-9d1d-07692c2dd221",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 87,
    "xorig": 35,
    "yorig": 31
}