{
    "id": "37549679-8a43-43f9-b16d-2c5fdf15470f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship29",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 37,
    "bbox_right": 62,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29c38fdf-c9e5-498b-b028-476a27972ba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37549679-8a43-43f9-b16d-2c5fdf15470f",
            "compositeImage": {
                "id": "a8deb91d-7c65-4c61-a7ac-8666c4695c42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c38fdf-c9e5-498b-b028-476a27972ba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b11f37-b120-45a9-9392-1104d2da3355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c38fdf-c9e5-498b-b028-476a27972ba6",
                    "LayerId": "0ac694b7-f7fa-4ed3-85cd-cff4a8745da9"
                }
            ]
        },
        {
            "id": "2ce4339d-6b2e-4e04-b744-fc76dbfb0b33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37549679-8a43-43f9-b16d-2c5fdf15470f",
            "compositeImage": {
                "id": "76d554e1-18bd-407b-86e0-324f15b81c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce4339d-6b2e-4e04-b744-fc76dbfb0b33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "626b8b5c-3b3d-49fa-8bc1-f48fd9940e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce4339d-6b2e-4e04-b744-fc76dbfb0b33",
                    "LayerId": "0ac694b7-f7fa-4ed3-85cd-cff4a8745da9"
                }
            ]
        },
        {
            "id": "5ad00e1f-e5a4-4067-a2f0-0cd08dd4f8ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37549679-8a43-43f9-b16d-2c5fdf15470f",
            "compositeImage": {
                "id": "8676aa68-f387-4197-b1fa-4b46d121bae3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ad00e1f-e5a4-4067-a2f0-0cd08dd4f8ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a722bb9-266a-4a0e-95ee-9e7fb9278f09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ad00e1f-e5a4-4067-a2f0-0cd08dd4f8ad",
                    "LayerId": "0ac694b7-f7fa-4ed3-85cd-cff4a8745da9"
                }
            ]
        },
        {
            "id": "f25ab60a-e0f1-4577-84ce-ab414fd94724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37549679-8a43-43f9-b16d-2c5fdf15470f",
            "compositeImage": {
                "id": "6fbb4653-1d35-4057-b4c9-cf4fd12f2492",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f25ab60a-e0f1-4577-84ce-ab414fd94724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dabddd6f-4c65-420c-a6b8-b80dde945e10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f25ab60a-e0f1-4577-84ce-ab414fd94724",
                    "LayerId": "0ac694b7-f7fa-4ed3-85cd-cff4a8745da9"
                }
            ]
        },
        {
            "id": "25f5e794-427e-4f83-8efd-cd1c5e4c0333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37549679-8a43-43f9-b16d-2c5fdf15470f",
            "compositeImage": {
                "id": "5ae2ebc6-01cf-43c9-8bbe-53a4410808de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25f5e794-427e-4f83-8efd-cd1c5e4c0333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8da86d1-eea4-486b-a8ea-2a3ec31c9d94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25f5e794-427e-4f83-8efd-cd1c5e4c0333",
                    "LayerId": "0ac694b7-f7fa-4ed3-85cd-cff4a8745da9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "0ac694b7-f7fa-4ed3-85cd-cff4a8745da9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37549679-8a43-43f9-b16d-2c5fdf15470f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}