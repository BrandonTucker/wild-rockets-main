{
    "id": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": -1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca79183b-2734-442e-954c-693d01f3e40a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "4ddbc1dd-fb1e-4dba-a2b6-91652f71bfa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca79183b-2734-442e-954c-693d01f3e40a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5300d186-60a8-42d1-9bbb-379c7b79a104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca79183b-2734-442e-954c-693d01f3e40a",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "90c593da-03bd-4b1b-b0a9-b94c596ca128",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "7ac0b168-2d55-4882-bdf5-58da50786e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90c593da-03bd-4b1b-b0a9-b94c596ca128",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06fa78da-51a2-4f24-a5c1-08e627f7b055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90c593da-03bd-4b1b-b0a9-b94c596ca128",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "a0645570-42a6-4efb-b84e-1d9a8079eb7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "e444e3c7-3321-4054-aee4-964fe8ba6acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0645570-42a6-4efb-b84e-1d9a8079eb7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dac84ead-7b35-4d1a-8b80-fe5329dc81c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0645570-42a6-4efb-b84e-1d9a8079eb7e",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "c8128933-d53f-411f-92c6-0fa970e36250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "5a1313fb-4a60-49c6-9df5-f0dc85223ba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8128933-d53f-411f-92c6-0fa970e36250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "993a958b-c4aa-4fd2-bed2-47ded36656f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8128933-d53f-411f-92c6-0fa970e36250",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "e41e6f1f-d28b-42fd-b754-559929a95904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "1adf2b71-a6c4-4d28-a9f2-3fece1156953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e41e6f1f-d28b-42fd-b754-559929a95904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f482988d-e215-4d3e-b681-980141395146",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e41e6f1f-d28b-42fd-b754-559929a95904",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "8d4b2a6f-aebb-451b-ac48-5075c9e44b05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "165415e3-112f-42d6-a9ca-bc5b64c13b7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d4b2a6f-aebb-451b-ac48-5075c9e44b05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b701611b-c911-497b-80f5-b9ec3aa85af5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d4b2a6f-aebb-451b-ac48-5075c9e44b05",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "dbeabaab-41c8-4aba-ad1a-cd0a9dcbd657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "d466632c-c412-46fa-b1af-8f20e22aa986",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbeabaab-41c8-4aba-ad1a-cd0a9dcbd657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e7cfecd-a7a7-44f2-aa26-bd98e70985d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbeabaab-41c8-4aba-ad1a-cd0a9dcbd657",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "0f3c8dea-57c3-4dd0-9068-17262b7ff656",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "6a63ee89-797f-41bc-8dcc-18bbc6e0116f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f3c8dea-57c3-4dd0-9068-17262b7ff656",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd60b1be-24ce-4de8-be31-0281f3756ebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f3c8dea-57c3-4dd0-9068-17262b7ff656",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "d58db8f7-89f8-490b-acb7-21186c57b6a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "1dc19f67-7a9c-4d22-895e-b60a65470d84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d58db8f7-89f8-490b-acb7-21186c57b6a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c70ad207-7b43-4ae6-87d6-b7557d3910c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d58db8f7-89f8-490b-acb7-21186c57b6a1",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "94fbd3ed-2626-4e0b-9f2f-0cd36739a881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "b0270828-a2e0-4194-9442-6b107fbf22cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94fbd3ed-2626-4e0b-9f2f-0cd36739a881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1150c936-e1c0-4bb0-8dc0-c521766777f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94fbd3ed-2626-4e0b-9f2f-0cd36739a881",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "5b7bb891-02d8-4123-b830-0a1e44341904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "6cb4dc7a-b727-49a1-b2e0-3cf04ee9ffed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7bb891-02d8-4123-b830-0a1e44341904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23649eaa-dfef-4d79-b137-7437ea6e1965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7bb891-02d8-4123-b830-0a1e44341904",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        },
        {
            "id": "adc775f5-6c10-4795-8e9e-9268d78366c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "compositeImage": {
                "id": "f1557f3f-76a4-4b63-8f92-ee3951367fb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adc775f5-6c10-4795-8e9e-9268d78366c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7cbfa4f-356d-41b6-a785-5d4ae475e0ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adc775f5-6c10-4795-8e9e-9268d78366c0",
                    "LayerId": "bd8470a4-cd08-4617-8d0a-267781e0c090"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "bd8470a4-cd08-4617-8d0a-267781e0c090",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90b1f82a-54b5-4ea3-911a-f5e69bf2759e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 39
}