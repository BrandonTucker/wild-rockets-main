{
    "id": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange35buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d73f5917-3dcc-4d4f-8717-d2e37980d3f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "compositeImage": {
                "id": "8000fa74-bffc-430d-9b51-a66b4f8ec470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d73f5917-3dcc-4d4f-8717-d2e37980d3f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "292b2941-0ae8-4091-9dcf-6e9750d47a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d73f5917-3dcc-4d4f-8717-d2e37980d3f1",
                    "LayerId": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0"
                }
            ]
        },
        {
            "id": "f493cf4d-92e4-42f7-beb0-e2a52201ab2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "compositeImage": {
                "id": "e02f90cf-dd3c-4b6d-803d-57ca7896cb14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f493cf4d-92e4-42f7-beb0-e2a52201ab2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aabba12-c3d0-4bc5-b10c-f28be8958860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f493cf4d-92e4-42f7-beb0-e2a52201ab2b",
                    "LayerId": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0"
                }
            ]
        },
        {
            "id": "9aa701b2-adf0-4b9b-a041-388dd50e39e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "compositeImage": {
                "id": "99cdda47-fb5b-4e03-9a23-01fcf102a16b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa701b2-adf0-4b9b-a041-388dd50e39e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd974022-a4f1-43aa-9912-3eabf7dedb7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa701b2-adf0-4b9b-a041-388dd50e39e3",
                    "LayerId": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0"
                }
            ]
        },
        {
            "id": "63fd49d3-f0ad-4a5e-b1d2-793f4189a752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "compositeImage": {
                "id": "341f6264-a09b-4b09-a7a5-9d673ce98ef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63fd49d3-f0ad-4a5e-b1d2-793f4189a752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88dfc57e-a07d-4fac-a23b-5d41e28a1dd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63fd49d3-f0ad-4a5e-b1d2-793f4189a752",
                    "LayerId": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0"
                }
            ]
        },
        {
            "id": "4b12151d-8ea9-442a-8152-fe1364cacfa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "compositeImage": {
                "id": "8be9a68f-d2b1-4106-9ec7-c9ad4b60fa0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b12151d-8ea9-442a-8152-fe1364cacfa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45d85318-7403-4515-a7a1-17750b3d805e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b12151d-8ea9-442a-8152-fe1364cacfa2",
                    "LayerId": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0"
                }
            ]
        },
        {
            "id": "8a909de1-c69e-4b22-aee2-5720dcacea5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "compositeImage": {
                "id": "444cb6b8-0ed3-49a5-8176-465d8b00929a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a909de1-c69e-4b22-aee2-5720dcacea5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41b4b340-e3b9-4da9-a22b-c93a56a5d66f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a909de1-c69e-4b22-aee2-5720dcacea5b",
                    "LayerId": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0"
                }
            ]
        },
        {
            "id": "15db4de0-bc70-45e0-a1fc-31edc9f122c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "compositeImage": {
                "id": "995f2410-3711-49a4-a086-a143423032dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15db4de0-bc70-45e0-a1fc-31edc9f122c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b926263-2326-4032-947c-efb4f5afa8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15db4de0-bc70-45e0-a1fc-31edc9f122c0",
                    "LayerId": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0"
                }
            ]
        },
        {
            "id": "a25792f6-29be-4657-8a9a-d4acfaf05087",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "compositeImage": {
                "id": "d64e68e1-931a-46c4-a560-3829b7f53987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a25792f6-29be-4657-8a9a-d4acfaf05087",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd10c175-9025-4380-ae06-b00c033c74c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a25792f6-29be-4657-8a9a-d4acfaf05087",
                    "LayerId": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0"
                }
            ]
        },
        {
            "id": "9db7a941-d309-4ca2-9351-686b9f787e1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "compositeImage": {
                "id": "3286ab4b-2a32-4663-ab0e-3647f6aaeb3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db7a941-d309-4ca2-9351-686b9f787e1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2568081c-be29-44d6-9435-0c15af9249c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db7a941-d309-4ca2-9351-686b9f787e1c",
                    "LayerId": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0"
                }
            ]
        },
        {
            "id": "6204cd60-b346-445e-bdbe-9ab439b005cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "compositeImage": {
                "id": "1ebe21d0-20b8-4776-a46f-e1d0b44e87fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6204cd60-b346-445e-bdbe-9ab439b005cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c70c671-9fe0-47c9-a407-8d3c9dc0c635",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6204cd60-b346-445e-bdbe-9ab439b005cf",
                    "LayerId": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "a41ee37c-d85b-43c9-94d4-0b77af12c6c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8594bb7e-683d-4fa6-973c-ccc373b0b097",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 51
}