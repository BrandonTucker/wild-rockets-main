{
    "id": "95e010d2-31f8-4413-9e89-57406e7e023a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_minusLife",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 158,
    "bbox_left": 33,
    "bbox_right": 160,
    "bbox_top": 72,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b2d10f7-469a-40e2-8c12-6a56cecc4214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "b3eda87a-58c6-4e7b-acc9-1039b9d3bcbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b2d10f7-469a-40e2-8c12-6a56cecc4214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e46cc29-ae99-4938-bab4-36c0fbb0c67a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b2d10f7-469a-40e2-8c12-6a56cecc4214",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "903ae1f2-4ef6-424f-98c8-0997df40a629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "99f7f36f-7d3b-4c1a-be46-202c5c1e0489",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "903ae1f2-4ef6-424f-98c8-0997df40a629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2071c081-c68a-4dde-a2b5-10ecfe4dc5b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "903ae1f2-4ef6-424f-98c8-0997df40a629",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "c50a688b-82e1-44cd-bb3e-e4e1c22ea284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "4e440368-5a6d-4505-bfb3-905f41768cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c50a688b-82e1-44cd-bb3e-e4e1c22ea284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f612e84d-195a-47ab-b2e5-b0deeae5b1b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c50a688b-82e1-44cd-bb3e-e4e1c22ea284",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "847b117b-f27c-4107-84c7-7b8db04d5320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "9a3ba8a2-ad78-4403-bdc3-784744aa2531",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "847b117b-f27c-4107-84c7-7b8db04d5320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97437881-083d-4350-be74-4add75dee4bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "847b117b-f27c-4107-84c7-7b8db04d5320",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "a3b9049f-68ec-49a9-a9bc-c5ffeae7cd16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "f5b1cc61-59c4-4233-bb7e-d27bc983c1c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3b9049f-68ec-49a9-a9bc-c5ffeae7cd16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e68fd36b-8177-47a8-893f-005975f5f819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3b9049f-68ec-49a9-a9bc-c5ffeae7cd16",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "d893bfe4-92db-4483-a212-40d961f9e8ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "3b367681-5c82-4ce3-9326-079f0d77cc3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d893bfe4-92db-4483-a212-40d961f9e8ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b836215-3f15-4c33-b577-dde0705cb375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d893bfe4-92db-4483-a212-40d961f9e8ec",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "89ac6d6a-3e60-4465-99c1-5935e74ee34e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "d55fafb7-0441-4552-a693-f9e810117bdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89ac6d6a-3e60-4465-99c1-5935e74ee34e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d91e4a0-56d0-4dae-acc6-f495e43b3f72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89ac6d6a-3e60-4465-99c1-5935e74ee34e",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "c5c75145-b471-4611-a640-c8c8fb9816a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "9f06d623-2bb8-4baf-b537-04070609c7f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5c75145-b471-4611-a640-c8c8fb9816a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f5fbe59-d428-4ba3-a8d7-bd9d0ecb700f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5c75145-b471-4611-a640-c8c8fb9816a7",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "6d161a04-030e-4bb2-a0d9-e165ef0bc3dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "b17388a3-d22c-4af1-8e88-1cbd3ed94911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d161a04-030e-4bb2-a0d9-e165ef0bc3dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a8c21ad-afc3-458b-a10b-4b85bc838873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d161a04-030e-4bb2-a0d9-e165ef0bc3dc",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "0ef47eef-f325-4cb5-8605-2e1faa054534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "50d27101-d6b4-4329-b12d-daa8659f3bfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ef47eef-f325-4cb5-8605-2e1faa054534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca301c13-db8c-4412-b24b-90e2dc918fc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef47eef-f325-4cb5-8605-2e1faa054534",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "642dd133-05d5-43a8-aa3c-0974d4c7b722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "83f95c49-2dde-4e43-95c7-01c00439b195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "642dd133-05d5-43a8-aa3c-0974d4c7b722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1447b244-0a86-4aa5-856d-75e2b485c34c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "642dd133-05d5-43a8-aa3c-0974d4c7b722",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "2b0e70cb-6426-4b9e-9020-2bba6b041553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "a7fe482f-7f17-4217-bb86-bb9504f32eab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b0e70cb-6426-4b9e-9020-2bba6b041553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce33789-3e54-4fa0-b3cc-425113eea9e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b0e70cb-6426-4b9e-9020-2bba6b041553",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "6a1d7064-74db-4376-825d-236025721bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "8ef99c86-d57a-40af-b6d6-b479b19226b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a1d7064-74db-4376-825d-236025721bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "062eabd0-6e25-480e-895d-68357f156615",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a1d7064-74db-4376-825d-236025721bbc",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        },
        {
            "id": "a1d683e8-1b2f-48dc-86bb-461e1cf896f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "compositeImage": {
                "id": "6eb664e5-a9bd-49c3-bc67-462594752d5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1d683e8-1b2f-48dc-86bb-461e1cf896f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7da20893-dbc6-44c9-a8f2-371efd361b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1d683e8-1b2f-48dc-86bb-461e1cf896f9",
                    "LayerId": "9fde61ce-ded9-4549-ac11-a2c08541f4fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 230,
    "layers": [
        {
            "id": "9fde61ce-ded9-4549-ac11-a2c08541f4fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95e010d2-31f8-4413-9e89-57406e7e023a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 233,
    "xorig": 116,
    "yorig": 115
}