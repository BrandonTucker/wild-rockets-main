{
    "id": "504b56d7-b050-4169-bf1d-6bbd6a7fae1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_block4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94a2ee04-20fd-4787-8c05-f1c743902efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "504b56d7-b050-4169-bf1d-6bbd6a7fae1a",
            "compositeImage": {
                "id": "dc44ae08-aa48-409f-b026-d9f9396317f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94a2ee04-20fd-4787-8c05-f1c743902efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bef1882-81b7-4164-af0a-8b4f150d2913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94a2ee04-20fd-4787-8c05-f1c743902efd",
                    "LayerId": "8cfefbd7-47c3-46de-bc42-e3ab0d9828de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8cfefbd7-47c3-46de-bc42-e3ab0d9828de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "504b56d7-b050-4169-bf1d-6bbd6a7fae1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 16,
    "yorig": 16
}