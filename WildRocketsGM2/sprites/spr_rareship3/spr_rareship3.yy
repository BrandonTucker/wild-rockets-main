{
    "id": "b9060480-afff-4df9-9135-aac7e444a20f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rareship3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 66,
    "bbox_left": 11,
    "bbox_right": 59,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3165c69-16f7-4b2b-893d-f85842bcda34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "compositeImage": {
                "id": "5b0afc77-c4fe-4a07-a5ec-c402b348b87a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3165c69-16f7-4b2b-893d-f85842bcda34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c0db68d-e3b9-4b04-a1aa-3b23814521e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3165c69-16f7-4b2b-893d-f85842bcda34",
                    "LayerId": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44"
                }
            ]
        },
        {
            "id": "a9129b41-7374-4a3d-8533-715ef8046cc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "compositeImage": {
                "id": "7e954b33-57a4-4001-890d-996ae80e6245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9129b41-7374-4a3d-8533-715ef8046cc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a377b0-3bdb-4656-a2bb-fc7ad00a083b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9129b41-7374-4a3d-8533-715ef8046cc0",
                    "LayerId": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44"
                }
            ]
        },
        {
            "id": "8b414ebd-64de-4cea-bf6e-fae811957159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "compositeImage": {
                "id": "9ba0904a-f174-4658-b01c-298eb3c89a18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b414ebd-64de-4cea-bf6e-fae811957159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a5589ce-3523-4c84-a331-48a30478f5e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b414ebd-64de-4cea-bf6e-fae811957159",
                    "LayerId": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44"
                }
            ]
        },
        {
            "id": "6fa610af-2c5d-461d-86f4-589e34d87040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "compositeImage": {
                "id": "bac54f64-075f-4c55-a411-45f09aec7587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fa610af-2c5d-461d-86f4-589e34d87040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adfa6461-2d98-4fa1-91f6-afe7982f1bd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fa610af-2c5d-461d-86f4-589e34d87040",
                    "LayerId": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44"
                }
            ]
        },
        {
            "id": "297a3c7b-2c8e-4597-8604-0ef2cd59e22f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "compositeImage": {
                "id": "4a4c0f75-8fb2-4d6d-bfea-f12f12e10045",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "297a3c7b-2c8e-4597-8604-0ef2cd59e22f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebf873cc-5787-4448-9fd3-ccfbb9afec53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "297a3c7b-2c8e-4597-8604-0ef2cd59e22f",
                    "LayerId": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44"
                }
            ]
        },
        {
            "id": "1ee4e2ee-1a8c-466d-86ad-77ed0ebd276f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "compositeImage": {
                "id": "29f5e3dc-24b4-464f-9e88-28a21426ee86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ee4e2ee-1a8c-466d-86ad-77ed0ebd276f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d3de179-43c5-49e0-90f4-fcc21c9276c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ee4e2ee-1a8c-466d-86ad-77ed0ebd276f",
                    "LayerId": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44"
                }
            ]
        },
        {
            "id": "61f0bf69-e4b2-4ff1-8320-ccd3c63487e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "compositeImage": {
                "id": "ef3cf4a9-c0fb-478b-a308-06946605702d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61f0bf69-e4b2-4ff1-8320-ccd3c63487e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ce57689-cbab-4cf9-aba6-15dcbaf0088f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61f0bf69-e4b2-4ff1-8320-ccd3c63487e5",
                    "LayerId": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44"
                }
            ]
        },
        {
            "id": "149eb467-7ca5-4d4b-b417-9ea1b25040eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "compositeImage": {
                "id": "77f30467-8aa7-4c9e-8e47-c1ae1b4e3166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "149eb467-7ca5-4d4b-b417-9ea1b25040eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e07bfdbc-7d46-4128-bcf9-738bc38db09c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "149eb467-7ca5-4d4b-b417-9ea1b25040eb",
                    "LayerId": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44"
                }
            ]
        },
        {
            "id": "c7ceece4-7909-460f-9223-0c44c7e27bef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "compositeImage": {
                "id": "9f9f9a36-539b-4b8e-93ca-8e8d4e8c2f7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7ceece4-7909-460f-9223-0c44c7e27bef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84814438-432a-4467-aea9-2e88ccefa6c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7ceece4-7909-460f-9223-0c44c7e27bef",
                    "LayerId": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44"
                }
            ]
        },
        {
            "id": "b10849ab-1d2e-4c83-ba4b-bcb691ed689a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "compositeImage": {
                "id": "6e6dbea5-3e5b-4d9d-b70d-b57033664f56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b10849ab-1d2e-4c83-ba4b-bcb691ed689a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b544d51-12e9-47ec-826a-cc7953fa0b09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b10849ab-1d2e-4c83-ba4b-bcb691ed689a",
                    "LayerId": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 81,
    "layers": [
        {
            "id": "c0d3467b-c53f-43de-84b1-8aa9b1a01d44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9060480-afff-4df9-9135-aac7e444a20f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 69,
    "xorig": 37,
    "yorig": 33
}