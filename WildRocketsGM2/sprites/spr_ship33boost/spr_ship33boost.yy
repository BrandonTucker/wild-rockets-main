{
    "id": "f224853e-e368-46ea-a33d-810489abc67c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship33boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 153,
    "bbox_left": 54,
    "bbox_right": 79,
    "bbox_top": 61,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "510ed196-537d-48f2-a0bd-78dda742e54a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f224853e-e368-46ea-a33d-810489abc67c",
            "compositeImage": {
                "id": "a5152d96-75a8-4569-8bb3-5cf56535818b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "510ed196-537d-48f2-a0bd-78dda742e54a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06845afc-e577-4983-ae0a-036a0162f656",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "510ed196-537d-48f2-a0bd-78dda742e54a",
                    "LayerId": "855f56f9-8d27-423c-b1ca-da84edabf6be"
                }
            ]
        },
        {
            "id": "8da4d485-c1df-4375-b12f-5b1bf7138fd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f224853e-e368-46ea-a33d-810489abc67c",
            "compositeImage": {
                "id": "ff11620f-25a6-438c-8dba-874ac0e904fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da4d485-c1df-4375-b12f-5b1bf7138fd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21295577-e48d-476a-965f-0911caa3286f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da4d485-c1df-4375-b12f-5b1bf7138fd9",
                    "LayerId": "855f56f9-8d27-423c-b1ca-da84edabf6be"
                }
            ]
        },
        {
            "id": "b3336a8e-732f-4036-8864-69ccf39b6e75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f224853e-e368-46ea-a33d-810489abc67c",
            "compositeImage": {
                "id": "6da2c939-c5bc-40ab-bdc6-2a1e310cadb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3336a8e-732f-4036-8864-69ccf39b6e75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e60f0842-2a2c-4126-81a7-2e6576dfac82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3336a8e-732f-4036-8864-69ccf39b6e75",
                    "LayerId": "855f56f9-8d27-423c-b1ca-da84edabf6be"
                }
            ]
        },
        {
            "id": "08f1b8a9-dbcd-4428-9578-7aaaee3d6a78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f224853e-e368-46ea-a33d-810489abc67c",
            "compositeImage": {
                "id": "e52fc69a-9ef9-4158-bce1-7eefde3b063b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f1b8a9-dbcd-4428-9578-7aaaee3d6a78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cb7b97a-80d8-47fa-8621-1880d7969c19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f1b8a9-dbcd-4428-9578-7aaaee3d6a78",
                    "LayerId": "855f56f9-8d27-423c-b1ca-da84edabf6be"
                }
            ]
        },
        {
            "id": "77036b14-cfee-4e69-a623-c2f0b861147f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f224853e-e368-46ea-a33d-810489abc67c",
            "compositeImage": {
                "id": "8c8ae04d-f47c-4b65-b3e1-db71b8449086",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77036b14-cfee-4e69-a623-c2f0b861147f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a6cbfb-026d-4cb9-b0a1-b55e94708c57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77036b14-cfee-4e69-a623-c2f0b861147f",
                    "LayerId": "855f56f9-8d27-423c-b1ca-da84edabf6be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 207,
    "layers": [
        {
            "id": "855f56f9-8d27-423c-b1ca-da84edabf6be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f224853e-e368-46ea-a33d-810489abc67c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 135,
    "xorig": 67,
    "yorig": 102
}