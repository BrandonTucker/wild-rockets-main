{
    "id": "82d264c2-bc33-41a6-8dde-c641fbb0fc56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship18",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 30,
    "bbox_right": 55,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ddaafc64-e2e1-4fe2-a0fc-a9c05f0d2c25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d264c2-bc33-41a6-8dde-c641fbb0fc56",
            "compositeImage": {
                "id": "71d78456-98b0-4058-847b-6230b629d3b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddaafc64-e2e1-4fe2-a0fc-a9c05f0d2c25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "084de3bf-1ce0-4866-a38f-3609cfb9d68f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddaafc64-e2e1-4fe2-a0fc-a9c05f0d2c25",
                    "LayerId": "f4a2129f-e915-439e-80cf-975fddba3df0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 112,
    "layers": [
        {
            "id": "f4a2129f-e915-439e-80cf-975fddba3df0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82d264c2-bc33-41a6-8dde-c641fbb0fc56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 86,
    "xorig": 42,
    "yorig": 50
}