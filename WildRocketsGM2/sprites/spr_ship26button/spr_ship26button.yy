{
    "id": "3e44dd5f-c9ce-4302-a0ac-8cdbbf1f4cac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship26button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 13,
    "bbox_right": 89,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14ceecf4-5a8b-4491-9e9e-b509f42dfe13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e44dd5f-c9ce-4302-a0ac-8cdbbf1f4cac",
            "compositeImage": {
                "id": "a5efd7b8-791a-4a2c-9826-edd466d48124",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14ceecf4-5a8b-4491-9e9e-b509f42dfe13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac492f25-731b-4e32-a5a3-6c0adc76d094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14ceecf4-5a8b-4491-9e9e-b509f42dfe13",
                    "LayerId": "5b5669a3-908f-47a8-b338-f7723dd15954"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "5b5669a3-908f-47a8-b338-f7723dd15954",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e44dd5f-c9ce-4302-a0ac-8cdbbf1f4cac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}