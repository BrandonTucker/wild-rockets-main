{
    "id": "b3b8eab4-7ef8-4901-939c-38eb69e3906d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_octopus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21d10e2e-965e-4091-977a-0bcdb91f745a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b8eab4-7ef8-4901-939c-38eb69e3906d",
            "compositeImage": {
                "id": "52e6cca7-7e71-4d73-9961-9a3c1bf44ae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21d10e2e-965e-4091-977a-0bcdb91f745a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27aa0852-35fb-4290-be01-6526b9b510f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21d10e2e-965e-4091-977a-0bcdb91f745a",
                    "LayerId": "44b69d81-fcff-4f2d-8bef-e5614a847777"
                }
            ]
        },
        {
            "id": "06faa88f-dd46-4dba-a88a-821ca5b40f27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b8eab4-7ef8-4901-939c-38eb69e3906d",
            "compositeImage": {
                "id": "03791c8a-4358-4efc-91aa-293993a7f871",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06faa88f-dd46-4dba-a88a-821ca5b40f27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a77a216f-13a1-451c-86ca-d713d4aa988b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06faa88f-dd46-4dba-a88a-821ca5b40f27",
                    "LayerId": "44b69d81-fcff-4f2d-8bef-e5614a847777"
                }
            ]
        },
        {
            "id": "79c49c96-9ac8-45a9-ae72-d3f705dc3282",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b8eab4-7ef8-4901-939c-38eb69e3906d",
            "compositeImage": {
                "id": "99c9e1e4-22a2-4575-be4a-dda286a7624a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79c49c96-9ac8-45a9-ae72-d3f705dc3282",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14875cce-f87d-4160-af2c-c554268181a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79c49c96-9ac8-45a9-ae72-d3f705dc3282",
                    "LayerId": "44b69d81-fcff-4f2d-8bef-e5614a847777"
                }
            ]
        },
        {
            "id": "31d35d58-b63f-4356-bd0e-ac09699a4956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b8eab4-7ef8-4901-939c-38eb69e3906d",
            "compositeImage": {
                "id": "961758ab-8ca7-4a02-a7f7-580cc60d34c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d35d58-b63f-4356-bd0e-ac09699a4956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bae8946-f144-4501-b7c3-2a941f3d95fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d35d58-b63f-4356-bd0e-ac09699a4956",
                    "LayerId": "44b69d81-fcff-4f2d-8bef-e5614a847777"
                }
            ]
        },
        {
            "id": "77e7d4ed-f15c-4d66-9900-2e2bce573484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b8eab4-7ef8-4901-939c-38eb69e3906d",
            "compositeImage": {
                "id": "db6c589c-9272-4dda-881b-0b5cc997763c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e7d4ed-f15c-4d66-9900-2e2bce573484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cfa0be8-dc34-4849-af41-cb4e3e2df8fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e7d4ed-f15c-4d66-9900-2e2bce573484",
                    "LayerId": "44b69d81-fcff-4f2d-8bef-e5614a847777"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "44b69d81-fcff-4f2d-8bef-e5614a847777",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3b8eab4-7ef8-4901-939c-38eb69e3906d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}