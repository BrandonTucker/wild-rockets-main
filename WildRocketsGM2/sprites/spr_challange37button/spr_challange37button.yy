{
    "id": "7005f84d-79a3-43a8-b6d9-42bb8b5b5014",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange37button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d619645e-16fc-4731-b680-3a74ec34acd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7005f84d-79a3-43a8-b6d9-42bb8b5b5014",
            "compositeImage": {
                "id": "db96efae-b8c3-446e-af5a-abd2f5197164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d619645e-16fc-4731-b680-3a74ec34acd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "468fc213-cea0-4bc3-be8c-7d76d5f58918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d619645e-16fc-4731-b680-3a74ec34acd0",
                    "LayerId": "328665a5-2d04-4958-8a65-9f8da6eee454"
                }
            ]
        },
        {
            "id": "bfa0258f-785e-49f1-b61d-dccabc4cb155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7005f84d-79a3-43a8-b6d9-42bb8b5b5014",
            "compositeImage": {
                "id": "b3a61517-4283-4295-a6a4-709a8432cbf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa0258f-785e-49f1-b61d-dccabc4cb155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9fc14c5-eddc-421e-a125-899b974d147a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa0258f-785e-49f1-b61d-dccabc4cb155",
                    "LayerId": "328665a5-2d04-4958-8a65-9f8da6eee454"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "328665a5-2d04-4958-8a65-9f8da6eee454",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7005f84d-79a3-43a8-b6d9-42bb8b5b5014",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}