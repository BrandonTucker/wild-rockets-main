{
    "id": "89d24856-42fc-42c6-9f75-b85af1f07098",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship11locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 100,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "601b1aa3-2870-4f9b-b839-62c0347ff168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d24856-42fc-42c6-9f75-b85af1f07098",
            "compositeImage": {
                "id": "91abf9d0-7fe1-4762-8844-5001458ca7d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "601b1aa3-2870-4f9b-b839-62c0347ff168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd72e84b-86c1-40ac-99bc-e14a213ba643",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "601b1aa3-2870-4f9b-b839-62c0347ff168",
                    "LayerId": "8e4c2811-6459-4c07-8ae8-fb5d301bc65a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "8e4c2811-6459-4c07-8ae8-fb5d301bc65a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89d24856-42fc-42c6-9f75-b85af1f07098",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}