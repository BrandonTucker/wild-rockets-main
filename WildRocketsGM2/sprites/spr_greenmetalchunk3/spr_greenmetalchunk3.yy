{
    "id": "bf94e270-8785-4e89-bddb-54eb467edc73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_greenmetalchunk3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 1,
    "bbox_right": 88,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f0a9b7f-ed50-4634-b29b-f5cbeb218d97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "9eb789f7-778b-4ce6-84aa-e087ea453290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f0a9b7f-ed50-4634-b29b-f5cbeb218d97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d50d3f1d-1b29-43bd-a645-1ce1308036a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f0a9b7f-ed50-4634-b29b-f5cbeb218d97",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "9f6ed308-e3d3-41a3-b987-f441c1f1086e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "b4629711-3581-46f0-979e-0c90a5934104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f6ed308-e3d3-41a3-b987-f441c1f1086e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5744d783-4aa1-44a7-949a-0ee1f9c67613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f6ed308-e3d3-41a3-b987-f441c1f1086e",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "54ff3093-8a2c-46ef-9b9a-8bcfe4372bf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "57cef9cf-548b-45dc-b503-99fccc43cb68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54ff3093-8a2c-46ef-9b9a-8bcfe4372bf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8176de03-ac4c-4740-8ed9-65cd0b72f591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54ff3093-8a2c-46ef-9b9a-8bcfe4372bf0",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "86375bf5-f490-4698-a9ef-50b1667112e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "85eedb9b-879b-482b-9989-c529cc898cff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86375bf5-f490-4698-a9ef-50b1667112e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e28a3dc-b1af-47ee-ab58-23806312dd3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86375bf5-f490-4698-a9ef-50b1667112e7",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "708f190b-236b-4c8e-b2e7-62c8ab8381bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "e0b66863-5fe0-4060-89dd-07999c8da407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "708f190b-236b-4c8e-b2e7-62c8ab8381bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48eb12a0-52a0-44b7-a816-0635313f3f58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "708f190b-236b-4c8e-b2e7-62c8ab8381bc",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "7f5176f9-df91-4855-b93b-937414f4008a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "dd9bc0a2-0f53-4ede-b07a-90d748f7e4cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f5176f9-df91-4855-b93b-937414f4008a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28e40a74-1ed9-4228-9e28-d02630a9a28e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f5176f9-df91-4855-b93b-937414f4008a",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "26a17d9a-fd55-48b4-9293-93c642b09416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "57208994-cbbe-4683-8da4-47da2157da15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26a17d9a-fd55-48b4-9293-93c642b09416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51f79444-65d5-46ac-8644-84b760822a52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a17d9a-fd55-48b4-9293-93c642b09416",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "8ce82f19-58b2-470c-b8cf-0841eab9ad64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "066677f3-24ad-45e0-b857-fe783ffa69bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce82f19-58b2-470c-b8cf-0841eab9ad64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b24e5a78-657a-42f1-9927-b1ba172acb6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce82f19-58b2-470c-b8cf-0841eab9ad64",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "779b6678-950f-4331-a28d-dfcf36bb8543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "9145381a-19ee-4665-9df5-a3415f7475d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "779b6678-950f-4331-a28d-dfcf36bb8543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48b2d4a1-4200-4af0-9d8e-93375ee4a06b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "779b6678-950f-4331-a28d-dfcf36bb8543",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "700c20e1-e342-4be8-8ebf-6e8f6a417785",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "dbcf528e-328d-4689-ab10-dc9d6c5b7892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "700c20e1-e342-4be8-8ebf-6e8f6a417785",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f51ac3-4b2c-4bc2-96c9-466868a25ee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "700c20e1-e342-4be8-8ebf-6e8f6a417785",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "4028fe9d-bf62-4d52-9746-9f95a72a5e83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "7b53e6e5-b719-42b6-b763-9823a3d787b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4028fe9d-bf62-4d52-9746-9f95a72a5e83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "176fa364-6194-4474-8f80-20427ac8b3cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4028fe9d-bf62-4d52-9746-9f95a72a5e83",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "65a42dd1-45a4-416d-b4ce-0fa04058ad18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "739cac0d-711d-42f8-8130-ad5ac8a11c4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65a42dd1-45a4-416d-b4ce-0fa04058ad18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed293360-b9b5-4785-a264-1f599bc6ee2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a42dd1-45a4-416d-b4ce-0fa04058ad18",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "df59df48-1f9c-4cd2-b428-a6a75b61b7f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "000b4558-5b29-48bd-b48a-d739ac4e4199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df59df48-1f9c-4cd2-b428-a6a75b61b7f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e47769a2-2a82-44e7-8522-963e2d752373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df59df48-1f9c-4cd2-b428-a6a75b61b7f1",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "6054a773-85fb-483b-a483-f598a6491e85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "e49e83be-464f-4dc5-8700-3bdfde68b8d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6054a773-85fb-483b-a483-f598a6491e85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6ebd4f5-5a87-4abd-a28c-4d3abe1deb48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6054a773-85fb-483b-a483-f598a6491e85",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        },
        {
            "id": "1e9d5fbf-df03-4b02-82cd-1707a4292c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "compositeImage": {
                "id": "d11fe3e6-4b96-401e-82aa-453ade3ecce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e9d5fbf-df03-4b02-82cd-1707a4292c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aa57566-da3b-418f-9e73-795e75508514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e9d5fbf-df03-4b02-82cd-1707a4292c76",
                    "LayerId": "fc316a49-263e-4b48-9de5-6acf5b8ceeea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 112,
    "layers": [
        {
            "id": "fc316a49-263e-4b48-9de5-6acf5b8ceeea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf94e270-8785-4e89-bddb-54eb467edc73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 45,
    "yorig": 56
}