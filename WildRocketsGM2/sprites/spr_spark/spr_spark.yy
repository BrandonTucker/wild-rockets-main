{
    "id": "98f42764-ac83-40c8-9b52-3d6c8de68105",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53a1f06e-4807-4323-9a02-5ecfc3751ded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f42764-ac83-40c8-9b52-3d6c8de68105",
            "compositeImage": {
                "id": "00cd24da-1f48-44fb-a018-d306e3e7867d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53a1f06e-4807-4323-9a02-5ecfc3751ded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adf3540c-56af-4064-baeb-42e13578d523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53a1f06e-4807-4323-9a02-5ecfc3751ded",
                    "LayerId": "04c277f5-8af5-40e2-b2b4-06c3f4be4f72"
                }
            ]
        },
        {
            "id": "f480de9e-e6d0-47ce-923a-84e89abf4104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f42764-ac83-40c8-9b52-3d6c8de68105",
            "compositeImage": {
                "id": "754e39ad-4a53-4d5f-a89e-e6f683eb6c49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f480de9e-e6d0-47ce-923a-84e89abf4104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b5e2d4-5be5-44fc-88b0-d302b22f65af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f480de9e-e6d0-47ce-923a-84e89abf4104",
                    "LayerId": "04c277f5-8af5-40e2-b2b4-06c3f4be4f72"
                }
            ]
        },
        {
            "id": "cbae0f20-5e2a-4bf5-96d7-9d36acc62644",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f42764-ac83-40c8-9b52-3d6c8de68105",
            "compositeImage": {
                "id": "abec0614-31bb-4bde-9e6e-ce06de40eea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbae0f20-5e2a-4bf5-96d7-9d36acc62644",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "770c0a48-193d-4083-b5f7-40fb263f6a20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbae0f20-5e2a-4bf5-96d7-9d36acc62644",
                    "LayerId": "04c277f5-8af5-40e2-b2b4-06c3f4be4f72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "04c277f5-8af5-40e2-b2b4-06c3f4be4f72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98f42764-ac83-40c8-9b52-3d6c8de68105",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 5
}