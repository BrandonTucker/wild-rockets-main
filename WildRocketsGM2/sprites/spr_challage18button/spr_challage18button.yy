{
    "id": "75729b4d-5fae-45bd-a775-eea8adf050cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challage18button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 1,
    "bbox_right": 99,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3221969a-81c3-4df9-9ee8-9a4a84657d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75729b4d-5fae-45bd-a775-eea8adf050cb",
            "compositeImage": {
                "id": "09b02957-6fa9-46b7-8043-e1424de74b91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3221969a-81c3-4df9-9ee8-9a4a84657d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54de949e-c52d-4658-ae9e-666924cd8c5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3221969a-81c3-4df9-9ee8-9a4a84657d12",
                    "LayerId": "37cecca3-b0e9-4c4b-962a-459ce5e8735c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "37cecca3-b0e9-4c4b-962a-459ce5e8735c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75729b4d-5fae-45bd-a775-eea8adf050cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}