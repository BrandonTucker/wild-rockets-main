{
    "id": "fede7f10-0750-42e8-ac19-f1c3b2cf042c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background13",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 959,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c80252bd-a3b4-49ad-8281-19a9197eaf1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fede7f10-0750-42e8-ac19-f1c3b2cf042c",
            "compositeImage": {
                "id": "e04ef813-892c-481c-a288-eabcaf789164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c80252bd-a3b4-49ad-8281-19a9197eaf1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "982e3193-0633-44d7-a598-55219f65bd5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c80252bd-a3b4-49ad-8281-19a9197eaf1f",
                    "LayerId": "703d8d1a-a904-44e3-ae90-a53773003fdf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "703d8d1a-a904-44e3-ae90-a53773003fdf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fede7f10-0750-42e8-ac19-f1c3b2cf042c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}