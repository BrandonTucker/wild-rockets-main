{
    "id": "2a132351-f771-425b-9ba7-169939928823",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_changepage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 1,
    "bbox_right": 73,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "372cd574-8c90-46f7-b31a-bbc40aab5eb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a132351-f771-425b-9ba7-169939928823",
            "compositeImage": {
                "id": "1f245ce6-f36a-4a4f-9224-9bd769b4df14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "372cd574-8c90-46f7-b31a-bbc40aab5eb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e388924-5f42-44b3-b479-da77e7e2e5e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "372cd574-8c90-46f7-b31a-bbc40aab5eb9",
                    "LayerId": "5520b038-51dc-4030-8df7-cc1bbb892052"
                }
            ]
        },
        {
            "id": "a6385a2f-752b-44ad-9d04-446e6c0c96bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a132351-f771-425b-9ba7-169939928823",
            "compositeImage": {
                "id": "3409e607-a2d6-4054-a309-f496e0d9fde7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6385a2f-752b-44ad-9d04-446e6c0c96bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "045d81e2-f3a6-4ae1-bbf1-c7d4760f36e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6385a2f-752b-44ad-9d04-446e6c0c96bc",
                    "LayerId": "5520b038-51dc-4030-8df7-cc1bbb892052"
                }
            ]
        },
        {
            "id": "0c06f944-0a13-4f8d-92ed-b5ddac741500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a132351-f771-425b-9ba7-169939928823",
            "compositeImage": {
                "id": "8e1796c9-f1af-4b3f-9a8b-ffd584cc2888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c06f944-0a13-4f8d-92ed-b5ddac741500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd05917a-09a0-48ba-830a-5882e36344d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c06f944-0a13-4f8d-92ed-b5ddac741500",
                    "LayerId": "5520b038-51dc-4030-8df7-cc1bbb892052"
                }
            ]
        },
        {
            "id": "a8d37f0c-cf18-45f4-b27a-e8be9c066293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a132351-f771-425b-9ba7-169939928823",
            "compositeImage": {
                "id": "dc174d51-2b53-4a88-b533-2a089456f5a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8d37f0c-cf18-45f4-b27a-e8be9c066293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7419f98d-1bbb-4a31-a7e1-57975d0bca3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8d37f0c-cf18-45f4-b27a-e8be9c066293",
                    "LayerId": "5520b038-51dc-4030-8df7-cc1bbb892052"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "5520b038-51dc-4030-8df7-cc1bbb892052",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a132351-f771-425b-9ba7-169939928823",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 37,
    "yorig": 57
}