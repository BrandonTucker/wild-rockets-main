{
    "id": "23fff036-1f11-4f75-89b2-8e4b130fcd90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange22button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 0,
    "bbox_right": 98,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90edf1c3-f275-423c-a339-15a1a32cdfc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23fff036-1f11-4f75-89b2-8e4b130fcd90",
            "compositeImage": {
                "id": "34570ce6-03bc-458b-b8f5-3a5cc5889765",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90edf1c3-f275-423c-a339-15a1a32cdfc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa5c7064-cb34-4233-8e32-6c194ab5f33f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90edf1c3-f275-423c-a339-15a1a32cdfc9",
                    "LayerId": "87a88191-fbe9-4c6f-871c-0a163db171d5"
                }
            ]
        },
        {
            "id": "0964dea7-0fce-4b79-a100-24be210ecd46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23fff036-1f11-4f75-89b2-8e4b130fcd90",
            "compositeImage": {
                "id": "dca5637e-ab5f-4481-83e3-c3070ac3df56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0964dea7-0fce-4b79-a100-24be210ecd46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6af85e57-01b7-43ee-8e8f-a28919c63f67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0964dea7-0fce-4b79-a100-24be210ecd46",
                    "LayerId": "87a88191-fbe9-4c6f-871c-0a163db171d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "87a88191-fbe9-4c6f-871c-0a163db171d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23fff036-1f11-4f75-89b2-8e4b130fcd90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 48
}