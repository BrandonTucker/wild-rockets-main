{
    "id": "304a6685-ec8d-4bcd-bfa1-5c9a085ec271",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship26",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef8c06ed-817e-43e1-8d39-b83865244b76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "304a6685-ec8d-4bcd-bfa1-5c9a085ec271",
            "compositeImage": {
                "id": "a1bf0491-e937-4e28-8ddf-a733ebc71f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef8c06ed-817e-43e1-8d39-b83865244b76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2734a425-e17a-4d28-9c08-aa2661136545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef8c06ed-817e-43e1-8d39-b83865244b76",
                    "LayerId": "24a4f611-0d92-4088-8022-7420593bbe38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "24a4f611-0d92-4088-8022-7420593bbe38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "304a6685-ec8d-4bcd-bfa1-5c9a085ec271",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 51
}