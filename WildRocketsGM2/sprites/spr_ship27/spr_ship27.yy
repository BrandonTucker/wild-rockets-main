{
    "id": "c7e243d6-5b25-464d-b448-232f7b4e0172",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship27",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 38,
    "bbox_right": 63,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a69561d3-fd3b-4648-8b9e-32e6aa0e0100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7e243d6-5b25-464d-b448-232f7b4e0172",
            "compositeImage": {
                "id": "9d06ba24-7a7c-4bdb-a5c3-644129149023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a69561d3-fd3b-4648-8b9e-32e6aa0e0100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5af442a7-38b2-4eb9-8523-6f20b719e5a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a69561d3-fd3b-4648-8b9e-32e6aa0e0100",
                    "LayerId": "022252eb-4054-4b47-81f0-1d67faa69ef4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "022252eb-4054-4b47-81f0-1d67faa69ef4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7e243d6-5b25-464d-b448-232f7b4e0172",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 101,
    "xorig": 50,
    "yorig": 50
}