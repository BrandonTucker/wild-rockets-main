{
    "id": "79ae4c91-b256-409d-9087-ccc70316eacb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship4boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1c20807-f586-4111-acbe-e2b0a13ca82d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ae4c91-b256-409d-9087-ccc70316eacb",
            "compositeImage": {
                "id": "a3f6309a-2f89-4a51-92bc-08c57f44f93d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1c20807-f586-4111-acbe-e2b0a13ca82d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c699658-8b36-4a5e-a644-7151199a4866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1c20807-f586-4111-acbe-e2b0a13ca82d",
                    "LayerId": "6bdb9be7-6240-4b19-b783-584337096a2e"
                }
            ]
        },
        {
            "id": "5032fee3-9d55-4267-ae9d-610c22a29b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ae4c91-b256-409d-9087-ccc70316eacb",
            "compositeImage": {
                "id": "393733df-1e23-4d80-bba3-dcb51171d296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5032fee3-9d55-4267-ae9d-610c22a29b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9f644b8-b599-4085-8573-e1deaae8aa48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5032fee3-9d55-4267-ae9d-610c22a29b3b",
                    "LayerId": "6bdb9be7-6240-4b19-b783-584337096a2e"
                }
            ]
        },
        {
            "id": "59b7705a-4b44-4819-afa6-77ff18ed150b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ae4c91-b256-409d-9087-ccc70316eacb",
            "compositeImage": {
                "id": "505b7521-e2e7-4df9-9fec-7fdec086df9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59b7705a-4b44-4819-afa6-77ff18ed150b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "398c3446-e3eb-41e0-aa67-7fef04410a1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59b7705a-4b44-4819-afa6-77ff18ed150b",
                    "LayerId": "6bdb9be7-6240-4b19-b783-584337096a2e"
                }
            ]
        },
        {
            "id": "da87ac51-c0a3-4caf-adb6-0831f2ee7e74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ae4c91-b256-409d-9087-ccc70316eacb",
            "compositeImage": {
                "id": "6438860a-6196-47f2-b9f1-529a17f38bfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da87ac51-c0a3-4caf-adb6-0831f2ee7e74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0311b1af-a855-478c-a69d-94a17d38cd59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da87ac51-c0a3-4caf-adb6-0831f2ee7e74",
                    "LayerId": "6bdb9be7-6240-4b19-b783-584337096a2e"
                }
            ]
        },
        {
            "id": "46f2f1b0-7653-4a46-952f-371751118bf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ae4c91-b256-409d-9087-ccc70316eacb",
            "compositeImage": {
                "id": "8cecf548-ab85-4bd5-8e60-67778b98bc84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46f2f1b0-7653-4a46-952f-371751118bf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aa5c87b-05ce-432e-83ef-8309511db5dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46f2f1b0-7653-4a46-952f-371751118bf6",
                    "LayerId": "6bdb9be7-6240-4b19-b783-584337096a2e"
                }
            ]
        },
        {
            "id": "b9554d67-c62e-47cd-816d-eabde0a48fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ae4c91-b256-409d-9087-ccc70316eacb",
            "compositeImage": {
                "id": "0d0f1b13-a735-4071-a6cc-cb4617fbf3c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9554d67-c62e-47cd-816d-eabde0a48fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40b0f32-6946-4aa3-b475-ac55b7cacc3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9554d67-c62e-47cd-816d-eabde0a48fc9",
                    "LayerId": "6bdb9be7-6240-4b19-b783-584337096a2e"
                }
            ]
        },
        {
            "id": "03edc90b-a058-491c-b649-4879056175bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79ae4c91-b256-409d-9087-ccc70316eacb",
            "compositeImage": {
                "id": "36744917-5266-42e8-9938-c97ab589f177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03edc90b-a058-491c-b649-4879056175bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82157d5f-72c4-4ebd-9a1c-fb4dd9ebec12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03edc90b-a058-491c-b649-4879056175bf",
                    "LayerId": "6bdb9be7-6240-4b19-b783-584337096a2e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 169,
    "layers": [
        {
            "id": "6bdb9be7-6240-4b19-b783-584337096a2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79ae4c91-b256-409d-9087-ccc70316eacb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 42
}