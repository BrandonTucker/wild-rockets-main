{
    "id": "3189fa00-a786-43d8-9939-beede88241a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship13boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 30,
    "bbox_right": 55,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d34fd1c5-fcae-4eba-a7eb-83ec3c004bf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3189fa00-a786-43d8-9939-beede88241a0",
            "compositeImage": {
                "id": "d97921bf-bbf3-4c0e-bacd-06133c58d833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d34fd1c5-fcae-4eba-a7eb-83ec3c004bf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7b4a3f0-c770-44c7-b35e-0a372c690b3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d34fd1c5-fcae-4eba-a7eb-83ec3c004bf5",
                    "LayerId": "f372ecaa-c39a-4a53-927c-5e7f7653991c"
                }
            ]
        },
        {
            "id": "f0b16ded-2e78-48d9-ad17-073e7da37fa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3189fa00-a786-43d8-9939-beede88241a0",
            "compositeImage": {
                "id": "189c1663-9622-46c5-93d6-6b9a90e2cce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0b16ded-2e78-48d9-ad17-073e7da37fa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "359e074f-fec7-45b7-afd4-2a94b555fc60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0b16ded-2e78-48d9-ad17-073e7da37fa3",
                    "LayerId": "f372ecaa-c39a-4a53-927c-5e7f7653991c"
                }
            ]
        },
        {
            "id": "2ca52681-c44c-4778-aab6-ba95282fda3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3189fa00-a786-43d8-9939-beede88241a0",
            "compositeImage": {
                "id": "a7b23a40-c21f-483c-9ded-6092a08a25ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ca52681-c44c-4778-aab6-ba95282fda3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfa57654-b0dc-4ff8-9277-641244d42fb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ca52681-c44c-4778-aab6-ba95282fda3d",
                    "LayerId": "f372ecaa-c39a-4a53-927c-5e7f7653991c"
                }
            ]
        },
        {
            "id": "c448c735-6c3e-4141-ab80-d922cb43df43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3189fa00-a786-43d8-9939-beede88241a0",
            "compositeImage": {
                "id": "09ccd57f-2cf4-4667-861a-9320d666dd67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c448c735-6c3e-4141-ab80-d922cb43df43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65ddb3d0-c697-4c25-8577-74c8e99b9735",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c448c735-6c3e-4141-ab80-d922cb43df43",
                    "LayerId": "f372ecaa-c39a-4a53-927c-5e7f7653991c"
                }
            ]
        },
        {
            "id": "c4a8ef18-3d74-4271-b208-2438bc637406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3189fa00-a786-43d8-9939-beede88241a0",
            "compositeImage": {
                "id": "eb9ab8e4-f560-4616-a559-e2f6d7e9592e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4a8ef18-3d74-4271-b208-2438bc637406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "677046b3-0539-45f0-8760-a101db26ca46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4a8ef18-3d74-4271-b208-2438bc637406",
                    "LayerId": "f372ecaa-c39a-4a53-927c-5e7f7653991c"
                }
            ]
        },
        {
            "id": "b5e83e23-9a4e-44e6-a5eb-f384082b6f30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3189fa00-a786-43d8-9939-beede88241a0",
            "compositeImage": {
                "id": "83471e3b-a3a1-4e0d-8cc8-77d6d652fa29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e83e23-9a4e-44e6-a5eb-f384082b6f30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06438831-a2e7-4336-b2a3-ecac792a68da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e83e23-9a4e-44e6-a5eb-f384082b6f30",
                    "LayerId": "f372ecaa-c39a-4a53-927c-5e7f7653991c"
                }
            ]
        },
        {
            "id": "34439dc7-931e-41d1-9d68-7f8d382be3ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3189fa00-a786-43d8-9939-beede88241a0",
            "compositeImage": {
                "id": "755806a4-70c4-4dee-8d0e-3f75b8423c5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34439dc7-931e-41d1-9d68-7f8d382be3ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54d09b3b-6d4f-4f58-ac04-2f0830bdacb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34439dc7-931e-41d1-9d68-7f8d382be3ff",
                    "LayerId": "f372ecaa-c39a-4a53-927c-5e7f7653991c"
                }
            ]
        },
        {
            "id": "255d3b4e-5e5e-42d2-926e-96d59e2c4a9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3189fa00-a786-43d8-9939-beede88241a0",
            "compositeImage": {
                "id": "5c1d1b18-1ab5-4ce2-83ca-368ccf3952e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "255d3b4e-5e5e-42d2-926e-96d59e2c4a9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b203fb7-7100-4999-81c8-198399fb89fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "255d3b4e-5e5e-42d2-926e-96d59e2c4a9e",
                    "LayerId": "f372ecaa-c39a-4a53-927c-5e7f7653991c"
                }
            ]
        },
        {
            "id": "b4c965ee-4a66-4ed0-83d3-fea1358f368d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3189fa00-a786-43d8-9939-beede88241a0",
            "compositeImage": {
                "id": "018fcb66-24bb-4c12-a436-839c9d9cf600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c965ee-4a66-4ed0-83d3-fea1358f368d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17aa2ecb-9798-450f-82e4-9cd6231d4e10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c965ee-4a66-4ed0-83d3-fea1358f368d",
                    "LayerId": "f372ecaa-c39a-4a53-927c-5e7f7653991c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "f372ecaa-c39a-4a53-927c-5e7f7653991c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3189fa00-a786-43d8-9939-beede88241a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 87,
    "xorig": 43,
    "yorig": 42
}