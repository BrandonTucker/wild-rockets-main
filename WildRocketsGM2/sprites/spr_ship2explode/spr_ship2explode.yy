{
    "id": "11a90a15-207d-4c57-87c2-16f813ff5689",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship2explode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 16,
    "bbox_right": 61,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02ea6eda-2f23-448d-ad91-f6ffec60fd6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a90a15-207d-4c57-87c2-16f813ff5689",
            "compositeImage": {
                "id": "9c6df2cf-daef-43d2-883e-01b8452109ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ea6eda-2f23-448d-ad91-f6ffec60fd6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44851268-61d6-4e64-966b-d6572e80335b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ea6eda-2f23-448d-ad91-f6ffec60fd6b",
                    "LayerId": "98bdf6cf-081e-4acf-a3a6-751d224898f2"
                }
            ]
        },
        {
            "id": "0a8b45e2-0e1c-491f-8af6-10efea84440f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a90a15-207d-4c57-87c2-16f813ff5689",
            "compositeImage": {
                "id": "4569aa2a-7844-43b2-a049-139adc758b2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a8b45e2-0e1c-491f-8af6-10efea84440f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5f74ad7-bae0-4eb8-9f1e-9362ab25519a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a8b45e2-0e1c-491f-8af6-10efea84440f",
                    "LayerId": "98bdf6cf-081e-4acf-a3a6-751d224898f2"
                }
            ]
        },
        {
            "id": "6aaab3f5-ff40-4a16-a0a6-8610a8d527da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a90a15-207d-4c57-87c2-16f813ff5689",
            "compositeImage": {
                "id": "3406f62b-bc0a-4a6f-923b-6e7e4490ac3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aaab3f5-ff40-4a16-a0a6-8610a8d527da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c357252-9b6f-4895-b09b-14014d4ab490",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aaab3f5-ff40-4a16-a0a6-8610a8d527da",
                    "LayerId": "98bdf6cf-081e-4acf-a3a6-751d224898f2"
                }
            ]
        },
        {
            "id": "28816d17-c7f6-4b27-ab70-6498119a696c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a90a15-207d-4c57-87c2-16f813ff5689",
            "compositeImage": {
                "id": "bc4a0583-dcaa-4fdd-ace0-49ce87df9fe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28816d17-c7f6-4b27-ab70-6498119a696c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8478ff0-12a7-4cab-afda-bd9d2fad4198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28816d17-c7f6-4b27-ab70-6498119a696c",
                    "LayerId": "98bdf6cf-081e-4acf-a3a6-751d224898f2"
                }
            ]
        },
        {
            "id": "79835bb1-8a24-441c-93ce-68cae5a30ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a90a15-207d-4c57-87c2-16f813ff5689",
            "compositeImage": {
                "id": "a9820df7-0e33-4037-b763-6fa8a428f6a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79835bb1-8a24-441c-93ce-68cae5a30ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "236aaa4a-06fc-416c-83d2-d90f6a6837e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79835bb1-8a24-441c-93ce-68cae5a30ff1",
                    "LayerId": "98bdf6cf-081e-4acf-a3a6-751d224898f2"
                }
            ]
        },
        {
            "id": "81fb0248-ae2a-4cc6-8099-44e4e8b32ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a90a15-207d-4c57-87c2-16f813ff5689",
            "compositeImage": {
                "id": "52a285b9-f46e-467e-bd04-e0a5123f780e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81fb0248-ae2a-4cc6-8099-44e4e8b32ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eca13e04-aa27-4c87-83b5-4cd63fa70b7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81fb0248-ae2a-4cc6-8099-44e4e8b32ad9",
                    "LayerId": "98bdf6cf-081e-4acf-a3a6-751d224898f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "98bdf6cf-081e-4acf-a3a6-751d224898f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11a90a15-207d-4c57-87c2-16f813ff5689",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 39,
    "yorig": 41
}