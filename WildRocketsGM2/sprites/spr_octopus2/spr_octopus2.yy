{
    "id": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_octopus2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f84cc680-db1b-4df0-937b-98c2485f085a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "9d0317ff-55d5-41fb-8a01-90f31803ee9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f84cc680-db1b-4df0-937b-98c2485f085a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a572feba-d3cd-4293-8ea1-06c3839e9f5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f84cc680-db1b-4df0-937b-98c2485f085a",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "918ec942-faf6-4165-b6f1-5ae358b1751e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "c9424590-0585-4ba7-b1bf-a9b033b03b4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "918ec942-faf6-4165-b6f1-5ae358b1751e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "904e143f-47ab-40d6-8878-58bc65941e2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "918ec942-faf6-4165-b6f1-5ae358b1751e",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "e7c0aa22-67ca-4267-b9b1-f796c59088fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "39392ffb-9866-46d8-a891-065b9256c8c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c0aa22-67ca-4267-b9b1-f796c59088fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa11f5b4-6791-4b17-9702-f61fbbfcf0a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c0aa22-67ca-4267-b9b1-f796c59088fd",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "dc5e1240-e1bb-4b3e-bc51-91e520537756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "4d92ccc4-9fe8-4047-91a0-02fb18b7dbe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc5e1240-e1bb-4b3e-bc51-91e520537756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2afc0bee-f961-4b89-87f0-0392f12d925c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc5e1240-e1bb-4b3e-bc51-91e520537756",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "9727bbf2-00d5-456d-b11c-6b5e3a57083b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "cf1ca68e-5a74-4c36-a1a2-60ac6c742698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9727bbf2-00d5-456d-b11c-6b5e3a57083b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8261134-f047-4921-a18c-fccd279bde2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9727bbf2-00d5-456d-b11c-6b5e3a57083b",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "56cfeb49-8922-4c95-973a-f3d76a992e5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "8eb5d06d-54ed-4bfb-9e96-463cf1ebd7ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56cfeb49-8922-4c95-973a-f3d76a992e5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1aba632-c672-4335-a744-5fa37f8c655f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56cfeb49-8922-4c95-973a-f3d76a992e5e",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "00344253-f279-4dc4-bc00-e08d52703ff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "df0905a3-4082-4a4b-8bb2-03ae7669313e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00344253-f279-4dc4-bc00-e08d52703ff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d3dbed-0372-463c-b091-3127dff1b5f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00344253-f279-4dc4-bc00-e08d52703ff4",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "b07bdfd1-4147-4118-97ec-2d476b2fc55f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "9bb8adc3-bdfe-40d9-9926-9aa9a56cb68d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b07bdfd1-4147-4118-97ec-2d476b2fc55f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ce2a016-9b37-4fb7-9cba-1e4ea49aebc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b07bdfd1-4147-4118-97ec-2d476b2fc55f",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "e840ba22-6467-4a4c-88f5-27f1dc2f9cd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "78e44756-eded-43a0-9380-88a967ffa144",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e840ba22-6467-4a4c-88f5-27f1dc2f9cd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db2b45ee-d316-4475-a97f-4f81a05d1b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e840ba22-6467-4a4c-88f5-27f1dc2f9cd4",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "917f2be9-3741-4942-9893-567ce0093c99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "84261197-872a-4a60-a767-e2984337a01e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "917f2be9-3741-4942-9893-567ce0093c99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5265404-9005-43e5-9180-442866df1565",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "917f2be9-3741-4942-9893-567ce0093c99",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "79fe00c0-62ba-4397-8730-8cccab106df7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "d444f82d-11fa-450a-9f30-46fe4a746d1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79fe00c0-62ba-4397-8730-8cccab106df7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e44af99-7a90-4876-8e12-e2972b6fed81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79fe00c0-62ba-4397-8730-8cccab106df7",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "5cb0eec8-85ab-45b6-a4e2-b6954b86a1d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "b9b8e9e2-a78b-446e-8ced-df57e864823e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cb0eec8-85ab-45b6-a4e2-b6954b86a1d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5183ca6-bff1-4715-9bf4-2414d3978410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cb0eec8-85ab-45b6-a4e2-b6954b86a1d9",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "45ed883d-4c50-4053-9d65-d6e2618b549c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "1b393b3b-81db-4f6d-b03a-ac074660c00a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45ed883d-4c50-4053-9d65-d6e2618b549c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22a7c39c-8a0e-4617-b314-d980ec0e3c75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45ed883d-4c50-4053-9d65-d6e2618b549c",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "b98f08fa-2dd5-4b26-9b87-9f4c6a3cef5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "435b2100-d6d5-4182-adf0-cc4022ab1d76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b98f08fa-2dd5-4b26-9b87-9f4c6a3cef5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45637e85-9590-43e5-b6b1-6cdb2d2d5de3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b98f08fa-2dd5-4b26-9b87-9f4c6a3cef5b",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        },
        {
            "id": "9482c9fc-a102-4482-91a7-2f4885587a0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "compositeImage": {
                "id": "b0e38a27-e705-470e-a133-bf2f152109cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9482c9fc-a102-4482-91a7-2f4885587a0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6c65366-b008-4158-8459-6ba54b2a79e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9482c9fc-a102-4482-91a7-2f4885587a0a",
                    "LayerId": "a2275b79-babb-4356-9c2e-72cc6acc1efa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "a2275b79-babb-4356-9c2e-72cc6acc1efa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "508dc0ae-44c2-40ac-9ffa-b74534a35aba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}