{
    "id": "e0989f38-e81c-4b6a-9efc-7ff51fabeb8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yellowfireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37bec9b5-50d7-4c96-ba70-1c3ef2a709c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0989f38-e81c-4b6a-9efc-7ff51fabeb8c",
            "compositeImage": {
                "id": "7ddd7682-fd52-4f84-9fcb-de7e002fbab1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37bec9b5-50d7-4c96-ba70-1c3ef2a709c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fde66eb3-2dc4-439d-a1d6-d6c36af08c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37bec9b5-50d7-4c96-ba70-1c3ef2a709c9",
                    "LayerId": "26a27db5-de41-4d2f-a045-bbbbb539cca4"
                }
            ]
        },
        {
            "id": "b9b684e4-405d-41f5-abe2-47e1a9afea9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0989f38-e81c-4b6a-9efc-7ff51fabeb8c",
            "compositeImage": {
                "id": "30c5fc48-970d-49d9-87d3-da47c0cdd86c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9b684e4-405d-41f5-abe2-47e1a9afea9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d1f50df-f0e5-4fce-af93-9770cbb808f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b684e4-405d-41f5-abe2-47e1a9afea9f",
                    "LayerId": "26a27db5-de41-4d2f-a045-bbbbb539cca4"
                }
            ]
        },
        {
            "id": "029a0e17-c7e2-4086-95d4-48911e726db7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0989f38-e81c-4b6a-9efc-7ff51fabeb8c",
            "compositeImage": {
                "id": "a6e32094-0c3e-47e8-a56c-6c18af08fb9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "029a0e17-c7e2-4086-95d4-48911e726db7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41a1f6bb-6ad4-4c5e-9c60-e42d52ef95c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "029a0e17-c7e2-4086-95d4-48911e726db7",
                    "LayerId": "26a27db5-de41-4d2f-a045-bbbbb539cca4"
                }
            ]
        },
        {
            "id": "f18a68ea-9fa8-4170-a4e6-b49b68d4c959",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0989f38-e81c-4b6a-9efc-7ff51fabeb8c",
            "compositeImage": {
                "id": "db9b3cf9-b1f6-413d-823e-e49b29e795d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f18a68ea-9fa8-4170-a4e6-b49b68d4c959",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8dcc147-fe2d-4a55-b28c-6f44ecb46028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f18a68ea-9fa8-4170-a4e6-b49b68d4c959",
                    "LayerId": "26a27db5-de41-4d2f-a045-bbbbb539cca4"
                }
            ]
        },
        {
            "id": "af577b5f-da43-43e1-aa81-9c59694dd8de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0989f38-e81c-4b6a-9efc-7ff51fabeb8c",
            "compositeImage": {
                "id": "a21d0293-d051-4340-b4ad-02346d563d4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af577b5f-da43-43e1-aa81-9c59694dd8de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03499451-6249-4820-94f4-c3266f4b02ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af577b5f-da43-43e1-aa81-9c59694dd8de",
                    "LayerId": "26a27db5-de41-4d2f-a045-bbbbb539cca4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "26a27db5-de41-4d2f-a045-bbbbb539cca4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0989f38-e81c-4b6a-9efc-7ff51fabeb8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 16
}