{
    "id": "dbcf849b-b367-44c9-843f-d441a6c25007",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_purplealien",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00bfbcf9-9355-46b8-9396-9492d2c5f614",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "07d5221f-a55b-4ab3-a9c7-20a92ebe4147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00bfbcf9-9355-46b8-9396-9492d2c5f614",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6937cf35-aea1-4888-9071-7aba865ac016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00bfbcf9-9355-46b8-9396-9492d2c5f614",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "9e617766-36c5-49f9-9f2f-0f8f6aa6b2c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "6e336ac5-32e2-4eb7-a946-85a9ccaf84e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e617766-36c5-49f9-9f2f-0f8f6aa6b2c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7ddc150-f6ee-4d22-b0d5-609de9912b4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e617766-36c5-49f9-9f2f-0f8f6aa6b2c7",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "ea5b8556-f179-4627-9731-590d9b8dd4e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "703191ce-65f9-43e5-a98c-62e9acd3110a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea5b8556-f179-4627-9731-590d9b8dd4e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf2df988-5fd8-40ef-84c2-af6131a80e02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea5b8556-f179-4627-9731-590d9b8dd4e1",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "7d0c697f-adf6-46bc-a41c-4ef8fbdc06ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "024c233a-b985-4615-8a17-214ac96e14fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d0c697f-adf6-46bc-a41c-4ef8fbdc06ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d9236b-6c31-4f96-881e-ccd4130f6384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0c697f-adf6-46bc-a41c-4ef8fbdc06ff",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "5ae40585-e6da-4f90-96da-bd81ce3a34c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "0bc9ae68-149d-42fb-9ef6-d79b75c5d030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ae40585-e6da-4f90-96da-bd81ce3a34c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee0b1a8d-77a3-4524-9834-2275da209a8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ae40585-e6da-4f90-96da-bd81ce3a34c9",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "c1980980-87fe-4d92-a2bc-309d204123fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "1dda5e48-8ccc-4291-a2b9-4bdee6228dea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1980980-87fe-4d92-a2bc-309d204123fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1e07448-0b9c-4491-801e-6ade1c39be05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1980980-87fe-4d92-a2bc-309d204123fd",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "4e6f8251-fa31-47b4-a3f6-e24a2746ade0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "baa83679-6327-4a18-b657-effc524c2c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e6f8251-fa31-47b4-a3f6-e24a2746ade0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fed12d63-4974-4255-a041-40a1dc4f6a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e6f8251-fa31-47b4-a3f6-e24a2746ade0",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "7a70d3df-0b31-42dc-93bb-5a94b6a70dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "615243d3-3ffb-4391-a440-b1a010f26e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a70d3df-0b31-42dc-93bb-5a94b6a70dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788644d5-4937-47e8-8f0b-b17bccf6936b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a70d3df-0b31-42dc-93bb-5a94b6a70dee",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "14edcdd8-fc2d-4e60-8b67-ccc290ccb125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "ac59b617-1722-4904-8eae-dc4383a7a152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14edcdd8-fc2d-4e60-8b67-ccc290ccb125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3776585b-fa2a-4011-af55-ecaf539d4d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14edcdd8-fc2d-4e60-8b67-ccc290ccb125",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "e97347e2-12f5-4ab6-801c-ee35a446c904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "f30013c5-a517-49a8-8655-d7d24b1f26d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97347e2-12f5-4ab6-801c-ee35a446c904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37c68afd-86d9-4a62-8238-a980f650d942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97347e2-12f5-4ab6-801c-ee35a446c904",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "5529001c-da17-4487-b541-8d29d2f571a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "bfee2ebf-5443-414f-945b-efbc115c7fdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5529001c-da17-4487-b541-8d29d2f571a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0af3714-2aff-4a1c-bbef-6bd17175c680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5529001c-da17-4487-b541-8d29d2f571a2",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "bf13debf-e35d-48e3-b183-9dc35f7a5844",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "f315dc5e-ff9f-4d87-b489-90e41cc0e17b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf13debf-e35d-48e3-b183-9dc35f7a5844",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcc91a1b-8ce3-45bd-9bac-bcfc9e6ee310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf13debf-e35d-48e3-b183-9dc35f7a5844",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "cc5854e1-97e2-440d-a045-32ba7f84e00b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "269105e8-8572-45f9-832f-e14194320976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc5854e1-97e2-440d-a045-32ba7f84e00b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8d2f016-6ddd-4aa6-b3c4-7530f19b4dae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc5854e1-97e2-440d-a045-32ba7f84e00b",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "3e1d8681-bee5-4a67-9c85-c7745cef4191",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "a02d7fba-96e6-4004-96c6-6c9c83b18103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e1d8681-bee5-4a67-9c85-c7745cef4191",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d95a01cd-a68b-4206-9baa-73480e88c9ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e1d8681-bee5-4a67-9c85-c7745cef4191",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        },
        {
            "id": "f38a75db-e856-41db-8bda-37dd613d5642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "compositeImage": {
                "id": "866d3ce3-c57c-4df5-a199-f49344f42e34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f38a75db-e856-41db-8bda-37dd613d5642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6001b8a1-18f6-4ef9-b6b1-2eac820b24de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f38a75db-e856-41db-8bda-37dd613d5642",
                    "LayerId": "b7821a00-52a0-4d87-a14b-f2091da5e4c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "b7821a00-52a0-4d87-a14b-f2091da5e4c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbcf849b-b367-44c9-843f-d441a6c25007",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 23
}