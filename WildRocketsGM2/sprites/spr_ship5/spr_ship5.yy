{
    "id": "b0f328af-9b61-4b11-97eb-4f2df57d72ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 28,
    "bbox_right": 53,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61486b00-8865-47f0-a85e-5411e46a6f53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0f328af-9b61-4b11-97eb-4f2df57d72ef",
            "compositeImage": {
                "id": "7bd79661-ef32-485e-870c-d6a9ccd9e2e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61486b00-8865-47f0-a85e-5411e46a6f53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6084733-992b-45ee-bbf9-bbff433fb990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61486b00-8865-47f0-a85e-5411e46a6f53",
                    "LayerId": "0cec819c-3805-4035-8305-f7c1ee1af40a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "0cec819c-3805-4035-8305-f7c1ee1af40a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0f328af-9b61-4b11-97eb-4f2df57d72ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 40,
    "yorig": 46
}