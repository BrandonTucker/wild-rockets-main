{
    "id": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fuel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 0,
    "bbox_right": 77,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d484ffa-aa14-45d2-ad3b-34c0820de947",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "4bb147ae-48b6-49b1-8f1b-629066f5dd70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d484ffa-aa14-45d2-ad3b-34c0820de947",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3108b768-184c-4bc2-bc75-28045b4560d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d484ffa-aa14-45d2-ad3b-34c0820de947",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "9dc74cd9-251b-44bd-af47-b3553d1e6623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "0610a789-3719-4cfa-88d5-593f0a5a795d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dc74cd9-251b-44bd-af47-b3553d1e6623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3121ec4d-85e6-4504-bcb3-e499bc4e402f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dc74cd9-251b-44bd-af47-b3553d1e6623",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "4aec56f7-daee-47ec-b1d8-4c9ff5448f0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "d9b2a766-75f4-4d77-909c-3b07ee79d572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aec56f7-daee-47ec-b1d8-4c9ff5448f0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52732d4e-ae7a-452e-8f92-83f56db70355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aec56f7-daee-47ec-b1d8-4c9ff5448f0e",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "c04a78d1-0ac4-408e-b310-346825bc1a6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "6c6dacba-b163-4fe7-98c9-9df5a020df7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c04a78d1-0ac4-408e-b310-346825bc1a6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d0361e-a5af-4993-ac88-9ae996974ede",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c04a78d1-0ac4-408e-b310-346825bc1a6c",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "ab96a423-5760-4f9a-afc4-75a1c9c7abd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "840f1d09-f5ca-49bc-8504-5b975ede031c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab96a423-5760-4f9a-afc4-75a1c9c7abd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71d97df6-1f8e-4e27-94e5-4d27bc6f436e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab96a423-5760-4f9a-afc4-75a1c9c7abd0",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "e7c69757-7adc-47ba-b1ec-dd57e3125b94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "0026f9a1-4cb6-43d8-820c-bf81a486eaee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c69757-7adc-47ba-b1ec-dd57e3125b94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb7ac040-5866-4edd-bfb5-479a61d3cc14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c69757-7adc-47ba-b1ec-dd57e3125b94",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "55acc09f-42ab-46b1-a250-357a68ad316e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "ca0c6a54-ec29-45c9-be64-703b013019b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55acc09f-42ab-46b1-a250-357a68ad316e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad9c770-8e97-4818-8114-d352decb10a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55acc09f-42ab-46b1-a250-357a68ad316e",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "c3f397b0-579c-4643-90c5-768ad9298a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "5971cf61-d3a8-4a0b-9e98-3c41921d844d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f397b0-579c-4643-90c5-768ad9298a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998087df-15a2-4520-8ff5-b6a68c26f93c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f397b0-579c-4643-90c5-768ad9298a80",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "e63432af-d5f3-477c-a15b-92220040d7f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "1b8dc040-6752-4981-908a-9babcb0d4394",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e63432af-d5f3-477c-a15b-92220040d7f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81b1dbc2-5f27-412b-add0-eb4133dc4325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e63432af-d5f3-477c-a15b-92220040d7f5",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "5933e39a-fd9a-4562-b9ad-d0d93e0748d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "0c568806-1428-4e8b-bf2a-89d8ed88cea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5933e39a-fd9a-4562-b9ad-d0d93e0748d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d887bf7-0df1-4436-9f10-9fa10c1c9ac0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5933e39a-fd9a-4562-b9ad-d0d93e0748d1",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "46ce2349-a104-4137-8b3e-363e460ddad3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "bf7dedfa-856a-4f9d-b141-df8f0f389913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46ce2349-a104-4137-8b3e-363e460ddad3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5e4b674-5e3b-44b8-8b6b-912b3146ab6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46ce2349-a104-4137-8b3e-363e460ddad3",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "f0037fb1-64d6-4389-9abd-ee0cd105f8b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "1c433e4d-db5a-4b28-9031-0999df5f7398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0037fb1-64d6-4389-9abd-ee0cd105f8b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d3062c1-6a59-4308-b2c6-08f1937d1e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0037fb1-64d6-4389-9abd-ee0cd105f8b6",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "067b8432-ebff-4024-a306-d785c3155837",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "5fb355c5-a25b-49f5-bb65-d5b7f967ed23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "067b8432-ebff-4024-a306-d785c3155837",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "789f84af-ed95-4ccf-9bf7-4a017c4f944c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "067b8432-ebff-4024-a306-d785c3155837",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "cd094b52-6a3d-43c9-963a-c216076befa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "f47ba6e0-97c6-422d-a854-39ba85c689df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd094b52-6a3d-43c9-963a-c216076befa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "203648e0-ef82-40fc-9ae5-6941d7d26456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd094b52-6a3d-43c9-963a-c216076befa1",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "2f0e0020-bc41-42f8-b536-97a8712600ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "66dc6f9c-e7e2-4175-a7a6-4ab48a417400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f0e0020-bc41-42f8-b536-97a8712600ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8477aab5-1f75-4ea7-bccb-5952ad2ff68e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f0e0020-bc41-42f8-b536-97a8712600ba",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "2500735a-2ef2-4aa7-8956-803cdde8fee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "c37c7d4a-4608-451d-9db4-0ef76b2e7d61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2500735a-2ef2-4aa7-8956-803cdde8fee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b3c7d2-f444-4e1a-8ba0-0c7ae50c13dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2500735a-2ef2-4aa7-8956-803cdde8fee3",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "edbf28c8-84c1-424b-accd-35538ef4c5bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "fd503791-bfa7-4619-9cba-58f8702905b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edbf28c8-84c1-424b-accd-35538ef4c5bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda3b793-c471-4da9-80c7-bee01c9a4518",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edbf28c8-84c1-424b-accd-35538ef4c5bb",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "1be7d8d4-9def-4223-8eee-34255ba8e52b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "266d23f3-02f3-49c6-9007-29ff760e9357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1be7d8d4-9def-4223-8eee-34255ba8e52b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45e6ed72-9f74-479f-b76f-faf3b9616e0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1be7d8d4-9def-4223-8eee-34255ba8e52b",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "afb2a8dc-7a90-4ea2-8d99-8fd0fbb0daa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "fa870629-7241-4617-b838-c0662b04037b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb2a8dc-7a90-4ea2-8d99-8fd0fbb0daa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08685a3c-95b7-4a85-8a32-3895363ccfb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb2a8dc-7a90-4ea2-8d99-8fd0fbb0daa2",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "c2caa415-5abf-4236-8923-2ecc5746fd15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "68350d80-e49a-4586-b0f7-193477981e48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2caa415-5abf-4236-8923-2ecc5746fd15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eddf3ca-fd7d-46fd-9cee-df68ec2b6f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2caa415-5abf-4236-8923-2ecc5746fd15",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "67abbb76-8f8a-474d-ae71-f5686eefc034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "262869c8-8079-4546-9afc-fc6a3e9845bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67abbb76-8f8a-474d-ae71-f5686eefc034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e167c93f-3702-4841-b8ac-a0ee9d1ccffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67abbb76-8f8a-474d-ae71-f5686eefc034",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "1256b015-4b7e-4c6f-a94d-f2c39bac548c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "a3566b31-9d63-4029-894d-5ef6dbf88bf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1256b015-4b7e-4c6f-a94d-f2c39bac548c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e9b655-bfd9-4fbb-a4f6-8448a3fdf33d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1256b015-4b7e-4c6f-a94d-f2c39bac548c",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "a7e326bf-c98d-4a81-8a1b-ef03587afd32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "1857879b-2e54-4604-a40f-d347e1cbae86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e326bf-c98d-4a81-8a1b-ef03587afd32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d54aa258-9202-47f1-9360-9a1019701bc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e326bf-c98d-4a81-8a1b-ef03587afd32",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "5c0e20b8-81a7-4e37-8423-1d7cff44c609",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "4ea6ffdf-a824-4fe8-82be-66a7059fa6e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c0e20b8-81a7-4e37-8423-1d7cff44c609",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f1361b-c4d1-4bb1-85e5-2acc46bc91fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c0e20b8-81a7-4e37-8423-1d7cff44c609",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "05b478bc-0ff0-4043-a8f0-46b9ed236de1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "76f89172-a5f8-488a-8797-ac3badf6a73f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05b478bc-0ff0-4043-a8f0-46b9ed236de1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "163422b3-fd92-4470-bf44-7e75766f050b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05b478bc-0ff0-4043-a8f0-46b9ed236de1",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "2fce4160-b539-4e52-94a0-a64e8f740a34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "dcae61c8-77ca-4c96-807e-d95dd0234437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fce4160-b539-4e52-94a0-a64e8f740a34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22a05ade-a44c-4bb7-adf6-8ec8da74be48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fce4160-b539-4e52-94a0-a64e8f740a34",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "df463bc0-c3bb-4ff7-97cc-35a27b627b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "8cf0dd74-9965-49e1-a00c-249c212d160f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df463bc0-c3bb-4ff7-97cc-35a27b627b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e703ec05-1532-4d3f-9672-92d3fd40c165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df463bc0-c3bb-4ff7-97cc-35a27b627b58",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "d262e88c-0d16-44f7-ba3d-b0da28c1dec0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "47508b9b-5ff3-4143-a5a6-09d53b43ce7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d262e88c-0d16-44f7-ba3d-b0da28c1dec0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7bb1284-e21a-4595-add2-6edcf683c2db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d262e88c-0d16-44f7-ba3d-b0da28c1dec0",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "5556526e-14f4-42c2-b26b-73721b87e57b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "2acc2008-795b-4d09-b80f-5c0c52b772be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5556526e-14f4-42c2-b26b-73721b87e57b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "520c5aff-c60c-4437-a58f-865f1f934b0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5556526e-14f4-42c2-b26b-73721b87e57b",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "e65296f2-7aa7-4080-820e-98aef05d9de3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "ffad7c12-6532-43a0-9c3c-8d628da54cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e65296f2-7aa7-4080-820e-98aef05d9de3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cde4fa9-2b68-4244-a2c6-079703ea9154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e65296f2-7aa7-4080-820e-98aef05d9de3",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        },
        {
            "id": "dc9ecdd7-2a7d-403c-ac11-367d6351aa4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "compositeImage": {
                "id": "1a80b619-9926-41d5-8868-268b43748abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc9ecdd7-2a7d-403c-ac11-367d6351aa4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45200a28-44fa-420e-8344-c3f91bd5783a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc9ecdd7-2a7d-403c-ac11-367d6351aa4e",
                    "LayerId": "dd843e39-0918-451f-9f49-d650a95dfca6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "dd843e39-0918-451f-9f49-d650a95dfca6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6342eb2-1043-47b7-986b-e2ac20e7e66f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 38,
    "yorig": 66
}