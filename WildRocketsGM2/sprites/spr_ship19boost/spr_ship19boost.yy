{
    "id": "7f74c8ca-93a4-41bb-835e-a0e69bf7b5a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship19boost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 31,
    "bbox_right": 56,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cd296bc-d473-4273-82a7-f4815ba9e474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f74c8ca-93a4-41bb-835e-a0e69bf7b5a9",
            "compositeImage": {
                "id": "5bf6f288-3aaf-4fe2-9e5f-adfe3e5dbd76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cd296bc-d473-4273-82a7-f4815ba9e474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f601d5-9fd4-405c-9c56-c358f088afc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cd296bc-d473-4273-82a7-f4815ba9e474",
                    "LayerId": "9e8948ee-7402-478f-b403-ecb3dbeee534"
                }
            ]
        },
        {
            "id": "a74072b4-4931-41ee-9998-ea5f0761b04a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f74c8ca-93a4-41bb-835e-a0e69bf7b5a9",
            "compositeImage": {
                "id": "6be1abfb-bf09-4868-b7dd-3284cb4e3a27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a74072b4-4931-41ee-9998-ea5f0761b04a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e5f8062-23e2-4ecc-9df1-3d2a54f220a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a74072b4-4931-41ee-9998-ea5f0761b04a",
                    "LayerId": "9e8948ee-7402-478f-b403-ecb3dbeee534"
                }
            ]
        },
        {
            "id": "65efdd30-f733-4daf-ab66-ac37738aab3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f74c8ca-93a4-41bb-835e-a0e69bf7b5a9",
            "compositeImage": {
                "id": "bcb9e396-25bd-4924-8972-7135f25184ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65efdd30-f733-4daf-ab66-ac37738aab3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2832df25-1f1a-4543-8c84-c9ec146662b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65efdd30-f733-4daf-ab66-ac37738aab3b",
                    "LayerId": "9e8948ee-7402-478f-b403-ecb3dbeee534"
                }
            ]
        },
        {
            "id": "b7ddf3a9-e677-4ae1-befd-db45f1f2a98f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f74c8ca-93a4-41bb-835e-a0e69bf7b5a9",
            "compositeImage": {
                "id": "44813a41-1f04-4a3b-86b4-7bfb9a2e9e16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7ddf3a9-e677-4ae1-befd-db45f1f2a98f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "237595f6-4856-4e01-b943-e653094896ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7ddf3a9-e677-4ae1-befd-db45f1f2a98f",
                    "LayerId": "9e8948ee-7402-478f-b403-ecb3dbeee534"
                }
            ]
        },
        {
            "id": "171b2ba1-ffe7-4f72-b43f-47b9bbb5a017",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f74c8ca-93a4-41bb-835e-a0e69bf7b5a9",
            "compositeImage": {
                "id": "eeb45d0d-2c17-41b6-94a1-6c7f67c4400f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "171b2ba1-ffe7-4f72-b43f-47b9bbb5a017",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf96e936-5eeb-4f32-8175-0fbd42f2e5ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "171b2ba1-ffe7-4f72-b43f-47b9bbb5a017",
                    "LayerId": "9e8948ee-7402-478f-b403-ecb3dbeee534"
                }
            ]
        },
        {
            "id": "5f7b07c5-c715-402a-a259-0e1ecfee2ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f74c8ca-93a4-41bb-835e-a0e69bf7b5a9",
            "compositeImage": {
                "id": "c17fd35e-727b-49c8-aabb-8c16f535a76d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f7b07c5-c715-402a-a259-0e1ecfee2ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44d684f1-4ee2-4d3f-9e69-1200f16f786d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7b07c5-c715-402a-a259-0e1ecfee2ea9",
                    "LayerId": "9e8948ee-7402-478f-b403-ecb3dbeee534"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 141,
    "layers": [
        {
            "id": "9e8948ee-7402-478f-b403-ecb3dbeee534",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f74c8ca-93a4-41bb-835e-a0e69bf7b5a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 44,
    "yorig": 46
}