{
    "id": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challange34buttonout",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb056df6-594d-4fd1-bc01-3408b37cd707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "compositeImage": {
                "id": "0ee8b6da-78ed-4421-87d0-03068bbe4d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb056df6-594d-4fd1-bc01-3408b37cd707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c73cf174-591a-41a9-ae7d-f986677e09b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb056df6-594d-4fd1-bc01-3408b37cd707",
                    "LayerId": "4a7b0fb8-9800-4807-be2a-89036ddaf587"
                }
            ]
        },
        {
            "id": "2cd72d6c-ad6e-475f-b263-d81cdc854566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "compositeImage": {
                "id": "51f193bf-a04f-47a8-9616-b696c128a302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cd72d6c-ad6e-475f-b263-d81cdc854566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7fbeebf-d5c3-4984-a061-c53b462fa606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cd72d6c-ad6e-475f-b263-d81cdc854566",
                    "LayerId": "4a7b0fb8-9800-4807-be2a-89036ddaf587"
                }
            ]
        },
        {
            "id": "ec3ed0ff-a8de-4a40-bb4d-84dd8faf5488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "compositeImage": {
                "id": "0ff0a35c-05b1-45f9-b440-02a90bcff13c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec3ed0ff-a8de-4a40-bb4d-84dd8faf5488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3019af62-325a-44e8-8b96-e64fd8f83ac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec3ed0ff-a8de-4a40-bb4d-84dd8faf5488",
                    "LayerId": "4a7b0fb8-9800-4807-be2a-89036ddaf587"
                }
            ]
        },
        {
            "id": "39484d43-698a-4e0e-ac4c-8f4480340af6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "compositeImage": {
                "id": "fd618487-ab63-411c-9b9a-2b1ba8bfdf28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39484d43-698a-4e0e-ac4c-8f4480340af6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28166b61-7fa1-463b-9603-0621ef434a20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39484d43-698a-4e0e-ac4c-8f4480340af6",
                    "LayerId": "4a7b0fb8-9800-4807-be2a-89036ddaf587"
                }
            ]
        },
        {
            "id": "a58f5b91-c4c4-4204-b795-64664f76c787",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "compositeImage": {
                "id": "f85cd772-8af3-4753-9205-131a4a156d5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a58f5b91-c4c4-4204-b795-64664f76c787",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5606a3d6-0f3c-4dc7-a3a0-345f2b9ee165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a58f5b91-c4c4-4204-b795-64664f76c787",
                    "LayerId": "4a7b0fb8-9800-4807-be2a-89036ddaf587"
                }
            ]
        },
        {
            "id": "20c44794-61b2-4eb3-b6ae-8309617619f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "compositeImage": {
                "id": "39b76edf-a8db-482e-8902-87c05e5ce232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20c44794-61b2-4eb3-b6ae-8309617619f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e33b80a7-3df6-4e7f-af4e-5eccd8b89310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20c44794-61b2-4eb3-b6ae-8309617619f2",
                    "LayerId": "4a7b0fb8-9800-4807-be2a-89036ddaf587"
                }
            ]
        },
        {
            "id": "d2129039-4cc5-43f3-bd16-fded7dbbd58a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "compositeImage": {
                "id": "282a5005-8f01-45b6-9590-684fc9c1dc4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2129039-4cc5-43f3-bd16-fded7dbbd58a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eef39637-ed33-4b54-9a62-112e4013d058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2129039-4cc5-43f3-bd16-fded7dbbd58a",
                    "LayerId": "4a7b0fb8-9800-4807-be2a-89036ddaf587"
                }
            ]
        },
        {
            "id": "fc7aba9b-7585-432f-94f2-571e5a4d972b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "compositeImage": {
                "id": "fdfc28fc-dc27-4b05-a322-13860ea6c6a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc7aba9b-7585-432f-94f2-571e5a4d972b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5a93b06-f348-45bd-88e3-a87897aab7cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc7aba9b-7585-432f-94f2-571e5a4d972b",
                    "LayerId": "4a7b0fb8-9800-4807-be2a-89036ddaf587"
                }
            ]
        },
        {
            "id": "91ae46dc-8189-45d8-803e-135643c90b3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "compositeImage": {
                "id": "9efe0069-1ae0-4272-9291-227f3ee18a74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91ae46dc-8189-45d8-803e-135643c90b3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb1e068-54d2-4a47-a8d2-f7e01917838b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91ae46dc-8189-45d8-803e-135643c90b3e",
                    "LayerId": "4a7b0fb8-9800-4807-be2a-89036ddaf587"
                }
            ]
        },
        {
            "id": "6cdf754e-a57c-460b-8d4d-0edf5649e656",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "compositeImage": {
                "id": "bfcaa7ba-71fa-4f74-9d3b-790b97e1a435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cdf754e-a57c-460b-8d4d-0edf5649e656",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8375bc49-c7c6-4650-9aaf-2367ce13e6d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cdf754e-a57c-460b-8d4d-0edf5649e656",
                    "LayerId": "4a7b0fb8-9800-4807-be2a-89036ddaf587"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "4a7b0fb8-9800-4807-be2a-89036ddaf587",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf612d5c-0deb-4c8c-8d9c-402b60d77263",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 53,
    "yorig": 52
}