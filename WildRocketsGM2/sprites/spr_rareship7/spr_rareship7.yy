{
    "id": "b8248a79-0056-4443-984a-49b7c3516cf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rareship7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 128,
    "bbox_left": 34,
    "bbox_right": 88,
    "bbox_top": 50,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22813b3f-6623-4b00-a184-8ef67cda6b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "compositeImage": {
                "id": "de948bbb-4268-428d-affc-0be026063fba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22813b3f-6623-4b00-a184-8ef67cda6b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ceaa923-9ed6-4f04-b653-fdc179a79c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22813b3f-6623-4b00-a184-8ef67cda6b1d",
                    "LayerId": "9f7196b7-9001-4a0c-b837-dbfbe080897c"
                }
            ]
        },
        {
            "id": "852a4e31-86a0-473d-8f16-03db7541c6a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "compositeImage": {
                "id": "56fab28f-e2e8-4983-ade6-de5e536fe26c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "852a4e31-86a0-473d-8f16-03db7541c6a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9d121fd-af6c-490a-84e2-4692fc16da5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "852a4e31-86a0-473d-8f16-03db7541c6a2",
                    "LayerId": "9f7196b7-9001-4a0c-b837-dbfbe080897c"
                }
            ]
        },
        {
            "id": "9642a2d3-31e8-4d70-be5a-a902134ea740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "compositeImage": {
                "id": "dd209f21-92ca-4ae6-81d8-fc60144ccfb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9642a2d3-31e8-4d70-be5a-a902134ea740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5b8aaac-d8ec-43ce-966f-20be2145e334",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9642a2d3-31e8-4d70-be5a-a902134ea740",
                    "LayerId": "9f7196b7-9001-4a0c-b837-dbfbe080897c"
                }
            ]
        },
        {
            "id": "8fc83b80-3539-44bf-99c7-fcbabf058167",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "compositeImage": {
                "id": "570d0c15-e3a8-4e68-bc34-971808541f46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fc83b80-3539-44bf-99c7-fcbabf058167",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de205f2e-6cf1-4e7c-a810-a164b7dad9ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fc83b80-3539-44bf-99c7-fcbabf058167",
                    "LayerId": "9f7196b7-9001-4a0c-b837-dbfbe080897c"
                }
            ]
        },
        {
            "id": "596660cb-fe5a-4931-b5b9-1931610d47ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "compositeImage": {
                "id": "aa1de3fc-e8a9-4d71-9061-48993c301080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "596660cb-fe5a-4931-b5b9-1931610d47ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beb57511-c652-4db9-ab64-44493dee0705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "596660cb-fe5a-4931-b5b9-1931610d47ea",
                    "LayerId": "9f7196b7-9001-4a0c-b837-dbfbe080897c"
                }
            ]
        },
        {
            "id": "df26e15f-f886-4a33-8f14-9a7b2b32d156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "compositeImage": {
                "id": "ee686559-1967-4cad-b367-c82d2653a484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df26e15f-f886-4a33-8f14-9a7b2b32d156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82fbd9e5-fc7e-4466-a513-1a0e2e6354a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df26e15f-f886-4a33-8f14-9a7b2b32d156",
                    "LayerId": "9f7196b7-9001-4a0c-b837-dbfbe080897c"
                }
            ]
        },
        {
            "id": "d7aae7a4-33c4-4599-9f46-9d64826a4fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "compositeImage": {
                "id": "cbb272d7-e5f3-4acd-86a4-08e970eb27d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7aae7a4-33c4-4599-9f46-9d64826a4fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d62c0e9-ef07-47dd-a12a-417142d646af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7aae7a4-33c4-4599-9f46-9d64826a4fc6",
                    "LayerId": "9f7196b7-9001-4a0c-b837-dbfbe080897c"
                }
            ]
        },
        {
            "id": "1e0bfa49-89a4-41cc-9ae4-846548230468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "compositeImage": {
                "id": "d44cf3a1-c8f7-4564-8802-5599cd53ce58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e0bfa49-89a4-41cc-9ae4-846548230468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c70532-1ad6-499c-887a-0430e49e53e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e0bfa49-89a4-41cc-9ae4-846548230468",
                    "LayerId": "9f7196b7-9001-4a0c-b837-dbfbe080897c"
                }
            ]
        },
        {
            "id": "f090b107-a4ce-4caf-a887-202b108b880b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "compositeImage": {
                "id": "24ab2082-fff8-4226-880b-2c71eeb4901a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f090b107-a4ce-4caf-a887-202b108b880b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c0e313c-4559-46f2-a013-250999949b8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f090b107-a4ce-4caf-a887-202b108b880b",
                    "LayerId": "9f7196b7-9001-4a0c-b837-dbfbe080897c"
                }
            ]
        },
        {
            "id": "6ffa0ce6-f0df-4243-bec0-734d0eb0061a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "compositeImage": {
                "id": "929245a6-7307-4fcd-b919-89a8ba19e9dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ffa0ce6-f0df-4243-bec0-734d0eb0061a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "148cf5ba-346d-49d4-abef-bf87cb098dec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ffa0ce6-f0df-4243-bec0-734d0eb0061a",
                    "LayerId": "9f7196b7-9001-4a0c-b837-dbfbe080897c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "9f7196b7-9001-4a0c-b837-dbfbe080897c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8248a79-0056-4443-984a-49b7c3516cf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 61,
    "yorig": 91
}